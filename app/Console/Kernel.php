<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $txtconnect = new \App\Http\Models\otsuka\Txtconnect;

            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

            $url = "https://lc-txtconnect5.globe.com.ph/inbox?token=".$token->token."&txtid=onequest";
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
            $json = curl_exec($ch);
            if(!$json) {
                echo curl_error($ch);
            }
            curl_close($ch);

            if(json_decode($json)->response->code !== 200)
            {
                $url = "https://lc-txtconnect5.globe.com.ph/login?username=onequest&password=3dc2d8f42470f4a22018a9d84e48837b047d09bb7ffddd8caf07d4aedb54faf2&app_id=oqs";
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,$url);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
                $json = curl_exec($ch);
                if(!$json) {
                    echo curl_error($ch);
                }
                curl_close($ch);
                
                $txtconnect->token = json_decode($json)->response->token;
                $txtconnect->save();

            }

            else
            {
                $url = "https://lc-txtconnect5.globe.com.ph/inbox?token=".$token->token."&txtid=onequest";
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,$url);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
                $json = curl_exec($ch);
                if(!$json) {
                    echo curl_error($ch);
                }
                curl_close($ch);
            }
            
            //print_r(json_decode($json));
            //echo(json_decode($json)->response->token);

        })->everyThirtyMinutes();


        $schedule->call(function () {
            $upload = new \App\Http\Models\otsuka\Upload;

        $now = date('Y-m-d');
        $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

            $callback = $upload->select('id')->where('type','=','Enroll')->where('verification_attempt', '!=','5')->where('callback_date',$now)->where('verify_submit',0)->get()->toArray();
            $update_callback = $upload->whereIn('id',$callback)->update(['callback_date' => $tomorrow]);

/*            $callback = $upload->select('id')->where('type','=','Enroll')->where('verification_attempt', '!=','5')->where('callback_date','=','2018-02-19')->where('verify_submit',0)->get()->toArray();
            $update_callback = $upload->whereIn('id',$callback)->update(['callback_date' => '2018-02-20']);*/

        })->cron('0 23 * * *');

        $schedule->call(function () {
            $upload = new \App\Http\Models\otsuka\Upload;


            $now = date('Y-m-d');
            $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));
            $yesterday = date('Y-m-d', strtotime($now.'-1 day'));

            $callback = $upload->select('id')->where('type','=','Enroll')->where('verification_attempt', '!=','5')->where(function($query){$query->where('verification_status','!=','1')->orWhere('verification_status','!=','2');})->where('verify_submit',0)->get()->toArray();

            $update_callback = $upload->whereIn('id',$callback)->update(['callback_date' => $now]);

/*            $callback = $upload->select('id')->where('type','=','Enroll')->where('verification_attempt', '!=','5')->where('callback_date','=','2018-02-19')->where('verify_submit',0)->get()->toArray();
            $update_callback = $upload->whereIn('id',$callback)->update(['callback_date' => '2018-02-20']);*/

        })->cron('0 3 * * *');

        $schedule->call(function () {
            $months = new \App\Http\Models\otsuka\Months;


            $month_now = date('m');
            $year_now = date('Y');
            $now = date('Y-m-d');
            $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));
            $yesterday = date('Y-m-d', strtotime($now.'-1 day'));
            $last_month = date('m', strtotime($now.'-1 month'));

            //pre($last_month);

            $check_months = $months->select('id')->where('month','=',$month_now)->where('year','=',$year_now)->first();
            $check_last_month = $months->select('id')->where('month','=',$last_month)->where('year','=',$year_now)->first();

            //pre($check_months);

            //pre(empty($check_months)?$check_months:'')

            if(empty($check_months))
            {
            $months = new \App\Http\Models\otsuka\Months;

            //pre(date('F'));
            //pre(date('F', $month_now).' '.date('Y'));

            $months->name = date('F').' '.date('Y');
            $months->month = date('m');
            $months->year = date('Y');
            $months->tag_deleted = 0;
            $months->save();

            }
            elseif(empty($check_last_month))
            {
            $months->name = date('F', strtotime($now.'-1 month')).' '.date('Y');
            $months->month = date('m', strtotime($now.'-1 month'));
            $months->year = date('Y');
            $months->tag_deleted = 0;
            $months->save(); 
            }

        })->cron('* * * * *');

        $schedule->call(function () {
            $upload = new \App\Http\Models\otsuka\Upload;
            $med_rep = new \App\Http\Models\otsuka\Medrep_distro;
            $patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
            $daily_emails = new \App\Http\Models\otsuka\Daily_emails;
            $user = new \App\Http\Models\gem\User;


            $now = date('Y-m-d');

            //$mr_list = $med_rep->select(['new_code','email', 'pso_name','psm_email','psm_name'])->get()->toArray();

            //$excluded = array('Referral','Voluntary');

            $px_kit = $upload->select('patient_kit_number')->where('active',1)->whereNull('patient_type')->whereBetween('created_at',['2018-02-01', $now])->get()->toArray();
            $area_code = $patients_kit_allocation->select('area_code')->whereIn('one_quest_id',$px_kit)->where('product')->get()->toArray();
            $email_recipient = $med_rep->select(['emp_code','new_code','email', 'pso_name','psm_email','psm_name'])->get()->toArray();

            //pre($email_recipient);

            foreach($email_recipient as $email)
            {       
                    $ytd_start = '2018-02-01 00:00:00';
                    $mtd_start = date('Y-m-01').' 00:00:00';
                    $now = date('Y-m-d').' 17:00:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-2 day')).' 17:00:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

/*                    $mtd_start = '2018-02-01 00:00:00';
                    $yesterday = '2018-02-01 00:00:00';
                    $now =  '2018-02-21 21:00:00';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));*/

                    //$product = array('Abilify', 'Aminoleban', 'Pletaal');

                    //pre($yesterday);

                    $data = $user->select('id')->where('email',str_replace(' ', '', $email['emp_code']))->get()->toArray();

                    $test_email = 'marc.briones@magellan-solutions.com';


                    //pre($data);

                    $data2 = $upload->where('active',1)->where('uploaded_by', str_replace(' ', '', $data))->where('type', '=', 'Enroll')->whereBetween('created_at',[$yesterday, $now])->with(['fetch_user','fetch_mdinfo','fetch_status','fetch_call','fetch_allocation_report','encoded_by'])->get();
                    $verified_count = $upload->where('active',1)->where('uploaded_by', str_replace(' ', '', $data))->where('type', '=', 'Enroll')->whereBetween('created_at',[$yesterday, $now])->where('patient_consent','!=','')->whereNotNull('patient_consent')->where('trail_status','!=','199')->count();
                    $notverified_count = $upload->where('active',1)->where('uploaded_by', str_replace(' ', '', $data))->where('type', '=', 'Enroll')->whereBetween('created_at',[$yesterday, $now])->where(function($query){$query->whereNull('patient_consent')->orWhere('patient_consent','=','')->orWhere('trail_status',199);})->count();
                    $mtd_count = $upload->where('active',1)->where('uploaded_by', str_replace(' ', '', $data))->where('type', '=', 'Enroll')->whereBetween('created_at',[$mtd_start, $now])->count();
                    $ytd_count = $upload->where('active',1)->where('uploaded_by', str_replace(' ', '', $data))->where('type', '=', 'Enroll')->whereBetween('created_at',[$ytd_start, $now])->count();

/*                    $to = 'marc.briones@magellan-solutions';
                    $from = 'info@onequest.com.ph';
                    $cc = 'webdev@magellan-solutions.com';
                    $bcc = 'webdev_reports@magellan-solutions.com';
                    $EmailSubject = "Daily individual Retrieval Report"." - ".date('Ymd'). " TEST";*/

                    $to = str_replace(' ', '', $email['email']);
                    //$to = str_replace(' ', '', $test_email);
                    $from = "info@onequest.com.ph";
                    $cc = [str_replace(' ', '', $email['psm_email']),'ahirata@otsuka.com.ph','mcastro@otsuka.com.ph'];
                    //$cc = ['marc.briones@magellan-solutions.com'];
                    $bcc = ['webdev@magellan-solutions.com','gerardo.resano@magellan-solutions.com'];
                    $EmailSubject = "Daily individual Retrieval Report"." - ".date('Ymd');

                    $daily_emails = new \App\Http\Models\otsuka\Daily_emails;
                    $daily_emails->subject = json_encode($EmailSubject);
                    $daily_emails->from = json_encode($from);
                    $daily_emails->to = json_encode($to);
                    $daily_emails->cc = json_encode($cc);
                    $daily_emails->bcc = json_encode($bcc);
                    $daily_emails->content = json_encode($data2);
                    $daily_emails->verified_count = $verified_count;
                    $daily_emails->notverified_count = $notverified_count;
                    $daily_emails->mtd_count = $mtd_count;
                    $daily_emails->ytd_count = $ytd_count;

                    if($to == $test_email)
                    {
                        $daily_emails->status = 'test';
                    }
                    else
                    {
                        $daily_emails->status = 'live';
                    }


                    $daily_emails->save();

                    $daily_id = $daily_emails->id;

                    //pre($emailcc);

                    if($to && $from):

                        Mail::send('email.daily_emails', [  'data2' => $data2,
                                                            'medrep' => trim($email['pso_name'],'"'),
                                                            'verified_count'=> $verified_count,
                                                            'notverified_count'=> $notverified_count,
                                                            'mtd_count'=> $mtd_count,
                                                            'ytd_count' => $ytd_count,

                                                        ], function ($m) use ($to,$from,$cc,$bcc,$EmailSubject){                                
                            $m->from($from);
                            $m->to($to)->subject($EmailSubject);
                            $m->cc($cc);
                            $m->bcc($bcc);
                        });              

                    endif;  

                    $check_sent = $daily_emails->where('id',$daily_id)->update(['sent' => 1]);
                    //$counter = $user->where('emp_code',str_replace(' ', '', $email['emp_code']))->update(['counter' => 1]);

            }


        })->cron('0 20 * * *'); 
        //})->cron('20 17 * * *');

    }
}
