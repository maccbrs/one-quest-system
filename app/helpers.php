<?php

function gravatar_profile($email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array()){

	    $url = 'https://www.gravatar.com/avatar/';
	    $url .= md5( strtolower( trim( $email ) ) );
	    $url .= "?s=$s&d=$d&r=$r";
	    if ( $img ) {
	        $url = '<img src="' . $url . '"';
	        foreach ( $atts as $key => $val )
	            $url .= ' ' . $key . '="' . $val . '"';
	        $url .= ' class="avatar img-circle" />';
	    }
	    return $url;
	    
}	

function pre($arr,$die = true){

	echo '<pre>';
	if(is_array($arr) || is_object($arr)){
		 print_r($arr);
	}else{
		echo $arr; 
	}
	
	if($die) die;
}

function mbTimeZones(){
	$tz = [];
	foreach(timezone_abbreviations_list() as $abbr => $timezone){
	        foreach($timezone as $val){
	                if(isset($val['timezone_id']) && !in_array($val['timezone_id'], $tz)){
	                        $tz[] = $val['timezone_id'];
	                }
	        }
	}	
	return $tz;
}

function isAllowed($user,$app){

	if($user->user_type == 'administrator') return true;

	if($user->access):
		$arr = json_decode($user->access,true);
		if($arr):
			if(in_array($app,$arr)) return true;
		endif;
	endif;
	return false;
}

function isAdmin($user){

	if($user->user_type == 'administrator') return true;

	return false;
}

function roundsix($n){
	if($n):
		if($n<=30) return 30;
		$a = $n - 30;
		$abs = floor($a/6)*6;
		$mod = (($n%6)>0?6:0);
		return $abs+$mod+30;
	endif;
	return 0;
}

function month($n){

	$arr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	return $arr[$n-1];

}

function isPast($date){

	if(date('Y-m-d') > $date) return 1;

	return 0;
}

