<?php
namespace App\Helpers;
use App\Http\Requests;
use App\Http\Models\bloodstone\Logs;
use Auth;

class AppHelper
{
	
	  
	  
	public function SaveLog($data)
	{
		
		$masterfilelist = new \App\Http\Models\otsuka\Logs;
		$masterfilelist ->description = strtolower($data);
		$masterfilelist ->user = Auth::user()->name;
		$masterfilelist ->user_id = Auth::user()->id;
		$masterfilelist ->save();		
		
		return ;
		
		
	}
	
	public function DisplayLog($data)
	{
		
		$Logs = new \App\Http\Models\otsuka\Logs;

		$larr_myarray = array();
		//return $masterfilelist = Logs::where('user_id', '=' , $data)->get();
		return json_encode($masterfilelist = Logs::where('user_id', '=' , $data)->get()->sortByDesc('id'),true);

	}
	public function fetchuser($data)
	{
		
		$User = new \App\Http\Models\gem\GemUser;
		$larr_myarray = array();
		$masterfilelist = $User::find($data);
		return ($masterfilelist);
		
		

	}
	
	public function Approver($data)
	{
		
		$Logs = new \App\Http\Models\bloodstone\Logs;

		$larr_myarray = array();
	
		return json_encode($masterfilelist = Logs::where('user_id', '=' , $data)->get()->sortByDesc('id'),true);

	}
	// Fetch Table By ID -- Return All Record 
	public function fetchTableByID($ref,$ModelName)
	{
		
		
		
		$table_model = array("\App\Http\Models" . chr(92) .$ModelName);	
		$Model = new $table_model[0];
		$larr_myarray = array();
		$masterfilelist = $Model::find($ref);
		return ($masterfilelist);
		
		

	}
	// Fetch Table By Conditions -- Return All Record 
		public function fetchTableByCondition($ref,$ModelName)
	{
		if (Auth::user()->is_representative == 0){
		$table_model = array("\App\Http\Models" . chr(92) .$ModelName);	
		$Model = new $table_model[0];
		
		$larr_myarray = array();
		//return $masterfilelist = Logs::where('user_id', '=' , $data)->get();
		$masterfilelist = $Model::where('level1', '=' , $ref)->get()->sortByDesc('id');
		}
		else 
		{$masterfilelist = null;}
		return ($masterfilelist);


	}
			
	// Fetch Table -- Return All Record 
		public function fetchTable($ModelName,$sort,$sortby)
	{
		
		$table_model = array("\App\Http\Models" . chr(92) .$ModelName);	
		$Model = new $table_model[0];
						
		$masterfilelist = $Model::All()->$sortby($sort);
		
		return ($masterfilelist);
		
		//return json_encode($masterfilelist = $User::find($data);

	}
	
	public function DisplayRecordHistory($data)
	{
		
		$Logs = new \App\Http\Models\bloodstone\AssignLogs;

		$larr_myarray = array();
		//return $masterfilelist = Logs::where('user_id', '=' , $data)->get();
		$masterfilelist = $Logs::where('unique_id', '=' , $data)->get()->sortByDesc('id');
		return ($masterfilelist);

	}
	
	
		// Fetch Table By ID -- Return All Record 
	public function fetchSKUProgram($ref)
	{

		$Model = new \App\Http\Models\otsuka\Psp_Sku_Program;				
		$masterfilelist = $Model::where("skuid","=",$ref)->get()->first();
		return ($masterfilelist);
		
	}
	
	
			// Fetch Table By ID -- Return All Record 
	public function fetchPatientTypeLegend($ref)
	{

		$Model = new \App\Http\Models\otsuka\SystemSetting;			
		if (strtolower($ref) == 'referral')
		{
			$masterfilelist = $Model::where("key","=","patient_type_referral")->first();
		}
		elseif (strtolower($ref) == 'retrieval')
		{
			$masterfilelist = $Model::where("key","=","patient_type_retrieval")->first();
		}
		elseif (strtolower($ref) == 'voluntary')
		{
			$masterfilelist = $Model::where("key","=","patient_type_voluntary")->first();
		}
		else
		{
			$masterfilelist = $Model::where("key","=","patient_type_default")->first();
			
		}
		
		
		return ($masterfilelist);
		
	}
	
	
				// Fetch Table By ID -- Return All Record 
	public function fetchUpload($ref)
	{

		$Model = new \App\Http\Models\otsuka\Upload;		
		$modelRecord = $Model->select('filename')->where('patientid','=',$ref)->first();
		if(!empty($modelRecord))
		{	$image = $modelRecord->filename;
		}
		else{
			$image = "default-image.jpg";
			
		}
		
		//if($modelRecord)
		
		
		return ($image);
		
	}
	
		public function fetch_uploader($ref)
	{

		$Model = new \App\Http\Models\otsuka\Upload;		
		$modelRecord = $Model->where('patientid','=',$ref)->first();
		//pre($modelRecord->user->name);
		if(!empty($modelRecord))
		{	$image = $modelRecord->user->name;
		}
		else{
			$image = "";
			
		}
		
		//if($modelRecord)
		
		
		return ($image);
		
	}
	
	
	
	
	public function fetchRedemptionStatus($ref)
	{
		$Patient_Validated_Model = new \App\Http\Models\otsuka\Patient_Validated;
			
			$Patient_Validated = $Patient_Validated_Model->find(18446);
			$checkYTD = new \App\Http\Models\otsuka\Ytd_Redemption;	
				$redeem_counter = $checkYTD->where('patient_code','=',$Patient_Validated->patient_code)->where('no_tab_free','!=','')->count();
				if($redeem_counter > 0){
				$Patient_Validated->date_of_first_redeem =  date('Y-m-d H:i:s');
				}
				$Patient_Validated->redeem_counter = $redeem_counter;
			
			dd($redeem_counter);
			//return 
		
		
	}
	
  
}