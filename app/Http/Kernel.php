<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        //\App\Http\Middleware\VerifyCsrfToken::class,
        \UxWeb\SweetAlert\ConvertMessagesIntoSweatAlert::class,        
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [

        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually. 
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'opal' => \App\Http\Middleware\OpalAccess::class,
        'bloodstone' => \App\Http\Middleware\bloodstone::class,
        'pearl' => \App\Http\Middleware\PearlAccess::class,
        'central' => \App\Http\Middleware\CentralAccess::class,
        'ruby' => \App\Http\Middleware\RubyAccess::class,
        'diamond' => \App\Http\Middleware\DiamondAccess::class,
        'amber' => \App\Http\Middleware\MakeAmber::class,
        'garnet' => \App\Http\Middleware\MakeGarnet::class,
        'topaz' => \App\Http\Middleware\MakeTopaz::class,
        'it_manager' => \App\Http\Middleware\it_manager::class,
        'noc' => \App\Http\Middleware\mustBeNOC::class,
        'rhyolite' => \App\Http\Middleware\rhyolite::class,
        'jade' => \App\Http\Middleware\jade::class,
        'agate' => \App\Http\Middleware\Agate::class,

    ];
}
