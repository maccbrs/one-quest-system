<?php namespace App\Http\Controllers\jade;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class subscribersController extends Controller
{

	public function index(){

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$subscribers = $Subscribers->where('status',1)->paginate(20);
		return view('jade.subscribers.index',compact('subscribers'));

	}

	public function add(){
		return view('jade.subscribers.add');
	}

	public function create(Request $r){

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$Subscribers->create($r->all());
		return redirect()->route('jade.subscribers.index');

	} 

	public function delete($id){
		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$Subscribers->find($id)->update(['status' => 0]);
		return redirect()->back();
	}

		  /* -------------------------------*/
		 /* ----------- DID  --------------*/
		/* ------------------------------ */

	public function didIndex($id){

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$Connectors = new \App\Http\Models\jade\Connectors;
		$subscriber = $Subscribers->find($id);
		$dids = $Connectors->where('subscriber_id',$id)->where('status',1)->get();

		return view('jade.subscribers.did-index',compact('subscriber','dids'));

	}

	public function didAdd($id){

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$subscriber = $Subscribers->find($id);		
		return view('jade.subscribers.did-add',compact('subscriber'));

	}

	public function didCreate(Request $r,$id){

		$this->validate($r,[
			'did' => 'required|numeric'
		]);

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$subscriber = $Subscribers->find($id);	

		$Connectors = new \App\Http\Models\jade\Connectors;
		$Connectors->create([
			'subscriber_id' => $subscriber->id,
			'did' => $r->did,
			'account_status' => $subscriber->account_status
		]);

		return redirect()->route('jade.subscribers.did-index',$subscriber->id);

	}

	public function didDelete($id){
		$Connectors = new \App\Http\Models\jade\Connectors;
		$Connectors->find($id)->update(['status' => 0]);
		return redirect()->back();
	}

	  /* -------------------------------*/
	 /* ----------- load --------------*/
	/* ------------------------------ */

	public function loadIndex($id){

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$Loads = new \App\Http\Models\jade\Loads;
		$subscriber = $Subscribers->find($id);
		$loads = $Loads->where('subscriber_id',$id)->orderBy('startdate','desc')->get();

		return view('jade.subscribers.load-index',compact('subscriber','loads'));

	}

	public function loadAdd($id){

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$subscriber = $Subscribers->find($id);		
		return view('jade.subscribers.load-add',compact('subscriber'));

	} 

	public function loadCreate(Request $r,$id){

		$this->validate($r,[
			'minutes' => 'required|numeric',
			'month' => 'required',
			'year' => 'required'
		]);

		$Loads = new \App\Http\Models\jade\Loads;

		$exist = $Loads->where('subscriber_id',$id)
				->where('month',$r->month)
				->where('year',$r->year)
				->count();

		if($exist)
		 	return redirect()->back()->with('error','Load already exist, you might want to edit the existing?');

		$Date = Carbon::createFromDate($r->year, $r->month);

		$a[] = ['minutes' => $r->minutes,'date' => Carbon::now()->format('Y-m-d H:i:s'),'by' => Auth::user()->name];

		$Loads->create([
			'minutes' => $r->minutes,
			'remaining' => $r->minutes, 
		 	'subscriber_id' => $id,
		 	'month' => $r->month,
		 	'year' => $r->year,
		 	'breakdown' => json_encode($a),
		 	'startdate' => $Date->startOfMonth()->format('Y-m-d'),
		 	'enddate' => $Date->endOfMonth()->format('Y-m-d') 
		]);

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$subscriber = $Subscribers->find($id);	

		return redirect()->route('jade.subscribers.load-index',$subscriber->id)->with('success','Load successfully added!');

	}

	public function loadDelete($id){

		$Loads = new \App\Http\Models\jade\Loads;
		$Loads->destroy($id);
		return redirect()->back()->with('success','Load deleted!');
	}	

	public function loadView($id){

		$Loads = new \App\Http\Models\jade\Loads;
		$load = $Loads->with(['subscriber'])->find($id);
	//	pre($load);
		return view('jade.subscribers.load-view',compact('load'));

	}

    
    	public function loadEdit($id){

		$Loads = new \App\Http\Models\jade\Loads;
		$load = $Loads->with(['subscriber'])->find($id);
	//	pre($load);
		return view('jade.subscribers.load-edit',compact('load'));

	}
	public function loadAddminute(Request $r,$id){

		$this->validate($r,['minutes' => 'required|numeric']);
		$Loads = new \App\Http\Models\jade\Loads;
		$load = $Loads->find($id);

		$a = json_decode($load->breakdown,true);
		$a[] = ['minutes' => $r->minutes,'date' => Carbon::now()->format('Y-m-d H:i:s'),'by' => Auth::user()->name];
		$min = 0;
		
		foreach($a as $v):
			$min += $v['minutes'];
		endforeach;

		$load->minutes = $min;
		$load->remaining = $load->remaining + $r->minutes;
		$load->breakdown = json_encode($a);
		$load->save();

		$this->reloadLoad();

		return redirect()->back()->with('success','minute added!');

	} 





	public function loadReserved($id){

		$Reserved = new \App\Http\Models\jade\Reserved;
		$reserved = $Reserved->firstOrCreate(['subscriber_id' => $id]);
		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$subscriber = $Subscribers->find($id);

		return view('jade.subscribers.load-reserved',compact('reserved','subscriber'));

	}

	public function loadAddreserved(Request $r,$id){

		$this->validate($r,['minutes' => 'required|numeric']);
		$Reserved = new \App\Http\Models\jade\Reserved;
		$res = $Reserved->find($id);

		$a = $res->breakdown?json_decode($res->breakdown,true):[];
		$a[] = ['minutes' => $r->minutes,'date' => Carbon::now()->format('Y-m-d H:i:s'),'by' => Auth::user()->name];
		$min = 0;
		
		foreach($a as $v):
			$min += $v['minutes'];
		endforeach;

		$res->minutes = $min;
		$res->remaining = $res->remaining + $r->minutes;
		$res->breakdown = json_encode($a);
		$res->save();

		$this->reloadLoad();
		
		return redirect()->back()->with('success','reserved minutes added!');

	} 

	public function reloadLoad(){

		$year = date('Y');
		$month = date('m');

		$Connectors = new \App\Http\Models\jade\Connectors;
		$Loads = new \App\Http\Models\jade\Loads;
		$Reserved = new \App\Http\Models\jade\Reserved;
		$items = $Connectors->groupBy('subscriber_id')->get();

		foreach($items as $item):
			$load_id = $load_status = $reserved_status = $reserved_id = 0;

			$load = $Loads->where('subscriber_id',$item->subscriber_id)->where('month',$month)->where('year',$year)->where('status',1)->first();
			if($load):
				$load_id = $load->id;
				$load_status = 1;
			else:
				$load_status = 0;
			endif;

			$res = $Reserved->where('subscriber_id',$item->subscriber_id)->where('status',1)->first();
			if($res):
				$reserved_id = $res->id;
				$reserved_status = 1;
			else:
				$reserved_status = 0;
			endif;
			
			$Connectors->where('subscriber_id',$item->subscriber_id)->update([
					'load_id' => $load_id,
					'load_status' => $load_status,
					'reserved_status' => $reserved_status,
					'reserved_id' => $reserved_status
				]);

		endforeach; 

	}

	public function settings($id){

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$subscriber = $Subscribers->find($id);

		return view('jade.subscribers.settings',compact('subscriber'));

	}

	public function settingsAccountTypeUpdate(Request $r,$id){

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$Subscribers->find($id)->update(['account_status' => $r->value]);

		$Connectors = new \App\Http\Models\jade\Connectors;
		
		$connectors = $Connectors->where('subscriber_id',$id);

		if($connectors->count()):
			$connectors->update([
				'account_status' => $r->value
			]);
		endif;

	}

	public function refreshRemaining(){

	}


}