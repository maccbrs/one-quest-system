<?php namespace App\Http\Controllers\jade;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class usersController extends Controller
{

	public function index(){

		$Users = new \App\Http\Models\jade\Users;
		$users = $Users->get();
		return view('jade.users.index',compact('users'));

	}

	public function add(){

		$GemUsers = new \App\Http\Models\jade\GemUsers;
		$gemusers = $GemUsers->orderBy('name','asc')->get();

		return view('jade.users.add',compact('gemusers'));

	}


	public function create(Request $r){

		if($r->gem_id):
			$Users = new \App\Http\Models\jade\Users;
			$a = explode('||',$r->gem_id);
			$Users->create(['gem_id' => $a[0],'name' => $a[1]]);

			$GemUsers = new \App\Http\Models\jade\GemUsers;
			$gemuser = $GemUsers->find($a[0]);
			$gemuser->access = $this->updateAccess($gemuser->access,'jade');
			$gemuser->save();
		endif;

		return redirect()->back();

	}

	private function updateAccess($access,$new){

		$data = [];
		if($access):
			$data = json_decode($access,true);
		endif;
		$data[] = $new;
		return json_encode($data);
	}


}