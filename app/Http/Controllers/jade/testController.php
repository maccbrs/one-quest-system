<?php namespace App\Http\Controllers\jade;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class testController extends Controller
{

	public function index(){

		$this->testrun();

	}

	public function testrun(){

		$Testrun = new \App\Http\Models\jade\Testrun;
		$item = $Testrun->where('done',0)->first();
		if($item):

			$this->test1($item->did,$item->transaction_id);

			$item->done = 1;
			$item->save();
		endif;
	}


	public function test1($did,$transid){

		$Connectors = new \App\Http\Models\jade\Connectors;
		$Hungups = new \App\Http\Models\jade\Hungups;

		$msg = '';

		$a = $Connectors->where('did',$did)->first();
		if($a):
			if($a->status):

				$CallDetails = new \App\Http\Models\jade\CallDetails;
				if($a->load_status):

					$CallDetails->create([
					  'transaction_id' => $transid,
					  'did' => $did,
					  'load_id' => $a->load_id,
					  'load_type' => 1,
					  'subscriber_id' => $a->subscriber_id
					]);
					return 1;

				elseif($a->reserved_status):


					$CallDetails->create([
					  'transaction_id' => $transid,
					  'did' => $did,
					  'load_id' => $a->reserved_id,
					  'load_type' => 2,
					  'subscriber_id' => $a->subscriber_id
					]);
					return 1;

				else:

					$msg = 'no load';

				endif;
			else:
				$msg = 'subscriber disabled';
			endif;
		else:
			$msg = 'did '.$did.' not registered';
		endif;

		$Hungups->create([
			'did' => $did,
			'message' => $msg
		]);

		return 0;

	}


	public function getConnectorsLoad(){

		$year = date('Y');
		$month = date('m');

		$Connectors = new \App\Http\Models\jade\Connectors;
		$Loads = new \App\Http\Models\jade\Loads;
		$Reserved = new \App\Http\Models\jade\Reserved;
		$items = $Connectors->groupBy('subscriber_id')->get();

		foreach($items as $item):
			$load_id = $load_status = $reserved_status = $reserved_id = 0;

			$load = $Loads->where('subscriber_id',$item->subscriber_id)->where('month',$month)->where('year',$year)->where('remaining','>',0)->where('status',1)->first();
			if($load):
				$load_id = $load->id;
				$load_status = 1;
			else:
				$load_status = 0;
			endif;

			$res = $Reserved->where('subscriber_id',$item->subscriber_id)->where('remaining','>',0)->where('status',1)->first();
			if($res):
				$reserved_id = $res->id;
				$reserved_status = 1;
			else:
				$reserved_status = 0;
			endif;
			
			$Connectors->where('subscriber_id',$item->subscriber_id)->update([
					'load_id' => $load_id,
					'load_status' => $load_status,
					'reserved_status' => $reserved_status,
					'reserved_id' => $reserved_status
				]);

		endforeach;

	}

	//get details from cic db
	public function check($verified = 0){


		$DboCalldetailViw = new \App\Http\Models\nexus\DboCalldetailViw; 
		$CallDetails = new \App\Http\Models\jade\CallDetails;



		switch($verified):
		 	case 0:

		 		$item = $CallDetails->where('verified',0)->whereNotNull('transaction_id')->first();

		 		if($item):
			 		$call = $DboCalldetailViw->where('CallId','like', $item->transaction_id.'%')->first();
			 		if($call):
			 			$item->transaction_id = $call->CallId;
			 			$item->durations = roundsix($call->CallDurationSeconds);
			 			$item->user = $call->LocalName;
			 		endif;	 		
		 		endif;

	 			$item->verified = 1;
	 			$item->save();	

		 		break;
		 	case 1:
		 	case 2:
		 	case 3:
		 		$items = $CallDetails->where('verified',$verified)->whereNotNull('transaction_id')->get();

		 		if($items->count()):

		 			$callid = [];
		 			foreach($items as $item):
		 				$callid[] = $item->transaction_id;
		 			endforeach;
		 			$calls = $DboCalldetailViw->whereIn('CallId',$callid)->get();
		 			if($calls->count()):

		 				$data = [];
		 				

		 				foreach($calls as $call):
		 					$data[$call->CallId] = roundsix($call->CallDurationSeconds);
		 				endforeach;

			 			foreach($items as $item):
			 				$item->durations = (isset($data[$item->transaction_id])?$data[$item->transaction_id]:0);
			 				$item->verified = $verified + 1;
			 				$item->save();
			 			endforeach;		 				
		 			endif;
		 		endif;

		 		break;		 		
		endswitch;

		return true;
	}

}