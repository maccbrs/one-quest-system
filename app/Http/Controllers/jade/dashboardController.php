<?php namespace App\Http\Controllers\jade;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DateTimeZone;

class dashboardController extends Controller
{

	public function index(){
		$Eod = new \App\Http\Models\jade\Eod;
		$campaigndata = new \App\Http\Models\jade\CampaignData;
		$Loads = new \App\Http\Models\jade\Loads;
		$Overage = new \App\Http\Models\jade\Reserved;
		$loads =  $Loads->current()->with(['subscriber','overage'])->orderBy('remaining','asc')->get();
		$overages = $Overage->with('subscriber')->where('remaining','!=','0')->orderBy('remaining','asc')->get();

		$item = $Eod->where('ready_to_send',1)->where('sent',0)->where('status',1)->get();
		$test = array($item);
		/*$test = json_decode($item->dummyboard_id,true) ? json_decode($item->dummyboard_id,true): [] ;
		//pre($overages);
					$boarddata = $campaigndata->whereBetween('created_at', ['2017-12-13 13:29:00', '2017-12-14 13:28:59'])->whereIn('campaign_id',$test)->get();
					$dboard_data = [];
			if($boarddata->count()):
				foreach($boarddata->toArray() as $a):
					$b['Date'] = $this->convertTime($a['created_at'],$item->timezone);
					$content = json_decode($a['contents']);
					$except = ['request_status'];
					foreach($content as $k => $v):
						$str = $this->str_process($k);
						if(!in_array($str, $except)):
							$b[$str] = $v;
						endif;
					endforeach;
					$dboard_data[] = $b;
				endforeach;
			endif;*/

		pre($test);
		return view('jade.dashboard.index',compact('loads','overages'));
	}

	private function convertTime($date,$tz){

		$a = Carbon::createFromFormat('Y-m-d H:i:s',$date);
		if($tz != 'Asia/Manila'):
			$a->setTimezone('Australia/Sydney');
		endif;
		return $a->format('Y-m-d H:i:s');

	}

	private function str_process($str){
		return ucwords(str_replace('_',' ',$str));
	}

	public function load($id){

		$Loads = new \App\Http\Models\jade\Loads;
		$load = $Loads->with(['subscriber','overage'])->find($id);
		$Calls = $CallDetails = new \App\Http\Models\jade\CallDetails;
		$calls = $Calls->where('load_id',$load->id)->where('load_type',1)->where('is_complete',1)->orderBy('id','desc')->paginate(20);
		//pre($calls->toArray());
		return view('jade.dashboard.load',compact('load','calls'));
	}

	public function overage($id){

		$Reserved = new \App\Http\Models\jade\Reserved;
		$reserved = $Reserved->where('subscriber_id',$id)->with(['subscriber'])->first();
		$Calls = $CallDetails = new \App\Http\Models\jade\CallDetails;
		$calls = $Calls->where('load_id',$reserved->id)->where('load_type',2)->where('is_complete',1)->orderBy('id','desc')->paginate(20);		
		return view('jade.dashboard.reserved',compact('reserved','calls'));
		
	}

	public function client($id){

		$Subscribers = new \App\Http\Models\jade\Subscribers;
		$Connectors = new \App\Http\Models\jade\Connectors;
		$subscriber = $Subscribers->with(['dids'])->find($id);
		//pre($subscriber->toArray());
		return view('jade.dashboard.client',compact('subscriber'));

	}

	public function hangups(){

		$Hangups = new \App\Http\Models\jade\Hungups;
		$hangups =  $Hangups->orderBy('created_at','desc')->get();
		//pre($hangups->toArray());
		return view('jade.dashboard.hangups',compact('hangups'));
	}

}