<?php namespace App\Http\Controllers\gem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use \App\Http\Models\amber\GarnetDeptNotes;
use DB;
use Auth;
use Alert;

class usersController extends Controller
{

	public function index(){

		$Users = new \App\Http\Models\gem\User;
		$Access = new \App\Http\Models\gem\Accesslist;
		$helper = new \App\Http\Controllers\gem\Helper;
		$notedata = [];
		$userId = Auth::user()->id;

		$user_type = Auth::user()->user_type;

		$notes = new GarnetDeptNotes;

		$note_list = $notes
			->orderBy('created_at','desc')
			->get();


		$notedata = null;

		foreach ($note_list as $key => $value) {

			$note_list[$key]['content'] = json_decode($value['content']);
			$note_list[$key]['header'] = json_decode($value['header']);
			$note_list[$key]['type'] = json_decode($value['type']);
				
			$notedata[$value['to']]['header'] = $note_list[$key]['header']; 
			$notedata[$value['to']]['content'] = $note_list[$key]['content']; 
			$notedata[$value['to']]['type'] = $note_list[$key]['type']; 
			$notedata[$value['to']]['id'] = $note_list[$key]['id']; 

		}

		$accesslist = DB::table('accesses')->pluck('access_name','id');

		$users = $Users
			->where('user_type','<>','admin')
			->with('pass')
			->paginate(25);
		
		foreach ($users as $key => $value) {

			$access_json = json_decode($value['access']); 

			if(!empty($access_json)){
				foreach ($access_json as $ctr => $access) {

					$users[$key]['accesslist'] .= $access . ',';
	
				}
			}

		}

		return view('gem.users.index',compact('users','helper','accesslist','userId','notedata','user_type'));

	}

	public function pending_users(){

		$addedusers = new \App\Http\Models\gem\AddedUser;
		$added_users = $addedusers->pluck('user_id');

		$stage = new \App\Http\Models\gem\MagnetiteTrainingStage;
		$pending = $stage
			->wherein('stage',['Passed'])
			->whereNotIn('trainee_id',$added_users)
			->with('trainee')
			->with('wave')
			->get();

		$pending_non = new \App\Http\Models\gem\SunstonePendingNonAgent;
		$pending_non_agent = $pending_non
			->whereNotIn('applicant_id',$added_users)
			->with('trainee')
			->get();

		$camp = new \App\Http\Models\gem\GarnetCampaign;
		$campaign = $camp->pluck('title','id');

		$position = new \App\Http\Models\gem\SunstonePosition;
		$positions = $position->pluck('name','id');
		$dept = $position->pluck('dept','id');

		return view('gem.users.pending',compact('pending','campaign','positions','pending_non_agent','dept'));

	}

	public function agents(){

		$agent = new \App\Http\Models\gem\GarnetAgents;
		$agents = $agent
			->with('teamlead')
			->with('supervisor')
			->orderBy('id','desc')
			->paginate(25);

		$tl = new \App\Http\Models\gem\GarnetTeamLeads;
		$tl_list = $tl->select('id','first_name','last_name')->get();

		$ps = new \App\Http\Models\gem\GarnetProjectSupervisors;
		$ps_list = $ps->select('id','first_name','last_name')->get();

		return view('gem.users.agent',compact('agents','tl_list','ps_list'));

	}

	public function register(Request $r){

		if(!empty($r->input())){

			$Users = new \App\Http\Models\gem\User;
			$AddedUsers = new \App\Http\Models\gem\AddedUser;

			$pending_users['auditor'] = $r->input('auditor');
			$pending_users['auditor'] = 0;
			$pending_users['is_representative'] = 1;
			$pending_users['password'] = bcrypt($r->input('password'));  
			$pending_users['user_level'] = $r->input('user_level');
			$pending_users['options'] = $r->input('options');
			$pending_users['user_type'] = $r->input('user_type');
			$pending_users['status'] = $r->input('status');
			$pending_users['active'] = $r->input('active');
			$pending_users['name'] = $r->input('name');
			$pending_users['email'] = $r->input('email');


			if($r->input('dept') == 'noc'){

				$pending_users['access'] = '["opal","ruby","pearl","diamond","central","sunstone","magnetite"]';
				$pending_users['is_representative'] = 0;

			}

			if($r->input('dept') == 'qa'){

				$pending_users['access'] = '["ruby"]';
				$pending_users['auditor'] = 1;
				$pending_users['is_representative'] = 0;

			}

			if($r->input('dept') == 'hr'){

				$pending_users['access'] = '["opal"]';
				$pending_users['is_representative'] = 0;
				

			}

			if($r->input('dept') == 'rec'){

				$pending_users['access'] = '["sunstone"]';
				$pending_users['is_representative'] = 0;

			}

			if($r->input('dept') == 'tr'){

				$pending_users['access'] = '["magnetite"]';
				$pending_users['is_representative'] = 0;

			}

			if($r->input('dept') == 'ra'){

				$pending_users['is_representative'] = 0;

			}

			$Users->create($pending_users);

			$added_users['user_id'] = $r->input('id');

			$AddedUsers->create($added_users);

			Alert::message('New User has Been Added!')->persistent('Close');

			return $this->pending_users();
		}
	}

	public function agent_register(Request $r){

		if(!empty($r->input())){

			$agent = new \App\Http\Models\gem\GarnetAgents;
			$Users = new \App\Http\Models\gem\User;
			$AddedUsers = new \App\Http\Models\gem\AddedUser;

			$agentdata['first_name'] = $r->input('first_name');
			$agentdata['middle_name'] = $r->input('middle_name');
			$agentdata['last_name'] = $r->input('last_name');
			$agentdata['tl_id'] = $r->input('tl_id');
			$agentdata['ps_id'] = $r->input('ps_id');

			$agent->create($agentdata);

			Alert::message('Agent has been Added!')->persistent('Close');

			return $this->agents();
		}

	}

	public function agent_edit(Request $r){

		if(!empty($r->input())){

			$agent = new \App\Http\Models\gem\GarnetAgents;
			$Users = new \App\Http\Models\gem\User;
			$AddedUsers = new \App\Http\Models\gem\AddedUser;

			$agentdata['first_name'] = $r->input('first_name');
			$agentdata['middle_name'] = $r->input('middle_name');
			$agentdata['last_name'] = $r->input('last_name');
			$agentdata['tl_id'] = $r->input('tl_id');
			$agentdata['ps_id'] = $r->input('ps_id');

			$agent->where('id','=',$r->input('id'))->update($agentdata);

			Alert::message('Agent has been Updated!')->persistent('Close');

			return $this->agents();
		}

	}

	public function create(Request $r){

		$Users = new \App\Http\Models\gem\User;
		$Access = new \App\Http\Models\gem\Accesslist;
		$helper = new \App\Http\Controllers\gem\Helper;

		$userId = Auth::user()->id;

		$notes = new GarnetDeptNotes;

		$note_list = $notes
			->orderBy('created_at','desc')
			->get();

		foreach ($note_list as $key => $value) {

			$note_list[$key]['content'] = json_decode($value['content']);
			$note_list[$key]['header'] = json_decode($value['header']);
			$note_list[$key]['type'] = json_decode($value['type']);
				
			$notedata[$value['to']]['header'] = $note_list[$key]['header']; 
			$notedata[$value['to']]['content'] = $note_list[$key]['content']; 
			$notedata[$value['to']]['type'] = $note_list[$key]['type']; 
			$notedata[$value['to']]['id'] = $note_list[$key]['id']; 

		}

		$accesslist = DB::table('accesses')->pluck('access_name','id');

		$users = $Users->where('user_type','<>','admin')->paginate(25);
		
		foreach ($users as $key => $value) {

			$access_json = json_decode($value['access']); 

			if(!empty($access_json)){
				foreach ($access_json as $ctr => $access) {

					$users[$key]['accesslist'] .= $access . ',';
	
				}
			}
		}

		return view('gem.users.index',compact('users','helper','accesslist','userId','notedata'));

	}

	public function settings(Request $r,$id){

		$helper = new \App\Http\Controllers\gem\Helper;
		$Users = new \App\Http\Models\gem\User;
		$user = $Users->find($id);
		$user->user_type = $r->user_type;
		$user->ops_function = $r->ops_function;
		$user->is_representative = ($r->ops_function == 'agent'?1:0);
		$user->save();
		return redirect()->route('gemstone.users.index');

	}

	public function connector($id){

		$helper = new \App\Http\Controllers\gem\Helper;
		$Users = new \App\Http\Models\gem\User;
		$user = $Users->find($id);
		$tls = $pss = $agent = false;

		switch ($user->ops_function) {
			case 'agent':
				$GarnetAgents = new \App\Http\Models\gem\GarnetAgents;
				$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
				$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
				$pss = $GarnetProjectSupervisors->get();
				$tls = $GarnetTeamLeads->get();
				$items = $GarnetAgents->get();
				$agent = $GarnetAgents->where('user_id',$id)->first();
				break;
			case 'teamlead':
				$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
				$items = $GarnetTeamLeads->get();
				break;
			case 'supervisor':
				$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
				$items = $GarnetProjectSupervisors->get();
				break;							
			default:
				$items = false;
				break;
		}
		//$helper->pre($user);
		return view('gem.users.connector',compact('user','items','pss','tls','agent'));

	}

	public function connect(Request $r){

		$helper = new \App\Http\Controllers\gem\Helper;
		switch ($r->ops_function){
			case 'agent':
				$GarnetAgents = new \App\Http\Models\gem\GarnetAgents;
				$item = $GarnetAgents->find($r->id);
				$item->tl_id = $r->tl_id;
				$item->ps_id = $r->ps_id;
				break;
			case 'teamlead':
				$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
				$item = $GarnetTeamLeads->find($r->id);
				break;
			case 'supervisor':
				$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
				$item = $GarnetProjectSupervisors->find($r->id);
				break;							
			default:
				$item = false;
				break;
		}

		if($item):
			$item->user_id = $r->user_id;
			$item->save();
		endif;		
		
		return redirect()->back();

	}

	public function search(Request $r){


		$Users = new \App\Http\Models\gem\User;
		$Access = new \App\Http\Models\gem\Accesslist;
		$helper = new \App\Http\Controllers\gem\Helper;

		$userId = Auth::user()->id;

		$user_type = Auth::user()->user_type;

		$notes = new GarnetDeptNotes;

		$note_list = $notes
			->orderBy('created_at','desc')
			->get();

		foreach ($note_list as $key => $value) {

			$note_list[$key]['content'] = json_decode($value['content']);
			$note_list[$key]['header'] = json_decode($value['header']);
			$note_list[$key]['type'] = json_decode($value['type']);
				
			$notedata[$value['to']]['header'] = $note_list[$key]['header']; 
			$notedata[$value['to']]['content'] = $note_list[$key]['content']; 
			$notedata[$value['to']]['type'] = $note_list[$key]['type']; 
			$notedata[$value['to']]['id'] = $note_list[$key]['id']; 

		}

		$accesslist = DB::table('accesses')->pluck('access_name','id');

		$users = $Users->where('user_type','<>','admin')->SearchByKeyword($r->keyword)->paginate(25);
		
		foreach ($users as $key => $value) {

			$access_json = json_decode($value['access']); 

			if(!empty($access_json)){
				foreach ($access_json as $ctr => $access) {

					$users[$key]['accesslist'] .= $access . ',';
	
				}
			}

		}

		return view('gem.users.index',compact('users','helper','accesslist','userId','notedata','user_type'));

	}


}