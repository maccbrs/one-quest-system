<?php namespace App\Http\Controllers\gem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class dashboardController extends Controller
{

	public function index(){

		$GarnetAnnouncements = new \App\Http\Models\amber\GarnetAnnouncements;
		$GarnetMessages = new \App\Http\Models\amber\GarnetMessages;
		$help = new \App\Http\Controllers\amber\Helper;
		$GarnetGroups = new \App\Http\Models\amber\GarnetGroups;

		$group_list = array();

		$user_id = Auth::user()->id;
		$groups = $GarnetGroups->get();

		foreach ($groups as $key => $value) {
			$member = json_decode($value['members']);

			if(in_array($user_id, $member)){
				$group_list[] = $value['id'];
			}
		}

		$message = $GarnetMessages->whereIn('to',[$user_id])->whereNotIn('page',['group']);

		$message2 = $GarnetMessages->whereIn('to',$group_list)->whereIn('page',['group']);

		$all_message = $message2
            ->union($message)
            ->get();

		$items = $GarnetAnnouncements->whereIn('destination',['all',$help->user_type()])->where('status',1)->get();

		$statuses = $help->opened($items); 
		
		$statuses_message = $help->opened_message($all_message); 

		$help = new \App\Http\Controllers\gem\Helper;
		$disputes = 0;

		$type = $help->userType();

		if($type != 'tl' || $type != 'ra' || $type != 'hr' || $type != 'hr'){
			
			$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;

			switch($type):
				case 'tl':
					$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
					$item = $GarnetTeamLeads->self()->with(['agents'])->first();

					$ids = [];

					if($item && $item->agents->count()):
						foreach($item->agents as $i):
							if($i->user_id):
								$ids[] = $i->user_id;
							endif;
						endforeach;
					endif;

					$disputes = $OnyxDisputes->whereIn('user',$ids)->where('tl_response',1)->where('status',1)->where('ra_response',0)->with(['period','issueObj','userObj'])->count();
					$disputesnonagent = $OnyxDisputes->where('user_type',2)->where('sup_response',1)->where('status',1)->whereIn('user',$ids)->with(['period','issueObj','userObj'])->orderBy('updated_at','desc')->count();
					$disputes2 = $OnyxDisputes->whereIn('user',$ids)->with(['period','issueObj','userObj'])->orderBy('created_at','desc')->first();
				
					
				break;



				case 'ra':
				$disputes = $OnyxDisputes->ra()->where('status',1)->count(); 
				$disputes2 = $OnyxDisputes->ra()->orderBy('created_at','desc')->first();

				$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
				$GemApprover = new \App\Http\Models\gem\GemApprover;
				$items = $GemApprover->select('level1')->self()->get();
				$ids = [];

				if($items->count()):
					foreach($items as $i):
					$ids[] = $i->level1;
					endforeach;
				endif;

				$disputesnonagent = $OnyxDisputes->where('user_type',2)->where('sup_response',1)->where('status',1)->whereIn('user',$ids)->with(['period','issueObj','userObj'])->orderBy('updated_at','desc')->count();

				break;



				case 'hr':
				$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;

				$disputes = $OnyxDisputes->hr2()->agents()->count(); 
				$disputes2 = $OnyxDisputes->hr2()->agents()->orderBy('created_at','desc')->first();

				$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
				$GemApprover = new \App\Http\Models\gem\GemApprover;
				$items = $GemApprover->select('level1')->self()->get();
				$ids = [];

				if($items->count()):
					foreach($items as $i):
					$ids[] = $i->level1;
					endforeach;
				endif;

				$disputesnonagent = $OnyxDisputes->where('user_type',2)->whereIn('sup_response',[2])->whereIn('hr_response',[1])->with(['userObj','periodObj','issueObj'])->count();

				break;


				case 'user':
				$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
				$GemApprover = new \App\Http\Models\gem\GemApprover;
				$items = $GemApprover->select('level1')->self()->get();
				$ids = [];

				if($items->count()):
					foreach($items as $i):
					$ids[] = $i->level1;
					endforeach;
				endif;

				$disputesnonagent = $OnyxDisputes->where('user_type',2)->where('sup_response',1)->where('status',1)->whereIn('user',$ids)->with(['period','issueObj','userObj'])->orderBy('updated_at','desc')->count();

				$disputes2 = $OnyxDisputes->where('user_type',2)->where('status',1)->whereIn('user',$ids)->with(['period','issueObj','userObj'])->orderBy('created_at','desc')->first();

				break;

				case 'program_supervisor':
				$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
				$GemApprover = new \App\Http\Models\gem\GemApprover;
				$items = $GemApprover->select('level1')->self()->get();
				$ids = [];

				if($items->count()):
					foreach($items as $i):
					$ids[] = $i->level1;
					endforeach;
				endif;

				$disputesnonagent = $OnyxDisputes->where('user_type',2)->where('sup_response',1)->where('status',1)->whereIn('user',$ids)->with(['period','issueObj','userObj'])->orderBy('updated_at','desc')->count();

				$disputes2 = $OnyxDisputes->where('user_type',2)->where('status',1)->whereIn('user',$ids)->with(['period','issueObj','userObj'])->orderBy('created_at','desc')->first();

			endswitch; 

		}

		$created_message_agent = $GarnetMessages->where('to','=',$user_id)->where('status',1)->where('page','=','agents')->orwhere('page','=','team_leader')->first();

		$created_message_user = $GarnetMessages->where('to','=',$user_id)->where('status',1)->first();

		$created_message_hr = $GarnetMessages->where('to','=',$user_id)->first();

		$created_noc = $GarnetAnnouncements->whereIn('destination',['all',$help->user_type()])->where('status',1)->where('source','=','noc')->orderBy('created_at','desc')->first();

		$created_hrd = $GarnetAnnouncements->whereIn('destination',['all',$help->user_type()])->where('status',1)->where('source','=','hrd')->orderBy('created_at','desc')->first();

		$created_qa = $GarnetAnnouncements->whereIn('destination',['all',$help->user_type()])->where('status',1)->where('source','=','qa')->orderBy('created_at','desc')->first();

		$latest_announcement = $GarnetAnnouncements->where('destination','=','all')->orderBy('created_at','desc')->first();


		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
				$GemApprover = new \App\Http\Models\gem\GemApprover;
				$items = $GemApprover->select('level1')->self()->get();
				$ids = [];

				if($items->count()):
					foreach($items as $i):
					$ids[] = $i->level1;
					endforeach;
				endif;

		$disputesuser = $OnyxDisputes->where('user_type',2)->where('sup_response',1)->where('status',1)->whereIn('user',$ids)->with(['period','issueObj','userObj'])->orderBy('updated_at','desc')->count();

		/*$help->pre($disputes);*/
		return view('gem.dashboard.index',compact('created_messagehr','created_message','created_noc','created_hrd','created_qa','latest_announcement','items','statuses','help','statuses_message','disputes','disputes2','item2','type','disputesnonagent','disputesuser'));


	}

}