<?php namespace App\Http\Controllers\gem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Alert;

class requestController extends Controller
{

	public function disputes(){

		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
		$OnyxDisputesDates = new \App\Http\Models\gem\OnyxDisputesDates;
		$OnyxDisputesIssues = new \App\Http\Models\gem\OnyxDisputesIssues;
		$help = new \App\Http\Controllers\gem\Helper;
		$items = $OnyxDisputes->self()->orderBy('created_at')->where('status','=',1)->get();
		$dates = $help->onyx_dates($OnyxDisputesDates->get());

		$gemissue = $OnyxDisputesIssues->where('status','=',1)->pluck('issue_name','id');

		$issues = $help->onyx_issues($OnyxDisputesIssues->get());

		//$help->pre($gemissue);
		//$help->pre($items);
		return view('gem.request.disputes',compact('items','dates','issues','help','date_pluck','issues_pluck','gemissue'));
		

	} 

	public function dispute_create(Request $r){

		$help = new \App\Http\Controllers\gem\Helper;
		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;

		$data = $r->all();
		$data['user'] = Auth::user()->id;
		$data['user_type'] = 2;
		$data['sup_response'] = 1;
		$data['dispute_date'] = date("Y-m-d", strtotime($data['dispute_date']));
		// $data['dispute_time'] = date("H:i", strtotime($data['dispute_time']));
		$OnyxDisputes->create($data);	

		return redirect()->back();

	}	

	public function dispute_delete(Request $r){

		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
		
		$data['status'] = 0;

		$dispute_data = $OnyxDisputes->where('id','=',$r->input('id'))->update($data);	

        Alert::message('Dispute File has been moved to Trash')->persistent('Close');

		return redirect()->back();

	}	

	public function dispute_update(Request $r){

		$help = new \App\Http\Controllers\gem\Helper;
		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;

		if($r->input('sup_response') == 1){

			$date = $r->all();

			$data['dispute_date'] = date("Y-m-d", strtotime($date['dispute_date']));
			$data['issue'] =  $r->input('issue_name');
			$data['dispute_period'] =  $r->input('dispute_period');
			$data['notes'] = $r->input('notes');
			$OnyxDisputes->where('id', $r->input('id'))->update($data);	

			//$help->pre($data);

			Alert::message('Your Dispute File has been Updated.')->persistent('Close');
		}

		return redirect()->back();

	}	

} 