<?php namespace App\Http\Controllers\gem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class garnetController extends Controller
{

 	public function agents(){

 		$help = new \App\Http\Controllers\gem\Helper;
		$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
		$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
		$Users = new \App\Http\Models\gem\User;
		$supervisors = $GarnetProjectSupervisors->get();
		$teamleads = $GarnetTeamLeads->get();
		$GarnetAgents = new \App\Http\Models\gem\GarnetAgents; 
		$users = $Users->get();
		$agents = $GarnetAgents->with('gem','teamlead','supervisor')->orderBy('createdAt','desc')->paginate(20);
		//$help->pre($users);		
 		return view('gem.garnet.agents',compact('supervisors','teamleads','users','agents'));

 	}

 	public function add_agent(Request $r){

		$this->validate($r, [
		    'first_name' => 'required',
		    'last_name' => 'required',
		    'user_id' => 'required',
		    'ps_id' => 'required',
		    'tl_id' => 'required'
		]);
		$help = new \App\Http\Controllers\gem\Helper;
		$GarnetAgents = new \App\Http\Models\gem\GarnetAgents;
		$GarnetAgents->create($r->all());
		return redirect()->route('gem.garnet.agents');

 	}

 	public function edit_agent(Request $r,$id){
		$this->validate($r, [
		    'first_name' => 'required',
		    'last_name' => 'required',
		    'user_id' => 'required',
		    'ps_id' => 'required',
		    'tl_id' => 'required'
		]);

		$help = new \App\Http\Controllers\gem\Helper;
		//$help->pre($r->all());
		$GarnetAgents = new \App\Http\Models\gem\GarnetAgents;
		$item = $GarnetAgents->find($id);
		$item->update($r->all());
		return redirect()->route('gem.garnet.agents');	

 	}

 	public function teamleads(){

 		$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
 		$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
 		$supervisors = $GarnetProjectSupervisors->get();
 		$Users = new \App\Http\Models\gem\User;
 		$help = new \App\Http\Controllers\gem\Helper;
 		$users = $Users->get();
 		$teamleads = $GarnetTeamLeads->orderBy('createdAt','desc')->with(['gem'])->paginate(20);
 		//$help->pre($teamleads);
 		return view('gem.garnet.teamleads',compact('users','teamleads','supervisors'));

 	}

 	public function add_teamlead(Request $r){
		$this->validate($r, [
		    'first_name' => 'required',
		    'last_name' => 'required',
		    'user_id' => 'required'
		]);

		$help = new \App\Http\Controllers\gem\Helper;
		//$help->pre($r->all());
		$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
		$GarnetTeamLeads->create($r->all());
		return redirect()->route('gem.garnet.teamleads');	

 	}



 	public function supervisors(){

 		$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
 		$supervisors = $GarnetProjectSupervisors->get();
		$Users = new \App\Http\Models\gem\User;
		$users = $Users->get(); 		
 		return view('gem.garnet.supervisors',compact('supervisors','users'));

 	}

 	public function add_supervisors(Request $r){

 		$help = new \App\Http\Controllers\gem\Helper;
		$this->validate($r, [
		    'first_name' => 'required',
		    'last_name' => 'required',
		    'user_id' => 'required'
		]);
		//$help->pre($r->all());
		$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
		$GarnetProjectSupervisors->create($r->all());
		return redirect()->route('gem.garnet.supervisors');

 	}

 	public function edit_teamlead(Request $r,$id){

		$this->validate($r, [
		    'first_name' => 'required',
		    'last_name' => 'required',
		    'user_id' => 'required',
		    'ps_id' => 'required'
		]);

		$help = new \App\Http\Controllers\gem\Helper;
		//$help->pre($r->all());
		$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
		$item = $GarnetTeamLeads->find($id);
		$item->update($r->all());
		return redirect()->route('gem.garnet.teamleads');	

 	}

}