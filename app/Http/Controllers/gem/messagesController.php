<?php namespace App\Http\Controllers\gem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class messagesController extends Controller
{

	public function listz($source,$status){

		$GarnetMessageOpened = new \App\Http\Models\gem\GarnetMessageOpened;
		$GarnetMessages = new \App\Http\Models\gem\GarnetMessages;
		$GarnetGroups = new \App\Http\Models\gem\GarnetGroups;
		$help = new \App\Http\Controllers\gem\Helper;

		$user_id = Auth::user()->id;
		$items = $GarnetMessageOpened->self()->get();

		$group_list = array();


		$groups = $GarnetGroups->get();

		foreach ($groups as $key => $value) {

			$member = json_decode($value['members']);

			if(in_array($user_id, $member)){
				$group_list[] = $value['id'];
			}
			
		}

		$ids = [];

		if($items->count()):
			foreach($items as $k => $v):
				$ids[] = $v->message_id;
			endforeach;				
		endif;

		switch ($status):

			case 'read':

				$message = $GarnetMessages->whereIn('to',[$user_id])->whereNotIn('page',['group'])
					->whereIn('id',$ids);

				$message2 = $GarnetMessages->whereIn('to',$group_list)->whereIn('page',['group'])
					->whereIn('id',$ids);

				$messages = $message2
		            ->union($message)
		            ->orderby('id','desc')
		            ->get();

				break;

			case 'unread':

				$message = $GarnetMessages->whereIn('to',[$user_id])->whereNotIn('page',['group'])->whereNotIn('id',$ids);

				$message2 = $GarnetMessages->whereIn('to',$group_list)->whereIn('page',['group'])->whereNotIn('id',$ids);

				$messages = $message2
		            ->union($message)
		            ->orderby('id','desc')
		            ->get();

				break;	

			default:
				$message = $GarnetMessages->whereIn('to',[$user_id])->whereNotIn('page',['group']);

				$message2 = $GarnetMessages->whereIn('to',$group_list)->whereIn('page',['group']);

				$messages = $message2
		            ->union($message)
		            ->orderby('id','desc')
		            ->get();
		            
				break;

		endswitch;

		foreach ($messages as $key => $value) {

			$excerpt = json_decode($value->content)->content;
			
			$messages[$key]['excerpt'] = substr($excerpt,0,10) ; 

		}

		return view('gem.messages.lists',compact('messages','help'));
	}

	public function lizt($id){

		$GarnetMessages = new \App\Http\Models\gem\GarnetMessages;
		$GarnetOpened = new \App\Http\Models\gem\GarnetMessageOpened;
		$Users = new \App\Http\Models\gem\User;

		$userId = Auth::user()->id;
		$item = $GarnetMessages->find($id);
		$recipient = $item['user'];
		$to = $item['to'];

		$users_list = $Users
			->pluck('name','id');

		$user_involved = $Users
			->find($recipient);

		$messages_list  = $GarnetMessages->get_conversation($recipient,$userId);

		$GarnetOpened->firstOrCreate(['user_id' => Auth::user()->id,'message_id' => $id]);

		return view('gem.messages.list',compact('item','messages_list','user_involved','userId','users_list'));

	}

	public function grouplist($id){

		$GarnetMessages = new \App\Http\Models\gem\GarnetMessages;
		$GarnetOpened = new \App\Http\Models\gem\GarnetMessageOpened;
		$Groups = new \App\Http\Models\gem\GarnetGroups;
		$Users = new \App\Http\Models\gem\User;

		$userId = Auth::user()->id;
		$item = $GarnetMessages->find($id);
		$recipient = $item['user'];
		$to = $item['to'];

		$group_list = $Groups
			->pluck('name','id');

		$users_list = $Users
			->pluck('name','id');

		$user_involved = $Groups
			->find($to);

		$messages_list  = $GarnetMessages->get_group_conversation($to,$userId);

		$GarnetOpened->firstOrCreate(['user_id' => Auth::user()->id,'message_id' => $id]);

		return view('gem.messages.grouplist',compact('item','messages_list','user_involved','userId','users_list','group_list'));

	}

}