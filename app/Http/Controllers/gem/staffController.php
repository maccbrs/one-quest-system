<?php namespace App\Http\Controllers\gem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class staffController extends Controller
{

	public function index(){

		$Users = new \App\Http\Models\gem\User;
		$helper = new \App\Http\Controllers\gem\Helper;
		$users = $Users->where('user_type','admin')->paginate(25);
		return view('gem.staff.index',compact('users','helper'));

	}

}