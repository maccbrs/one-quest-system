<?php namespace App\Http\Controllers\gem;
use Auth;

class accessController
{

	public function access($app){

		$access = [];
		if(isset(Auth::user()->id)):
	        if(Auth::user()->user_type == 'admin'):
	            $access = ['dahlia','rosebud','primrose','lily','pearl','ruby','opal','central','diamond','sunstone'];
	        else:
	            $access = (Auth::user()->access != ''?json_decode(Auth::user()->access,true):[]);
	        endif;
        endif;

        if($access):
        	if(in_array($app, $access)) return true;
        endif;

        return false; 
	}

	public function routes(){
		$routes = [];

		if(isset(Auth::user()->id)):
			$a = (Auth::user()->routes != ''?json_decode(Auth::user()->routes,true):[]);
			foreach ($a as $val):
				if($val):
					foreach ($val as $v):
						$routes[] = $v;
					endforeach;
				endif;
			endforeach;
		endif;
		return $routes;
	}
 
	public function allowed($route){ 

		$routes = $this->routes(); //return $routes;
		if(in_array($route,$routes) || Auth::user()->user_type == 'admin'):
			return true;
		endif;
			return false;
			
	}

	public function btnRoute($route,$id = 0){

		if($this->allowed($route)):
			if($id): 
				return route($route,$id); 
			else: 
				return route($route); 
			endif;
		else:
			//return '#';
		endif;
	}

	public function btnDisabler($route){
		
		if(!$this->allowed($route)):
			return 'mb-btn-disabled';
		endif;
			return '';
	}


}