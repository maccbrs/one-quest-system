<?php namespace App\Http\Controllers\gem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class qandaController extends Controller
{

	public function index(){


	} 

	public function add(){

		return view('gem.qanda.add');

	} 

	public function dept($id){

		$help = new \App\Http\Controllers\gem\Helper;
		$GarnetQanda = new \App\Http\Models\gem\GarnetQanda;	
		$items = $GarnetQanda->where('dept_to',$id)->where('userid_from',Auth::user()->id)->with(['user'])->paginate(20);	
		return view('gem.qanda.dept',compact('id','items'));

	}

	public function create(Request $r){

	    $this->validate($r, [
	        'inquiry' => 'required'
	    ]);

		$help = new \App\Http\Controllers\gem\Helper;
		$GarnetQanda = new \App\Http\Models\gem\GarnetQanda;
		$data = $r->all();
		$data['userid_from'] = Auth::user()->id;
		$GarnetQanda->create($data);
		return redirect()->back();

	}

	public function agentlist(){
		$help = new \App\Http\Controllers\gem\Helper;
		$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
		$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
		$Users = new \App\Http\Models\gem\User;
		$supervisors = $GarnetProjectSupervisors->get();
		$teamleads = $GarnetTeamLeads->get();
		$GarnetAgents = new \App\Http\Models\gem\GarnetAgents; 
		$users = $Users->get();

		$id = Auth::user()->id;
		$id2 = $GarnetTeamLeads->select('id')->where('user_id','=', $id)->get();
		$tlid = $id2[0]['id'];

		$agents = $GarnetAgents->with('gem','teamlead','supervisor')->where('tl_id','=',$tlid)->orderBy('createdAt','desc')->get();

		//$help->pre($agents);
		
				
		return view('gem.qanda.agentlist',compact('gem','supervisors','teamleads','users','agents'));
	}

	public function nonagentlist(){
		$GemUsers = new \App\Http\Models\amethyst\GemUsers;
		$GemApprover = new \App\Http\Models\amethyst\GemApprover;
		$help = new \App\Http\Controllers\amethyst\Helper;
		
		$id = Auth::user()->id;

		$items = $GemUsers
			->select('name','id')
			->where('active','=','1')
			->orderBy('name', 'asc')
			->get();

			//$help->pre($id);

		$approvers = $GemApprover->where('level2','=',$id)->with(['emp','sup'])->get();

		return view('gem.qanda.nonagentlist',compact('help','items','approvers'));	
	}

} 