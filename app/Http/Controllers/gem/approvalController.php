<?php namespace App\Http\Controllers\gem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class approvalController extends Controller
{

	public function disputes(){

		$help = new \App\Http\Controllers\gem\Helper;
		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
		$Users = new \App\Http\Models\gem\User;
		$usersObj = $Users->select('emp_code','name','id')->get();
		$users = [];
		$disputes = false;
		$disputes2 = false;

		foreach($usersObj as $u):
			$users[$u->id] = $u->name;
		endforeach;

		$type = $help->userType();

		switch($type):
			case 'tl':
				$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
				$item = $GarnetTeamLeads->self()->with(['agents'])->first();

				$ids = [];

				if($item && $item->agents->count()):
					foreach($item->agents as $i):
						if($i->user_id):
							$ids[] = $i->user_id;
						endif;
					endforeach;
				endif;

				$disputes = $OnyxDisputes->whereIn('user',$ids)->where('tl_response',1)->where('status',1)->with(['period','issueObj','userObj'])->get();
				$disputes2 = $OnyxDisputes->whereIn('user',$ids)->whereIn('tl_response',[3,2])->where('ra_response',1)->where('posted',0)->where('status',1)->with(['period','issueObj','userObj'])->get();

			break;

			case 'program_supervisor':
				$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
				$item = $GarnetProjectSupervisors->self()->with(['agents'])->first();

				$ids = [];

				if($item && $item->agents->count()):
					foreach($item->agents as $i):
						if($i->user_id):
							$ids[] = $i->user_id;
						endif;
					endforeach;
				endif;

				$disputes = $OnyxDisputes->whereIn('user',$ids)->where('tl_response',1)->where('status',1)->with(['period','issueObj','userObj'])->get();
				$disputes2 = $OnyxDisputes->whereIn('user',$ids)->whereIn('tl_response',[3,2])->where('ra_response',1)->where('posted',0)->where('status',1)->with(['period','issueObj','userObj'])->get();

			break;

			case 'ra':
			    $disputes = $OnyxDisputes->ra()->where('status',1)->with(['period','issueObj','userObj'])->get(); 
				$disputes2 = $OnyxDisputes->whereIn('ra_response',[3,2])->where('posted',0)->where('status',1)->where('hr_response',1)->where('user_type',1)->with(['period','issueObj','userObj'])->get(); 
				break;
			case 'hr':
			    $disputes = $OnyxDisputes->hr2()->where('status',1)->agents()->with(['period','issueObj','userObj'])->orderBy('updated_at','desc')->get(); 
				$disputes2 = $OnyxDisputes->whereIn('hr_response',[3,2])->where('status',1)->where('posted',0)->agents()->with(['period','issueObj','userObj'])->orderBy('updated_at','desc')->get(); 
				break;		

		endswitch; 

		$tl_details = null;
		$ra_details = null;
		$hr_details = null;
		$sup_details = null;

		if($disputes != null){
			foreach ($disputes as $key => $value) {

				if($value['tl_comments'] != null){
					$tl_details = json_decode($value['tl_comments']);
				}
				if($value['ra_comments'] != null){
					$tl_details = json_decode($value['ra_comments']);
				}
				if($value['hr_comments'] != null){
					$tl_details = json_decode($value['hr_comments']);
				}
				if($value['sup_comments'] != null){
					$tl_details = json_decode($value['sup_comments']);
				}

				$disputes[$key]['tl_details'] = $tl_details;
				$disputes[$key]['ra_details'] = $ra_details;
				$disputes[$key]['hr_details'] = $hr_details;
				$disputes[$key]['sup_details'] = $sup_details;

			}
		}


		$tl_details2 = null;
		$ra_details2 = null;
		$hr_details2 = null;
		$sup_details2 = null;

		if($disputes2 != null){
			foreach ($disputes2 as $key2 => $value2) {

				if($value2['tl_comments'] != null){
					$tl_details2 = json_decode($value2['tl_comments']);
				}
				if($value2['ra_comments'] != null){
					$tl_details2 = json_decode($value2['ra_comments']);
				}
				if($value2['hr_comments'] != null){
					$tl_details2 = json_decode($value2['hr_comments']);
				}
				if($value2['sup_comments'] != null){
					$tl_details2 = json_decode($value2['sup_comments']);
				}

				$disputes2[$key2]['tl_details'] = $tl_details2;
				$disputes2[$key2]['ra_details'] = $ra_details2;
				$disputes2[$key2]['hr_details'] = $hr_details2;
				$disputes2[$key2]['sup_details'] = $sup_details2;

			}
		}

		//$help->pre($disputes);
		return view('gem.approval.disputes',compact('disputes','disputes2','help','type','users')); 

	}  

	public function disputes2(){

		$help = new \App\Http\Controllers\gem\Helper;
		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
		$GemApprover = new \App\Http\Models\gem\GemApprover;
		$items = $GemApprover->select('level1')->self()->get();
		$ids = [];

		if($items->count()):
			foreach($items as $i):
			$ids[] = $i->level1;
			endforeach;
		endif;

		$disputes = $OnyxDisputes->where('user_type',2)->where('sup_response',1)->where('status',1)->whereIn('user',$ids)->with(['period','issueObj','userObj'])->orderBy('updated_at','desc')->get();
		
		$tl_details = null;
		$ra_details = null;
		$hr_details = null;
		$sup_details = null;

		foreach ($disputes as $key => $value) {

			if($value['tl_comments'] != null){
				$tl_details = json_decode($value['tl_comments']);
			}
			if($value['ra_comments'] != null){
				$tl_details = json_decode($value['ra_comments']);
			}
			if($value['hr_comments'] != null){
				$tl_details = json_decode($value['hr_comments']);
			}
			if($value['sup_comments'] != null){
				$tl_details = json_decode($value['sup_comments']);
			}

			$disputes[$key]['tl_details'] = $tl_details;
			$disputes[$key]['ra_details'] = $ra_details;
			$disputes[$key]['hr_details'] = $hr_details;
			$disputes[$key]['sup_details'] = $sup_details;

		}

		$disputes2 = $OnyxDisputes
			->where('user_type',2)
			->where('sup_response','<>',1)
			->whereIn('hr_response',[0,1])
			->where('status',1)
			->whereIn('user',$ids)
			->with(['period','issueObj','userObj'])
			->orderBy('updated_at','desc')
			->get();

		$tl_details2 = null;
		$ra_details2 = null;
		$hr_details2 = null;
		$sup_details2 = null;

		foreach ($disputes2 as $key2 => $value2) {

			if($value2['tl_comments'] != null){
				$tl_details2 = json_decode($value2['tl_comments']);
			}
			if($value2['ra_comments'] != null){
				$tl_details2 = json_decode($value2['ra_comments']);
			}
			if($value2['hr_comments'] != null){
				$tl_details2 = json_decode($value2['hr_comments']);
			}
			if($value2['sup_comments'] != null){
				$tl_details2 = json_decode($value2['sup_comments']);
			}

			$disputes2[$key2]['tl_details'] = $tl_details2;
			$disputes2[$key2]['ra_details'] = $ra_details2;
			$disputes2[$key2]['hr_details'] = $hr_details2;
			$disputes2[$key2]['sup_details'] = $sup_details2;

		}
		//$help->pre($disputes);
		return view('gem.approval.disputes2',compact('disputes','disputes2','help'));
		
	}

	public function backup_approver(){

		$help = new \App\Http\Controllers\gem\Helper;
		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
		$Users = new \App\Http\Models\gem\User;
		$usersObj = $Users->select('name','id')->get();
		$users = [];
		$disputes = false;
		$disputes2 = false;

		foreach($usersObj as $u):
			$users[$u->id] = $u->name;
		endforeach;

		$type = $help->userType();

		switch($type):

			case 'program_supervisor':
				$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
				$item = $GarnetProjectSupervisors->self()->with(['agents'])->first();

				$ids = [];

				if($item && $item->agents->count()):
					foreach($item->agents as $i):
						if($i->user_id):
							$ids[] = $i->user_id;
						endif;
					endforeach;
				endif;

				$disputes = $OnyxDisputes->whereIn('user',$ids)->where('tl_response',1)->where('status',1)->with(['period','issueObj','userObj'])->get();
			//	$disputes2 = $OnyxDisputes->whereIn('user',$ids)->whereIn('tl_response',[3,2])->where('ra_response',1)->where('posted',0)->where('status',1)->with(['period','issueObj','userObj'])->get();

			break;

		endswitch; 

		$tl_details = null;
		$ra_details = null;
		$hr_details = null;
		$sup_details = null;

		if($disputes != null){
			foreach ($disputes as $key => $value) {

				if($value['tl_comments'] != null){
					$tl_details = json_decode($value['tl_comments']);
				}
				if($value['ra_comments'] != null){
					$tl_details = json_decode($value['ra_comments']);
				}
				if($value['hr_comments'] != null){
					$tl_details = json_decode($value['hr_comments']);
				}
				if($value['sup_comments'] != null){
					$tl_details = json_decode($value['sup_comments']);
				}

				$disputes[$key]['tl_details'] = $tl_details;
				$disputes[$key]['ra_details'] = $ra_details;
				$disputes[$key]['hr_details'] = $hr_details;
				$disputes[$key]['sup_details'] = $sup_details;

			}
		}

		$GemApprover = new \App\Http\Models\gem\GemApprover;
		$items2 = $GemApprover->select('level1')->self2()->get();

		$ids2 = [];

		if($items2->count()):
			foreach($items2 as $i):
			$ids2[] = $i->level1;
			endforeach;
		endif;

		$disputes2 = $OnyxDisputes
			->where('user_type',2)
			->where('status',1)
			->whereIn('user',$ids2)
			->where('sup_response',1)
			->with(['period','issueObj','userObj'])
			->get();
		
		//print_r('<pre>');print_r($disputes2);print_r('</pre>'); exit;

		$tl_details2 = null;
		$ra_details2 = null;
		$hr_details2 = null;
		$sup_details2 = null;

		foreach ($disputes2 as $key2 => $value2) {

			if($value2['tl_comments'] != null){
				$tl_details2 = json_decode($value2['tl_comments']);
			}
			if($value2['ra_comments'] != null){
				$tl_details2 = json_decode($value2['ra_comments']);
			}
			if($value2['hr_comments'] != null){
				$tl_details2 = json_decode($value2['hr_comments']);
			}
			if($value2['sup_comments'] != null){
				$tl_details2 = json_decode($value2['sup_comments']);
			}

			$disputes2[$key2]['tl_details'] = $tl_details2;
			$disputes2[$key2]['ra_details'] = $ra_details2;
			$disputes2[$key2]['hr_details'] = $hr_details2;
			$disputes2[$key2]['sup_details'] = $sup_details2;
		}

		//print_r('<pre>');print_r($disputes2);print_r('</pre>'); exit; 

		return view('gem.approval.disputes_backup',compact('disputes','disputes2','help','type','users'));
	}

	public function disputes_update(Request $r){

		$help = new \App\Http\Controllers\gem\Helper;
		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
		$dispute =  $OnyxDisputes->find($r->id);

	
		if($dispute): 

			switch($help->userType()):

				case 'tl':
					$dispute->tl_response = $r->status;
					$comments = ($dispute->tl_comments?json_decode($dispute->tl_comments,true):[]);
					$comments['approver'] = Auth::user()->id;
					$comments['notes'] = $r->notes;
					$comments['date'] = $help->mbdate();
					$dispute->tl_comments = json_encode($comments);
					$dispute->ra_response = ($r->status = 2?1:0);
					$dispute->save();
				break;
				case 'program_supervisor':
					$dispute->tl_response = $r->status;
					$comments = ($dispute->tl_comments?json_decode($dispute->tl_comments,true):[]);
					$comments['approver'] = Auth::user()->id;
					$comments['notes'] = $r->notes;
					$comments['date'] = $help->mbdate();
					$dispute->tl_comments = json_encode($comments);
					$dispute->ra_response = ($r->status = 2?1:0);
					$dispute->save();
				break;
				case 'ra':
					$dispute->ra_response = $r->status;
					$comments = ($dispute->ra_comments?json_decode($dispute->ra_comments,true):[]);
					$comments['approver'] = Auth::user()->id;
					$comments['notes'] = $r->notes;
					$comments['date'] = $help->mbdate();
					$dispute->ra_comments = json_encode($comments);
					$dispute->hr_response = ($r->status = 2?1:0);
					$dispute->save();
				break;
				case 'hr':
					$dispute->hr_response = $r->status;
					$comments = ($dispute->ra_comments?json_decode($dispute->ra_comments,true):[]);
					$comments['approver'] = Auth::user()->id;
					$comments['notes'] = $r->notes;
					$comments['date'] = $help->mbdate();
					$comments['hr_response'] = $r->status;
					$dispute->hr_comments = json_encode($comments);
					$dispute->save();
				break;
			endswitch;

		endif;

		return redirect()->back();
		
	}

	public function disputes_update2(Request $r){ 

		$help = new \App\Http\Controllers\gem\Helper;
		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
		$dispute =  $OnyxDisputes->find($r->id);

		if($dispute):
			$dispute->sup_response = $r->status;
			if($r->status == 2):
				$dispute->hr_response = 1;
			endif;
			$comments = ($dispute->sup_comments?json_decode($dispute->sup_comments,true):[]);
			$comments['approver'] = Auth::user()->id;
			$comments['notes'] = $r->notes;
			$comments['date'] = $help->mbdate();
			$dispute->sup_comments = json_encode($comments);
			$dispute->save();		
		endif;

		return redirect()->back();

	}

	public function disputes_posted(Request $r){ 

		
		$help = new \App\Http\Controllers\gem\Helper;
		$OnyxDisputes = new \App\Http\Models\gem\OnyxDisputes;
		$dispute =  $OnyxDisputes->find($r->id);

		if($dispute):
			
			$data['posted'] = $r->status;
			$dispute =  $OnyxDisputes->where('id',$r->id)->update($data);

		endif;

		return redirect()->back();

	}


}