<?php namespace App\Http\Controllers\gem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class announcementsController extends Controller
{

	public function lists($source,$status){

		$GarnetOpened = new \App\Http\Models\gem\GarnetOpened;
		$GarnetAnnouncements = new \App\Http\Models\gem\GarnetAnnouncements;

		$help = new \App\Http\Controllers\gem\Helper;
		$items = $GarnetOpened->self()->get();
		$ids = [];



		if($items->count()):
			foreach($items as $k => $v):
				$ids[] = $v->announcements_id;
			endforeach;				
		endif;

		switch ($status):
			case 'read':
				$announcements = $GarnetAnnouncements->source($source)->where('status',"<>",0)->whereIn('id',$ids)->whereIn('destination',['all',$help->user_type()])->orderBy('created_at','desc')->get(); 
				break;
			case 'unread':
				$announcements = $GarnetAnnouncements->source($source)->where('status',"<>",0)->whereNotIn('id',$ids)->whereIn('destination',['all',$help->user_type()])->orderBy('created_at','desc')->get();
				break;			
			default:
				$announcements = $GarnetAnnouncements->source($source)->where('status',"<>",0)->whereIn('destination',['all',$help->user_type()])->orderBy('created_at','desc')->get();
				break;
		endswitch;
		// print_r('sad'); exit;
		return view('gem.announcements.lists',compact('announcements','help'));


	}

	public function lizt($id){

		$GarnetAnnouncements = new \App\Http\Models\gem\GarnetAnnouncements;
		$GarnetOpened = new \App\Http\Models\gem\GarnetOpened;
		$help = new \App\Http\Controllers\gem\Helper;

		$item = $GarnetAnnouncements->find($id);
		//$help->pre($item);
		$GarnetOpened->firstOrCreate(['user_id' => Auth::user()->id,'announcements_id' => $id]);

		return view('gem.announcements.list',compact('item'));

	}

	public function update(Request $r){

		$GarnetAnnouncements = new \App\Http\Models\gem\GarnetAnnouncements;

		$data['destination'] = $r->input('destination');
		$data['title'] = $r->input('title');
		$data['excerpt'] = $r->input('excerpt');
		$data['content'] = $r->input('content');

		$GarnetAnnouncements->where('id', $r->input('id'))->update($data);

		return redirect()->back();

	}

	public function delete(Request $r){
		
		$GarnetAnnouncements = new \App\Http\Models\gem\GarnetAnnouncements;

		$data['status'] = '0';

		$GarnetAnnouncements->where('id', $r->input('id'))->update($data);

		return redirect()->back();

	}

}