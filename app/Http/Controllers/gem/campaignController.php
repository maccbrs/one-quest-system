<?php namespace App\Http\Controllers\gem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class campaignController extends Controller
{

	public function index(){

		$helper = new \App\Http\Controllers\gem\Helper;
		$Campaign = new \App\Http\Models\gem\Campaign;	
		$campaigns = $Campaign->paginate(10);	
		return view('gem.campaign.index',compact('helpers','campaigns'));
	}

	public function store(Request $r){

		$helper = new \App\Http\Controllers\gem\Helper;
		$Campaign = new \App\Http\Models\gem\Campaign;

		$input = $r->all();
		$input['user_id'] = Auth::user()->id;
		$Campaign->create($input);
		return redirect()->back();

	}


	public function add(){
		
	}

}