<?php namespace App\Http\Controllers\topaz;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class youController extends Controller
{


	public function index(){

		$help = new \App\Http\Controllers\topaz\Helper;
		$Ticket = new \App\Http\Models\topaz\Ticket;
		$CarbonCopy = new \App\Http\Models\topaz\CarbonCopy;
		$items = $Ticket->where('user_id',Auth::user()->id)->where('status','!=','closed')->with(['reply'])->get();
		$admins = $cc = [];
		if($items->count()):
		$item = end($items);
		$CampaignToAdmin = new \App\Http\Models\topaz\CampaignToAdmin;
		$admins = $help->admin_ids($CampaignToAdmin->where('campaign_id',$item[0]->campaign_id)->get());
		$cc = $CarbonCopy->where('user_id',Auth::user()->id)->where('status','true')->with(['ticket'])->get();		
		endif;

		return view('topaz.you.index',compact('items','help','admins','cc'));

	}

	public function create(){

		$help = new \App\Http\Controllers\topaz\Helper;
		$CampaignToUser = new \App\Http\Models\topaz\CampaignToUser;
		$items = $CampaignToUser->where('user_id',Auth::user()->id)->with(['campaign'])->get();
		//$help->pre($items);
		return view('topaz.you.create',compact('items'));
		
	}

	public function store(Request $r){

		$help = new \App\Http\Controllers\topaz\Helper;
		$Ticket = new \App\Http\Models\topaz\Ticket;
		$Content = new \App\Http\Models\topaz\Content;

		$cont = $Content->create(['content' => $r->content]);

		$tick = $Ticket->create([
			'title' => $r->title,
			'content_id' => $cont->id,
			'user_id' => Auth::user()->id,
			'campaign_id' => $r->campaign_id
		]);

		return redirect()->route('topaz.you.show',$tick->id);

	}

	public function show($id){

		$Ticket = new \App\Http\Models\topaz\Ticket;
		$help = new \App\Http\Controllers\topaz\Helper;
		$CampaignToAdmin = new \App\Http\Models\topaz\CampaignToAdmin;
		$item = $Ticket->where('id',$id)->with(['content','user','reply'])->first();
		$admins = $help->admin_ids($CampaignToAdmin->where('campaign_id',$item->campaign_id)->get());
		$responders = $help->responders($item,$admins);
		$CampaignToUser = new \App\Http\Models\topaz\CampaignToUser;
		$users = $CampaignToUser->where('campaign_id',$item->campaign_id)->with(['user'])->get();
		$CarbonCopy = new \App\Http\Models\topaz\CarbonCopy;
		$cc = $help->getCc($CarbonCopy->where('ticket_id',$id)->where('status','true')->get());

		return view('topaz.you.show',compact('item','responders','admins','users','cc','cctickets'));

	} 

	public function reply(Request $r){

		$help = new \App\Http\Controllers\topaz\Helper;
		$Content = new \App\Http\Models\topaz\Content;
		$Reply = new \App\Http\Models\topaz\Reply;
		$cont = $Content->create(['content' => $r->content]);

		$rep = $Reply->create([
			'ticket_id' => $r->ticket_id,
			'content_id' => $cont->id,
			'user_id' => Auth::user()->id
		]);
		
		return redirect()->back();

	} 

	public function close(Request $r,$id){

		$help = new \App\Http\Controllers\topaz\Helper;
		$Point = new \App\Http\Models\topaz\Point;
		$Ticket = new \App\Http\Models\topaz\Ticket;
		$item = $Point->updateOrCreate(['ticket_id' => $id],[
			'user_id' => $r->user_id,
			'closer_id' => Auth::user()->id,
			'notes' => $r->notes,
			'point' => $r->point
		]);
		$Ticket->find($id)->update(['status' => 'closed','point_id' => $item->id]);
		return redirect()->route('topaz.you.index');

	}

	public function reopen(Request $r,$id){

		$help = new \App\Http\Controllers\topaz\Helper;
		$Ticket = new \App\Http\Models\topaz\Ticket;
		$Ticket->find($id)->update(['status' => 'open']);
		return redirect()->route('topaz.you.show',$id);

	}	

	public function add_cc(Request $r){

		$help = new \App\Http\Controllers\topaz\Helper;
		$CarbonCopy = new \App\Http\Models\topaz\CarbonCopy;
		$CarbonCopy->updateOrCreate(
		   ['ticket_id' => $r->ticket_id,'user_id' => $r->user_id],
		   ['status' => $r->status]
		);

	}

	public function show_cc($id){

		$CarbonCopy = new \App\Http\Models\topaz\CarbonCopy;
		$item = $CarbonCopy->where('user_id',Auth::user()->id)->where('ticket_id',$id)->with('ticket')->first();
		$help = new \App\Http\Controllers\topaz\Helper;
		//$help->pre($item);
		return view('topaz.you.show_cc',compact('item'));
		
	}


}