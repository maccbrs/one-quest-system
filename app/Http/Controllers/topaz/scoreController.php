<?php namespace App\Http\Controllers\topaz;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class scoreController extends Controller
{

	public function index($id){

		$helper = new \App\Http\Controllers\topaz\Helper;
		$CampaignToAdmin = new \App\Http\Models\topaz\CampaignToAdmin;
		$users = $CampaignToAdmin->where('campaign_id',$id)->with(['user'])->get(); 
		$userids = [];

		if($users->count()):

			foreach($users as $u):
				$userids[] = $u->user_id;
			endforeach;
			$cw = $helper->current_week();
			$Point = new \App\Http\Models\topaz\Point;
			$points = $Point->whereIn('user_id',$userids)->whereBetween('created_at', [$helper->start_week($cw), $helper->end_week($cw)])->get();

			if($points->count()):
				$tempusers = [];

				foreach($points as $point):
					if(isset($tempusers[$point->user_id])):
						$tempusers[$point->user_id]['count']++;
						$tempusers[$point->user_id]['score'] += $point->point;
					else:
						$tempusers[$point->user_id]['count'] = 1;
						$tempusers[$point->user_id]['score'] = $point->point;					
					endif;
				endforeach;

				if($tempusers):
					foreach($users as $u):
						if(isset($tempusers[$u->user_id])):
							$u->points = ($tempusers[$u->user_id]['score']/$tempusers[$u->user_id]['count']);
							$u->count = $tempusers[$u->user_id]['count'];
						else:
							$u->points = 70;
							$u->count = 0;
						endif;
					endforeach;					
				endif;

			endif;

		endif;

		return view('topaz.score.index',compact('helper','users'));

	}


}