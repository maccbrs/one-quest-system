<?php namespace App\Http\Controllers\topaz;
use Auth;
use DateTime;


class Helper 
{


	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}

	public function last_reply($reply){
		if($reply):
			$item = $reply->toArray();
			//$this->pre(end($item));
			return end($item);
		endif;
	}


	public function time_ago( $date )
	{
	    if( empty( $date ) )
	    {
	        return "No date provided";
	    }

	    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");

	    $lengths = array("60","60","24","7","4.35","12","10");

	    $now = time();

	    $unix_date = strtotime( $date );

	    // check validity of date

	    if( empty( $unix_date ) )
	    {
	        return "Bad date";
	    }

	    // is it future date or past date

	    if( $now > $unix_date )
	    {
	        $difference = $now - $unix_date;
	        $tense = "ago";
	    }
	    else
	    {
	        $difference = $unix_date - $now;
	        $tense = "from now";
	    }

	    for( $j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++ )
	    {
	        $difference /= $lengths[$j];
	    }

	    $difference = round( $difference );

	    if( $difference != 1 )
	    {
	        $periods[$j].= "s";
	    }

	    return "$difference $periods[$j] {$tense}";

	}

	public function admin_ids($data){

		if($data->count()):
			$ids = [];
			foreach($data as $item):
				$ids[] = $item->user_id;
			endforeach;
			return $ids;
		else:
			return [];
		endif;
	}

	public function responders($item,$admins){

		$data = [];

		if($item->reply):
			foreach($item->reply as $reply):
				if(!array_key_exists($reply->user_id, $data) && in_array($reply->user_id, $admins)):
					$data[$reply->user_id] = $reply->user->name;
				endif;
			endforeach;
		endif;
		return $data;

	}

	public function getCc($obj){
		$data = [];

		if($obj->count()):
			foreach($obj as $d):
				$data[] = $d->user_id;
			endforeach;
		endif;
		return $data;
	}

	public function current_week(){
		return date("W");
	}

	public function getStartAndEndDate($week){
	  $dto = new DateTime();
	  $dto->setISODate(date('Y'), $week);
	  $ret['week_start'] = $dto->format('Y-m-d');
	  $dto->modify('+6 days');
	  $ret['week_end'] = $dto->format('Y-m-d');
	  return $ret;
	}

	public function start_week($w){
		$drange = $this->getStartAndEndDate($w);
		return $drange['week_start'].' 00:00:00';
	}
	public function end_week($w){
		$drange = $this->getStartAndEndDate($w);
		return $drange['week_end'].' 23:59:59';		
	}	

}