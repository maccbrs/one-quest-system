<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\agate\Helper;
use Carbon\Carbon;
use Auth;
use App\Http\Models\agate\CustomAudits;

class customController extends Controller
{
	
	public function index(){

		$CustomAudits =  CustomAudits::orderBy('created_at', 'desc')->get();
		//$agentsname = CustomAudits::find(1)->agentObj;
		//$CustomAudits = new \App\Http\Models\agate\CustomAudits;
		//$CustomAudits = $CustomAudits->get();	
		//echo "<pre>".print_r($agentsname)."<pre>";
		return view('agate.custom.index')->with('CustomAudits',$CustomAudits);

	}  

	public function add(){

		$Archives = new \App\Http\Models\agate\Archives;
		$archives = $Archives->owned()->with(['campaignObj'])->paginate(10);
		$Templates = new \App\Http\Models\agate\Templates;
		$templates = $Templates->get();	
		
		return view('agate.custom.add',compact('templates','archives'));

	}  

	public function create(Request $r){ 

		$this->validate($r,[
			'template_id' => 'required'
		]);

		$CustomAudits = new \App\Http\Models\agate\CustomAudits;

		$Templates = new \App\Http\Models\agate\Templates;
		$template = $Templates->find($r->template_id);

		if($template):

			$keyfactors = [];
			$data = [];
			foreach(json_decode($template->keyfactors) as $keyfactor):

				$tempParameters = [];
				if($keyfactor->parameters):
					foreach($keyfactor->parameters as $parameter):

							$tempItems = [];
							foreach($parameter->items as $i):
								$tempItems[] = [
									'title' => $i,
									'status' => 0
								];
							endforeach;
							$parameter->items = $tempItems;
							$parameter->score = $parameter->points;
							$tempParameters[] = $parameter;
					endforeach;
				endif;
				$keyfactor->parameters = $tempParameters;
				$keyfactor->score = $keyfactor->points;
				$keyfactors[] = $keyfactor;
			endforeach;

			$template->keyfactors = $keyfactors;
			$result = $this->computeScore(json_decode(json_encode($template)));
			$template->points = 0;
			$data['qa_results'] = json_encode($template);
			$data['point'] = $result['point'];
			$data['score'] = $result['score'];
			$data['evaluator'] = Auth::user()->id;
			$audit = $CustomAudits->create($data);



			return redirect()->route('agate.custom.audit',$audit->id)->with('success','success');

		endif;


	}

	// public function audit(){
	// 	echo 1;
	// }

	public  function audit($id){

		$help = new Helper;
		$CustomAudits = new \App\Http\Models\agate\CustomAudits;
		$Archives = new \App\Http\Models\agate\Archives;
		$GemUsers = new \App\Http\Models\agate\GemUsers;
		$Campaigns = new \App\Http\Models\agate\Campaigns;

		$agents = $GemUsers->agent()->get();
		$campaigns = $Campaigns->get();
		$archives = $Archives->owned()->get();

		$item = $CustomAudits->find($id);
		//pre(json_decode($item->qa_results));
		return view('agate.custom.audit',compact('item','agents','campaigns','archives','id'));

	}

	private function agents($bound){

		if($bound == 'inbound'):
			$Obj = new \App\Http\Models\agate\RecordingsCloser;
		else:
			$Obj = new \App\Http\Models\agate\Recordings;
		endif;

		$agents = $Obj->agent()->get();
		return $agents;	

	}

	private function campaigns($bound){

		if($bound == 'inbound'):
			$Obj = new \App\Http\Models\agate\RecordingsCloser;
		else:
			$Obj = new \App\Http\Models\agate\Recordings;
		endif;

		$campaigns = $Obj->campaigns()->get();
		return $campaigns;	

	}

	public function score($id,Request $r){

		$Helper = new \App\Http\Controllers\agate\Helper;
		$CustomAudits = new \App\Http\Models\agate\CustomAudits; 
		$audit = $CustomAudits->find($id);
		$data = [];

		foreach($r->all() as $key => $val):

			if (strpos($key, '|') !== false):
				$a = explode('|', $key);
				$data[$a[0]][$a[1]][str_replace('_',' ',$a[2])][$a[3]] = $val;
			endif;

		endforeach;


		$audit->score = 0;

		$qa_results = json_decode($audit->qa_results,true);
		$keyfactors = [];
		$redflag = false;
		//pre($r->all());
		foreach($qa_results['keyfactors'] as $keyfactor):

			$parameters = [];
			$keyfactor['score'] = 0;

			foreach($keyfactor['parameters'] as $parameter):

				$items = [];
				$parameter['score'] = $parameter['points'];

				foreach($parameter['items'] as $k => $item):

					if(isset($data[$keyfactor['id']][$parameter['id']][$k])):

						$input = $data[$keyfactor['id']][$parameter['id']][$k];
				//	pre($input);
						$item['aoo'] = $input['aoo'];
						$item['fb'] = $input['fb'];
						$item['status'] = (isset($input['status'])?1:0);
//pre($item);
					endif;
					$items[] = $item;
				endforeach;

				$parameter['items'] = $items;
				//pre($parameter);
				$parameters[] = $parameter;
			endforeach;

			$keyfactor['parameters'] = $parameters; 
			$audit->score += $keyfactor['score'];

			$keyfactors[] = $keyfactor;
		endforeach;
		//pre($keyfactors);
		$qa_results['keyfactors'] = $keyfactors;
		$qa_results = $this->computeScore(json_decode(json_encode($qa_results)));
		$audit->qa_results = json_encode($qa_results['result']);
		$audit->score = ($qa_results['alert']?0:$qa_results['score']);
		$audit->point = $qa_results['point'];
		$audit->monitoring_type = $r->monitoring_type;
		$audit->customer_name = $r->customer_name;
		$audit->customer_phone = $r->customer_phone;
		$audit->call_summary = $r->call_summary;
		

		if($r->call_date):
			$date = $this->getDate($r->call_date);
			$audit->week_no = $date['week'];
			$audit->month = $date['month'];
			$audit->year = $date['year'];
			$audit->call_date = $r->call_date;
		endif;

		$audit->archive_id = $r->archive_id;
		$audit->campaign_id = $r->campaign_id;
		$audit->dispo = $r->dispo;
		$audit->agent = $r->agent_id;

		$audit->save();

		return redirect()->back()->with('success','updated successfully!');

	}


	private function getDate($date){

		$date = Carbon::createFromFormat('Y-m-d H:i:s', $date);

		return [
			'week' => $date->weekOfYear,
			'month' => $date->month,
			'year' => $date->year
		];

	}

	private function computeScore($qa_results){
		//pre($qa_results);
		$score = 0;
		$point = 0;
		$alert = false;
		if($qa_results->keyfactors):
			$keyfactors = [];
			foreach($qa_results->keyfactors as $keyfactor):
				$keyfactor->score = 0;
				$keyfactor->points = 0;
				if($keyfactor->parameters):
					foreach($keyfactor->parameters as $parameter):

						if(!isset($keyfactor->scoringtype)) $keyfactor->scoringtype = 'standard';

						switch ($keyfactor->scoringtype):
							case 'autozero':
								$alert = $this->computeAutozero($parameter);
								break;
							case 'category':
								$parameter = $this->computeCategory($parameter);
								$keyfactor->score += $parameter->score;
								$point += $parameter->points;
								$keyfactor->points += $parameter->points;
								break;								
							default:
									$parameter = $this->computeStandard($parameter);
								    $keyfactor->score += $parameter->score;
								    $point += $parameter->points;
								    $keyfactor->points += $parameter->points;	
								break;
						endswitch;

					endforeach;
				endif;
				$score += $keyfactor->score;
				$keyfactors[] = $keyfactor;
			endforeach;

			$qa_results->keyfactors = $keyfactors;
		endif;

		return ['result' => $qa_results,'score' => $score,'point' => $point,'alert' => $alert];
	}

	private function computeStandard($parameter){

		$parameter->score = 0;
		//pre($parameter);
		if($parameter->items && isset($parameter->sub_points)): 
			foreach($parameter->items as $k => $item):
				if($item->status):
					$parameter->sub_points[$k]->score = 0;
				else:
					$parameter->sub_points[$k]->score = $parameter->sub_points[$k]->point;
				endif;
			endforeach;

			foreach($parameter->sub_points as $sub_points):
				$parameter->score += $sub_points->score;
			endforeach;
		endif;
		return $parameter;

	}

	private function computeCategory($parameter){

		$parameter->score = $parameter->points;
		if($parameter->items): 
			foreach($parameter->items as $item):
				if($item->status):
					$parameter->score = 0;
					break;
				endif;
			endforeach;
		endif;
		return $parameter;

	}

	private function computeAutozero($parameter){
		if($parameter->items):
			foreach($parameter->items as $k => $item):
				if($item->status):
					return true;
				endif;
			endforeach;	
			return false;		
		endif;
	}
// foreach (getallheaders() as $name => $value) {
//     // echo "$name: $value\n";
//     if($name == 'Host'):
//     	echo $value;
//     endif;
// }
// die; 


}