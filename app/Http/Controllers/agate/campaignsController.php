<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\agate\Helper;
use Auth;

class campaignsController extends Controller
{

	public function index(){

		$Helper = new \App\Http\Controllers\agate\Helper;
		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$campaigns = $Campaigns->paginate(20);
		return view('agate.campaigns.index',compact('campaigns'));
	}

	public function audits($id){

		$CustomAudits = new \App\Http\Models\agate\CustomAudits;
		$items = $CustomAudits->where('campaign_id',$id)->orderBy('call_date','desc')->paginate(20);
		return view('agate.campaigns.audits',compact('items'));

	}

	// public function users($bound,$campaign){

	// 	if($bound == 'inbound'):
	// 		$Obj = new \App\Http\Models\ruby\RecordingsCloser;
	// 	else:
	// 		$Obj = new \App\Http\Models\ruby\Recordings;
	// 	endif;

	// 	$users = $Obj->where('campaign_id',$campaign)->agent()->get();

	// 	return view('agate.campaigns.users',compact('users','bound','campaign'));

	// } 

	// public function user($bound,$campaign,$id){

	// 	if($bound == 'inbound'):
	// 		$Obj = new \App\Http\Models\ruby\RecordingsCloser;
	// 	else:
	// 		$Obj = new \App\Http\Models\ruby\Recordings;
	// 	endif;
		
	// 	$items = $Obj->where('campaign_id',$campaign)->where('agent',$id)->Notin()->orderBy('call_date','desc')->paginate(10);
	// 	//print_r('<pre>');print_r($id);print_r('</pre>'); exit; 
	// 	return view('agate.campaigns.view',compact('items','bound','campaign','id'));

	// }


	// public function view($bound,$campaign){

	// 	$Helper = new \App\Http\Controllers\agate\Helper;
	// 	if($bound == 'inbound'):
	// 		$Obj = new \App\Http\Models\ruby\RecordingsCloser;
	// 	else:
	// 		$Obj = new \App\Http\Models\ruby\Recordings;
	// 	endif;

 // 		$items = $Obj->where('campaign_id',$campaign)->Notin()->orderBy('call_date','desc')->paginate(10);

 // 		return view('agate.campaigns.view',compact('items','bound'));

	// }

	// public function search($bound,$campaign,$id,Request $r){

	// 	$daterangeArr = [];
	// 	if($bound == 'inbound'):
	// 		$Obj = new \App\Http\Models\ruby\RecordingsCloser;
	// 	else:
	// 		$Obj = new \App\Http\Models\ruby\Recordings;
	// 	endif;
		
	// 	$help = new Helper;

	// 	if($r->daterange):

	// 		$daterangeArr = array_map('trim',explode('|', $r->daterange));

	// 		//$help->pre($daterangeArr);

	// 		if($r->dispo):
	// 			$items = $Obj->where('agent',$id)->where('campaign_id',$campaign)->where('status',$r->dispo)->whereBetween('call_date',$daterangeArr)->Notin()->orderBy('call_date','desc')->paginate(10);
	// 		else:
	// 			$items = $Obj->where('agent',$id)->where('campaign_id',$campaign)->whereBetween('call_date',$daterangeArr)->Notin()->orderBy('call_date','desc')->paginate(10);
	// 		endif;

	// 	else:
	// 		return redirect()->back()->with('failed','you need to provide date range!');
	// 	endif;

		
	// 	//$help->pre($r->all());
	// 	$daterange = $r->daterange;
	// 	$dispo = $r->dispo;

	// 	return view('agate.campaigns.search',compact('items','bound','campaign','id','dispo','daterange'));

	// }			

}