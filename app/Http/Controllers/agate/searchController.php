<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class searchController extends Controller
{

	public function index(){

		$items = [];
		$bound = 'NA';
		return view('agate.search.index',compact('items','bound'));

	}

	public function result(Request $r){ 

		$this->validate($r,[
			'daterange' => 'required',
			'bound' => 'required'
		]);
		if($r->bound == 'inbound'):
			$Obj = new \App\Http\Models\agate\RecordingsCloser;
		else:
			$Obj = new \App\Http\Models\agate\Recordings;
		endif;



		$daterangeArr = array_map('trim',explode('|', $r->daterange));

			//$help->pre($daterangeArr);

		if($r->agent):
			$items = $Obj->where('agent',$r->agent)->whereBetween('call_date',$daterangeArr)->Notin()->orderBy('call_date','desc')->paginate(10);
		else:
			$items = $Obj->whereBetween('call_date',$daterangeArr)->Notin()->orderBy('call_date','desc')->paginate(10);
		endif;

		$agent = $r->agent;
		$daterange = $r->daterange;
		$bound = $r->bound;

		return view('agate.search.index',compact('bound','items','agent','daterange'));

	}

}