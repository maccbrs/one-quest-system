<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class auditsController extends Controller
{

	public function index($id){

		$Audit = new \App\Http\Models\agate\Audit;
		$audit = $Audit->where('id',$id)->with(['closer','recordings','set'])->first();
		return view('agate.audits.index',compact('audit'));
		
	} 


	public function audit($bound,$id){

		$templates = false;
		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$Archives = new \App\Http\Models\agate\Archives;
		$archives = $Archives->owned()->with(['campaignObj'])->paginate(10);
		$campaigns = $Campaigns->get(); 

		if($bound == 'inbound'):
			$Obj = new \App\Http\Models\agate\RecordingsCloser;
		else:
			$Obj = new \App\Http\Models\agate\Recordings;
		endif;	
 
		$item = $Obj->with(['account'])->find($id);
		$owned = ($item->evaluator == Auth::user()->id?true:false);
		$Templates = new \App\Http\Models\agate\Templates;
		$templates = $Templates->get();			


		if(!$item->isfinal):
			if(!$item->locked || $owned):
				return view('agate.audits.audit',compact('item','templates','bound','id','campaigns','archives'));
			endif;
		endif; 

		return view('agate.audits.close',compact('item','templates','bound','id','campaigns','archives'));

	}

	public function score($bound,$id,Request $r){


		$Helper = new \App\Http\Controllers\agate\Helper;

		$data = [];
		//$Helper->pre($r->all());
		foreach($r->all() as $key => $val):

			if (strpos($key, '|') !== false):
				$a = explode('|', $key);
				$data[$a[0]][$a[1]][str_replace('_',' ',$a[2])][$a[3]] = $val;
			endif;

		endforeach;

		if($bound == 'inbound'):
			$Obj = new \App\Http\Models\agate\RecordingsCloser;
		else:
			$Obj = new \App\Http\Models\agate\Recordings;
		endif;	

		$audit = $Obj->find($id);
		$audit->score = 0;

		$qa_results = json_decode($audit->qa_results,true);
		$keyfactors = [];
		$redflag = false;

		foreach($qa_results['keyfactors'] as $keyfactor):

			$parameters = [];
			$keyfactor['score'] = 0;

			foreach($keyfactor['parameters'] as $parameter):

				$items = [];
				$parameter['score'] = $parameter['points'];

				foreach($parameter['items'] as $item):

					if(isset($data[$keyfactor['id']][$parameter['id']][$item['title']])):

						$input = $data[$keyfactor['id']][$parameter['id']][$item['title']];
					
						$item['aoo'] = $input['aoo'];
						$item['fb'] = $input['fb'];

						if(isset($input['mark'])&&$input['mark'] == 'on'):
							$item['status'] = 1;
							$parameter['score'] = 0;
							$keyfactor['scoringtype'] = (isset($keyfactor['scoringtype'])?$keyfactor['scoringtype']:'standard');
							$redflag = ($keyfactor['scoringtype'] == 'autozero'?true:false);
						else:
							$item['status'] = 0;
						endif;

					endif;
					$items[] = $item;
				endforeach;

				$parameter['items'] = $items;
				$keyfactor['score'] += $parameter['score'];
				$parameters[] = $parameter;
			endforeach;

			$keyfactor['parameters'] = $parameters;
			$audit->score += $keyfactor['score'];
			$keyfactors[] = $keyfactor;
		endforeach;

		//print_r('<pre>');print_r($keyfactors);print_r('</pre>'); exit;

		$qa_results['keyfactors'] = $keyfactors;
		$audit->qa_results = json_encode($qa_results);
		$audit->monitoring_type = $r->monitoring_type;
		$audit->customer_name = $r->customer_name;
		$audit->customer_phone = $r->customer_phone;
		$audit->score = ($redflag?0:$audit->score);
		$audit->call_summary = $r->call_summary;
		$audit->save();

		return redirect()->back()->with('success','updated successfully!');

	}



	public function lock(Request $r, $bound,$id){

		if($bound == 'inbound'):
			$Obj = new \App\Http\Models\agate\RecordingsCloser;
		else:
			$Obj = new \App\Http\Models\agate\Recordings;
		endif;

			$item = $Obj->find($id);

			if($item):
				$Templates = new \App\Http\Models\agate\Templates;
				$template = $Templates->find($r->template_id);
				if($template):


					$Helper = new \App\Http\Controllers\agate\Helper;
					$keyfactors = [];

					//$Helper->pre($r->all());
					foreach(json_decode($template->keyfactors) as $keyfactor):

						$tempParameters = [];
						if($keyfactor->parameters):
							foreach($keyfactor->parameters as $parameter):

									$tempItems = [];
									foreach($parameter->items as $i):
										$tempItems[] = [
											'title' => $i,
											'status' => 0
										];
									endforeach;
									$parameter->items = $tempItems;
									$tempParameters[] = $parameter;
							endforeach;
						endif;
						$keyfactor->parameters = $tempParameters;

						$keyfactors[] = $keyfactor;
					endforeach;

					$template->keyfactors = $keyfactors;
					$item->qa_results = json_encode($template);
					$item->monitoring_type = $r->monitoring_type;
					$item->evaluator = Auth::user()->id;

					if($r->account_id):
						$item->account_id = $r->account_id;
					endif;
					
					$item->archive_id = $r->archive_id;
					$item->locked = 1;
					$item->save();

				endif;				
			endif;

		return redirect()->back();

	}




	private function computeScore($obj){

		$Helper = new \App\Http\Controllers\agate\Helper;
		//print_r($obj);die;
		if($obj->items):
			$items = json_decode($obj->items);

			switch ($obj->type) {
				case '3':
					$subs = 0;
					foreach ($items as $val):
						$subs += ($val->mark?$val->points:0);
						$score = $obj->points - $subs;
						$obj->score = ($score < 0?0:$score);
					endforeach;
					break;

				case '2':
					$add = 0;
					$total = 0;
					foreach ($items as $val):
						$add += (!$val->mark?$val->points:0);
						$total += $val->points;
						$percent = ($add/$total)*100;
						$obj->score = ($percent/100)*$obj->points;
					endforeach;
					break;

				case '1':
					$marked = 0;
					foreach ($items as $val):
						if($val->mark):
							$marked++;
						endif;
					endforeach;

					if($marked):
						$obj->score = 0;
					endif;

					break;									
			}
		endif;

		return $obj;

	}

	public function auditAccountUpdate(Request $r,$bound,$id){

		if($bound == 'inbound'):
			$Obj = new \App\Http\Models\ruby\RecordingsCloser;
		else:
			$Obj = new \App\Http\Models\ruby\Recordings;
		endif;	

		$campaign = $Obj->find($id);


		if($campaign):

			$campaign->account_id = $r->account_id;
			$campaign->save();
			return redirect()->back()->with('success',' successfully updated!');

		endif;

		return redirect()->back()->with('failed','data not found!');

	}


}