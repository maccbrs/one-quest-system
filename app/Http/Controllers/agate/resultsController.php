<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class resultsController extends Controller
{

	public function index(){

		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$campaigns = $Campaigns->get();
		return view('agate.results.index',compact('campaigns')); 
	}

	public function read($id){ 

		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$Closer = new \App\Http\Models\agate\RecordingsCloser;
		$Recordings = new \App\Http\Models\agate\Recordings;		
		$help = new \App\Http\Controllers\gem\Helper;
		$campaign = $Campaigns->find($id);
		$campaigns = json_decode($campaign->campaigns,true);
		$title = $campaign->title;
		
		

		if($campaign->bound == 'inbound'):
			$results = $Closer->week()->whereIn('campaign_id', $campaigns)->orderBy('year','desc')->orderBy('week_no','desc')->get();
		else:
			$results = $Recordings->week()->whereIn('campaign_id', $campaigns)->orderBy('year','desc')->orderBy('week_no','desc')->get();
		endif;

		$counts = $this->weekly_counts($results,$campaigns,$campaign->bound);
		return view('agate.results.read',compact('results','id','title','counts'));
	}

	public function lists($id,$weekno){

		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$Closer = new \App\Http\Models\agate\RecordingsCloser;
		$Recordings = new \App\Http\Models\agate\Recordings;		
		$help = new \App\Http\Controllers\agate\Helper;
		$campaign = $Campaigns->find($id);
		$campaigns = json_decode($campaign->campaigns,true);
		$bound = $campaign->bound;
		if($bound == 'inbound'):
			$results = $Closer->where('week_no',$weekno)->where('locked',1)->whereIn('campaign_id', $campaigns)->get();
		else:
			$results = $Recordings->where('week_no',$weekno)->where('locked',1)->whereIn('campaign_id', $campaigns)->get();
		endif;

		return view('agate.results.lists',compact('results','bound','help'));

	}

	public function weekly_counts($results,$campaigns,$bound){

		$Closer = new \App\Http\Models\agate\RecordingsCloser;
		$Recordings = new \App\Http\Models\agate\Recordings;		
		$help = new \App\Http\Controllers\gem\Helper;
		$return = [];
		$weeks = [];
		foreach($results as $r):
			$weeks[] = $r->week_no;
		endforeach;	

		if($weeks):
			if($bound == 'inbound'):
				$results = $Closer->whereIn('campaign_id', $campaigns)->whereIn('week_no',$weeks)->where('locked',1)->get();
			else:
				$results = $Recordings->whereIn('campaign_id', $campaigns)->whereIn('week_no',$weeks)->where('locked',1)->get();
			endif;

			if($results):
				foreach($results as $r):
					if(isset($return[$r->week_no])):
						$return[$r->week_no]++;
					else:
						$return[$r->week_no] = 1;
					endif;
				endforeach;				
			endif;
		endif;

		return $return;
			
	}		

}