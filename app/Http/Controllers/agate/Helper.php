<?php namespace App\Http\Controllers\agate;
use Auth;


class Helper {

	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}


	public function evaluator($id){

		if($id):
			$Users = new \App\Http\Models\gem\User;
			$users = $Users->evaluator()->get();
			
			foreach($users as $u){
				if($u->id == $id){
					return ucwords($u->name);
				}
			}
		else:
			return 'None';
		endif;

	}
	

}