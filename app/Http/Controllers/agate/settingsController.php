<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\agate\Helper;
use Auth;
use Image;
use File;

class settingsController extends Controller
{

	public function campaigns(){

		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$campaigns = $Campaigns->where('status','=',1)->get();

		return view('agate.settings.campaigns',compact('campaigns'));

	}

	public function addCampaign(Request $r){

		$this->validate($r,[
			'title' => 'required|unique:ruby.campaigns,title'
		]);

		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$result = $Campaigns->create(['title' => $r->title]);

		if($result):
			return redirect()->back()->with('success',$r->title.' created!');
		endif;

		return redirect()->back()->with('failed','error occured!');

	}


	public function editCampaign($id){

		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$campaign = $Campaigns->find($id);

		if($campaign):
			return view('agate.settings.edit-campaign',compact('campaign','id'));
		endif;

		return redirect()->back()->with('failed','data not found!');

	}


	public function deleteCampaign(Request $r){


		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$campaign = $Campaigns->find($r->input('id'));


		if($campaign):

			$data['status'] = 0; 
			$Campaigns->where('id','=', $r->input('id'))->update($data);

			return redirect()->back()->with('success','Campaign has been deleted!');

		endif;

		return redirect()->back()->with('failed','data not found!');

	}

	public function updateCampaign(Request $r,$id){


		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$campaign = $Campaigns->find($id);

		if($campaign):

			$campaign->update($r->only(['customer_name','caller_id','phone_number','file_name']));
			return redirect()->back()->with('success', $campaign->title.' successfully updated!');

		endif;

		return redirect()->back()->with('failed','data not found!');

	}

	public function campaignUpdateLogo(Request $r,$id){

		$this->validate($r,['file' => 'required']);
		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$campaign = $Campaigns->find($id);

		if($campaign):

	        $image = $r->file('file');
	        $filename  = time() . '.' . $image->getClientOriginalExtension();
	        $sm = public_path('uploads/sm-' . $filename);
	        $m = public_path('uploads/m-' . $filename);
	        $l = public_path('uploads/l-' . $filename);
	        \Image::make($image->getRealPath())->resize(50, 50)->save($sm);
	        \Image::make($image->getRealPath())->resize(100, 100)->save($m);
	        \Image::make($image->getRealPath())->resize(200, 200)->save($l);

	        $this->unlink($campaign->logo);	
	        $campaign->logo = $filename;
	        $campaign->save();

	        return redirect()->back()->with('success',$campaign->title.' logo updated!');

		endif;

		return redirect()->back()->with('failed','data not found!');

	}


	private function unlink($fn){
        $sm = 'uploads/sm-'.$fn;
        $m = 'uploads/m-'.$fn;
        $l = 'uploads/l-'.$fn;
        File::Delete($sm);
        File::Delete($m);
        File::Delete($l);
        return true;		
	}

	public function deactivate($bound,$type){

		if($bound == 'inbound'):
			$Obj = new \App\Http\Models\agate\RecordingsCloser;
		else:
			$Obj = new \App\Http\Models\agate\Recordings;
		endif;	

		if($type == 'campaigns'):
			$items = $Obj->campaigns()->get(); 
		else:
			$items = $Obj->agent()->get();
		endif;

		return view('agate.settings.deactivate',compact('items','bound','type'));	

	}

	public function postDeactivate(Request $r,$bound,$type){

		if($bound == 'inbound'):
			$Obj = new \App\Http\Models\agate\RecordingsCloser;
		else:
			$Obj = new \App\Http\Models\agate\Recordings;
		endif;	

		if($type == 'campaigns'):
			$this->validate($r,['campaign_ids' => 'required']);
			$Obj->whereIn('campaign_id',$r->campaign_ids)->update(['is_active' => '0']);
		else:
			$this->validate($r,['agents' => 'required']);
			$Obj->whereIn('agent',$r->agents)->update(['is_active' => '0']);
		endif;

		return redirect()->back()->with('success','updated successfully!');

	}


} 

