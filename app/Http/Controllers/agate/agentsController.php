<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\agate\Helper;
use Auth;

class agentsController extends Controller
{

	public function index(){ 

		$GemUsers = new \App\Http\Models\agate\GemUsers;
		$agents = $GemUsers->agent()->paginate(20);
		$Helper = new \App\Http\Controllers\agate\Helper;
		return view('agate.agents.index',compact('agents'));

	}

	// public function campaigns($bound,$campaign,$user){

	// 	if($bound == 'inbound'):
	// 		$Obj = new \App\Http\Models\ruby\RecordingsCloser;
	// 	else:
	// 		$Obj = new \App\Http\Models\ruby\Recordings;
	// 	endif;

	// 	$campaigns = $Obj->where('campaign_id',$campaign)->campaigns()->get();

	// //rint_r('<pre>');print_r($campaigns);print_r('</pre>'); exit; 

	// 	$breadc_data['bound'] = $bound; 
	// 	$breadc_data['campaign'] = $campaign; 
	// 	$breadc_data['user'] = $user; 

	// 	return view('agate.agents.campaigns',compact('campaigns','bound','user','campaign','breadc_data'));

	// }

	// public function campaign($bound,$user,$id){

	// 	if($bound == 'inbound'):
	// 		$Obj = new \App\Http\Models\ruby\RecordingsCloser;
	// 	else:
	// 		$Obj = new \App\Http\Models\ruby\Recordings;
	// 	endif;
		
	// 	$items = $Obj->where('agent',$user)->where('campaign_id',$id)->Notin()->orderBy('call_date','desc')->paginate(10);

	// 	$agent = $user;

	// //	print_r('<pre>');print_r($id);print_r('</pre>'); exit;

	// 	return view('agate.agents.view',compact('items','bound','agent','id','user'));

	// }

	// public function search($bound,$user,$id,Request $r){

	// 	$daterangeArr = [];
	// 	if($bound == 'inbound'):
	// 		$Obj = new \App\Http\Models\ruby\RecordingsCloser;
	// 	else:
	// 		$Obj = new \App\Http\Models\ruby\Recordings;
	// 	endif;
		
	// 	$help = new Helper;

	// 	if($r->daterange):

	// 		$daterangeArr = array_map('trim',explode('|', $r->daterange));

	// 		//$help->pre($daterangeArr);

	// 		if($r->dispo):
	// 			$items = $Obj->where('agent',$user)->where('campaign_id',$id)->where('status',$r->dispo)->whereBetween('call_date',$daterangeArr)->Notin()->orderBy('call_date','desc')->paginate(10);
	// 		else:
	// 			$items = $Obj->where('agent',$user)->where('campaign_id',$id)->whereBetween('call_date',$daterangeArr)->Notin()->orderBy('call_date','desc')->paginate(10);
	// 		endif;

	// 	else:
	// 		return redirect()->back()->with('failed','you need to provide date range!');
	// 	endif;

		

		
	// 	//$help->pre($r->all());
	// 	$agent = $user;
	// 	$daterange = $r->daterange;
	// 	$dispo = $r->dispo;

	// 	return view('agate.agents.search',compact('items','bound','agent','id','dispo','daterange'));

	// }

	// public function view($bound,$agent){

	// 	$Helper = new \App\Http\Controllers\agate\Helper;
	// 	if($bound == 'inbound'):
	// 		$Obj = new \App\Http\Models\agate\RecordingsCloser;
	// 	else:
	// 		$Obj = new \App\Http\Models\agate\Recordings;
	// 	endif;

 // 		$items = $Obj->where('agent',$agent)->Notin()->orderBy('call_date','desc')->paginate(10);
 // 		//$Helper->pre($items->toArray());
 // 		return view('agate.agents.view',compact('items','bound','agent'));

	// }

	// public function results($bound){

	// 	if($bound == 'inbound'):
	// 		$Obj = new \App\Http\Models\agate\RecordingsCloser;
	// 	else:
	// 		$Obj = new \App\Http\Models\agate\Recordings;
	// 	endif;
	// 	$items = $Obj->agent()->Notin()->get();
	// 	return view('agate.agents.results',compact('items','bound'));
	// }

	// public function agent($bound,$agent){


	// 	if($bound == 'inbound'):
	// 		$Obj = new \App\Http\Models\agate\RecordingsCloser;
	// 	else:
	// 		$Obj = new \App\Http\Models\agate\Recordings;
	// 	endif;

	// 	$items = $Obj->where('agent',$agent)->week()->orderBy('year','desc')->orderBy('week_no','desc')->paginate(10);

	// 	$counts = $this->weekly_counts($items,$agent,$bound);
	// 	return view('agate.agents.agent',compact('items','agent','bound','counts')); 
	// }

	// public function weekly_counts($results,$agent,$bound){

	// 	$Closer = new \App\Http\Models\agate\RecordingsCloser;
	// 	$Recordings = new \App\Http\Models\agate\Recordings;
	// 	$return = [];
	// 	$weeks = [];
	// 	foreach($results as $r):
	// 		$weeks[] = $r->week_no;
	// 	endforeach;	

	// 	if($weeks):
	// 		if($bound == 'inbound'):
	// 			$results = $Closer->where('agent', $agent)->whereIn('week_no',$weeks)->where('locked',1)->get();
	// 		else:
	// 			$results = $Recordings->where('agent', $agent)->whereIn('week_no',$weeks)->where('locked',1)->get();
	// 		endif;

	// 		if($results):
	// 			foreach($results as $r):
	// 				if(isset($return[$r->week_no])):
	// 					$return[$r->week_no]++;
	// 				else:
	// 					$return[$r->week_no] = 1;
	// 				endif;
	// 			endforeach;				
	// 		endif;
	// 	endif;

	// 	return $return;
			
	// }

	// public function week($bound,$agent,$weekno){

	// 	$help = new \App\Http\Controllers\agate\Helper;
	// 	if($bound == 'inbound'):
	// 		$Closer = new \App\Http\Models\agate\RecordingsCloser;
	// 		$items = $Closer->where('agent',$agent)->where('week_no',$weekno)->where('locked',1)->orderBy('call_date','desc')->paginate(10);
	// 	else:
	// 		$Recordings = new \App\Http\Models\agate\Recordings;
	// 		$items = $Recordings->where('agent',$agent)->where('week_no',$weekno)->where('locked',1)->orderBy('call_date','desc')->paginate(10);
	// 	endif;
	// 	return view('agate.agents.week',compact('items','bound','help','weekno','agent'));

	// }		
	
}