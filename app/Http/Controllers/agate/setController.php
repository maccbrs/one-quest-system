<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class setController extends Controller
{

	public function index($id){ 
		
		$Set = new \App\Http\Models\agate\QaSettings;
		$Audit = new \App\Http\Models\agate\Audit;
		$audits = [];
		$set = $Set->find($id);

		if($set->bound == 'inbound'):
			$audits = $Audit->where('set_id',$id)->orderBy('id','desc')->with(['closer'])->paginate(10);
		endif;

		if($set->bound == 'outbound'):
			$audits = $Audit->where('set_id',$id)->orderBy('id','desc')->with(['recordings'])->paginate(10);
		endif;
		//$help->pre($audits);

		return view('agate.set.index',compact('set','audits','id'));

	}

}




