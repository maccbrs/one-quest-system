<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class metricsController extends Controller
{

	public function keyFactors(){
		$KeyFactors = new \App\Http\Models\agate\KeyFactors;
		$keyfactors = $KeyFactors->paginate(10);
		return view('agate.metrics.keyfactors',compact('keyfactors'));
	}

	public function postKeyFactor(Request $r){  

		$this->validate($r,[
			'title' => 'required|unique:ruby.key_factors,title',
			'description' => 'required'
		]);
		$KeyFactors = new \App\Http\Models\agate\KeyFactors;
		$KeyFactors->create(['title' =>$r->title,'description' =>$r->description]);
		return redirect()->back()->with('success',$r->title.' created successfully!');
	}

	public function editKeyFactor($id){


		$Parameters = new \App\Http\Models\agate\Parameters;
		$parameters = $Parameters->get();
		$KeyFactors = new \App\Http\Models\agate\KeyFactors;
		$keyfactor = $KeyFactors->find($id);
		return view('agate.metrics.edit-key-factor',compact('keyfactor','parameters'));

	}

	public function deleteKeyFactor($id){

		$KeyFactors = new \App\Http\Models\agate\KeyFactors;
		$keyfactor = $KeyFactors->find($id);

		if($keyfactor):
			$keyfactor->delete();
			return redirect()->back()->with('success','data deleted!');
		else:
			return redirect()->back()->with('failed','data not found!');
		endif;

	}

	public function deleteKeyFactorParameter($keyfactorid,$parameterid){ 

		$KeyFactors = new \App\Http\Models\agate\KeyFactors;
		$keyfactor = $KeyFactors->find($keyfactorid);
		$title = '';
		if($keyfactor):

			$parameters = [];			
			foreach(json_decode($keyfactor->parameters) as $parameter):
				if($parameter->id != $parameterid):
					$parameters[] = $parameter;
				else:
					$title = $parameter->title;
				endif;
			endforeach;
			$keyfactor->parameters = json_encode($parameters);
			$keyfactor->save();
			return redirect()->back()->with('success',$title.' deleted!');
		else:
			return redirect()->back()->with('failed','data not found!');
		endif;
		
	}

	public function parameters(){

		$Parameters = new \App\Http\Models\agate\Parameters;
		$parameters = $Parameters->paginate(10);	
		return view('agate.metrics.parameters',compact('parameters'));

	}

	public function postParameter(Request $r){

		$this->validate($r,[
			'title' => 'required|unique:ruby.parameters,title'
		]);

		$Parameters = new \App\Http\Models\agate\Parameters;
		$Parameters->create(['title' =>$r->title]);
		return redirect()->back()->with('success',$r->title.' successfully added!');		
	}

	public function deleteParameter($id){

		//print_r($id); exit;

		$Parameters = new \App\Http\Models\agate\Parameters;
		$parameter = $Parameters->find($id);

		if($parameter):
			$parameter->delete();
			return redirect()->back()->with('success','data deleted!');
		else:
			return redirect()->back()->with('failed','found no data!');
		endif;
	}

	public function postParameterItem(Request $r,$parameterid){

		$this->validate($r,[
			'item' => 'required'
		]);

		$Parameters = new \App\Http\Models\agate\Parameters;
		$parameter = $Parameters->find($parameterid);

		if($parameter):
			$items = ($parameter->items?json_decode($parameter->items,true):[]);
			$items[] = $r->item;
			$parameter->items = json_encode($items);
			$parameter->save();
		else:

			return redirect()->back()->with('failed','not existing data!');

		endif;

		return redirect()->back()->with('success',$parameter->title.' successfully updated!');

	}	

	public function editParameterItem(Request $r,$param,$item){
		
		$this->validate($r,[
			'item' => 'required'
		]);

		$Parameters = new \App\Http\Models\agate\Parameters;
		$parameter = $Parameters->find($param);

		if($parameter):
			$items = ($parameter->items?json_decode($parameter->items,true):[]);
			$temp = [];
			foreach($items as $k => $v):
				if($item == $v):
					$temp[$k] = $r->item;
				else:
					$temp[$k] = $v;
				endif;
			endforeach;
			$parameter->items = json_encode($temp);
			$parameter->save();

			return redirect()->back()->with('success','updated successfully');
		endif;

		return redirect()->back()->with('failed','not existing data!');

	}

	public function deleteParameterItem(Request $r,$param,$item){
		$this->validate($r,[
			'item' => 'required'
		]);


		$Parameters = new \App\Http\Models\agate\Parameters;
		$parameter = $Parameters->find($param);

	
		if($parameter):
			$items = ($parameter->items?json_decode($parameter->items,true):[]);
			$temp = [];

			foreach($items as $k => $v):

				if (($k = array_search($r->input('item'), $items)) !== false) {
				    unset($items[$k]);
				}

			endforeach;

			$parameter->items = json_encode($items);

			$parameter->save();

			return redirect()->back()->with('success','updated successfully');
		endif;

		return redirect()->back()->with('failed','not existing data!');

	}


	public function keyfactorAddParameter(Request $r,$keyfactorid){

		$KeyFactors = new \App\Http\Models\agate\KeyFactors;
		$Parameters = new \App\Http\Models\agate\Parameters;
		$keyfactor = $KeyFactors->find($keyfactorid);
		$parameter = $Parameters->find($r->parameterid);

		if($keyfactor && $parameter):
			$parameters = ($keyfactor->parameters?json_decode($keyfactor->parameters,true):[]);
			$parameters[] = [
				'id' => $parameter->id,
				'title' => $parameter->title,
				'points' => 0,
				'items' => json_decode($parameter->items,true)
			];

			$keyfactor->parameters = json_encode($parameters);
			$keyfactor->save();

		endif;

		return redirect()->back()->with('success',$keyfactor->title.' updated!');

	}

	public function templates(){

		$Templates = new \App\Http\Models\agate\Templates;
		$templates = $Templates->paginate(10);
		a``111
		return view('agate.metrics.templates',compact('templates'));

	}

	public function createTemplate(Request $r){

		$this->validate($r,[
			'title' => 'required|unique:ruby.templates,title'
		]);
		$Templates = new \App\Http\Models\agate\Templates;
		$Templates->create(['title' => $r->title]);
		return redirect()->back()->with('success',$r->title.' created!');

	}

	public function editTemplate($id){

		$Templates = new \App\Http\Models\agate\Templates;
		$KeyFactors = new \App\Http\Models\agate\KeyFactors;
		$keyfactors = $KeyFactors->get();
		$template = $Templates->find($id);

		//pre(json_decode($template->keyfactors));

		return view('agate.metrics.edit-template',compact('template','keyfactors'));


	}

	public function deleteTemplate($id){

		$Templates = new \App\Http\Models\agate\Templates;
		$template = $Templates->find($id);
	
		if($template):
			$title = $template->title;
			$template->delete();
			return redirect()->back()->with('success',$title.' deleted!');
		else:
			return redirect()->back()->with('failed','data not found!');
		endif;

	}

	public function templateAddKeyFactor(Request $r,$templateid){

		$Templates = new \App\Http\Models\agate\Templates;
		$KeyFactors = new \App\Http\Models\agate\KeyFactors;
		$template = $Templates->find($templateid);
		$keyfactor = $KeyFactors->find($r->keyfactorid);



		if($template && $keyfactor):

			$keyfactors = ($template->keyfactors?json_decode($template->keyfactors,true):[]);

			$keyfactors[] = [
				'id' => $keyfactor->id,
				'title' => $keyfactor->title,
				'points' => 0,
				'description' => $keyfactor->description,
				'parameters' => json_decode($keyfactor->parameters,true) 
			];

			$template->keyfactors = json_encode($keyfactors);
			$template->save();
		else:
			return redirect()->back()->with('failed','not existing data');
		endif;

		return redirect()->back()->with('success',$template->title.' updated!');

	}

	public function templateEditKeyFactor(Request $r, $templateid,$keyfactorid){
 
		$this->validate($r,[
			'description' => 'required',
			'scoringtype' => 'required'
		]);

		$Templates = new \App\Http\Models\agate\Templates;
		$template = $Templates->find($templateid);
		$title = '';

		if($template):
			$keyfactors = [];
			foreach(json_decode($template->keyfactors,true) as $keyfactor):
				if($keyfactor['id'] == $keyfactorid):
					$keyfactor['description'] = $r->description;
					$keyfactor['scoringtype'] = $r->scoringtype;	
					$title = $keyfactor['title'];				
				endif;	
				$keyfactors[] = $keyfactor;
			endforeach;
			$template->keyfactors = json_encode($keyfactors);
			$template->save();
		endif;

		return redirect()->back()->with('success',$title.' succesfully updated!');

	}
	public function templateEditKeyFactorParameter(Request $r, $templateid,$keyfactorid,$parameterid){ 


		$Templates = new \App\Http\Models\agate\Templates;
		$template = $Templates->find($templateid);
		$Helper = new \App\Http\Controllers\agate\Helper;

		if($template):
			$keyfactors = [];
			foreach(json_decode($template->keyfactors) as $keyfactor):
				if($keyfactor->id == $keyfactorid):
					
					$paramArr = [];
					$keyfactor->points = 0;

					if(isset($keyfactor) && $keyfactor->scoringtype != 'standard'):

						switch ($keyfactor->scoringtype):
							case 'category':
								foreach($keyfactor->parameters as $parameter):
									if($parameter->id == $parameterid):
										$parameter->points = ($r->points?$r->points:0);
										$parameter->sub_points = [];
									endif;
									$paramArr[] = $parameter;
									$keyfactor->points += $parameter->points;
								endforeach;
								break;
						endswitch;

					else:

						$input = $this->filterPoint($r->all());

						foreach($keyfactor->parameters as $parameter):
							if($parameter->id == $parameterid):
								$parameter->points = $input['total'];
								$parameter->sub_points =  $input['sub_point'];
							endif;
							$paramArr[] = $parameter;
							$keyfactor->points += $parameter->points;
						endforeach;

					endif;

					$keyfactor->parameters = $paramArr;
				endif;

				$keyfactors[] = $keyfactor;
			endforeach;
			$template->keyfactors = json_encode($keyfactors);

			$template->save();
		endif;

		return redirect()->back();

	}


	public function updateKeyFactor(Request $r,$id){

	    $this->validate($r,[
	        'title' => 'required',
	        'description' => 'required',
	    ]);

		$KeyFactors = new \App\Http\Models\agate\KeyFactors;
		$keyfactor = $KeyFactors->find($id);

		if($keyfactor):
			$keyfactor->title = $r->title;
			$keyfactor->description = $r->description;
			$keyfactor->save();
		endif;

		return redirect()->back()->with('success', $keyfactor->title.' updated succesfully!');;

	}

	public function templateDeleteKeyFactor($templateid,$keyfactorid){

		$Templates = new \App\Http\Models\agate\Templates;
		$template = $Templates->find($templateid);

		if($template):
			$keyfactors = [];
			$title = '';
			foreach(json_decode($template->keyfactors) as $keyfactor):
				if($keyfactor->id != $keyfactorid):
					$keyfactors[] = $keyfactor;
				else:
					$title = $keyfactor->title;
				endif;
			endforeach;

			$template->keyfactors = json_encode($keyfactors);
			$template->save();

			return redirect()->back()->with('success',$title.' deleted!');

		else:

			return redirect()->back()->with('failed','data not found!');

		endif;
	}
	
	private function filterPoint($arr){

		$total = 0;
		$data['sub_point'] = [];
		foreach($arr as $k => $v):
			//pre($k,false);
			if(strpos($k,'point_') !== false):
				$key = explode('_',$k);
				$data['sub_point'][$key[1]]['point'] = $v;
				$data['sub_point'][$key[1]]['score'] = $v;
				$total += $v;
			endif;
		endforeach; //die;

		$data['total'] = $total;
		return $data;
		
	}

}