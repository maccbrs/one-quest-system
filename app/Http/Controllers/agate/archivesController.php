<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\agate\Helper;
use Auth;

class archivesController extends Controller
{

	public function index(){ 

		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$Archives = new \App\Http\Models\agate\Archives;
		$campaigns = $Campaigns->get();
		$archives = $Archives->owned()->with(['campaignObj'])->paginate(10);
		return view('agate.archives.index',compact('campaigns','archives'));

	}

	public function create(Request $r){

		$this->validate($r,[
			'title' => 'required',
			'date' => 'required',
			'month' => 'required',
			'year' => 'required',
			'campaign' => 'required'
		]);

		$Archives = new \App\Http\Models\agate\Archives;
		$input = $r->all();
		$input['name'] = $r->title;
		$input['user'] = Auth::user()->id;
		$Archives->create($input);
		return redirect()->back()->with('success','created successfully!');

	}

	public function contents($id){

		$CustomAudits = new \App\Http\Models\agate\CustomAudits;
		$items = $CustomAudits->where('archive_id',$id)->with(['campaign','agentObj'])->get();
		return view('agate.archives.contents',compact('items'));   

	}

	public function isfinal(Request $r){

		$help = new Helper;
		//$help->pre($r->all());
		$inbounds = $outbounds = false;

		foreach($r->all() as $key => $val):
			if (strpos($key, 'inbound') !== false):
				$arr = explode('_', $key);
				$inbounds[] = $arr[1];
			endif;
			if (strpos($key, 'outbound') !== false):
				$arr = explode('_', $key);
				$outbounds[] = $arr[1];
			endif;
		endforeach;	

		if($inbounds):
			$Inbound = new \App\Http\Models\agate\RecordingsCloser;
			$Inbound->whereIn('id',$inbounds)->update(['final' => 1]);
		endif;
		if($outbounds):
			$Outbound->whereIn('id',$outbounds)->update(['final' => 1]);
		endif;
	
		if($outbounds || $inbounds):
			return redirect()->back()->with('success','successfully updated!');
		else:
			return redirect()->back();
		endif;

	}

	public function audit($bound,$id){

		$templates = false;
		$Campaigns = new \App\Http\Models\agate\Campaigns;
		$Archives = new \App\Http\Models\agate\Archives;
		$archives = $Archives->owned()->with(['campaignObj'])->paginate(10);
		$campaigns = $Campaigns->get(); 

		if($bound == 'inbound'):
			$Obj = new \App\Http\Models\agate\RecordingsCloser;
		else:
			$Obj = new \App\Http\Models\agate\Recordings;
		endif;	

		$item = $Obj->with(['account'])->find($id);
		$owned = ($item->evaluator == Auth::user()->id?true:false);

		$Templates = new \App\Http\Models\agate\Templates;
		$templates = $Templates->get();	

		if($item->final || !$owned):
			return view('agate.audits.close',compact('item','templates','bound','id','campaigns','archives'));
		else:
			return view('agate.audits.audit',compact('item','templates','bound','id','campaigns','archives'));
		endif; 

	}

}