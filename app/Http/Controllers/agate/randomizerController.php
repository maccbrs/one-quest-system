<?php namespace App\Http\Controllers\agate;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class randomizerController extends Controller
{

	public function index(){

		$QaSets = new \App\Http\Models\agate\QaSettings;
		$sets = $QaSets->with(['account'])->get();

		return view('agate.randomizer.index',compact('sets'));

	}

}