<?php namespace App\Http\Controllers\emerald;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use AppHelper;


class CrudController extends Controller
{


	public function index(){
	
			return view('emerald.tpcast.index');//,compact('items','users','help','type','larr_myarray','count','userlist','qrcode'));
	}
	
	public function dropdownviewlist($dropdown){
	
			return view('emerald.tpcast.dropdownlist')->with('dropdown',$dropdown);//,compact('items','users','help','type','larr_myarray','count','userlist','qrcode'));
	}
	public function formview(){
	
			return view('emerald.tpcast.form');//,compact('items','users','help','type','larr_myarray','count','userlist','qrcode'));
	}
	
	
	public function dropdownview(){
					
					
					$MasterFileName =  "MstDropDown";
					$Test = array("\App\Http\Models" . chr(92). "emerald" . chr(92) .$MasterFileName);		
					$A = new $Test[0];
					$masterfilelist =  $A->where('status', '=' , 'enable')->get();
					//$urllist  = $mstlist;
	
	
	
	
			return view('emerald.tpcast.dropdown');//->with('table_columns',$A['fillable']);//,compact('items','users','help','type','larr_myarray','count','userlist','qrcode'));
	}
	
	public function store(Request $request)
	{
		$return_msg ="";
		$log_msg ="";
		if(isset($request->ModelName) &&  ($request->ModelName !=""))
		{
						$model = $request->ModelName;
					
					
					
					$MasterFileName = ($model);
					$table_model = array("\App\Http\Models" . chr(92). "emerald" . chr(92) .$MasterFileName);		
					$table_columns = new $table_model[0];
					/*$validateMstBrand =  $table_columns->where('name',strtolower($request->name))->count();		
							
					 //echo $validateMstBrand;
					if 	($validateMstBrand > 0)
					{
						$return_msg = $return_msg  .  " Duplicate Entry " . $request->name ;
						$log_msg = $return_msg ;
						$masterfilelist =  $table_columns->where('status', '=' , 'enable')->get();
						//break;
					}
					
					else  */
					{
					
					$return_msg = $return_msg  .  $request->name  . " has successfully created" ;
					
				
					$masterfilelist = new $table_model[0];
					
							foreach($table_columns['fillable'] as $array_num => $table_column)
							{
								if(isset($request->$table_column))
									{
										$masterfilelist->$table_column = $request->$table_column;
									}
							}

							$masterfilelist->created_by = Auth::user()->id;
							$masterfilelist->updated_by = Auth::user()->id;
					
					
					$masterfilelist ->save();		
					$log_msg = $request->name  . " has successfully created "  . ucfirst($model) . " MasterFile" ;
					//$masterfilelist = MstBrand::where('status', '=' , 'enable')->get();//MstBrand::All();
					
					
					}
					
					$Logger = new AppHelper;
					$Logger->SaveLog($log_msg);
		
		}	
		
		return back()->with('return_msg', $log_msg);
		//return redirect('/bloodstone/masterfile/' . $mstlist)->with('MstBrand',$masterfilelist)->with('urllist', $mstlist)->with('return_msg', $return_msg);
		
	} 
	
	public function formstore(Request $request)
	{
		$return_msg ="";
		$log_msg ="";
		if(isset($request->ModelName) &&  ($request->ModelName !=""))
		{
						$model = $request->ModelName;
					
					
					
					$MasterFileName = ($model);
					$table_model = array("\App\Http\Models" . chr(92). "emerald" . chr(92) .$MasterFileName);		
					$table_columns = new $table_model[0];
					/* $validateMstBrand =  $table_columns->where('name',strtolower($request->name))->count();		
							
					//echo $validateMstBrand;
					if 	($validateMstBrand > 0)
					{
						$return_msg = $return_msg  .  " Duplicate Entry " . $request->name ;
						$log_msg = $return_msg ;
						$masterfilelist =  $table_columns->where('status', '=' , 'enable')->get();
						//break;
					}
					
					else  */
					{
					
					$return_msg = $return_msg  .  $request->name  . " has successfully created" ;
					
				
					$masterfilelist = new $table_model[0];
					
							foreach($table_columns['fillable'] as $array_num => $table_column)
							{
								if(isset($request->$table_column))
									{
										$masterfilelist->$table_column = $request->$table_column;
									}
							}

							$masterfilelist->created_by = Auth::user()->id;
							$masterfilelist->updated_by = Auth::user()->id;
					
					
					$masterfilelist ->save();		
					$log_msg = $request->name  . " has successfully created "  . ucfirst($model) . " MasterFile" ;
					//$masterfilelist = MstBrand::where('status', '=' , 'enable')->get();//MstBrand::All();
					
					
					}
					
					$Logger = new AppHelper;
					$Logger->SaveLog($log_msg);
		
		}	
		
		return back()->with('return_msg', $log_msg);
		//return redirect('/bloodstone/masterfile/' . $mstlist)->with('MstBrand',$masterfilelist)->with('urllist', $mstlist)->with('return_msg', $return_msg);
		
	} 
	
	
	
	
	
	public function update(Request $request)
    {	
	
	
		$return_msg ="";
		$log_msg ="";
		$MasterFileName = $request->ModelName;
		$table_model = array("\App\Http\Models". chr(92). "emerald" . chr(92) .$MasterFileName);
		$table_columns = new $table_model[0];
		//$validateMstBrand =  $table_columns->where('name',strtolower($request->name))->count();		
				
		

		$masterfilelist = $table_columns::find($request->id);					
		
		
		
		//$masterfilelist = MstBrand::find($request->id);
		
				if($request->action == "edit")
				{
				$log_msg = $masterfilelist ->name . " has been change to " . $request->name . " from " . $request->name . " MasterFile " ;
				
				foreach($table_columns['fillable'] as $array_num => $table_column)
				{
					if(isset($request->$table_column) && ($request->$table_column != $masterfilelist->$table_column))
						{
							$log_msg .= $masterfilelist->$table_column . " has been change to " . $request->$table_column;
							$masterfilelist->$table_column = $request->$table_column;
							
						}
				}
				

				$masterfilelist ->updated_by = (Auth::user()->name);
				
				
				}
				else if($request->action == "delete")
				{
				$log_msg =  $request->name . " have been deleted from " . $request->name . " MasterFile" ;
				$masterfilelist ->updated_by = (Auth::user()->name);
				$masterfilelist ->status = "Disabled";
				}
				$masterfilelist->save();

		$Logger = new \AppHelper;
		$Logger->SaveLog($log_msg);
		
		return back()->with('return_msg', $log_msg);
		//return view('bloodstone.masterfile')->with('MstBrand',$masterfilelist)->with('urllist', $urllist);
		//return redirect('/bloodstone/masterfile/' . $mstlist)->with('MstBrand',$masterfilelist)->with('urllist', $mstlist)->with('return_msg', $log_msg);
		
	
		
		
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	

	

}