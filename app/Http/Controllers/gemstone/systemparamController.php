<?php namespace App\Http\Controllers\gemstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\peridot\MstSystemParameter;



class systemparamController extends Controller
{
 //$columns = new App\Http\Models\peridot\Mst_system_parameter;

	public function index(){
		
	
		$MstSystemParameter = MstSystemParameter::where('status', '=' , 'enable')->get();
		// $MstSystemParameter = new \App\Http\Models\peridot\MstSystemParameter;
   
	//var_dump($MstSystemParameter);
		return view('gemstone.system.index')->with('MstSystemParameter',$MstSystemParameter);

	}
	
	    public function store(Request $request)
		{
		
    	$MstSystemParameter = new \App\Http\Models\peridot\MstSystemParameter;
    	$MstSystemParameter ->parameter_key = strtolower($request->parameter_key);
    	$MstSystemParameter ->parameter_value = $request->parameter_value; 
    	$MstSystemParameter ->remarks = $request->remarks;
    	$MstSystemParameter ->save();
    	//return redirect()->route('product.index');
    	//return view('gemstone.system.index')->with('MstSystemParameter',$MstSystemParameter);
		$MstSystemParameter = MstSystemParameter::where('status', '=' , 'enable')->get();
		
    	return view('gemstone.system.index')->with('MstSystemParameter',$MstSystemParameter);
		//return redirect()->action('systemparamController@index');
		} 
		
		 public function update(Request $request)
    {	
		$MstSystemParameter = MstSystemParameter::find($request->id);
		if($request->action == "edit")
		{
    	
    	$MstSystemParameter ->parameter_key = $request->parameter_key;
    	$MstSystemParameter ->parameter_value = $request->parameter_value; 
    	$MstSystemParameter ->remarks = $request->remarks;
    	

		
		}
		else if($request->action == "delete")
		{
		
		$MstSystemParameter ->status = "Disabled";
		}
		$MstSystemParameter->save();
		$MstSystemParameter = MstSystemParameter::where('status', '=' , 'enable')->get();
    	return view('gemstone.system.index')->with('MstSystemParameter',$MstSystemParameter);
    }
	 
}