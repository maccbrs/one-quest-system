<?php namespace App\Http\Controllers\gemstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class inventoryController extends Controller
{

	public function index(){

		return view('gemstone.inventory.index');

	}

	public function costume_pc_index(){

		return view('gemstone.inventory.costume-pc-index');

	}
	
}