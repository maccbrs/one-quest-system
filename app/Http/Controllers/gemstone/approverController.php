<?php namespace App\Http\Controllers\gemstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Alert;

class approverController extends Controller
{


	public function index(){

		$GemUsers = new \App\Http\Models\amethyst\GemUsers;
		$GemApprover = new \App\Http\Models\amethyst\GemApprover;
		$help = new \App\Http\Controllers\amethyst\Helper;
		
		$items = $GemUsers
			->select('name','id')
			->where('active','=','1')
			->orderBy('name', 'asc')
			->get();	

		$approvers = $GemApprover->with(['emp','sup'])->get();
		//$help->pre($items);
		return view('gemstone.approver.index',compact('help','items','approvers'));	

	}

	public function create(Request $r){

		$GemApprover = new \App\Http\Models\amethyst\GemApprover;
		$data = $r->all();
		$data['added_by'] = Auth::user()->id;
		$GemApprover->create($data);
	

		Alert::message('Approver has been assigned!')->persistent('Close');

		return $this->index();

	}

} 