<?php namespace App\Http\Controllers\gemstone;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Alert;

class departmentController extends Controller
{

	public function index(){

		$Campaign = new \App\Http\Models\topaz\Campaign;
		$items =  $Campaign->get();
		return view('gemstone.department.index',compact('items'));

	}

	public function store(Request $r){

		$help = new \App\Http\Controllers\topaz\Helper;
		$Campaign = new \App\Http\Models\topaz\Campaign;
		$Campaign->create(['name' => $r->name]);

		Alert::message('Department has been Added')->persistent('Close');

		return $this->index();

	}

	public function show($id){

		$Campaign = new \App\Http\Models\topaz\Campaign;
		$item =  $Campaign->find($id);
		return view('gemstone.deparment.show',compact('item'));

	}

	public function users($id){ 

		$help = new \App\Http\Controllers\topaz\Helper;
		$GemUsers = new \App\Http\Models\topaz\GemUsers;
		$CampaignToUser = new \App\Http\Models\topaz\CampaignToUser;
		$Campaign = new \App\Http\Models\topaz\Campaign;
		$campaign =  $Campaign->find($id);		
		$exclude = [];
		$items = $CampaignToUser->where('campaign_id',$id)->with(['user'])->get();
		if($items->count()):
			foreach($items as $item):
				$exclude[] = $item->user_id;
			endforeach;
		endif;

		$users = $GemUsers->whereNotIn('id',$exclude)->get();
		//$help->pre($items);
		return view('gemstone.department.users',compact('items','users','id','campaign'));

	}

	public function user_store(Request $r){

		$help = new \App\Http\Controllers\topaz\Helper;
		$CampaignToUser = new \App\Http\Models\topaz\CampaignToUser;
		$CampaignToUser->create($r->all());
		Alert::message('User has been Added')->persistent('Close');

		return $this->users();

	}

	public function admin($id){

		$help = new \App\Http\Controllers\topaz\Helper;
		$Campaign = new \App\Http\Models\topaz\Campaign;
		$campaign =  $Campaign->find($id);
		$GemUsers = new \App\Http\Models\topaz\GemUsers;
		$exclude = [];
		
		$CampaignToAdmin = new \App\Http\Models\topaz\CampaignToAdmin;
		$items = $CampaignToAdmin->where('campaign_id',$id)->with(['user'])->get();
		if($items->count()):
			foreach($items as $item):
				$exclude[] = $item->user_id;
			endforeach;
		endif;

		$users = $GemUsers->whereNotIn('id',$exclude)->get();		
		return view('gemstone.department.admin',compact('campaign','users','id','items'));

	}

	public function admin_store(Request $r){

		$help = new \App\Http\Controllers\topaz\Helper;
		$CampaignToAdmin = new \App\Http\Models\topaz\CampaignToAdmin;
		$CampaignToAdmin->create($r->all()); 

		Alert::message('Admin has been Added!')->persistent('Close');

		return $this->admin();

	}

	public function tickets($id){
//echo Carbon::now();die;
		$help = new \App\Http\Controllers\topaz\Helper;
		$Ticket = new \App\Http\Models\topaz\Ticket;
		$CampaignToAdmin = new \App\Http\Models\topaz\CampaignToAdmin;
		$users = $CampaignToAdmin->where('campaign_id',$id)->with(['user'])->get();
		//$help->pre($users);
		$admins = $help->admin_ids($users);
		$items = $Ticket->where('campaign_id',$id)->where('status','!=','closed')->with(['reply','user'])->get();
		//echo date('Y-m-d H:i:s');die;
		//$help->pre($items);
		return view('gemstone.department.tickets',compact('items','help','admins','users'));

	}

	public function ticket($id){
		$help = new \App\Http\Controllers\topaz\Helper;
		$Ticket = new \App\Http\Models\topaz\Ticket;
		$item = $Ticket->with(['reply','user'])->find($id);

		return view('gemstone.department.ticket',compact('item','help'));		
	}

	public function status(Request $r){

		$help = new \App\Http\Controllers\topaz\Helper;
		$Point = new \App\Http\Models\topaz\Point;
		$Ticket = new \App\Http\Models\topaz\Ticket;
		$item = $Point->firstOrCreate(['ticket_id' => $r->id]);
		$ticket = $Ticket->find($r->id)->update(['point_id' => $item->id,'status' => $r->content]);
		$help->pre($ticket);
	}

	public function assign(Request $r){

		$help = new \App\Http\Controllers\topaz\Helper;
		$Ticket = new \App\Http\Models\topaz\Ticket;
		$item = $Ticket->find($r->ticket_id);
		if($item):
			$item->assigned_id = $r->user_id;
			$item->save();
		endif;
		echo 'hello';
	}

}