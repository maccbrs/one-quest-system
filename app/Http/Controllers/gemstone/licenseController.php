<?php namespace App\Http\Controllers\gemstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class licenseController extends Controller
{

	public function index(){

		$License = new \App\Http\Models\gemstone\License;
		$Options = new \App\Http\Models\gemstone\Options;
		$Help = new \App\Http\Controllers\gemstone\Helper;
		$options = $Options->where('model','licenses')->get();		
		$items = $License->orderBy('created_at','desc')->paginate(20);
		return view('gemstone.license.index',compact('items','Help','options'));

	}

	public function add(){
		$Options = new \App\Http\Models\gemstone\Options;
		$Help = new \App\Http\Controllers\gemstone\Helper;
		$options = $Options->where('model','licenses')->get();
		return view('gemstone.license.add',compact('Help','options'));
	} 

	public function assign(Request $r,$id){

		$Help = new \App\Http\Controllers\gemstone\Helper;
		$AssetToLicense = new \App\Http\Models\gemstone\AssetToLicense;
		$License = new \App\Http\Models\gemstone\License;
		$arr = explode('_', $r->license);

		$license = $License->find($arr[0]);

		if($license):
			$input['licenses_id'] = $arr[0];
			$input['license'] = $arr[1];
			$input['assets_id'] = $id;
			$input['user_id'] = Auth::user()->id;
			$AssetToLicense->create($input);

			$license->remaining_seats--;
			$license->save();
		endif;

		return redirect()->back();

	}

	public function reassign(Request $r,$id){

		$Help = new \App\Http\Controllers\gemstone\Helper;
		$AssetToLicense = new \App\Http\Models\gemstone\AssetToLicense;
		$License = new \App\Http\Models\gemstone\License;
		$atl = $AssetToLicense->find($id);

		if($atl):

			$assets_id = $atl->assets_id;

			$oldlicense = $License->find($atl->licenses_id);
			if($oldlicense):
				$oldlicense->remaining_seats++;
				$oldlicense->save();
				$atl->delete();
			endif;

			$arr = explode('_', $r->license);
			$newlicense = $License->find($arr[0]);
			if($newlicense):
				$input['licenses_id'] = $arr[0];
				$input['license'] = $arr[1];
				$input['assets_id'] = $assets_id;
				$input['user_id'] = Auth::user()->id;
				$AssetToLicense->create($input);

				$newlicense->remaining_seats--;
				$newlicense->save();
			endif;
		endif;

		return redirect()->back();

	}


	public function view($id){

		$License = new \App\Http\Models\gemstone\License;
		$item = $License->find($id);
		return view('gemstone.license.view',compact('item'));

	}	

	public function group($field,$value){

		$License = new \App\Http\Models\gemstone\License;
		$items = $License->where($field,$value)->orderBy('created_at','desc')->paginate(20);
		return view('gemstone.license.group',compact('items','field','value'));

	}	

	public function create(Request $r){

		$License = new \App\Http\Models\gemstone\License;
		$Help = new \App\Http\Controllers\gemstone\Helper;
		$input = $r->all();
		$input['remaining_seats'] = $r->seats;
		$License->create($input);
		return redirect()->route('gemstone.license.index');

	}

	public function create_options(Request $r,$opt){

		$help = new \App\Http\Controllers\gemstone\Helper;
		$Options = new \App\Http\Models\gemstone\Options;
		$input = $r->all();
		if(isset($input[$opt])):

			$item = $Options->firstOrCreate(['type' => $opt,'model' => 'licenses']);

			if($item->lists):
				$a = json_decode($item->lists,true);
				$a[] = $input[$opt];
				$item->lists = json_encode($a);
			else:
				$item->lists = json_encode([$input[$opt]]);
			endif;

			$item->save();

		endif;
		
		return redirect()->back();

	}

}