<?php namespace App\Http\Controllers\gemstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

use Auth;

class bioController extends Controller
{


	public function all(){

		$Bio = new \App\Http\Models\gemstone\Bio;
		$items = $Bio->orderBy('id','desc')->paginate(10);
		return view('gemstone.bio.all',compact('items'));

	}

	public function create(Request $r){

	    $this->validate($r, [
	        'name' => 'required|max:50',
	        'detail' => 'required|max:20',
	    ]);
		$helper = new \App\Http\Controllers\gemstone\Helper;
		$Bio = new \App\Http\Models\gemstone\Bio;
		$Bio->create($r->only(['name','detail']));
		return redirect()->back();

	}

	public function check(){

		$helper = new \App\Http\Controllers\gemstone\Helper;
		$BioChecklist = new \App\Http\Models\gemstone\BioChecklist;
		$carbon = new \Carbon\Carbon;
		$checklist = $BioChecklist->where('is_done',0)->with('bio_statuses')->first();
		//$helper->pre($checklist->toArray());
		return view('gemstone.bio.check',compact('checklist','carbon'));

	}

	public function check_status($status){

		$helper = new \App\Http\Controllers\gemstone\Helper;

		if($status =='new'):

			$BioChecklist = new \App\Http\Models\gemstone\BioChecklist;
			$BioStatuses = new \App\Http\Models\gemstone\BioStatuses;
			$Bio = new \App\Http\Models\gemstone\Bio;
			$ccl = $BioChecklist->create(['is_done' => 0]);

			$bios = $Bio->get();
			$temp = [];

			foreach($bios as $bio):
				$temp[] = [
					'bio_checklist_id' => $ccl->id,
					'bio_id' => $bio->id,
					'status' => 1
				];
			endforeach;
			$BioStatuses->insert($temp);
		endif;

		return redirect()->back();
		
		
	}

	public function update_status(Request $r){

		$helper = new \App\Http\Controllers\gemstone\Helper;
		$BioStatuses = new \App\Http\Models\gemstone\BioStatuses;
		$status = ($r->status=='true'?1:0);
		$BioStatuses->find($r->id)->update(['status' => $status]);
		
	}

	public function submit_checklist(Request $r){

		$BioChecklist = new \App\Http\Models\gemstone\BioChecklist;
		$BioChecklist->find($r->id)->update(['is_done' => 1,'user_id' => Auth::user()->id]);
		return redirect()->back();

	}

	public function results(){

		$helper = new \App\Http\Controllers\gemstone\Helper;
		$BioChecklist = new \App\Http\Models\gemstone\BioChecklist;
		$carbon = new \Carbon\Carbon;
		$items = $BioChecklist->with(['offline','online'])->paginate(20);
		//$helper->pre($items->toArray());
		return view('gemstone.bio.results',compact('items','carbon'));

	}

	public function result($chkid,$stats){

		$BioStatuses = new \App\Http\Models\gemstone\BioStatuses;
		$helper = new \App\Http\Controllers\gemstone\Helper;
		$carbon = new \Carbon\Carbon;
		switch ($stats):
			case 'offline':
				$items = $BioStatuses->where('bio_checklist_id',$chkid)->where('status',0)->with(['bio','checklist'])->paginate(20);
				break;
			case 'online':
				$items = $BioStatuses->where('bio_checklist_id',$chkid)->where('status',1)->with(['bio','checklist'])->paginate(20);
				break;			
			default:
				$items = $BioStatuses->where('bio_checklist_id',$chkid)->with(['bio','checklist'])->paginate(20);
				break;
		endswitch;
		//$helper->pre($items->toArray());
		return view('gemstone.bio.result',compact('items','carbon'));

	}
	
}