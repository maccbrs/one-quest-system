<?php namespace App\Http\Controllers\gemstone;
use Auth;


class Helper 
{


	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}

	public function get_gravatar($email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array()){

	    $url = 'https://www.gravatar.com/avatar/';
	    $url .= md5( strtolower( trim( $email ) ) );
	    $url .= "?s=$s&d=$d&r=$r";
	    if ( $img ) {
	        $url = '<img src="' . $url . '"';
	        foreach ( $atts as $key => $val )
	            $url .= ' ' . $key . '="' . $val . '"';
	        $url .= ' />';
	    }
	    return $url;
	    
	}

	public function sortByDate($obj){
		
		$arr = [];
		foreach($obj as $k => $v):
			$arr[$v->created] = $v;
		endforeach;
		$this->pre($arr);
		
	}	

	public function access($str,$x){

		if($str == '') return false;
		$arr = json_decode($str,true);
		if(in_array($x, $arr)){
			return true;
		}else{
			return false;
		}

	}


	public function userType(){
		switch (Auth::user()->user_type):
			case 'operation':
					switch(Auth::user()->user_level):
						case 1:
							echo 'hi';
						case 2: 
							return 'tl';
						break;
					endswitch;
				break;
			case 'reportanalyst':
					return 'ra';
				break;
			case 'hr':
					return 'hr';
				break;
			default:
				return 'user';
		endswitch;	
		return false;	
	}


	public function fetchComments($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['notes'];
		endif;
	}

	public function fetchUser($str,$users){
		if($str != ''):
			$obj = json_decode($str,true);
			return $users[$obj['approver']];
		endif;
	}

	public function fetchDate($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['date'];
		endif;
	}

	public function fetchStatus($int){
		switch ($int):
			case 0:
				return 'na';
				break;
			case 1:
				return 'pending';
				break;
			case 2:
				return 'approved';
				break;
			case 3:
				return 'rejected';
				break;								
		endswitch;
	}

	public function mbdate(){
		return date('Y-m-d H:i:s');
	}

	public function onyx_dates($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->dispute_label;
			endforeach;
			return $arr;
		endif;

		return false;

	}


	public function onyx_issues($obj){

		if($obj->count()):

			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->issue_name;
			endforeach;
			return $arr;

		endif;

		return false;

	} 

	public function subs(){

		$Hierarchy = new \App\Http\Models\gem\Hierarchy;
		$idsObj = $Hierarchy->subs()->get();
		
		if($idsObj->count()):
			$ids = [];
			foreach($idsObj as $i):
				$ids[] = $i->level2;
			endforeach;
			return $ids;
		endif;

		return false;

	}

	public function opened($a){

		$GarnetOpened = new \App\Http\Models\gem\GarnetOpened;
		$items = $GarnetOpened->self()->get();
		$opened = false;

		if($items->count()):
			foreach($items as $k => $v):
				$opened[] = $v->announcements_id;
			endforeach;
		endif;

		$return = [];

		if($a->count()):
			foreach($a as $k => $v):
			//	print_r('<pre>');print_r($return[$v->source]['read']);print_r('</pre>'); exit;
				if($opened):
					if(in_array($v->id, $opened)):
						$return[$v->source]['read'] = (isset($return[$v->source]['read'])?$return[$v->source]['read'] + 1:$return[$v->source]['read'] = 1);
					else:
						$return[$v->source]['unread'] = (isset($return[$v->source]['unread'])?$return[$v->source]['unread'] + 1:$return[$v->source]['unread'] = 1);
					endif;
				else:
					$return[$v->source]['unread'] = (isset($return[$v->source]['unread'])?$return[$v->source]['unread'] + 1:$return[$v->source]['unread'] = 1);
				endif;
			endforeach;
		endif;
		return $return;

	}

	public function print_status($a,$source,$status){

		if($a):
			if(isset($a[$source]) && isset($a[$source][$status])):
				return $a[$source][$status];
			endif;
		endif;

		return 0;

	}

	public function user_type(){

	//	return (Auth::user()->user_type?Auth::user()->user_type:'user');
		$a = [
			'administrator' => 'noc',
			'hr' => 'hrd'
		];
		$b = Auth::user()->user_type;

		if(isset($a[$b])):
			return $a[$b];
		endif;

		return 'user';
	}


	public function rejected($a,$b){
		if($a == 3||$b == 3):
			return true;
		endif;
			return false;
	}

	public function tls(){

		$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
		$items = $GarnetTeamLeads->get();
		$data = [];
		foreach($items as $item):
			$data[$item->id] = $item;
		endforeach;
		return $data;
	}

	public function ps(){

		$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
		$items = $GarnetProjectSupervisors->get();
		$data = [];
		foreach($items as $item):
			$data[$item->id] = $item;
		endforeach;
		return $data;		
		
	}

	public function opened_message($a){

		$GarnetOpened = new \App\Http\Models\amber\GarnetMessageOpened;
		$items = $GarnetOpened->self()->get();


		$opened = false;

		if($items->count()):
			foreach($items as $k => $v):
				$opened[] = $v->message_id;
			endforeach;
		endif;

		$return = [];

		if($a->count()):
			foreach($a as $k => $v):
				if($opened):
					
					if(in_array($v->id, $opened)):
						
						$return['read'] = (isset($return['read'])?$return['read'] + 1:$return['read'] = 1);
					else:
					
						$return['unread'] = (isset($return['unread'])?$return['unread'] + 1:$return['unread'] = 1);
					endif;
				else:
					
					$return['unread'] = (isset($return['unread'])?$return['unread'] + 1:$return['unread'] = 1);
				endif;
			endforeach;
		endif;

		return $return;

	}
	public function print_status_message($a,$source,$status){
		
		if($a):
			if(isset($a) && isset($a[$status])):
				
				return $a[$status];
			endif;
		endif;

		return 0;

	}

	public function convert_to_key_case($data, $case){

        $condition = ($case == 'lowercase')? 'lowercase' : 'uppercase';
        $test = [];
        foreach($data as  $key => $value) {
            if ($condition == 'lowercase') {
                $test[strtolower($key)] = strtoupper($value);
            } else {
                $test[strtoupper($key)] = strtoupper($value);
            }
        }
        
    return $test;
}

	public function print_options($type,$lists){
		$item = false;
		foreach($lists as $list):
			if($list->type == $type):
				$item = $list;
			endif;
		endforeach;
			$a = json_decode($item->lists,true);
			$a = $this->convert_to_key_case($a, 'uppercase');

			$a = array_unique($a);

		if(!$item):

			echo "<p>No options available,create new</p>";
		else:

       		echo "<select id='".$type."' name='".$type."' class='form-control' size='1'><option value=''>Please select</option>";
           
           if(!empty($a)):

           		foreach($a as $b):
           			echo "<option value='".$b."'>".$b."</option>";
           		endforeach;
           endif;
        echo "</select>";
        endif;

	}

	public function print_options2($type,$lists,$title){

		$item = false;
		foreach($lists as $list):
			if($list->type == $type):
				$item = $list;
			endif;
		endforeach;

		if(!$item):
			echo "<p>No options available,create new</p>";
		else:
			echo "<span class='dropdown'>
			    <a href='javascript:void(0)' class='dropdown-toggle' data-toggle='dropdown'>
			       ".$title."
			    </a>
			    <ul class='dropdown-menu'>";
           $a = json_decode($item->lists,true);
           if(!empty($a)):
           		foreach($a as $b):
           			echo "<li><a href='".route('gemstone.asset.group',['field' => $type,'value' => $b])."'>".$b."</a></li>";
           		endforeach;
           endif;
        echo "</ul></span>";
        endif;

	}

	public function proper_title($str){
		return ucwords(str_replace(['_'], " ", $str));
	}


}  


        
