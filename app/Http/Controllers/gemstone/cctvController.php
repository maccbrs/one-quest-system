<?php namespace App\Http\Controllers\gemstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

use Auth;

class cctvController extends Controller
{


	public function all(){

		$Cctv = new \App\Http\Models\gemstone\Cctv;
		$items = $Cctv->orderBy('id','desc')->paginate(10);
		return view('gemstone.cctv.all',compact('items'));

	}

	public function create(Request $r){

	    $this->validate($r, [
	        'name' => 'required|max:50',
	        'ip' => 'required|max:20',
	    ]);
		$helper = new \App\Http\Controllers\gemstone\Helper;
		$Cctv = new \App\Http\Models\gemstone\Cctv;
		$Cctv->create($r->only(['name','ip']));
		return redirect()->back();

	}

	public function check(){

		$helper = new \App\Http\Controllers\gemstone\Helper;
		$CctvChecklist = new \App\Http\Models\gemstone\CctvChecklist;
		$carbon = new \Carbon\Carbon;
		$checklist = $CctvChecklist->where('is_done',0)->with('cctv_statuses')->first();
		//$helper->pre($checklist->toArray());
		return view('gemstone.cctv.check',compact('checklist','carbon'));

	}

	public function check_status($status){

		$helper = new \App\Http\Controllers\gemstone\Helper;

		if($status =='new'):

			$CctvChecklist = new \App\Http\Models\gemstone\CctvChecklist;
			$CctvStatuses = new \App\Http\Models\gemstone\CctvStatuses;
			$Cctv = new \App\Http\Models\gemstone\Cctv;
			$ccl = $CctvChecklist->create(['is_done' => 0]);

			$cctvs = $Cctv->get();
			$temp = [];

			foreach($cctvs as $cctv):
				$temp[] = [
					'cctv_checklist_id' => $ccl->id,
					'cctv_id' => $cctv->id,
					'status' => 1
				];
			endforeach;
			$CctvStatuses->insert($temp);
		endif;

		return redirect()->back();
		
		
	}

	public function update_status(Request $r){

		$helper = new \App\Http\Controllers\gemstone\Helper;
		$CctvStatuses = new \App\Http\Models\gemstone\CctvStatuses;
		$status = ($r->status=='true'?1:0);
		$CctvStatuses->find($r->id)->update(['status' => $status]);
		
	}

	public function submit_checklist(Request $r){

		$CctvChecklist = new \App\Http\Models\gemstone\CctvChecklist;
		$CctvChecklist->find($r->id)->update(['is_done' => 1,'user_id' => Auth::user()->id]);
		return redirect()->back();

	}

	public function results(){

		$helper = new \App\Http\Controllers\gemstone\Helper;
		$CctvChecklist = new \App\Http\Models\gemstone\CctvChecklist;
		$carbon = new \Carbon\Carbon;
		$items = $CctvChecklist->with(['offline','online'])->paginate(20);
		//$helper->pre($items->toArray());
		return view('gemstone.cctv.results',compact('items','carbon'));

	}

	public function result($chkid,$stats){

		$CctvStatuses = new \App\Http\Models\gemstone\CctvStatuses;
		$helper = new \App\Http\Controllers\gemstone\Helper;
		$carbon = new \Carbon\Carbon;
		switch ($stats):
			case 'offline':
				$items = $CctvStatuses->where('cctv_checklist_id',$chkid)->where('status',0)->with(['cctv','checklist'])->paginate(20);
				break;
			case 'online':
				$items = $CctvStatuses->where('cctv_checklist_id',$chkid)->where('status',1)->with(['cctv','checklist'])->paginate(20);
				break;			
			default:
				$items = $CctvStatuses->where('cctv_checklist_id',$chkid)->with(['cctv','checklist'])->paginate(20);
				break;
		endswitch;
		//$helper->pre($items->toArray());
		return view('gemstone.cctv.result',compact('items','carbon'));

	}
	
}