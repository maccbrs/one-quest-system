<?php namespace App\Http\Controllers\gemstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class assetController extends Controller
{

	public function index(){

		$Assets = new \App\Http\Models\gemstone\Assets;
		$Options = new \App\Http\Models\gemstone\Options;
		$Help = new \App\Http\Controllers\gemstone\Helper;
		$options = $Options->where('model','assets')->get();		
		$items = $Assets->orderBy('created_at','desc')->paginate(20);
		return view('gemstone.asset.index',compact('items','Help','options')); 

	}

	public function view($id){

		$Help = new \App\Http\Controllers\gemstone\Helper;
		$License = new \App\Http\Models\gemstone\License;
		$licenses = $License->where('remaining_seats','>',0)->get();
		$Assets = new \App\Http\Models\gemstone\Assets;
		$item = $Assets->with(['ObjLicense'])->find($id);
		return view('gemstone.asset.view',compact('item','licenses'));

	}


	public function group($field,$value){

		$Assets = new \App\Http\Models\gemstone\Assets;
		$Help = new \App\Http\Controllers\gemstone\Helper;
		$Options = new \App\Http\Models\gemstone\Options;
		$options = $Options->where('model','assets')->get();
		$items = $Assets->where($field,$value)->orderBy('created_at','desc')->paginate(20);
		return view('gemstone.asset.group',compact('items','field','value','Help','options'));

	}

	public function add(){

		$Options = new \App\Http\Models\gemstone\Options;
		$Help = new \App\Http\Controllers\gemstone\Helper;
		$options = $Options->where('model','assets')->get();
		//$Help->pre($options);		
		return view('gemstone.asset.add',compact('Help','options'));

	}	

	public function create(Request $r){

		$Assets = new \App\Http\Models\gemstone\Assets;
		$Help = new \App\Http\Controllers\gemstone\Helper;
		$Assets->create($r->all());
		return redirect()->route('gemstone.asset.index');

	}

	public function create_options(Request $r,$opt){

		$help = new \App\Http\Controllers\gemstone\Helper;
		$Options = new \App\Http\Models\gemstone\Options;
		$input = $r->all();

		if(isset($input[$opt])):

			$item = $Options->firstOrCreate(['type' => $opt,'model' => 'assets']);

			if($item->lists):
				$a = json_decode($item->lists,true);
				$a[] = $input[$opt];

				// if (in_array($input['status'], $a)) {
				//     echo "Got mac";
				// }

				//print_r('<pre>');print_r($input);print_r('</pre>'); exit;
				$item->lists = json_encode($a);
			else:
				$item->lists = json_encode([$input[$opt]]);
			endif;

			//print_r('<pre>');print_r($item);print_r('</pre>'); exit;

			$item->save();

		endif;
		
		return redirect()->back();

	}

}