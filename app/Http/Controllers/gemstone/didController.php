<?php namespace App\Http\Controllers\gemstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;

class didController extends Controller
{

	public function index(){

		$GemDids = new \App\Http\Models\gemstone\GemDids;
		$help = new \App\Http\Controllers\gem\Helper;
		$items = $GemDids->where('status',1)->with(['last_test_call'])->get();
		//$help->pre($items->toArray());
		return view('gemstone.did.index',compact('items'));
	}

	public function create(Request $r){

		$this->validate($r, [
		    'phone' => 'required'
		]);	

		$GemDids = new \App\Http\Models\gemstone\GemDids;
		$help = new \App\Http\Controllers\gem\Helper;
		$data = $r->all();
		$data['added_by'] = Auth::user()->id;
		$GemDids->create($data);
		return redirect()->back();

	}

	public function testcalls(){

		$GemTestCalls = new \App\Http\Models\gemstone\GemTestCalls;
		$help = new \App\Http\Controllers\gem\Helper;
		$items =  $GemTestCalls->orderBy('created_at','desc')->paginate(20);
		//$help->pre($items->toArray());
		return view('gemstone.did.testcalls',compact('items'));
	} 

	public function create_tescalls(Request $r){

		$dt = Carbon::now();
		$dt->setTimezone('Asia/Manila');
		$help = new \App\Http\Controllers\gemstone\Helper;

		$GemTestCalls = new \App\Http\Models\gemstone\GemTestCalls;
		$data = [
			'created_at' => $dt->format('Y-m-d H:i:00'),
			'updated_at' => $dt->format('Y-m-d H:i:00')
		]; 

		$a = $GemTestCalls->create($data);

		if($a):
			$GemDids = new \App\Http\Models\gemstone\GemDids;
			$GemTestCallItems = new \App\Http\Models\gemstone\GemTestCallItems;
			$dids = $GemDids->where('status',1)->get();
			$b = [];
			$total = 0;
			foreach($dids as $did):
				$b[] = [
					'test_calls_id' => $a->id,
					'dids_id' => $did->id,
					'status' => 'failed'
				];
				$total++;
			endforeach;
			$GemTestCallItems->insert($b);
			$a->total = $total;
			$a->failed = $total;
			$a->save();
			return redirect()->route('gemstone.did.testcall',$a->id);

		endif;

		return redirect()->back();
		
	}

	public function testcall($id){

		$GemTestCallItems = new \App\Http\Models\gemstone\GemTestCallItems;
		$valid = false;


		$items = $GemTestCallItems->where('test_calls_id',$id)->with('obj_did','obj_testcall')->paginate(20);
		return view('gemstone.did.testcall',compact('items','valid','id')); 

	}	

	public function mbvalid($id){

		$GemTestCallItems = new \App\Http\Models\gemstone\GemTestCallItems;
		$ViciCallLog = new \App\Http\Models\gemstone\ViciCallLog;
		$GemTestCalls = new \App\Http\Models\gemstone\GemTestCalls;
		$items = $GemTestCallItems->where('test_calls_id',$id)->with('obj_did','obj_testcall')->get();
		$help = new \App\Http\Controllers\gem\Helper;
		$help->pre($items->toArray());
		$a = $GemTestCalls->find($id);
		$calllogs = $ViciCallLog->where('caller_code','nocmarlon')->whereBetween('end_time',[$a->created_at->subMinutes(5)->format('Y-m-d H:i:s'),$a->created_at->addHours(20)->format('Y-m-d H:i:s')])->get();
		$phones = [];

		foreach($calllogs as $log):
			$phones[$log->number_dialed] = $log->end_time;
		endforeach;

		foreach($items as $item):
			if(array_key_exists($item->obj_did->phone, $phones)):
				$item->status = 'passed';
				$item->date = $phones[$item->obj_did->phone];
				$item->save();
			endif;
		endforeach;
		
		return redirect()->back();
		
	}

	public function mbcheckupdate(Request $r){

		$help = new \App\Http\Controllers\gem\Helper;
		$GemTestCallItems = new \App\Http\Models\gemstone\GemTestCallItems;
		$item = $GemTestCallItems->find($r->id);
		if($item):
			if($item->checked):
				$item->checked = 0;
			else:
				$item->checked = 1;
			endif;
			$item->save();
		endif;

	}

	public function notes(Request $r,$id){

		$help = new \App\Http\Controllers\gem\Helper;
		$GemTestCallItems = new \App\Http\Models\gemstone\GemTestCallItems;
		$item = $GemTestCallItems->find($r->id);
		if($item):
			$item->notes = $r->notes;
			$item->save();
		endif;
		return redirect()->back();

	}

	public function passed($id){

		$GemTestCallItems = new \App\Http\Models\gemstone\GemTestCallItems;
		$valid = false;
		$items = $GemTestCallItems->where('test_calls_id',$id)->where('status','passed')->with('obj_did','obj_testcall')->paginate(20);
		return view('gemstone.did.testcall',compact('items','valid','id')); 

	}
	
	public function failed($id){
		$GemTestCallItems = new \App\Http\Models\gemstone\GemTestCallItems;
		$valid = false;


		$items = $GemTestCallItems->where('test_calls_id',$id)->where('status','failed')->with('obj_did','obj_testcall')->paginate(20);
		return view('gemstone.did.testcall',compact('items','valid','id')); 	
	}

	public function dids($id){

		$GemTestCallItems = new \App\Http\Models\gemstone\GemTestCallItems;
		$valid = false;
		$items = $GemTestCallItems->where('dids_id',$id)->with('obj_did','obj_testcall')->paginate(20);
		return view('gemstone.did.testcall',compact('items','valid','id')); 

	}	

	public function destroy($id){

		$GemDids = new \App\Http\Models\gemstone\GemDids;
		$help = new \App\Http\Controllers\gem\Helper;
		$i = $GemDids->find($id)->update(['status' => 0]);
		return redirect()->back();

	}


}

