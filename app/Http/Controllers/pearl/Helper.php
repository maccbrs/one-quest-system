<?php namespace App\Http\Controllers\pearl;



class Helper 
{

	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}


	public function sortByDate($obj){
		
		$arr = [];
		foreach($obj as $k => $v):
			$arr[$v->created] = $v;
		endforeach;
		$this->pre($arr);
		
	}	
	
	public function mb_timereformat($date){

		$o = preg_split( "/(\/|\s|:)/",trim($date));
		if($o[6] == 'PM' && $o[3] < 12) $o[3] += 12;
		return $o[2].'-'.$o[0].'-'.$o[1].' '.$o[3].':'.$o[4].':'.$o[5];
		
	}	

	public function tblFormat($rows){
		//$this->pre($rows);
		if(!$rows->count()) return false;

		$head=[];
		$row = $rows[0]->toArray();
		$contents = [];
		foreach ($row as $k => $v):
			$head[0][] = $this->headers($k);
		endforeach;
		foreach($rows as $item):
			$contents[] = $item->toArray();
		endforeach;
		return array_merge($head,$contents);

	}

	public function tblFormat2($rows){

		if(!$rows) return [];
		$head=[];
		$row = $rows[0];
		$contents = [];
		foreach ($row as $k => $v):
			$head[0][] = $this->headers($k);
		endforeach;
		foreach($rows as $item):
			$contents[] = $item;
		endforeach;
		return array_merge($head,$contents);

	}

	public function headers($item){
		$arr = [
			'campaign_id' => 'Campaign Id',
			'call_date' => 'Call Date',
			'length_in_sec' => 'Seconds',
			'status' => 'Disposition',
			'phone_number' => 'Phone Number',
			'user' => 'Agent',
			'queue_seconds' => 'Queue Seconds',
			'number_dialed' => 'Dialed Number',
			'createdAt' => 'Call Date'
		];
		if(isset($arr[$item])) return $arr[$item];
		return $item;
	}

	public function filterItems($rows){

		$dispo = [];
		foreach($rows as $k => $items):
			foreach($items->toArray() as $a => $b):
				if($a == 'status'):
					if(!in_array($b,$dispo)) $dispo[] = $b;
				endif;
			endforeach;
		endforeach;
		return [
			'dispo' => $dispo
		];
	}
	public function filterItems2($rows){

		$dispo = [];
		foreach($rows as $k => $items):
			foreach($items as $a => $b):
				if($a == 'status'):
					if(!in_array($b,$dispo)) $dispo[] = $b;
				endif;
			endforeach;
		endforeach;
		return [
			'dispo' => $dispo
		];
	}

	public function filterOut($rows,$items,$type){

		$res = [];
		switch ($type) {
			case 'dispo':
				foreach ($rows as $v):
					if(!in_array($v['status'],$items)):
						$res[] = $v;
					endif;
				endforeach;
				break;
		}
		return $res;

	}


}