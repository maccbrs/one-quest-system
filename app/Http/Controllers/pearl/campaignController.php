<?php namespace App\Http\Controllers\pearl;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class campaignController extends Controller
{

	public function __construct(){

        $this->middleware('pearl');

    }

	public function get($id){
		$h = new \App\Http\Controllers\gem\Helper;
		//$h->pre(Auth::user()->toArray());
		$Campaign = new \App\Http\Models\pearl\Campaigns;
		return view('pearl.campaign.get',compact('id'));

	}

}