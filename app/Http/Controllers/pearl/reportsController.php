<?php namespace App\Http\Controllers\pearl;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class reportsController extends Controller
{

	public function __construct(){

        $this->middleware('pearl');

    }

	public function get(Request $r, $id){

		$Campaign = new \App\Http\Models\pearl\Campaigns;
		$carbon = new \Carbon\Carbon;
		$help = new \App\Http\Controllers\pearl\Helper;
		$campaign = $Campaign->find($id);
		$data = false;
		$fr = $to = $result = $cached = false;
		$filters = [];

		if($r->all()): 

			// $Content = new \App\Http\Models\gardenia\Content;
			// $Campaign = new \App\Http\Models\gardenia\Campaigns;
			$Cached = new \App\Http\Models\pearl\CachedReports;
			// $campaign = $Campaign->where('campaign_id','CENTMORT')->first();
			// $fields = $help->getEnabled($campaign);		
			 $dates = explode('-', $r->daterange);
			 $fr = $help->mb_timereformat($dates[0]);
			 $to = $help->mb_timereformat($dates[1]);
			 $ingroup = $campaign->ingroups;

			 switch ($r->bound):
			 	case 'in':
			 			$Closer = new \App\Http\Models\pearl\Closers;
			 			$rows = $Closer->select('call_date','length_in_sec','status','phone_number','user','queue_seconds')->where('campaign_id',$ingroup)->whereBetween('call_date', [$fr, $to])->get();
			 			$data = $help->tblFormat($rows);
			 			$filters = $help->filterItems($rows);
			 		break;
			 	case 'out':
			 			$Outbounds = new \App\Http\Models\pearl\Outbounds;
			 			$rows = $Outbounds->select('call_date','length_in_sec','status','phone_number','user')->where('campaign_id',$ingroup)->whereBetween('call_date', [$fr, $to])->get();
			 			$data = $help->tblFormat($rows);
			 			$filters = $help->filterItems($rows);			 			
			 		break;
			 	case 'trans':
			 		$CallLogs = new \App\Http\Models\pearl\CallLogs;
			 		$rows = $CallLogs->select('length_in_sec','number_dialed','createdAt')->where('number_dialed','like',$campaign->prefix.'%')->whereBetween('createdAt', [$fr, $to])->get();
		 			$data = $help->tblFormat($rows);	 		
			 		break;			 					 		
			 endswitch;

			 $items = [];
			 $result = true;

			// foreach ($rows as $row):
			// 	$items[] = $help->printFields($fields,$row); 
			// endforeach;
			// $data = $help->tbl_format($items);

			$cached = $Cached->updateOrCreate(['campaign_id' => $id],['content' => json_encode($rows),'from' => $fr,'to' => $to,'filters' => json_encode($filters)]);
			// $result = true;

		endif;

		return view('pearl.reports.get',compact('campaign','data','fr','to','help','result','cached','filters'));

	}

	public function filter(Request $r,$id){

		if(!$r->all()) return redirect()->back();

		$help = new \App\Http\Controllers\pearl\Helper;
		$filters = [];
		$result = true;
		$campaign = false;

		$Cached = new \App\Http\Models\pearl\CachedReports;
		$cached = $Cached->find($id);
		$fr = $cached->from;
		$to = $cached->to;

		
		$rows = json_decode($cached->content,true);	

		if($r->dispo) $rows = $help->filterOut($rows,$r->dispo,'dispo');	

		$filters = $help->filterItems2($rows);

		$data = $help->tblFormat2($rows);

		$cached = $Cached->updateOrCreate(['id' => $id],['content' => json_encode($rows),'filters' => json_encode($filters)]);
		

		return view('pearl.reports.get',compact('campaign','data','fr','to','help','result','cached','filters'));

	}

	public function excel($id){

		$help = new \App\Http\Controllers\pearl\Helper;
		$Cached = new \App\Http\Models\pearl\CachedReports;
		$cached = $Cached->find($id);
		if(!$cached->count()) return redirect()->back();
		$rows = json_decode($cached->content,true);
		$items = $help->tblFormat2($rows);

		$Excel = new \Excel;
		$fr = str_replace(array(':',' '), '-',$cached->from);
		$to = str_replace(array(':',' '), '-',$cached->to);
		$title = $fr.'_to_'.$to;

		$Excel::create('Dated_'.$title, function($excel) use ($items) {
		    $excel->sheet('sheet1', function($sheet) use ($items) {
		    	$ctr = 1;
		    	foreach ($items as $v):
					$sheet->row($ctr,$v);
					$ctr++;
		    	endforeach;
		    });
		})->export('xls');

	}


}

