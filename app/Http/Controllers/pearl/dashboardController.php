<?php namespace App\Http\Controllers\pearl;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class dashboardController extends Controller
{

	public function __construct(){

        $this->middleware('pearl');

    }

	public function index(){

		return view('pearl.dashboard.index');

	}

}