<?php namespace App\Http\Controllers\opal;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class announcementsController extends Controller
{


	public function index(){

		$help = new \App\Http\Controllers\opal\Helper;
		$OpalAnnouncements = new \App\Http\Models\opal\OpalAnnouncements;
		$items = $OpalAnnouncements->source('hrd')->where('status','<>',0)->orderBy('created_at','desc')->paginate(20);		
		$seen = $this->viewed($items);
		return view('opal.announcements.index',compact('items','seen'));
	}

	public function create(Request $r){

		$help = new \App\Http\Controllers\opal\Helper;
		$OpalAnnouncements = new \App\Http\Models\opal\OpalAnnouncements;
		$item = $r->all();
		$item['user_id'] = Auth::user()->id;
		
		$OpalAnnouncements->create($item);
		return redirect()->back();

	}

	public function get($id){

		$help = new \App\Http\Controllers\opal\Helper;
		$OpalAnnouncements = new \App\Http\Models\opal\OpalAnnouncements;
		$item = $OpalAnnouncements->find($id);
		return view('opal.announcements.get',compact('item'));

	} 

	public function edit($id){

		$help = new \App\Http\Controllers\opal\Helper;
		$OpalAnnouncements = new \App\Http\Models\opal\OpalAnnouncements;
		$item = $OpalAnnouncements->find($id);
		return view('opal.announcements.edit',compact('item'));
	} 

	private function viewed($items){

		$help = new \App\Http\Controllers\opal\Helper;
		$return = [];
		$ids = [];
		foreach($items as $i):
			$ids[] = $i->id;
		endforeach;

		if($ids):
			$OpalOpened = new \App\Http\Models\opal\OpalOpened;
			$data = $OpalOpened->whereIn('announcements_id',$ids)->get();
			foreach($data as $d):
				if(isset($return[$d->announcements_id])):
					$return[$d->announcements_id]++;
				else:
					$return[$d->announcements_id] = 1;
				endif;
			endforeach;
		endif;
		
		foreach($items as $item):
			if(!isset($return[$item->id])):
				$return[$item->id] = 0;
			endif;
		endforeach;

		return $return;

	}

	public function seen($id){

		$help = new \App\Http\Controllers\opal\Helper;
		$OpalOpened = new \App\Http\Models\opal\OpalOpened;
		$items = $OpalOpened->where('announcements_id',$id)->with(['userObj'])->get();
		return view('opal.announcements.seen',compact('items'));

	}

} 