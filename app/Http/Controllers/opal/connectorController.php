<?php namespace App\Http\Controllers\opal;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class connectorController extends Controller
{

	public function refs(){

		$help = new \App\Http\Controllers\gem\Helper;
		$GarnetAgents = new \App\Http\Models\opal\GarnetAgents;
		$GemUsers = new \App\Http\Models\opal\GemUsers;
		$users = $GemUsers->agent()->get();
		$agents = $GarnetAgents->get();
		return view('opal.connector.refs',compact('agents','help','users'));

	}

	public function connect(Request $r,$id){

		//validations
		$GarnetAgents = new \App\Http\Models\opal\GarnetAgents;
		$agent = $GarnetAgents->find($id);

		if($agent):
			$agent->user_id = $r->user_id;
			$agent->save();
		endif;

		return redirect()->back();

	}



}