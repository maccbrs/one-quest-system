<?php namespace App\Http\Controllers\opal;



class Helper {

	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}


	public function userType(){
		switch (Auth::user()->user_type):
			case 'operation':
					switch(Auth::user()->user_level):
						case 1:
							echo 'hi';
						case 2: 
							return 'tl';
						break;
					endswitch;
				break;
			case 'reportanalyst':
					return 'ra';
				break;
			case 'hr':
					return 'hr';
				break;
		endswitch;	
		return false;	
	}

	public function fetchComments($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['notes'];
		endif;
	}

	public function fetchUser($str,$users){
		if($str != ''):
			$obj = json_decode($str,true);
			return $users[$obj['approver']];
		endif;
	}

	public function fetchDate($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['date'];
		endif;
	}

	public function fetchStatus($int){
		switch ($int):
			case 0:
				return 'na';
				break;
			case 1:
				return 'pending';
				break;
			case 2:
				return 'approved';
				break;
			case 3:
				return 'rejected';
				break;								
		endswitch;
	}

	public function mbdate(){
		return date('Y-m-d H:i:s');
	}

	public function onyx_dates($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->dispute_label;
			endforeach;
			return $arr;
		endif;

		return false;

	}


	public function onyx_issues($obj){

		if($obj->count()):

			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->issue_name;
			endforeach;
			return $arr;

		endif;

		return false;

	} 

	public function subs(){

		$Hierarchy = new \App\Http\Models\gem\Hierarchy;
		$idsObj = $Hierarchy->subs()->get();
		
		if($idsObj->count()):
			$ids = [];
			foreach($idsObj as $i):
				$ids[] = $i->level2;
			endforeach;
			return $ids;
		endif;

		return false;

	}

}