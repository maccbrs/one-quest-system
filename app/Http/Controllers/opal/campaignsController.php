<?php namespace App\Http\Controllers\opal;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class campaignsController extends Controller
{

	public function index(){

		$help = new \App\Http\Controllers\opal\Helper;
		$GarnetCampaign = new \App\Http\Models\opal\GarnetCampaign;
		$items = $GarnetCampaign->orderBy('id','desc')->paginate(20);
		return view('opal.campaigns.index',compact('items'));

	}

	public function create(Request $r){

		$help = new \App\Http\Controllers\opal\Helper;
		$GarnetCampaign = new \App\Http\Models\opal\GarnetCampaign;
		$GarnetCampaign->create($r->all());
		return redirect()->back();

	}

	public function get($id){

		$help = new \App\Http\Controllers\opal\Helper;
		$GarnetCampaign = new \App\Http\Models\opal\GarnetCampaign;
		$GarnetCampUsers = new \App\Http\Models\opal\GarnetCampUsers;
		$GarnetSupervisor = new \App\Http\Models\opal\GarnetSupervisor;
		$GarnetTeamLeads = new \App\Http\Models\opal\GarnetTeamLeads;
		$GarnetAgents = new \App\Http\Models\opal\GarnetAgents;
		$supervisors = $GarnetSupervisor->get();
		$teamleads = $GarnetTeamleads->get();
		$agents = $GarnetAgents->get();		  
		$item = $GarnetCampaign->find($id);
		$dataSups = $GarnetCampUsers->campaignSup($id)->with(['supObj'])->get();
		$dataTl = $GarnetCampUsers->campaignTl($id)->with(['tlObj'])->get();
		$dataAgents = $GarnetCampUsers->campaignAgent($id)->with(['agentObj'])->get();
		//$help->pre($dataSups);
		return view('opal.campaigns.get',compact('item','supervisors','teamleads','agents','id','dataSups','dataTl','dataAgents'));

	}

	public function add_sup(Request $r){

		$help = new \App\Http\Controllers\opal\Helper;
		$GarnetCampUsers = new \App\Http\Models\opal\GarnetCampUsers;
		$GarnetCampUsers->firstOrCreate(['sup_id' => $r->sup_id,'camp_id' => $r->camp_id]);
		return redirect()->back();

	}

	public function add_tl(Request $r){

		$help = new \App\Http\Controllers\opal\Helper;
		$GarnetCampUsers = new \App\Http\Models\opal\GarnetCampUsers;
		$GarnetCampUsers->firstOrCreate(['tl_id' => $r->tl_id,'camp_id' => $r->camp_id]);
		return redirect()->back();

	}

	public function add_agent(Request $r){

		$help = new \App\Http\Controllers\opal\Helper;
		$GarnetCampUsers = new \App\Http\Models\opal\GarnetCampUsers;
		$GarnetCampUsers->firstOrCreate(['agent_id' => $r->agent_id,'camp_id' => $r->camp_id]);
		return redirect()->back();

	}
}