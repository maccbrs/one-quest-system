<?php namespace App\Http\Controllers\opal;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Alert;

class disputesController extends Controller
{

	public function index(){

		$disputeDates = new \App\Http\Models\opal\disputeDates;
		$help = new \App\Http\Controllers\opal\Helper;
		$dates = $disputeDates->get();
		return view('opal.disputes.index',compact('dates'));
	}

	public function period($id){

		$help = new \App\Http\Controllers\opal\Helper;
		$disputeDates = new \App\Http\Models\opal\disputeDates;
		$disputeIssues = new \App\Http\Models\opal\disputeIssues;
		$dates = $help->onyx_dates($disputeDates->get());
		$issues = $help->onyx_issues($disputeIssues->get());		
		$OnyxDisputes = new \App\Http\Models\opal\OnyxDisputes;
		$disputes = $OnyxDisputes->period($id)->approved()->with(['userObj','periodObj','issueObj'])->paginate(20);
		return view('opal.disputes.period',compact('disputes','help','dates','issues','id'));
	}


	public function approved(){

		$hr_status[0] = 'n/a';
		$hr_status[1] = 'Pending';
		$hr_status[2] = 'Approved';
		$hr_status[3] = 'Rejected';

		$OnyxDisputes = new \App\Http\Models\opal\OnyxDisputes;
		$disputes = $OnyxDisputes
			->where('hr_response', '=',2)
			->with(['userObj','periodObj','issueObj'])
			->orderBy('created_at', 'desc')
			->limit(100)
			->get();

		$Users = new \App\Http\Models\opal\GemUsers;
		$users_list = $Users
			->pluck('name','id');

		$Issues = new \App\Http\Models\opal\disputeIssues;
		$issues_list = $Issues
			->pluck('issue_name','id');

		$tl_details = null;
		$ra_details = null;
		$hr_details = null;
		$sup_details = null;

		foreach ($disputes as $key => $value) {

			$ts1 = strtotime($value->created_at);
			$ts2 = strtotime($value->updated_at);     
			$seconds_diff = $ts2 - $ts1;  
			$days = floor($seconds_diff/86400);                          

			$disputes[$key]->diff_day = $days;
			$disputes[$key]->issue_detail = $issues_list[$value->issue];

			if(!empty($value['hr_comments'])){

				$hrdetails = json_decode($value['hr_comments']);
				$disputes[$key]->hr_approver = $users_list[$hrdetails->approver];
				$disputes[$key]->hr_comment = $hrdetails->notes;
			}

			if(!empty($value['sup_comments'])){

				$supdetails = json_decode($value['sup_comments']);
				$disputes[$key]->sup_approver = $users_list[$supdetails->approver];
				$disputes[$key]->sup_comment = $supdetails->notes;
				
			}

			if(!empty($value['ra_comments'])){

				$radetails = json_decode($value['ra_comments']);
				$disputes[$key]->ra_approver = $users_list[$radetails->approver];
				$disputes[$key]->ra_comment = $radetails->notes;
			}


			if(!($value->userObj->user_type)){

				if($value->userObj->user_type == 'administrator'){

					$disputes[$key]['dept'] == 'IT';
				}

				if($value->userObj->user_type == 'user'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'operation'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'hr'){

					$disputes[$key]['dept'] == 'Human Resources';
				}

				if($value->userObj->user_type == 'qa'){

					$disputes[$key]['dept'] == 'Quality Assurance';
				}

				if($value->userObj->user_type == 'trainer'){

					$disputes[$key]['dept'] == 'Training';
				}

				if($value->userObj->user_type == 'reportanalyst'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'executive'){

					$disputes[$key]['dept'] == 'Executive';
				}

				if($value->userObj->user_type == 'recruitment'){

					$disputes[$key]['dept'] == 'Recruitment';
				}

				if($value->userObj->user_type == 'marketing'){

					$disputes[$key]['dept'] == 'Marketing';
				}

				if($value->userObj->user_type == 'accounting'){

					$disputes[$key]['dept'] == 'Accounting';
				}

			}else{

				$disputes[$key]['dept'] = 'user';
			}

			if($value['tl_comments'] != null){
				$tl_details = json_decode($value['tl_comments']);
			}
			if($value['ra_comments'] != null){
				$tl_details = json_decode($value['ra_comments']);
			}
			if($value['hr_comments'] != null){
				$tl_details = json_decode($value['hr_comments']);
			}
			if($value['sup_comments'] != null){
				$tl_details = json_decode($value['sup_comments']);
			}

			$disputes[$key]['tl_details'] = $tl_details;
			$disputes[$key]['ra_details'] = $ra_details;
			$disputes[$key]['hr_details'] = $hr_details;
			$disputes[$key]['sup_details'] = $sup_details;

		}

		$help = new \App\Http\Controllers\opal\Helper;

		return view('opal.disputes.approved',compact('disputes','hr_status'));
	}

	public function expected(){

		$hr_status[0] = 'n/a';
		$hr_status[1] = 'Pending';
		$hr_status[2] = 'Approved';
		$hr_status[3] = 'Rejected';

		$Users = new \App\Http\Models\opal\GemUsers;
		$usersObj = $Users->select('name','id')->get();
		$users = [];

		foreach($usersObj as $u):
			$users[$u->id] = $u->name;
		endforeach;

		$help = new \App\Http\Controllers\opal\Helper;
		$OnyxDisputes = new \App\Http\Models\opal\OnyxDisputes;
		$disputes = $OnyxDisputes
			->where('hr_response','=',0)
			->where('status','=',1)
			->with(['userObj','periodObj','issueObj'])
			->orderBy('created_at', 'desc')
			->limit(100)
			->get();

		$tl_details = null;
		$ra_details = null;
		$hr_details = null;
		$sup_details = null;

		foreach ($disputes as $key => $value) {

			if($value['tl_comments'] != null){
				$tl_details = json_decode($value['tl_comments']);
			}
			if($value['ra_comments'] != null){
				$tl_details = json_decode($value['ra_comments']);
			}
			if($value['hr_comments'] != null){
				$tl_details = json_decode($value['hr_comments']);
			}
			if($value['sup_comments'] != null){
				$tl_details = json_decode($value['sup_comments']);
			}

			$disputes[$key]['tl_details'] = $tl_details;
			$disputes[$key]['ra_details'] = $ra_details;
			$disputes[$key]['hr_details'] = $hr_details;
			$disputes[$key]['sup_details'] = $sup_details;

			//$help->pre($disputes);

		}

		return view('opal.disputes.expected',compact('disputes','help','users','hr_status'));

	}

	public function rejected(){

		$hr_status[0] = 'n/a';
		$hr_status[1] = 'Pending';
		$hr_status[2] = 'Approved';
		$hr_status[3] = 'Rejected';

		$OnyxDisputes = new \App\Http\Models\opal\OnyxDisputes;
		$disputes = $OnyxDisputes
			->orwhere('tl_response', '=',3)
			->orwhere('ra_response', '=',3)
			->orwhere('hr_response', '=',3)
			->with(['userObj','periodObj','issueObj'])
			->orderBy('created_at', 'desc')
			->limit(100)
			->get();

		$Users = new \App\Http\Models\opal\GemUsers;
		$users_list = $Users
			->pluck('name','id');

		$Issues = new \App\Http\Models\opal\disputeIssues;
		$issues_list = $Issues
			->pluck('issue_name','id');

		$tl_details = null;
		$ra_details = null;
		$hr_details = null;
		$sup_details = null;

		foreach ($disputes as $key => $value) {

			$ts1 = strtotime($value->created_at);
			$ts2 = strtotime($value->updated_at);     
			$seconds_diff = $ts2 - $ts1;  
			$days = floor($seconds_diff/86400);                          

			$disputes[$key]->diff_day = $days;
			$disputes[$key]->issue_detail = $issues_list[$value->issue];

			if(!empty($value['hr_comments'])){

				$hrdetails = json_decode($value['hr_comments']);
				$disputes[$key]->hr_approver = $users_list[$hrdetails->approver];
				$disputes[$key]->hr_comment = $hrdetails->notes;
			}

			if(!empty($value['sup_comments'])){

				$supdetails = json_decode($value['sup_comments']);
				$disputes[$key]->sup_approver = $users_list[$supdetails->approver];
				$disputes[$key]->sup_comment = $supdetails->notes;
				
			}

			if(!empty($value['ra_comments'])){

				$radetails = json_decode($value['ra_comments']);
				$disputes[$key]->ra_approver = $users_list[$radetails->approver];
				$disputes[$key]->ra_comment = $radetails->notes;
			}


			if(!($value->userObj->user_type)){

				if($value->userObj->user_type == 'administrator'){

					$disputes[$key]['dept'] == 'IT';
				}

				if($value->userObj->user_type == 'user'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'operation'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'hr'){

					$disputes[$key]['dept'] == 'Human Resources';
				}

				if($value->userObj->user_type == 'qa'){

					$disputes[$key]['dept'] == 'Quality Assurance';
				}

				if($value->userObj->user_type == 'trainer'){

					$disputes[$key]['dept'] == 'Training';
				}

				if($value->userObj->user_type == 'reportanalyst'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'executive'){

					$disputes[$key]['dept'] == 'Executive';
				}

				if($value->userObj->user_type == 'recruitment'){

					$disputes[$key]['dept'] == 'Recruitment';
				}

				if($value->userObj->user_type == 'marketing'){

					$disputes[$key]['dept'] == 'Marketing';
				}

				if($value->userObj->user_type == 'accounting'){

					$disputes[$key]['dept'] == 'Accounting';
				}

			}else{

				$disputes[$key]['dept'] = 'user';
			}

			if($value['tl_comments'] != null){
				$tl_details = json_decode($value['tl_comments']);
			}
			if($value['ra_comments'] != null){
				$tl_details = json_decode($value['ra_comments']);
			}
			if($value['hr_comments'] != null){
				$tl_details = json_decode($value['hr_comments']);
			}
			if($value['sup_comments'] != null){
				$tl_details = json_decode($value['sup_comments']);
			}

			$disputes[$key]['tl_details'] = $tl_details;
			$disputes[$key]['ra_details'] = $ra_details;
			$disputes[$key]['hr_details'] = $hr_details;
			$disputes[$key]['sup_details'] = $sup_details;

		}

		$help = new \App\Http\Controllers\opal\Helper;

		//$help = pre($disputes);

		return view('opal.disputes.rejected',compact('disputes','help','users','hr_status'));

	}


}