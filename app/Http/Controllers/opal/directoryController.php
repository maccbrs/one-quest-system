<?php namespace App\Http\Controllers\opal;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Alert;

class directoryController extends Controller
{

	public function operation($type){

		$help = new \App\Http\Controllers\opal\Helper;
		$data = false;
		$items = false;
		$title = '';

		switch ($type) {
			case 'agents':
				
				$GarnetAgents = new \App\Http\Models\opal\GarnetAgents;
				$items = $GarnetAgents->with('gem')->paginate(20);
				$title = 'Agent Lists';
				break;

			case 'teamleads':
				$GarnetTeamLeads = new \App\Http\Models\opal\GarnetTeamLeads;
				$items = $GarnetTeamLeads->with('gem')->paginate(20);
				$title = 'Team Leads Lists';
				break;
			case 'campaigns':
				$GarnetCampaign = new \App\Http\Models\opal\GarnetCampaign;
				$items = $GarnetCampaign->with('gem')->paginate(20);
				$title = 'campaigns';
				break;				

		}

		switch($type):
			case 'campaigns':
				return view('opal.directory.campaigns',compact('items','title'));
			break;
			default:

				if($items && $items->count()):
					foreach($items as $item):
						if(!empty($item->gem)){	
							if(!empty($item->gem->active == 1)){	
								$data[] = [
									'fname' => $item->first_name,
									'lname' => $item->last_name,
									'mname' => $item->middle_name,
									'gem_id' => $item->gem->id,
									'name' => $item->gem->name,
									'status' => $item->gem->status,
								];
							}
						}
					endforeach;
				endif;

				//print_r('<pre>');print_r($type);print_r('</pre>'); exit; 

				return view('opal.directory.index',compact('items','data','title','type'));

			break;
		endswitch;
		
	}

	public function department($type){

		$help = new \App\Http\Controllers\opal\Helper;
		$data = false;
		$items = false;
		$title = '';

		//print_r($type);exit; 

		switch ($type) {

			case 'executives':

				$dept = 'executive';
				$title = 'Executive Staffs';

			break;

			case 'bd':
				
				$dept = 'bd';
				$title = 'Business Development Staffs';

			break;

			case 'hr':
			
				$dept = 'hr';
				$title = 'Human Resource Staffs';

			break;			

			case 'admin':
			
				$dept = 'adminstaff';
				$title = 'Admin Staffs';

			break;	

			case 'accounting':
			
				$dept = 'accounting';
				$title = 'Accounting Staffs';

			break;	

			case 'quality':
			
				$dept = 'Quality Assurance';
				$title = 'Quality Assurance Staffs';

			break;					

			case 'reportanalyst':
			
				$dept = 'reportanalyst';
				$title = 'Report Analyst Staffs';

			break;	

			case 'recruitment':
			
				$dept = 'recruitment';
				$title = 'Recruitment Staffs';

			break;	

			case 'trainer':
			
				$dept = 'Trainer';
				$title = 'Trainer Staffs';

			break;	

			case 'noc':
			
				$dept = 'Administrator';
				$title = 'NOC Staffs';

			break;		

			case 'teamleads':
			
				$dept = 'Operations';
				$title = 'Operations Staffs';

			break;	

			case 'agents':
			
				$dept = 'Operations';
				$title = 'Agents';

			break;			


		}

		$GarnetUsers = new \App\Http\Models\opal\GemUsers;
		$users = $GarnetUsers
			->where('user_type','=',$dept)
			->where('active','=','1')
			->paginate(20);

		return view('opal.department.index',compact('users','title','type'));

	}

	public function clearance(Request $r){

		//print_r($r->input()); exit;

		$GarnetUsers = new \App\Http\Models\opal\GemUsers;
		$Clearance = new \App\Http\Models\opal\OnyxClearance;

		$data['status'] = 2;

		$GarnetUsers->where('id','=', $r->input('user_id'))->update($data);

		if($r->input('status') == 1){

			$cl['accounting'] = 0;
			$cl['hr'] = 0;
			$cl['it'] = 0;
			$cl['operations'] = '[]' ;
			$cl['user_id'] = $r->input('user_id') ;
			$Clearance->create($cl);

		}

		$type = $r->input('type');

		Alert::message('Clearance has been Issued to the User')->persistent('Close');

		$ops = ['agents','teamleads'];

		if(in_array($type,$ops)){
 
			return $this->operation($type);

		}else{

			return $this->department($type);

		}
	}

} 