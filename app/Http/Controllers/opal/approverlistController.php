<?php namespace App\Http\Controllers\opal;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Alert;
use Yajra\Datatables\Datatables;

class approverlistController extends Controller
{

	 public function getIndex()
    {
        return view('opal.disputes.agentapprover');
    }

	public function index(){
		$help = new \App\Http\Controllers\gem\Helper;
		$GarnetTeamLeads = new \App\Http\Models\gem\GarnetTeamLeads;
		$GarnetProjectSupervisors = new \App\Http\Models\gem\GarnetProjectSupervisors;
		$Users = new \App\Http\Models\gem\User;
		$supervisors = $GarnetProjectSupervisors->get();
		$teamleads = $GarnetTeamLeads->get();
		$GarnetAgents = new \App\Http\Models\gem\GarnetAgents; 
		$users = $Users->get();
		$agents = $GarnetAgents->with('gem','teamlead','supervisor')->orderBy('createdAt','desc')->get();
		//help->pre($agents);		
		return view('opal.disputes.approverlistagents',compact('gem','supervisors','teamleads','users','agents'));
	}

	public function approverlistnonagents(){
		$GemUsers = new \App\Http\Models\amethyst\GemUsers;
		$GemApprover = new \App\Http\Models\amethyst\GemApprover;
		$help = new \App\Http\Controllers\amethyst\Helper;
		
		
		$items = $GemUsers
			->select('name','id')
			->where('active','=','1')
			->orderBy('name', 'asc')
			->get();	

		$approvers = $GemApprover->with(['emp','sup'])->get();

		return view('opal.disputes.approverlistnonagents',compact('help','items','approvers'));	
	}

}