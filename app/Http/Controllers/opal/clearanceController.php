<?php namespace App\Http\Controllers\opal;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Alert;
use Auth;
use Excel;

class clearanceController extends Controller
{

	public function index(){

		$dept['noc'] = 'NOC Staffs';
		$dept['trainer'] = 'Trainer Staffs';
		$dept['recruitment'] = 'Recruitment';
		$dept['reportanalyst'] = 'Report Analyst';
		$dept['quality'] = 'Quality Assurance';
		$dept['admin'] = 'Admin';
		$dept['admin'] = 'Admin';
		$dept['hr'] = 'Human Resources';
		$dept['bd'] = 'Business Development';
		$dept['executives'] = 'Executive';
		$help = new \App\Http\Controllers\opal\Helper;

		$GarnetUsers = new \App\Http\Models\opal\OnyxClearance;
		$users = $GarnetUsers
			->with('gem')
			->where('status','=',0)
			->orderBy('updated_at','desc')
			->paginate(20);

		foreach ($users as $key => $value) {
			
			if(!empty($value['operations'])){
				$operations = json_decode($value['operations']);
				$users[$key]['operations2'] = $operations ; 
			}
		}

		$cleared_users = $GarnetUsers
			->with('gem')
			->where('status','=',2)
			->orderBy('updated_at','desc')
			->paginate(15);

		foreach ($cleared_users as $key2 => $value2) {

			if(!empty($value2['operations'])){

				$operations2 = json_decode($value2['operations']);
				$cleared_users[$key2]['operations2'] = $operations2 ; 
			}

		}

		$incomplete_users = $GarnetUsers
			->with('gem')
			->where('status','=',3)
			->orderBy('updated_at','desc')
			->paginate(15);

		foreach ($incomplete_users as $key3 => $value3) {

			if(!empty($value3['operations'])){

				$operations3 = json_decode($value3['operations']);
				$incomplete_users[$key3]['operations2'] = $operations3 ; 
			}

		}

		return view('opal.clearance.index',compact('users','dept','cleared_users','incomplete_users'));

	}

	public function clearance_update(Request $r){


		$Clearance = new \App\Http\Models\opal\OnyxClearance;

		$cl['admin'] = $r->input('admin');
		$cl['it'] = $r->input('it');
		$cl['hr'] = $r->input('hr');

		$cl['operations'] = json_encode($r->input('operations')); 
		$cl['status'] = $this->clearance_validator($r->input()); 
		//print_r('<pre>');print_r($cl['status']);print_r('</pre>');  exit;

		if($cl['status'] == 1){

			$GarnetUsers = new \App\Http\Models\opal\GemUsers;

			$gem['active'] = 0 ;

			$GarnetUsers->where('id','=', $r->input('gem_id'))->update($gem);

			$cl['status'] = 2; 

		}

		$Clearance->where('id','=', $r->input('user_id'))->update($cl);

		Alert::message('Clearance has been Updated')->persistent('Close');

		return $this->index();

	}

	public function clearance_close(Request $r){

		//print_r('<pre>');print_r($r->input());print_r('</pre>'); exit; 

			$GarnetUsers = new \App\Http\Models\opal\GemUsers;

			$Clearance = new \App\Http\Models\opal\OnyxClearance;

			$gem['active'] = 0 ;
			$gem['status'] = 3 ;

			$GarnetUsers->where('id','=', $r->input('gem_id'))->update($gem);

			$cl['status'] = 3; 

			$Clearance->where('id','=', $r->input('user_id'))->update($cl);

			Alert::message('Clearance has been Marked as Incomplete')->persistent('Close');

		return $this->index();

	}

	public function clearance_validator($data){

		if(!empty($data['operations'])){
			foreach ($data['operations'] as $key => $value) {
				$data[$key] = $value;
			}
		}

		if($data['is_agent'] == 1) {

			$dept = ['admin','it','hr','teamleader','program_supervisor','manager'];

		}else{

			$dept = ['admin','it','hr','manager'];

		}

		foreach ($dept as $key ) {

			if(empty($data[$key])){


				return 0;
			}

			if($data[$key] == 0){

				
				
				return 0;
			}
		}

	
		return 1;
	}

	public function export(Request $r){

		$date = explode(" - ", $r->input('daterange'));

		$from = date( "Y/m/d H:i:s", strtotime($date[0]));
		$to = date( "Y/m/d H:i:s", strtotime($date[1]));

		$GarnetUsers = new \App\Http\Models\opal\GemUsers;

		if($r->input('type') == 1){

			$users = $GarnetUsers
				->where('status','=',2)
				->where('active','=',1)
				->get();

			$indicator = 'on Process';

		}

		if($r->input('type') == 2){

			$Clearance = new \App\Http\Models\opal\OnyxClearance;

			$users = $Clearance
				->with('gem')
				->where('status','=',3)
				->whereBetween('updated_at', [$from, $to])
				->get();


			$indicator = 'Incomplete';

		}

		if($r->input('type') == 3){

			$Clearance = new \App\Http\Models\opal\OnyxClearance;

			$users = $Clearance
				->with('gem')
				->where('status','=',2)
				->whereBetween('updated_at', [$from, $to])
				->get();


			$indicator = 'Completed';

		}

		$filename = $indicator . "_Clearance";

		Excel::create($filename, function($excel) use($users,$indicator) {

		    $excel->sheet('Clearance', function($sheet) use($users,$indicator) {

		        $sheet->loadView('opal.clearance.export',compact('users','indicator'));
		    });

		    ob_clean();

		})->download('xls');
					
	}
}