<?php namespace App\Http\Controllers\opal;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\opal\disputeDates;
use App\Http\Models\opal\disputeIssues;
use Excel;


class disputeSettingController extends Controller
{

	public function index(){

		$dispute_dates = new disputeDates;
		$datelists = $dispute_dates->get(); 

		return view('opal.dispute_setting.index',compact('datelists'));

	}

	public function create(Request $r){

		$dispute_dates = new disputeDates;
		$datelists = $dispute_dates->get(); 

		if(!empty($r->input("daterange"))){

			$daterange = new disputeDates;
			$datedata = explode("-", $r->input("daterange")); 
			$daterange['dispute_label'] = $r->input("dispute_label");
			$daterange['from'] = $datedata[0];
			$daterange['to'] = $datedata[1];
			$daterange['status'] = 1;
			$daterange->save();

			$msg = 'Dispute Date has been Saved!';

		}

		return back();
	}

	public function update(Request $r){

		$dispute_dates = new disputeDates;

		if(!empty($r->input('id'))){

			$datedata = explode("-", $r->input("daterange")); 
			$daterange['dispute_label'] = $r->input("daterange");
			$daterange['from'] = $datedata[0];
			$daterange['to'] = $datedata[1];
			$daterange['status'] = $r->input("status");
			$daterange['id'] = $r->input("id");
			disputeDates::find($daterange['id'])->update($daterange);

		}

		return back();

	}

	public function index_issue(){

		$dispute_issues = new disputeIssues;
		$date_issues = $dispute_issues->get(); 

		return view('opal.dispute_setting.index_issue',compact('date_issues'));

	}

	public function create_issue(Request $r){

	
		if(!empty($r->input("issue_name"))){

			$issue_data = new disputeIssues;
			$issue_data['issue_name'] = $r->input('issue_name');
			$issue_data['status'] = 1;
			$issue_data->save();
			$date_issues = $issue_data->get(); ; 

			$msg = 'Dispute Issue has been Saved!';

		}

		return back();
	}

	public function edit_issue(Request $r){
	
		if(!empty($r->input("id"))){
		
			$issue_data = new disputeIssues;

			disputeIssues::find($r->input('id'))->update($r->all());

			$msg = 'Dispute Issue has been Saved!';

		}

		return back();
	}

	public function export(Request $r){

		$date = explode(" - ", $r->input('daterange'));

		$from = date( "Y/m/d H:i:s", strtotime($date[0]));
		$to = date( "Y/m/d H:i:s", strtotime($date[1]));

		$status = $r->input('status');

		if($status == 0){

			$status = [1,2,3];

		}else{

			$status =  [$r->input('status')];
		}

		$hr_status[0] = 'n/a';
		$hr_status[1] = 'Pending';
		$hr_status[2] = 'Approved';
		$hr_status[3] = 'Rejected';

		$OnyxDisputes = new \App\Http\Models\opal\OnyxDisputes;
		$disputes = $OnyxDisputes
			//->period($r->input('id'))
			//->approved()
			//->whereIn('hr_response', $status)
			->where('hr_response', '=',$r->input('status'))
			->with(['userObj','periodObj','issueObj'])
			->whereBetween('created_at', [$from, $to])
			->orderBy('created_at', 'desc')
			->get();

	//	print_r('<pre>'); print_r($disputes);	print_r('</pre>'); exit; 

		$Users = new \App\Http\Models\opal\GemUsers;
		$users_list = $Users
			->pluck('name','id');

		$Issues = new \App\Http\Models\opal\disputeIssues;
		$issues_list = $Issues
			->pluck('issue_name','id');

		foreach ($disputes as $key => $value) {

			$ts1 = strtotime($value->created_at);
			$ts2 = strtotime($value->updated_at);     
			$seconds_diff = $ts2 - $ts1;  
			$days = floor($seconds_diff/86400);                          

			$disputes[$key]->diff_day = $days;
			$disputes[$key]->issue_detail = $issues_list[$value->issue];
			$disputes[$key]->from = $from;
			$disputes[$key]->to = $to;

			if(!empty($value['hr_comments'])){

				$hrdetails = json_decode($value['hr_comments']);
				$disputes[$key]->hr_approver = $users_list[$hrdetails->approver];
				$disputes[$key]->hr_comment = $hrdetails->notes;
			}

			if(!empty($value['sup_comments'])){

				$supdetails = json_decode($value['sup_comments']);
				$disputes[$key]->sup_approver = $users_list[$supdetails->approver];
				$disputes[$key]->sup_comment = $supdetails->notes;
				
			}

			if(!empty($value['ra_comments'])){

				$radetails = json_decode($value['ra_comments']);
				$disputes[$key]->ra_approver = $users_list[$radetails->approver];
				$disputes[$key]->ra_comment = $radetails->notes;
			}


			if(!($value->userObj->user_type)){

				if($value->userObj->user_type == 'administrator'){

					$disputes[$key]['dept'] == 'IT';
				}

				if($value->userObj->user_type == 'user'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'operation'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'hr'){

					$disputes[$key]['dept'] == 'Human Resources';
				}

				if($value->userObj->user_type == 'qa'){

					$disputes[$key]['dept'] == 'Quality Assurance';
				}

				if($value->userObj->user_type == 'trainer'){

					$disputes[$key]['dept'] == 'Training';
				}

				if($value->userObj->user_type == 'reportanalyst'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'executive'){

					$disputes[$key]['dept'] == 'Executive';
				}

				if($value->userObj->user_type == 'recruitment'){

					$disputes[$key]['dept'] == 'Recruitment';
				}

				if($value->userObj->user_type == 'marketing'){

					$disputes[$key]['dept'] == 'Marketing';
				}

				if($value->userObj->user_type == 'accounting'){

					$disputes[$key]['dept'] == 'Accounting';
				}

			}else{

				$disputes[$key]['dept'] = 'user';
			}

		}

		$filename = "dispute_" . $from ."-" . $to ;

		$help = new \App\Http\Controllers\opal\Helper;

		Excel::create($filename, function($excel) use($disputes,$help,$hr_status) {

		    $excel->sheet('disputes', function($sheet) use($disputes,$help, $hr_status) {

		        $sheet->loadView('opal.disputes.export',compact('disputes','help','hr_status'));
		    });

		    ob_clean();

		})->download('xls');
					
	}

	public function excel_approved(Request $r, $status = 'approved'){

		$this->export_funtion($r,$status);

	}

	public function excel_rejected(Request $r, $status = 'rejected'){

		$this->export_funtion($r,$status);

	}

	public function export_funtion($r,$status){

		$date = explode(" - ", $r->input('daterange'));

		$from = date( "Y/m/d H:i:s", strtotime($date[0]));
		$to = date( "Y/m/d H:i:s", strtotime($date[1]));

		$hr_status[0] = 'n/a';
		$hr_status[1] = 'Pending';
		$hr_status[2] = 'Approved';
		$hr_status[3] = 'Rejected';

		if($status == 'rejected'){

			$OnyxDisputes = new \App\Http\Models\opal\OnyxDisputes;
			$disputes = $OnyxDisputes
				->orWhere('tl_response','=',3)
				->orWhere('ra_response','=',3)
				->orWhere('hr_response','=',3)
				->whereBetween('created_at', [$from, $to])
				->with(['userObj','periodObj','issueObj'])
				->orderBy('created_at', 'desc')
				->limit(100)
				->get();
		}else{

			$OnyxDisputes = new \App\Http\Models\opal\OnyxDisputes;
			$disputes = $OnyxDisputes
				->orWhere('hr_response','=',2)
				->whereBetween('created_at', [$from, $to])
				->with(['userObj','periodObj','issueObj'])
				->orderBy('created_at', 'desc')
				->limit(100)
				->get();

		}

		$Users = new \App\Http\Models\opal\GemUsers;
		$users_list = $Users
			->pluck('name','id');

		$Issues = new \App\Http\Models\opal\disputeIssues;
		$issues_list = $Issues
			->pluck('issue_name','id');

		$tl_details = null;
		$ra_details = null;
		$hr_details = null;
		$sup_details = null;

		foreach ($disputes as $key => $value) {

			$ts1 = strtotime($value->created_at);
			$ts2 = strtotime($value->updated_at);     
			$seconds_diff = $ts2 - $ts1;  
			$days = floor($seconds_diff/86400);                          

			$disputes[$key]->diff_day = $days;
			$disputes[$key]->issue_detail = $issues_list[$value->issue];
			$disputes[$key]->from = $from;
			$disputes[$key]->to = $to;

			if(!empty($value['hr_comments'])){

				$hrdetails = json_decode($value['hr_comments']);
				$disputes[$key]->hr_approver = $users_list[$hrdetails->approver];
				$disputes[$key]->hr_comment = $hrdetails->notes;
			}

			if(!empty($value['sup_comments'])){

				$supdetails = json_decode($value['sup_comments']);
				$disputes[$key]->sup_approver = $users_list[$supdetails->approver];
				$disputes[$key]->sup_comment = $supdetails->notes;
				
			}

			if(!empty($value['ra_comments'])){

				$radetails = json_decode($value['ra_comments']);
				$disputes[$key]->ra_approver = $users_list[$radetails->approver];
				$disputes[$key]->ra_comment = $radetails->notes;
			}

			if($value['tl_comments'] != null){
				$tl_details = json_decode($value['tl_comments']);
			}
			if($value['ra_comments'] != null){
				$tl_details = json_decode($value['ra_comments']);
			}
			if($value['hr_comments'] != null){
				$tl_details = json_decode($value['hr_comments']);
			}
			if($value['sup_comments'] != null){
				$tl_details = json_decode($value['sup_comments']);
			}

			$disputes[$key]['tl_details'] = $tl_details;
			$disputes[$key]['ra_details'] = $ra_details;
			$disputes[$key]['hr_details'] = $hr_details;
			$disputes[$key]['sup_details'] = $sup_details;


			if(!($value->userObj->user_type)){

				if($value->userObj->user_type == 'administrator'){

					$disputes[$key]['dept'] == 'IT';
				}

				if($value->userObj->user_type == 'user'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'operation'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'hr'){

					$disputes[$key]['dept'] == 'Human Resources';
				}

				if($value->userObj->user_type == 'qa'){

					$disputes[$key]['dept'] == 'Quality Assurance';
				}

				if($value->userObj->user_type == 'trainer'){

					$disputes[$key]['dept'] == 'Training';
				}

				if($value->userObj->user_type == 'reportanalyst'){

					$disputes[$key]['dept'] == 'Operations';
				}

				if($value->userObj->user_type == 'executive'){

					$disputes[$key]['dept'] == 'Executive';
				}

				if($value->userObj->user_type == 'recruitment'){

					$disputes[$key]['dept'] == 'Recruitment';
				}

				if($value->userObj->user_type == 'marketing'){

					$disputes[$key]['dept'] == 'Marketing';
				}

				if($value->userObj->user_type == 'accounting'){

					$disputes[$key]['dept'] == 'Accounting';
				}

			}else{

				$disputes[$key]['dept'] = 'user';
			}

		}

		$filename = "dispute_" . $from ."-" . $to ;

		$help = new \App\Http\Controllers\opal\Helper;

		Excel::create($filename, function($excel) use($disputes,$help,$hr_status) {

		    $excel->sheet('disputes', function($sheet) use($disputes,$help, $hr_status) {

		        $sheet->loadView('opal.disputes.export-rejected',compact('disputes','help','hr_status'));
		    });

		    ob_clean();

		})->download('xls');
					
	}


}