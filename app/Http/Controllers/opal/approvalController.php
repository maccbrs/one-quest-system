<?php namespace App\Http\Controllers\opal;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class approvalController extends Controller
{

	public function nonagent(){

		$Users = new \App\Http\Models\opal\GemUsers;
		$usersObj = $Users->select('name','id')->get();
		$users = [];
		$GemApprover = new \App\Http\Models\gem\GemApprover;
		$items = $GemApprover->select('level1')->self()->get();
		$ids = [];

		if($items->count()):
			foreach($items as $i):
			$ids[] = $i->level1;
			endforeach;
		endif;

		foreach($usersObj as $u):
			$users[$u->id] = $u->name;
		endforeach;
		$help = new \App\Http\Controllers\opal\Helper;
		$OnyxDisputes = new \App\Http\Models\opal\OnyxDisputes;
		$disputes = $OnyxDisputes
			->where('user_type',2)
			->whereIn('sup_response',[2])
			->whereIn('hr_response',[1])
			->with(['userObj','periodObj','issueObj'])
			->get();
		
		$tl_details = null;
		$ra_details = null;
		$hr_details = null;
		$sup_details = null;

		foreach ($disputes as $key => $value) {

			if($value['tl_comments'] != null){
				$tl_details = json_decode($value['tl_comments']);
			}
			if($value['ra_comments'] != null){
				$tl_details = json_decode($value['ra_comments']);
			}
			if($value['hr_comments'] != null){
				$tl_details = json_decode($value['hr_comments']);
			}
			if($value['sup_comments'] != null){
				$tl_details = json_decode($value['sup_comments']);
			}

			$disputes[$key]['tl_details'] = $tl_details;
			$disputes[$key]['ra_details'] = $ra_details;
			$disputes[$key]['hr_details'] = $hr_details;
			$disputes[$key]['sup_details'] = $sup_details;

		}

		$disputes2 = $OnyxDisputes
			->where('user_type',2)
			->where('sup_response','<>',0)
			->whereIn('hr_response',[2,3])
			->where('status',1)
			->where('posted',0)
			//->whereIn('user',$ids)
			->with(['periodObj','issueObj','userObj'])
			->get();

		//print_r('<pre>'); print_r($disputes2); print_r('</pre>'); exit; 

		$tl_details2 = null;
		$ra_details2 = null;
		$hr_details2 = null;
		$sup_details2 = null;

		foreach ($disputes2 as $key2 => $value2) {

			if($value2['tl_comments'] != null){
				$tl_details2 = json_decode($value2['tl_comments']);
			}
			if($value2['ra_comments'] != null){
				$tl_details2 = json_decode($value2['ra_comments']);
			}
			if($value2['hr_comments'] != null){
				$tl_details2 = json_decode($value2['hr_comments']);
			}
			if($value2['sup_comments'] != null){
				$tl_details2 = json_decode($value2['sup_comments']);
			}

			$disputes2[$key2]['tl_details'] = $tl_details2;
			$disputes2[$key2]['ra_details'] = $ra_details2;
			$disputes2[$key2]['hr_details'] = $hr_details2;
			$disputes2[$key2]['sup_details'] = $sup_details2;

		}
//$help->pre($disputes);

		return view('opal.approval.nonagent',compact('disputes','disputes2','help','users'));

	}
}