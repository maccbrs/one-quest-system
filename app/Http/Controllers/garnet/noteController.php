<?php namespace App\Http\Controllers\garnet;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Models\gem\GarnetAgents;
use \App\Http\Models\gem\GarnetDeptNotes;
use \App\Http\Models\gem\GarnetGroups;
use \App\Http\Models\gem\GemUser;
use \App\Http\Models\gem\GarnetTeamLeads;
use \App\Http\Models\gem\GarnetProjectSupervisors;
use DB;
use Auth;

class noteController extends Controller
{

	public function index($dept = null){

		$all_users = new GemUser;
		$all_garnet = new GarnetAgents;
		$teamleads_garnet = new GarnetTeamleads;
		$program_sup_garnet = new GarnetProjectSupervisors;

		$pro_sup = array();
		$note_list = null;
		$note_data = array();

		$userId = Auth::user()->id;
		$dept  = $dept;

		$user_name = Auth::user()->name;

		$users_list = $all_users
			->pluck('name','id');

		$notes = new GarnetDeptNotes;

		$note_list = $notes
			->where('to', $userId )
			->orderBy('created_at','desc')
			->get();

		if(!empty($note_list)){

			foreach ($note_list as $key => $value) {
				
				$note_list[$key]['content'] = json_decode($value['content']);
				$note_list[$key]['header'] = json_decode($value['header']);
				$note_list[$key]['type'] = json_decode($value['type']);
				$note_list[$key]['status'] = json_decode($value['status']);

			}
		}

		//print_r('<pre>');print_r($note_list);print_r('</pre>'); exit; 

		return view('garnet.notes.index',compact('userId','users_list','team_lead','pro_sup','note_list','user_name','dept'));

	}

	public function add_notes(Request $r, $user= null){

		$notes = new GarnetDeptNotes;
	
		if(!empty($r->input())){

			$message_content['content'] = $r->input("content");
			$subject_holder['subject'] = $r->input("subject");
			$type_holder['type'] = $r->input("type");
			$dept_holder['status'] = $r->input("user_type");


			foreach ($dept_holder['status'] as $key => $value) {

				if($value == 'operation'){
					
					if(Auth::user()->user_level == 1){

						$dept_holder['status'][$key] = 'Team Leader';

					}else{

						$dept_holder['status'][$key]  = 'Manager';
					}
				}

				if($value == 'hr'){
					
					$dept_holder['status'][$key]  = 'Human Resource';

				}

				if($value == 'administrator'){
					
					$dept_holder['status'][$key]  = 'Administrator';

				}

			}

			$subject_holder = json_encode($subject_holder);
			$content_holder = json_encode($message_content);
			$type_holder = json_encode($type_holder);
			$dept_holder = json_encode($dept_holder);

			$notes['user'] = $r->input("user");
			$notes['header'] = $subject_holder;
			$notes['content'] =$content_holder;
			$notes['to'] = $r->input("to");
			$notes['type'] = $type_holder;
			$notes['status'] =$dept_holder;
		
			$notes->save();

		return back();

		}

	}

	public function update_notes(Request $r, $user= null){

		$note = new GarnetDeptNotes;
		
		if(!empty($r->input())){

			$message_content['content'] = $r->input("content");
			$subject_holder['subject'] = $r->input("subject");
			$type_holder['type'] = $r->input("type");
			$dept_holder['status'] = $r->input("user_type");

			foreach ($dept_holder['status'] as $key => $value) {

				if($value == 'operation'){
					//print_r($r->input("user_type")); exit; 
					if(Auth::user()->user_level == 1){

						$dept_holder['status'][$key] = 'Team Leader';

					}else{

						$dept_holder['status'][$key]  = 'Manager';
					}
				}

				if($value == 'hr'){
					
					$dept_holder['status'][$key]  = 'Human Resource';

				}

				if($value == 'administrator'){
					
					$dept_holder['status'][$key]  = 'Administrator';

				}

			}

			$subject_holder = json_encode($subject_holder);
			$content_holder = json_encode($message_content);
			$type_holder = json_encode($type_holder);
			$dept_holder = json_encode($dept_holder);

			$values=array('user'=> $r->input("user"),
				'header'=>$subject_holder,
				'content'=>$content_holder,
				'to'=>$r->input("to"),
				'type'=>$type_holder,
				'status'=>$dept_holder,
			);

			GarnetDeptNotes::whereIn('id',[$r->input("id")])->update($values);

		return back();

		}

	}


}