<?php namespace App\Http\Controllers\garnet;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Models\gem\GarnetAgents;
use App\Http\Models\gem\GarnetMessages;
use App\Http\Models\gem\GarnetGroups;
use App\Http\Models\gem\GemUser;
use App\Http\Models\gem\GarnetTeamLeads;
use App\Http\Models\gem\GarnetDeptNotes;
use App\Http\Models\gem\GarnetProjectSupervisors;
use Auth;
use DB;

class teamController extends Controller
{

	public function agents(){

		$all_users = new GemUser;
		$all_garnet = new GarnetAgents;
		$messages = new GarnetMessages;
		$all_agents = null;
	
		$user = Auth::user();

		$userId = $user->id;
		$userType = $user->user_type;

		$superior_data = $all_users->garnet($userId,$userType);

		$users_list = $all_users
			->pluck('name','id');

		$all_agents = $all_garnet->get_agent($superior_data,$userType);

		$message_data = $messages->get_message($userId);

		foreach ($message_data as $key => $value) {

			$messages_list[$value['to']][] = $value;
			$messages_list[$value['user']][] = $value;

		}

		return view('garnet.team.index',compact('userId','users_list','all_agents','messages_list'));

	}

	public function mail($dept = null){

		$all_users = new GemUser;
		$all_garnet = new GarnetAgents;
		$messages = new GarnetMessages;
		$noc_users = null;
	
		$user = Auth::user();

		$userId = $user->id;
		$userType = $user->user_type;

		$users_list = $all_users
			->pluck('name','id');

		if($dept == 'Operations'){

			$dept_holder = array('Team Leader','Manager');


		}else{

			if($dept == 'Human Resource'){

				$dept = 'hr';
			}

			$dept_holder = array($dept);
			
		}

		$magellan_users = $all_users->whereIn('user_type',$dept_holder)->get();

		$message_data = $messages->get_message($userId);

		foreach ($message_data as $key => $value) {

			$messages_list[$value['to']][] = $value;
			$messages_list[$value['user']][] = $value;

		}

		if($dept == 'Administrator'){

			$dept = 'NOC';

		}

		return view('garnet.team.mail',compact('userId','users_list','magellan_users','messages_list','dept'));

	}

	public function group(){

		$all_users = new GemUser;
		$all_garnet = new GarnetAgents;
		$user_groups = new GarnetGroups;
		$messages = new GarnetMessages;

		$all_agents = null;
		$all_groups = array();
		$message_collection = array();
		
		$userId = Auth::user()->id;
		$userType = Auth::user()->user_type;
		$user_name = Auth::user()->name;

		$all_groups_raw = $user_groups
			->where('status','=','1')
			->orderBy('id','desc')
			->get();

		foreach ($all_groups_raw as $key => $value) {

			$members = json_decode($value['members']);
			$members_list[$value['id']] = json_decode($value['members']);


			if (in_array($userId, $members)) {

			   $all_groups[$key] = $value;
			   $all_groups[$key]['members_list'] = $members;
			   $message_collection []= $value['id'];

			}
		}

		$superior_data = $all_users->garnet($userId,$userType);

		$all_agents = $all_garnet->get_agent($superior_data,$userType);


		$non_agent = $all_users
			->select('id','name')
			->whereNotIn('user_type',['Human Resource','Team Leader','Manager'])
			->get();

		$human_resource = $all_users
			->select('id','name')
			->whereIn('user_type',['Human Resource'])
			->get();

		$users_list = $all_users
			->pluck('name','id');

		$messages_data = $messages
			->whereIn('to',$message_collection)
			->where('page','group')
			->orderBy('created_at','desc')
			->get();

		foreach ($messages_data as $key => $value) {

			$messages_data[$key]['content'] = json_decode($value['content']);
			$messages_list[$value['to']][] = $messages_data[$key];
			
		}

		return view('garnet.team.group',compact('userId','users_list','team_lead','pro_sup','messages_list','user_name','all_agents','all_groups','members_list','non_agent','userType','human_resource'));


	}

	public function new_message(Request $r){
		
		$messages = new GarnetMessages;
		
		if(!empty($r->input())){

			$message_content['content'] = $r->input("content");
			$content_holder = json_encode($message_content);

			$messages['user'] = $r->input("user");
			$messages['subject'] = $r->input("subject");
			$messages['content'] =$content_holder;
			$messages['to'] = $r->input("to");
			$messages['type'] = $r->input("type");
			$messages['status'] =$r->input("status");
			$messages['page'] =$r->input("page");
	
			$messages->save();

		}

	return back();

	}

	public function new_group(Request $r){

		$group = new GarnetGroups;
		$member3 = array();
		$member2 = array();
		$member = array();

		if(!empty($r->input())){

			if(!empty($r->input("members"))){


				$member= $r->input("members");

			}

			if(!empty($r->input("members2"))){

				$member2= $r->input("members2");

			}

			if(!empty($r->input("members3"))){

				
				$member3= $r->input("members3");

			}

			$group_members= $r->input("members");
	
			$group_members = array_merge($member,$member2,$member3);
			$group_members = json_encode($group_members);

			if(empty($r->input("name"))){

				$group['name'] = 'No Name Group';

			}else{

				$group['name'] = $r->input("name");

			}

			$group['user'] = $r->input("user");
			$group['members'] = $group_members;
			$group['status'] = $r->input("status");
			$group->save();

		}

		return redirect()->back();
	}

	public function edit_group(Request $r){

		$group = new GarnetGroups;
		$member3 = array();
		$member2 = array();
		$member = array();

		if(!empty($r->input("members"))){

			$member= $r->input("members");

		}

		if(!empty($r->input("members2"))){

			$member2= $r->input("members2");

		}

		if(!empty($r->input("members3"))){

			$member3= $r->input("members3");

		}

		$group_members= $r->input("members");

		$group_members = array_merge($member,$member2,$member3);
		$group_members = json_encode($group_members);

		if(empty($r->input("name"))){

			$group['name'] = 'No Name Group';

		}else{

			$group['name'] = $r->input("name");

		}


		$values=array('user'=> $r->input("user"),
			'user'=>$r->input("user"),
			'members'=>$group_members,
			'status'=>$r->input("status"),
			'name'=>$r->input("name"),
		);


		$group->whereIn('id',[$r->input("id")])->update($values);

		return redirect()->back();
	}

	public function notes(){

		$Users = new \App\Http\Models\gem\User;
		$Access = new \App\Http\Models\gem\Accesslist;
		$helper = new \App\Http\Controllers\gem\Helper;

		$notedata = null;

		$user_type = Auth::user()->user_type;

		$userId = Auth::user()->id;

		$notes = new GarnetDeptNotes;

		$note_list = $notes
			->where('user','=',$userId)
			->orderBy('created_at','desc')
			->get();


		foreach ($note_list as $key => $value) {

			$note_list[$key]['content'] = json_decode($value['content']);
			$note_list[$key]['header'] = json_decode($value['header']);
			$note_list[$key]['type'] = json_decode($value['type']);
			$note_list[$key]['user_type'] = json_decode($value['status']);
			
			$notedata[$value['to']]['header'] = $note_list[$key]['header']; 
			$notedata[$value['to']]['content'] = $note_list[$key]['content']; 
			$notedata[$value['to']]['type'] = $note_list[$key]['type']; 
			$notedata[$value['to']]['id'] = $note_list[$key]['id']; 

		}

		$accesslist = DB::table('accesses')->pluck('access_name','id');

		$users = $Users->where('user_type','<>','admin')->get();

		foreach ($users as $key => $value) {

			$access_json = json_decode($value['access']); 

			if(!empty($access_json)){
				foreach ($access_json as $ctr => $access) {

					$users[$key]['accesslist'] .= $access . ',';
	
				}
			}else{

				$users[$key]['accesslist'] = '';
			}

		}

		return view('gem.team.notes',compact('users','helper','accesslist','userId','notedata','user_type'));


	}

}