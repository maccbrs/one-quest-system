<?php namespace App\Http\Controllers\garnet;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\gem\Helper;
use App\Http\Models\gem\GarnetAgents;
use App\Http\Models\gem\GarnetMessages;
use App\Http\Models\gem\GarnetGroups;
use App\Http\Models\gem\GemUser;
use App\Http\Models\gem\GemUserInfo;
use App\Http\Models\gem\GarnetProjectSupervisors;
use Auth;
use Image;
use File;
use Alert;

class profileController extends Controller
{

	public function index(Request $r){

		$help = new Helper;
		$request = $r->all();
		$user = Auth::user();

		$user_info = new GemUserInfo;

		$user_data = $user_info
			->where('user_id','=',$user->id)
			->orderby('id','desc')
			->with('applicant')
			->first();

		$user_data['information'] = json_decode($user_data['information']);

		if(!empty($user_data->applicant['additional'])){

			$user_data['applicant'] = json_decode($user_data->applicant['additional']);

		}

	//	print_r('<pre>');print_r($user_data);print_r('</pre>'); exit;
		
		return view('garnet.profile.index',compact('user','user_data','request','help'));

	}

	public function update_image(Request $r){

		$GemUser = new \App\Http\Models\gem\GemUser;
		$user = $GemUser->find(Auth::user()->id);

		if($user):

	        $image = $r->file('file');
	        $filename  = time() . '.' . $image->getClientOriginalExtension();
	        $sm = public_path('uploads/sm-' . $filename);
	        $m = public_path('uploads/m-' . $filename);
	        $l = public_path('uploads/l-' . $filename);
	        \Image::make($image->getRealPath())->resize(50, 50)->save($sm);
	        \Image::make($image->getRealPath())->resize(100, 100)->save($m);
	        \Image::make($image->getRealPath())->resize(200, 200)->save($l);	
	        $this->unlink($user->avatar);
	        $user->avatar = $filename;
	        $user->save();

		endif;

		return redirect()->back();

	}

	private function unlink($fn){
        $sm = 'uploads/sm-'.$fn;
        $m = 'uploads/m-'.$fn;
        $l = 'uploads/l-'.$fn;
        File::Delete($sm);
        File::Delete($m);
        File::Delete($l);
        return true;		
	}

	public function add_info(Request $r){ 

		$user_info = new GemUserInfo;
		$information['header'] = $r->input("header");
		$information['content'] = $r->input("content");

		$information = json_encode($information);

		$input['information'] = $information;
		$input['user_id'] = $r->input('user_id');
		$user_info->updateOrCreate($input);

		return back();
	}

	public function motto(Request $r){ 

		$user_info = new GemUserInfo;
		$motto = $r->input("motto");

		$input['motto'] = $motto;
		$input['user_id'] = $r->input("user_id");
		$user_info->updateOrCreate($input);

		return back();
	}

}

