<?php namespace App\Http\Controllers\rubellite;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

use AppHelper;


class MasterfileController extends Controller
{

	public function index(){
		
	
		return view('rubellite.masterfile.index');

	}
	
	public function masterfile(Request $request,$mstlist){
		
		
		
		$MasterFileName =  $mstlist;
		$Test = array("\App\Http\Models" . chr(92) ."rubellite" .chr(92) . $MasterFileName);		
		$A = new $Test[0];
		//var_dump($A);
		$masterfilelist =  $A->where('status', '=' , 'enable')->get();
		
		
		
		return view('rubellite.masterfile.list-view')->with('MstRecords',$masterfilelist)->with('MstRecordsColumn',$A['displaycolumn'])->with('urllist', $mstlist)->with('Records',$A);
		
		
	}
	
	
	
	public function StaticViewerElement(){

		$MasterFileName =  "MstElements";
		$Test = array("\App\Http\Models" . chr(92) ."rubellite" .chr(92) . $MasterFileName);		
		$A = new $Test[0];
		//var_dump($A);
		$masterfilelist =  $A->where('status', '=' , 'enable')->get();
		return view('rubellite.masterfile.list-view-element')->with('MstRecords',$masterfilelist)->with('MstRecordsColumn',$A['displaycolumn'])->with('urllist', $MasterFileName)->with('Records',$A);
		
		
	}
	
		public function StaticViewerParameter(){

		$MasterFileName =  "MstParameters";
		$Test = array("\App\Http\Models" . chr(92) ."rubellite" .chr(92) . $MasterFileName);		
		$A = new $Test[0];
		//var_dump($A);
		$masterfilelist =  $A->where('status', '=' , 'enable')->get();
		
		
		
		return view('rubellite.masterfile.list-view-element')->with('MstRecords',$masterfilelist)->with('MstRecordsColumn',$A['displaycolumn'])->with('urllist', $MasterFileName)->with('Records',$A);
		
		
	}
	
	
	
	
	
	public function store(Request $request,$mstlist)
	{
		
		if(isset($request->ModelName))
		{
			$model = $request->ModelName;
		}
		
		$return_msg ="";
		$MasterFileName = ($mstlist);
		$table_model = array("\App\Http\Models" . chr(92) ."rubellite" .chr(92) . $MasterFileName);	
		$table_columns = new $table_model[0];
		/* $validateMstBrand =  $table_columns->where('name',strtolower($request->name))->count();		
				
		//echo $validateMstBrand;
		if 	($validateMstBrand > 0)
		{
			$return_msg = $return_msg  .  " Duplicate Entry " . $request->name ;
			$log_msg = $return_msg ;
			$masterfilelist =  $table_columns->where('status', '=' , 'enable')->get();
			//break;
		}
		
		else  */
		{
		
		$return_msg = $return_msg  .  $request->name  . " has successfully created" ;
		
	
		$masterfilelist = new $table_model[0];
		
				foreach($table_columns['fillable'] as $array_num => $table_column)
				{
					if(isset($request->$table_column))
						{
							$masterfilelist->$table_column = $request->$table_column;
						}
				}

				$masterfilelist->created_by = Auth::user()->id;
				$masterfilelist->updated_by = Auth::user()->id;
		
		
		$masterfilelist ->save();		
		$log_msg = $request->name  . " has successfully created "  . ucfirst($mstlist) . " MasterFile" ;
		
		
		
		}
		
		$Logger = new AppHelper;
		$Logger->SaveLog($log_msg);
		
		return back()->with('return_msg', $log_msg);
		
		
	} 
	
	
		public function update(Request $request,$mstlist)
    {	
	
	
		$return_msg ="";
		$MasterFileName = ucfirst($mstlist);
		$table_model = array("\App\Http\Models\bloodstone" . chr(92) .$MasterFileName);		
		$table_columns = new $table_model[0];
		$validateMstBrand =  $table_columns->where('name',strtolower($request->name))->count();		
				
		

		$masterfilelist = $table_columns::find($request->id);					
		
		
		
		//$masterfilelist = MstBrand::find($request->id);
		
				if($request->action == "edit")
				{
				$log_msg = $masterfilelist ->name . " has been change to " . $request->name . " from " . ucfirst($mstlist) . " MasterFile " ;
				
				foreach($table_columns['fillable'] as $array_num => $table_column)
				{
					if(isset($request->$table_column) && ($request->$table_column != $masterfilelist->$table_column))
						{
							$log_msg .= $masterfilelist->$table_column . " has been change to " . $request->$table_column;
							$masterfilelist->$table_column = $request->$table_column;
							
						}
				}
				

				$masterfilelist ->updated_by = (Auth::user()->name);
				
				
				}
				else if($request->action == "delete")
				{
				$log_msg =  $request->name . " have been deleted from " . ucfirst($mstlist) . " MasterFile" ;
				$masterfilelist ->updated_by = (Auth::user()->name);
				$masterfilelist ->status = "Disabled";
				}
				$masterfilelist->save();

		$Logger = new \AppHelper;
		$Logger->SaveLog($log_msg);
		
		return back()->with('return_msg', $log_msg);
		//return view('bloodstone.masterfile')->with('MstBrand',$masterfilelist)->with('urllist', $urllist);
		//return redirect('/bloodstone/masterfile/' . $mstlist)->with('MstBrand',$masterfilelist)->with('urllist', $mstlist)->with('return_msg', $log_msg);
		
	
		
		
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Batch Create File 
	public function batch(Request $request,$mstlist)
	{	$return_msg ="";
		$log_msg = "";
		if (strtolower($mstlist) == "brand") //(strtolower($mstlist) == "brand")
		{
		$data_list = array();
		$display ="" ;
		$data_list = json_decode($request->batchdata, true);
			 //Validate Data
			foreach($data_list["Sheet1"] as $larr_key => $larr_val)		
			{
				$validateMstBrand = MstBrand::where('name',strtolower($larr_val['Name']))->count();		
				
				//echo $validateMstBrand;
				if 	($validateMstBrand > 0)
				{
					$return_msg = $return_msg  .  " Duplicate Entry " . $larr_val['Name'] . ",";
					//break;
				}
			}
			
			
	
			// Save Data		
			if( $validateMstBrand <= 0)
			{	 foreach($data_list["Sheet1"] as $larr_key => $larr_val)		
				{
					//echo $larr_val['Name'];
					$masterfilelist = new \App\Http\Models\bloodstone\MstBrand;
					$masterfilelist ->name = strtolower($larr_val['Name']);
					$masterfilelist ->remarks = $larr_val['Remarks']; 
					$masterfilelist ->created_by = Auth::user()->name;
					$masterfilelist ->updated_by = Auth::user()->name;
					$masterfilelist ->save();
					
				} 
			} // if( $validateMstBrand > 0) */
		
		
		
		$urllist  = "brand";

		$masterfilelist = MstBrand::where('status', '=' , 'enable')->get();//MstBrand::All();
		
		}

		
		$Logger = new AppHelper;
		$Logger->SaveLog($log_msg);
		return redirect('/bloodstone/masterfile/' . $urllist)->with('MstBrand',$masterfilelist)->with('urllist', $urllist)->with('return_msg', $return_msg);
		
	} 
	

	

}