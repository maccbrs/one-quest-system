<?php namespace App\Http\Controllers\amethyst;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class approverController extends Controller
{


	public function index(){

		$GemUsers = new \App\Http\Models\amethyst\GemUsers;
		$GemApprover = new \App\Http\Models\amethyst\GemApprover;
		$help = new \App\Http\Controllers\amethyst\Helper;
		
		$items = $GemUsers
			->select('name','id')
			->where('active','=','1')
			->orderBy('name', 'asc')
			->get();	

		$approvers = $GemApprover->with(['emp','sup'])->get();

		return view('amethyst.approver.index',compact('help','items','approvers'));	

	}

	public function create(Request $r){

		$GemApprover = new \App\Http\Models\amethyst\GemApprover;
		$data = $r->all();
		$data['added_by'] = Auth::user()->id;
		$GemApprover->create($data);
		return redirect()->back();

	}

} 