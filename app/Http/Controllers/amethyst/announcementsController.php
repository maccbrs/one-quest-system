<?php namespace App\Http\Controllers\amethyst;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class announcementsController extends Controller
{


	public function index(){

		$help = new \App\Http\Controllers\amethyst\Helper;
		$GarnetAnnouncements = new \App\Http\Models\amethyst\GarnetAnnouncements;
		$items = $GarnetAnnouncements->source('noc')->where('status','<>',0)->orderBy('created_at','desc')->paginate(20);		
		$seen = $this->viewed($items);
		return view('amethyst.announcements.index',compact('items','seen'));
	}

	public function create(Request $r){

		$help = new \App\Http\Controllers\amethyst\Helper;
		$GarnetAnnouncements = new \App\Http\Models\amethyst\GarnetAnnouncements;
		$item = $r->all();
		$item['user_id'] = Auth::user()->id;
		$GarnetAnnouncements->create($item);
		return redirect()->back();

	} 

	public function get($id){

		$help = new \App\Http\Controllers\amethyst\Helper;
		$GarnetAnnouncements = new \App\Http\Models\amethyst\GarnetAnnouncements;
		$item = $GarnetAnnouncements->find($id);
		return view('amethyst.announcements.get',compact('item'));

	} 

	private function viewed($items){

		$help = new \App\Http\Controllers\amethyst\Helper;
		$return = [];
		$ids = [];
		foreach($items as $i):
			$ids[] = $i->id;
		endforeach;

		if($ids):
			$GarnetOpened = new \App\Http\Models\amethyst\GarnetOpened;
			$data = $GarnetOpened->whereIn('announcements_id',$ids)->get();
			foreach($data as $d):
				if(isset($return[$d->announcements_id])):
					$return[$d->announcements_id]++;
				else:
					$return[$d->announcements_id] = 1;
				endif;
			endforeach;
		endif;
		
		foreach($items as $item):
			if(!isset($return[$item->id])):
				$return[$item->id] = 0;
			endif;
		endforeach;

		return $return;

	}

	public function seen($id){

		$help = new \App\Http\Controllers\amethyst\Helper;
		$GarnetOpened = new \App\Http\Models\amethyst\GarnetOpened;
		$items = $GarnetOpened->where('announcements_id',$id)->with(['userObj'])->get();
		return view('amethyst.announcements.seen',compact('items'));

	}


} 