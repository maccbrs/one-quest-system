<?php namespace App\Http\Controllers\amethyst;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Alert;

class nocStatController extends Controller
{

	public function index(){

		$GarnetNoc = new \App\Http\Models\amethyst\GarnetNoc;

		$noc = $GarnetNoc
			->with('attendance')
			->orderBy('last_name','asc')
			->get();

		return view('amethyst.noc_stats.index',compact('noc'));	

	}

	public function attendance(Request $r){

		$data['late'] = $r->input('lates');
		$data['missed_logs'] = $r->input('missed_logs');

		$GarnetNocAttendance = new \App\Http\Models\amethyst\GarnetNocAttandace;
		$GarnetNocAttendance->where('noc_id','=', $r->input('noc_id'))->update($data);

		Alert::message('Attendance has been Updated.')->persistent('Close');

		return $this->index();

	}

} 