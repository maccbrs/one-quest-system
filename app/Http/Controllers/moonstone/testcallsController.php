<?php namespace App\Http\Controllers\moonstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;

class testcallsController extends Controller
{

	public function latest(){
		
		$GemTestCalls = new \App\Http\Models\moonstone\GemTestCalls;
		$help = new \App\Http\Controllers\moonstone\Helper;

		$item = $GemTestCalls->orderBy('created_at','desc')->with(['obj_testcalls'])->take(1)->first();
		//$help->pre($item);
		return view('moonstone.testcalls.latest',compact('item'));

	}

} 