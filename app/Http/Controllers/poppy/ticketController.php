<?php namespace App\Http\Controllers\poppy;

use App\Http\Controllers\poppy\accessController as Restrictions;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\poppy\Helper;

class ticketController extends Controller
{

	public function __construct(){

        $this->middleware('opal');

    }


	public function answered(){

		$Ticket = new \App\Http\Models\poppy\Ticket;
		$helper = new \App\Http\Controllers\poppy\Helper;
		$tickets = $Ticket->answered()->order('desc')->with(['subject','thread'])->get();
		return view('poppy.ticket.open',compact('tickets','helper')); 

	}

	public function open(){

		$Ticket = new \App\Http\Models\poppy\Ticket;
		$helper = new \App\Http\Controllers\poppy\Helper;
		$tickets = $Ticket->open()->order('desc')->with(['subject','thread'])->get();
		return view('poppy.ticket.open',compact('tickets','helper')); 

	}

	public function webdev(){
		$Ticket = new \App\Http\Models\poppy\Ticket;
		$helper = new \App\Http\Controllers\poppy\Helper;
		$tickets = $Ticket->where('closed',null)->whereIn('staff_id',[9,10])->order('desc')->with(['subject','thread'])->get();
		return view('poppy.ticket.webdev',compact('tickets','helper')); 	
	}


	public function create(){

		$restrictions = new Restrictions;
		return view('poppy.ticket.create',compact('restrictions'));
		
	}

	public function store(Request $r){

        $this->validate($r, [
            'title' => 'required',
            'content' => 'required'
        ]);	

        $title = $r->input('title');
        $content = $r->input('content');

		$user = Auth::user();
		$date = date('y-m-d H:i:s');

		$ueo = new \App\Http\Models\poppy\UserEmail;
		$to = new \App\Http\Models\poppy\Ticket;
		$tto = new \App\Http\Models\poppy\TicketThread;
		$cdo = new \App\Http\Models\poppy\TicketCdata;
		$email = $ueo->where('address',$user->email)->with(['user'])->first();
		
		if($email):

			$userid = $email->user_id;
			$emailid = $email->id;
			$poster = $email->user['name'];

		else:
			$uo = new \App\Http\Models\poppy\User;
			$newueo = $ueo->create([
						'user_id' => 0,
						'address' => $user->email
					]);
			$newuo = $uo->create([
				'org_id' => 0,
				'default_email_id' => $newueo->id,
				'status' => 0,
				'name' => $user->name,
				'created' => $date,
				'updated' => $date
				]);
			$newueo->user_id = $newuo->id;
			$newueo->save();

			$userid = $newuo->id;
			$emailid = $newueo->id;
			$poster = $newuo->name;

		endif;

		$uniqueid = $to->select('ticket_id')->orderBy('ticket_id','desc')->first();

		$newto = $to->create([
				'number' => sprintf("%06d", $uniqueid['ticket_id']),
				'dept_id' => 1,
				'status_id' => 1,
				'user_id' => $userid,
				'lastmessage' => $date,
				'lastresponse' => $date,
				'ip_address' => '::1',
				'created' => $date,
				'updated' => $date
			]);

		$newtto = $tto->create([
					'ticket_id' => $newto->id,
					'user_id' => $userid,
					'poster' => $poster,
					'thread_type' => 'M',
					'title' => $title,
					'body' => $content,
					'format' => 'html',
					'ip_address' => '::1',
					'created' => $date,
					'updated' => $date
				]);

		$cdo->create([
			'ticket_id' => $newto->id,
			'subject' => $title,
			'priority' => 2
			]);

		return redirect()->route('poppy.index');
		
	}

	public function reply(Request $r,$ticketid,$userid){

		$user = Auth::user();

        $this->validate($r, [
            'title' => 'required',
            'content' => 'required'
        ]);	

        $date = date('Y-m-d H:i:s');
        $tto = new \App\Http\Models\poppy\TicketThread;
        $tto->create([
        	'pid' => 0,
        	'ticket_id' => $ticketid,
        	'user_id' => $userid,
        	'staff_id' => 0,
        	'thread_type' => 'M',
        	'poster' => $user->name,
        	'title' => $r->input('title'),
        	'body' => $r->input('content'),
        	'format' => 'html',
        	'ip_address' => '::1',
        	'created' => $date,
        	'updated' => $date
        ]);

        $to = new \App\Http\Models\poppy\Ticket;
        $to->where('ticket_id',$ticketid)->update([
        	'lastmessage' => $date,
        	'lastresponse' => $date
        	]);

        return redirect()->route('poppy.index');

	}

}