<?php namespace App\Http\Controllers\poppy;



class Helper 
{

	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}

	public function lastEntry($entries){
		$count = count($entries) - 1;

		return $entries[$count];
	}

	public function sortByDate($obj){
		
		$obj = $obj->toArray();
		usort($obj,function( $a, $b ) {
		    return strtotime($b['created']) - strtotime($a['created']);
		});
		return $obj;

	}		

}