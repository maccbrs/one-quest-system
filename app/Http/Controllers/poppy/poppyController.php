<?php namespace App\Http\Controllers\poppy;

use App\Http\Controllers\poppy\accessController as Restrictions;
use App\Http\Controllers\Controller;
use Auth;

class poppyController extends Controller
{

	public function __construct(){

        $this->middleware('opal');

    }


	public function index(){

		$user = Auth::user();
		$restrictions = new Restrictions;
		$obj = new \App\Http\Models\poppy\UserEmail;
		$ticketObj = new \App\Http\Models\poppy\Ticket;
		$ticketuser = $obj->owner($user->email)->first();
		$tickets = ($ticketuser?$ticketObj->owner($ticketuser->user_id)->order('desc')->with(['threads','subject'])->paginate(5):[]); //pre($tickets->toArray());
		return view('poppy.index',compact('restrictions','tickets'));
		
	}

} 