<?php namespace App\Http\Controllers\ruby;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class auditsController extends Controller
{

	public function __construct(){

        $this->middleware('ruby');

    }

	public function index($id){

		$help = new \App\Http\Controllers\gem\Helper;
		$Audit = new \App\Http\Models\ruby\Audit;
		$audit = $Audit->where('id',$id)->with(['closer','recordings','set'])->first();
	//	$help->pre($audit);
		return view('ruby.audits.index',compact('audit'));
		
	}

} 