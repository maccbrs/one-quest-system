<?php namespace App\Http\Controllers\ruby;



class Helper {

	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}

	public function accountFields(){

		return [
			'contact_information',
			'service_coverage',
			'documents_provided',
			'tech_requirements',
			'recruitment_requirements',
			'operational_requirements'
		];

	}

	public function properName(){

		return [
			'contact_information' => 'Contact Information',
			'service_coverage' => 'Service Coverage',
			'documents_provided' => 'Document Provided',
			'tech_requirements' => 'Technical Requirements',
			'recruitment_requirements' => 'Recruitment Recruitments',
			'operational_requirements' => 'Operational Requirements',
			'project_name' => 'Project Name',
			'project_alias' => 'Project Alias',
			'fst_training_date' => 'FST Training Date',
			'product_training_date' => 'Product Training Date',
			'program_launch_date' => 'Program Launch Date',
			'project_status' => 'Project Status',
			'billing_cycle' => 'Billing Cycle',
			'commercial_arrangement' => 'Commercial Arrangement',
			'type_of_program' => 'Type of Program',
			'project_description' => 'Project Description',
			'agent_headcount' => 'Agent Headcount',
			'operational_location' => 'Operational Location',
			'est_call_volume' => 'Est Call Volume',
			'est_avg_handle_time' => 'EST avg Handle Time',
			'train_requirements' => 'Train Requirements'
		];

	}	

	public function assembleFields($fields,$account,$fieldvalues){

		$proper = $this->properName();
		$except = ['id','createdAt','updatedAt'];

		$a = [];
		foreach($fields as $field):
			$a[$field->field_name][] = $field->toArray(); 
		endforeach;

		$arr = [];

		if($account):
			foreach($account->toArray() as $k => $v):
				if(isset($a[$k])):
					$arr[$k] = $a[$k];
				else:
					$arr[$k] = $v;
				endif;
			endforeach;
		endif;

		$newarr = [];
		$fvArr = [];

		if($fieldvalues->count()):
			$fv = $fieldvalues->toArray();
			foreach($fv as $k => $v):
				$fvArr[$v['field_name']] = $v['value'];
			endforeach;
		endif;

		foreach($arr as $ak => $av):

			if(!in_array($ak,$except)):
				$cat = (isset($proper[$ak])?$proper[$ak]:$ak);
				if(is_array($av)):
						$title = $cat;
					foreach($av as $bk => $bv):
						$newarr[] = [
							'cat' => $title,
							'sub' => $bv['label'],
							'ik' => $arr['id'].'||'.$bv['key'].'||0',
							'value' => (isset($fvArr[$bv['key']])?$fvArr[$bv['key']]:'')
					    	];
					    $title = '';
					endforeach;
				else:
					$newarr[] = [
						'cat' => $cat,
						'sub' => '',
						'ik' => $arr['id'].'||'.$ak.'||1',
						'value' => $av
				    ];
				endif;
			endif;

		endforeach;

		return $newarr; 
	}

	public function bounds($row){

		$rowArr = ($row == ''?[]: explode(',',$row));
		$defaults = ['inbound','outbound','chat','email'];

		$newArr = [];
		foreach($defaults as $v):
			if(in_array($v,$rowArr)):
				$newArr[$v] = 1;
			else:
				$newArr[$v] = 0;
			endif;
		endforeach;

		return $newArr;

	}

	public function auditorFormat($str){

		if($str == '') return '';
		
		$a = explode('_',$str);
		unset($a[0]);
		return implode(' ', $a);

	}

	public function evaluator($id){

		if($id):
			$Users = new \App\Http\Models\gem\User;
			$users = $Users->evaluator()->get();
			
			foreach($users as $u){
				if($u->id == $id){
					return ucwords($u->name);
				}
			}
		else:
			return 'None';
		endif;

	}

}