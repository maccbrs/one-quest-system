<?php namespace App\Http\Controllers\ruby;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class complianceController extends Controller
{

	public function __construct(){

        $this->middleware('ruby');

    }

	public function index(){

		$Users = new \App\Http\Models\gem\User;
		$help = new \App\Http\Controllers\gem\Helper;
		$users = $Users->evaluator()->get();
		return view('ruby.compliance.index',compact('users'));

	}


	public function user($id){

		$Compliance = new \App\Http\Models\ruby\Compliance;
		$items = $Compliance->user($id)->get();
		return view('ruby.compliance.user',compact('id','items'));
	}


	public function add($id){

		$help = new \App\Http\Controllers\gem\Helper;
		$Closer = new \App\Http\Models\ruby\RecordingsCloser;
		$Recordings = new \App\Http\Models\ruby\Recordings; 
		$closer = $Closer->campaigns()->get(); 
		$recordings = $Recordings->campaigns()->get();	
		$agent_closer = $Closer->agent()->Notin()->get();
		$agent_recordings = $Recordings->agent()->Notin()->get();
		//$help->pre($agent_closer);
		return view('ruby.compliance.add',compact('closer','recordings','agent_closer','agent_recordings','id'));

	} 

	public function create(Request $r,$id){

        $this->validate($r, [
            'title' => 'required'
        ]);	

		$help = new \App\Http\Controllers\gem\Helper;
		$Compliance = new \App\Http\Models\ruby\Compliance;
		
		foreach($r->all() as $key => $val):
			if($key != '_token'):
				if(is_array($val)):
					$data[$key] = json_encode($val);
				else:
					$data[$key] = $val;
				endif;
			endif;
		endforeach;

		$data['user'] = $id;
		$Compliance->create($data);
		
		return redirect()->route('ruby.compliance.user',$id);
		

	}

	public function detail($id){

		$help = new \App\Http\Controllers\gem\Helper;
		$Compliance = new \App\Http\Models\ruby\Compliance;
		$item = $Compliance->find($id);
		return view('ruby.compliance.detail',compact('item'));
		
	}

}
