<?php namespace App\Http\Controllers\ruby;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class campaignsController extends Controller
{

	public function __construct(){

        $this->middleware('ruby');

    }

	public function index(){ 

		$help = new \App\Http\Controllers\gem\Helper;
		$Closer = new \App\Http\Models\ruby\RecordingsCloser;
		$Recordings = new \App\Http\Models\ruby\Recordings;
		$closer = $Closer->campaigns()->get();



		$recordings = $Recordings->campaigns()->get();
		return view('ruby.campaigns.index',compact('closer','recordings'));
	}


	public function inbound($id){



		$Closer = new \App\Http\Models\ruby\RecordingsCloser;
		$help = new \App\Http\Controllers\ruby\Helper;
		$closer = $Closer->where('campaign_id',$id)->Notin()->orderBy('call_date','desc')->paginate(10);

		return view('ruby.campaigns.inbound',compact('closer','help'));

	}

	public function outbound($id){

		$Recordings = new \App\Http\Models\ruby\Recordings;
		$help = new \App\Http\Controllers\ruby\Helper;
		$recordings = $Recordings->where('campaign_id',$id)->Notin()->orderBy('call_date','desc')->paginate(10);
		return view('ruby.campaigns.outbound',compact('recordings','help'));

	}

}
