<?php namespace App\Http\Controllers\ruby;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class agentsController extends Controller
{

	public function __construct(){

        $this->middleware('ruby');

    }

	public function index(){ 

		$help = new \App\Http\Controllers\gem\Helper;
		$Closer = new \App\Http\Models\ruby\RecordingsCloser;
		$Recordings = new \App\Http\Models\ruby\Recordings;
		$closer = $Closer->agent()->Notin()->get();
		$recordings = $Recordings->agent()->Notin()->get();
		return view('ruby.agents.index',compact('closer','recordings'));
	}


	public function inbound($id){


		$Closer = new \App\Http\Models\ruby\RecordingsCloser;
		$help = new \App\Http\Controllers\ruby\Helper;
		$closer = $Closer->where('agent',$id)->Notin()->orderBy('call_date','desc')->paginate(10);

		return view('ruby.campaigns.inbound',compact('closer','help'));

	}

	public function outbound($id){

		$Recordings = new \App\Http\Models\ruby\Recordings;
		$help = new \App\Http\Controllers\ruby\Helper;
		$recordings = $Recordings->where('agent',$id)->Notin()->orderBy('call_date','desc')->paginate(10);
		return view('ruby.campaigns.outbound',compact('recordings','help'));

	}

	public function results(){
		$help = new \App\Http\Controllers\gem\Helper;
		$Closer = new \App\Http\Models\ruby\RecordingsCloser;
		$Recordings = new \App\Http\Models\ruby\Recordings;
		$closer = $Closer->agent()->Notin()->get();
		$recordings = $Recordings->agent()->Notin()->get();
		return view('ruby.agents.results',compact('closer','recordings'));
	}

	public function result($bound,$agent){
		
		$help = new \App\Http\Controllers\ruby\Helper;
		if($bound == 'inbound'):
			$Closer = new \App\Http\Models\ruby\RecordingsCloser;
			$results = $Closer->where('agent',$agent)->week()->orderBy('year','desc')->orderBy('week_no','desc')->paginate(10);
		else:
			$Recordings = new \App\Http\Models\ruby\Recordings;
			$results = $Recordings->where('agent',$agent)->week()->orderBy('year','desc')->orderBy('week_no','desc')->paginate(10);
		endif;

		$counts = $this->weekly_counts($results,$agent,$bound);
		return view('ruby.agents.result',compact('results','agent','bound','counts')); 
		
	}

	public function week($bound,$agent,$weekno){

		$help = new \App\Http\Controllers\ruby\Helper;
		if($bound == 'inbound'):
			$Closer = new \App\Http\Models\ruby\RecordingsCloser;
			$results = $Closer->where('agent',$agent)->where('week_no',$weekno)->where('locked',1)->orderBy('call_date','desc')->paginate(10);
		else:
			$Recordings = new \App\Http\Models\ruby\Recordings;
			$results = $Recordings->where('agent',$agent)->where('week_no',$weekno)->where('locked',1)->orderBy('call_date','desc')->paginate(10);
		endif;
		return view('ruby.agents.week',compact('results','bound','help'));

	}

	public function weekly_counts($results,$agent,$bound){

		$Closer = new \App\Http\Models\ruby\RecordingsCloser;
		$Recordings = new \App\Http\Models\ruby\Recordings;		
		$help = new \App\Http\Controllers\gem\Helper;
		$return = [];
		$weeks = [];
		foreach($results as $r):
			$weeks[] = $r->week_no;
		endforeach;	

		if($weeks):
			if($bound == 'inbound'):
				$results = $Closer->where('agent', $agent)->whereIn('week_no',$weeks)->where('locked',1)->get();
			else:
				$results = $Recordings->where('agent', $agent)->whereIn('week_no',$weeks)->where('locked',1)->get();
			endif;

			if($results):
				foreach($results as $r):
					if(isset($return[$r->week_no])):
						$return[$r->week_no]++;
					else:
						$return[$r->week_no] = 1;
					endif;
				endforeach;				
			endif;
		endif;

		return $return;
			
	}


}  