<?php namespace App\Http\Controllers\ruby;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class searchController extends Controller

{	

	public function __construct(){

        $this->middleware('ruby');

    }


	public function index(){

		$help = new \App\Http\Controllers\ruby\Helper;
		return view('ruby.audit.search');
	}

	public function search(Request $r){

		$this->validate($r, [
		    'keyword' => 'required',
		    'bound' => 'required',
		    'daterange' => 'required',
		]);

		return redirect()->route('ruby.audit.result',['key'=>$r->keyword,'daterange'=>$r->daterange,'bound'=>$r->bound]);

	}	

	public function result($key,$daterange,$bound){

		$date_range = explode(' - ',$daterange); 

		$help = new \App\Http\Controllers\ruby\Helper;
		if($bound == 'inbound'):
			$Model = new \App\Http\Models\ruby\RecordingsCloser;
		else:
			$Model = new \App\Http\Models\ruby\Recordings;
		endif;
		$result = $Model->search($key)->whereBetween('call_date', [$date_range[0],$date_range[1]])->paginate(20);
		//$help->pre($result);
		return view('ruby.audit.search',compact('result','help'));	

	}

}