<?php namespace App\Http\Controllers\ruby;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class auditController extends Controller
{

	public function __construct(){

        $this->middleware('ruby');

    }

	public function get(Request $r,$id){


		$Sessions = new \App\Http\Models\ruby\Sessions;

		$token = [
			'token' => uniqid(),
			'content' => json_encode(['bound' => $r->get('bound'),'account' => $r->get('account'),'id' => $id,'user' => Auth::user()->id])
		];
		$a = $Sessions->create($token);
		return view('ruby.audit.get',compact('token','bound','account'));

	}

}