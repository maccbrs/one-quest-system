<?php namespace App\Http\Controllers\ruby;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;


class setController extends Controller
{

	public function __construct(){

        $this->middleware('ruby');

    }

	public function index($id){ 
		
		$help = new \App\Http\Controllers\gem\Helper;
		$Set = new \App\Http\Models\ruby\QaSettings;
		$Audit = new \App\Http\Models\ruby\Audit;
		$audits = [];
		$set = $Set->find($id);

		if($set->bound == 'inbound'):
			$audits = $Audit->where('set_id',$id)->orderBy('id','desc')->with(['closer'])->paginate(5);
		endif;

		if($set->bound == 'outbound'):
			$audits = $Audit->where('set_id',$id)->orderBy('id','desc')->with(['recordings'])->paginate(5);
		endif;
		
		//$help->pre($audits);
		
		return view('ruby.set.index',compact('set','audits'));

	}

}  