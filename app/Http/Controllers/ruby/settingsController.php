<?php namespace App\Http\Controllers\ruby;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class settingsController extends Controller
{

	public function __construct(){

        $this->middleware('ruby');

    }

	public function index(){

		$help = new \App\Http\Controllers\ruby\Helper;
		$fields = $help->accountFields();
		return view('ruby.settings.index',compact('fields'));
	}


	public function accountFields(){

		$AccountFields = new \App\Http\Models\ruby\AccountFields;
		$help = new \App\Http\Controllers\ruby\Helper;
		$fields = $help->accountFields();

		$items = $AccountFields->get();
		return view('ruby.settings.account-fields',compact('items','fields'));

	}

	public function accounts(){

		$Accounts = new \App\Http\Models\ruby\Accounts;
		$accounts = $Accounts->get();
		return view('ruby.accounts.index',compact('accounts')); 

	}

	public function account_create(Request $r){
		$help = new \App\Http\Controllers\gem\Helper;
		$Accounts = new \App\Http\Models\ruby\Accounts;
		$Accounts->create(['project_name' => $r->campaign]);
		return redirect()->back();
	}

	public function accountEdit($id){

		$help = new \App\Http\Controllers\ruby\Helper;
		$Accounts = new \App\Http\Models\ruby\Accounts;
		$AccountFields = new \App\Http\Models\ruby\AccountFields;
		$AccountFieldsValues = new \App\Http\Models\ruby\AccountFieldsValues;
		$account = $Accounts->find($id);
		$fieldvalues = $AccountFieldsValues->where('account_id',$id)->get();
		$row = $help->assembleFields($AccountFields->get(),$account,$fieldvalues);
		return view('ruby.settings.account-edit',compact('row'));

	}

	public function accountsQaSettings($id){

		return view('ruby.qa.sets',compact('id'));

	}

	public function accountsQaSettingsEdit($id){

		$QaSettings = new \App\Http\Models\ruby\QaSettings;
		$help = new \App\Http\Controllers\ruby\Helper;
		$row = $QaSettings->find($id);
		$ingroup = explode(',', $row->ingroups);
		$agent = explode(',', $row->agents);
		$auditor = explode(',', $row->auditor);
		$dispo = explode(',', $row->disposition);
	
		return view('ruby.qa.sets-edit',compact('id','row','help','ingroup','agent','auditor','dispo'));
		
	}

	public function auditors(){

		return view('ruby.settings.auditors');
 
	}


	public function auditor($id){

		return view('ruby.settings.auditor',compact('id'));
 
	}

	public function accounts_index(){

		$Campaigns = new \App\Http\Models\ruby\Campaigns;
		$campaigns = $Campaigns->get();
		return view('ruby.settings.accounts_index',compact('campaigns')); 
	}

	public function accounts_create(){

		$help = new \App\Http\Controllers\gem\Helper;
		$Closer = new \App\Http\Models\ruby\RecordingsCloser;
		$Recordings = new \App\Http\Models\ruby\Recordings;
		$closer = $Closer->campaigns()->get(); 
		$recordings = $Recordings->campaigns()->get();		
		return view('ruby.settings.accounts_create',compact('closer','recordings'));

	}

	public function accounts_post(Request $r){

		$help = new \App\Http\Controllers\gem\Helper;
		$Campaigns = new \App\Http\Models\ruby\Campaigns;
		$campaigns = [];

		if($r->bound == 'inbound' && isset($r->inbounds)):
				$campaigns = $r->inbounds;
		else:
			if(isset($r->outbounds)):
				$campaigns = $r->outbounds;
			endif;
		endif;

		$result = $Campaigns->create([
			'title' => $r->title,
			'bound' => $r->bound,
			'campaigns' => json_encode($campaigns)
		]);
		$help->pre($result);

	}
	
	public function accounts_read($id){

		$Campaigns = new \App\Http\Models\ruby\Campaigns;
		$help = new \App\Http\Controllers\gem\Helper;
		$campaign = $Campaigns->find($id);
		return view('ruby.settings.accounts_read',compact('campaign'));

	}

}