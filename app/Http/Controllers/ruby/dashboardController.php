<?php namespace App\Http\Controllers\ruby;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class dashboardController extends Controller
{

	public function __construct(){

        $this->middleware('ruby');

    }

	public function index(){

		return view('ruby.dash.index');
	}


}
