<?php namespace App\Http\Controllers\ruby;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class randomizerController extends Controller
{

	public function __construct(){

        $this->middleware('ruby');

    }

	public function index(){

		$QaSets = new \App\Http\Models\ruby\QaSettings;
		$sets = $QaSets->with(['account'])->get();

		return view('ruby.randomizer.index',compact('sets'));

	}


}
