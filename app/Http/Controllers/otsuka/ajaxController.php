<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;


class ajaxController extends Controller
{

	public function index(){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->orderBy('created_at','asc')->get();
		$tn = date("Y-m-d H:i:s");


		return view('otsuka.dashboard.queue',compact('Queue','Upload','Mr','Px','tn'));
	}
	
	
	public function update_upload_info(Request $request){
		$med_upload = new \App\Http\Models\otsuka\Upload;
		if($request->mode == 'update')
		{
		$upload_record = $med_upload::find($request->ref_upload_id);
		$upload_record->lastname = $request->upload_last_name;
		$upload_record->firstname = $request->upload_first_name;
		$upload_record->middlename = $request->upload_middle_name;
		$upload_record->nickname = $request->upload_nickname;
		$upload_record->gender = $request->upload_gender;				
		$upload_record->birthdate = $request->upload_birthdate;				
		$upload_record->age = $request->upload_age;				
		$upload_record->address = $request->upload_address;				
		$upload_record->mobile_number_2 = $request->upload_mobile_number_2;				
		$upload_record->time_to_call = $request->upload_time_to_call;				
		$upload_record->verification_remarks = $request->upload_verification_remarks;				
		$upload_record->save();
		echo "test";
		}
		
		
		//$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->orderBy('created_at','asc')->get();
		//$tn = date("Y-m-d H:i:s");
		//return view('otsuka.dashboard.queue',compact('Queue','Upload','Mr','Px','tn'));
	}
	
	

	
	public function searchPerson(Request $request){
		$searching = $request->person;
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$doctors_list = new \App\Http\Models\otsuka\DoctorsModel;
	    $national_md_list = new \App\Http\Models\otsuka\National_md_list;

		$Mr = $med_rep->take(50)->get();		
		$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->orderBy('created_at','asc')->get();
		$tn = date("Y-m-d H:i:s");
		$person = $Queue;
		if(($request->person) == "doctors")
		{
		$doctors = $national_md_list::where('mdname', 'like', '%' . $request->person_filter .'%')->take(2000)->get();
		return view('otsuka.dashboard.searchperson',compact('Queue','Upload','Mr','Px','tn','person','searching','doctors'));		
	
		}
		else if(($request->person) == "patient")			
		{
		//$Queue = $patient->where('patient_lastname', 'like', '%T%')
		$Queue = $patient::where('patient_lastname', 'like', '%' . $request->person_filter .'%')->orWhere('patient_fullname', 'like', '%' . $request->person_filter .'%')->orWhere('patient_firstname', 'like', '%' . $request->person_filter .'%')->orWhere('patient_kit_number', 'like', '%' . $request->person_filter .'%')->orWhere('patient_code', 'like', '%' . $request->person_filter .'%')->take(2000)->get();
			
		return view('otsuka.dashboard.searchperson',compact('Queue','Mr','Px','tn','person','searching'));			
		}
		else if(($request->person) == "queue")			
		{
		//$Queue = $patient->where('patient_lastname', 'like', '%T%')
		$Queue = $med_upload::where('id',substr($request->person_filter, -7))->where('active',1)->get();
			
		return view('otsuka.dashboard.searchperson',compact('Queue','Mr','Px','tn','person','searching'));			
		}
		else if(($request->person) == "upload_queue")			
		{
			if(Auth::user()->user_level == 2)  // COndition FOR TL VIEW Display ALL INCOMING UPLOAD
			{
			$Queue = $med_upload->whereBetween('trail_status',[100,198])->where('active',1)->where('submit',0)->with(['fetch_trail_status','encoded_by','user'])->orderBy('created_at','asc')->get();
			}
			else// COndition FOR AGENT VIEW Display And Claim INCOMING UPLOAD
			{
			$Queue = $med_upload->whereBetween('trail_status',[100,198])->where('active',1)->where('submit',0)->where(function($query){$query->where('claimed',0)->orWhere('claimed_by',Auth::user()->id)->orWhere('viber_queue',1);})->orderBy('created_at','asc')->get();
			}

			$tn = date("Y-m-d H:i:s");
			
		return view('otsuka.dashboard.searchperson',compact('Queue','Mr','Px','tn','person','searching'));			
		}
		else if(($request->person) == "verification_queue")			
		{
			$Queue = $med_upload->whereBetween('trail_status',[200,210])->where('active',1)->where('verify_submit',0)->where(function($query){$query->where('verify_claimed',0)->orWhere('verify_claimed_by',Auth::user()->id);})->with(['fetch_call','encoded_by','verification_claimed_by'])->orderBy('created_at','asc')->get();

			$tn = date("Y-m-d H:i:s");
			$tn = date("Y-m-d H:i:s");
			
		return view('otsuka.dashboard.searchperson',compact('Queue','Mr','Px','tn','person','searching'));			
		}
		else if(($request->person) == "verification_callback")			
		{
		$Queue = $med_upload->whereBetween('trail_status',[211,299])->where('active',1)->where(function($query){$query->where('verify_claimed',0)->orWhere('verify_claimed_by',Auth::user()->id);})->where('verify_submit',0)->whereIn('callback_date',[date('Y-m-d')])->with(['fetch_call_queue','fetch_status','encoded_by','verification_claimed_by','encoded_by'])->orderBy('created_at','asc')->get();

		//pre($Queue);

			$tn = date("Y-m-d H:i:s");
			$now = date("Y-m-d");
			$tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

			$count = $med_upload->whereBetween('trail_status',[211,299])->where('active',1)->where(function($query){$query->where('verify_claimed',0)->orWhere('verify_claimed_by',Auth::user()->id);})->where('verify_submit',0)->whereIn('callback_date',[date('Y-m-d'),$tomorrow])->with(['fetch_call_queue','fetch_status','encoded_by','verification_claimed_by','encoded_by'])->orderBy('created_at','asc')->count();

		return view('otsuka.dashboard.searchperson',compact('Queue','Mr','Px','tn','person','searching','now','tomorrow','count'));			
		}
		else if(($request->person) == "upload")			
		{
		//$Queue = $patient->where('patient_lastname', 'like', '%T%')
		$Queue = $med_upload->where('patient_name', 'like', '%' . $request->person_filter .'%')->orWhere('patient_kit_number', 'like', '%' . $request->person_filter .'%')->where('active',1)->with(['fetch_status','encoded_by','verification_claimed_by'])->get();
			
		return view('otsuka.dashboard.searchperson',compact('Queue','Mr','Px','tn','person','searching'));			
		}
		else if(($request->person) == "globalmd")			
		{
		//$Queue = $patient->where('patient_lastname', 'like', '%T%')
				$globalmd = $national_md_list::where('mdname', 'like', '%' . $request->person_filter .'%')->take(2000)->get();
				
			
		return view('otsuka.dashboard.searchperson',compact('Queue','Mr','Px','tn','person','searching','globalmd'));			
		}
		else if(($request->person) == "filteredmd")			
		{
		//$Queue = $patient->where('patient_lastname', 'like', '%T%')
		$filteredmd = $national_md_list::where('mdname', 'like', '%' . $request->person_filter .'%')->where('empcode','like', '%'.$request->emp_code.'%')->take(2000)->get();
			
		return view('otsuka.dashboard.searchperson',compact('Queue','Mr','Px','tn','person','searching','filteredmd'));			
		}
		else if(($request->person) == "globalmdv2")			
		{
		//$Queue = $patient->where('patient_lastname', 'like', '%T%')
				$globalmd = $national_md_list::where('mdname', 'like', '%' . $request->person_filter .'%')->orWhere('doctorid', 'like', '%' . $request->person_filter .'%')->take(2000)->get();
				
			
		return view('otsuka.dashboard.searchperson',compact('Queue','Mr','Px','tn','person','searching','globalmd'));			
		}
		else if(($request->person) == "filteredmdv2")			
		{
		//$Queue = $patient->where('patient_lastname', 'like', '%T%')
		$filteredmd = $national_md_list::where('mdname', 'like', '%' . $request->person_filter .'%')->where('empcode','like', '%'.$request->emp_code.'%')->take(2000)->get();
			
		return view('otsuka.dashboard.searchperson',compact('Queue','Mr','Px','tn','person','searching','filteredmd'));			
		}
		
		 else if(($request->person) == "official_reciept")			
		{
			
			
			$Or = new \App\Http\Models\otsuka\Encoded_Purchases_Validated;
			$searching = $request->person;
			
			$Queue = $Or::where('or_number', 'like',  '%' . $request->person_filter .'%')->take(2000)->with('fetch_encoded_dtl')->get();
			
			return view('otsuka.dashboard.searchperson',compact('Queue','person','searching'));			
		
		} 

		 else if(($request->person) == "receipt_unloaded")			
		{
			
			
			$Or = new \App\Http\Models\otsuka\Encoded_Purchases;
			$searching = $request->person;
			
			$Queue = $Or::where('or_number', 'like',  '%' . $request->person_filter .'%')->take(2000)->with('fetch_encoded_dtl')->get();
			
			return view('otsuka.dashboard.searchperson',compact('Queue','person','searching'));			
		
		} 
		
	}
	
	
	public function searchPatient(Request $request){
		$searching = $request->person;
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$doctors_list = new \App\Http\Models\otsuka\DoctorsModel;
	    $national_md_list = new \App\Http\Models\otsuka\National_md_list;

		$Mr = $med_rep->take(50)->get();		
		$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->orderBy('created_at','asc')->get();
		$tn = date("Y-m-d H:i:s");
		$person = $Queue;
		if(($request->person) == "patient")			
		{
		//$Queue = $patient->where('patient_lastname', 'like', '%T%')
		$Queue = $patient::where('patient_lastname', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_fullname', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_firstname', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_kit_number', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_code', 'like', '%' . $request->person_filter .'%')
				->orWhere('mobile_number', 'like', '%' . $request->person_filter .'%')
				->orWhere('mobile_number_2', 'like', '%' . $request->person_filter .'%')
				->orWhere('address', 'like', '%' . $request->person_filter .'%')
				->orWhere('phone_number', 'like', '%' . $request->person_filter .'%')->take(5000)->get();
			
		return view('otsuka.dashboard.redemptionsearch',compact('Queue','Mr','Px','tn','person','searching'));			
		}
				
		 else if(($request->person) == "official_reciept")			
		{
			
			
			$Or = new \App\Http\Models\otsuka\Encoded_Purchases;
			$searching = $request->person;
			
			$Queue = $Or::where('or_number', 'like',  '%' . $request->person_filter .'%')->take(500)->with('fetch_encoded_dtl')->get();
			
			return view('otsuka.dashboard.redemptionsearch',compact('Queue','person','searching'));			
		
		} 
		
	}
	
	
	public function searchPatient_only(Request $request){
		$searching = $request->person;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;			
		$tn = date("Y-m-d H:i:s");
		
		if(($request->person) == "patient")			
		{
		//$Queue = $patient->where('patient_lastname', 'like', '%T%')
		$Queue = $patient::where('patient_lastname', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_fullname', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_firstname', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_kit_number', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_code', 'like', '%' . $request->person_filter .'%')
				->orWhere('mobile_number', 'like', '%' . $request->person_filter .'%')
				->orWhere('mobile_number_2', 'like', '%' . $request->person_filter .'%')
				->orWhere('phone_number', 'like', '%' . $request->person_filter .'%')
				->orWhere('address', 'like', '%' . $request->person_filter .'%')
				->take(2000)->get();
			
		return view('otsuka.dashboard.patient-search',compact('Queue','tn','person','searching'));			
		}
				
		
		
	}
	
	
	
	public function displaypatient(Request $request){
		
		$patient = new \App\Http\Models\otsuka\Patient_Validated;			
		//$tn = date("Y-m-d H:i:s");
		$sku = new \App\Http\Models\otsuka\Sku;
		$Mgabilify = $sku->where('is_active_in_oqs',1)->get();
		$patient_record = $patient->with('fetch_sku')->find($request->refid);
		$Patients_kit_allocation_reserve = new \App\Http\Models\otsuka\Patients_kit_allocation_reserve;
		$Reserve_Patient_Kit = $Patients_kit_allocation_reserve->where('emp_code','=','')->take(20)->get();
		echo "test";
		//pre($patient_record);
		return view('otsuka.dashboard.patient-edit',compact('patient_record','Reserve_Patient_Kit','Mgabilify'));				
		
		
	}
	
	
	
	
	
	
	
	

	
		public function reject_upload(Request $request){
		echo "test";
		$med_upload = new \App\Http\Models\otsuka\Upload;
		
		$Queue = $med_upload::find($request->id);
		$Queue->viber_queue =2;
		$Queue->status ="Rejected";
		$Queue->save();
		


		//return view('otsuka.dashboard.queue',compact('Queue','Upload','Mr','Px','tn'));
	}
	
	
	public function get_supp_upload(Request $request){
		 $UploadSupportingDocs = new \App\Http\Models\otsuka\UploadSupportingDocs;
		 $record = $UploadSupportingDocs::find($request->id);
		 
		 
		 echo json_encode($record);
		 
		
		
	}
	
	
	public function get_national_md_record(Request $request){
		 $national_md_list = new \App\Http\Models\otsuka\National_md_list;
		 $md_record = $national_md_list::find($request->national_md_id);
		 
		 
		 echo json_encode($md_record);
		 
		
		
	}

	public function get_upload_record(Request $request){
		 $med_upload = new \App\Http\Models\otsuka\Upload;
		 
		 $Queue = $med_upload::find($request->id);

		 echo json_encode($Queue);
		 
		
		
	}
	
	public function get_patient_kit_record(Request $request){
		 $national_md_list = new \App\Http\Models\otsuka\Patients_kit_allocation;
		 $md_record = $national_md_list::find($request->national_md_id);
		 
		 
		 echo json_encode($md_record);
		 
		
		
	}
	

	



}