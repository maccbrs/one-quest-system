<?php namespace App\Http\Controllers\otsuka;
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type: application/json');

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;


class mtdredemptionreportController extends Controller
{

	public function index(){

        

		return view('otsuka.dashboard.test-page',compact('url','token'));
	}
	
	public function mtd_report(Request $request){
		$Ytd_Redemption = new \App\Http\Models\otsuka\Ytd_Redemption;
		$Update_patient_modal = new \App\Http\Models\otsuka\Patient_Validated;
		//$from = '2018-02-03 08:56:18';		
		//$to = '2018-02-07 11:04:38';

		$dt = date('Y-m-d');
		$first_day = date("Y-m-01", strtotime($dt)).' 00:00:00';
		//$first_day = date("2018-04-01", strtotime($dt)).' 00:00:00';
        
		$mtd_start = $first_day;// '2018-05-01 00:00:00';
		//$mtd_start = '2018-03-01 00:00:00';
		
        $now = date('Y-m-d').' 20:30:00';
        //$now = '2018-03-31 23:59:59';
		if(!empty($request->from))
		{
			$from = $request->from;
		}
		else
		{
			$from =  $mtd_start ;
		}
		if(!empty($request->to))
		{
			$to = $request->to;
		}
		else{
			$to =  $now;
		}
		
		
		
		
        $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
        $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

		$ctr=0;

		$ytd_redemtion_report = $Ytd_Redemption->whereBetween('created_at', [$from, $to])->with('fetch_patient_info','fetch_patient_kit_allocation_reserve')->where('tag_deleted',0)->get();
		//$ytd_redemtion_report = $Ytd_Redemption->where('created_at','like','%2018-02%')->with('fetch_patient_info','fetch_patient_kit_allocation_reserve')->where('tag_deleted',0)->get();

		return view('otsuka.dashboard.MTD-redemption-report',compact('ytd_redemtion_report'));
	}
	

	
	
}