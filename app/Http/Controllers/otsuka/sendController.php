<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;


class sendController extends Controller
{

	public function send(Request $r){

			$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
			$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

			$msg = $txttemplates->where('id',1)->first();
            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

			$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$r->mobile_number."&message=".$msg->message;
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL,$url);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
				$json = curl_exec($ch);
				if(!$json) {
				    echo curl_error($ch);
				}
				curl_close($ch);

		return redirect()->route('otsuka.dashboard.queue');
	}


}