<?php

namespace App\Http\Controllers\otsuka;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\otsuka\Brand;
use App\Http\Models\otsuka\Upload;
use Auth;
class BrandController extends Controller
{
    public function index(){
    	$data = Brand::all();


    	return view('otsuka.brand.brands', compact('data'));
    }

    public function upload(Request $request){
        
        $lastid = Upload::all()->pluck('id')->last() +1; 
        $path_parts = pathinfo($request->file('fileUpload')->getClientOriginalName());
        $filename  = $request->type . '-' . 'oqs-' . $lastid . '.' . $path_parts['extension'];
        $loc = public_path('upload_test/'.$filename);   
	 
    	$file = new Upload;

        $file->connector_id = time();
        $file->filename = $filename;
        $file->original_filename = $request->file('fileUpload')->getClientOriginalName();
        $file->location = $loc;
        $file->added_by = Auth::user()->id;
        $file->medicine = $request->brandname;
    	$file->type = $request->type;
        $file->patient_name = $request->patient_name;
        $file->mobile_number = $request->mobile_no;
        $file->phone_no = $request->landline_no;
        $file->uploaded_by = Auth::user()->id;
       // dd($file);
         \Image::make($request->file('fileUpload')->getRealPath())->orientate()->save($loc);
        $file->save();

        return redirect()->back()->with('success', 'Upload Successfully');
    //	}
    }

}

