<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;



class receiptController extends Controller
{

	public function index(){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->orderBy('created_at','asc')->get();
		$tn = date("Y-m-d H:i:s");
	

		return view('otsuka.dashboard.queue',compact('Queue','Upload','Mr','Px','tn'));
	}
	
		public function create(Request $request){
		$tn = date("Y-m-d H:i:s");
		
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$encoded_purchases = new \App\Http\Models\otsuka\Encoded_Purchases;
		$encoded_purchases = $encoded_purchases::find($request->encoded_purchases_id);
		$updatequeue = $med_queue::find($request->id);
		if($request->invalid == "true"){
			$upload_records = $med_upload->where('id','=', $request->id)->first();
			$upload_records->remarks = $request->remarks;
			$upload_records->submit = 1;
			if($request->remarks=="Duplicated Receipts (Within The Day)"){
			$upload_records->status = "Reject"; //REMOVE IN QUEUE			
			}
			else{						
			$Queue_Redemption = new \App\Http\Models\otsuka\Queue_Redemption;
			//$Queue_Redemption->patient_code = '';
			$Queue_Redemption->upload_id = $request->id;
			$Queue_Redemption->status = "Invalid";
			$upload_records->encoded_at = date('Y-m-d H:i:s');
			/*$Queue_Redemption->active = 
			$Queue_Redemption->claimed = 
			$Queue_Redemption->submit =   
			$Queue_Redemption->remarks = 
			$Queue_Redemption->claimed_by =
			$Queue_Redemption->connector = */
			$Queue_Redemption->save();
			
			
			
			
			$upload_records->status = "On Queue"; // RETURN ON REDEMPTION QUEUE
			
			
			}
		$upload_records->trail_status = 199;
		$upload_records->upload_remarks = $request->upload_remarks;
		$upload_records->save();
		
			
		}
		else{
		
		$tn = date("Y-m-d H:i:s");
		$encoded_purchases->patient_code = $request->patient_code_encoded;		
		$encoded_purchases->submission_date = $tn;
		$encoded_purchases->or_date = $request->trans_date;
		$encoded_purchases->or_number = $request->or_no;
		$encoded_purchases->save();
		//$updatequeue->receipt_id = $encoded_purchases->id;
		//$updatequeue->save();
		$patient_code = $patient::find($encoded_purchases->id);

		/*pre($patient_code);*/
		$upload_records = $med_upload->where('encoded_purchases_id','=', $encoded_purchases->id)->first();
		$upload_records->status = "Encoded";
		$upload_records->submit = 1;
		$upload_records->upload_remarks = $request->upload_remarks;
		$upload_records->trail_status = 300;
		$upload_records->encoded_at = date('Y-m-d H:i:s');

		$upload_records->save();
		
		$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
							$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

							$msg = $txttemplates->where('id',7)->first();
							$token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

											
							$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$upload_records->mobile_number."&message=".$msg->message;
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_URL,$url);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
							$json = curl_exec($ch);
							if(!$json) {
								echo curl_error($ch);
							}
							curl_close($ch);

		/*
		$Queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$Queue_call_verification->patient_code = $patient_code->patient_code;
		$Queue_call_verification->cmid = $patient_code->id;
		$Queue_call_verification->connector = $updatequeue->connector;
		$Queue_call_verification->save();

		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$call_verification->cmid = $patient_code->id;
		$call_verification->attempt = 1;
		$call_verification->allowcallback = 1;
		$call_verification->save();
		*/
		/*pre($call_verification);*/
		}
		return redirect('otsuka/agent/upload-queue');
		//return redirect()->back();


	}
	
		public function createdtl(Request $request){			
		
		$encoded_purchases_item_dtl = new \App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl;
		$encoded_purchases_item_dtl->encoded_purchases_id =  $request->encoded_purchases_id;
		$encoded_purchases_item_dtl->sku = $request->sku;
		$encoded_purchases_item_dtl->qty = $request->qty;
		$encoded_purchases_item_dtl->active = 1;
		$encoded_purchases_item_dtl->save();
		
		return $encoded_purchases_item_dtl->id;
		}
		
		public function removedtl(Request $request){			
		
		$encoded_purchases_item_dtl = new \App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl;
		$records = $encoded_purchases_item_dtl::find($request->id);
		$records->delete();
		
		}
	



}