<?php namespace App\Http\Controllers\otsuka;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Image;
use File;
use Alert;
use Hash;

class profileController extends Controller
{

	public function picture(){
		return view('otsuka.profile.index');
	}

	public function update_pic(Request $r){
		

		$help = new \App\Http\Controllers\topaz\Helper;
		$GemUser = new \App\Http\Models\topaz\GemUsers;
		$user = $GemUser->find(Auth::user()->id);
		
		if($user):

	        $image = $r->file('file');
	        $filename  = Auth::user()->id . '.' . $image->getClientOriginalExtension();
	        $sm = public_path('uploads/users/sm-' . $filename);
	        $m = public_path('uploads/users/m-' . $filename);
	        $l = public_path('uploads/users/l-' . $filename);
	        \Image::make($image->getRealPath())->resize(50, 50)->save($sm);
	        \Image::make($image->getRealPath())->resize(100, 100)->save($m);
	        \Image::make($image->getRealPath())->resize(200, 200)->save($l);	
	        $this->unlink($user->avatar);
	        $user->avatar = $filename;
	        $user->save();

		endif;
		//return view('otsuka.profile.index');
		return redirect()->back();

	}

	private function unlink($fn){
        $sm = 'uploads/sm-'.$fn;
        $m = 'uploads/m-'.$fn;
        $l = 'uploads/l-'.$fn;
        File::Delete($sm);
        File::Delete($m);
        File::Delete($l);
        return true;		
	}

	public function password(Request $r){
	/*	$GemUser = new \App\Http\Models\topaz\GemUsers;
		$self_data = $GemUser->where('id','=',Auth::user()->id)->first(); */
		
	
		$GemUser = new \App\Http\Models\topaz\GemUsers;
		$self_data = $GemUser->where('id','=',Auth::user()->id)->first();

		$UserPassword = new \App\Http\Models\topaz\GemUsersPassword;
	
		$data['password'] = bcrypt($r['new_password1']);

		$pw['password'] = $r['new_password1'];
		$pw['name'] = $self_data['name'];
		$pw['gem_id'] = Auth::user()->id;

		if(Hash::check($r['current_password'],$self_data['password'])) {

			$user = $GemUser->where('id','=',Auth::user()->id)->update($data);

			$pass = $UserPassword->where('gem_id','=',Auth::user()->id)->first();
			

			if(!empty($pass)){

				// print_r($pass->gem_id);
				// print_r($pw); exit;

				$user = $UserPassword->where('gem_id','=',Auth::user()->id)->update($pw);
	
			}else{

				// print_r('asdsdd');
				// print_r($pw); exit;

				$UserPassword->create($pw);
				
			}
			$message = "Your password has been Changes";
			
			 return redirect('otsuka/profile/')->with('message', $message)->with('status', 'success');
			
		}else{
			$message = "Sorry. Current Password doesnt match our record.";
			//Alert::error('Sorry. Current Password doesnt match our record.')->persistent('Close');

		}
		
			 return redirect('otsuka/profile/')->with('message', $message)->with('status', 'failed');
		

	}	

}