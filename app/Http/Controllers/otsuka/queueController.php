<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;
use Carbon\Carbon;
use Mail;


class queueController extends Controller
{

	public function index_backup(){
		$upload_supporting_docs = new \App\Http\Models\otsuka\UploadSupportingDocs;
		$supporting_docs_list = $upload_supporting_docs->whereNull('patient_id')->orderBy('created_at','desc')->get();	
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$patient_validated = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$queue_Redemption = new \App\Http\Models\otsuka\Queue_Redemption;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$redemption = $queue_Redemption->where('active',1)->with('fetch_upload_id')->get();
		$compliance = $patient_validated->where('active',1)->take(50)->get();
		//pre($compliance);

		//pre($redemption);
		//$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->where('submit',0)->orWhere('viber_queue',1)->orderBy('created_at','asc')->get();

		if(Auth::user()->user_level == 2)  // COndition FOR TL VIEW Display ALL INCOMING UPLOAD
		{
		$Queue = $med_upload->whereBetween('trail_status',[100,198])->where('active',1)->with(['fetch_trail_status','encoded_by','user'])->orderBy('created_at','asc')->get();
		}
		else// COndition FOR AGENT VIEW Display And Claim INCOMING UPLOAD
		{
		$Queue = $med_upload->whereBetween('trail_status',[100,198])->where('active',1)->where('submit',0)->where(function($query){$query->where('claimed',0)->orWhere('claimed_by',Auth::user()->id)->orWhere('viber_queue',1);})->orderBy('created_at','asc')->get();
		}

		$Queue_Verification = $med_upload->whereBetween('trail_status',[200,210])->where('active',1)->where('verify_submit',0)->where(function($query){$query->where('verify_claimed',0)->orWhere('verify_claimed_by',Auth::user()->id);})->with(['fetch_call','encoded_by','verification_claimed_by'])->get();

				$Queue_Verification_Callback = $med_upload->whereBetween('trail_status',[211,299])->where('active',1)->where(function($query){$query->where('verify_claimed',0)->orWhere('verify_claimed_by',Auth::user()->id);})->where('verify_submit',0)->whereIn('callback_date',[date('Y-m-d')])->with(['fetch_call_queue','fetch_status','encoded_by','verification_claimed_by','encoded_by'])->get();

		$Queue_Remdeption = $med_upload->whereBetween('trail_status',[300,399])->where('active',1)->whereIn('redemption_process_by',[0,Auth::user()->id])->orderBy('created_at','asc')->get();

		$Old_enroll = $old_retrieval_enrollment->select('patientid')->where('brandid',1)->orWhere('brandid',2)->orWhere('brandid',8)->get()->toArray();
		$New_enroll = $cmid->select('patientid')->where('new',1)->get()->toArray();
		$Compliance_id = $cmid->select('id')->where('calltype',2)->where('purpose',2)->where('active',1)->whereIn('patientid',$Old_enroll)->orWhereIn('patientid',$New_enroll)->get()->toArray();
		//$Queue_Remdeption = $med_upload->whereBetween('trail_status',[300,399])->where('active',1)->whereIn('redemption_process_by',[Auth::user()->id,null])->orderBy('created_at','asc')->get();

		$Queue_Compliance = $call_verification->where('attempt',1)->where('allowcallback',1)->whereIn('cmid', $Compliance_id)->orderBy('updated_at','asc')->with(['fetch_cmid'])->whereBetween('updated_at', [date('Y-m-d',strtotime('-4 Month')),date('Y-m-d',strtotime('-7 days'))])->get();

/*		$Upload = $med_upload->get();*/
		
/*		foreach($Queue_Remdeption as $qcv){
			foreach($qcv as $qfp){}
		}
*/

		$tn = date("Y-m-d H:i:s");

		return view('otsuka.dashboard.queue',compact('Queue','Queue_Verification','Queue_Verification_Callback','Queue_Remdeption','Queue_Compliance','tn','supporting_docs_list'));

	}

	public function index(){
		$med_upload = new \App\Http\Models\otsuka\Upload;

			if(Auth::user()->user_level == 2)  // COndition FOR TL VIEW Display ALL INCOMING UPLOAD
			{
			$Queue = $med_upload->whereBetween('trail_status',[100,198])->where('active',1)->where('submit',0)->with(['fetch_trail_status','encoded_by','user'])->orderBy('created_at','asc')->get();
			}
			else// COndition FOR AGENT VIEW Display And Claim INCOMING UPLOAD
			{
			$Queue = $med_upload->whereBetween('trail_status',[100,198])->where('active',1)->where('submit',0)->where(function($query){$query->where('claimed',0)->orWhere('claimed_by',Auth::user()->id)->orWhere('viber_queue',1);})->orderBy('created_at','asc')->get();
			}

			$tn = date("Y-m-d H:i:s");

		return view('otsuka.dashboard.queue',compact('Queue','tn'));
	}

	public function verification_queue(){
		$med_upload = new \App\Http\Models\otsuka\Upload;

			$Queue_Verification = $med_upload->whereBetween('trail_status',[200,210])->where('active',1)->where('verify_submit',0)->where(function($query){$query->where('verify_claimed',0)->orWhere('verify_claimed_by',Auth::user()->id);})->with(['fetch_call','encoded_by','verification_claimed_by'])->orderBy('created_at','asc')->get();

			$tn = date("Y-m-d H:i:s");

		return view('otsuka.dashboard.verification-queue',compact('Queue_Verification','tn'));
	}

	public function verification_callback_queue(){
		$med_upload = new \App\Http\Models\otsuka\Upload;

			$Queue_Verification_Callback = $med_upload->whereBetween('trail_status',[211,299])->where('active',1)->where(function($query){$query->where('verify_claimed',0)->orWhere('verify_claimed_by',Auth::user()->id);})->where('verify_submit',0)->whereIn('callback_date',[date('Y-m-d')])->with(['fetch_call_queue','fetch_status','encoded_by','verification_claimed_by','encoded_by'])->orderBy('created_at','asc')->get();

			$tn = date("Y-m-d H:i:s");
			$now = date("Y-m-d");
			$tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

			$count = $med_upload->whereBetween('trail_status',[211,299])->where('active',1)->where(function($query){$query->where('verify_claimed',0)->orWhere('verify_claimed_by',Auth::user()->id);})->where('verify_submit',0)->whereIn('callback_date',[date('Y-m-d'),$tomorrow])->with(['fetch_call_queue','fetch_status','encoded_by','verification_claimed_by','encoded_by'])->orderBy('created_at','asc')->count();

		return view('otsuka.dashboard.verification-callback-queue',compact('Queue_Verification_Callback','tn','count'));
	}

	public function redemption_queue(){
		$med_upload = new \App\Http\Models\otsuka\Upload;

			$Queue_Remdeption = $med_upload->whereBetween('trail_status',[300,399])->where('active',1)->whereIn('redemption_process_by',[0,Auth::user()->id])->orderBy('created_at','asc')->get();

			$tn = date("Y-m-d H:i:s");

		return view('otsuka.dashboard.redemption-queue',compact('Queue_Remdeption','tn'));
	}

	public function supporting_docs_queue(){
		$upload_supporting_docs = new \App\Http\Models\otsuka\UploadSupportingDocs;
		
			$supporting_docs_list = $upload_supporting_docs->whereNull('patient_id')->orderBy('created_at','desc')->get();

			$tn = date("Y-m-d H:i:s");	

		return view('otsuka.dashboard.supporting-docs-queue',compact('supporting_docs_list','tn'));
	}

	public function compliance_ref_voluntary_queue(){	
		$now = date("Y-m-d");
		$minus_month = date('Y-m-d', strtotime($now.'-1 month'));
		$patient_validated = new \App\Http\Models\otsuka\Patient_Validated;
		$Data = $patient_validated->where('patient_type', '!=', 'Retrieval')->where('created_at','<',$minus_month)->where('id','>','150000')->whereNull('last_compliance_date')->get();
		//pre($Data);
		return view('otsuka.dashboard.compliance-ref-voluntary-queue',compact('Data'));
	}
	public function compliance_queue_v2(){
		$patient_validated = new \App\Http\Models\otsuka\Patient_Validated;
		$patients_list = $patient_validated->where("tag_deleted",0)->orderBy('last_compliance_date','asc')->limit(500)->get();
		return view('otsuka.dashboard.compliance-queue-v2',compact('patients_list'));
	}
	
	public function compliance_queue(){
		$patient_validated = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		//$old_doctor = new \App\Http\Models\otsuka\Old_doctor;

			//$compliance = $patient_validated->where('active',1)->take(50)->get();

			//$Old_enroll = $old_retrieval_enrollment->select('patientid')->where('brandid',1)->orWhere('brandid',2)->orWhere('brandid',8)->count();

/*			$Old_enroll = $old_retrieval_enrollment->select('patientid')->where(function($query){$query->where('brandid',1)->orWhere('brandid',2)->orWhere('brandid',8);})->get()->toArray();*/

			$Old_enroll = $old_retrieval_enrollment->select('patientid')->where('brandid',1)->get()->toArray();

			$referral_voluntary = $patient_validated->select('id')->whereNotNull('last_compliance_date')->get()->toArray();
			
			$Upload = $med_upload->select('patientid')->where('type','=','Enroll')->where('medicine','=','Abilify')->where('trail_status','!=',199)->whereNotNull('patientid')->where('active',1)->get()->toArray();

			$New_enroll = $cmid->select('patientid')->whereIn('patientid',$Upload)->get()->toArray();
			$Compliance_id = $cmid->select('id')->where('calltype',2)->where('purpose',2)->where('active',1)->whereIn('patientid',$Old_enroll)->orWhereIn('patientid',$referral_voluntary)->orWhereIn('patientid',$New_enroll)->get()->toArray();

			$now = date("Y-m-d");
			$month = date('Y-m-d', strtotime($now.'-4 month'));
			$minus_month = date('Y-m-d', strtotime($now.'-1 month'));
			$month_now = date('Y-m-d', strtotime($minus_month.'+10 days'));
			$month_tomorrow = date('Y-m-d', strtotime($minus_month.'+11 days'));
			$month_day_after = date('Y-m-d', strtotime($minus_month.'+12 days'));
			$month_days = date('Y-m-d', strtotime($month.'+7 days'));

			$Queue_Compliance = $call_verification->where('attempt',1)->where('allowcallback',1)->whereIn('cmid', $Compliance_id)->orderBy('updated_at','asc')->with(['fetch_cmid'])->whereBetween('updated_at', [$month,$month_now])->take(30)->get();

			$q_count = $call_verification->whereIn('cmid', $Compliance_id)->where('attempt',1)->where('allowcallback',1)->orderBy('updated_at','asc')->whereBetween('updated_at', [$month,$month_now])->count();

			$today_count = $call_verification->where('attempt',1)->where('allowcallback',1)->whereIn('cmid', $Compliance_id)->orderBy('updated_at','asc')->with(['fetch_cmid'])->whereBetween('updated_at', [$month,$month_now])->count();

			$tom_count = $call_verification->whereIn('cmid', $Compliance_id)->where('attempt',1)->where('allowcallback',1)->orderBy('updated_at','asc')->whereBetween('updated_at', [$month,$month_tomorrow])->count();

			$day_after_count = $call_verification->whereIn('cmid', $Compliance_id)->where('attempt',1)->where('allowcallback',1)->orderBy('updated_at','asc')->whereBetween('updated_at', [$month,$month_day_after])->count();

			$total_count = $call_verification->whereIn('cmid', $Compliance_id)->where('attempt',1)->where('allowcallback',1)->orderBy('updated_at','asc')->whereBetween('updated_at', [$month,$now])->count();

			$tn = date("Y-m-d H:i:s");


			//pre($Queue_Compliance);

		return view('otsuka.dashboard.compliance-queue',compact('Queue_Compliance','tn','tom_count','day_after_count','total_count','q_count','today_count'));
	}


	public function encode(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$sku = new \App\Http\Models\otsuka\Sku;

		$Mr = $med_rep->take(50)->get();
		$Px = $patient->take(50)->get();

		$px_last = $patient->orderBy('id','desc')->first();
		$px_number = $px_last->id;
		$px_code = 'OQPXN'.sprintf('%07d',$px_number++);

		$claim = $med_upload->where('id','=',$r->encode)->update(['claimed' => 1, 'claimed_by' => Auth::user()->id]);
		$data[] = $r->encode;

		//$patient->patient_lastname = $r->patient_name;
		//$patient->mobile_number = $r->contact_no;



		$item = $med_upload->where('id','=',$data[0])->with(['user'])->get();

		$details[] = $item;
		
		if($item[0]->type =="Receipt")			
			{
				if(($item[0]->encoded_purchases_id == NULL) || ($item[0]->encoded_purchases_id == ""))
				{
					$encoded_purchases = new \App\Http\Models\otsuka\Encoded_Purchases;
					$encoded_purchases->file_path = $item[0]->filename;
					$encoded_purchases->save();
					$item[0]->where('id','=',$r->encode)->update(['encoded_purchases_id' => $encoded_purchases->id]);
					$item[0]->encoded_purchases_id = $encoded_purchases->id;
					$item[0]->trail_status = 101;
					$item[0]->save();
				}
				else {
					
				}
						
			}

		foreach($item as $i)
		{
		}

		$encoded_purchases_records = new \App\Http\Models\otsuka\Encoded_Purchases;
		$encoded_purchases = $encoded_purchases_records->where('id', '=' ,$item[0]->encoded_purchases_id)->first();
		//pre($encoded_purchases_records);
		$encoded_purchases_item_dtl = new \App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl;
		$sku_item_dtl = $encoded_purchases_item_dtl->where('encoded_purchases_id', '=' ,$item[0]->encoded_purchases_id)->with('sku_details')->get();

		$Mgabilify = $sku->where('variant','like','%'.$item[0]->medicine.'%')->get();
		$Mgaminoleban = $sku->where('variant','like','%'.$item[0]->medicine.'%')->get();
		//$Mgpletal = $sku->where('variant','like','%'.$item[0]->medicine.'%')->get();
		$Mgpletal = $sku->where('brand','=',strtoupper($item[0]->medicine) )->get();
		/*pre($item); */
		$item[0]->trail_status = 101;
		return view('otsuka.dashboard.encode',compact('item','upload','Mr','Px','px_code','Mgabilify','Mgaminoleban','Mgpletal','sku_item_dtl','encoded_purchases','i'));
	}

	public function enroll(Request $r){
		$users = new \App\Http\Models\gem\User;
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;

		$m1 = ltrim($r->mobile_number,'0');
		$m2 = ltrim($r->mobile_number_2,'0');
		$pn = ltrim($r->phone_number,'0');


		$patient_kit_mr = $patients_kit_allocation->select('id')->where('one_quest_id','=',$r->patient_kit_number)->first();

		if(!empty($r->patient_kit_number))
		{
						$validate_kit_upload = $med_upload->where('patient_kit_number',$r->patient_kit_number)
						->where('trail_status','!=',199)
						->where('type','=','Enroll')
						->where('id','!=',$r->done)
						->where('active',1)
						->where(function($query){$query->where('verification_status',0)->orWhere('verification_status',6);})
						->get();

						$validate_kit = $patients_kit_allocation->where('one_quest_id',$r->patient_kit_number)->get();
		}
		else
		{
			$validate_kit_upload = [];
			$validate_kit = [];
		}

		if(!empty($r->patient_kit_number))
		{
			$validate_kit_validated = $patient->where('patient_kit_number',$r->patient_kit_number)->get();
		}
		else
		{
			$validate_kit_validated = [];
		}

		if(!empty($r->doctor_id))
		{
			$validate_doctor = $national_md_list->where('doctorid',$r->doctor_id)->get();
		}
		else
		{
			$validate_doctor = [];
		}

		if(!empty($m1) and !empty($m2) and !empty($pn))
		{
		$validate_number = $patient
		->where(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number','like','%'.$m1.'%')->orWhere('mobile_number','like','%'.$m2.'%')->orWhere('mobile_number','like','%'.$pn.'%')
			->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number_2','like','%'.$m1.'%')->orWhere('mobile_number_2','like','%'.$m2.'%')->orWhere('mobile_number_2','like','%'.$pn.'%')
				->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('phone_number','like','%'.$m1.'%')->orWhere('phone_number','like','%'.$m2.'%')->orWhere('phone_number','like','%'.$pn.'%');});});})
						->where('id','!=',$r->done)
						->get();
		}
		elseif(!empty($m1) and !empty($m2) and empty($pn))
		{
		$validate_number = $patient
		->where(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number','like','%'.$m1.'%')->orWhere('mobile_number','like','%'.$m2.'%')
			->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number_2','like','%'.$m1.'%')->orWhere('mobile_number_2','like','%'.$m2.'%')
				->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('phone_number','like','%'.$m1.'%')->orWhere('phone_number','like','%'.$m2.'%');});});})
					->where('id','!=',$r->done)
					->get();
		}
		elseif(!empty($m1) and empty($m2) and empty($pn))
		{
		$validate_number = $patient
		->where(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number','like','%'.$m1.'%')->orWhere('mobile_number_2','like','%'.$m1.'%')->orWhere('phone_number','like','%'.$m1.'%');})
			->where('id','!=',$r->done)
			->get();
		}
		elseif(empty($m1) and empty($m2) and !empty($pn)) 
		{
		$validate_number = $patient
		->where(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number','like','%'.$pn.'%')->orWhere('mobile_number','like','%'.$pn.'%')
			->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number_2','like','%'.$pn.'%')->orWhere('mobile_number_2','like','%'.$pn.'%')
				->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('phone_number','like','%'.$pn.'%')->orWhere('phone_number','like','%'.$pn.'%');});});})
					->where('id','!=',$r->done)
					->get();
		}
		elseif(empty($m1) and !empty($m2) and !empty($pn))
		{
		$validate_number = $patient
		->where(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number','like','%'.$m2.'%')->orWhere('mobile_number','like','%'.$pn.'%')
			->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number_2','like','%'.$m2.'%')->orWhere('mobile_number_2','like','%'.$pn.'%')
				->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('phone_number','like','%'.$m2.'%')->orWhere('phone_number','like','%'.$pn.'%');});});})
					->where('id','!=',$r->done)
					->get();
		}
		else
		{
			$validate_number = [];
		}



		if(count($validate_kit_upload) == 0 or count($validate_kit_validated) == 0 or count($validate_number) == 0)
		{
			if($r->invalid == 'true')
			{
				if($r->remarks == 'No Px Consent')
				{
				$med_upload->where('id','=',$r->id)
				->update(
					[
						'viber_queue'=>'1',
						'remarks'=>$r->remarks,
						'status'=>'Reject',
						'submit'=>'0',
						'trail_status'=> 199,
						'patient_kit_number' => preg_replace('/\s+/', '', $r->patient_kit_number),
						'patient_name' => $r->patient_fullname,
						'mobile_number' => $r->mobile_number,
						'mobile_number_2' => $r->mobile_number_2,
						'phone_no' => $r->phone_number,
						'time_to_call' => $r->time_to_call,
						'patient_consent' => NULL,
						'enrollment_date' => !empty($r->enrollment_date) ? date("Y-m-d", strtotime($r->enrollment_date)) : null,
						'doctor_name' => $r->doctor_name,
						'doctor_id' => $r->doctor_id,
						'doctor_consent' => $r->doctor_consent,
						'upload_remarks' => $r->upload_remarks,
						'hospital_name' => $r->hospital_name
					]);
				}
				else
				{
				$med_upload->where('id','=',$r->id)
				->update(
					[
						'viber_queue'=>'1',
						'remarks'=>$r->remarks,
						'status'=>'Reject',
						'submit'=>'0',
						'trail_status'=> 199,
						'patient_kit_number' => preg_replace('/\s+/', '', $r->patient_kit_number),
						'patient_name' => $r->patient_fullname,
						'mobile_number' => $r->mobile_number,
						'mobile_number_2' => $r->mobile_number_2,
						'phone_no' => $r->phone_number,
						'time_to_call' => $r->time_to_call,
						'patient_consent' => $r->patient_consent,
						'enrollment_date' => !empty($r->enrollment_date) ? date("Y-m-d", strtotime($r->enrollment_date)) : null,
						'doctor_name' => $r->doctor_name,
						'doctor_id' => $r->doctor_id,
						'doctor_consent' => $r->doctor_consent,
						'upload_remarks' => $r->upload_remarks,
						'hospital_name' => $r->hospital_name
					]);
				}
				return redirect('otsuka/agent/upload-queue'); 
			}

			elseif($r->invalid != 'true')
				{
					/*if(count($validate_kit_upload) == 0 and count($validate_kit_validated) == 0 and count($validate_number) == 0 and count($validate_doctor) >= 1 and count($validate_kit) >= 1)*/
					if(count($validate_kit_upload) == 0 and count($validate_kit_validated) == 0 and count($validate_number) == 0)
					{

						$cmid->calltype = 1;
						$cmid->purpose = 1;
						$cmid->employeeid = Auth::user()->id;
						$cmid->save();

						$call_verification->cmid = $cmid->id;
						$call_verification->attempt = 1;
						$call_verification->date = date('m/d/Y');
						$call_verification->save();

						$submit = $med_upload->where('id','=',$r->done)
						->update(
							[
								'patient_kit_number' => preg_replace('/\s+/', '', $r->patient_kit_number),
								'patient_name' => $r->patient_fullname,
								'mobile_number' => $r->mobile_number,
								'mobile_number_2' => $r->mobile_number_2,
								'phone_no' => $r->phone_number,
								'time_to_call' => $r->time_to_call,
								'patient_consent' => $r->patient_consent,
								'enrollment_date' => !empty($r->enrollment_date) ? date("Y-m-d", strtotime($r->enrollment_date)) : null,
								'doctor_name' => $r->doctor_name,
								'doctor_id' => $r->doctor_id,
								'doctor_consent' => $r->doctor_consent,
								'hospital_name' => $r->hospital_name,
								'cmid' => $cmid->id,
								'trail_status' => 200,
								'upload_remarks' => $r->upload_remarks,
								'submit' => 1
							]
						);

						if(!empty($r->mobile_number))
						{

						$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
						$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

						$msg = $txttemplates->where('id',2)->first();
			            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

						$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$r->mobile_number."&message=".$msg->message;
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_URL,$url);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
							$json = curl_exec($ch);
							if(!$json) {
							    echo curl_error($ch);
							}
							curl_close($ch);
						}

					return redirect('otsuka/agent/upload-queue'); 	
					}
					else
					{

					return redirect()->back()->withInput()->with(['validate_kit_upload' => $validate_kit_upload, 'validate_kit_validated' => $validate_kit_validated, 'validate_number' => $validate_number, 'validate_doctor' => $validate_doctor, 'validate_kit' => $validate_kit]);	
					}

				}
				
			}
			else
			{
				return redirect()->back()->withInput()->with(['validate_kit_upload' => $validate_kit_upload,'validate_kit_validated' => $validate_kit_validated, 'validate_number' => $validate_number, 'validate_doctor' => $validate_doctor, 'validate_kit' => $validate_kit]);
			}
		}

	public function enroll_proceed(Request $r){
		$users = new \App\Http\Models\gem\User;
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;

						$cmid->calltype = 1;
						$cmid->purpose = 1;
						$cmid->employeeid = Auth::user()->id;
						$cmid->save();

						$call_verification->cmid = $cmid->id;
						$call_verification->attempt = 1;
						$call_verification->date = date('m/d/Y');
						$call_verification->save();

						$submit = $med_upload->where('id','=',$r->id)
						->update(
							[
								'patient_kit_number' => preg_replace('/\s+/', '', $r->patient_kit_number),
								'patient_name' => $r->patient_fullname,
								'mobile_number' => $r->mobile_number,
								'mobile_number_2' => $r->mobile_number_2,
								'phone_no' => $r->phone_number,
								'time_to_call' => $r->time_to_call,
								'patient_consent' => $r->patient_consent,
								'enrollment_date' => !empty($r->enrollment_date) ? date("Y-m-d", strtotime($r->enrollment_date)) : null,
								'doctor_name' => $r->doctor_name,
								'doctor_id' => $r->doctor_id,
								'doctor_consent' => $r->doctor_consent,
								'hospital_name' => $r->hospital_name,
								'cmid' => $cmid->id,
								'trail_status' => 200,
								'submit' => 1
							]
						);

						if(!empty($r->mobile_number))
						{

						$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
						$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

						$msg = $txttemplates->where('id',2)->first();
			            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

						$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$r->mobile_number."&message=".$msg->message;
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_URL,$url);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
							$json = curl_exec($ch);
							if(!$json) {
							    echo curl_error($ch);
							}
							curl_close($ch);
						}

					return redirect('otsuka/agent/upload-queue'); 
	}

	public function unclaim(Request $r){
		$users = new \App\Http\Models\gem\User;
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;

		$unclaim = $med_upload->where('id', '=', $r->unclaim)
		->update(
			[
				'claimed_by' => null,
				'claimed' => 0
			]
		);
		return redirect('otsuka/agent/upload-queue');
	}



		public function enroll_reciept(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$sku = new \App\Http\Models\otsuka\Sku;

		$Mr = $med_rep->take(50)->get();
		$Px = $patient->take(50)->get();

		$px_last = $patient->orderBy('id','desc')->first();
		$px_number = $px_last->id;
		$px_code = 'OQPXN'.sprintf('%07d',$px_number++);

		$claim = $med_upload->where('id','=',$r->encode)->update(['claimed' => 1, 'claimed_by' => Auth::user()->id]);
		$data[] = $r->encode;

		$patient->patient_lastname = $r->patient_name;
		$patient->mobile_number = $r->contact_no;


		$item = $med_upload->where('id','=',$data[0])->get();

		$details[] = $item;
		$Mg = $sku->where('variant','like','%'.$item[0]->medicine.'%')->get();
		//pre($r->encode);

		$details[] = $item;
		$Mg = $sku->where('variant','like','%'.$item[0]->medicine.'%')->get();
		/*pre($r->encode);*/

		return view('otsuka.dashboard.encode',compact('item','upload','Mr','Px','px_code','Mg'));
	}

	public function reciept(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$encoded_purchases = new \App\Http\Models\otsuka\Encoded_Purchases;

		pre($encoded_purchases->all());
/* 
		$patient->patient_code = $r->patient_code;
		$patient->patient_lastname = $r->patient_lastname;
		$patient->patient_firstname = $r->patient_firstname;
		$patient->patient_middlename = $r->patient_middlename;
		$patient->nickname = $r->nickname;
		$patient->address = $r->address;
		$patient->birth_date = $r->birth_date;
		$patient->age = $r->age;
		$patient->gender = $r->gender;
		$patient->mobile_number = $r->mobile_number;
		$patient->mobile_number_2 = $r->mobile_number_2;
		$patient->phone_number = $r->phone_number;

		$patient->save(); */


		$claim = $med_upload->where('id','=',$r->id)->update(['claimed' => 1, 'claimed_by' => Auth::user()->id, 'submit' => 1]);

		/*pre($item);*/
		return redirect('otsuka/agent/upload-queue');
	}


}