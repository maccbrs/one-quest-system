<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;


class complianceController extends Controller
{

	public function index(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance_reason = new \App\Http\Models\otsuka\Compliance_reason;

		$Mgabilify = $sku->where('variant','like','%'.'Abilify'.'%')->get();
		$Mgaminoleban = $sku->where('variant','like','%'. 'Aminoleban' .'%')->get();		
		$Mgpletal = $sku->where('variant','like','%'. 'Pletaal' .'%')->get();

		

		$patient_record = $cmid->where('id',$r->encode)->with(['fetch_patient','fetch_verification_queue_compliance','fetch_compliance_details'])->get();
		foreach($patient_record as $pr){
		
		}
			

			$old_record = $old_retrieval_enrollment->where('patientid',$pr->patientid)->with(['fetch_product','fetch_sku'])->get();
			foreach($old_record as $od){}

		$drop_months = $months->orderBy('year')->orderBy('month')->get();
		$drop_reasons = $compliance_reason->where('status',1)->get();
		$drop_real_reasons = $compliance_reason->where('status',0)->get();


		//pre($pr->fetch_verification_queue->id);
		//pre($pr->fetch_patient->fetch_compliance_upload[0]);
		//pre($od->fetch_product->brandname);
		/*pre($pr->fetch_compliance->fetch_compliance_upload);*/
		//pre($pr);
		return view('otsuka.dashboard.compliance',compact('patient_record','pfc','od','pr','v','Mgabilify','Mgaminoleban','Mgpletal','drop_months','drop_reasons','drop_real_reasons'));
	
	}
	
	
	public function ref_voluntary_index(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance_reason = new \App\Http\Models\otsuka\Compliance_reason;
		

		$Mgabilify = $sku->where('variant','like','%'.'Abilify'.'%')->get();
		$Mgaminoleban = $sku->where('variant','like','%'. 'Aminoleban' .'%')->get();		
		$Mgpletal = $sku->where('variant','like','%'. 'Pletaal' .'%')->get();
		$patient_record = $patient->find($r->patient_id);
		$drop_months = $months->get();
		$drop_reasons = $compliance_reason->where('status',1)->get();


		return view('otsuka.dashboard.compliance-ref-voluntary',compact('patient_record','pfc','od','pr','v','Mgabilify','Mgaminoleban','Mgpletal','drop_months','drop_reasons'));
	
	}
	
		public function save_ref_voluntary(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$compliance = new \App\Http\Models\otsuka\Compliance;

		$Mgabilify = $sku->where('variant','like','%'.'Abilify'.'%')->get();
		$Mgaminoleban = $sku->where('variant','like','%'. 'Aminoleban' .'%')->get();		
		$Mgpletal = $sku->where('variant','like','%'. 'Pletaal' .'%')->get();

		$compliance->patientid = $r->patientid;
		$compliance->product_taken = $r->product_taken;
		$compliance->sku = $r->sku;
		$compliance->date_start_medication = !empty($r->date_start_medication) ? date("Y-m-d", strtotime($r->date_start_medication)) : NULL;
		$compliance->still_medication = $r->still_medication;
		$compliance->reason_not_med = $r->reason_not_med;
		$compliance->others_reason_not_med = $r->others_reason_not_med;
		$compliance->month_stopped = $r->month_stopped;
		$compliance->reason = $r->reason;
		$compliance->medication = $r->medication;
		$compliance->remarks = $r->remarks;
		$compliance->callnotes = $r->callnotes;
		$compliance->save();

		$patient->where('id',$r->patientid)
		->update([
				'patient_fullname' => $r->patient_fullname,
				'patient_lastname' => $r->patient_lastname,
				'patient_firstname' => $r->patient_firstname,
				'patient_middlename' => $r->patient_middlename,
				'birth_date' => $r->birth_date,
				'gender' => $r->gender,
				'age' => $r->age,
				'address' => $r->address,
				'mobile_number' => $r->mobile_number,
				'mobile_number_2' => $r->mobile_number_2,
				'phone_number' => $r->phone_number,
				'last_compliance_date' => date('Y-m-d')
			]);

		if(($r->reason == 5 ) || ($r->reason == 6 ))
		{
			
		
		$cmid->calltype =  2;
		$cmid->purpose =  2;
		$cmid->caller =  1;
		$cmid->cardnumberid = 0 ;
		//$cmid->patientid =  ;
		$cmid->reid = 0;
		$cmid->employeeid = Auth::user()->id;;
		//$cmid->remarks = ;
		$cmid->active = 	1;	
		$cmid->patientid = $r->patientid;		
		$cmid->remarks = $r->remarks;		
		$cmid->save();
		
		$call_verification->attempt = 1;
		$call_verification->cmid = $cmid->id;
		$call_verification->date = date('m/d/Y'); 
		$call_verification->actualdate = date('m/d/Y'); 
		$call_verification->remarks = $r->remarks;
		$call_verification->remarks_others =  $r->medication;
		$call_verification->callnotes = $r->callnotes;
		$call_verification->allowcallback = 1;
		$call_verification->employeeid =  Auth::user()->id;
		$call_verification->save();


		
		/*	
		$cmid
		->where('id',$r->cmid)
		->update(['active' => 1, 'remarks' => $r->remarks]);

		$call_verification
		->where('id',$r->v_id)
		->update(['remarks' => $r->remarks, 'actualdate' => date('Y-m-d'),'remarks_others' => $r->medication, 'callnotes' => $r->callnotes, 'employeeid' => Auth::user()->id]);
		*/
		}
		elseif($r->reason == 4)
		{
		$cmid->calltype =  2;
		$cmid->purpose =  2;
		$cmid->caller =  1;
		$cmid->cardnumberid = 0 ;
		//$cmid->patientid =  ;
		$cmid->reid = 0;
		$cmid->employeeid = Auth::user()->id;;
		//$cmid->remarks = ;
		$cmid->active = 	1;	
		$cmid->patientid = $r->patientid;		
		$cmid->remarks = $r->remarks;		
		$cmid->save();
		
		$call_verification->attempt = 1;
		$call_verification->cmid = $cmid->id;
		$call_verification->date = date('m/d/Y'); 
		$call_verification->actualdate = date('m/d/Y'); 
		$call_verification->remarks = $r->remarks;
		$call_verification->remarks_others =  $r->medication;
		$call_verification->callnotes = $r->callnotes;
		$call_verification->allowcallback = 2;
		$call_verification->employeeid =  Auth::user()->id;
		$call_verification->save();	
		/*
		$cmid
		->where('id',$r->cmid)
		->update(['active' => 1, 'remarks' => $r->remarks]);

		$call_verification
		->where('id',$r->v_id)
		->update(['remarks' => $r->remarks, 'actualdate' => date('Y-m-d'), 'allowcallback' => 2,'remarks_others' => $r->medication, 'callnotes' => $r->callnotes, 'employeeid' => Auth::user()->id]);
		*/
		}
		elseif($r->reason == 5)
		{
			$cmid->calltype =  2;
		$cmid->purpose =  2;
		$cmid->caller =  1;
		$cmid->cardnumberid = 0 ;
		//$cmid->patientid =  ;
		$cmid->reid = 0;
		$cmid->employeeid = Auth::user()->id;;
		//$cmid->remarks = ;
		$cmid->active = 	1;	
		$cmid->patientid = $r->patientid;		
		$cmid->remarks = $r->remarks;		
		$cmid->save();
		
		$call_verification->attempt = 1;
		$call_verification->cmid = $cmid->id;
		$call_verification->date = date('m/d/Y'); 
		$call_verification->actualdate = date('m/d/Y'); 
		$call_verification->remarks = $r->remarks;
		$call_verification->remarks_others =  $r->medication;
		$call_verification->callnotes = $r->callnotes;
		$call_verification->employeeid =  Auth::user()->id;
		$call_verification->save();
		/*
		$cmid
		->where('id',$r->cmid)
		->update(['active' => 1, 'remarks' => $r->remarks]);

		$call_verification
		->where('id',$r->v_id)
		->update(['remarks' => $r->remarks,'actualdate' => date('Y-m-d'), 'callnotes' => $r->callnotes, 'employeeid' => Auth::user()->id]);
		*/
			$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
			$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

			$msg = $txttemplates->where('id',4)->first();
            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

			$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$r->mobile_number."&message=".$msg->message;
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL,$url);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
				$json = curl_exec($ch);
				if(!$json) {
				    echo curl_error($ch);
				}
				curl_close($ch);

		}
		return redirect('otsuka/agent/compliance-queue');
	
	}
	

		public function save(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$compliance = new \App\Http\Models\otsuka\Compliance;

		$Mgabilify = $sku->where('variant','like','%'.'Abilify'.'%')->get();
		$Mgaminoleban = $sku->where('variant','like','%'. 'Aminoleban' .'%')->get();		
		$Mgpletal = $sku->where('variant','like','%'. 'Pletaal' .'%')->get();

		$compliance->patientid = $r->patientid;
		$compliance->product_taken = $r->product_taken;
		$compliance->sku = $r->sku;
		$compliance->date_start_medication = !empty($r->date_start_medication) ? date("Y-m-d", strtotime($r->date_start_medication)) : NULL;
		$compliance->still_medication = $r->still_medication;
		$compliance->reason_not_med = $r->reason_not_med;
		$compliance->others_reason_not_med = $r->others_reason_not_med;
		$compliance->month_stopped = $r->month_stopped;
		$compliance->reason = $r->reason;
		$compliance->medication = $r->medication;
		$compliance->remarks = $r->remarks;
		$compliance->callnotes = $r->callnotes;
		$compliance->save();

		$patient->where('id',$r->patientid)
		->update([
				'patient_fullname' => $r->patient_fullname,
				'patient_lastname' => $r->patient_lastname,
				'patient_firstname' => $r->patient_firstname,
				'patient_middlename' => $r->patient_middlename,
				'birth_date' => $r->birth_date,
				'gender' => $r->gender,
				'age' => $r->age,
				'address' => $r->address,
				'mobile_number' => $r->mobile_number,
				'mobile_number_2' => $r->mobile_number_2,
				'phone_number' => $r->phone_number
			]);


		if($r->reason == 4 or $r->reason == 7 or $r->reason == 12 or $r->reason == 13)
		{

		$cmid
		->where('id',$r->cmid)
		->update(['active' => 1, 'remarks' => $r->remarks]);

		$call_verification
		->where('id',$r->v_id)
		->update(['remarks' => $r->reason, 'actualdate' => date('Y-m-d'), 'allowcallback' => 2,'remarks_others' => $r->medication, 'callnotes' => $r->callnotes, 'employeeid' => Auth::user()->id]);

		}
		elseif($r->reason == 5)
		{

		$cmid
		->where('id',$r->cmid)
		->update(['active' => 1, 'remarks' => $r->remarks]);

		$call_verification
		->where('id',$r->v_id)
		->update(['remarks' => $r->reason,'actualdate' => date('Y-m-d'), 'callnotes' => $r->callnotes, 'employeeid' => Auth::user()->id]);

			$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
			$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

			$msg = $txttemplates->where('id',4)->first();
            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

			$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$r->mobile_number."&message=".$msg->message;
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL,$url);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
				$json = curl_exec($ch);
				if(!$json) {
				    echo curl_error($ch);
				}
				curl_close($ch);

		}
		else
		{

		$cmid
		->where('id',$r->cmid)
		->update(['active' => 1, 'remarks' => $r->remarks]);

		$call_verification
		->where('id',$r->v_id)
		->update(['remarks' => $r->reason, 'actualdate' => date('Y-m-d'),'remarks_others' => $r->medication, 'callnotes' => $r->callnotes, 'employeeid' => Auth::user()->id]);

		}
		return redirect('otsuka/agent/compliance-queue');
	
	}
	
	

}