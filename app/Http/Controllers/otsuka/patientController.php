<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;


class patientController extends Controller
{

	public function index(Request $request){
		$now = date('Y-m-d');
		//$patients = new \App\Http\Models\otsuka\Patient_Validated;
		$sku = new \App\Http\Models\otsuka\Sku;
		$Patients_kit_allocation_reserve = new \App\Http\Models\otsuka\Patients_kit_allocation_reserve;
		$Mgabilify = $sku->where('is_active_in_oqs',1)->get();
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		/*$Mgaminoleban = $sku->where('brand','like','%'.'AMINOLEBAN'.'%')->get();
		$Mgpletal = $sku->where('brand','like','%'.$v->medicine.'%')->get(); */
		$Reserve_Patient_Kit = $Patients_kit_allocation_reserve->whereNull('used_by')->take(20)->get();

		return view('otsuka.dashboard.patient-masterfile',compact('patients_list','Reserve_Patient_Kit','Mgabilify'));
	}


	public function searchPatient(Request $request){
		$searching = $request->person;
		
		$patient = new \App\Http\Models\otsuka\Patient_Validated;

		
		$tn = date("Y-m-d H:i:s");
		$person = $Queue;
		if(($request->person) == "patient")			
		{
		//$Queue = $patient->where('patient_lastname', 'like', '%T%')
		$Queue = 
		$patient::where('patient_lastname', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_fullname', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_firstname', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_kit_number', 'like', '%' . $request->person_filter .'%')
				->orWhere('patient_code', 'like', '%' . $request->person_filter .'%')
				->orWhere('mobile_number', 'like', '%' . $request->person_filter .'%')
				->orWhere('mobile_number_2', 'like', '%' . $request->person_filter .'%')
				->orWhere('phone_number', 'like', '%' . $request->person_filter .'%')
				->get();
			
		return view('otsuka.dashboard.redemptionsearch',compact('Queue','Mr','Px','tn','person','searching'));			
		}
				
		
		
	}
	
		public function enroll_patient(Request $request){
			$sku = new \App\Http\Models\otsuka\Sku;
			$return_value = array();
				$tn = date("Y-m-d H:i:s");
			$Patients_kit_allocation_reserve = new \App\Http\Models\otsuka\Patients_kit_allocation_reserve;
			if($request->action =='save')
			{
			//$Patient_Validated 
			$Patient_Validated_Model= new \App\Http\Models\otsuka\Patient_Validated;
			//$Patient_Validated->patient_kit_number = $request->enroll_patient_kit
			if($request->enroll_refid !='' )
			{ 
			$Patient_Validated = $Patient_Validated_Model::find($request->enroll_refid);
			//$Patient_Validated->remarks ="test";
			}
			else
			{
				$Patient_Validated = new \App\Http\Models\otsuka\Patient_Validated;
				//$Patient_Validated->remarks ="NONON";
			}

			$px_last = $Patient_Validated->orderBy('id','desc')->first();
			$px_number = $px_last->id;
			$px_code = 'OQPXN'.sprintf('%07d',$px_number++);

			$pxkit_enroll = $Patients_kit_allocation_reserve->whereNull('used_by')->first();

			$Patient_Validated->patient_code = $px_code;
			$Patient_Validated->enrollment_date = date("m/d/Y",strtotime($tn)) ;
			$Patient_Validated->date_encoded = date("m/d/Y",strtotime($tn)) ;
			$Patient_Validated->patient_enrollment_date = $tn ;
			$Patient_Validated->doctor_name = $request->enroll_doctors_name;
			$Patient_Validated->doctor_consent = $request->enroll_doctor_consent;		
			//$Patient_Validated->medrep = ;
			$Patient_Validated->hospital = $request->enroll_hospital;
			$Patient_Validated->patient_kit_number = $pxkit_enroll->one_quest_id;
			$Patient_Validated->patient_consent = "true";
			$Patient_Validated->patient_fullname = $request->enroll_firstname . " " . $request->enroll_middlename  . " " . $request->enroll_lastname;
			$Patient_Validated->patient_firstname = $request->enroll_firstname;
			$Patient_Validated->patient_middlename = $request->enroll_middlename;
			$Patient_Validated->patient_lastname = $request->enroll_lastname;
			$Patient_Validated->mobile_number = $request->enroll_mobile_no;
			$Patient_Validated->mobile_number_2 = $request->enroll_mobile_no_2;
			$Patient_Validated->phone_number = $request->enroll_phone_no;
			$Patient_Validated->nickname = $request->enroll_nickname;
			$Patient_Validated->address = $request->enroll_address;
			$Patient_Validated->age = $request->enroll_age;
			$Patient_Validated->birth_date = $request->enroll_birthdate;
			$Patient_Validated->patient_type = $request->patient_type;
			$Patient_Validated->doctor_wont_disclose = $request->doctor_wont_disclose;
			
			$Patient_Validated->patient_tagging_doctor_name =  $request->enroll_doctors_name;
			$Patient_Validated->patient_tagging_doctorid =  $request->doctor_id;
			$Patient_Validated->patient_tagging_mr_empcode = $request->medrep_id;
			
			$Patient_Validated->patient_tagging_product_prescribe = $request->enroll_product;
			
			
			// ADD BY CLEO TAGGING DETAILS			
			$Patient_Validated->doctor_name = $request->doctor;
			$Patient_Validated->hospital = $request->hospital;
			//$patient_Validated->updated_by = Auth::user()->name;
			$Patient_Validated->patient_mr_id = $request->patient_mr_id;
			$Patient_Validated->patient_tagging_mr_empcode = $request->patient_mr_id;
			$Patient_Validated->patient_mr_name = $request->patient_mr_name;
			$Patient_Validated->patient_tagging_team = $request->patient_tagging_team;
			$Patient_Validated->patient_tagging_area_code = $request->patient_tagging_area_code;
			$Patient_Validated->patient_tagging_doctor_name = $request->patient_tagging_doctor_name;
			
			if($request->doctor_wont_disclose == 'on')
			{$Patient_Validated->doctor_wont_disclose = "true";}
			else
			{$Patient_Validated->doctor_wont_disclose = "false";}
			
			$Patient_Validated->patient_tagging_hospital = $request->patient_tagging_hospital;
			$Patient_Validated->patient_tagging_md_class = $request->patient_tagging_md_class;
			$Patient_Validated->patient_tagging_specialty = $request->patient_tagging_specialty;
			$Patient_Validated->patient_tagging_doctorid = $request->patient_tagging_doctorid;
			$Patient_Validated->patient_tagging_emp_doctorid  = $request->patient_tagging_emp_doctorid;
			// END HERE
			
			
			$Patient_Validated->created_by = Auth::user()->name; 
			
			$sku_record = $sku::find($request->sku);
			if(!empty($sku_record))
			{
			$Patient_Validated->patient_tagging_sku_prescribe = $sku_record->skuname;
			}
			$Patient_Validated->save();

			$Patients_kit_allocation_reserve->where('one_quest_id','=',$pxkit_enroll->one_quest_id)->update(['product' => $request->enroll_product, 'patient_code' => $Patient_Validated->patient_code, 'used_by' => Auth::user()->id]);
		


			
			$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
			$patient_med_info->patient_id = $Patient_Validated->id;
			$patient_med_info->patient_code = $Patient_Validated->patient_code;
			$patient_med_info->doctors_id = $request->doctor_id;
			$patient_med_info->medrep_id = $request->medrep_id;
			$patient_med_info->sku = $request->sku;
			//$patient_med_info->sku_desc = $request->sku;
			$patient_med_info->brand = $request->enroll_product;
			$patient_med_info->no_of_tabs_prescribe = $request->notabsprescribe;
			$patient_med_info->tabs_prescribe_remarks = $request->tabsprescribe;
			$patient_med_info->no_of_days_prescribe = $request->nodaysprescribe;
			$patient_med_info->days_prescribe_remarks = $request->daysprescribe;
			$patient_med_info->total_no_of_tabs = $request->totalnotabs;
			$patient_med_info->total_tabs_remarks = $request->totaltabs;
			$patient_med_info->purchased = $request->purchased;
			$patient_med_info->save();
			
			
				if(!empty($request->enroll_mobile_no))
					{

					$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
					$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

					$msg = $txttemplates->where('id',9)->first();
		            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

					$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$request->enroll_mobile_no."&message=".$msg->message;
						$ch = curl_init();
						curl_setopt($ch,CURLOPT_URL,$url);
						curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
						curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
						$json = curl_exec($ch);
						if(!$json) {
						    echo curl_error($ch);
						}
						curl_close($ch);
					}
			
			}
			else if($request->action =='edit'){
				$Patient_Validated_Model= new \App\Http\Models\otsuka\Patient_Validated;
				$Patient_Validated = $Patient_Validated_Model::find($request->refid);
				
				$Patient_Validated->doctor_name = $request->enroll_doctors_name;
				$Patient_Validated->doctor_consent = $request->enroll_doctor_consent;		
				//$Patient_Validated->medrep = ;
				$Patient_Validated->hospital = $request->enroll_hospital;
				$Patient_Validated->gender = $request->enroll_gender;
				$Patient_Validated->patient_kit_number = preg_replace('/\s+/', '', $request->enroll_patient_kit_number);
				$Patient_Validated->patient_consent = $request->enroll_patient_consent;
				$Patient_Validated->patient_fullname = $request->enroll_firstname . " " . $request->enroll_middlename  . " " . $request->enroll_lastname;
				$Patient_Validated->doctor_consent = 'true';
				$Patient_Validated->patient_consent = 'true';
				$Patient_Validated->patient_firstname = $request->enroll_firstname;
				$Patient_Validated->patient_middlename = $request->enroll_middlename;
				$Patient_Validated->patient_lastname = $request->enroll_lastname;
				$Patient_Validated->mobile_number = $request->enroll_mobile_no;
				$Patient_Validated->mobile_number_2 = $request->enroll_mobile_no_2;
				$Patient_Validated->phone_number = $request->enroll_phone_no;
				$Patient_Validated->nickname = $request->enroll_nickname;
				$Patient_Validated->address = $request->enroll_address;
				$Patient_Validated->age = $request->enroll_age;
				$Patient_Validated->birth_date = $request->enroll_birthdate;
				$Patient_Validated->doctor_wont_disclose = $request->doctor_wont_disclose;
				
				
				$Patient_Validated->patient_tagging_product_prescribe = $request->enroll_product;
				if(!empty($request->sku))
				{//$Patient_Validated->patient_tagging_sku_prescribe = $request->sku;
				}
			
				$Patient_Validated->save();
				
					
				$patient_med_info_model = new \App\Http\Models\otsuka\Patients_med_info;
				
				$patient_med_info = $patient_med_info_model->where('patient_id','=',$request->refid)->first();
					
				if(!empty($patient_med_info))
				{$patient_med_info = $patient_med_info_model->where('patient_id','=',$request->refid)->first();
				echo "Existing";
				}
				else{
					$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
					echo "New";
				}
				//pre($patient_med_info_model);
				$patient_med_info->patient_id = $request->refid;
				$patient_med_info->patient_code = $Patient_Validated->patient_code;
				$patient_med_info->doctors_id = $request->doctor_id;
				$patient_med_info->medrep_id = $request->medrep_id;
				$patient_med_info->sku = $request->sku;
				
				$patient_med_info->brand = $request->enroll_product;
				$patient_med_info->no_of_tabs_prescribe = $request->notabsprescribe;
				$patient_med_info->tabs_prescribe_remarks = $request->tabsprescribe;
				$patient_med_info->no_of_days_prescribe = $request->nodaysprescribe;
				$patient_med_info->days_prescribe_remarks = $request->daysprescribe;
				$patient_med_info->total_no_of_tabs = $request->totalnotabs;
				$patient_med_info->total_tabs_remarks = $request->totaltabs;
				$patient_med_info->purchased = $request->purchased;
				$patient_med_info->save();				
				$now = date('Y-m-d');
				
				//$patients = new \App\Http\Models\otsuka\Patient_Validated;
				$sku = new \App\Http\Models\otsuka\Sku;
				$Patients_kit_allocation_reserve = new \App\Http\Models\otsuka\Patients_kit_allocation_reserve;
				$Mgabilify = $sku->where('is_active_in_oqs',1)->get();
				$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
				/*$Mgaminoleban = $sku->where('brand','like','%'.'AMINOLEBAN'.'%')->get();
				$Mgpletal = $sku->where('brand','like','%'.$v->medicine.'%')->get(); */
				$Reserve_Patient_Kit = $Patients_kit_allocation_reserve->where('emp_code','=','')->take(20)->get();
				
				
				

				return view('otsuka.dashboard.patient-masterfile',compact('patients_list','Reserve_Patient_Kit','Mgabilify'));
				
			}
	
		}
	
	

}