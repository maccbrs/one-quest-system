<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;


class testController extends Controller
{

	public function index(){

            $txtconnect = new \App\Http\Models\otsuka\Txtconnect;

            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

            $url = "https://lc-txtconnect5.globe.com.ph/inbox?token=".$token->token."&txtid=onequest";

            //pre($token);

		return view('otsuka.dashboard.test-page',compact('url','token'));
	}

}