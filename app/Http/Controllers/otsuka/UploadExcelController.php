<?php

namespace App\Http\Controllers\otsuka;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\Http\Models\otsuka\ExportDataTest;
use App\Http\Models\otsuka\Patients_kit_allocation;
class UploadExcelController extends Controller
{
	public function uploadExcelView(){

		return view('otsuka.upload.uploadExcel');
	}

	public function uploadExcel(Request $request){

		if($request->hasFile('uploadExcel')){
			$path = $request->file('uploadExcel')->getRealPath();
			$data = Excel::load($path, function($reader){

			})->get();
			$heading = $data->first()->keys()->toArray();
			
			//$heading = $data->getHeading();
			
			if(!empty($data) && $data->count()){

				foreach($data as $key => $d){

					//$medrep = new Medrep_distro;     
					$medrep = new ExportDataTest;     
					
					foreach($heading as $head){
						$medrep->$head = $d->$head;
					}
					$medrep->save(); 
				}
			}

		}

		return redirect()->back();
	}
	
	public function allocationFile(Request $request){

		if($request->hasFile('uploadExcel')){
			$path = $request->file('uploadExcel')->getRealPath();
			$data = Excel::load($path, function($reader){

			})->get();
			$heading = $data->first()->keys()->toArray();
			
			//$heading = $data->getHeading();
			
			if(!empty($data) && $data->count()){

				foreach($data as $key => $d){

					//$medrep = new Medrep_distro;     
					$medrep = new Patients_kit_allocation;     
					
					foreach($heading as $head){
						$medrep->$head = $d->$head;
					}
					$medrep->save(); 
					try {
					  // ...

					} catch ( \Illuminate\Database\QueryException $e) {
						var_dump($e->errorInfo );
					}
				}
			}

		}
		

		return redirect()->back();
	}
}
