<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;
use Session;


class otsukaController extends Controller
{

	public function index(){

/*		$test = new \App\Http\Models\otsuka\JasperIcbm;
		$var = $test->where('callid','=','300184312520171227')->get();
		echo($var);*/
		return view('otsuka.dashboard.index');
	}

	public function aeformget(){

		return view('otsuka.dashboard.pvform');
	}
	
	public function aeformsave(Request $request){
/* 		$sku = new \App\Http\Models\otsuka\Sku;		 */
		$aeform = new \App\Http\Models\otsuka\Ae_form;
		$aeform::create($request->all());
		echo "testy";
		return redirect('otsuka/pqc-ev-queue');
	}
	
	public function pvform(){

		return redirect()->back();
	}
	public function aeview($refid){
		$now = date('Y-m-d');
		$aeform = new \App\Http\Models\otsuka\Ae_form;
		$aeform_details = $aeform::find($refid);
		
	
		

		return view('otsuka.dashboard.pvform-view',compact('aeform_details'));
	}
	
	
		public function postae($refid){
		$now = date('Y-m-d');
		$ae = new \App\Http\Models\otsuka\Ae_form;
		
		$aelist = $ae::find($refid);
		$aelist->ae_status = "posted";	
		$aelist->save();

		//return view('otsuka.dashboard.pqcform-view',compact('pqclist'))->with('sku_list',$sku_list);;
	}
	
	public function aeupdate(Request $request){

		$now = date('Y-m-d');
		$aeform = new \App\Http\Models\otsuka\Ae_form;
		$pqc = new \App\Http\Models\otsuka\Pqc_form;
		$aeformdetails = $aeform::find($request->refid);

			foreach($aeform['fillable'] as $array_num => $table_column)
				{
					if(isset($request->$table_column))
						{
							$aeformdetails->$table_column = $request->$table_column;
						}
				}
		$aeformdetails->save();
		$aeform = new \App\Http\Models\otsuka\Ae_form;
		$aelist = $aeform->where('ae_status','=','new')->with(['fetch_user'])->get();
		//$pqclist = $pqc->all();
		$pqclist = $pqc->where('pqc_status','=','new')->with(['fetch_user'])->get();
		

		return view('otsuka.dashboard.pqc-ev',compact('pqclist','aelist'));
	}
	

	public function pqcformget(){
		$sku = new \App\Http\Models\otsuka\Sku;
		$sku_list = $sku->where('is_active_in_oqs','=','1')->get();
		return view('otsuka.dashboard.pqcform')->with('sku_list',$sku_list);
	}
	public function pqcsave(Request $request){
		$sku = new \App\Http\Models\otsuka\Sku;		
		$pqcform = new \App\Http\Models\otsuka\Pqc_form;
		$pqcform::create($request->all());
		//Product::create($request->all());		
		//echo 'save';
		//return view('otsuka.dashboard.pqcform');
		return redirect('otsuka/pqc-ev-queue');
	}
	
	public function pqcev_list(){
		$now = date('Y-m-d');
		$pqc = new \App\Http\Models\otsuka\Pqc_form;
		$aeform = new \App\Http\Models\otsuka\Ae_form;
		$aelist = $aeform->where('ae_status','=','new')->get();
		//$pqclist = $pqc->all();
		$pqclist = $pqc->where('pqc_status','=','new')->get();
		

		return view('otsuka.dashboard.pqc-ev',compact('pqclist','aelist'));
	}
	
	public function viewpqc($refid){
		$now = date('Y-m-d');
		$pqc = new \App\Http\Models\otsuka\Pqc_form;
		$sku = new \App\Http\Models\otsuka\Sku;
		$sku_list = $sku->where('is_active_in_oqs','=','1')->get();
		//pre($refid);
		$pqclist = $pqc::find($refid);
		

		return view('otsuka.dashboard.pqcform-view',compact('pqclist'))->with('sku_list',$sku_list);
	}
	public function postpqc($refid){
		$now = date('Y-m-d');
		$pqc = new \App\Http\Models\otsuka\Pqc_form;
		$sku = new \App\Http\Models\otsuka\Sku;
		$sku_list = $sku->where('is_active_in_oqs','=','1')->get();
		//pre($refid);
		$pqclist = $pqc::find($refid);
		$pqclist->pqc_status = "posted";	
		$pqclist->save();

		//return view('otsuka.dashboard.pqcform-view',compact('pqclist'))->with('sku_list',$sku_list);;
	}

	public function pqcupdate(Request $request){

		$now = date('Y-m-d');
		$pqc = new \App\Http\Models\otsuka\Pqc_form;

		$pqcdetails = $pqc::find($request->refid);

			foreach($pqc['fillable'] as $array_num => $table_column)
				{
					if(isset($request->$table_column))
						{
							$pqcdetails->$table_column = $request->$table_column;
						}
				}
		$pqcdetails->save();
		$aeform = new \App\Http\Models\otsuka\Ae_form;
		$aelist = $aeform->where('ae_status','=','new')->get();
		//$pqclist = $pqc->all();
		$pqclist = $pqc->where('pqc_status','=','new')->get();
		

		return view('otsuka.dashboard.pqc-ev',compact('pqclist','aelist'));
		//return redirect('otsuka/pqc-ev-queue');
		//echo "test";
		
		//pre($pqc);
		//$pqclist->
		//$pqclist->pqc_status = "posted";	
		//$pqclist->save();

		//return view('otsuka.dashboard.pqcform-view',compact('pqclist'))->with('sku_list',$sku_list);;
	}


	public function update_image(Request $r){
			$message ="";
			$type="";
			
/*		$otsukaReceipt = new \App\Http\Models\otsuka\Receipt;
		*/

		//return $r->all();

			$queue = new \App\Http\Models\otsuka\Queue;
			$connect = time();
			$queue->queue_name = $r->input('type')." ".$r->input('medicine')." ".date("Y-m-d H:i:s");
			$queue->type = $r->input('type');
			$queue->medicine = $r->input('medicine');
			$queue->connector = $connect;
			$queue->queued_by = Auth::user()->id;
			$queue->save();

			if($r->hasFile('file'))
			{
	      /*  $images = $r->file('images');*/
	        foreach($r->file as $image){
				
			
			
	        $file = new \App\Http\Models\otsuka\Upload;
			$GemUser = new \App\Http\Models\gem\GemUser;
			$user = $GemUser->find(Auth::user()->id);
	       
	        $file->added_by = $user->id;
	        $file->medicine = $r->input('medicine');
	        $file->type = $r->input('type');
	        $file->patient_name = $r->input('patient_name');
	        $file->phone_no = $r->input('phone_no');
			$file->mobile_number = $r->input('mobile_no');
	    	$file->connector_id = $connect;
			$file->status = "On Queue";
	    	$file->uploaded_by = Auth::user()->id;
			$file->trail_status = 100;
	    	$file->save();
			$lastUploadId = $file->id; //$file::all()-> pluck('id')->last() +1; 
			
	       
			
			$path_parts = pathinfo($image->getClientOriginalName());	
	        $filename  = $r->input('type') . '-' . 'oqs-' . $lastUploadId . '.' . $path_parts['extension'];

/*	        $sm = public_path('uploads/oqs-'.$filename);
	        $m = public_path('uploads/oqs-'.$filename);*/
	        $l = public_path('uploads/'.$filename);	
/*	        \Image::make($image->getRealPath())->resize(50, 50)->save($sm);
	        \Image::make($image->getRealPath())->resize(100, 100)->save($m);
	        \Image::make($image->getRealPath())->resize(800, 600)->save($l);*/
	        \Image::make($image->getRealPath())->orientate()->save($l);
			
			
			$file->filename = $filename;
			$file->original_filename = $image->getClientOriginalName();
	        $file->location = $l;
			
			$file->save();
			
			
			
	    	$images[]=$file;
			}

			
			//pre($type);
/*			pre($images);*/
		/*return redirect()->back();*/
		return view('otsuka.dashboard.index',compact('message','images'))->with('type',$type);

			}
	}

		public function disable(Request $r){
		$users = new \App\Http\Models\gem\User;
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;


		$disable = $med_upload->where('id', '=', $r->active)
		->update(
			[
				'active' => 0
			]
		);
		return redirect()->back();
	}
	
		public function disable_upload(Request $request){
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$disable = $med_upload->where('id', '=', $request->active)
		->update(
			[
				'active' => 0
			]
		);
		$log_msg = "upload-id " . $request->active  . " has been move to archive " ;
		$Logger = new AppHelper;
		$Logger->SaveLog($log_msg);
		
		//return redirect('otsuka/agent');
	}

	public function logout(Request $request){
		Auth::logout();
		Session::flush();

  		return redirect('/');
	}
	

}