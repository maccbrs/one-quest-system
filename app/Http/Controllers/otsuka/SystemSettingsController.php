<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Models\otsuka\SystemSetting;
use Auth;
class SystemSettingsController extends Controller
{
    public function index(){

    	$data = SystemSetting::where('tag_deleted', 0)->get();
    	return view('otsuka.settings.system_settings', compact ('data'));
    }


    public function addSystem(Request $request){

    	$data = new SystemSetting;
    	$data->key = $request->key;
    	$data->value = $request->value;
    	$data->remarks = $request->remarks;
    	$data->created_by = Auth::user()->id;
    	$data->save();

    	return redirect()->back()->with('addsuccess', 'Success');
    }

    public function updateSystem(Request $request){

    	$data = SystemSetting::where('id', $request->hidden_id)
    	->update([
    		'key' => $request->update_key,
    		'value' => $request->update_value,
    		'remarks' => $request->update_remarks,
    		'updated_by' => Auth::user()->id
    	]);

    	return redirect()->back()->with('updatesuccess', 'Updated Successfully');
    }

    public function deleteSystem(Request $request){

    	$data = SystemSetting::where('id', $request->delete_id)
    	->update([
    		'tag_deleted' => '1'
    	]);

    	return redirect()->back()->with('deletesuccess', 'Successfully Deleted');
    }
}
