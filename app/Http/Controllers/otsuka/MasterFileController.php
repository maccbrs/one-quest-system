<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;


class MasterFileController extends Controller
{

	public function index(){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->orderBy('created_at','asc')->get();
		$tn = date("Y-m-d H:i:s");


		return view('otsuka.masterfile.index',compact('Queue','Upload','Mr','Px','tn'));
	}
	
	
	public function sku_groupings(Request $request){
		$model = new \App\Http\Models\otsuka\Sku_groupings;	
		$defaultlimit = 1000;
		$filter ="";
		$filter_search = "";
		if(!empty($request->limit))			
		{$defaultlimit = $request->limit;}
		
		if(!empty($request->filter))
		{
			if($request->filter != "all")
			{	
			$data = $model::where('tag_deleted',0)->where($request->filter,"like","%" . $request->filter_search . "%")->orderBy('one_quest_id','desc')->take($defaultlimit)->get();
			$filter_search = $request->filter_search;
			$filter =  $request->filter;
			}
			else
			{
				$data = $model::where('tag_deleted',0)->orderBy('id','desc')->take($defaultlimit)->get();	
			}
		}
		else		
		{
		$data = $model::where('tag_deleted',0)->orderBy('id','desc')->take($defaultlimit)->get();	
		}
		
		$columns =  $model['fillable'];
		//pre($data['fillable']);
		//$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->orderBy('created_at','asc')->get();
		//$tn = date("Y-m-d H:i:s");


		return view('otsuka.masterfile.records',compact('columns','data','filter_search','filter'));
	}
	
	
	
	
	public function patient_kit_allocation(Request $request){
		$model = new \App\Http\Models\otsuka\Patients_kit_allocation;	
		$defaultlimit = 1000;
		$filter ="";
		$filter_search = "";
		if(!empty($request->limit))			
		{$defaultlimit = $request->limit;}
		
		if(!empty($request->filter))
		{
			if($request->filter != "all")
			{	
			$data = $model::where('tag_deleted',0)->where($request->filter,"like","%" . $request->filter_search . "%")->orderBy('one_quest_id','desc')->take($defaultlimit)->get();
			$filter_search = $request->filter_search;
			$filter =  $request->filter;
			}
			else
			{
				$data = $model::where('tag_deleted',0)->orderBy('one_quest_id','desc')->take($defaultlimit)->get();	
			}
		}
		else		
		{
		$data = $model::where('tag_deleted',0)->orderBy('one_quest_id','desc')->take($defaultlimit)->get();	
		}
		
		$columns =  $model['fillable'];
		//pre($data['fillable']);
		//$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->orderBy('created_at','asc')->get();
		//$tn = date("Y-m-d H:i:s");


		return view('otsuka.masterfile.records',compact('columns','data','filter_search','filter'));
	}
	
	
	
	public function patient_kit_allocation_save(Request $request){
		//pre( $request->one_quest_id);
		$model = new \App\Http\Models\otsuka\Patients_kit_allocation;	
		$update_model = $model->find($request->id);
		$columns =  $model['fillable'];
		foreach($columns  as $key => $val)
		{
			if(!empty($request->$val))
			{
			$update_model->$val = $request->$val;
			}	
		}
		
		
		
		
		$update_model->save();
		
		
		
		
		$defaultlimit = 1000;
		$filter ="";
		$filter_search = "";
		if(!empty($request->limit))			
		{$defaultlimit = $request->limit;}
		
		if(!empty($request->filter))
		{
			if($request->filter != "all")
			{	
			$data = $model::where('tag_deleted',0)->where($request->filter,"like","%" . $request->filter_search . "%")->orderBy('one_quest_id','desc')->take($defaultlimit)->get();
			$filter_search = $request->filter_search;
			$filter =  $request->filter;
			}
			else
			{
				$data = $model::where('tag_deleted',0)->orderBy('one_quest_id','desc')->take($defaultlimit)->get();	
			}
		}
		else		
		{
		$data = $model::where('tag_deleted',0)->orderBy('one_quest_id','desc')->take($defaultlimit)->get();	
		}
		
		
		return back()->with('filter', $filter)->with('filter_search', $filter_search)->with('data', $data)->with('columns', $columns);
		//return view('otsuka.masterfile.records',compact('columns','data','filter_search','filter'));
	}
	
	
	public function national_md_list(Request $request){
		$model = new \App\Http\Models\otsuka\National_md_list;	
		$defaultlimit = 1000;
		$filter ="";
		$filter_search = "";
		if(!empty($request->limit))			
		{$defaultlimit = $request->limit;}
		
		if(!empty($request->filter))
		{
			if($request->filter != "all")
			{	
			$data = $model::where('tag_deleted',0)->where($request->filter,"like","%" . $request->filter_search . "%")->take($defaultlimit)->get();
			$filter_search = $request->filter_search;
			$filter =  $request->filter;
			}
			else
			{
				$data = $model::where('tag_deleted',0)->take($defaultlimit)->get();	
			}
		}
		else		
		{
		$data = $model::where('tag_deleted',0)->take($defaultlimit)->get();	
		}
		
		$columns =  $model['fillable'];
		//pre($data['fillable']);
		//$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->orderBy('created_at','asc')->get();
		//$tn = date("Y-m-d H:i:s");
		return view('otsuka.masterfile.national-md-list',compact('columns','data','filter_search','filter'));
	}
	public function national_md_list_save(Request $request){
		//pre( $request->one_quest_id);
		$model = new \App\Http\Models\otsuka\National_md_list;	
		$update_model = $model->find($request->id);
		$columns =  $model['fillable'];
		foreach($columns  as $key => $val)
		{
			if(!empty($request->$val))
			{
			$update_model->$val = $request->$val;
			}	
		}
		
		$update_model->save();
		
		
		$defaultlimit = 1000;
		$filter ="";
		$filter_search = "";
		if(!empty($request->limit))			
		{$defaultlimit = $request->limit;}
		
		if(!empty($request->filter))
		{
			if($request->filter != "all")
			{	
			$data = $model::where('tag_deleted',0)->where($request->filter,"like","%" . $request->filter_search . "%")->take($defaultlimit)->get();
			$filter_search = $request->filter_search;
			$filter =  $request->filter;
			}
			else
			{
				$data = $model::where('tag_deleted',0)->take($defaultlimit)->get();	
			}
		}
		else		
		{
		$data = $model::where('tag_deleted',0)->take($defaultlimit)->get();	
		}
		
		
		return back()->with('filter', $filter)->with('filter_search', $filter_search)->with('data', $data)->with('columns', $columns);
		//return view('otsuka.masterfile.records',compact('columns','data','filter_search','filter'));
	}
	
	
	
	
	public function GetUpload(Request $request){
		$patients = new \App\Http\Models\otsuka\Upload;
		$records = $patients::find($request->upload_id);	
		echo json_encode($records);
		
		
	}

	public function queues_done_index(){
		$med_upload = new \App\Http\Models\otsuka\Upload;

		return view('otsuka.masterfile.queues_done_index');
	}
	
	public function queues_done_save(Request $request){
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;

		$med_upload = $med_upload
						->where('id',$request->upload_id)
						->update(['patient_kit_number' => $request->patient_kit_number, 'medicine' => $request->medicine, 'patient_name' => $request->patient_fullname, 'mobile_number' => $request->mobile_number, 'mobile_number_2' => $request->mobile_number_2, 'phone_no' => $request->phone_number, 'doctor_name' => $request->doctor_name, 'hospital_name' => $request->hospital_name, 'doctor_id' => $request->doctor_id]);

		if(!empty($request->change_dispo))
		{
			$crc = $call_verification
					->where('id',$request->c_id)
					->update(['remarks' => $request->change_dispo]);

			$upload = $med_upload
						->where('id',$request->upload_id)
						->update(['verification_status' => $request->change_dispo]);
		}

		return view('otsuka.masterfile.queues_done_index');	
	}
	



}