<?php namespace App\Http\Controllers\otsuka;
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type: application/json');

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;


class redemptionreportController extends Controller
{

	public function index(){

        

		return view('otsuka.dashboard.test-page',compact('url','token'));
	}
	
	public function dispatch_report(Request $request){
		$dispatch_model = new \App\Http\Models\otsuka\Dispatch_status;
		//$dispatch_record = $dispatch_model->where('etimestamp','like', "%" . $request->dispatcheddatefrom . "%")->with(['fetch_patient_details','fetch_brand_desc','sku_product'])->take(2000)->get();
		//$dispatch_record = $dispatch_model->whereBetween('etimestamp', [$request->dispatcheddatefrom, $request->dispatcheddateto])->with(['fetch_patient_details','fetch_brand_desc','sku_product'])->take(2000)->get();
		$dispatch_record = $dispatch_model->whereBetween('etimestamp', [$request->dispatcheddatefrom, $request->dispatcheddateto])->with(['fetch_patient_details','fetch_brand_desc','sku_product'])->where('tag_deleted',0)->get();
     //   echo "test";

		//pre($dispatch_record);
		return view('otsuka.dashboard.redemption-report',compact('dispatch_record'));
	}
	
	public function zpc_dispatch_report(Request $request){
		$dispatch_model = new \App\Http\Models\otsuka\Dispatch_status;
		
				$dispatch_record = $dispatch_model->whereBetween('etimestamp', [$request->dispatcheddatefrom, $request->dispatcheddateto])->with(['fetch_patient_details','fetch_brand_desc','sku_product'])->where('tag_deleted',0)
		
		->update(
						[
							'date_dispatch_generated' => date("Y-m-d H:i:s"),
							'generated_by' => Auth::user()->name
						
						]
					);	
		//$dispatch_record = $dispatch_model->where('etimestamp','like', "%" . $request->dispatcheddatefrom . "%")->with(['fetch_patient_details','fetch_brand_desc','sku_product'])->take(2000)->get();
		//$dispatch_record = $dispatch_model->whereBetween('etimestamp', [$request->dispatcheddatefrom, $request->dispatcheddateto])->with(['fetch_patient_details','fetch_brand_desc','sku_product'])->take(2000)->get();
		$dispatch_record = $dispatch_model->whereBetween('etimestamp', [$request->dispatcheddatefrom, $request->dispatcheddateto])->with(['fetch_patient_details','fetch_brand_desc','sku_product'])->where('tag_deleted',0)
		->get();
		
		

     //   echo "test";

		//pre($dispatch_record);
		return view('otsuka.dashboard.zpc-dispatch-report',compact('dispatch_record'));
	}
	
	
}