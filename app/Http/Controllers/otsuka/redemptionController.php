<?php namespace App\Http\Controllers\otsuka;
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type: application/json');

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;


class redemptionController extends Controller
{

	public function index(Request $request){
		$Patients_kit_allocation_reserve = new \App\Http\Models\otsuka\Patients_kit_allocation_reserve;
		$Reserve_Patient_Kit = $Patients_kit_allocation_reserve->whereNull('used_by')->take(20)->get();

		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload; 
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$Patient_Validated = new \App\Http\Models\otsuka\Patient_Validated;		
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;	
		$upload_supporting_docs_model = new \App\Http\Models\otsuka\UploadSupportingDocs;
		
		$Call_Status = new \App\Http\Models\otsuka\Call_status;
		$Call_status_list = $Call_Status::All();
		$Sku = new \App\Http\Models\otsuka\Sku;
		$Mgabilify = $Sku->where('is_active_in_oqs',1)->get();
		$Sku_list = $Sku::All();
		
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$Encoded_Purchases = new \App\Http\Models\otsuka\Encoded_Purchases;
		$Encoded_Purchases_Item_Dtl= new \App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl;
		$Ytd_Redemption = new \App\Http\Models\otsuka\Ytd_Redemption;
		$sku_programs_model = new \App\Http\Models\otsuka\Psp_Sku_Program;		
		$List_of_Purchases_sku;
		$Total_Med_Purchase ="";
		$Total_Med_Redeem ="";
		$myarray = explode(",",$request->upload_id);
		$product_purchased_per_sku_list = array();
		$product_redeem_per_sku_list = array();
		$product_redeem_per_sku_list_forthemonth = array();
		$sku_details = array();
		$sku_program_dtl = array();
		

		$product_list_sum;

	if($request->action == "redemption")
		{ 
			$Patient_Validated = new \App\Http\Models\otsuka\Patient_Validated;
			$Encoded_Purchases_Validated = new \App\Http\Models\otsuka\Encoded_Purchases_Validated;

			$Encoded_Purchases_Item_Dtl_Validated = new \App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl_Validated;
			$Total_Med_Purchase = $Patient_Validated->select('TotalMed')->where('id','=',$request->patient_id)->first();
			$Total_Med_Redeem = $Patient_Validated->select('TotalRedeem')->where('id','=',$request->patient_id)->first();
			$patient_record = $Patient_Validated->where('id','=',$request->patient_id)->with(['fetch_receipt_encoded','fetch_redeem_product','fetch_sku'])->get();
			//$patient_record = $Patient_Validated->where('id','=',$request->patient_id)->get();
			//$Patient_Validated-
			//pre($patient_record[0]->fetch_sku->fetch_prescribed->sku_details);
			if(!empty($patient_record[0]->patient_code)){
			$upload_supporting_docs = $upload_supporting_docs_model->where('patient_id','=',$patient_record[0]->id)->get();
			//pre($upload_supporting_docs);
			$List_of_Purchases_sku = $Ytd_Redemption::where('patient_code','=',$patient_record[0]->patient_code)->where('tag_deleted',0)->distinct()->get(['sku_purchased']);
				
				foreach($List_of_Purchases_sku as $key => $val)
				{
					//pre($val);
					//echo $val->sku_purchased;
					$product_purchased_per_sku_list[$val->sku_purchased . "SANDMAN"] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('sku_purchased','=',$val->sku_purchased)->where('tag_deleted',0)->where('created_by','=','SANDMAN')->sum('no_tab');
					$product_purchased_per_sku_list[$val->sku_purchased] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('sku_purchased','=',$val->sku_purchased)->where('tag_deleted',0)->where('created_by','!=','SANDMAN')->sum('no_tab');
					$product_redeem_per_sku_list[$val->sku_purchased . "SANDMAN"] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('sku_purchased','=',$val->sku_purchased)->where('tag_deleted',0)->where('created_by','=','SANDMAN')->sum('no_tab_free');
					$product_redeem_per_sku_list[$val->sku_purchased] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('sku_purchased','=',$val->sku_purchased)->where('created_by','!=','SANDMAN')->where('tag_deleted',0)->sum('no_tab_free');
					$product_redeem_per_sku_list_forthemonth[$val->sku_purchased] = $Ytd_Redemption->where('date_of_redemption','like','%' . date('Y-m') . '%')->where('patient_code','=',$patient_record[0]->patient_code)->where('tag_deleted',0)->where('sku_purchased','=',$val->sku_purchased)->sum('no_tab_free');				
					$sku_details[$val->sku_purchased] = $Sku->where('skuname','=',$val->sku_purchased)->first();		

					$sku_program_dtl[$val->sku_purchased] = $sku_programs_model::where("skuid","=",$sku_details[$val->sku_purchased]->id)->where('created_by','=','MSE')->with('fetch_program_desc')->first();
					//pre($sku_program_dtl);
					//pre($sku_program_dtl);
					$sku_program_dtl[$val->sku_purchased . 'SANDMAN'] = $sku_programs_model::where("skuid","=",$sku_details[$val->sku_purchased]->id)->where('created_by','=','SANDMAN')->with('fetch_program_desc')->first();
					
				}
			}

			

			
			$Encoded_Purchases = $Encoded_Purchases_Validated->where('patient_id','=',$request->patient_id)->where('patient_id','!=','')->with('fetch_encoded_dtl')->where('tag_deleted',0)->orderby('id','desc')->get();
			$old_retrieval_enrollment_record = $old_retrieval_enrollment->where('patientid','',$request->patient_id)->get()->first();
			$basicinfo = $med_upload::with('receipt_details')->with('user')->where('type','=','Receipt')->where('trail_status','>=',300)->whereIn('id',explode(',',$request->upload_id))->where('redeem_assign_to','=',null)->get();
			if(!empty($basicinfo->encoded_purchases_id)){
			//$or_sku_details = $Encoded_Purchases_Item_Dtl->where('encoded_purchases_id','=',$basicinfo->encoded_purchases_id)->with(['sku_details'])->get();
			}
			
				
			return view('otsuka.dashboard.redemption',compact('Call_status_list',
						'Queue',
						'Upload',
						'Mr',
						'Px',
						'tn',
						'Queue_cv',
						'Encoded_Purchases',
						'patient_record',
						'basicinfo',
						'or_sku_details',
						'Sku_list',
						'Reserve_Patient_Kit',
						'old_retrieval_enrollment_record',
						'Total_Med_Redeem',
						'Total_Med_Purchase',
						'List_of_Purchases_sku',
						'product_purchased_per_sku_list',
						'product_redeem_per_sku_list',
						'product_redeem_per_sku_list_forthemonth',
						'sku_program_dtl',
						'upload_supporting_docs',
						'Mgabilify',
						'sku_details'));
		}
	elseif ($request->action == "redemptionv2")
		{ 
			$Patient_Validated = new \App\Http\Models\otsuka\Patient_Validated;
			$Encoded_Purchases_Validated = new \App\Http\Models\otsuka\Encoded_Purchases_Validated;

			$Encoded_Purchases_Item_Dtl_Validated = new \App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl_Validated;
			$Total_Med_Purchase = $Patient_Validated->select('TotalMed')->where('id','=',$request->patient_id)->first();
			$Total_Med_Redeem = $Patient_Validated->select('TotalRedeem')->where('id','=',$request->patient_id)->first();
			$patient_record = $Patient_Validated->where('id','=',$request->patient_id)->with(['fetch_receipt_encoded','fetch_redeem_product','fetch_sku'])->get();
			//$patient_record = $Patient_Validated->where('id','=',$request->patient_id)->get();
			//$Patient_Validated-
			//pre($patient_record[0]->fetch_sku->fetch_prescribed->sku_details);
			if(!empty($patient_record[0]->patient_code)){
			$upload_supporting_docs = $upload_supporting_docs_model->where('patient_id','=',$patient_record[0]->id)->get();
			//pre($upload_supporting_docs);
			$List_of_Purchases_sku = $Ytd_Redemption::where('patient_code','=',$patient_record[0]->patient_code)->where('tag_deleted',0)->distinct()->get(['sku_purchased']);
			$List_of_product = 	$Ytd_Redemption::where('patient_code','=',$patient_record[0]->patient_code)->where('tag_deleted',0)->distinct()->get(['product_purchased']);
				
				foreach($List_of_product as $key => $val)
				{	
				
				
					$dt = date('Y-m-d');
					
					$buy = 1;
					$free = 1;
					$from = date("Y-m-01", strtotime($dt)).' 00:00:00';
					$to = date('Y-m-d').' 20:30:00';
					$monthlimit = 1;
					
					$product_purchased_per_sku_list[$val->product_purchased] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('product_purchased','=',$val->product_purchased)->where('tag_deleted',0)->sum('no_tab');
					$product_redeem_per_sku_list[$val->product_purchased] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('product_purchased','=',$val->product_purchased)->where('tag_deleted',0)->sum('no_tab_free');
					
					if($val->product_purchased == "PLETAAL")
					{
						$buy = 40;
						$free = 20;
						$monthlimit = 40;
						
					}else if ($val->product_purchased == "ABILIFY")
					{
						$buy = 10;
						$free = 5;
						$monthlimit = 30;
					}
					
					$Var_Balance = $product_purchased_per_sku_list[$val->product_purchased] - (($product_redeem_per_sku_list[$val->product_purchased] * $buy) / $free);
					
					
					$redeemable[$val->product_purchased] = (intval($Var_Balance / $buy) * $free);
					
					if($redeemable[$val->product_purchased]> 0)
					{
					$product_balance[$val->product_purchased] = $Var_Balance - (( $redeemable[$val->product_purchased] * $buy) / $free );
					}
					else
					{
					$product_balance[$val->product_purchased] =	$Var_Balance;
					}
					//$product_balance[$val->product_purchased] = $product_purchased_per_sku_list[$val->product_purchased] - (($product_redeem_per_sku_list[$val->product_purchased] * $buy) / $free);
					
					$buy_array[$val->product_purchased] = $buy;
					$free_array[$val->product_purchased] = $free;
					
					$no_redeem_current_month[$val->product_purchased] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->whereBetween('created_at_date', [$from, $to])->where('product_purchased','=',$val->product_purchased)->where('tag_deleted',0)->sum('no_tab_free');
					$monthlimit_array[$val->product_purchased] = $monthlimit - $no_redeem_current_month[$val->product_purchased];
					if($monthlimit_array[$val->product_purchased] < 0 )
					{
						$monthlimit_array[$val->product_purchased] = 0;						
					}
					///$product_display_balance[$val->product_purchased] = 
					
					
					
				}
				
				
				
				
				
				
				foreach($List_of_Purchases_sku as $key => $val)
				{
					//pre($val);
					//echo $val->sku_purchased;
					//$product_purchased_per_sku_list["product_purchased_Total"] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('product_purchased','=',$val->product_purchased)->where('tag_deleted',0)->sum('no_tab');
					//$product_redeem_per_sku_list["product_purchased_Total"] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('product_purchased','=',$val->product_purchased)->where('tag_deleted',0)->sum('no_tab_free');
					
					
					
					$product_purchased_per_sku_list[$val->sku_purchased . "SANDMAN"] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('sku_purchased','=',$val->sku_purchased)->where('tag_deleted',0)->where('created_by','=','SANDMAN')->sum('no_tab');
					$product_purchased_per_sku_list[$val->sku_purchased] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('sku_purchased','=',$val->sku_purchased)->where('tag_deleted',0)->where('created_by','!=','SANDMAN')->sum('no_tab');
					$product_redeem_per_sku_list[$val->sku_purchased . "SANDMAN"] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('sku_purchased','=',$val->sku_purchased)->where('tag_deleted',0)->where('created_by','=','SANDMAN')->sum('no_tab_free');
					$product_redeem_per_sku_list[$val->sku_purchased] = $Ytd_Redemption->where('patient_code','=',$patient_record[0]->patient_code)->where('sku_purchased','=',$val->sku_purchased)->where('created_by','!=','SANDMAN')->where('tag_deleted',0)->sum('no_tab_free');
					$product_redeem_per_sku_list_forthemonth[$val->sku_purchased] = $Ytd_Redemption->where('date_of_redemption','like','%' . date('Y-m') . '%')->where('patient_code','=',$patient_record[0]->patient_code)->where('tag_deleted',0)->where('sku_purchased','=',$val->sku_purchased)->sum('no_tab_free');				
					$sku_details[$val->sku_purchased] = $Sku->where('skuname','=',$val->sku_purchased)->first();		

					$sku_program_dtl[$val->sku_purchased] = $sku_programs_model::where("skuid","=",$sku_details[$val->sku_purchased]->id)->where('created_by','=','MSE')->with('fetch_program_desc')->first();
					//pre($sku_program_dtl);
					//pre($sku_program_dtl);
					$sku_program_dtl[$val->sku_purchased . 'SANDMAN'] = $sku_programs_model::where("skuid","=",$sku_details[$val->sku_purchased]->id)->where('created_by','=','SANDMAN')->with('fetch_program_desc')->first();
					
				}
				
				
				
				
				
				
				
			}

			

			
			$Encoded_Purchases = $Encoded_Purchases_Validated->where('patient_id','=',$request->patient_id)->where('patient_id','!=','')->with('fetch_encoded_dtl')->where('tag_deleted',0)->orderby('id','desc')->get();
			$old_retrieval_enrollment_record = $old_retrieval_enrollment->where('patientid','',$request->patient_id)->get()->first();
			$basicinfo = $med_upload::with('receipt_details')->with('user')->where('type','=','Receipt')->where('trail_status','>=',300)->whereIn('id',explode(',',$request->upload_id))->where('redeem_assign_to','=',null)->get();
			if(!empty($basicinfo->encoded_purchases_id)){
			//$or_sku_details = $Encoded_Purchases_Item_Dtl->where('encoded_purchases_id','=',$basicinfo->encoded_purchases_id)->with(['sku_details'])->get();
			}
			
				
			return view('otsuka.dashboard.redemptionv2',compact('Call_status_list',
						'Queue',
						'Upload',
						'Mr',
						'Px',
						'tn',
						'Queue_cv',
						'Encoded_Purchases',
						'patient_record',
						'basicinfo',
						'or_sku_details',
						'Sku_list',
						'Reserve_Patient_Kit',
						'old_retrieval_enrollment_record',
						'Total_Med_Redeem',
						'Total_Med_Purchase',
						'List_of_Purchases_sku',
						'List_of_product',
						'product_balance',
						'redeemable',
						'buy_array',
						'free_array',
						'no_redeem_current_month',
						'monthlimit_array',
						'product_purchased_per_sku_list',
						'product_redeem_per_sku_list',
						'product_redeem_per_sku_list_forthemonth',
						'sku_program_dtl',
						'upload_supporting_docs',
						'Mgabilify',
						//NEW 
						'product_purchased',
						
						'sku_details'));
		}


	else if ($request->action == "view")
		{	
			$Encoded_Purchases_Item_Dtl_Validated = new \App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl_Validated;
			$Encoded_Purchases_Validated = new \App\Http\Models\otsuka\Encoded_Purchases_Validated;
			$basicinfo = $med_upload::with('receipt_details')->with('user')->where('type','=','Receipt')->whereIn('id',explode(',',$request->upload_id))->where('redeem_assign_to','=',null)->get();
			$Encoded_Purchases = $Encoded_Purchases_Validated->where('patient_id','=',$request->patient_id)->with('fetch_encoded_dtl')->where('tag_deleted',0)->orderby('id','desc')->get();
			return view('otsuka.dashboard.redemption',compact('Call_status_list','Queue','Upload','Mr','Px','tn','Queue_cv','Encoded_Purchases','patient_record','basicinfo','or_sku_details','Sku_list','Reserve_Patient_Kit'));
		}
		
		//return view('otsuka.dashboard.utility',compact('patient_record','Encoded_Purchases'));
	
	}
	public function assign_patient(Request $request){
		$Patient_Validated_Model = new \App\Http\Models\otsuka\Patient_Validated;
		$Patient_Validated =  $Patient_Validated_Model::find($request->patient_id);
		$med_upload = new \App\Http\Models\otsuka\Upload; 
		$basicinfo_list = $med_upload::find(explode(',',$request->upload_id_list));
		foreach($basicinfo_list as $basicinfo){
		$basicinfo->redemption_process_by =  Auth::user()->id;
		//$basicinfo->redeem_assign_to =  Auth::user()->id;
		$basicinfo->trail_status = 301;
		$basicinfo->redeem_by =  $request->patient_id;
		$basicinfo->save();
		}

		//return redirect('otsuka/redemption?action=redemption&upload_id=' . $request->upload_id_list . '&refid=' .  $Patient_Validated->id );

	}

	
	public function done(Request $request){
	
		$med_upload = new \App\Http\Models\otsuka\Upload; 
		
		$basicinfo = $med_upload::find($request->upload_id);
		//$basicinfo->redemption_process_by =  Auth::user()->id;
		//$basicinfo->redeem_assign_to =  Auth::user()->id;
		$basicinfo->trail_status = 1000;
		//$basicinfo->redeem_by =  $request->patient_id;		
		$basicinfo->save();
		

		return redirect('otsuka/agent/redemption-queue');

	}


	
	public function save_call_log(Request $request){
		//pre('test');
		
		
		$cmid = new \App\Http\Models\otsuka\Cmid; 		
		$callVerification = new \App\Http\Models\otsuka\Call_verification; 		
		$med_upload = new \App\Http\Models\otsuka\Upload; 
		$upload_record = $med_upload->whereIn('id',explode(',',$request->upload_id))->get();
		foreach($upload_record as $key => $val);
		{
			$val->redemption_remarks = $request->remarks_others;
			$val->save();
			$mobile_number = $val->mobile_number;
		}
		//$cmid->channel = ;
		$cmid->calltype =  4;
		$cmid->purpose =  4;
		$cmid->caller =  0;
		$cmid->cardnumberid = 0 ;
		//$cmid->patientid =  ;
		$cmid->reid = 0;
		$cmid->employeeid = Auth::user()->id;;
		//$cmid->remarks = ;
		$cmid->active = 	1;	
		$cmid->upload_id = $request->upload_id;		
		$cmid->save();
		$callVerification->attempt = 1;
		$callVerification->cmid = $cmid->id;
		$callVerification->date = date('m/d/Y'); 
		$callVerification->remarks = $request->remarks_call;
		$callVerification->remarks_others =  $request->remarks_others;
		$callVerification->callnotes = $request->callnotes;
		$callVerification->employeeid =  Auth::user()->id;
		$callVerification->save();

		if($request->remarks_call == '6')
			{
							$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
							$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

							$msg = $txttemplates->where('id',5)->first();
							$token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

											
							$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$mobile_number."&message=".$msg->message;
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_URL,$url);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
							$json = curl_exec($ch);
							if(!$json) {
								echo curl_error($ch);
							}
							curl_close($ch);
						
				
				
			}


		
		
		//echo  $callVerification->id;



		//$basicinfo = $med_upload::find($request->upload_id);		
		//$basicinfo->save();
		//return redirect('otsuka/agent');

	}

	
 
	public function assign_receipt_to_patient(Request $request){
		$Ytd_Redemption = new \App\Http\Models\otsuka\Ytd_Redemption;
		$med_upload = new \App\Http\Models\otsuka\Upload; 
		$basicinfo = $med_upload::find($request->assign_upload_id);
		
		$Encoded_Purchases = new \App\Http\Models\otsuka\Encoded_Purchases;
		$Encoded_Purchases_Item_Dtl = new \App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl;
		
		$Encoded_Purchases_Validated = new \App\Http\Models\otsuka\Encoded_Purchases_Validated;
		//$Encoded_Purchases_Item_Dtl_Validated = new \App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl_Validated;
		$Receipt = $Encoded_Purchases::find($basicinfo->encoded_purchases_id);
		$Receipt_Dtl = $Encoded_Purchases_Item_Dtl->where('encoded_purchases_id','=',$basicinfo->encoded_purchases_id)->with('sku_details')->get();
		
		$Patient_Validated_Model = new \App\Http\Models\otsuka\Patient_Validated;
		$Patient_Validated =  $Patient_Validated_Model::find($request->patient_id);
		
		$Encoded_Purchases_Validated->cardnumberid = $Receipt->cardnumberid ;
		$Encoded_Purchases_Validated->notcredited = $Receipt->notcredited ;
		$Encoded_Purchases_Validated->patient_id = $request->patient_id ;
		$Encoded_Purchases_Validated->cmid = $Receipt->cmid ;
		$Encoded_Purchases_Validated->submission_date = $Receipt->submission_date ;
		$Encoded_Purchases_Validated->branch = $Receipt->branch ;
		$Encoded_Purchases_Validated->or_date = $Receipt->or_date ;
		$Encoded_Purchases_Validated->or_number = $Receipt->or_number ;
		$Encoded_Purchases_Validated->file_path = $Receipt->file_path ;
		$Encoded_Purchases_Validated->added_by = $basicinfo->added_by ;
		$Encoded_Purchases_Validated->remarks = $Receipt->remarks ;		
		$Encoded_Purchases_Validated->created_by = Auth::user()->name; ;		
		$Encoded_Purchases_Validated->save() ;
		$basicinfo->redeem_assign_to = $request->patient_id;		
		$basicinfo->redeem_assign_to_date = date('Y-m-d h:i:s');	
		
		
	 	if ($Patient_Validated->date_of_first_purchase == null)
			{
				$Patient_Validated->date_of_first_purchase = $Receipt->or_date;
				$Patient_Validated->save();
			}
	 
		
		
		 if(!empty($Receipt_Dtl))
		{
			foreach($Receipt_Dtl as $key => $val)
			{
			$Encoded_Purchases_Item_Dtl_Validated = new \App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl_Validated;
			$Encoded_Purchases_Item_Dtl_Validated->encoded_purchases_id = $Encoded_Purchases_Validated->id;
			$Encoded_Purchases_Item_Dtl_Validated->sku = $val->sku;
			$Encoded_Purchases_Item_Dtl_Validated->qty = $val->qty;
			$Encoded_Purchases_Item_Dtl_Validated->active = $val->active;
			$Encoded_Purchases_Item_Dtl_Validated->ds_id = $val->ds_id;
			$Encoded_Purchases_Item_Dtl_Validated->remarks = $val->remarks;
			$Encoded_Purchases_Item_Dtl_Validated->or_id = $val->or_id;
			$Encoded_Purchases_Item_Dtl_Validated->created_by = Auth::user()->name;
			$Encoded_Purchases_Item_Dtl_Validated->save();			
				$ytd_counter = new \App\Http\Models\otsuka\Ytd_Redemption;
				$counter = $ytd_counter->where('patient_code','=',$Patient_Validated->patient_code)->where('tag_deleted',0)->count();
				/*$ytd_counter = new \App\Http\Models\otsuka\Ytd_Redemption;
				$counter =  $ytd_counter->where('patient_code','=','OQPXD0006824')->count(); */
				
				
				$create_ydt_redemption = new \App\Http\Models\otsuka\Ytd_Redemption;
				$create_ydt_redemption->patient_code = $Patient_Validated->patient_code;
				$create_ydt_redemption->Programid = ""; // PROGRAM CODE
				$create_ydt_redemption->OneQuestNumber =  $Patient_Validated->patient_kit_number;
				$create_ydt_redemption->Patient_Kit_Number = $Patient_Validated->patient_kit_number; // OLD PATIENT KIT
				
				
				
				$create_ydt_redemption->Allocation_team = $Patient_Validated->patient_tagging_team;
				$create_ydt_redemption->Allocation_area = $Patient_Validated->patient_tagging_area_code;
				$create_ydt_redemption->Allocation_mr_name  = $Patient_Validated->patient_mr_name;
				$create_ydt_redemption->Team = $Patient_Validated->patient_tagging_team;
				$create_ydt_redemption->Area_code = $Patient_Validated->patient_tagging_area_code;
				$create_ydt_redemption->MR_name  = $Patient_Validated->patient_mr_name;
				//$create_ydt_redemption->enrollment_date  = "";
				$create_ydt_redemption->enrollment_type = $Patient_Validated->patient_type;
				$create_ydt_redemption->enrollment_md_id  = $Patient_Validated->patient_tagging_doctorid;
				$create_ydt_redemption->enrollment_md_name = $Patient_Validated->patient_tagging_doctor_name;
				$create_ydt_redemption->Date_verify = "";
				/*$create_ydt_redemption->Allocation_team = "";
				$create_ydt_redemption->Allocation_area = "" ;
				$create_ydt_redemption->Allocation_mr_name  = "";
				$create_ydt_redemption->Team = "";
				$create_ydt_redemption->Area_code = "";
				$create_ydt_redemption->MR_name  = "";
				$create_ydt_redemption->enrollment_date  = "";
				$create_ydt_redemption->enrollment_type = "";
				$create_ydt_redemption->enrollment_md_id  = "";
				$create_ydt_redemption->enrollment_md_name = ""; */
				$create_ydt_redemption->date_of_redemption  = date('Y-m-d h:i:s');
				$create_ydt_redemption->product_purchased = $val->sku_details->brand;
				$create_ydt_redemption->sku_purchased = $val->sku_details->skuname;
				$create_ydt_redemption->no_tab = $val->qty;
				$create_ydt_redemption->prod_redemp  = "";
				$create_ydt_redemption->sku_redemp  = "";
				$create_ydt_redemption->no_tab_free = "";
				$create_ydt_redemption->no_non_med = "";
				$create_ydt_redemption->date_of_purchased  = $val->or_id;;
				$create_ydt_redemption->date_of_submission  = $basicinfo->created_at;
				
				$dt = date('Y-m-d');
				$first_day = date("Y-m-01", strtotime($dt)).' 00:00:00';
				$now = date('Y-m-d').' 20:30:00';
				
				if(($Patient_Validated->created_at  > $first_day) && ($Patient_Validated->created_at <= $now))
				{
					$create_ydt_redemption->px_class  = "NEW";
					$create_ydt_redemption->px_class_3  = "NEW";
				}
				else{
					$create_ydt_redemption->px_class  = "OLD";
					$create_ydt_redemption->px_class_3  = "OLD";
				}
					
				/*
				if($counter > 0){
				$create_ydt_redemption->px_class  = "OLD";
				}
				else
				{
				$create_ydt_redemption->px_class  = "NEW";	
				}
				*/
				$create_ydt_redemption->or_number  = $Receipt->or_number;
				//$create_ydt_redemption->created_at = date('Y-m-d H:mm:ss');
				$create_ydt_redemption->created_at_date =  date('Y-m-d');
				$create_ydt_redemption->created_by = Auth::user()->name;
				//$create_ydt_redemption->updated_at = date('Y-m-d H:mm:ss');
				$create_ydt_redemption->updated_by	= Auth::user()->name;
				$create_ydt_redemption->save();




			}

		
		}
		/* $med_upload = new \App\Http\Models\otsuka\Upload; 
		$basicinfo = $med_upload::find($request->upload_id);
		$basicinfo->redemption_process_by =  Auth::user()->id;
		//$basicinfo->redeem_assign_to =  Auth::user()->id;
		$basicinfo->trail_status = 301;
		$basicinfo->redeem_by =  $request->patient_id;
		//$basicinfo->redeem_date =  Auth::user()->id;
		//pre($basicinfo); */
		//$basicinfo->trail_status = 303;
		//$basicinfo->save();		
		//return redirect('otsuka/redemption?action=redemption&upload_id=' . $basicinfo->id . '&refid=' .  $Patient_Validated->id );
		$basicinfo->trail_status = 1000;
		$basicinfo->save();
	}	 


	
	public function enroll_patient(Request $request){
		$return_value = array();
		if($request->action =='save')
		{
		//$Patient_Validated 
		$Patient_Validated_Model= new \App\Http\Models\otsuka\Patient_Validated;
		//$Patient_Validated->patient_kit_number = $request->enroll_patient_kit
		if($request->enroll_refid !='' )
		{ 
		$Patient_Validated = $Patient_Validated_Model::find($request->enroll_refid);
		//$Patient_Validated->remarks ="test";
		}
		else
		{
			$Patient_Validated = new \App\Http\Models\otsuka\Patient_Validated;
			//$Patient_Validated->remarks ="NONON";
		}

		$Patients_kit_allocation_reserve = new \App\Http\Models\otsuka\Patients_kit_allocation_reserve;
		$patient_kit_number = $Patients_kit_allocation_reserve->select('id','one_quest_id')->whereNull('used_by')->orderBy('id','asc')->first();
		$Patients_kit_allocation_reserve->where('id',$patient_kit_number->id)->update(['used_by' => Auth::user()->id]);

		$Patient_Validated->patient_kit_number = $patient_kit_number->one_quest_id;
		$Patient_Validated->enrollment_date = $request->enroll_date;
		$Patient_Validated->doctor_name = $request->enroll_doctorname;
		$Patient_Validated->doctor_consent = $request->enroll_doctorconsent;		
		//$Patient_Validated->medrep = ;
		$Patient_Validated->hospital = $request->hospital;
		//$Patient_Validated->time_to_call = $request->time_to_call;
		$Patient_Validated->patient_consent = $request->enroll_patientconsent;
		$Patient_Validated->patient_fullname = $request->enroll_fullname;
		$Patient_Validated->patient_firstname = $request->enroll_firstname;
		$Patient_Validated->patient_lastname = $request->enroll_lastname;
		$Patient_Validated->patient_middlename = $request->enroll_middlename;
		$Patient_Validated->nickname = $request->enroll_nickname;
		$Patient_Validated->address = $request->enroll_address;
		$Patient_Validated->age = $request->enroll_age;
		$Patient_Validated->birth_date = $request->enroll_birthdate;
		$Patient_Validated->mobile_number = $request->mobile_number;
		$Patient_Validated->created_by =  Auth::user()->name;
	
		$Patient_Validated->save();

		echo $Patient_Validated->id; 
		}			
		if($request->action =='edit')
		{
		//$Patient_Validated 
		$Patient_Validated_Model= new \App\Http\Models\otsuka\Patient_Validated;
		//$Patient_Validated->patient_kit_number = $request->enroll_patient_kit
		if($request->patient_id !='' )
		{ 
		$Patient_Validated = $Patient_Validated_Model::find($request->patient_id);
		//$Patient_Validated->remarks ="test";
		}
		else
		{
			$Patient_Validated = new \App\Http\Models\otsuka\Patient_Validated;
			//$Patient_Validated->remarks ="NONON";
		}

		$Patients_kit_allocation_reserve = new \App\Http\Models\otsuka\Patients_kit_allocation_reserve;
		//$patient_kit_number = $Patients_kit_allocation_reserve->select('id','one_quest_id')->whereNull('used_by')->orderBy('id','asc')->first();
		//$Patients_kit_allocation_reserve->where('id',$patient_kit_number->id)->update(['used_by' => Auth::user()->id]);

		//$Patient_Validated->patient_kit_number = $patient_kit_number->one_quest_id;
		if(!empty($request->remarks))
		{$Patient_Validated->remarks = $request->remarks;}
		if(!empty($request->enroll_date))
		{$Patient_Validated->enrollment_date = $request->enroll_date;}
		if(!empty($request->enroll_doctorname))
		{$Patient_Validated->doctor_name = $request->enroll_doctorname;}
		if(!empty($request->enroll_doctorconsent))
		{$Patient_Validated->doctor_consent = $request->enroll_doctorconsent;}
		//$Patient_Validated->medrep = ;
		if(!empty($request->hospital))
		{$Patient_Validated->hospital = $request->hospital;}
		//$Patient_Validated->time_to_call = $request->time_to_call;
		if(!empty($request->enroll_patientconsent))
		{$Patient_Validated->patient_consent = $request->enroll_patientconsent;}
		if(!empty($request->patient_fullname))
		{$Patient_Validated->patient_fullname = $request->patient_fullname;}
		if(!empty($request->patient_firstname))
		{$Patient_Validated->patient_firstname = $request->patient_firstname;}
		if(!empty($request->patient_lastname))
		{$Patient_Validated->patient_lastname = $request->patient_lastname;}
		if(!empty($request->patient_middlename))
		{$Patient_Validated->patient_middlename = $request->patient_middlename;}
		if(!empty($request->enroll_nickname))
		{$Patient_Validated->nickname = $request->enroll_nickname;}
		if(!empty($request->mobile_number))
		{$Patient_Validated->mobile_number = $request->mobile_number;}
		if(!empty($request->mobile_number_2))
		{$Patient_Validated->mobile_number_2 = $request->mobile_number_2;}
		if(!empty($request->phone_number))
		{$Patient_Validated->phone_number = $request->phone_number;}
		if(!empty($request->address))
		{$Patient_Validated->address = $request->address;}
		if(!empty($request->age))
		{$Patient_Validated->age = $request->age;}
		if(!empty($request->birth_date))
		{$Patient_Validated->birth_date = $request->birth_date;}
		if(!empty($request->gender))		
		{$Patient_Validated->gender = $request->gender;}
		
		$Patient_Validated->updated_by =  Auth::user()->name;

		//$Patient_Validated->

		$Patient_Validated->save();

		echo $Patient_Validated->id; //return ;
		}
		

			
	}
	
		public function updated_patient_med_info(Request $request){		
			$Patient_Validated_model = new \App\Http\Models\otsuka\Patient_Validated;
			$patient_Validated = $Patient_Validated_model->find($request->patient_id);
			$Patient_Validated_med_info_model = new \App\Http\Models\otsuka\Patients_med_info;
			$sku_model = new \App\Http\Models\otsuka\Sku;
			$sku_records = $sku_model->where('id','=',$request->product_sku)->first();
			//pre($patient_Validated->doctor_name);
			$Patient_Validated_med_info = $Patient_Validated_med_info_model->where('patient_id','=',$request->patient_id)->first();
			//TAGGING PATIENT
			
			
			$patient_Validated->doctor_name = $request->doctor;
			
			
			//
			
			$patient_Validated->doctor_name = $request->doctor;
			$patient_Validated->hospital = $request->hospital;
			$patient_Validated->updated_by = Auth::user()->name;
			$patient_Validated->patient_mr_id = $request->patient_mr_id;
			$patient_Validated->patient_tagging_mr_empcode = $request->patient_mr_id;
			$patient_Validated->patient_mr_name = $request->patient_mr_name;
			$patient_Validated->patient_tagging_team = $request->patient_tagging_team;
			$patient_Validated->patient_tagging_area_code = $request->patient_tagging_area_code;
			$patient_Validated->patient_tagging_doctor_name = $request->patient_tagging_doctor_name;
			
			if($request->doctor_wont_disclose == 'on')
			{$patient_Validated->doctor_wont_disclose = "true";}
			else
			{$patient_Validated->doctor_wont_disclose = "false";}
			
			$patient_Validated->patient_tagging_hospital = $request->patient_tagging_hospital;
			$patient_Validated->patient_tagging_md_class = $request->patient_tagging_md_class;
			$patient_Validated->patient_tagging_specialty = $request->patient_tagging_specialty;
			$patient_Validated->patient_tagging_doctorid = $request->patient_tagging_doctorid;
			$patient_Validated->patient_tagging_emp_doctorid  = $request->patient_tagging_emp_doctorid;
			$patient_Validated->patient_tagging_product_prescribe  = $request->product;
			if(!empty($sku_records))
			{
			$patient_Validated->patient_tagging_sku_prescribe  = $sku_records->skuname;
			}
			$patient_Validated->save();
			
			if(!empty($Patient_Validated_med_info))
			{	$Patient_Validated_med_info->sku = $request->product_sku;
				//$Patient_Validated_med_info_model->patient_id = $request->patient_id;
				//$Patient_Validated_med_info_model->patient_code = $patient_Validated->patient_code;
				$Patient_Validated_med_info->doctors_id = $request->doctor_id;
				$Patient_Validated_med_info->medrep_id = $request->medrep_id;
				$Patient_Validated_med_info->medrep_id = $request->p_name;
				$Patient_Validated_med_info->brand = $request->product;
				$Patient_Validated_med_info->no_of_tabs_prescribe = $request->notabsprescribe;
				if(!empty($request->tabsprescribe))
				{$Patient_Validated_med_info->tabs_prescribe_remarks = $request->tabsprescribe;
				}
				
				$Patient_Validated_med_info->no_of_days_prescribe = $request->nodaysprescribe;
				if(!empty($request->daysprescribe))
				{$Patient_Validated_med_info->days_prescribe_remarks = $request->daysprescribe;}
			
				if(!empty($request->totaltabs))
				{$Patient_Validated_med_info->total_tabs_remarks = $request->totaltabs;}
				$Patient_Validated_med_info->total_no_of_tabs = $request->totalnotabs;
			
				$Patient_Validated_med_info->purchased =  $request->purchased;
				
				$Patient_Validated_med_info->updated_by = Auth::user()->name;
				$Patient_Validated_med_info->save();
				//pre($Patient_Validated_med_info);
			}
			else
			{	$Patient_Validated_med_info_model = new \App\Http\Models\otsuka\Patients_med_info;
				$Patient_Validated_med_info_model->patient_id = $request->patient_id;
				$Patient_Validated_med_info_model->patient_code = $patient_Validated->patient_code;
				$Patient_Validated_med_info_model->medrep_id = $request->medrep_id;
				$Patient_Validated_med_info_model->doctors_id = $request->doctor_id;
				$Patient_Validated_med_info_model->medrep_id =  $request->p_name;
				$Patient_Validated_med_info_model->sku =  $request->product_sku;
				$Patient_Validated_med_info_model->brand = $request->product;
				$Patient_Validated_med_info_model->no_of_tabs_prescribe = $request->notabsprescribe;
				$Patient_Validated_med_info_model->tabs_prescribe_remarks =  $request->tabsprescribe;
				$Patient_Validated_med_info_model->no_of_days_prescribe = $request->nodaysprescribe;
				$Patient_Validated_med_info_model->days_prescribe_remarks = $request->daysprescribe;
				$Patient_Validated_med_info_model->total_tabs_remarks =$request->totaltabs;
				$Patient_Validated_med_info_model->total_no_of_tabs = $request->totalnotabs;
				$Patient_Validated_med_info_model->purchased = $request->purchased;
				$Patient_Validated_med_info_model->created_by = Auth::user()->name;
				$Patient_Validated_med_info_model->save();
				
			}
				
			//$Patient_Validated_med_info = 
			
		
		
		}
		
		public function Save_Dispatchmst(Request $request){			
		//echo "test";
		$Dispatch_status_mst = new \App\Http\Models\otsuka\Dispatch_status_mst;

		
		$Dispatch_status_mst->dsid = "";
		$Dispatch_status_mst->patient_id = $request->patient_id;
		$Dispatch_status_mst->completiondate = "";
		$Dispatch_status_mst->programid = $request->patient_id;
		$Dispatch_status_mst->brandid = $request->brand_id;
		$Dispatch_status_mst->skuid = $request->sku_id;
		$Dispatch_status_mst->qtytabs = $request->qtytabs;
		$Dispatch_status_mst->qtynonmed = $request->qtynonmed;
		$Dispatch_status_mst->dispatched = 1;
		$Dispatch_status_mst->dispatcheddate = date("Y/m/d H:i:s");
		$Dispatch_status_mst->etimestamp = date("Y/m/d H:i:s");
		$Dispatch_status_mst->active = 1;
		$Dispatch_status_mst->save();
		return $Dispatch_status_mst->id;
		}

	
		public function Save_Dispatch(Request $request){			
		//echo "test";
		$Dispatch_status_draft = new \App\Http\Models\otsuka\Dispatch_status_draft;

		
		$Dispatch_status_draft->dsid_mst = $request->dsid_mst;
		$Dispatch_status_draft->patient_id = $request->patient_id;
		$Dispatch_status_draft->completiondate = "";
		$Dispatch_status_draft->programid = $request->programid;
		$Dispatch_status_draft->brandid = $request->brand_id;
		$Dispatch_status_draft->skuid = $request->sku_id;
		$Dispatch_status_draft->qtytabs = $request->qtytabs;
		$Dispatch_status_draft->qtynonmed = $request->qtynonmed;
		$Dispatch_status_draft->dispatched = 0;
		$Dispatch_status_draft->dispatcheddate = date("Y/m/d H:i:s");
		$Dispatch_status_draft->etimestamp = date("Y/m/d H:i:s");
		$Dispatch_status_draft->active = 1;
		$Dispatch_status_draft->save();
		return $Dispatch_status_draft->id;
		}

		public function Dispatch_post(Request $request){	
		//$med_upload = new \App\Http\Models\otsuka\Upload; 		
		//echo "test";
		//$upload = $med_upload->find([upload_id]);
		$Patient_Validated_Model = new \App\Http\Models\otsuka\Patient_Validated;
		$Dispatch_status_draft = new \App\Http\Models\otsuka\Dispatch_status_draft;
		$Dispatch_status_draft_record = $Dispatch_status_draft->where('dsid_mst','=',$request->dsid_mst)->with('sku_product')->where('tag_deleted',0)->get();	
		
		foreach($Dispatch_status_draft_record as $key => $val)
		{
		$Patient_Validated = $Patient_Validated_Model->find($val->patient_id);
		$Dispatch_status = new \App\Http\Models\otsuka\Dispatch_status;
		$Dispatch_status->dsid = $val->dsid_mst;
		$Dispatch_status->patient_id = $val->patient_id;
		$Dispatch_status->completiondate = date("Y-m-d H:i:s");
		$Dispatch_status->programid = $val->programid;
		$Dispatch_status->brandid = $val->brandid;
		$Dispatch_status->skuid = $val->skuid;
		$Dispatch_status->qtytabs = $val->qtytabs;
		$Dispatch_status->qtynonmed = $val->qtynonmed;
		$Dispatch_status->dispatched = 1;
		$Dispatch_status->dispatcheddate = date("Y-m-d H:i:s");
		$Dispatch_status->etimestamp = date("Y-m-d H:i:s");
		$Dispatch_status->created_by =  Auth::user()->name;
		$Dispatch_status->active = 1;
		$Dispatch_status->save();
		


				if(($val->qtytabs != "" ) && (($val->qtytabs != NULL )))
				{
				$checkYTD = new \App\Http\Models\otsuka\Ytd_Redemption;	
				$create_ydt_redemption = new \App\Http\Models\otsuka\Ytd_Redemption;
				$redeem_counter = $checkYTD->where('patient_code','=',$Patient_Validated->patient_code)->where('no_tab_free','!=','')->where('tag_deleted',0)->count();
				$redeem_counter2 = $checkYTD->where('patient_code','=',$Patient_Validated->patient_code)->where('tag_deleted',0)->count();
				if($redeem_counter <= 0){
				$Patient_Validated->date_of_first_redeem =  date('Y-m-d H:i:s');
				$create_ydt_redemption->redemption_status = "First";
				}
				else{
				$create_ydt_redemption->redemption_status = "Repeat";	
				}
				$Patient_Validated->redeem_counter = $redeem_counter + 1;
				$Patient_Validated->save();
				
				
				$create_ydt_redemption->patient_code = $Patient_Validated->patient_code;
				$create_ydt_redemption->Programid = $val->programid; // PROGRAM CODE
				$create_ydt_redemption->OneQuestNumber =  $Patient_Validated->patient_kit_number;
				$create_ydt_redemption->Patient_Kit_Number = ""; // OLD PATIENT KIT
				//$create_ydt_redemption->Date_verify = "";
				$create_ydt_redemption->Allocation_team = $Patient_Validated->patient_tagging_team;
				$create_ydt_redemption->Allocation_area = $Patient_Validated->patient_tagging_area_code;
				$create_ydt_redemption->Allocation_mr_name  = $Patient_Validated->patient_mr_name;
				$create_ydt_redemption->Team = $Patient_Validated->patient_tagging_team;
				$create_ydt_redemption->Area_code = $Patient_Validated->patient_tagging_area_code;
				$create_ydt_redemption->MR_name  = $Patient_Validated->patient_mr_name;
				//$create_ydt_redemption->enrollment_date  = "";
				$create_ydt_redemption->enrollment_type = $Patient_Validated->patient_type;
				$create_ydt_redemption->enrollment_md_id  = $Patient_Validated->patient_tagging_doctorid;
				$create_ydt_redemption->enrollment_md_name = $Patient_Validated->patient_tagging_doctor_name;
				$create_ydt_redemption->date_of_redemption  = date('Y-m-d h:i:s');
				$create_ydt_redemption->product_purchased = $val->sku_product->brand;;
				$create_ydt_redemption->sku_purchased = $val->sku_product->skuname;
				$create_ydt_redemption->no_tab = $val->qty;
				$create_ydt_redemption->prod_redemp  = $val->sku_product->brand;;
				$create_ydt_redemption->sku_redemp  = $val->sku_product->skuname;;
				$create_ydt_redemption->no_tab_free = $val->qtytabs;
				$create_ydt_redemption->no_non_med = $val->qtynonmed;
				$create_ydt_redemption->date_of_purchased  = "";
				$create_ydt_redemption->date_of_submission  = date('Y-m-d H:i:s');
				
				$dt = date('Y-m-d');
				$first_day = date("Y-m-01", strtotime($dt)).' 00:00:00';
				$now = date('Y-m-d').' 20:30:00';
				
				if(($Patient_Validated->created_at  > $first_day) && ($Patient_Validated->created_at <= $now))
				{
					$create_ydt_redemption->px_class  = "NEW";
					$create_ydt_redemption->px_class_3  = "NEW";
				}
				else{
					$create_ydt_redemption->px_class  = "OLD";
					$create_ydt_redemption->px_class_3  = "OLD";
				}
					
				
				
				
				/*
				if($redeem_counter2 > 0)
				{$create_ydt_redemption->px_class  = "OLD";}
				else
				{
					$create_ydt_redemption->px_class  = "NEW";
				}
				*/
				//$create_ydt_redemption->or_number  = "";
				//$create_ydt_redemption->created_at = date('Y-m-d H:mm:ss');
				$create_ydt_redemption->created_at_date =  date('Y-m-d');
				$create_ydt_redemption->created_by = Auth::user()->name;
				//$create_ydt_redemption->updated_at = date('Y-m-d H:mm:ss');
				$create_ydt_redemption->updated_by	= Auth::user()->name;
				$create_ydt_redemption->save();
				}
		
													
							
						


		}

							$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
							$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

							$msg = $txttemplates->where('id',6)->first();
							$token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

											
							$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$Patient_Validated->mobile_number."&message=".$msg->message;
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_URL,$url);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
							$json = curl_exec($ch);
							if(!$json) {
								echo curl_error($ch);
							}
							curl_close($ch);
		
		//return $Dispatch_status->id; */
		}
		
		
		public function removedtl(Request $request){			
		
		$encoded_purchases_item_dtl = new \App\Http\Models\otsuka\Dispatch_status_draft;
		$records = $encoded_purchases_item_dtl::find($request->id);
		$records->delete();
		
		}
	
	
		public function revert_receipt(Request $request){			
		$Uploads = new \App\Http\Models\otsuka\Upload;
		$upload_record = $Uploads->where('id','=',$request->receipt_upload_id)->first();
		if(($upload_record->audit_trail == "") || ($upload_record->audit_trail == null))
		{
		$upload_record->audit_trail = "Encoded By :" . $upload_record->encoded_by->name ."-" . $upload_record->encoded_at . "<br/>Revert By : " . Auth::user()->name . "-" . date('Y-m-d H:i:s') . "<br/>Revert_remarks :" . $request->revert_remarks ;
		}
		else
		{
		$upload_record->audit_trail = $upload_record->audit_trail . "|" ."Encoded By :" . $upload_record->encoded_by->name ."-" . $upload_record->encoded_at . "<br/>Revert By : " . Auth::user()->name . "-" . date('Y-m-d H:i:s') . "<br/>Revert_remarks :" . $request->revert_remarks ;	
		}
		$upload_record->claimed  = 0;
		$upload_record->claimed_by  = null;
		$upload_record->status  = "On Queue";	
		$upload_record->submit  = 0;
		$upload_record->trail_status  = 105;
		
		
		
		$upload_record->save();
		//pre($upload_record->audit_trail);
		
		
		
		}
	
	
}