<?php namespace App\Http\Controllers\otsuka;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Image;
use File;
use Alert;
use Hash;
use Mail;

class uploaderController extends Controller
{

	public function index(){
		$upload_supporting_docs = new \App\Http\Models\otsuka\UploadSupportingDocs;
		$supporting_docs_list = $upload_supporting_docs->whereNull('patient_id')->where('tag_deleted',0)->get();
		return view('otsuka.uploader.index',compact('supporting_docs_list'));
	}
	
	
	public function view_all(){
		$upload_supporting_docs = new \App\Http\Models\otsuka\UploadSupportingDocs;
		$supporting_docs_list = $upload_supporting_docs->where('tag_deleted',0)->get();
		return view('otsuka.uploader.index',compact('supporting_docs_list'));
	}
	
	

	public function uploadimage(Request $r){
		$return_mgs = "";
		if(($r->file('file')) && ($r->uploaded_to)){
		
		$help = new \App\Http\Controllers\topaz\Helper;
		$upload_supporting_docs = new \App\Http\Models\otsuka\UploadSupportingDocs;
		//$GemUser = new \App\Http\Models\topaz\GemUsers;
		//$user = $GemUser->find(Auth::user()->id);
		//$upload_supporting_docs
		//if($user):

	        $image = $r->file('file');
	        //$filename  = Auth::user()->id . '.' . $image->getClientOriginalExtension();
	        $upload_supporting_docs->uploaded_to = $r->uploaded_to;
	        $upload_supporting_docs->remarks = $r->remarks;
	        $upload_supporting_docs->doctorname = $r->doctorname;
	        $upload_supporting_docs->number_of_tabs = $r->number_of_tabs;
	        $upload_supporting_docs->product_prescribe = $r->product_prescribe;
	        $upload_supporting_docs->rx_date = $r->rx_date;
	        $upload_supporting_docs->created_by = Auth::user()->name;

	        $upload_supporting_docs->save();	       
			$upload_supporting_docs->filename =  Auth::user()->id . '-' . date('Y') . date('m') . ( $upload_supporting_docs->id) . '.'  . $image->getClientOriginalExtension(); 
			$l = public_path('uploads/supportingdocs/' . $upload_supporting_docs->filename);
			\Image::make($image->getRealPath())->orientate()->save($l);
			
			$upload_supporting_docs->save();
		//return redirect()->back();
		//echo "Success";
		$return_mgs	 = "File Successfully Uploaded.";
		$return_result = '1';	
		return redirect()->back()->with('return_msg', $return_mgs)->with('return_result', $return_result);
		}
		else{
			if(!($r->uploaded_to))
			{$return_mgs = $return_mgs . "Please Input PatientName.<br/>";}
			if(!($r->file('file')))
			{$return_mgs = $return_mgs . "Please Attach File .png .jpg .<br/>";}
		$return_result	 = '0';		
		return redirect()->back()->with('return_msg', $return_mgs)->with('return_result', $return_result);
		}
	}
	
	
	
	
	public function Update(Request $r){
		$return_mgs = "";
		if($r->uploaded_to){
		
		//$help = new \App\Http\Controllers\topaz\Helper;
		
		$model = new \App\Http\Models\otsuka\UploadSupportingDocs;
		
		$upload_supporting_docs = $model->find($r->id);
		//$GemUser = new \App\Http\Models\topaz\GemUsers;
		//$user = $GemUser->find(Auth::user()->id);
		//$upload_supporting_docs
		//if($user):

	        //$image = $r->file('file');
	        //$filename  = Auth::user()->id . '.' . $image->getClientOriginalExtension();
	        $upload_supporting_docs->uploaded_to = $r->uploaded_to;
	        $upload_supporting_docs->remarks = $r->remarks;
	        $upload_supporting_docs->doctorname = $r->doctorname;
	        $upload_supporting_docs->doctor_emp_id = $r->doctor_emp_id;
	        $upload_supporting_docs->number_of_tabs = $r->number_of_tabs;
	        $upload_supporting_docs->product_prescribe = $r->product_prescribe;
	        $upload_supporting_docs->rx_date = $r->rx_date;
	        //$upload_supporting_docs->created_by = Auth::user()->name;

	        $upload_supporting_docs->save();	       
			//$upload_supporting_docs->filename =  Auth::user()->id . '-' . date('Y') . date('m') . ( $upload_supporting_docs->id) . '.'  . $image->getClientOriginalExtension(); 
			//$l = public_path('uploads/supportingdocs/' . $upload_supporting_docs->filename);
			//\Image::make($image->getRealPath())->orientate()->save($l);
			
			//$upload_supporting_docs->save();
		//return redirect()->back();
		//echo "Success";
		$return_mgs	 = "Successfully Updated.";
		$return_result = '1';	
		return redirect()->back()->with('return_msg', $return_mgs)->with('return_result', $return_result);
		}
		else{
			if(!($r->uploaded_to))
			{$return_mgs = $return_mgs . "Please Input PatientName.<br/>";}
			if(!($r->file('file')))
			{$return_mgs = $return_mgs . "Please Attach File .png .jpg .<br/>";}
		$return_result	 = '0';		
		return redirect()->back()->with('return_msg', $return_mgs)->with('return_result', $return_result);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	public function view_upload_docs(Request $request){
		$upload_supporting_docs = new \App\Http\Models\otsuka\UploadSupportingDocs;
		$supporting_docs_list = $upload_supporting_docs->whereNull('patient_id')->where('tag_deleted',0)->get();
		$skuid = $request->skuid;
		$programid = $request->programid;

		$tn = date("Y-m-d H:i:s");

			
		return view('otsuka.uploader.view-upload_docs',compact('supporting_docs_list','skuid','programid'));					
			
	}
	
	
	public function assign_rx_to_patient(Request $request){
		
		//echo "test";
		
		$Patient_Validated = new \App\Http\Models\otsuka\Patient_Validated;
		$upload_supporting_docs = new \App\Http\Models\otsuka\UploadSupportingDocs;
		$patient_info = $Patient_Validated->with('fetch_mr')->find($request->patient_id);
		$uploaded_docs = $upload_supporting_docs->find($request->uploaded_id);
		$supporting_docs_list = $upload_supporting_docs::All();
		$tn = date("Y-m-d H:i:s");
		
		//pre($EmailSubject->value);
		$uploaded_docs->patient_id = $request->patient_id;
		$uploaded_docs->backend_status = "Has Been Assign To patient";
		$uploaded_docs->frontend_status = "Has Been Assign To patient";
		$uploaded_docs->updated_by = Auth::user()->name;
		$uploaded_docs->save();

		//pre($request->total_dispatch_for_the_month);
		
		$var_total_dispatch_for_the_Month = $request->total_dispatch_for_the_month;
		$var_total_free_goods_for_redemption = $request->total_free_goods_for_redemption;
		$var_total_qty_purchased = $request->total_qty_purchased;
		$var_total_total_redeem = $request->total_total_redeem;
		$var_program_name_id =  $request->program_name_id;
		$Sku_purchased = $request->sku_purchases;
		$sku_programs_model = new \App\Http\Models\otsuka\Psp_Sku_Program;	
		$programmodel = new \App\Http\Models\otsuka\Psp_Program;	
		$sku_program = $programmodel::find($var_program_name_id);

		$valid_purchases_balance = $var_total_qty_purchased - (($var_total_total_redeem / $sku_program->free) * $sku_program->buy) ;
		//pre($valid_purchases_balance);
		$Patient_Validated = new \App\Http\Models\otsuka\Patient_Validated;
		
		//$eod = $EodDetails::find($emailer_id);
		//pre($EodDetails);
					//$to = $patient_info->fetch_mr->fetch_psm_list->email;
		        	//$from = json_decode($eod->sender,true);
		        	//$cc= json_decode($eod->cc,true);
		        	//$bcc = ['hazel.osias@magellan-solutions.com']; 
					//$val = "test";
					/*
					$SystemSetting = new \App\Http\Models\otsuka\SystemSetting;
					$EmailSubject = $SystemSetting->where('key','=','redemption_email_subject')->first();
					$EmailfromList = $SystemSetting->where('key','=','pmm_approval_from')->first();
					$Emailfrom = $EmailfromList->value;
					$EmailToList = $SystemSetting->where('key','=','pmm_brand_' . $val)->first();
					$Emailto = explode(",",$EmailToList->value);
					$Emailcclist = $SystemSetting->where('key','=','email_cc_test')->first();
					$emailcc = explode(",",$Emailcclist->value);
					
					//pre($emailcc);

					$to = $emailcc;
					$from = $Emailfrom;
					$cc = $emailcc;
					$bcc = "webdev@magellan-solutions.com";


					//pre($emailcc);

		        	if($to && $from):

				        Mail::send('email.pmm_approval', [	'eod' => $to,
															'patient_info' => $patient_info,
															'var_total_dispatch_for_the_Month'=> $var_total_dispatch_for_the_Month,
															'var_total_free_goods_for_redemption'=> $var_total_free_goods_for_redemption,
															'var_total_qty_purchased'=> $valid_purchases_balance,
															'sku_program' => $sku_program,
															'Sku_purchased' => $Sku_purchased,

														], function ($m) use ($to,$to,$from,$cc,$bcc,$EmailSubject, $patient_info,$sku_program){					            
				            $m->from($from);
				            $m->to($to)->subject($EmailSubject->value . " " . trim($patient_info->patient_fullname,'"'));
				            $m->cc($cc);
				            $m->bcc($bcc);
				        });
					echo "test";

			        endif;	
					*/
				
			
			

		//pre($uploaded_docs);
		//pre($patient_info);
			
		//return view('otsuka.uploader.view-upload_docs',compact('supporting_docs_list'));					
		

		
	}
	
	
	
	public function sendPMM(Request $request){
		
		
		
		$Patient_Validated = new \App\Http\Models\otsuka\Patient_Validated;
		$uploads = new \App\Http\Models\otsuka\Upload;
		$Encoded_Purchases_Validated = new \App\Http\Models\otsuka\Encoded_Purchases_Validated;
		$receipt_list = $Encoded_Purchases_Validated->whereIn('id',explode(',',$request->receipt_id_list))->where('tag_deleted',0)->get();		
		//$receipt_list_array = $Encoded_Purchases_Validated->whereIn('id',explode(',',$request->receipt_id_list))->where('tag_deleted',0)->pluck('word_two')->toArray()	
		
		
		$upload_supporting_docs = new \App\Http\Models\otsuka\UploadSupportingDocs;
		$supp_docs_list = $upload_supporting_docs->whereIn('id',explode(',',$request->supp_docs_id))->where('tag_deleted',0)->get();
		//$supp_docs_list_array = $upload_supporting_docs->whereIn('id',explode(',',$request->supp_docs_id))->where('tag_deleted',0)->get();
		$patient_info = $Patient_Validated->with('fetch_mr')->find($request->patient_id);
		$uploaded_docs = $upload_supporting_docs->find($request->uploaded_id);
		$supporting_docs_list = $upload_supporting_docs::All();
		//pre($receipt_list);
		$tn = date("Y-m-d H:i:s");
		//$receipt_id = $request->receipt_id_list;
		//$supp_docs_id = $request->supp_docs_id;
		//pre($EmailSubject->value);
		//$uploaded_docs->patient_id = $request->patient_id;
		//$uploaded_docs->backend_status = "Has Been Assign To patient";
		//$uploaded_docs->frontend_status = "Has Been Assign To patient";
		//$uploaded_docs->updated_by = Auth::user()->name;
		//$uploaded_docs->save();

		//pre($request->total_dispatch_for_the_month);
		
		$var_total_dispatch_for_the_Month = $request->total_dispatch_for_the_month;
		$var_total_free_goods_for_redemption = $request->total_free_goods_for_redemption;
		$var_total_qty_purchased = $request->total_qty_purchased;
		$var_total_total_redeem = $request->total_total_redeem;
		$var_program_name_id =  $request->program_name_id;
		$Sku_purchased = $request->sku_purchases;
		$sku_model = new \App\Http\Models\otsuka\Sku;
		$sku_details = $sku_model->find($request->skuid);
		$sku_programs_model = new \App\Http\Models\otsuka\Psp_Sku_Program;	
		$programmodel = new \App\Http\Models\otsuka\Psp_Program;	
		$sku_program = $programmodel::find($var_program_name_id);

		$valid_purchases_balance = $var_total_qty_purchased - (($var_total_total_redeem / $sku_program->free) * $sku_program->buy) ;
		//pre($valid_purchases_balance);
		$Patient_Validated = new \App\Http\Models\otsuka\Patient_Validated;
			
			
			
			
		//$eod = $EodDetails::find($emailer_id);
		//pre($EodDetails);
					//$to = $patient_info->fetch_mr->fetch_psm_list->email;
		        	//$from = json_decode($eod->sender,true);
		        	//$cc= json_decode($eod->cc,true);
		        	//$bcc = ['hazel.osias@magellan-solutions.com']; 

					$val = $sku_details->brandid;
					//$val = "test";
					
					$SystemSetting = new \App\Http\Models\otsuka\SystemSetting;
					$EmailSubject = $SystemSetting->where('key','=','redemption_email_subject')->first();
					$EmailfromList = $SystemSetting->where('key','=','pmm_approval_from')->first();
					$Emailfrom = $EmailfromList->value;
					$EmailToList = $SystemSetting->where('key','=','pmm_brand_' . $val)->first();
					$pmm_approver = $EmailToList->remarks;
					$Emailto = explode(",",$EmailToList->value);
					$Emailcclist = $SystemSetting->where('key','=','email_cc')->first();
					$emailcc = explode(",",$Emailcclist->value);
					$Emailbcclist = $SystemSetting->where('key','=','email_bcc')->first();
					$emailbcc = explode(",",$Emailbcclist->value);
					
					//pre($emailcc);

					$to = $Emailto;
					$from = $Emailfrom;
					$cc = $emailcc;
					$bcc = $emailbcc;
		$Or_numbers="";
		$or_dates="";
		$or_submission_date="";
		

		/* foreach($receipt_list as $key => $val)
		{
		$Or_numbers = $Or_numbers . $val->or_number . ',';	
		$or_dates = $or_dates . $val->or_date . ',';	
		$or_submission_date = $or_submission_date . $val->submission_date . ',';	
		} */
		//$uploads->where('filename','=','');
		


		$Pmm_Approval_Report = new \App\Http\Models\otsuka\Pmm_Approval_Report;
		$Pmm_Approval_Report->pmm_name = $pmm_approver;
		$Pmm_Approval_Report->program_name = $sku_program->programname;
		$Pmm_Approval_Report->max_allocation = $sku_program->max_alloc_to_patient;
		$Pmm_Approval_Report->total_free_goods_already_dispatch = $var_total_dispatch_for_the_Month;
		$Pmm_Approval_Report->patient_code = $patient_info->patient_code;
		$Pmm_Approval_Report->patient_name = $patient_info->patient_fullname;
		$Pmm_Approval_Report->sku_purchased = $Sku_purchased;
		$Pmm_Approval_Report->or_number_list = $request->receipt_id_list;
		$Pmm_Approval_Report->support_docs_list = $request->supp_docs_id;
		$Pmm_Approval_Report->sku_purchased = $Sku_purchased;
		$Pmm_Approval_Report->total_quantity_remaining_purchased = $valid_purchases_balance;
		$Pmm_Approval_Report->remaining_free_goods_total_count = $var_total_total_redeem ;
		$Pmm_Approval_Report->created_by = Auth::user()->name;
		$Pmm_Approval_Report->updated_by = Auth::user()->name;

/* 		$Pmm_Approval_Report->or_transaction_date = $or_dates;
		$Pmm_Approval_Report->or_number = $Or_numbers;
		$Pmm_Approval_Report->receipt_submission_date = $or_submission_date; */
		//$Pmm_Approval_Report->second_follow_up_date = "";
		//$Pmm_Approval_Report->third_follow_up_date = "";
		//$Pmm_Approval_Report->date_approved = "";
		//$Pmm_Approval_Report->date_dispatched = "";


	
		$Pmm_Approval_Report->save();
					//pre($emailcc);
 
		        	if($to && $from):

				        Mail::send('email.pmm_approval', [	'eod' => $to,
															'patient_info' => $patient_info,
															'var_total_dispatch_for_the_Month'=> $var_total_dispatch_for_the_Month,
															'var_total_free_goods_for_redemption'=> $var_total_free_goods_for_redemption,
															'var_total_qty_purchased'=> $valid_purchases_balance,
															'sku_program' => $sku_program,
															'Sku_purchased' => $Sku_purchased,
															'supp_docs_list' => $supp_docs_list,
															'receipt_list' => $receipt_list

														], function ($m) use ($to,$to,$from,$cc,$bcc,$EmailSubject, $patient_info,$sku_program){					            
				            $m->from($from);
				            $m->to($to)->subject($EmailSubject->value . " " . trim($patient_info->patient_fullname,'"'));
				            $m->cc($cc);
				            $m->bcc($bcc);
				        });
					echo "test";

			        endif;	
			 
				
			
			

		//pre($uploaded_docs);
		//pre($patient_info);
			
		//return view('otsuka.uploader.view-upload_docs',compact('supporting_docs_list'));					
		

		
	}


		

}