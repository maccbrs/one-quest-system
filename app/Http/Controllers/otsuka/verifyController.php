<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;


class verifyController extends Controller
{
	public function verify(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$call_status = new \App\Http\Models\otsuka\Call_status;

		if(Auth::user()->user_level == 1)
		{
		$claim = $med_upload
					->where('id',$r->encode)
					->update(
						[
							'verify_claimed_by' => Auth::user()->id,
							'verify_claimed' => 1
						]
					);	
		}
		elseif(Auth::user()->user_level == 2)
		{
		$claim = $med_upload
					->where('id',$r->encode)
					->update(
						[
							'verify_claimed_by' => NULL,
							'verify_claimed' => 0
						]
					);	
		}
		/*pre($v);*/		

		$verify = $med_upload
					->where('id',$r->encode)
					->where('active',1)
					->with(['fetch_allocation','fetch_call'])->get();
						foreach($verify as $v){}
		
		$status = $call_status->where('status',1)->get();

		$Mgabilify = $sku->where('variant','like','%'.$v->medicine.'%')->get();
		$Mgaminoleban = $sku->where('variant','like','%'.$v->medicine.'%')->get();
		$Mgpletal = $sku->where('variant','like','%'.$v->medicine.'%')->get();

		/*pre($verify);*/
		
		return view('otsuka.dashboard.verify',compact('v','status','Mgabilify','Mgaminoleban','Mgpletal'));
	}

	public function unclaim(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$call_status = new \App\Http\Models\otsuka\Call_status;


		$unclaim = $med_upload->where('id', '=', $r->unclaim)
		->update(
			[
				'verify_claimed_by' => null,
				'verify_claimed' => 0
			]);

		if($r->v_count == 0)
			{
				return redirect('otsuka/agent/verification-queue');
			}
		else
			{
				return redirect('otsuka/agent/verification-callback-queue');
			}

	}

	public function encode_edit(Request $r){
		$users = new \App\Http\Models\gem\User;
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;

		$m1 = ltrim($r->mobile_number,'0');
		$m2 = ltrim($r->mobile_number_2,'0');
		$pn = ltrim($r->phone_number,'0');
		
		$patient_kit_mr = $patients_kit_allocation->select('id')->where('one_quest_id','=',$r->patient_kit_number)->first();

		if(!empty($r->patient_kit_number))
		{
						$validate_kit_upload = $med_upload->where('patient_kit_number',$r->patient_kit_number)
						->where('trail_status','!=',199)
						->where('type','=','Enroll')
						->where('id','!=',$r->done)
						->where('active',1)
						->where(function($query){$query->where('verification_status',0)->orWhere('verification_status',6);})
						->get();
		}
		else
		{
			$validate_kit_upload = NULL;
		}

		if(!empty($r->patient_kit_number))
		{
			$validate_kit_validated = $patient->where('patient_kit_number',$r->patient_kit_number)->get();
		}
		else
		{
			$validate_kit_validated = NULL;
		}

		if(!empty($m1) and !empty($m2) and !empty($pn))
		{
		$validate_number = $patient
		->where(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number','like','%'.$m1.'%')->orWhere('mobile_number','like','%'.$m2.'%')->orWhere('mobile_number','like','%'.$pn.'%')
			->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number_2','like','%'.$m1.'%')->orWhere('mobile_number_2','like','%'.$m2.'%')->orWhere('mobile_number_2','like','%'.$pn.'%')
				->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('phone_number','like','%'.$m1.'%')->orWhere('phone_number','like','%'.$m2.'%')->orWhere('phone_number','like','%'.$pn.'%');});});})
						->where('id','!=',$r->done)
						->get();
		}
		elseif(!empty($m1) and !empty($m2) and empty($pn))
		{
		$validate_number = $patient
		->where(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number','like','%'.$m1.'%')->orWhere('mobile_number','like','%'.$m2.'%')
			->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number_2','like','%'.$m1.'%')->orWhere('mobile_number_2','like','%'.$m2.'%')
				->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('phone_number','like','%'.$m1.'%')->orWhere('phone_number','like','%'.$m2.'%');});});})
					->where('id','!=',$r->done)
					->get();
		}
		elseif(!empty($m1) and empty($m2) and empty($pn))
		{
		$validate_number = $patient
		->where(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number','like','%'.$m1.'%')->orWhere('mobile_number_2','like','%'.$m1.'%')->orWhere('phone_number','like','%'.$m1.'%');})
			->where('id','!=',$r->done)
			->get();
		}
		elseif(empty($m1) and empty($m2) and !empty($pn)) 
		{
		$validate_number = $patient
		->where(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number','like','%'.$pn.'%')->orWhere('mobile_number','like','%'.$pn.'%')
			->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number_2','like','%'.$pn.'%')->orWhere('mobile_number_2','like','%'.$pn.'%')
				->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('phone_number','like','%'.$pn.'%')->orWhere('phone_number','like','%'.$pn.'%');});});})
					->where('id','!=',$r->done)
					->get();
		}
		elseif(empty($m1) and !empty($m2) and !empty($pn))
		{
		$validate_number = $patient
		->where(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number','like','%'.$m2.'%')->orWhere('mobile_number','like','%'.$pn.'%')
			->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('mobile_number_2','like','%'.$m2.'%')->orWhere('mobile_number_2','like','%'.$pn.'%')
				->orWhere(function($query) use ($r, $m1, $m2, $pn) {$query->where('phone_number','like','%'.$m2.'%')->orWhere('phone_number','like','%'.$pn.'%');});});})
					->where('id','!=',$r->done)
					->get();
		}
		else
		{
			$validate_number = NULL;
		}
		//pre(count($validate_kit_upload));

		if(count($validate_kit_upload) == 0 or count($validate_kit_validated) == 0 or count($validate_number) == 0)
		{
			if($r->invalid == 'true')
			{
				if($r->remarks == 'No Px Consent')
				{
				$med_upload->where('id','=',$r->id)
				->update(
					[
						'viber_queue'=>'1',
						'remarks'=>$r->remarks,
						'status'=>'Reject',
						'submit'=>'0',
						'trail_status'=> 199,
						'patient_kit_number' => preg_replace('/\s+/', '', $r->patient_kit_number),
						'patient_name' => $r->patient_fullname,
						'mobile_number' => $r->mobile_number,
						'mobile_number_2' => $r->mobile_number_2,
						'phone_no' => $r->phone_number,
						'time_to_call' => $r->time_to_call,
						'patient_consent' => NULL,
						'enrollment_date' => !empty($r->enrollment_date) ? date("Y-m-d", strtotime($r->enrollment_date)) : null,
						'doctor_name' => $r->doctor_name,
						'doctor_consent' => $r->doctor_consent,
						'hospital_name' => $r->hospital_name
					]);
				}
				else
				{
				$med_upload->where('id','=',$r->id)
				->update(
					[
						'viber_queue'=>'1',
						'remarks'=>$r->remarks,
						'status'=>'Reject',
						'submit'=>'0',
						'trail_status'=> 199,
						'patient_kit_number' => preg_replace('/\s+/', '', $r->patient_kit_number),
						'patient_name' => $r->patient_fullname,
						'mobile_number' => $r->mobile_number,
						'mobile_number_2' => $r->mobile_number_2,
						'phone_no' => $r->phone_number,
						'time_to_call' => $r->time_to_call,
						'patient_consent' => $r->patient_consent,
						'enrollment_date' => !empty($r->enrollment_date) ? date("Y-m-d", strtotime($r->enrollment_date)) : null,
						'doctor_name' => $r->doctor_name,
						'doctor_consent' => $r->doctor_consent,
						'hospital_name' => $r->hospital_name
					]);
				}
				return redirect('otsuka/agent/verification-queue'); 
			}

			elseif($r->invalid != 'true')
				{
					if($validate_kit_upload->count() == 0 and $validate_kit_validated->count() == 0 and $validate_number->count() == 0)
					{
						$cmid->calltype = 1;
						$cmid->purpose = 1;
						$cmid->employeeid = Auth::user()->id;
						$cmid->save();

						$call_verification->cmid = $cmid->id;
						$call_verification->attempt = 1;
						$call_verification->date = date('m/d/Y');
						$call_verification->save();

						$submit = $med_upload->where('id','=',$r->done)
						->update(
							[
								'patient_kit_number' => preg_replace('/\s+/', '', $r->patient_kit_number),
								'patient_name' => $r->patient_fullname,
								'mobile_number' => $r->mobile_number,
								'mobile_number_2' => $r->mobile_number_2,
								'phone_no' => $r->phone_number,
								'time_to_call' => $r->time_to_call,
								'patient_consent' => $r->patient_consent,
								'enrollment_date' => !empty($r->enrollment_date) ? date("Y-m-d", strtotime($r->enrollment_date)) : null,
								'doctor_name' => $r->doctor_name,
								'doctor_consent' => $r->doctor_consent,
								'hospital_name' => $r->hospital_name,
								'cmid' => $cmid->id,
								'trail_status' => 200,
								'submit' => 1
							]
						);

						if(!empty($r->mobile_number))
						{

						$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
						$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

						$msg = $txttemplates->where('id',2)->first();
			            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

						$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$r->mobile_number."&message=".$msg->message;
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_URL,$url);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
							$json = curl_exec($ch);
							if(!$json) {
							    echo curl_error($ch);
							}
							curl_close($ch);
						}

					return back()->with('success',['Your modifications has been saved!']);	 	
					}
					else
					{
					return redirect()->back()->withInput()->with(['validate_kit_upload' => $validate_kit_upload,'validate_kit_validated' => $validate_kit_validated, 'validate_number' => $validate_number]);	
					}

				}
				
			}
			else
			{
				return redirect()->back()->withInput()->with(['validate_kit_upload' => $validate_kit_upload,'validate_kit_validated' => $validate_kit_validated, 'validate_number' => $validate_number]);
			}
		}

	public function encode_proceed(Request $r){
		$users = new \App\Http\Models\gem\User;
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;

						$cmid->calltype = 1;
						$cmid->purpose = 1;
						$cmid->employeeid = Auth::user()->id;
						$cmid->save();

						$call_verification->cmid = $cmid->id;
						$call_verification->attempt = 1;
						$call_verification->date = date('m/d/Y');
						$call_verification->save();

						$submit = $med_upload->where('id','=',$r->id)
						->update(
							[
								'patient_kit_number' => preg_replace('/\s+/', '', $r->patient_kit_number),
								'patient_name' => $r->patient_fullname,
								'mobile_number' => $r->mobile_number,
								'mobile_number_2' => $r->mobile_number_2,
								'phone_no' => $r->phone_number,
								'time_to_call' => $r->time_to_call,
								'patient_consent' => $r->patient_consent,
								'enrollment_date' => !empty($r->enrollment_date) ? date("Y-m-d", strtotime($r->enrollment_date)) : null,
								'doctor_name' => $r->doctor_name,
								'doctor_id' => $r->doctor_id,
								'doctor_consent' => $r->doctor_consent,
								'hospital_name' => $r->hospital_name,
								'cmid' => $cmid->id,
								'trail_status' => 200,
								'submit' => 1
							]
						);

						if(!empty($r->mobile_number))
						{

						$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
						$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

						$msg = $txttemplates->where('id',2)->first();
			            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

						$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$r->mobile_number."&message=".$msg->message;
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_URL,$url);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
							$json = curl_exec($ch);
							if(!$json) {
							    echo curl_error($ch);
							}
							curl_close($ch);
						}

					return back()->with('success',['Your modifications has been saved!']);	
	}


	public function encode_verify(Request $r){

		if(Auth::user()->user_level == 1)
		{

		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;

		$px_last = $patient->orderBy('id','desc')->first();
		$px_number = $px_last->id;
		$px_code = 'OQPXN'.sprintf('%07d',$px_number++);

		$c_ver = $call_verification->where('id',$r->v_id)->orderBy('created_at','desc')->first();

				if($r->remarks_call == '6' or $r->remarks_call == '9')
					{

						if($c_ver->attempt != 5)
						{	

						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'trail_status' => 211,
								'verify_claimed' => 0,
								'verify_submit' => 0,
								'verify_claimed_by' => NULL,
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others,
								'callback_date' => date('Y-m-d', strtotime(' +1 day'))
							]);

						$allowcallback = -1;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes,
							'employeeid' => Auth::user()->id
						]);

						$call_verification->cmid = $r->cmid;
						$call_verification->attempt = (int)$r->v_attempt + 1;
						$call_verification->date = date('m/d/Y', strtotime(' +1 day'));
						$call_verification->save();


						$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
						$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

						$msg = $txttemplates->where('id',3)->first();
			            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

						$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$r->mobile_number."&message=".$msg->message;
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_URL,$url);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
							$json = curl_exec($ch);
							if(!$json) {
							    echo curl_error($ch);
							}
							curl_close($ch);

						}
						else
						{

						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others,
								'trail_status' => 239,
								'verify_submit' => 1
							]);

						$allowcallback = 2;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes,
							'employeeid' => Auth::user()->id
						]);	

						}

					}

				elseif($r->remarks_call == '8')
					{
						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'trail_status' => 211,
								'verify_claimed' => 0,
								'verify_submit' => 0,
								'verify_claimed_by' => NULL,
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others,
								'callback_date' => date('Y-m-d', strtotime(' +1 day'))
							]);

						$allowcallback = -1;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes,
							'employeeid' => Auth::user()->id
						]);

						$call_verification->cmid = $r->cmid;
						$call_verification->attempt = (int)$r->v_attempt + 1;
						$call_verification->date = date('m/d/Y', strtotime(' +1 day'));
						$call_verification->save();
					}

				elseif($r->remarks_call == '10')
					{
						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'trail_status' => 229,
								'verify_claimed' => 1,
								'verify_submit' => 1,
								'verify_claimed_by' => Auth::user()->id,
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others
							]);

						$allowcallback = 2;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes,
							'employeeid' => Auth::user()->id
						]);

					}

				elseif($r->remarks_call == '1')
					{

						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'status' => 'verified',
								'trail_status' => 400,
								'verify_submit' => 1,
								'verification_attempt' => $r->v_attempt,
								'verification_status' => 1,
								'verification_remarks' => $r->remarks_others
							]);

						$allowcallback = 1;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes,
							'employeeid' => Auth::user()->id
						]);

					}

		/*		elseif($r->remarks_call == '11')
				{
						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'verify_submit' => 1,
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others
							]);
				}*/

				else
					{
						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others,
								'trail_status' => 239,
								'verify_submit' => 1
							]);


						$allowcallback = 2;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes,
							'employeeid' => Auth::user()->id
						]);
					}

		}
		elseif(Auth::user()->user_level == 2)
		{

		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;

		$px_last = $patient->orderBy('id','desc')->first();
		$px_number = $px_last->id;
		$px_code = 'OQPXN'.sprintf('%07d',$px_number++);

		$c_ver = $call_verification->where('id',$r->v_id)->orderBy('created_at','desc')->first();

				if($r->remarks_call == '6' or $r->remarks_call == '9')
					{
						if($c_ver->attempt != 5)
						{	

						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'trail_status' => 211,
								'verify_claimed' => 0,
								'verify_submit' => 0,
								'verify_claimed_by' => NULL,
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others,
								'callback_date' => date('Y-m-d', strtotime(' +1 day'))
							]);

						$allowcallback = -1;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes,
							'employeeid' => Auth::user()->id
						]);

						$call_verification->cmid = $r->cmid;
						$call_verification->attempt = (int)$r->v_attempt + 1;
						$call_verification->date = date('m/d/Y', strtotime(' +1 day'));
						$call_verification->save();


						$txtconnect = new \App\Http\Models\otsuka\Txtconnect;
						$txttemplates = new \App\Http\Models\otsuka\Txt_templates;

						$msg = $txttemplates->where('id',3)->first();
			            $token = $txtconnect->select('token')->orderBy('created_at','desc')->first();

						$url = "https://lc-txtconnect5.globe.com.ph/sms?token=".$token->token."&txtid=onequest&recipients=".$r->mobile_number."&message=".$msg->message;
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_URL,$url);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
							$json = curl_exec($ch);
							if(!$json) {
							    echo curl_error($ch);
							}
							curl_close($ch);

						}
						else
						{

						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others,
								'trail_status' => 239,
								'verify_submit' => 1
							]);

						$allowcallback = 2;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes,
							'employeeid' => Auth::user()->id
						]);	

						}

					}

				elseif($r->remarks_call == '8')
					{
						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'trail_status' => 211,
								'verify_claimed' => 0,
								'verify_submit' => 0,
								'verify_claimed_by' => NULL,
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others,
								'callback_date' => date('Y-m-d', strtotime(' +1 day'))
							]);

						$allowcallback = -1;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes
						]);

						$call_verification->cmid = $r->cmid;
						$call_verification->attempt = (int)$r->v_attempt + 1;
						$call_verification->date = date('m/d/Y', strtotime(' +1 day'));
						$call_verification->save();
					}

				elseif($r->remarks_call == '10')
					{
						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'trail_status' => 229,
								'verify_claimed' => 1,
								'verify_submit' => 1,
								'verify_claimed_by' => Auth::user()->id,
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others
							]);

						$allowcallback = 2;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes
						]);

					}

				elseif($r->remarks_call == '1')
					{

						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'status' => 'verified',
								'trail_status' => 400,
								'verify_submit' => 1,
								'verification_attempt' => $r->v_attempt,
								'verification_status' => 1,
								'verification_remarks' => $r->remarks_others
							]);

						$allowcallback = 1;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes
						]);

					}

		/*		elseif($r->remarks_call == '11')
				{
						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'verify_submit' => 1,
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others
							]);
				}*/

				else
					{
						$med_upload
						->where('id',$r->upload_id)
						->update(
							[
								'verification_attempt' => $r->v_attempt,
								'verification_status' => $r->remarks_call,
								'verification_remarks' => $r->remarks_others,
								'trail_status' => 239,
								'verify_submit' => 1
							]);


						$allowcallback = 2;
						$call_verification
						->where('id',$r->v_id)
						->update(
						[
							'actualdate' => date('Y-m-d'),
							'allowcallback' => $allowcallback,
							'remarks' => $r->remarks_call,
							'remarks_others' => $r->remarks_others,
							'callnotes' => $r->callnotes
						]);
					}

		}



		/*pre($r->callnotes);*/
		if($r->v_attempt == 1)
			{
				return redirect('otsuka/agent/verification-queue');
			}
		else
			{
				return redirect('otsuka/agent/verification-callback-queue');
			}
	}

	public function encode_details(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;

		$px_last = $patient->orderBy('id','desc')->first();
		$px_number = $px_last->id;
		$px_code = 'OQPXN'.sprintf('%07d',$px_number++);

		$status = $call_status->where('status',1)->get();	


		/*$verify['details'] = (object)$r->all();*/

		$patient->patient_code = $px_code;
		$patient->patient_fullname = $r->patient_lastname.', '.$r->patient_firstname.' '.$r->patient_middlename;
		$patient->patient_lastname = $r->patient_lastname;
		$patient->patient_firstname = $r->patient_firstname;
		$patient->patient_middlename = $r->patient_middlename;
		$patient->birth_date = date('m/d/Y',strtotime($r->birth_date));
		$patient->gender = $r->gender;
		$patient->age = $r->age;
		$patient->address = $r->address;
		$patient->mobile_number = $r->mobile_number;
		$patient->mobile_number_2 = $r->mobile_number_2;
		$patient->phone_number = $r->phone_number;
		$patient->remarks = $r->remarks;
		$patient->medrep = $r->p_name;
		$patient->doctor_name = $r->doctor;
		$patient->doctor_wont_disclose = $r->doctor_wont_disclose;
		$patient->hospital = $r->hospital;
		$patient->patient_kit_number = preg_replace('/\s+/', '', $r->patient_kit_number);
		$patient->enrollment_date = date('m/d/Y');
		$patient->doctor_consent = $r->doctor_consent;
		$patient->patient_consent = $r->patient_consent;
		$patient->nickname = $patient->patient_fullname;
		$patient->date_encoded = date('m/d/Y');
		
		
		//Tagging MR, MD
		
		$patient->patient_type = 'Retrieval';
		$patient_kit_info = $patients_kit_allocation->where('one_quest_id','=',$r->patient_kit_number)->first();
		
		if(!empty($patient_kit_info))
			{
				$doctor_data = $national_md_list->where('areacode','=',$patient_kit_info->area_code)->where('doctorid','=', $r->d_id)->first();
				$patient->patient_mr_id = $patient_kit_info->emp_code;
				$patient->patient_mr_name = $patient_kit_info->mr_name;
				$patient->patient_tagging_team = $patient_kit_info->team;
				$patient->patient_tagging_area_code = $patient_kit_info->area_code;
				
				if(!empty($doctor_data))
				{
				$patient->patient_tagging_emp_doctorid = $doctor_data->employee;
				$patient->patient_tagging_doctorid =  $doctor_data->doctorid;
				$patient->patient_tagging_doctor_name = $doctor_data->mdname;
				$patient->patient_tagging_md_class = $doctor_data->mdclass;
				$patient->patient_tagging_specialty = $doctor_data->specialty;
				$patient->patient_tagging_hospital = $doctor_data->hospital;
				}
				
				
				$patient->patient_enrollment_date =  date('m/d/Y');
				
				
				
			}
		else{
			$patient->patient_tagging_doctor_name =  $r->doctor;
			$patient->patient_tagging_hospital = $r->hospital;
		}	
		$patient->patient_tagging_doctorid = $r->d_id;
		$patient->patient_tagging_product_prescribe = $r->enroll_product;
		
		// End OF Tagging
		
		
		$patient->save();

		$patient_med_info->patient_id = $patient->id;
		$patient_med_info->patient_code = $patient->patient_code;
		$patient_med_info->doctors_id = $r->d_id;
		$patient_med_info->medrep_id = $r->medrep_id;
		$patient_med_info->brand = $r->enroll_product;
		$patient_med_info->sku = $r->product;
		$patient_med_info->no_of_tabs_prescribe = $r->notabsprescribe;
		$patient_med_info->tabs_prescribe_remarks = $r->tabsprescribe;
		$patient_med_info->no_of_days_prescribe = $r->nodaysprescribe;
		$patient_med_info->days_prescribe_remarks = $r->daysprescribe;
		$patient_med_info->total_no_of_tabs = $r->totalnotabs;
		$patient_med_info->total_tabs_remarks = $r->totaltabs;
		$patient_med_info->purchased = $r->purchased;
		$patient_med_info->save();

		$med_upload
				->where('id',$r->encode)
				->update(
					[
						'patientid' => $patient->id,
						'patient_kit_number' => preg_replace('/\s+/', '', $r->patient_kit_number),
					]);

		/*if($r->dnc == 'yes')
		{
				$allowcallback = 2;

				$call_verification
				->where('id',$r->v_id)
				->update(
				[
					'actualdate' => date('Y-m-d'),
					'allowcallback' => $allowcallback,
					'remarks' => 10,
					'employeeid' => Auth::user()->id
				]);

				$med_upload
				->where('id',$r->encode)
				->update(
					[
						'verify_claimed' => 1,
						'verify_submit' => 1,
						'verification_status' => 10
					]);
		}
		elseif($r->dnc == '')
		{
			$allowcallback = 1;

			$call_verification
				->where('id',$r->v_id)
				->update(
				[
					'actualdate' => date('Y-m-d'),
					'allowcallback' => $allowcallback,
					'remarks' => 1,
					'employeeid' => Auth::user()->id
				]);

				$med_upload
				->where('id',$r->encode)
				->update(
					[
						'verify_claimed' => 1,
						'verify_submit' => 1,
						'verification_status' => 1
					]);

				$cmid->calltype = 2;
				$cmid->purpose = 2;
				$cmid->patientid = $patient->id;
				$cmid->cardnumberid = $patient->patient_kit_number;
				$cmid->save();
		}*/

				

				$cmid
				->where('id',$r->cmid)
				->update(
					[
						'patientid' => $patient->patient_code
					]);

				$cmid->calltype = 2;
				$cmid->purpose = 2;
				$cmid->patientid = $patient->id;
				$cmid->cardnumberid = $patient->patient_kit_number;
				$cmid->save();

				$call_verification->cmid = $cmid->id;
				$call_verification->attempt = 1;
				$call_verification->date = date('m/d/Y');
				$call_verification->allowcallback = 1;
				$call_verification->save();


		$verify_call = $med_upload
					->where('id',$r->encode)
					->where('active',1)
					->with(['fetch_allocation','fetch_call','fetch_status'])->get();
						foreach($verify_call as $vc){}
			

		/*pre($verify_call);*/
		return view('otsuka.dashboard.verify-details', compact('verify','vc','status'));
	}

}