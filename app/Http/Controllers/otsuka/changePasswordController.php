<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Models\gem\User;
use App\Http\Models\otsuka\SystemSetting;
class changePasswordController extends Controller
{
    
	public function index(){

		$data = User::orderBy('name', 'asc')->get();

		return view('otsuka.settings.users', compact('data'));
	}   

	public function addUser(Request $request){
		//dd($request->all());
		$settings = SystemSetting::where('key', '=', 'default_password')->first();
	
		$user = new User;
		$user->username = $request->empcode;
		$user->emp_code = 0;
		$user->name = $request->lastname.', '.$request->firstname.' '.$request->middlename;
		$user->email = $request->empcode;
		$user->department_id = 0;
		$user->password = bcrypt($settings->value);
		$user->options = '["otsuka"]';
		$user->user_level = 1;
		$user->user_type = 'medrep';
		$user->access = '["otsuka"]';
		$user->avatar = 'default.jpg';
		$user->routes = '["otsuka.medrep"]';
		$user->status = 1;
		$user->active = 1;
		$user->is_representative = 1;
		$user->created_by = Auth::user()->name;
		$user->save();

		return redirect()->back()->with('add', 'Success');
		// return view('otsuka.settings.users');
	}

	public function resetpassword(Request $request){
		
		$settings = SystemSetting::where('key', '=', 'default_password')->first();
	
		$name = User::where('id', $request->hidden_id)->first();

		$data = User::where('id', $request->hidden_id)
		->update(['password' => bcrypt($settings->value),
				  'updated_by' => Auth::user()->name
	]);
		
		return redirect()->back()->with('success', $name->name.' password has been reset successfully.');
	}

}