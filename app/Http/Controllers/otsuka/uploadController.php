<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;
use App\Http\Models\gem\User;

class uploadController extends Controller
{

	public function index(){
		$now = date('Y-m-d');
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$myUpload = $med_upload->where('added_by', Auth::user()->id)->where('created_at', 'like', '%' . $now  .'%')
		->where('active', 1)
		->orderby('created_at', 'desc')->get();
		// $myUpload = $med_upload->where('added_by','=', Auth::user()->id)->where('created_at', 'like', '%' . $now  .'%')->orderby('created_at', 'desc')->get();
		//dd($myUpload);
		return view('otsuka.dashboard.upload', compact('myUpload'));
	}

	public function postUpload (Request $request){
		$med_upload = new \App\Http\Models\otsuka\Upload;

		$date = $request->filter_date;
		$date = date('Y-m-d', strtotime($date));

		$myUpload = $med_upload->where('added_by', Auth::user()->id)->where('created_at', 'like', '%' . $date .'%')->orderby('created_at', 'desc')->where('active', 1)->get();

		return view('otsuka.dashboard.upload', compact('myUpload'));
	}

	public function uploadByMedRep(){

		$users = User::orderby('name')->get();
		return view('otsuka.settings.uploadByMedRep', compact('users'));
	}

	public function postuploadByMedRep(Request $request){
		$users = User::orderby('name')->get();
		$med_upload = new \App\Http\Models\otsuka\Upload;

		$from = $request->filter_from;
		$from = date('Y-m-d', strtotime($from));
		$to = $request->filter_to;
		$to = date('Y-m-d', strtotime($to));

		$user = $request->filter_name;

		if($user != '' && $request->filter_from == '' && $request->filter_to == ''){
			$myUpload = $med_upload->where('uploaded_by', $user)
					->where('active', 1)
					->orderby('created_at', 'desc')->get();
		}
		elseif($user != '' && $request->filter_from != '' && $request->filter_to != ''){
			$myUpload = $med_upload->where('uploaded_by', $user)
					->whereBetween('created_at', array($from, $to))
					->where('active', 1)
					->orderby('created_at', 'desc')->get();
		}

		return view('otsuka.settings.uploadByMedRep', compact('users','myUpload'));
	}

}