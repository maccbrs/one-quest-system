<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;
use Carbon\Carbon;


class apiController extends Controller
{

	public function index(){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient;
		$Queue = $med_upload->where('claimed',0)->where('submit',0)->orWhere('claimed_by','=',Auth::user()->id)->orderBy('created_at','asc')->get();
		$tn = date("Y-m-d H:i:s");


		return view('otsuka.dashboard.queue',compact('Queue','Upload','Mr','Px','tn'));
	}
	
	public function GetUpload(Request $request){
		$patients = new \App\Http\Models\otsuka\Upload;
		$records = $patients::find($request->upload_id);	
		echo json_encode($records);
		
		
	}
	
	
	public function compliance_list(Request $request){
		$patients = new \App\Http\Models\otsuka\Upload;
		$records = $patients::find($request->upload_id);	
		echo json_encode($records);
		
		
	}
	
	/*
	API REFERENCE 
	
	action = get , post
	modelname = Modelname 
	limit = limit of the records
	RefVal = reference value
	RefCol = Column in the Table
	Condition = equal ,notequal, lessthan, greaterthan,
	
	
	
	*/
	
	
	public function GetRecords(Request $request){
		//$patients = new \App\Http\Models\otsuka\Upload;
		$fetch_records = "";
		
		
		//$records = $patients::find($request->upload_id);	
		
		if(!empty($request->modelname))
		{
		$table_model = array("\App\Http\Models\otsuka" . chr(92) .$request->modelname);		
		
		
		
			$table_columns = new $table_model[0];
			
			$columns = $table_columns['fillable'];
			
			
			
			
			if((!empty($request->refval)) && (!empty($request->refcol)))
			{
				

				$fetch_records = $table_columns->where($request->refcol,'=',$request->refval)->get(); 
				
			}
			else
			{
				//pre($table_columns);
			}
			
		
		
		
		//pre($table_columns);
		
		
		}	
		
		
		//echo json_encode($records);
		
				
					//pre($fetch_records);
			return view('otsuka.api.index',compact('fetch_records','columns'));
		
		
		
		
		//return view('otsuka.dashboard.queue',compact('Queue','Upload','Mr','Px','tn'));
		
	}
	



}