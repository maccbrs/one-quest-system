<?php namespace App\Http\Controllers\otsuka;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\otsuka\Receipt;
use AppHelper;
use Mail;


class reportsController extends Controller
{

	public function index(){
		$reports_list = new \App\Http\Models\otsuka\Reports_list;

                    $mtd_start = '2018-02-01 00:00:00';
                    $now = date('Y-m-d').' 17:00:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

		$lists = $reports_list->where('active',1)->get();


		return view('otsuka.dashboard.generate',compact('lists','mtd_start','now','yesterday','tomorrow'));
	}

	public function index_eom(Request $request){
		$months = new \App\Http\Models\otsuka\Eom_months;
		$reports = new \App\Http\Models\otsuka\Eom_reports;

		$list_months = $months->get();
		$list_reports = $reports->get();


		return view('otsuka.dashboard.generate_eom', compact('list_months','list_reports'));
	}

	public function generate(Request $request){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance_reason = new \App\Http\Models\otsuka\Compliance_reason;
		$reports_list = new \App\Http\Models\otsuka\Reports_list;

            		$dt = date('Y-m-d');
            		//$dt = date('Y-m-d');
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-05-01 00:00:00';
                    $now = date('Y-m-d').' 17:00:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));
                    //$excluded = array('Referral','Voluntary');
					
		// URL PARAMETER From And TO			
		if(!empty($request->from))
		{
			$from = $request->from;
			$first_day  = $from;
		}
		else
		{
		
		}
		if(!empty($request->to))
		{
			$to = $request->to;
			$now = $to ;
		}
		else{
			//$to =  $now;
		}	
					
					
					


/*		$data = $med_upload->where('active',1)->where('type','=','Enroll')->where('trail_status','!=','100')->whereNull('patient_type')->where(function($query){$query->where('verification_status','!=','0')->orWhere(function($query){$query->where('verification_status',0)->whereNotNull('remarks')->orWhere('patient_consent','=','');});})->where(function($query){$query->where('remarks','=','No Px Consent')->orWhereNull('remarks');})->whereBetween('created_at',['2018-03-01 00:00:00', '2018-03-31 17:00:00'])->with(['fetch_user','fetch_validated','fetch_mdinfo','fetch_status','fetch_call','fetch_allocation','fetch_allocation_april','fetch_medinfo'])->get();



		//pre($data);
		return view('otsuka.dashboard.generated-report',compact('data','mtd_start','now','yesterday','tomorrow'));*/
		$data = $med_upload
		->where('active',1)
		->where('type','=','Enroll')
		->where('trail_status','!=','100')
		->where(function($query){$query->where('verification_status','!=','0')->orWhere(function($query){$query->where('verification_status',0)->whereNotNull('remarks')->orWhere('patient_consent','=','');});})->where(function($query){$query->where('remarks','=','No Px Consent')->orWhereNull('remarks');})
		->whereBetween('created_at',[$first_day, $now])
		->with(['fetch_user','fetch_validated','fetch_mdinfo','fetch_status','fetch_call','fetch_allocation','fetch_allocation_april','fetch_medinfo','fetch_mdinfO_upload'])->get();

		$data2 = $patient->whereBetween('patients_validated.created_at',[$first_day, $now])
		->join('patients_med_info', 'patients_med_info.patient_id', '=', 'patients_validated.id')
		//->where(function($query){$query->where('patient_type','=','referral')->orWhere(function($query){$query->where('patient_type','=','referral'));})
		->where(function($query){$query->where('patients_validated.patient_type','=','referral')->orWhere('patients_validated.patient_type','=','Voluntary');})
		->with(['fetch_sku_vol_ref','fetch_receipt_encoded'])
		->get();
		//pre($first_day);

		return view('otsuka.dashboard.generated-report',compact('data','mtd_start','now','yesterday','tomorrow','data2'));
	}

	public function generate_eom(Request $request){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance_reason = new \App\Http\Models\otsuka\Compliance_reason;
		$reports_list = new \App\Http\Models\otsuka\Reports_list;
		$medrep = new \App\Http\Models\otsuka\Medrep;

            		$dt = date('Y-m-d');
            		//$dt = date('Y-m-d');
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-02-01 00:00:00';
                    $now = date('Y-m-d').' 17:00:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));
                    //$excluded = array('Referral','Voluntary');
					
		// URL PARAMETER From And TO			
		
					


/*		$data = $med_upload->where('active',1)->where('type','=','Enroll')->where('trail_status','!=','100')->whereNull('patient_type')->where(function($query){$query->where('verification_status','!=','0')->orWhere(function($query){$query->where('verification_status',0)->whereNotNull('remarks')->orWhere('patient_consent','=','');});})->where(function($query){$query->where('remarks','=','No Px Consent')->orWhereNull('remarks');})->whereBetween('created_at',['2018-03-01 00:00:00', '2018-03-31 17:00:00'])->with(['fetch_user','fetch_validated','fetch_mdinfo','fetch_status','fetch_call','fetch_allocation','fetch_allocation_april','fetch_medinfo'])->get();



		//pre($data);
		return view('otsuka.dashboard.generated-report',compact('data','mtd_start','now','yesterday','tomorrow'));*/

if($request->report == '1')
{
	ini_set('memory_limit', '1024M');

		$data = $med_upload->where('active',1)->where('type','=','Enroll')->where('trail_status','!=','100')->where(function($query){$query->where('verification_status','!=','0')->orWhere(function($query){$query->where('verification_status',0)->whereNotNull('remarks')->orWhere('patient_consent','=','');});})->where(function($query){$query->where('remarks','=','No Px Consent')->orWhereNull('remarks');})->whereMonth('created_at','=',$request->month)->with(['fetch_user','fetch_validated','fetch_mdinfo','fetch_status','fetch_call','fetch_allocation','fetch_allocation_april','fetch_medinfo','fetch_mdinfO_upload','user','user_medrep'])->get();

		$data2 = $patient->whereMonth('patients_validated.created_at','=',$request->month)
		->join('patients_med_info', 'patients_med_info.patient_id', '=', 'patients_validated.id')
		//->where(function($query){$query->where('patient_type','=','referral')->orWhere(function($query){$query->where('patient_type','=','referral'));})
		->where(function($query){$query->where('patients_validated.patient_type','=','referral')->orWhere('patients_validated.patient_type','=','Voluntary');})
		->with(['fetch_sku_vol_ref','fetch_receipt_encoded'])
		->get();

		//pre($data2);

		return view('otsuka.dashboard.generated-report-eom',compact('data','mtd_start','now','yesterday','tomorrow','data2'));
}
elseif($request->report == '2')
{
	$month = $request->month;

	//dd($request);


	$list = $medrep
	->with(
		['connect_user' => function ($q) use ($month) 
			{$q->with(
				[
					'fetch_referral_abilify' => function ($q) use ($month) 
					{$q->whereMonth('patients_validated.created_at', '=',$month);},
					'fetch_referral_aminoleban' => function ($q) use ($month) 
					{$q->whereMonth('patients_validated.created_at', '=',$month);},
					'fetch_referral_pletaal' => function ($q) use ($month) 
					{$q->whereMonth('patients_validated.created_at', '=',$month);},
					'fetch_total_retrieval' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_new' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_finished' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_stopped' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_switched' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_total_verified' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_declined' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_wrong_number' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_noanswer_5' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_noanswer_4' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_noconsent' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_queue' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_abilify_total_unverified_retrieval' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_total_retrieval' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},

					'fetch_aminoleban_new' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_finished' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_stopped' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_switched' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_total_verified' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_declined' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_wrong_number' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_noanswer_5' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_noanswer_4' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_noconsent' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_queue' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_aminoleban_total_unverified_retrieval' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},

					'fetch_pletaal_new' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_finished' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_stopped' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_switched' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_total_verified' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_declined' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_wrong_number' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_noanswer_5' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_noanswer_4' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_noconsent' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_queue' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_pletaal_total_unverified_retrieval' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},

					'fetch_invalid_blurred' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_invalid_duplicate' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_invalid_already_enrolled' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_invalid_wrong_upload' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},
					'fetch_invalid_total' => function ($q) use ($month) 
					{$q->whereMonth('created_at', '=',$month);},

				]);
			},

	'fetch_voluntary_abilify' => function ($q) use ($month) {
    $q->whereMonth('created_at', '=',$month);},'fetch_voluntary_aminoleban' => function ($q) use ($month) {
    $q->whereMonth('created_at', '=',$month);},'fetch_voluntary_pletaal' => function ($q) use ($month) {
    $q->whereMonth('created_at', '=',$month);}])->get();

		//pre($list);
		return view('otsuka.dashboard.generated-summary-eom',compact('mtd_start','now','yesterday','tomorrow','list'));
}

elseif($request->report == '3')
	{
		$patient_validated = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance = new \App\Http\Models\otsuka\Compliance;
		$Upload = new \App\Http\Models\otsuka\Upload;

ini_set('memory_limit','-1');


            		$dt = date('Y-m-d');
            		//$first_day = '2018-02-01 00:00:00';
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-05-01 00:00:00';
                    $mtd_last = '2018-05-31 23:00:00';
                    $now = date('Y-m-d').' 20:30:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 20:30:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));


		$data = $compliance->whereMonth('created_at','=',$request->month)->with(['fetch_patient','fetch_callstatus','fetch_old','fetch_patients_medinfo','fetch_compliance_reason','fetch_month'])->get();


		return view('otsuka.dashboard.compliance-call-report',compact('data','mtd_start','now','yesterday','tomorrow'));
	}

elseif($request->report == '4')
	{
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$cmid = new \App\Http\Models\otsuka\Cmid;



		$cmid_data = $call_verification->select('cmid')->whereMonth('updated_at','=',$request->month)->where('tag_deleted',0)->orderBy('cmid','asc')->get()->toArray();
		$data = $cmid->whereIn('id',$cmid_data)->where('active',1)->with(['fetch_verification','fetch_upload','fetch_patient','fetch_redemption'])->get();

		return view('otsuka.dashboard.generated-report-daily-call',compact('data','mtd_start','now','yesterday','tomorrow'));
	}

elseif($request->report == '5')
	{	
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;

		$data = $med_upload->where('active',1)->where('type','=','Enroll')->where('trail_status',199)->where('active',1)->whereMonth('created_at','=',$request->month)->with(['user_medrep'])->get();

		return view('otsuka.dashboard.generated-invalid',compact('data','mtd_start','now','yesterday','tomorrow'));
	}

	}

	public function generate_summary(){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$medrep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance_reason = new \App\Http\Models\otsuka\Compliance_reason;
		$reports_list = new \App\Http\Models\otsuka\Reports_list;

            		$dt = date('Y-m-d');
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-02-01 00:00:00';
                    $now = date('Y-m-d').' 17:00:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));
                    //$excluded = array('Referral','Voluntary');

        $list = $medrep
		->with(
		['connect_user' => function ($q) use ($first_day, $now) 
			{$q->with(
				[
					'fetch_referral_abilify' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('patients_validated.created_at',[$first_day, $now]);},
					'fetch_referral_aminoleban' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('patients_validated.created_at',[$first_day, $now]);},
					'fetch_referral_pletaal' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('patients_validated.created_at',[$first_day, $now]);},
					'fetch_total_retrieval' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_new' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_finished' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_stopped' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_switched' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_total_verified' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_declined' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_wrong_number' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_noanswer_5' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_noanswer_4' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_noconsent' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_queue' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_abilify_total_unverified_retrieval' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_total_retrieval' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},

					'fetch_aminoleban_new' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_finished' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_stopped' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_switched' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_total_verified' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_declined' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_wrong_number' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_noanswer_5' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_noanswer_4' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_noconsent' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_queue' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_aminoleban_total_unverified_retrieval' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},

					'fetch_pletaal_new' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_finished' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_stopped' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_switched' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_total_verified' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_declined' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_wrong_number' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_noanswer_5' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_noanswer_4' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_noconsent' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_queue' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_pletaal_total_unverified_retrieval' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},

					'fetch_invalid_blurred' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_invalid_duplicate' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_invalid_already_enrolled' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_invalid_wrong_upload' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},
					'fetch_invalid_total' => function ($q) use ($first_day, $now) 
					{$q->whereBetween('created_at',[$first_day, $now]);},

				]);
			},

	'fetch_voluntary_abilify' => function ($q) use ($first_day, $now) {
    $q->whereBetween('created_at',[$first_day, $now]);},'fetch_voluntary_aminoleban' => function ($q) use ($first_day, $now) {
    $q->whereBetween('created_at',[$first_day, $now]);},'fetch_voluntary_pletaal' => function ($q) use ($first_day, $now) {
    $q->whereBetween('created_at',[$first_day, $now]);}])->orderBy('order_by')->get();

		//pre($list);
		return view('otsuka.dashboard.generated-summary',compact('mtd_start','now','yesterday','tomorrow','list'));
	}

	public function generate_summary_eom(){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$medrep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance_reason = new \App\Http\Models\otsuka\Compliance_reason;
		$reports_list = new \App\Http\Models\otsuka\Reports_list;

            		$dt = date('Y-m-d');
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-02-01 00:00:00';
                    $now = date('Y-m-d').' 17:00:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));
                    //$excluded = array('Referral','Voluntary');


         ini_set('memory_limit', '1024M');
        $list = $medrep->with(['connect_user','fetch_voluntary_abilify','fetch_voluntary_aminoleban','fetch_voluntary_pletaal'])->get();

		//pre($list);
		return view('otsuka.dashboard.generated-summary',compact('mtd_start','now','yesterday','tomorrow','list'));
	}

	public function generate_invalid(Request $request){
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;

            		$dt = date('Y-m-d');
            		//$dt = date('Y-m-d');
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-02-01 00:00:00';
                    $now = date('Y-m-d').' 17:00:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));
					
					
					// URL PARAMETER From And TO			
		if(!empty($request->from))
		{
			$from = $request->from;
			$first_day  = $from;
		}
		else
		{
		
		}
		if(!empty($request->to))
		{
			$to = $request->to;
			$now = $to ;
		}
		else{
			//$to =  $now;
		}	
					
					


		$data = $med_upload->where('active',1)->where('type','=','Enroll')->where('trail_status',199)->where('active',1)->whereBetween('created_at',[$first_day, $now])->with(['user_medrep'])->get();

		return view('otsuka.dashboard.generated-invalid',compact('data','mtd_start','now','yesterday','tomorrow'));
	}

	public function generate_daily_call(Request $request){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance_reason = new \App\Http\Models\otsuka\Compliance_reason;
		$reports_list = new \App\Http\Models\otsuka\Reports_list;

            		$dt = date('Y-m-d');
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-02-01 00:00:00';
                    $now = date('Y-m-d').' 20:30:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 20:30:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));
					
						// URL PARAMETER From And TO			
		if(!empty($request->from))
		{
			$from = $request->from;
			$first_day  = $from;
		}
		else
		{
		
		}
		if(!empty($request->to))
		{
			$to = $request->to;
			$now = $to ;
		}
		else{
			//$to =  $now;
		}				
					

		$cmid_data = $call_verification->select('cmid')->whereBetween('updated_at',[$first_day, $now])->orderBy('cmid','asc')->get()->toArray();
		$data = $cmid->whereIn('id',$cmid_data)->where('active',1)->with(['fetch_verification','fetch_upload','fetch_patient','fetch_redemption'])->get();

		return view('otsuka.dashboard.generated-report-daily-call',compact('data','mtd_start','now','yesterday','tomorrow'));
	}

	public function individual(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance_reason = new \App\Http\Models\otsuka\Compliance_reason;
		$reports_list = new \App\Http\Models\otsuka\Reports_list;

		$medreps = $med_rep->orderBy('pso_name','asc')->get();

		//pre($medreps);
		return view('otsuka.dashboard.individual-daily',compact('medreps','r'));
	}

	public function individual_report(Request $r){
		$med_queue = new \App\Http\Models\otsuka\Queue;
		$med_upload = new \App\Http\Models\otsuka\Upload;
		$med_rep = new \App\Http\Models\otsuka\Medrep;
		$patient = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$patients_kit_allocation = new \App\Http\Models\otsuka\Patients_kit_allocation;
		$national_md_list = new \App\Http\Models\otsuka\National_md_list;
		$sku = new \App\Http\Models\otsuka\Sku;
		$patient_med_info = new \App\Http\Models\otsuka\Patients_med_info;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$help = new \App\Http\Controllers\otsuka\helper;
		$call_status = new \App\Http\Models\otsuka\Call_status;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$months = new \App\Http\Models\otsuka\Months;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance_reason = new \App\Http\Models\otsuka\Compliance_reason;
		$reports_list = new \App\Http\Models\otsuka\Reports_list;

		$medreps = $med_rep->orderBy('pso_name','asc')->get();

		if($r->medrep == 'all')
		{
			$data2 = $med_upload->where('active',1)->whereBetween('created_at',[$r->from, $r->to])->with(['fetch_user','fetch_validated','fetch_mdinfo','fetch_status','fetch_call','fetch_allocation','encoded_by'])->get();
		}
		else
		{
			$data = $patients_kit_allocation->select('one_quest_id')->where('emp_code',$r->medrep)->where('product','!=','mucosta')->get()->toArray();
			$medrep = $med_rep->select('pso_name')->where('emp_code',$r->medrep)->get()->first();
			$data2 = $med_upload->where('active',1)->whereIn('patient_kit_number', $data)->whereBetween('created_at',[$r->from, $r->to])->with(['fetch_user','fetch_mdinfo','fetch_status','fetch_call','fetch_allocation_report','encoded_by'])->get();
			
			$verified_count = $med_upload->where('active',1)->whereIn('patient_kit_number', $data)->whereBetween('created_at',[$r->from, $r->to])->where('patient_consent','!=','')->whereNotNull('patient_consent')->where('trail_status','!=','199')->count();
			$notverified_count = $med_upload->where('active',1)->whereIn('patient_kit_number', $data)->whereBetween('created_at',[$r->from, $r->to])->where(function($query){$query->whereNull('patient_consent')->orWhere('patient_consent','=','')->orWhere('trail_status',199);})->count();
			$mtd_count = $med_upload->where('active',1)->whereIn('patient_kit_number', $data)->whereBetween('created_at',['2018-02-01 00:00:00', $r->to])->count();
					
					//pre($emailcc);

					$to = "marc.briones@magellan-solutions.com";
					$from = "info@onequest.com.ph";
					$cc = ['ahirata@onequest.com.ph','vernon.belgira@magellan-solutions.com'];
					$bcc = "webdev@magellan-solutions.com";
					$EmailSubject = "Daily individual Retrieval Report"." - ".date('mdy');


					//pre($emailcc);

		        	if($to && $from):

				        Mail::send('email.daily_emails', [	'data2' => $data2,
				        									'medrep' => trim($medrep->pso_name,'"'),
															'verified_count'=> $verified_count,
															'notverified_count'=> $notverified_count,
															'mtd_count'=> $mtd_count,

														], function ($m) use ($to,$from,$cc,$bcc,$EmailSubject){					            
				            $m->from($from);
				            $m->to($to)->subject($EmailSubject);
				            $m->cc($cc);
				            $m->bcc($bcc);
				        });
					echo "success";

			        endif;	
				
		}

		//pre(trim($medrep->pso_name,'"'));
		return view('otsuka.dashboard.individual-daily-report',compact('medreps','data','data2','r','verified_count','notverified_count','mtd_count'));
	}

	public function generate_compliance_call(){
		$patient_validated = new \App\Http\Models\otsuka\Patient_Validated;
		$call_verification = new \App\Http\Models\otsuka\Call_verification;
		$queue_call_verification = new \App\Http\Models\otsuka\Queue_call_verification;
		$cmid = new \App\Http\Models\otsuka\Cmid;
		$old_retrieval_enrollment = new \App\Http\Models\otsuka\Old_retrieval_enrollment;
		$compliance = new \App\Http\Models\otsuka\Compliance;
		$Upload = new \App\Http\Models\otsuka\Upload;



            		$dt = date('Y-m-d');
            		//$first_day = '2018-02-01 00:00:00';
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-05-01 00:00:00';
                    $mtd_last = '2018-05-31 23:00:00';
                    $now = date('Y-m-d').' 20:30:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 20:30:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));


		$data = $compliance->whereBetween('created_at',[$first_day,$now])->with(['fetch_patient','fetch_callstatus','fetch_old','fetch_patients_medinfo','fetch_compliance_reason','fetch_month'])->get();


		//pre($data);

		return view('otsuka.dashboard.compliance-call-report',compact('data','mtd_start','now','yesterday','tomorrow'));
	}


}