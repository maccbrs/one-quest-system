<?php namespace App\Http\Controllers\diamond;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class profileController extends Controller
{

	public function __construct(){

        $this->middleware('diamond');

    }

	public function index(){

		return view('diamond.profile.index');
	}
}