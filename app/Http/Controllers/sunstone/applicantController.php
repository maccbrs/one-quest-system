<?php namespace App\Http\Controllers\sunstone;

use App\Http\Controllers\Controller;
use App\Http\Models\gem\Campaign;
use App\Http\Models\sunstone\Exam;
use App\Http\Models\sunstone\Interview;
use App\Http\Models\sunstone\Position;
use App\Http\Requests;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use \App\Http\Models\sunstone\Applicant;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Auth;
use Alert;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\TemplateProcessor;

class applicantController extends Controller
{

	public function index(){

		
		$form_fields = new \App\Http\Models\sunstone\FormFields;

		$formfields = $form_fields->where('status','=','1')->orderby('order_id','asc')->get();

		$position = new \App\Http\Models\sunstone\Position;
		$positions = $position->pluck('name','id');

		return view('sunstone.applicant.index',compact('formfields','positions'));

	}

	public function applicant_list($status){
        $app = new \App\Http\Models\sunstone\Applicant;

        /*
         * Applicant status codes:
         * Pending = 1
         * Far-hire = 2
         * Near-hire = 3
         * Ready-hire = 4
         * For-training = 5
         * Endorse to Department = 6
         * Job offer = 7
         * Failed = 0
         **/

        switch ($status){
            case 'all':
                $applicant = $app
                    ->with('camp')
                    ->with('position')
                    ->orderBy('id', 'desc')
                    ->paginate(10);
                break;

            case 'pending':
            case 1:
                $applicant = $app
                    ->with('camp')
                    ->with('position')
                    ->orderBy('id', 'desc')
                    ->where('status', '=', 1)
                    ->paginate(10);
                break;

            case 'far-hire':
            case 2:
                $applicant = $app
                    ->with('camp')
                    ->with('position')
                    ->orderBy('id', 'desc')
                    ->where('status', '=', 2)
                    ->paginate(10);
                break;

            case 'near-hire':
            case 3:
                $applicant = $app
                    ->with('camp')
                    ->with('position')
                    ->orderBy('id', 'desc')
                    ->where('status', '=', 3)
                    ->paginate(10);
                break;

            case 'ready-hire':
            case 4:
                $applicant = $app
                    ->with('camp')
                    ->with('position')
                    ->orderBy('id', 'desc')
                    ->where('status', '=', 4)
                    ->paginate(10);
                break;

            case 'for-training':
            case 5:
                $applicant = $app
                    ->with('camp')
                    ->with('position')
                    ->orderBy('id', 'desc')
                    ->where('status', '=', 5)
                    ->paginate(10);
                break;
        }

        $camp = new \App\Http\Models\sunstone\Campaign;
        $campaign = $camp->pluck('title','id');

        $position = new \App\Http\Models\sunstone\Position;
        $positions = $position->pluck('name','id');

        foreach ($applicant as $key => $value) {
            $additional_decoded = json_decode($value->additional);
            $applicant[$key]['additional_decoded'] = $additional_decoded;

            switch($applicant[$key]['status']){
                case 1: $applicant[$key]['status_name'] = 'Pending'; break;
                case 2: $applicant[$key]['status_name'] = 'Far-hire'; break;
                case 3: $applicant[$key]['status_name'] = 'Near-hire'; break;
                case 4: $applicant[$key]['status_name'] = 'Ready-hire'; break;
                case 5: $applicant[$key]['status_name'] = 'For-training'; break;
            }
        }

        return view('sunstone.applicant.list',compact('applicant','campaign','positions'));

	}

	public function add(Request $r){

		$form_fields = new \App\Http\Models\sunstone\FormFields;

		$count = $form_fields->count();

		if(!empty($r->input())){

			$message_content['content'] = $r->input("content");
			$content_holder = json_encode($message_content);

			$form_fields['key'] = $r->input("key");
			$form_fields['label'] = $r->input("label");
			$form_fields['required_status'] = $r->input("required_status");
			$form_fields['type'] = $r->input("type");
			$form_fields['option_values'] =$r->input("option_values");
			$form_fields['label_values'] =$r->input("label_values");
			$form_fields['status'] =$r->input("status");
			$form_fields['size'] =$r->input("size");
			$form_fields['order_id'] =$count;
	
			$form_fields->save();
		}

		Alert::message('Applicant has been Added to Trainee List!')->persistent('Close');

		return redirect()->back();
	}

	public function update_applicant(Request $r)
    {

        $applicant = new \App\Http\Models\sunstone\Applicant;
        $formfields = null;
   
        if(!empty($r->input())){

            $additional = json_encode($r->input("additional"));

            $app_data['last_name'] = $r->input("last_name");
            $app_data['first_name'] = $r->input("first_name");
            $app_data['middle_name'] = $r->input("middle_name");
            $app_data['additional'] = $additional;
            $app_data['status'] = $r->input("status");
            $app_data['position_id'] =$r->input("position_id");
    
            $applicant->where('id','=',$r->input("id"))->update($app_data);

            if($r->input("status") == 5){

            	if($r->input("position_type") == 1){
            		$tr_stage = new \App\Http\Models\sunstone\TrainingStage;

	            	$tr_stage['trainee_id'] = $r->input("id");
					$tr_stage['stage'] = 'FST';
					$tr_stage['wave_id'] = null;
					$tr_stage['status'] = 1;
					$tr_stage['camp_id'] = $r->input("camp_id");;
		
					$tr_stage->save();

				}else{

					
					$tr_stage = new \App\Http\Models\sunstone\PendingNonAgent;

	            	$tr_stage['applicant_id'] = $r->input("id");
					$tr_stage['status'] = 1;
		
					$tr_stage->save();
				}
            }

        }

        Alert::message('Applicant has Information has been Updated')->persistent('Close');
        return $this->pool();
    }


    public function search(Request $request)
    {
        $app = new \App\Http\Models\sunstone\Applicant;
        $search_key = $request->input('search_key');
        $search_field = $request->input('search_field');

        if($search_field == "name")
        {
            $applicants = Applicant::with('camp')
                ->with('position')
                ->orderBy('id', 'desc')
                ->where('first_name', 'LIKE', '%'.$search_key.'%')
                ->orWhere('middle_name', 'LIKE', '%'.$search_key.'%')
                ->orWhere('last_name', 'LIKE', '%'.$search_key.'%')
                ->paginate(20);
        }
        elseif ($search_field == "position")
        {
            $applicants = Applicant::with('camp')
                ->with('position')
                ->orderBy('id', 'desc')
                ->whereHas('position', function($q) use ($search_key) {
                  $q->where('name', 'LIKE', '%'.$search_key.'%')
                  ->orWhere('alias', 'LIKE', '%'.$search_key.'%')
                  ->orWhere('dept', 'LIKE', '%'.$search_key.'%');
                })->paginate(20);

        }elseif ($search_field == "id")
        {
            $applicants = Applicant::with('camp')
                ->with('position')
                ->where('id', $search_key)
                ->paginate(20);
        }

        $positions = Position::all();
        $campaigns = Campaign::all();

        foreach ($applicants as $key => $value) {
            $additional_decoded = json_decode($value->additional);
            $applicant[$key]['additional_decoded'] = $additional_decoded;
        }
        return view('sunstone.applicant.pool', compact('applicants', 'positions', 'campaigns'));

    }
    public function sort(){

		$form_fields = new \App\Http\Models\sunstone\FormFields;
		$formfields = $form_fields->where('status','=','1')->orderby('order_id','asc')->get();

		$position = new \App\Http\Models\sunstone\Position;
		$positions = $position->pluck('name','id');

		return view('sunstone.applicant.sort',compact('formfields','positions'));

	}

	public function edit(){
		$form_fields = new \App\Http\Models\sunstone\FormFields;
		$formfields = $form_fields->where('status','=','1')->orderby('order_id','asc')->get();
		$position = new \App\Http\Models\sunstone\Position;
		$positions = $position->pluck('name','id');
		return view('sunstone.applicant.edit',compact('formfields','positions'));
	}

	public function update_field(Request $r){

		$form_fields = new \App\Http\Models\sunstone\FormFields;

		$field['type'] = $r->input('type'); 
		$field['option_values'] = $r->input('option_values'); 
		$field['label_values'] = $r->input('label_values'); 
		$field['size'] = $r->input('size'); 
		$field['label'] = $r->input('label');

		$form_fields->where('id','=',$r->input('id'))->update($field);

		return redirect()->back();

	}


	public function attach_resume(Request $r){
        $file = $r->file('resume');
        $applicant = Applicant::find($r->input('id'));
        $attachment = time() . 'resume.' . $file->getClientOriginalExtension();
        Storage::disk('local')->put($attachment, File::get($file));
        Applicant::find($r->input('id'))->update(['attachment' => $attachment]);

        Alert::message('Resume uploaded!')->persistent('Close');
        return redirect()->back();
    }

    public function download_resume($applicant_id){
        $applicant = Applicant::find($applicant_id);

       // print_r(storage_path('app\\'.$applicant->attachment)); exit;

        return response()->download(storage_path('app\\'.$applicant->attachment));
    }


	public function sort_field(Request $r){
		$form_fields = new \App\Http\Models\sunstone\FormFields;
		if(!empty($r->input('order'))){

			foreach ($r->input('order') as $key => $value) {

				$field['order_id'] = $key;

				$form_fields->where('id','=',$value)->update($field);
			}
		}
		return redirect()->back();
	}

	public function delete(Request $r){

		$form_fields = new \App\Http\Models\sunstone\FormFields;

		$field['status'] = 0; 

		$form_fields->where('id','=',$r->input('id'))->update($field);

		return redirect()->back();

	}

	public function pool(){

	    $applicants = Applicant::paginate(20);
	    $positions = Position::where('status', 1)->get();
        $campaigns = Campaign::where('status', 1)->get();

      //  print_r('<pre>');print_r( $campaigns);print_r('</pre>'); exit; 

	    return view('sunstone.applicant.pool', compact('applicants', 'positions', 'campaigns'));
    }

    public function application_form()
    {

        $form_fields = new \App\Http\Models\sunstone\FormFields;

        $formfields = $form_fields->where('status','=','1')->orderBy('order_id','asc')->get();


        $position = new \App\Http\Models\sunstone\Position;
        $positions = $position->pluck('name','id');

        return view('sunstone.applicant.application-form',compact('formfields','positions'));
    }

    public function application_save(Request $r)
    {

        $applicant = new \App\Http\Models\sunstone\Applicant;

        $form_field = new \App\Http\Models\sunstone\FormFields;
        $formfields = $form_field->select('key','required_status')->get();

        //$validator = $form_field->nullchecker($formfields,$r->input());

        $formfields = null;

        $additional = json_encode($r->input("additional"));
        $applicant['last_name'] = $r->input("last_name");
        $applicant['first_name'] = $r->input("first_name");
        $applicant['middle_name'] = $r->input("middle_name");

        $applicant['additional'] = $additional;
        $applicant['status'] =$r->input("status");
        $applicant['position_id'] =$r->input("position_id");
        $applicant['source_date'] = date("Y-m-d", strtotime($r->input("source_date")));;


        $applicant->save();

        $applicant_id = $applicant['id'];

        Alert::success('Your Application has been Saved!', 'Congratulations! ');

        return view('sunstone.applicant.application-form',compact('formfields', 'applicant_id', 'positions'));

    }

    public function sorry(Request $r)
    {

        $email = $r->user()->email;

        return view('sorry',compact('email'));
    }

    public function print_information($applicant_id){
        $applicant = Applicant::find($applicant_id);
        $custom_fields = json_decode($applicant->additional, true);

        $template = new TemplateProcessor('ApplicationFormTemplate.docx');
        $template->setValue('LastName', $applicant->last_name);
        $template->setValue('FirstName', $applicant->first_name);
        $template->setValue('MiddleName', $applicant->middle_name);
        $template->setValue('ApplicationDate', $applicant->source_date);

        $rowsSet = ceil(count($custom_fields) / 4);
        $template->cloneRow('field1', $rowsSet);

        $fields = array_keys($custom_fields);
        $values = array_values($custom_fields);

        $field_chunks = array_chunk($fields, 4);
        $value_chunks = array_chunk($values, 4);
        $row = 0;

        foreach ($field_chunks as $field_chunk){
            $row++;
            $column = 0;
            foreach ($field_chunk as $field){
                $column++;
                if (is_array($field)){
                    $field = json_encode($field);
                }
                $template->setValue('field'. $column .'#'.$row, $field);
            }
        }

        $row = 0;
        foreach ($value_chunks as $value_chunk){
            $row++;
            $column = 0;
            foreach ($value_chunk as $value){
                $column++;
                if (is_array($value)){
                    $value = json_encode($value);
                }
                $template->setValue('value'. $column .'#'.$row, $value);
            }
        }

        $template->cloneRow('ExamTitle', count($applicant->exams));

        $row = 1;
        foreach ($applicant->exams as $exam){
            $template->setValue('ExamTitle' .'#'. $row, $exam->name);
            $template->setValue('ExamScore' .'#'. $row, $exam->get_points().' / '.count($exam->exam_questions));
            $template->setValue('ExamRating' .'#'. $row, $exam->get_score().'% out of '.$exam->passing_rate."%");
            $template->setValue('ExamPassed' .'#'. $row, ($exam->is_passed() ? "Passed" : "Failed"));
            $row++;
        }

        $file = "ApplicantInfo.docx";
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' . $file . '"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $template->saveAs('php://output');

        return "test";
    }

    public function interview_dispo(Request $r){
        if(!empty($r->input())){
            $applicant = Applicant::find($r->input('applicant_id'));
            $applicant->status = $r->input('interview_dispo');
            $applicant->save();

            $interview = new Interview;
            $interview->remarks = $r->input('interview_remarks');
            $interview->type = $r->input('interview_type');
            $interview->interviewer_name = $r->input('interviewer_name');
            $interview->applicant()->associate($applicant);
            $interview->save();

            if(!empty($r->input('endorsed_campaigns'))){
                $applicant->campaigns = json_encode($r->input('endorsed_campaigns'));
            }

            $applicant->save();
            Alert::success('Applicant disposition saved', 'Congratulations! ');
            return $this->pool();
        }
    }

    public function active(Request $r){
        $positions = Position::where('status', 1)->get();
        $campaigns = Campaign::where('status', 1)->get();
        $applicants = Applicant::whereIn('status', [6, 7])->get();

        return view('sunstone.applicant.active', compact('applicants', 'positions', 'campaigns'));
    }

    public function handle_job_offer(Request $r){
        if ($r->input()){
            $applicant = Applicant::find($r->input('applicant_id'));
            $applicant->status = 7;
            $applicant->save();

            $interview = new Interview;
            $interview->remarks = $r->input('interview_remarks');
            $interview->type = "joboffer";
            $interview->interviewer_name = $r->input('interviewer_name');
            $interview->applicant()->associate($applicant);
            $interview->save();


            Alert::message('Applicant put on JO queue', 'Success!')->persistent('Close');
            return $this->active($r);
        }
    }

    public function back_to_pool(Request $r){
        if ($r->input()){
            $applicant = Applicant::find($r->input('applicant_id'));
            $applicant->status = $r->input('pool_category');
            $applicant->save();

            $interview = new Interview;
            $interview->remarks = $r->input('interview_remarks');
            $interview->type = "backtopool";
            $interview->interviewer_name = Auth::user()->name;
            $interview->applicant()->associate($applicant);
            $interview->save();


            Alert::message('Applicant sent back to Pool', 'Success!')->persistent('Close');
            return $this->active($r);
        }
    }

    public function send_to_training(Request $r){
        if ($r->input()){
            $applicant = Applicant::find($r->input('applicant_id'));
            $applicant->status = 5;
            $applicant->save();

            $interview = new Interview;
            $interview->remarks = $r->input('interview_remarks');
            $interview->type = "senttotraining";
            $interview->interviewer_name = Auth::user()->name;
            $interview->applicant()->associate($applicant);
            $interview->save();

            Alert::message('Applicant sent back to Pool', 'Success!')->persistent('Close');
            return $this->active($r);
        }
    }

    public function for_training(){
        $positions = Position::where('status', 1)->get();
        $campaigns = Campaign::where('status', 1)->get();
        $applicants = Applicant::where('status', 5)->get();

        return view('sunstone.applicant.for-training', compact('applicants', 'positions', 'campaigns'));

    }

}