<?php namespace App\Http\Controllers\sunstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use \App\Http\Models\sunstone\Campaign;
use Auth;
use Alert;

class campaignController extends Controller
{

	public function index(){

		$camp = new Campaign;
		$campaigns = $camp->get();

		return view('sunstone.campaign.index',compact('campaigns'));

	}

	public function add(Request $r){

		$campaign = new Campaign;

		if(!empty($r->input())){

			$campaign['title'] =$r->input("title");
			$campaign['alias'] =$r->input("alias");
			$campaign['status'] =$r->input("status");
	
			$campaign->save();

			Alert::message('Campaign has been Added to the List!')->persistent('Close');

		}

		return $this->index();

	}

	public function edit(Request $r){

        if(!empty($r->input())){
            $campaign = Campaign::find($r->input('id'));
            $campaign['title'] =$r->input("title");
            $campaign['alias'] =$r->input("alias");
            $campaign['status'] =$r->input("status");

            $campaign->save();

            Alert::message('Campaign updated!')->persistent('Close');

        }

        return $this->index();
    }

}

