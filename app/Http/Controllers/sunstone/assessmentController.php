<?php namespace App\Http\Controllers\sunstone;

use App\Http\Controllers\Controller;
use App\Http\Models\magnetite\Applicant;
use App\Http\Models\sunstone\Assessment;
use App\Http\Models\sunstone\Choice;
use App\Http\Models\sunstone\ExamQuestion;
use App\Http\Models\sunstone\Position;
use App\Http\Models\sunstone\Question;
use App\Http\Models\sunstone\Exam;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Alert;
use Illuminate\Support\Facades\App;

class assessmentController extends Controller
{
    public function index(){
        $positions_all = Position::all();
        $assessments_all = Assessment::with('positions')->get();
        return view('sunstone.assessment.index', compact('positions_all', 'assessments_all'));
    }

    public function add(Request $r){

        if(!empty($r->input())){

            if(count(Assessment::where('name', '=', $r->input('assessment_name'))->get())) {
                Alert::error('Assessment name already exists')->persistent('Close');
                return $this->index();
            }

            $assessment = new Assessment;
            $assessment['name'] = $r->input('assessment_name');
            $assessment->save();
            $assessment->positions()->attach($r->input('position_id'));
        }

        Alert::success('Assessment Added')->persistent('Close');
        return $this->index();


    }

    public function manage_question($id){

        $positions_all = Position::all();
        $assessments_all = Assessment::with('positions')->get();
        $assessment_found = Assessment::find($id);

        return view('sunstone.assessment.index', compact('positions_all', 'assessments_all', 'assessment_found'));
    }

    public function delete_question($qid, $aid){
        $question = Question::find($qid);
        $question->assessments()->detach($aid);
        Alert::success('Question Deleted.')->persistent('Close');
        return $this->manage_question($aid);
    }

    public function add_question(Request $r){

        if(!empty($r->input())){
            $question_decoded = json_decode($r->input('question'));
            $choices_decoded = json_decode($r->input('choices'), true);

            $question = new Question;

            $question['body'] = $question_decoded->value;
            $question['image'] = $question_decoded->image;
            $question['time_limit'] = $r->input('time_limit');
            $question->save();

            foreach ($choices_decoded as $key => $choice){
                $choices = New Choice;
                $choices['body'] = $choice['body'];
                $choices['is_correct_answer'] = $choice['is_correct_answer'];
                $choices->question()->associate($question);
                $choices->save();
            }
            $question->assessments()->attach($r->input('assessment_id'));
            $correct_answer = $question->choices()->where('is_correct_answer', true)->first();
            $question['correct_answer'] = $correct_answer->id;
            $question->save();
        }

        Alert::message('Question Added')->persistent('Close');
        return $this->manage_question($r->input('assessment_id'));
    }

    public function edit_question(Request $r){

        if (!empty($r->input())){
            $correct_answer = $r->input('answer');

            $question = Question::find($r->input('question_id'));
            $question->update(['body' => $r->input('body')]);
            $question->update(['time_limit' => $r->input('time_limit')]);

             foreach ($question->choices as $choice){
                 Choice::find($choice->id)->update(['is_correct_answer' => 0]);
             }

            Choice::find($correct_answer)->update(['is_correct_answer' => 1]);
            $question->update(['correct_answer' => $correct_answer]);

            Alert::message('Question updated')->persistent('Close');
            return $this->manage_question($r->input('assessment_id'));
        }

    }

    public function edit(Request $r){
        if (!empty($r->input())){
            $assessment = Assessment::find($r->input('assessment_id'));
            $assessment->update(['passing_rate' => $r->input('passing_rate')]);
            $assessment->positions()->sync($r->input('position_id'));
            Alert::success('Assessment Updated')->persistent('Close');
            return $this->index();
        }
    }

    public function results(){
        $exams = Exam::paginate(20);

        foreach ($exams as $key => $exam){
            $points = 0;
            foreach ($exam->exam_questions as $key2 => $question){
                if ($question->applicant_answer == $question->correct_answer){
                    $points = $points + 1;
                }
            }
            $score = ($points / (float) count($exam->exam_questions)) * 100;
            $rating = ($score < $exam->passing_rate) ? "Failed" : "Passed";

            $results[$exam->id] = ["points" => $points, "question_count" => count($exam->exam_questions),
            "score" => $score, "rating" => $rating, "passing_rate" => $exam->passing_rate];
        }

        return view('sunstone.assessment.results', compact('exams', 'results'));
    }

    public function search_applicant(Request $r){
        if(!empty($r->input())){
            $search_field = $r->input('search_field');
            $search_key = $r->input('search_key');
            $exams = "";

            if($search_field == 'id'){
                $exams = Exam::with('Applicant')->where('applicant_id', $r->input('search_key'))
                    ->paginate(20);
            }

            if ($search_field == 'name'){
                $applicants = new Applicant;
                $applicants_found = $applicants
                    ->where('first_name', 'LIKE', '%'.$search_key.'%')
                    ->orWhere('middle_name', 'LIKE', '%'.$search_key.'%')
                    ->orWhere('last_name', 'LIKE', '%'.$search_key.'%')
                    ->get();

                foreach ($applicants_found as $key => $applicant){
                    $exams = Exam::where('applicant_id', $applicant->id)->paginate(10);
                }

            }

            if (!empty($exams)){
                foreach ($exams as $key => $exam){
                    $points = 0;
                    foreach ($exam->exam_questions as $key2 => $question){
                        if ($question->applicant_answer == $question->correct_answer){
                            $points = $points + 1;
                        }
                    }
                    $score = ($points / (float) count($exam->exam_questions)) * 100;
                    $rating = ($score < $exam->passing_rate) ? "Failed" : "Passed";

                    $results[$exam->id] = ["points" => $points, "question_count" => count($exam->exam_questions),
                        "score" => $score, "rating" => $rating, "passing_rate" => $exam->passing_rate];
                }
            }

            return view('sunstone.assessment.results', compact('exams', 'results'));
        }
    }

}