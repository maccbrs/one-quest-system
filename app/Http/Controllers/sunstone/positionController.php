<?php namespace App\Http\Controllers\sunstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use \App\Http\Models\sunstone\Position;
use Auth;
use Alert;

class positionController extends Controller
{

	public function index(){

		$position = new Position;
		$positions = $position->get();

		return view('sunstone.position.index',compact('positions'));

	}

	public function add(Request $r){

		$position = new Position;

		if(!empty($r->input())){

			$position['name'] =$r->input("name");
			$position['alias'] =$r->input("alias");
			$position['type'] =$r->input("type");
			$position['status'] =$r->input("status");
	
			$position->save();

			Alert::message('Position has been Added to the List')->persistent('Close');

		}

		return $this->index();

	}

    public function edit(Request $r){

        if(!empty($r->input())){
            $position = Position::find($r->input('id'));
            $position['name'] =$r->input("name");
            $position['alias'] =$r->input("alias");
            $position['type'] =$r->input("type");
            $position['status'] =$r->input("status");

            $position->save();

            Alert::message('Position updated!')->persistent('Close');

        }

        return $this->index();

    }


}

