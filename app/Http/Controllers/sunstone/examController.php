<?php

namespace App\Http\Controllers\sunstone;

use App\Http\Controllers\Controller;

use App\Http\Models\magnetite\Applicant;
use App\Http\Models\sunstone\Exam;
use App\Http\Models\sunstone\ExamChoice;
use App\Http\Models\sunstone\ExamQuestion;
use App\Http\Models\sunstone\Position;
use App\Http\Requests;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\App;

class examController extends Controller
{
    public function search(Request $r){

        if (!empty($r->input())){
            $applicant = Applicant::find($r->input('applicant_id'));
        }

        return view('sunstone.assessment.exam-search', compact('applicant'));
    }

    public function next(Request $r){

        if (!empty($r->input())){
            $exam_done = false;
            $applicant_id = $r->input('applicant_id');
            $question_id = $r->input('question_id');
            $applicant_answer = (empty($r->input('answer'))) ? 0 : $r->input('answer');
            $exam_id = $r->input('exam_id');
            ExamQuestion::find($question_id)->update(['applicant_answer' => $applicant_answer]);

            $exam_question = $this->get_unanswered_question($exam_id);
            $exam = Exam::find($exam_id);
            if (!$exam_question){
                Exam::find($exam_id)->update(['done' => 1]);
                $exam = $this->get_incomplete_exam($applicant_id);
                if (!$exam){
                    return view('sunstone.assessment.exam-done');
                }
                $exam_question = $this->get_unanswered_question($exam->id);
            }
            return view('sunstone.assessment.exam-start', compact('exam', 'exam_question', 'applicant_id', 'exam_done'));
        }
    }

    public function start(Request $r){
        $applicant_id = $r->input('applicant_id');
        $exam_done = false;

        if (!$this->is_exam_prepared($applicant_id)){
            $this->prepare_exam($applicant_id);
        }

        $exam = $this->get_incomplete_exam($applicant_id);

        if (count($exam) < 1){
            $exam_done = true;
            return view('sunstone.assessment.exam-start', compact('exam', 'exam_question', 'applicant_id', 'exam_done'));
        }

        $exam_question = $this->get_unanswered_question($exam->id);

        return view('sunstone.assessment.exam-start', compact('exam', 'exam_question', 'applicant_id', 'exam_done'));
    }

    public function get_incomplete_exam($applicant_id){
        $exam = Exam::where('applicant_id', $applicant_id)
            ->where('done', 0)
            ->first();

        return (count($exam) < 1) ? false : $exam;
    }

    public function mark_answered($question_id, $applicant_answer_id){
        ExamQuestion::find($question_id)
        ->update(['applicant_answer' => $applicant_answer_id]);
    }

    public function get_unanswered_question($exam_id){
        $question = ExamQuestion::where('exam_id', $exam_id)
            ->whereNull('applicant_answer')
            ->first();

        return (count($question) < 1) ? false : $question;
    }

    public function are_questions_done($exam_id){
        $total_question = count(ExamQuestion::where('exam_id', $exam_id)->get());
        $questions_done = count(ExamQuestion::whereNotNull('answer_correct')->get());

        return ($total_question == $questions_done);
    }

    public function is_exam_done($applicant_id){
        $total_exams = count(Exam::where('applicant_id', $applicant_id)->get());
        $exams_done = count(Exam::where('done', 1)->get());

        return ($total_exams == $exams_done);
    }

    public function is_exam_prepared($applicant_id){
        $total_exams = count(Exam::where('applicant_id', $applicant_id)->get());
        return ($total_exams > 0);
    }

    public function prepare_exam($id){

        $applicant = Applicant::find($id);
        $assessments = Position::find($applicant['position_id'])->assessments()->get();

        foreach ($assessments as $key => $assessment){
            $exam = new Exam;
            $exam['name'] = $assessment->name;
            $exam['done'] = 0;
            $exam['passing_rate'] = $assessment->passing_rate;
            $exam->applicant()->associate($applicant);
            $exam->save();

            foreach ($assessment->questions as $key2 => $question){
                $exam_question = new ExamQuestion;
                $exam_question['body'] = $question->body;
                $exam_question['image'] = $question->image;
                $exam_question['time_limit'] = $question->time_limit;
                $exam_question['correct_answer'] = $question->correct_answer;
                $exam_question->exam()->associate($exam);
                $exam_question->save();

                foreach ($question->choices as $choice){
                    $exam_choice = new ExamChoice;
                    $exam_choice['body'] = $choice->body;
                    $exam_choice['is_correct_answer'] = $choice->is_correct_answer;
                    $exam_choice->question()->associate($exam_question);
                    $exam_choice->save();
                }
                $correct_answer = $exam_question->choices()->where('is_correct_answer', 1)->first();
                $exam_question['correct_answer'] = $correct_answer->id;
                $exam_question->save();
            }
        }
    }

}