<?php namespace App\Http\Controllers\magnetite;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use \App\Http\Models\magnetite\Waves;
use \App\Http\Models\magnetite\Applicant;
use Auth;
use Alert;

class waveController extends Controller
{

	public function index(){

		$waves = new Waves;
		$waves_list = $waves
			->with('trainer')
			->with('camp')
			->with('stage')
			->orderBy('id','desc')
			->get();

		$applicant = new  Applicant;

		$app_list = $applicant->select('first_name','last_name','id')->get(); 

		foreach ($app_list as $key => $value) {
			
			$app[$value->id] = $value->first_name . " " . $value->last_name;

		}

		return view('magnetite.wave.index',compact('waves_list','app'));

	}

	public function add(Request $r){

		$waves = new Waves;
		if(!empty($r->input())){

			$waves['name'] =$r->input("name");
			$waves['camp_id'] =$r->input("camp_id");
			$waves['trainer_id'] =$r->input("trainer_id");
			$waves->save();
		}

		Alert::message('New Wave has been Created')->persistent('Close');

		return $this->index();

	}


}

