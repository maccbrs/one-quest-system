<?php namespace App\Http\Controllers\magnetite;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use \App\Http\Models\magnetite\GemUsers;
use Auth;
use Alert;

class monitoringController extends Controller
{

	public function index(){

		$stage = new \App\Http\Models\magnetite\TrainingStage;

		$trainee = $stage
			->whereIn('stage',['FST','pst','Incubation','failed'])
			->with('trainee')
			->with('wave')
			->with('camp')
			->groupBy('trainee_id')
			->orderBy('id','desc')
			->get(); 

		$access['level'] = Auth::user()->user_level;
		$access['type'] = Auth::user()->user_type;
		$access['id'] = Auth::user()->id;

		$gem_user = new GemUsers;
		$get_access = $gem_user->access_checker($access); 

		$trainer = $gem_user->where('user_type','=','Trainer')->pluck('name', 'id'); 

		$camp = new \App\Http\Models\magnetite\Campaign;
		$campaign = $camp->pluck('title','id');

		$wave = new \App\Http\Models\magnetite\Waves;
		$waves = $wave->select('name','id','camp_id')->get();

		return view('magnetite.monitoring.index',compact('trainee','access','get_access','campaign','waves'));

	}

	public function update(Request $r){

		$stages = new \App\Http\Models\magnetite\TrainingStage;
       
        if(!empty($r->input())){

            $stage['stage'] = $r->input("stage");
            $stages->where('trainee_id','=',$r->input("trainee_id"))->update($stage);

        }

        Alert::message('Trainee moved to ' . $r->input("stage"))->persistent('Close');

		return $this->index();

	}

}

