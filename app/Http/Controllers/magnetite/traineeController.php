<?php namespace App\Http\Controllers\magnetite;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use \App\Http\Models\magnetite\Applicant;
use \App\Http\Models\magnetite\GemUsers;
use Auth;
use Alert;

class traineeController extends Controller
{

	public function index(){

		$stage = new \App\Http\Models\magnetite\TrainingStage;

		$trainee = $stage
			->whereNotIn('stage',['Passed'])
			->with('trainee')
			->with('wave')
			->with('camp')
			->groupBy('trainee_id')
			->orderBy('id','desc')
			->get(); 

		$access['level'] = Auth::user()->user_level;
		$access['type'] = Auth::user()->user_type;

		$gem_user = new GemUsers;
		$get_access = $gem_user->access_checker($access); 

		$trainer = $gem_user->where('user_type','=','Trainer')->pluck('name', 'id'); 

		$camp = new \App\Http\Models\magnetite\Campaign;
		$campaign = $camp->pluck('title','id');

		$wave = new \App\Http\Models\magnetite\Waves;
		$waves = $wave->select('name','id','camp_id')->get();
		
		return view('magnetite.trainee.index',compact('trainee','waves','campaign','get_access','trainer'));

	}

	public function add(Request $r){

		$campaign = new Campaign;

		if(!empty($r->input())){
			$campaign['title'] =$r->input("title");
			$campaign['alias'] =$r->input("alias");
			$campaign['status'] =$r->input("status");
			$campaign->save();
		}

		return redirect()->back();

	}

	public function assign_wave(Request $r){

		$stage = new \App\Http\Models\magnetite\TrainingStage;
	
		if(!empty($r->input())){

			$applicant = new Applicant;

			$stage['trainee_id'] =$r->input("trainee_id");
			$stage['stage'] =$r->input("stage");
			$stage['wave_id'] =$r->input("wave_id");
			$stage['status'] =1;
			$stage->save();
		}

		Alert::message('Trainee has been Assigned to ' . ucfirst($r->input("wave_id")))->persistent('Close');

		return $this->index();

	}


	public function update_wave(Request $r){

		$stages = new \App\Http\Models\magnetite\TrainingStage;

		//print_r($r->input()); exit; 
       
        if(!empty($r->input())){

            $stage['wave_id'] = $r->input("wave_id");
            $stage['stage'] = $r->input("stage");
           
            $stages->where('id','=',$r->input("trainee_id"))->update($stage);

        }

        Alert::message('Trainee has been Assigned ')->persistent('Close');

		return $this->index();

	}


}

