<?php namespace App\Http\Controllers\bloodstone;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use AppHelper;
use App\Http\Models\bloodstone\BloodstoneAssets;


class InventoryFormController extends Controller
{

	public function main(){
		
		$Brand = new \App\Http\Models\bloodstone\MstBrand;
		$Dept = new \App\Http\Models\bloodstone\MstDepartment;
		$Model = new \App\Http\Models\bloodstone\MstModel;
		$Status = new \App\Http\Models\bloodstone\MstStatus;
		$Type = new \App\Http\Models\bloodstone\MstType;
		$Vendor = new \App\Http\Models\bloodstone\MstVendor;
		$Po = new \App\Http\Models\bloodstone\MstPo;
		$Or = new \App\Http\Models\bloodstone\MstOr;
		$help = new \App\Http\Controllers\bloodstone\Helper;

		$branditems = $help->fetchbrand($Brand->where('status','=','enable')->get());
		$deptitems = $help->fetchdept($Dept->where('status','=','enable')->get());
		$modelitems = $help->fetchmodel($Model->where('status','=','enable')->get());
		$statusitems = $help->fetchstatus($Status->where('status','=','enable')->get());
		$typeitems = $help->fetchtype($Type->where('status','=','enable')->get());
		$vendoritems = $help->fetchvendor($Vendor->where('status','=','enable')->get());
		$purchaseitems = $help->fetchpo($Po->where('status','=','enable')->get());
		$receiptitems = $help->fetchor($Or->where('status','=','enable')->get());

		return view('bloodstone.forms.main', compact('help','branditems','deptitems','modelitems','statusitems','typeitems','vendoritems','purchaseitems','receiptitems'));
	}
	

	public function store(Request $r){

		$Inventory = new \App\Http\Models\bloodstone\BloodstoneAssets;
		$Type = new \App\Http\Models\bloodstone\MstType;
		$help = new \App\Http\Controllers\bloodstone\Helper;

		$i = 0;
		$active = array('1');

		$data = array();

		while($i < count($r['unique_id'])){
		    $data[] = array(
		    	'unique_id' => $r['unique_id'][$i],
		        'type_id' => $r['type_id'][$i],
		        'brand_id' => $r['brand_id'][$i],
		        'model_id' => $r['model_id'][$i],
		        'serial_no' => $r['serial_no'][$i],
		        'description' => $r['description'][$i], 
		        'status_id' => $r['status_id'][$i],
		        'created_at' => $r['cat'][$i],
		        'updated_at' => $r['uat'][$i],
		        'created_by' => $r['cby'][$i],
		        'updated_by' => $r['uby'][$i],
		        'remarks' => $r['remarks'][$i],
		        'active' => $r['active'][$i],    
		    );
		    $i++;
		}

		$Inventory->insert($data);
/*
		$Inventory['type_id'] = $r->input('type_id');
		$Inventory['brand_id'] = $r->input('brand_id');
		$Inventory['model_id'] = $r->input('model_id');
		$Inventory['serial_no'] = $r->input('serial_no');
		$Inventory['description'] = $r->input('description');
		$Inventory['status_id'] = $r->input('status_id');*/

/*		$Inventory['created_by'] = $r->input("remarks");
		$Inventory['updated_by'] = $r->input("type_id");*/
/*
		$Inventory->type_id = $type[$i];
		$Inventory->brand_id = $brand[$i];
		$Inventory->model_id = $model[$i];
		$Inventory->serial_no = $serial[$i];
		$Inventory->description = $desc[$i];
		$Inventory->status_id = $status[$i];
		$Inventory->remarks = $remarks[$i];
		$Inventory->active = $active;
		$Inventory->created_by = $user;
		$Inventory->updated_by = $user;
				$Inventory->save();*/

		/*$Inventory = $request->all();*/



/*		$name_msg = $Type->select('name')->where('id','=',$request->type_id)->get();
		$templog = $name_msg->toArray();*/
		$log_msg = count($data). " items has successfully added". ",";
		$Logger = new AppHelper;
		$Logger->SaveLog($log_msg);


		/*pre($data);*/
		return back()->with('return_msg', $log_msg);
	}
	public function update(Request $request)
    {			
		$Inventory_Obj = new \App\Http\Models\bloodstone\BloodstoneAssets;
		$AssignLogs = new \App\Http\Models\bloodstone\AssignLogs;
		$AssignLogs->unique_id = $request->unique_id;		
		$AssignLogs->assigned_to = $request->assigned_to;
		$AssignLogs->remarks = $request->remarks;
		$AssignLogs->assigned_by = Auth::user()->id;
		$AssignLogs->assigned_at = Auth::user()->id;
		$AssignLogs->save();
		$Inventory = $Inventory_Obj::where('unique_id', $request->unique_id)
					  ->update(['assigned_by' => Auth::user()->id,
								'assigned_to' => $request->assigned_to,
								'current_status' => 'deployed']	);
		$Logger = new AppHelper;
		$UserDetails = $Logger->fetchuser($request->assigned_to);
		$log_msg = $request->unique_id . " has been assigned to " . $UserDetails['name'] . " By:  " . Auth::user()->name ;
		
		$Logger->SaveLog($log_msg);
		return redirect('/bloodstone/');
	}
	public function returnAssets(Request $request)
    {			
		$Inventory_Obj = new \App\Http\Models\bloodstone\BloodstoneAssets;
		$AssignLogs = new \App\Http\Models\bloodstone\AssignLogs;
		$AssignLogs->unique_id = $request->unique_id;
		
		//$AssignLogs->assignee_id = Auth::user()->id;
		$AssignLogs->remarks = $request->remarks;
		$AssignLogs->assigned_to = 42;
		$AssignLogs->assigned_by = Auth::user()->id;
		$AssignLogs->assigned_at = Auth::user()->id;
		$AssignLogs->save();
		$Inventory = $Inventory_Obj::where('unique_id', $request->unique_id)
					  ->update(['assigned_by' => Auth::user()->id,
								'assigned_to' => 42,//$request->assigned_to,
								'current_status' => 'available']	);
		$Logger = new AppHelper;
		
		//$Logger->fetchuser('42');
		$UserDetails = $Logger->fetchuser('42');
		
		$log_msg = $request->unique_id . " has been Return  " . $UserDetails['name'] . $request->assigned_to . " By:  " . Auth::user()->name ;
		
		$Logger->SaveLog($log_msg);
		return redirect('/bloodstone/');
	}
	
}