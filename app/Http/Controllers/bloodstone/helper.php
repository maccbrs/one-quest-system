<?php namespace App\Http\Controllers\bloodstone;
use Auth;


class Helper 
{

	public function fetchuser($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->name;
			endforeach;
			return $arr;
		endif;

		return false;

	}

	public function fetchtype($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->name;
			endforeach;
			return $arr;
		endif;

		return false;

	}

		public function fetchbrand($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->name;
			endforeach;
			return $arr;
		endif;

		return false;

	}

	public function fetchmodel($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->name;
			endforeach;
			return $arr;
		endif;

		return false;

	}

	public function fetchstatus($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->name;
			endforeach;
			return $arr;
		endif;

		return false;

	}

	public function fetchdept($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->name;
			endforeach;
			return $arr;
		endif;

		return false;

	}

	public function fetchpo($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->name;
			endforeach;
			return $arr;
		endif;

		return false;

	}

	public function fetchor($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->name;
			endforeach;
			return $arr;
		endif;

		return false;

	}

	public function fetchvendor($obj){

		if($obj->count()):
			$arr = [];
			foreach ($obj as $k => $v):
				$arr[$v->id] = $v->name;
			endforeach;
			return $arr;
		endif;

		return false;

	}	
}