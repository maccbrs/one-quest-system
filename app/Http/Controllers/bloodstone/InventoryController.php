<?php namespace App\Http\Controllers\bloodstone;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Models\bloodstone\MstBrand;
use App\Http\Models\bloodstone\MstDepartment;
use App\Http\Models\bloodstone\MstModel;
use App\Http\Models\bloodstone\MstStatus;
use App\Http\Models\bloodstone\MstType;
use AppHelper;


class InventoryController extends Controller
{

	public function index(){
		$Users = new \App\Http\Models\gem\User;
		$userlist = $Users->where('user_level', '=' , '1')->orderBy('name', 'asc')->get();
		$Inventory = new \App\Http\Models\bloodstone\BloodstoneAssets;

		$Type = new \App\Http\Models\bloodstone\MstType;
		$GemUser = new \App\Http\Models\gem\GemUser;
		$help = new \App\Http\Controllers\bloodstone\helper;

		$test = $GemUser->get();
		$usersObj = $GemUser->select('emp_code','name','id')->get();

		$qrcode = 'https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl=' ;

		$items = $Inventory->with('createdby','updatedby','status','brand','model','type','dept','assignedby','assignedto')->get();

		return view('bloodstone.dashboard.index',compact('items','users','help','type','larr_myarray','count','userlist','qrcode'));

	}
	
	public function masterfile($mstlist){
				
		$MasterFileName =  ucfirst($mstlist);
		$Test = array("\App\Http\Models\bloodstone" .chr(92) . $MasterFileName);		
		$A = new $Test[0];
		$masterfilelist =  $A->where('status', '=' , 'enable')->get();
		$urllist  = $mstlist;
		//var_dump($masterfilelist[0]['fillable']);
			
		return view('bloodstone.masterfile')->with('MstBrand',$masterfilelist)->with('table_columns',$A['fillable'])->with('urllist', $urllist);
		
	}
	public function store(Request $request,$mstlist)
	{
		$return_msg ="";
		$MasterFileName = '\Mst'.ucfirst($mstlist);
		$Test = array("\App\Http\Models\bloodstone".$MasterFileName);		
		$A = new $Test[0];
		$validateMstBrand =  $A->where('name',strtolower($request->name))->count();		
				
		//echo $validateMstBrand;
		if 	($validateMstBrand > 0)
		{
			$return_msg = $return_msg  .  " Duplicate Entry " . $request->name ;
			$masterfilelist =  $A->where('status', '=' , 'enable')->get();
			//break;
		}
		
		else if (strtolower($mstlist) == "brand") //(strtolower($mstlist) == "brand")
		{
		$string1 = "alias"	;
		
		$return_msg = $return_msg  .  $request->name  . " has successfully created" ;
		
	
		$masterfilelist = new \App\Http\Models\bloodstone\MstBrand;
		$masterfilelist ->name = strtolower($request->name);
		if(isset($request-> $string1)){
		$masterfilelist ->$string1 = $request->$string1; 
		}
		$masterfilelist ->remarks = $request->remarks; 
		$masterfilelist ->created_by = Auth::user()->name;
		$masterfilelist ->updated_by = (Auth::user()->name);
		$masterfilelist ->save();		
		$log_msg = $request->name  . " has successfully created "  . ucfirst($mstlist) . " MasterFile" ;
		$masterfilelist = MstBrand::where('status', '=' , 'enable')->get();//MstBrand::All();
		
		
		}
		else if (strtolower($mstlist) == "department")
		{
		
		
		
		$masterfilelist = new \App\Http\Models\bloodstone\MstDepartment;
		$masterfilelist ->name = strtolower($request->name);
		$masterfilelist ->full_description = strtolower($request->full_description);
		$masterfilelist ->remarks = $request->remarks; 
		$masterfilelist ->alias = $request->alias; 
		$masterfilelist ->created_by = Auth::user()->name;
		$masterfilelist ->updated_by = (Auth::user()->name);
		$masterfilelist ->save();
		$log_msg = $request->name  . " has successfully created " ;
		$masterfilelist = MstDepartment::where('status', '=' , 'enable')->get();//MstBrand::All();	
		}
		else if (strtolower($mstlist) == "model")
		{
		
			
		$masterfilelist = new \App\Http\Models\bloodstone\MstModel;
		$masterfilelist ->name = strtolower($request->name);
		$masterfilelist ->remarks = $request->remarks; 
		$masterfilelist ->alias = $request->alias; 
		$masterfilelist ->created_by = Auth::user()->name;
		$masterfilelist ->updated_by = (Auth::user()->name);
		$masterfilelist ->save();
		$log_msg = $request->name  . " has successfully created "  . ucfirst($mstlist) . " MasterFile" ;
		$masterfilelist = MstModel::where('status', '=' , 'enable')->get();//MstBrand::All();	
		}
		else if (strtolower($mstlist) == "status")
		{
		
			
		$masterfilelist = new \App\Http\Models\bloodstone\MstStatus;
		$masterfilelist ->name = strtolower($request->name);
		$masterfilelist ->remarks = $request->remarks; 
		$masterfilelist ->alias = $request->alias; 
		$masterfilelist ->created_by = Auth::user()->name;
		$masterfilelist ->updated_by = (Auth::user()->name);
		$masterfilelist ->save();
		$log_msg = $request->name  . " has successfully created "  . ucfirst($mstlist) . " MasterFile" ;
		$masterfilelist = MstStatus::where('status', '=' , 'enable')->get();//MstBrand::All();	
		}
		else if (strtolower($mstlist) == "type")
		{
		
		
		$masterfilelist = new \App\Http\Models\bloodstone\MstType;
		$masterfilelist ->name = strtolower($request->name);
		$masterfilelist ->remarks = $request->remarks; 
		$masterfilelist ->alias = $request->alias; 
		$masterfilelist ->created_by = Auth::user()->name;
		$masterfilelist ->updated_by = (Auth::user()->name);
		$masterfilelist ->save();
		$log_msg = $request->name  . " has successfully created from " . ucfirst($mstlist) . " MasterFile" ;
		$masterfilelist = MstType::where('status', '=' , 'enable')->get();//MstBrand::All();	
		}
		$Logger = new AppHelper;
		$Logger->SaveLog($log_msg);
		return redirect('/bloodstone/masterfile/' . $mstlist)->with('MstBrand',$masterfilelist)->with('urllist', $mstlist)->with('return_msg', $return_msg);
		
	} 
	
	
	
	// Batch Create File 
	public function batch(Request $request,$mstlist)
	{	$return_msg ="";
		$log_msg = "";
		if (strtolower($mstlist) == "Mstbrand") //(strtolower($mstlist) == "brand")
		{
			
			
		$data_list = array();
		$display ="" ;
		$data_list = json_decode($request->batchdata, true);
			 //Validate Data
			foreach($data_list["Sheet1"] as $larr_key => $larr_val)		
			{
				$validateMstBrand = MstBrand::where('name',strtolower($larr_val['Name']))->count();		
				
				//echo $validateMstBrand;
				if 	($validateMstBrand > 0)
				{
					$return_msg = $return_msg  .  " Duplicate Entry " . $larr_val['Name'] . ",";
					//break;
				}
			}
			
			
	
			// Save Data		
			if( $validateMstBrand <= 0)
			{	 foreach($data_list["Sheet1"] as $larr_key => $larr_val)		
				{
					//echo $larr_val['Name'];
					$masterfilelist = new \App\Http\Models\bloodstone\MstBrand;
					$masterfilelist ->name = strtolower($larr_val['Name']);
					$masterfilelist ->remarks = $larr_val['Remarks']; 
					$masterfilelist ->created_by = Auth::user()->name;
					$masterfilelist ->updated_by = Auth::user()->name;
					$masterfilelist ->save();
					
				} 
			} // if( $validateMstBrand > 0) */
		
		
		
		$urllist  = "$mstlist";

		$masterfilelist = MstBrand::where('status', '=' , 'enable')->get();//MstBrand::All();
		
		}

		
		$Logger = new AppHelper;
		$Logger->SaveLog($log_msg);
		return redirect('/bloodstone/masterfile/' . $mstlist)->with('MstBrand',$masterfilelist)->with('urllist', $mstlist)->with('return_msg', $return_msg);
		
	} 
	
	public function update(Request $request,$mstlist)
    {	
		if (strtolower($mstlist) == "brand") //(strtolower($mstlist) == "brand")
		{

		$masterfilelist = MstBrand::find($request->id);					
		
		//$masterfilelist = $masterfilelist::find($request->id);;
		$urllist  = "brand";
		}

		else if (strtolower($mstlist) == "department")  {
		$masterfilelist = MstDepartment::find($request->id);					
		$urllist  = "department";	
		}
		else if (strtolower($mstlist) == "model")  {
		$masterfilelist = MstModel::find($request->id);					
		$urllist  = "model";	 
		}
		else if (strtolower($mstlist) == "status")  {
		$masterfilelist = MstStatus::find($request->id);					
		$urllist  = "status";	
		}
		else if (strtolower($mstlist) == "type")  {
		$masterfilelist = MstType::find($request->id);					
		$urllist  = "type";	
		} 
		
		//$masterfilelist = MstBrand::find($request->id);
		
				if($request->action == "edit")
				{
				$log_msg = $masterfilelist ->name . " has been change to " . $request->name . " from " . ucfirst($mstlist) . " MasterFile" ;;
				$masterfilelist ->name = $request->name;
				$masterfilelist ->alias = $request->alias;
				$masterfilelist ->remarks = $request->remarks;
				$masterfilelist ->updated_by = (Auth::user()->name);
				
				
				}
				else if($request->action == "delete")
				{
				$log_msg =  $request->name . " have been deleted from " . ucfirst($mstlist) . " MasterFile" ;
				$masterfilelist ->updated_by = (Auth::user()->name);
				$masterfilelist ->status = "Disabled";
				}
				$masterfilelist->save();
		
		
		
		
		$MasterFileName = '\Mst'.ucfirst($mstlist);
		$Test = array("\App\Http\Models\bloodstone".$MasterFileName);		
		$A = new $Test[0];
		$masterfilelist =  $A->where('status', '=' , 'enable')->get();
		$urllist  = strtolower($mstlist);
		
		

    	
		$Logger = new \AppHelper;
		$Logger->SaveLog($log_msg);
		//return view('bloodstone.masterfile')->with('MstBrand',$masterfilelist)->with('urllist', $urllist);
		return redirect('/bloodstone/masterfile/' . $mstlist)->with('MstBrand',$masterfilelist)->with('urllist', $mstlist)->with('return_msg', $log_msg);
		
	
		
		
    }
	

}