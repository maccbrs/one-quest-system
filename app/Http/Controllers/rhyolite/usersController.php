<?php namespace App\Http\Controllers\rhyolite;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class usersController extends Controller
{

	public function index(){

		$GemUsers = new \App\Http\Models\rhyolite\GemUsers;
		$Users = new \App\Http\Models\rhyolite\Users;
		$items = $Users->get();
		$users = $GemUsers->where('is_representative',0)->where('user_type','!=','administrator')->orderBy('name')->get();
		return view('rhyolite.users.index',compact('users','items'));
	}

	public function create(Request $r){

		if($r->gem_id):
			$Users = new \App\Http\Models\rhyolite\Users;
			$a = explode('||',$r->gem_id);
			$Users->create(['gem_id' => $a[0],'name' => $a[1]]);

			$GemUsers = new \App\Http\Models\rhyolite\GemUsers;
			$gemuser = $GemUsers->find($a[0]);
			$gemuser->access = $this->updateAccess($gemuser->access,'rhyolite');
			$gemuser->save();
		endif;

		return redirect()->back();

	}

	private function updateAccess($access,$new){

		$data = [];
		if($access):
			$data = json_decode($access,true);
		endif;
		$data[] = $new;
		return json_encode($data);
	}

}