<?php namespace App\Http\Controllers\rhyolite;
use Auth;


class Helper 
{

	public function returnValidEmails($arr){

		if($arr):
			$a = [];
			foreach($arr as $b):
				if(filter_var($b, FILTER_VALIDATE_EMAIL)):
					$a[] = $b;
				endif;
			endforeach;
			return $a;
		endif;
		return [];
	}

	public function parseEmailJsonToString($str){
		if($str):
			$arr = json_decode($str,true);
			if(count($arr)):
				return implode(',',$arr);
			endif;
		endif;
		return '';
	}
}
