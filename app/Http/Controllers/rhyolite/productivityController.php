<?php namespace App\Http\Controllers\rhyolite;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Excel;

class productivityController extends Controller
{

	public function index(){

		$DropdownCampaigns = new \App\Http\Models\rhyolite\DropdownCampaigns;
		$lists = $DropdownCampaigns->where('status','=','enable')->get();
		return view('rhyolite.productivity.index',compact('lists'));
	}

	public function addCampaign(){

		$WorkGroup = new \App\Http\Models\rhyolite\WorkGroup;		
		$workgroups = $WorkGroup->select('WorkGroup')->groupBy('WorkGroup')->get();
		return view('rhyolite.productivity.add-campaign',compact('workgroups'));

	}

	public function campaigns(){
		$DropdownCampaigns = new \App\Http\Models\rhyolite\DropdownCampaigns;
		$campaigns = $DropdownCampaigns->where('status','=','enable')->get();
		return view('rhyolite.productivity.campaigns',compact('campaigns'));
	}

	public function updateCic(Request $r,$id){
		$DropdownCampaigns = new \App\Http\Models\rhyolite\DropdownCampaigns;
		$DropdownCampaigns->find($id)->update(['campaigns' => json_encode($r->campaigns)]);
		return redirect()->back();

	}


	public function createCampaign(Request $r){

		$this->validate($r,[ 'title' => 'required','camplist' => 'required']);
		$DropdownCampaigns = new \App\Http\Models\rhyolite\DropdownCampaigns;
		$DropdownCampaigns->create(['title' => $r->title,'campaigns' => json_encode($r->camplist)]);
		return redirect()->back(); 

	}
	
	//Add By Cleo Removing Inactive Campaign Note Its not Deleting Just Tagging as Delete  In Status Column , Value = enabled , disabled , deleted,
	public function deleteCampaign(Request $r)
	{
		if(isset($r->id))
		{
			$DropdownCampaigns = new \App\Http\Models\rhyolite\DropdownCampaigns;
			$DropdownCampaigns_Record = $DropdownCampaigns::find($r->id);		
			$DropdownCampaigns_Record->status = "disabled";
			$DropdownCampaigns_Record->save();
			$Logger = new \AppHelper;
			$Logger->SaveLog("Campaign has been delete by " . Auth::user()->name);
			return back();
		}
		else{
			return back();
		}
		
		
	}

	public function generate(Request $r){

		$this->validate($r,[
			'from' => 'required',
			'to' => 'required',
			'camplist' => 'required'
		]);
		$calldetails = $dailyconntime = $dailycrc = $dispo = [];
		$export = false;
		$cached = false;
		$isEmpty = true;

		$DboCalldetailViw = new \App\Http\Models\rhyolite\DboCalldetailViw;
		$DropdownCampaigns = new \App\Http\Models\rhyolite\DropdownCampaigns;

		$camp = $DropdownCampaigns->find($r->camplist);
		$items = $DboCalldetailViw->whereIn('AssignedWorkGroup',json_decode($camp->campaigns,true))->whereBetween('ConnectedDate',[$r->from,$r->to])->with(['wrapup'])->get();
		$lists = $DropdownCampaigns->get();

		if($items->count()):

			$dispo = $this->dispo($items); 
			$CachedResults = new \App\Http\Models\rhyolite\CachedResults;
			$cached = $CachedResults->create(['contents' => json_encode($items),'user' => Auth::user()->id])->id;
			$export = $cached;

			$isEmpty = false;

			$calldetails = $this->callDetails($items);
			$dailyconntime = $this->dailyConTime($items);
			$dailycrc = $this->dailyCrc($calldetails);

		endif;
		//pre($calldetails);
		return view('rhyolite.productivity.generate',compact('lists','calldetails','dailyconntime','dailycrc','isEmpty','cached','dispo','export'));

	} 

	public function filter(Request $r){

		$this->validate($r,[
			'dispo' => 'required'
		]);
		$calldetails = $dailyconntime = $dailycrc = $dispo = [];
		$DropdownCampaigns = new \App\Http\Models\rhyolite\DropdownCampaigns;
		$lists = $DropdownCampaigns->get();
		$CachedResults = new \App\Http\Models\rhyolite\CachedResults;
		$export = false;

		$cached = $r->cached_id;
		$isEmpty = false;

		$cachedresults = $CachedResults->find($cached);


		if($cachedresults):

			$arr = json_decode($cachedresults->contents);
			$dispo = $this->dispo($arr);

			$items = [];
			foreach($arr as $a):

				$dcode = isset($a->wrapup->WrapupCode)? $a->wrapup->WrapupCode:'';

				if(!in_array($dcode,$r->dispo)):
					$items[] = $a;
				endif;

			endforeach;

			$export = $CachedResults->create(['contents' => json_encode($items),'user' => Auth::user()->id])->id;

			if($items):
				$calldetails = $this->callDetails($items);
				$dailyconntime = $this->dailyConTime($items);
				$dailycrc = $this->dailyCrc($calldetails);
			else:
				$isEmpty = true;
			endif;

			return view('rhyolite.productivity.generate',compact('lists','calldetails','dailyconntime','dailycrc','isEmpty','cached','dispo','export'));

		endif;
		return redirect()->back();

	}

	public function excel($id){

		$CachedResults = new \App\Http\Models\rhyolite\CachedResults;
		$cachedresults = $CachedResults->find($id);

		if($cachedresults):
			$items = json_decode($cachedresults->contents);
			$calldetails = $this->callDetails($items);
			$dailyconntime = $this->dailyConTime($items);
			$dailycrc = $this->dailyCrc($calldetails);
			//pre($calldetails);
		endif;

				ob_clean();
				Excel::create('data', function($excel) use ($dailycrc,$dailyconntime,$calldetails) {

				    $excel->sheet('Sheet1', function($sheet) use ($dailycrc,$dailyconntime,$calldetails) { 

				    	    $sheet->mergeCells('B2:B3');
							$sheet->cell('B2', function($cell){
							    $cell->setValue('CRC');
							    $cell->setBackground('#FFFF99');
							    $cell->setAlignment('center');
							    $cell->setValignment('center');
							});	
				    		$ldates = 'C';
							foreach ($dailyconntime as $dk => $dv):
								$sheet->cell($ldates.'2', function($cell) use ($dv){
								    $cell->setValue($dv['date']);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});
								$sheet->cell($ldates.'3', function($cell) use ($dv){
								    $cell->setValue($dv['day']);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});							
								$ldates++;
							endforeach;

							$dinum = 4;
							foreach ($dailycrc['data'] as $dik => $div):
								$sheet->cell('B'.$dinum, function($cell) use ($dik,$div){
								    $cell->setValue($dik);
								});
								$dinum++;									
							endforeach;

							$crc_n = 4;
							foreach ($dailycrc['data'] as $ck => $cv):
								$crc_l = 'C';
								foreach ($cv as $ck2 => $cv2):
									$sheet->cell($crc_l.$crc_n, function($cell) use ($cv2){
									    $cell->setValue($cv2['count']);
									});
									$crc_l++;
								endforeach;
								$crc_n++;
							endforeach;	

							$second_group = $crc_n + 2;
							$second_group2 = $second_group +1;
				    	    $sheet->mergeCells('B'.$second_group.':B'.$second_group2);
							$sheet->cell('B'.$second_group, function($cell){
							    $cell->setValue('CRC');
							    $cell->setBackground('#FFFF99');
							    $cell->setAlignment('center');
							    $cell->setValignment('center');
							});	


							$ldates2 = 'C';
							foreach ($dailyconntime as $dk => $dv):
								$sheet->cell($ldates2.$second_group, function($cell) use ($dv){
								    $cell->setValue($dv['date']);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});
								$sheet->cell($ldates2.$second_group2, function($cell) use ($dv){
								    $cell->setValue($dv['day']);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});							
								$ldates2++;
							endforeach;

							$dinum2 = $second_group2+1;
							foreach ($dailycrc['data'] as $dik => $div):
								$sheet->cell('B'.$dinum2, function($cell) use ($dik){
								    $cell->setValue($dik);
								});
								$dinum2++;									
							endforeach;	
							
							$crc_n2 = $second_group2+1;
							foreach ($dailycrc['data'] as $ck => $cv):
								$crc_l = 'C';
								foreach ($cv as $ck2 => $cv2):
									$sheet->cell($crc_l.$crc_n2, function($cell) use ($cv2){
									    $cell->setValue(round($cv2['percent'],2).'%');
									    $cell->setAlignment('right');
									});
									$crc_l++;
								endforeach;
								$crc_n2++;
							endforeach;	

							$third_group = $crc_n2 + 2;
							$third_group2 = $third_group + 1;
							$titles = ['Day','Date','Connect Mins','Talk Mins','Mins Billed'];
							$title_l = 'B';																		

							foreach ($titles as $k => $v):
								$sheet->cell($title_l.$third_group2, function($cell) use($v){
								    $cell->setValue($v);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								    $cell->setValignment('center');
								});	
								$title_l++;
							endforeach;

							$third_group3 = $third_group2 +1;
							foreach ($dailyconntime as $dk => $dv):
								$sheet->cell('C'.$third_group3, function($cell) use ($dv){
								    $cell->setValue($dv['date']);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});
								$sheet->cell('B'.$third_group3, function($cell) use ($dv){
								    $cell->setValue($dv['day']);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});	
								$sheet->cell('D'.$third_group3, function($cell) use ($dv){
								    $cell->setValue(round($dv['seconds']/60,2));
								});	
								$sheet->cell('E'.$third_group3, function($cell) use ($dv){
								    $cell->setValue(round($dv['seconds']/60,2));
								});	
								$sheet->cell('F'.$third_group3, function($cell) use ($dv){
								    $cell->setValue(round($dv['seconds']/60,2));
								});																														
								$third_group3++;
							endforeach;

							$fourthGroup = $third_group3 + 2;
							$fourthGroup2 = $third_group3 + 3;
							$agentTitles = ['Agent Name','Call Date','Phone Number','Campaign','Crc','Connect Time'];	
							
							$agent_t = 'B';
							foreach ($agentTitles as $k => $v):
								$sheet->cell($agent_t.$fourthGroup, function($cell) use ($v){
								    $cell->setValue($v);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								    $cell->setValignment('center');								    
								});
								$agent_t++;	
							endforeach;

							foreach ($calldetails as $k=> $v):
								$sheet->cell('B'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue($v['user']);
								});	
								$sheet->cell('C'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue($v['date']);
								});	
								$sheet->cell('D'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue($v['phone']);
								});
								$sheet->cell('E'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue($v['campaign']);
								});	
								$sheet->cell('F'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue($v['dispo']);
								});	
								$sheet->cell('G'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue(round($v['duration']/60,2));
								});	
								
								$fourthGroup2++;																																										
							endforeach;																											

				    });
				})->export('xls');



	}


	private function callDetails($items){

		$data = false;

		foreach($items as $item):
			$dispo = ($item->wrapup?$item->wrapup->WrapupCode:'');
			$data[] = [
				'user' => $item->LocalUserId,
				'date' => $item->ConnectedDate,
				'phone' => $item->RemoteNumber,
				'direction' => $item->CallDirection,
				'campaign' => $item->AssignedWorkGroup,
				'dispo' => $dispo,
				'duration' => roundsix($item->CallDurationSeconds)
			];
		endforeach;

		return $data;

	}

	private function dailyConTime($items){


		$dates = [];
		foreach($items as $k => $item):
			$day = $this->cleanDate($item->ConnectedDate);
			if(isset($dates[$day[0]])):
				$dates[$day[0]]['seconds'] += roundsix($item->CallDurationSeconds);
			else:
				$dates[$day[0]] = [
					'seconds' => roundsix($item->CallDurationSeconds),
					'date' => $day[0],
					'day' => $day[1]
				];
			endif;
		endforeach;

		return $dates;

	}

	private function dailyCrc($items){ 

		$data = $data2 = $data3 = [];
		$dispo = [];
		$dates = [];
		$counts = [];
		$return = [];

		foreach($items as $k => $item):
			$day = $this->cleanDate($item['date']);
			if($item['dispo']):
				if(isset($data[$day[0]][$item['dispo']])):
					$data[$day[0]][$item['dispo']]['count']++;
				else:
					$data[$day[0]][$item['dispo']] = [
						'count' => 1,
						'day' => $day[1],
						'date' => $day[0]
					];
				endif;

				if(!array_key_exists($day[0],$dates)): $dates[$day[0]] = $day[1]; endif;
				if(!in_array($item['dispo'],$dispo)): $dispo[] = $item['dispo']; endif;
				if(isset($count[$day[0]])): $count[$day[0]]++; else: $count[$day[0]] = 1; endif; 

			endif;
		endforeach;

		foreach($data as $k1 => $v1):
			foreach($v1 as $k2 => $v2):

				$v2['percent'] = 0;
				if(isset($count[$v2['date']])):
					$v2['percent'] = ($v2['count']/$count[$v2['date']])*100;
				endif;

				$data2[$k1][$k2] = $v2;
			endforeach;

		endforeach;

		foreach($dispo as $d):
			foreach($data2 as $k1 => $v1):
				$data3[$d][$k1]['count'] = isset($v1[$d]['count'])?$v1[$d]['count']:0;
				$data3[$d][$k1]['percent'] = isset($v1[$d]['percent'])?$v1[$d]['percent']:0;
			endforeach;
		endforeach;

		return [
			'data' => $data3,
			'dispo' => $dispo,
			'dates' => $dates
		];		
	}

	private function cleanDate($date){
		$a = explode('.',$date);
		$b = Carbon::createFromFormat('Y-m-d H:i:s',$a[0])->format('m-d|D');
		return explode('|', $b);
	}

	private function dispo($items){

		$data = [];
		foreach($items as $ia):
			if(isset($ia->wrapup->WrapupCode)):
				if(!in_array($ia->wrapup->WrapupCode,$data)):
					$data[] = $ia->wrapup->WrapupCode;
				endif;
			else:
				if(!in_array('', $data)):
					$data[] = '';
				endif;
			endif;
		endforeach;

		return $data;

	}

}