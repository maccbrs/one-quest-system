<?php namespace App\Http\Controllers\rhyolite;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Excel;

class callsController extends Controller
{

	public function index(Request $r){

		$DropdownCampaigns = new \App\Http\Models\rhyolite\DropdownCampaigns;
		$items = $cached = $dispo= false;

		if(isset($r->camplist)):

			$this->validate($r,[
				'from' => 'required',
				'to' => 'required'
			]);

			$DboCalldetailViw = new \App\Http\Models\rhyolite\DboCalldetailViw;
			$camp = $DropdownCampaigns->find($r->camplist); 
			$items = $DboCalldetailViw->whereIn('AssignedWorkGroup',json_decode($camp->campaigns,true))->whereBetween('ConnectedDate',[$r->from,$r->to])->with(['wrapup'])->get();	
			$dispo = $this->dispo($items);
			$CachedResults = new \App\Http\Models\rhyolite\CachedResults;
			$cached = $CachedResults->create(['contents' => json_encode($items),'user' => Auth::user()->id])->id;		

		endif;

		
		$lists = $DropdownCampaigns->where('status','=','enable')->get();
		//pre($items);
		return view('rhyolite.calls.index',compact('lists','items','cached','dispo')); 
	}

	public function excel($id){

		$CachedResults = new \App\Http\Models\rhyolite\CachedResults;
		$cachedresults = $CachedResults->find($id);
		if($cachedresults):
			$items = json_decode($cachedresults->contents);
		endif;

				ob_clean();
				Excel::create('data', function($excel) use ($items) {

				    $excel->sheet('Sheet1', function($sheet) use ($items) { 

				    		$title = [
				    			'B2' => 'Agent Name',
				    			'C2' => 'Call Date',
				    			'D2' => 'Local Number',
				    			'E2' => 'Remote Number',
				    			'F2' => 'Campaign (Ingroup Name)',
				    			'G2' => 'Disposition',
				    			'H2' => 'Duration (sec)',
				    			'I2' => 'Queue(ms)'
				    		];
				    		foreach($title as $k => $v):
								$sheet->cell($k, function($cell) use ($v){
								    $cell->setValue($v);
								});	
							endforeach;

							$count = 3;
							foreach($items as $item):
								$sheet->cell('B'.$count, function($cell) use ($item){
								    $cell->setValue($item->LocalUserId);
								});
								$sheet->cell('C'.$count, function($cell) use ($item){
								    $cell->setValue($item->ConnectedDate);
								});	
								$sheet->cell('D'.$count, function($cell) use ($item){
								    $cell->setValue($item->DNIS);
								});	
								$sheet->cell('E'.$count, function($cell) use ($item){
								    $cell->setValue($item->RemoteNumber);
								});
								$sheet->cell('F'.$count, function($cell) use ($item){
								    $cell->setValue($item->AssignedWorkGroup);
								});	
								$sheet->cell('G'.$count, function($cell) use ($item){
								    $cell->setValue(($item->wrapup?$item->wrapup->WrapupCode:'-'));
								});	
								$sheet->cell('H'.$count, function($cell) use ($item){
								    $cell->setValue($item->CallDurationSeconds);
								});	
								$sheet->cell('I'.$count, function($cell) use ($item){
								    $cell->setValue($item->tQueueWait);
								});									
								$count++;																			
							endforeach;


				    });
				})->export('xls');		
						
	}

	public function filter(Request $r){

		$this->validate($r,[
			'dispo' => 'required'
		]);

		$DropdownCampaigns = new \App\Http\Models\rhyolite\DropdownCampaigns;
		$CachedResults = new \App\Http\Models\rhyolite\CachedResults;
		$cached = $r->cached_id; 
		$cachedresults = $CachedResults->find($cached);

		if($cachedresults):
			$arr = json_decode($cachedresults->contents);
			$dispo = $this->dispo($arr);

			$items = [];
			foreach($arr as $a):

				$dcode = isset($a->wrapup->WrapupCode)? $a->wrapup->WrapupCode:''; 

				if(!in_array($dcode,$r->dispo)):
					$items[] = $a;
				endif;

			endforeach;

			$cached = $CachedResults->create(['contents' => json_encode($items),'user' => Auth::user()->id])->id;
			$lists = $DropdownCampaigns->get();

			return view('rhyolite.calls.index',compact('lists','items','cached','dispo'));
		endif;

		return redirect()->back();


	}	


	private function dispo($items){

		$data = [];
		foreach($items as $ia):
			if(isset($ia->wrapup->WrapupCode)):
				if(!in_array($ia->wrapup->WrapupCode,$data)):
					$data[] = $ia->wrapup->WrapupCode;
				endif;
			else:
				if(!in_array('', $data)):
					$data[] = '';
				endif;
			endif;
		endforeach;

		return $data;

	}
 
}