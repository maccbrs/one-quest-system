<?php namespace App\Http\Controllers\rhyolite;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DateTimeZone;

class eodController extends Controller
{

	public function index(){

		$Eod = new \App\Http\Models\rhyolite\Eod;
		$WorkGroup = new \App\Http\Models\rhyolite\WorkGroup;
		$Boards = new \App\Http\Models\rhyolite\Boards;
		$help = new \App\Http\Controllers\rhyolite\Helper;
		$boards = $Boards->select('campaign_id','lob')->get();
		$workgroups = $WorkGroup->select('WorkGroup')->groupBy('WorkGroup')->get();
		$items = $Eod->paginate(20);

		/*$timezones = mbTimeZones();
		pre($timezones);*/
		/*pre(Carbon::now()->format('Y-m-d H:i'));*/
		return view('rhyolite.eod.index',compact('items','workgroups','boards','help'));

	}


	public function create(Request $r){ 

		$this->validate($r,[
			'name' => 'required',
			'from' => 'date_format:H:i:s',
			'to' => 'date_format:H:i:s',
			'send_time' => 'date_format:H:i:s',
			'timezone' => 'required'		
		]);

		$Eod = new \App\Http\Models\rhyolite\Eod;
		$input = $r->all();

		$input['cfrom'] = date('Y-m-d', strtotime('-1 day')) .' '. $r['from'];
		$input['cto'] = date('Y-m-d') .' '. $r['to'];
		$input['converted_sendtime'] = date('Y-m-d') .' '. $r['send_time'];
		$input['status'] = 1;
		$Eod->create($input);

		//pre($input);
		return redirect()->back();

	}

	public function update(Request $r,$id){

		$this->validate($r,[
			'from' => 'date_format:H:i:s',
			'to' => 'date_format:H:i:s',
			'send_time' => 'date_format:H:i:s',
			'timezone' => 'required'	
		]);

		$Eod = new \App\Http\Models\rhyolite\Eod;
		$input['cfrom'] = date('Y-m-d', strtotime('-1 day')) .' '. $r['from'];
		$input['cto'] = date('Y-m-d') .' '. $r['to'];
		$input['converted_sendtime'] = date('Y-m-d') .' '. $r['send_time'];
		$Eod->find($id)->update($r->all());
		//pre($r->all());
		return redirect()->back();

	}

	public function updateCic(Request $r,$id){

		$Eod = new \App\Http\Models\rhyolite\Eod;
		$Eod->find($id)->update(['cic_id' => json_encode($r->cic)]);
		return redirect()->back();

	}

	public function updateDboards(Request $r,$id){
		$Eod = new \App\Http\Models\rhyolite\Eod;
		$Eod->find($id)->update(['dummyboard_id' => json_encode($r->dummyboard_id)]);
		return redirect()->back();
	}

	public function updateEmail(Request $r,$id){

		$Eod = new \App\Http\Models\rhyolite\Eod;
		$Helper = new \App\Http\Controllers\rhyolite\Helper;
		$input['email_from'] = json_encode($Helper->returnValidEmails(explode(',',$r->email_from)));
		$input['email_to'] = json_encode($Helper->returnValidEmails(explode(',',$r->email_to)));
		$input['email_cc'] = json_encode($Helper->returnValidEmails(explode(',',$r->email_cc)));
		$Eod->find($id)->update($input);
		return redirect()->back();
	}


}