<?php namespace App\Http\Controllers\lilac;


class generalController 
{

	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}

	public function get_color($opt){

		if($opt):
			$a = json_decode($opt,true);
			if(isset($a['color'])):
				return $a['color'];
			endif;
		endif;
		return '';
	}

	public function put_color($opt,$value){

		$arr = json_decode($opt,true);
		$arr['color'] = $value;
		return json_encode($arr);

	}

	public function put_campaign($opt,$value){

		$arr = json_decode($opt,true);
		$arr['campaign_id'] = $value;
		return json_encode($arr);

	}
	
}
//http://quotes.rest/qod.json?category=inspire

