<?php namespace App\Http\Controllers\lilac;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use \App\Http\Models\gem\GemUserInfo;
use Auth;
use Image;
use Alert;

class userController extends Controller
{

	public function __construct(){

        $this->middleware('central');

    }

	public function index(){

		$User = new \App\Http\Models\lilac\User;
		$help = new \App\Http\Controllers\lilac\generalController;
		$users = $User->paginate(10);

		$Campaign = new \App\Http\Models\lilac\Campaign;
		$campaigns = $Campaign
			->select('campaign_id')
			->get();

		return view('lilac.user.index',compact('users','campaigns','help'));

	}

	public function update(Request $r,$id){ 

		$input = $r->all();
		$User = new \App\Http\Models\lilac\User;
		$help = new \App\Http\Controllers\lilac\generalController;
		$user = $User->find($id);
	
		$input['options'] = $help->put_color($user->options,$r->color);
		$input['options'] = $help->put_campaign($user->options,$r->campaign_id); 
	
		$user->update($input);
		return redirect()->back();

	}

	public function access_edit(Request $r){ 

		$input = $r->all();
		$User = new \App\Http\Models\gem\User;
		
		$userdata = $User->find($r->input("id"));

		$accesslist = json_encode($r->input("access")); 

		$input['access'] = $accesslist;

		$userdata->update($input);
		
		return redirect()->route('gemstone.users.index');

	}

	public function access_create(Request $r){ 

		$input = $r->all();
		$User = new \App\Http\Models\gem\User;

		$avatar = $r->file('avatar');
  	 	$filename  = time() . '.' . $avatar->getClientOriginalExtension();

		$input['password'] = bcrypt($r->input('password'));
		$accesslist = json_encode($r->input("access")); 

		$input['access'] = $accesslist;
		$input['avatar'] = $filename;

		$User->create($input);

  	 	if($r->hasFile('avatar')){
  	 		
            $path = public_path('milestone/images/' . $filename);
            Image::make($avatar->getRealPath())->resize(320, 240)->save($path);

  	 		$user = Auth::user();
  	 		$user->avatar = $filename;
  	 		$user->save();
  	 	}
		
		return redirect()->back();

	}

	public function create(Request $r){

		$help = new \App\Http\Controllers\lilac\generalController;
		$User = new \App\Http\Models\lilac\User;
		$input = $r->all();
		$input['password'] = bcrypt($r->input('password'));
		$options['campaign_id'] = $r->input('campaign_id');
		$options = $help->put_color(json_encode($options),$r->color);
		$input['options'] = $options;
		$User->create($input);
		return redirect()->back();

	} 

	public function empcode_edit(Request $r, $id = null){

		if(!empty($id)){

			$User = new \App\Http\Models\lilac\GemUser;

			$data['emp_code'] = $r->input('emp_code');

			$User->where('id',$id)->update($data);

			return redirect()->back();

		}

	} 

}