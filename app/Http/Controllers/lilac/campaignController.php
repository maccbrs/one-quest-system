<?php namespace App\Http\Controllers\lilac;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class campaignController extends Controller
{

	public function __construct(){

        $this->middleware('central');

    }

	public function index(){

		$Campaign = new \App\Http\Models\lilac\Campaign;
		$help = new \App\Http\Controllers\lilac\generalController;
		$campaigns = $Campaign->paginate(10);


		return view('lilac.campaign.index',compact('campaigns','help'));

	}

	public function update(Request $r,$id){ 

		$input = $r->all();

		$Campaign = new \App\Http\Models\lilac\Campaign;
		$help = new \App\Http\Controllers\lilac\generalController;
		$campaign = $Campaign->find($id);
		$campaign->update($input);
		return redirect()->back();

	}

	public function create(Request $r){

		$help = new \App\Http\Controllers\lilac\generalController;
		$Campaign = new \App\Http\Models\lilac\Campaign;
		$input = $r->all();
		$Campaign->create($input);
		return redirect()->back();

	} 


}