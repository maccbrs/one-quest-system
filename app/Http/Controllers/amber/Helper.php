<?php namespace App\Http\Controllers\amber;
use Auth;


class Helper {

	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}


	public function tls(){

		$GarnetTeamLeads = new \App\Http\Models\amber\GarnetTeamLeads;
		$items = $GarnetTeamLeads->get();
		$data = [];
		foreach($items as $item):
			$data[$item->id] = $item;
		endforeach;
		return $data;
	}

	public function ps(){

		$GarnetProjectSupervisors = new \App\Http\Models\amber\GarnetProjectSupervisors;
		$items = $GarnetProjectSupervisors->get();
		$data = [];
		foreach($items as $item):
			$data[$item->id] = $item;
		endforeach;
		return $data;		

	}

	public function response($n){
		switch ($n) {
			case 0:
				return 'NA';
				break;
			case 1:
				return 'Pending';
				break;
			case 2:
				return 'Approved';
				break;							
			default:
				return 'Rejected';
				break;
		}
	}

	public function rejected($a,$b,$c){
		if($a == 3||$b == 3||$c==3){
			return true;
		}
			return false;
	}

	// Announcement

	public function opened($a){

		$GarnetOpened = new \App\Http\Models\amber\GarnetOpened;
		$items = $GarnetOpened->self()->get();

		$opened = false;

		if($items->count()):
			foreach($items as $k => $v):
				$opened[] = $v->announcements_id;
			endforeach;
		endif;

		$return = [];

		if($a->count()):
			foreach($a as $k => $v):
				if($opened):
					
					if(in_array($v->id, $opened)):
						//print_r('<pre>');print_r($v->id);print_r('</pre>'); exit;
						$return[$v->source]['read'] = (isset($return[$v->source]['read'])?$return[$v->source]['read'] + 1:$return[$v->source]['read'] = 1);
					else:
					
						$return[$v->source]['unread'] = (isset($return[$v->source]['unread'])?$return[$v->source]['unread'] + 1:$return[$v->source]['unread'] = 1);
					endif;
				else:
					
					$return[$v->source]['unread'] = (isset($return[$v->source]['unread'])?$return[$v->source]['unread'] + 1:$return[$v->source]['unread'] = 1);
				endif;
			endforeach;
		endif;

		return $return;

	}

	public function opened_message($a){

		$GarnetOpened = new \App\Http\Models\amber\GarnetMessageOpened;
		$items = $GarnetOpened->self()->get();


		$opened = false;

		if($items->count()):
			foreach($items as $k => $v):
				$opened[] = $v->message_id;
			endforeach;
		endif;

		$return = [];

		if($a->count()):
			foreach($a as $k => $v):
				if($opened):
					
					if(in_array($v->id, $opened)):
						
						$return['read'] = (isset($return['read'])?$return['read'] + 1:$return['read'] = 1);
					else:
					
						$return['unread'] = (isset($return['unread'])?$return['unread'] + 1:$return['unread'] = 1);
					endif;
				else:
					
					$return['unread'] = (isset($return['unread'])?$return['unread'] + 1:$return['unread'] = 1);
				endif;
			endforeach;
		endif;

		return $return;

	}

	public function user_type(){


		$a = [
			'Administrator' => 'noc',
			'Human Resource' => 'hrd'
		];
		$b = Auth::user()->user_type;
		if(isset($a[$b])):
			return $a[$b];
		endif;

		return 'user';
	}

	public function print_status_message($a,$source,$status){
		
		if($a):
			if(isset($a) && isset($a[$status])):
				
				return $a[$status];
			endif;
		endif;

		return 0;

	}

	public function print_status($a,$source,$status){

		if($a):
			if(isset($a[$source]) && isset($a[$source][$status])):
				return $a[$source][$status];
			endif;
		endif;

		return 0;

	}

	public function fetchComments($str){
		if($str != ''):
			$obj = json_decode($str,true);
			return $obj['notes'];
		endif;
	}

	// End of Announcement


} 