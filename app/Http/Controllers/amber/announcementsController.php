<?php namespace App\Http\Controllers\amber;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class announcementsController extends Controller
{

	public function listz($source,$status){

		$GarnetOpened = new \App\Http\Models\amber\GarnetOpened;
		$GarnetAnnouncements = new \App\Http\Models\amber\GarnetAnnouncements;

		$help = new \App\Http\Controllers\amber\Helper;
		$items = $GarnetOpened->self()->get();
		$ids = [];

		if($items->count()):
			foreach($items as $k => $v):
				$ids[] = $v->announcements_id;
			endforeach;				
		endif;

		switch ($status):
			case 'read':
				$announcements = $GarnetAnnouncements->source($source)->where('status','<>',0)->whereIn('id',$ids)->whereIn('destination',['all',$help->user_type()])->orderBy('created_at','desc')->get(); 
				break;
			case 'unread':
				$announcements = $GarnetAnnouncements->source($source)->where('status','<>',0)->whereNotIn('id',$ids)->whereIn('destination',['all',$help->user_type()])->orderBy('created_at','desc')->get();
				break;			
			default:
				$announcements = $GarnetAnnouncements->source($source)->where('status','<>',0)->whereIn('destination',['all',$help->user_type()])->orderBy('created_at','desc')->get();
				break;
		endswitch;

		//$help->pre($items);
		
		return view('amber.announcements.lists',compact('announcements','help'));


	}

	public function lizt($id){

		$GarnetAnnouncements = new \App\Http\Models\amber\GarnetAnnouncements;
		$GarnetOpened = new \App\Http\Models\amber\GarnetOpened;
		$item = $GarnetAnnouncements->find($id);
		$GarnetOpened->firstOrCreate(['user_id' => Auth::user()->id,'announcements_id' => $id]);

		return view('amber.announcements.list',compact('item'));

	}

}