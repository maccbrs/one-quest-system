<?php namespace App\Http\Controllers\amber;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use DB;

class qandaController extends Controller
{

	public function index($id){

		$help = new \App\Http\Controllers\amber\Helper;
		$GarnetQanda = new \App\Http\Models\amber\GarnetQanda;
		$items = $GarnetQanda->where('dept_to',$id)->where('userid_from',Auth::user()->id)->with(['user'])->paginate(20);
		return view('amber.qanda.index',compact('id','items'));


	}

	public function create(Request $r){

	    $this->validate($r, [
	        'inquiry' => 'required'
	    ]);

		$help = new \App\Http\Controllers\amber\Helper;
		$GarnetQanda = new \App\Http\Models\amber\GarnetQanda;
		$data = $r->all();
		$data['userid_from'] = Auth::user()->id;
		$GarnetQanda->create($data);
		return redirect()->back();

	}



}