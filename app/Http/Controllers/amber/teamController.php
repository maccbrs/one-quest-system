<?php namespace App\Http\Controllers\amber;

use App\Http\Controllers\Controller;
use App\Http\Models\amber\GarnetAgents;
use App\Http\Requests;
use \App\Http\Models\amber\GarnetMessages;
use \App\Http\Models\amber\GemUser;
use \App\Http\Models\amber\GarnetGroups;
use \App\Http\Models\amber\GarnetTeamLeads;
use \App\Http\Models\amber\GarnetProjectSupervisors;
use Illuminate\Http\Request;
use Auth;

class teamController extends Controller
{

	public function manager(){

		$all_users = new GemUser;
		$all_garnet = new GarnetAgents;
		$program_sup_garnet = new GarnetProjectSupervisors;

		$pro_sup = array();
		$messages_list = array();
		
		$userId = Auth::user()->id;
		$garnet_details = $all_garnet->get_superiors($userId);

		$users_list = $all_users
			->pluck('name','id');

		$pro_sup = $program_sup_garnet
			->select('id','first_name', 'last_name', 'user_id')
			->where('id',$garnet_details['ps_id'])
			->first();

		if(!empty($pro_sup)){

			$messages = new GarnetMessages;

			$messages_list = $messages->get_manager_message($pro_sup['user_id'],$userId);


		}else{

			$pro_sup = null;

		}

		return view('amber.team.manager',compact('userId','users_list','team_lead','pro_sup','messages_list','user_name'));

	}


	public function team_leader(){

		$all_users = new GemUser;
		$all_garnet = new GarnetAgents;
		$teamleads_garnet = new GarnetTeamleads;

		$messages_list = array();
		$team_lead = array();

		$userId = Auth::user()->id;
		$garnet_details = $all_garnet->get_superiors($userId);

		$users_list = $all_users
			->pluck('name','id');

		$team_lead = $teamleads_garnet
			->select('id','first_name', 'last_name', 'user_id')
			->where('id',$garnet_details['tl_id'])
			->first();

		if(!empty($team_lead)){
		

			$messages = new GarnetMessages;

			$messages_list = $messages->get_tl_message($team_lead['user_id'],$userId);


		}else{

			$team_lead = null;
		}

		return view('amber.team.team_leader',compact('userId','users_list','team_lead','pro_sup','messages_list','user_name'));
	}

	public function mail($dept = null){

		$all_users = new GemUser;
		$all_garnet = new GarnetAgents;
		$messages = new GarnetMessages;
		$noc_users = null;
	
		$user = Auth::user();

		$userId = $user->id;
		$userType = $user->user_type;

		if($dept == 'Human Resource'){

			$dept = 'hr';

		}

		$users_list = $all_users
			->pluck('name','id');

		$magellan_users = $all_users->where('user_type','=',$dept)->get();

		$message_data = $messages->get_message($userId);

		foreach ($message_data as $key => $value) {

			$messages_list[$value['to']][] = $value;
			$messages_list[$value['user']][] = $value;

		}

		if($dept == 'Administrator'){

			$dept = 'NOC';

		}

		return view('amber.team.mail',compact('userId','users_list','magellan_users','messages_list','dept'));
	}

	public function test(){



		return 'success';
	}


	public function team_mates(){

		$all_users = new GemUser;
		$all_garnet = new GarnetAgents;
		$user_groups = new GarnetGroups;
		$messages = new GarnetMessages;

		$all_agents = null;
		$all_groups = array();
		$message_collection = array();
		
		$userId = Auth::user()->id;
		$userType = Auth::user()->user_type;
		$user_name = Auth::user()->name;

		$all_groups_raw = $user_groups
			->where('status','=','1')
			->orderBy('id','desc')
			->get();

		foreach ($all_groups_raw as $key => $value) {

			$members = json_decode($value['members']);
			$members_list[$value['id']] = json_decode($value['members']);


			if (in_array($userId, $members)) {

			   $all_groups[$key] = $value;
			   $all_groups[$key]['members_list'] = $members;
			   $message_collection []= $value['id'];

			}
		}

		$superior_data = $all_users->garnet($userId,$userType);

		$all_agents = $all_garnet->get_agent($superior_data,$userType);


		$non_agent = $all_users
			->select('id','name')
			->whereNotIn('user_type',['Human Resource','Team Leader','Manager'])
			->get();

		$human_resource = $all_users
			->select('id','name')
			->whereIn('user_type',['Human Resource'])
			->get();

		$users_list = $all_users
			->pluck('name','id');

		$messages_data = $messages
			->whereIn('to',$message_collection)
			->where('page','group')
			->orderBy('created_at','desc')
			->get();

		foreach ($messages_data as $key => $value) {

			$messages_data[$key]['content'] = json_decode($value['content']);
			$messages_list[$value['to']][] = $messages_data[$key];
			
		}

		return view('amber.team.team_mates',compact('userId','users_list','team_lead','pro_sup','messages_list','user_name','all_agents','all_groups','members_list','non_agent','userType','human_resource'));


	}

	public function new_message(Request $r){


		$messages = new GarnetMessages;
		

		if(!empty($r->input())){

			$message_content['content'] = $r->input("content");
			$content_holder = json_encode($message_content);

			$messages['user'] = $r->input("user");
			$messages['subject'] = $r->input("subject");
			$messages['content'] =$content_holder;
			$messages['to'] = $r->input("to");
			$messages['type'] = $r->input("type");
			$messages['status'] =$r->input("status");
			$messages['page'] =$r->input("page");
	
			$messages->save();

			$msg = 'Dispute Date has been Saved!';

		}

	return back();
	}

}