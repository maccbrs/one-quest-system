<?php namespace App\Http\Controllers\amber;

use App\Http\Controllers\Controller;
use App\Http\Models\amber\GarnetAgents;
use App\Http\Requests;
use \App\Http\Models\amber\GarnetDeptNotes;
use \App\Http\Models\amber\GemUser;
use \App\Http\Models\amber\GarnetGroups;
use \App\Http\Models\amber\GarnetTeamLeads;
use \App\Http\Models\amber\GarnetProjectSupervisors;
use Illuminate\Http\Request;
use Auth;

class notesController extends Controller
{

	public function index($dept = null){

		$all_users = new GemUser;
		$all_garnet = new GarnetAgents;
		$teamleads_garnet = new GarnetTeamleads;
		$program_sup_garnet = new GarnetProjectSupervisors;

		$pro_sup = array();
		$note_list = array();
		$note_data = array();

		$userId = Auth::user()->id;
		$dept  = $dept;

		$garnet_details = $all_garnet->get_superiors($userId);

		$user_name = Auth::user()->name;

		$users_list = $all_users
			->pluck('name','id');

		$pro_sup = $program_sup_garnet
			->select('id','first_name', 'last_name', 'user_id')
			->where('id',$garnet_details['ps_id'])
			->get();


		if(!empty($pro_sup)){

			$notes = new GarnetDeptNotes;

			$note_list = $notes
				->where('to', $userId )
				->where('to', $userId )
				->orderBy('created_at','desc')
				->get();

			if(!empty($note_list)){

				foreach ($note_list as $key => $value) {
					
					$note_list[$key]['content'] = json_decode($value['content']);
					$note_list[$key]['header'] = json_decode($value['header']);
					$note_list[$key]['type'] = json_decode($value['type']);
					$note_list[$key]['status'] = json_decode($value['status']);

				}
			}


		}else{

			$pro_sup = null;

		}

		return view('amber.notes.noc',compact('userId','users_list','team_lead','pro_sup','note_list','user_name','dept'));

	}

}