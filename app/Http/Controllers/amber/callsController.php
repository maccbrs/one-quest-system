<?php namespace App\Http\Controllers\amber;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use DB;

class callsController extends Controller
{

	public function recordings($bound){

		$help = new \App\Http\Controllers\amber\Helper;

		if($bound == 'inbound'):
			$Rec = new \App\Http\Models\amber\RubyRecordingsCloser;
		else:
			$Rec = new \App\Http\Models\amber\RubyRecordings;
		endif;

		$items = $Rec->where('agent','aroman')->orderBy('call_date','desc')->paginate(10);
		
		return view('amber.calls.recordings',compact('help','items'));


	}

	public function qa($bound){
		$help = new \App\Http\Controllers\amber\Helper;

		if($bound == 'inbound'):
			$Rec = new \App\Http\Models\amber\RubyRecordingsCloser;
		else:
			$Rec = new \App\Http\Models\amber\RubyRecordings;
		endif;

		$items = $Rec->where('agent','aroman')->where('locked',1)->orderBy('call_date','desc')->paginate(10);
		
		return view('amber.calls.qa',compact('help','items'));		
	}

}