<?php namespace App\Http\Controllers\amber;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Alert;

class disputeController extends Controller
{

	public function index(){


		$OnyxDisputes = new \App\Http\Models\amber\OnyxDisputes;
		$items = $OnyxDisputes
			->where('user',Auth::user()->id)
			->where('status',1)
			->orderBy('created_at')
			->orderBy('tl_response')
			->with('period')
			->with('issueObj')
			->get();

		$help = new \App\Http\Controllers\amber\Helper;

		$DisputeDates = new \App\Http\Models\opal\disputeDates;
		//$dispute_period = $DisputeDates->select('dispute_label','id','status')->get();
		//$dispute_period = $DisputeDates->pluck('dispute_label','id'); 

		$DisputeIssues = new \App\Http\Models\opal\disputeIssues;
		// $issue_list = $DisputeIssues->select('issue_name','id','status')->get(); 
		//$issue_list = $DisputeIssues->pluck('issue_name','id');

		$issues_pluck = $DisputeIssues->where('status',1)->pluck('issue_name','id');
		$date_pluck = $DisputeDates->where('status',1)->pluck('dispute_label','id');

		//$help->pre($items);
		return view('amber.dispute.index',compact('items','issue_list','dispute_period','help','issues_pluck','date_pluck'));
		
	}

	public function create(Request $r){

		$help = new \App\Http\Controllers\amber\Helper;
		$OnyxDisputes = new \App\Http\Models\amber\OnyxDisputes;

		$data = $r->all();
		$data['user'] = Auth::user()->id;
		$data['dispute_date'] = date("Y-m-d", strtotime($data['dispute_date']));
		// $data['dispute_time'] = date("H:i", strtotime($data['dispute_time']));
		
		//$help->pre($data);

		$OnyxDisputes->create($data);	

		return redirect()->back();

	}	

	public function update(Request $r){
		$help = new \App\Http\Controllers\amber\Helper;
		$OnyxDisputes = new \App\Http\Models\amber\OnyxDisputes;

		if($r->input('tl_response') == 1){

			$date = $r->all();

			$data['dispute_date'] = date("Y-m-d", strtotime($date['dispute_date']));
			$data['issue'] =  $r->input('issue_name');
			$data['dispute_period'] =  $r->input('dispute_period');
			$data['notes'] = $r->input('notes');
			$OnyxDisputes->where('id', $r->input('id'))->update($data);	

			$help->pre($data);

			Alert::message('Your Dispute File has been Updated.')->persistent('Close');
		}

		return view('amber.dispute.index',compact('items','issue_list','dispute_period','help','issues_pluck','date_pluck'));

	}	

	public function delete(Request $r){

		
		$OnyxDisputes = new \App\Http\Models\amber\OnyxDisputes;

		if($r->input('tl_response') == 1){

			$data['status'] =  0;
			$OnyxDisputes->where('id', $r->input('id'))->update($data);	

			Alert::message('Your Dispute File has been Deleted.')->persistent('Close');

		}

		return redirect()->back();

	}	


}