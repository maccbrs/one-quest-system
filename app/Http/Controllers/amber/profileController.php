<?php namespace App\Http\Controllers\amber;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Image;
use File;
use Alert;
use Hash;

class profileController extends Controller
{

	public function picture(){

		return view('amber.profile.picture');
	}

	public function update_pic(Request $r){



		$help = new \App\Http\Controllers\amber\Helper;
		$GemUser = new \App\Http\Models\amber\GemUser;
		$user = $GemUser->find(Auth::user()->id);
	
		if($user):

	        $image = $r->file('file');
	        $filename  = time() . '.' . $image->getClientOriginalExtension();
	        $sm = public_path('uploads/sm-' . $filename);
	        $m = public_path('uploads/m-' . $filename);
	        $l = public_path('uploads/l-' . $filename);

	        \Image::make($image->getRealPath())->resize(50, 50)->save($sm);
	        \Image::make($image->getRealPath())->resize(100, 100)->save($m);
	        \Image::make($image->getRealPath())->resize(200, 200)->save($l);	
	        $this->unlink($user->avatar);
	        
	        $user->avatar = $filename;
	        $user->save();

		endif;

		return redirect()->back();

	}

	private function unlink($fn){
        $sm = 'uploads/sm-'.$fn;
        $m = 'uploads/m-'.$fn;
        $l = 'uploads/l-'.$fn;
        File::Delete($sm);
        File::Delete($m);
        File::Delete($l);
        return true;		
	}

	public function password(Request $r){

		$GemUser = new \App\Http\Models\amber\GemUser;
		$self_data = $GemUser->where('id','=',Auth::user()->id)->first();

		$UserPassword = new \App\Http\Models\amber\GemUsersPassword;

		$data['password'] = bcrypt($r['new_password1']);

		$pw['password'] = $r['new_password1'];
		$pw['name'] = $self_data['name'];
		$pw['gem_id'] = Auth::user()->id;


		if(Hash::check($r['current_password'],$self_data['password'])) {

			$user = $GemUser->where('id','=',Auth::user()->id)->update($data);

			$pass = $UserPassword->where('gem_id','=',Auth::user()->id)->first();
			

			if(!empty($pass)){

				$user = $UserPassword->where('gem_id','=',Auth::user()->id)->update($pw);
	
			}else{

				$UserPassword->create($pw);
				
			}

			Alert::message('Password has been Updated!')->persistent('Close');

			return $this->picture();

		}else{

			Alert::error('Sorry. Current Password doesnt match our record.')->persistent('Close');
		}

		return $this->picture();

	}	

}