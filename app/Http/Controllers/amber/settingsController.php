<?php namespace App\Http\Controllers\amber;

use App\Http\Controllers\Controller;
use App\Http\Controllers\amber\Helper;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Models\gem\GemUserInfo;
use Auth;
use Image;
use File;

class settingsController extends Controller
{

	public function index(Request $r){
	
		$help = new Helper;
		$request = $r->all();
		$user = Auth::user();

		$user_info = new GemUserInfo;

		$user_data = $user_info
			->where('user_id','=',$user->id)
			->orderby('id','desc')
			->with('applicant')
			->first();
		
		if(!empty($user_data->applicant['additional'])){

			$user_data['applicant'] = json_decode($user_data->applicant['additional']);

		}

		$user_data['information'] = json_decode($user_data['information']);
		
		return view('amber.settings.index',compact('user','user_data','request','help'));
	}

	public function update_image(Request $r){

		$help = new \app\Http\Controllers\amber\Helper;
		$GemUser = new \App\Http\Models\amber\GemUser;
		$user = $GemUser->find(Auth::user()->id);

		if($user):

	        $image = $r->file('file');
	        $filename  = time() . '.' . $image->getClientOriginalExtension();
	        $sm = public_path('uploads/sm-' . $filename);
	        $m = public_path('uploads/m-' . $filename);
	        $l = public_path('uploads/l-' . $filename);
	        \Image::make($image->getRealPath())->resize(50, 50)->save($sm);
	        \Image::make($image->getRealPath())->resize(100, 100)->save($m);
	        \Image::make($image->getRealPath())->resize(200, 200)->save($l);	
	        $this->unlink($user->avatar);
	        $user->avatar = $filename;
	        $user->save();

		endif;

		return redirect()->back();

	}

	private function unlink($fn){
        $sm = 'uploads/sm-'.$fn;
        $m = 'uploads/m-'.$fn;
        $l = 'uploads/l-'.$fn;
        File::Delete($sm);
        File::Delete($m);
        File::Delete($l);
        return true;		
	}

	public function add_info(Request $r){ 

		$user_info = new GemUserInfo;
		$information['header'] = $r->input("header");
		$information['content'] = $r->input("content");

		$information = json_encode($information);

		$input['information'] = $information;
		$input['user_id'] = $r->input('user_id');
		$user_info->updateOrCreate($input);

		return back();
	}

	public function motto(Request $r){ 

		$user_info = new GemUserInfo;
		$motto = $r->input("motto");

		$input['motto'] = $motto;
		$input['user_id'] = $r->input("user_id");
		$user_info->updateOrCreate($input);

		return back();
	}
}