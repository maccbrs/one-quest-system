<?php namespace App\Http\Controllers\amber;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class youController extends Controller
{

	public function info(){

		$GarnetAgents = new \App\Http\Models\amber\GarnetAgents;
		$help = new \App\Http\Controllers\amber\Helper;
		$self = $GarnetAgents->self()->first();

		return view('amber.you.info',compact('help','self'));
	}

}