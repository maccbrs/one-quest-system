<?php namespace App\Http\Controllers\amber;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class dashboardController extends Controller
{

	public function index(){

		$GarnetAnnouncements = new \App\Http\Models\amber\GarnetAnnouncements;
		$GarnetMessages = new \App\Http\Models\amber\GarnetMessages;
		$help = new \App\Http\Controllers\amber\Helper;
		$GarnetGroups = new \App\Http\Models\amber\GarnetGroups;

		$user_id = Auth::user()->id;
		$groups = $GarnetGroups->get();
		$group_list = array();

		foreach ($groups as $key => $value) {

			$member = json_decode($value['members']);

			if(in_array($user_id, $member)){
				$group_list[] = $value['id'];
			}
			
			
		}

		$message = $GarnetMessages->whereIn('to',[$user_id])->whereNotIn('page',['group']);

		$message2 = $GarnetMessages->whereIn('to',$group_list)->whereIn('page',['group']);

		$all_message = $message2
            ->union($message)
            ->get();

		$items = $GarnetAnnouncements->whereIn('destination',['all',$help->user_type()])->where('status',1)->get();

		$statuses = $help->opened($items); 
		
		$statuses_message = $help->opened_message($all_message);

		$created_message = $GarnetMessages->where('to','=',$user_id)->where('status',1)->where('page','=','agents')->first();

		$created_noc = $GarnetAnnouncements->whereIn('destination',['all',$help->user_type()])->where('status',1)->where('source','=','noc')->orderBy('created_at','desc')->first();

		$created_hrd = $GarnetAnnouncements->whereIn('destination',['all',$help->user_type()])->where('status',1)->where('source','=','hrd')->orderBy('created_at','desc')->first();

		$created_qa = $GarnetAnnouncements->whereIn('destination',['all',$help->user_type()])->where('status',1)->where('source','=','qa')->orderBy('created_at','desc')->first();

		$latest_announcement = $GarnetAnnouncements->where('destination','=','all')->orderBy('created_at','desc')->first();

		$test = time();
		//$help->pre(date("Y-m-d h:t:sa",$test));
		return view('amber.dashboard.index',compact('latest_announcement','created_message','created_noc','created_hrd','created_qa','items','statuses','help','statuses_message','user_id'));
	}

}