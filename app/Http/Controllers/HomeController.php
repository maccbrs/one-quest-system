<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function testxxxxxxxx(){ die;
        $helper = new \App\Http\Controllers\gem\Helper;
        $TestA = new \App\Http\Models\test\TestA;

        Excel::load('uploads/test.xlsx', function($reader) use($helper,$TestA) {

            $results = $reader->get();
            $data = [];
 //$helper->pre($results);
            foreach($results as $r):

                if(is_object($r['date'])):
                    $date = $r['date']->format('Y-m-d');
                else:
                    $date = $r['date'];
                endif;

                if(is_object($r['follow_up_date'])):
                    $fdate = $r['follow_up_date']->format('Y-m-d');
                else:
                    $fdate = $r['follow_up_date'];
                endif;               

                if($r['phone_number'] != ''):
                $data[] = [
                    'date' => $date,
                    'agents_name' => $r['agents_name'],
                    'clients_name' => $r['clients_name'],
                    'phone_number' => $r['phone_number'],
                    'store_name' => $r['store_name'],
                    'shipping_address' => $r['shipping_address'],
                    'follow_up_date' => $fdate,
                    'remarks' => $r['remarks'],
                    'notes' => $r['notes']
                ];
                endif;
                
            endforeach;
            $TestA->insert($data);
            $helper->pre($data);
        });
    }

    public function test(){ die;
        $helper = new \App\Http\Controllers\gem\Helper;
        $TestA = new \App\Http\Models\test\TestA;
        $TestB = new \App\Http\Models\test\TestB;
        $agent = [
        'Alvin Ballesteros' => 'aballesteros',
        'Ceva Santos' => 'csantos',
        'Michelle Alano' => 'malano',
        'Ryan' => 'rleyva',
        'ivan' => 'ideguzman'
        ];

        $data = [];
        $cont = [];
        $phones = [];

        $items = $TestA->get();

        foreach($items as $i):

            $randnum = mt_rand(111111111,999999999);
            $cont = [
                'lead_id'=>$randnum,
                'campaign_id'=>'smokescreen',
                'user'=> $agent[trim($i->agents_name)],
                'session_id'=>'',
                'fullname'=> $i->agents_name,
                'BusinessName'=> $i->store_name,
                'OwnerName'=> $i->clients_name,
                'title'=>'Owner',
                'phone'=> $i->phone_number,
                'address1'=> $i->shipping_address,
                'city'=>'',
                'state'=>'',
                'postal_code'=>'',
                'Disposition'=>'',
                'Notes'=> $i->notes,
                'SQLdate'=>$i->date
            ];


            $data[] = [
                'lead_id' => $randnum,
                'campaign_id' => 'smokescreen',
                'user' => $agent[trim($i->agents_name)],
                'content' => json_encode($cont),
                'lists' => '{}',
                'created_at' => $i->date,
                'updated_at' => $i->date
            ];
        endforeach;

        $TestB->insert($data);


    }    

}
