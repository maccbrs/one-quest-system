<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('agate.dashboard.index'));
});

Breadcrumbs::register('agate.agents.index', function($breadcrumbs )
{	
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Agent ', route('agate.agents.index'));
});

Breadcrumbs::register('agate.agents.campaigns', function($breadcrumbs, $bound,$campaign,$user)
{       
    $breadcrumbs->parent('agate.agents.index' , $bound);
    $breadcrumbs->push('Assigned Campaign',url('agate/agents/campaigns/' .$bound."/".$campaign."/".$user));
});

Breadcrumbs::register('agate.agents.campaign', function($breadcrumbs, $bound, $user, $id)
{   
    $breadcrumbs->parent('agate.agents.campaigns', $bound, $user, $id);
    $breadcrumbs->push('Calls',url('agate/agents/campaign/'.$bound."/".$user."/".$id));
}); 

Breadcrumbs::register('agate.audits.audit', function($breadcrumbs, $bound,$item)
{   
    $breadcrumbs->parent('agate.agents.campaign',$bound, $item->agent,$item->campaign_id);
    $breadcrumbs->push('Audit',url('agate/audits/audit/'.$bound."/".$item->id));
}); 

Breadcrumbs::register('agate.search.index', function($breadcrumbs)
{	
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Search',url('agate.search.index'));
});

Breadcrumbs::register('agate.campaigns.index', function($breadcrumbs)
{	
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Campaign ', route('agate.campaigns.index'));
});

Breadcrumbs::register('agate.randomizer.index', function($breadcrumbs )
{	
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Campaign List ', route('agate.randomizer.index'));
}); 

Breadcrumbs::register('agate.campaigns.users', function($breadcrumbs,$bound,$campaign )
{   
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Agent List ', url('agate/campaigns/users/'.$bound."/".$campaign));
}); 

Breadcrumbs::register('agate.campaigns.user', function($breadcrumbs,$bound,$campaign,$id )
{   
    $breadcrumbs->parent('agate.campaigns.users',$bound,$campaign);
    $breadcrumbs->push('Call List ', url('agate/campaigns/user/'.$bound."/".$campaign."/".$id));
});

Breadcrumbs::register('agate.archives.index', function($breadcrumbs )
{	
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Archives ', route('agate.archives.index'));
});

Breadcrumbs::register('agate.metrics.parameters', function($breadcrumbs )
{	
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Metrics Parameters ', route('agate.metrics.parameters'));
});

Breadcrumbs::register('agate.metrics.keyfactors', function($breadcrumbs )
{	
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Metrics Key Factors ', route('agate.metrics.keyfactors'));
});

Breadcrumbs::register('agate.metrics.template-edit-keyfactor', function($breadcrumbs)
{	
	$breadcrumbs->parent('agate.metrics.keyfactors');
    $breadcrumbs->push('Edit Key Factor ' , url('agate.metrics.template-edit-keyfactor'));
});

Breadcrumbs::register('agate.metrics.templates', function($breadcrumbs )
{	
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Metrics Templates ', route('agate.metrics.templates'));
});

Breadcrumbs::register('agate.audits.index', function($breadcrumbs ,$id , $set_id )
{   
    $breadcrumbs->parent('agate.set.index', $set_id);
    $breadcrumbs->push('Details ', route('agate.audits.index', $id ));
});

Breadcrumbs::register('agate.metrics.edit-template', function($breadcrumbs, $id )
{	
	$breadcrumbs->parent('agate.metrics.templates');
    $breadcrumbs->push('Edit - ' .ucfirst($id->title), route('agate.metrics.edit-template',$id->title));
});

Breadcrumbs::register('agate.settings.campaigns', function($breadcrumbs)
{	
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Settings - Campaign' , route('agate.settings.campaigns'));
});

Breadcrumbs::register('agate.settings.deactivate', function($breadcrumbs)
{	
	$breadcrumbs->parent('home');
    $breadcrumbs->push('Deactivate' , url('agate.settings.deactivate'));
});

Breadcrumbs::register('agate.set.index', function($breadcrumbs, $id)
{	
	$breadcrumbs->parent('agate.randomizer.index');
    $breadcrumbs->push('Audit' , route('agate.set.index',$id));
});

Breadcrumbs::register('agate.settings.edit-campaign', function($breadcrumbs, $id)
{   
    $breadcrumbs->parent('agate.settings.campaigns');
    $breadcrumbs->push('Edit ' , route('agate.settings.edit-campaign',$id));
});

Breadcrumbs::register('agate.custom.add', function($breadcrumbs)
{   
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Custom ' , route('agate.custom.add'));
});

Breadcrumbs::register('agate.custom.audit', function($breadcrumbs, $id)
{   
    $breadcrumbs->parent('agate.custom.add');
    $breadcrumbs->push('Create ' , route('agate.custom.audit', $id));
});


