<?php namespace App\Http\Models\moonstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemTestCalls extends Model
{

   protected $connection = 'gem';
   protected $table = 'test_calls';
   protected $fillable = ['created_at','updated_at','total','passed','failed'];

   public function obj_testcalls(){
   		return $this->hasMany('App\Http\Models\moonstone\GemTestCallItems','test_calls_id')->with(['obj_did']);
   }  

} 