<?php namespace App\Http\Models\topaz;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'topaz';
    protected $table = 'campaign';   
    protected $fillable = ['name','created_at','updated_at'];

    
}