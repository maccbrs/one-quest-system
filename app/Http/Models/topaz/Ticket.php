<?php namespace App\Http\Models\topaz;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'topaz';
    protected $table = 'ticket';   
    protected $fillable = ['title','content_id','user_id','campaign_id','assigned_id','point_id','status','created_at','updated_at'];

    public function content(){
      return $this->hasOne('App\Http\Models\topaz\Content','id','content_id');
    }  

    public function user(){
      return $this->hasOne('App\Http\Models\topaz\GemUsers','id','user_id');
    }    

    public function reply(){
      return $this->hasMany('App\Http\Models\topaz\Reply','ticket_id','id')->with(['content','user']);
    }  

} 