<?php namespace App\Http\Models\topaz;

use Illuminate\Database\Eloquent\Model;
use DB;

class GemUsersPassword extends Model
{

    protected $connection = 'gem';
    protected $table = 'users_passwords';   
    protected $fillable = ['name', 'password','gem_id'];

}