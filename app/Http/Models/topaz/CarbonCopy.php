<?php namespace App\Http\Models\topaz;

use Illuminate\Database\Eloquent\Model;

class CarbonCopy extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'topaz';
    protected $table = 'carbon_copy';   
    protected $fillable = ['ticket_id','user_id','status','created_at','updated_at'];
 
    public function ticket(){
      return $this->belongsTo('App\Http\Models\topaz\Ticket','ticket_id','id')->with('reply','content');
    } 
 
    public function user(){
        return $this->hasOne('App\Http\Models\topaz\GemUsers','id','user_id');
    }
}