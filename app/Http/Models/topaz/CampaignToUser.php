<?php namespace App\Http\Models\topaz;

use Illuminate\Database\Eloquent\Model;

class CampaignToUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'topaz';
    protected $table = 'campaign_to_user';   
    protected $fillable = ['campaign_id','user_id','created_at','updated_at'];

    public function user(){
      return $this->hasOne('App\Http\Models\topaz\GemUsers','id','user_id');
    } 

    public function campaign(){
      return $this->hasOne('App\Http\Models\topaz\Campaign','id','campaign_id');
    }         
}