<?php namespace App\Http\Models\topaz;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'topaz';
    protected $table = 'points';   
    protected $fillable = ['ticket_id','notes','user_id','closer_id','point','closer_point','created_at','updated_at'];


    
} 