<?php namespace App\Http\Models\topaz;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'topaz';
    protected $table = 'content';   
    protected $fillable = ['content','created_at','updated_at'];

    
}