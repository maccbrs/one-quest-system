<?php namespace App\Http\Models\topaz;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'topaz';
    protected $table = 'reply';   
    protected $fillable = ['ticket_id','content_id','user_id','created_at','updated_at'];

    public function content(){
      return $this->hasOne('App\Http\Models\topaz\Content','id','content_id');
    }  

    public function user(){
      return $this->hasOne('App\Http\Models\topaz\GemUsers','id','user_id');
    }  
    
} 