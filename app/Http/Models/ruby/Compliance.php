<?php namespace App\Http\Models\ruby;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Compliance extends Model
{

   protected $connection = 'ruby';
   protected $table = 'compliance';
   protected $fillable = ['title','qouta','bound','type','freq','user','campaign_inbound','campaign_outbound','agent_inbound','agent_outbound','created_at','updated_at'];
  
   public function scopeUser($query,$id)
   {
       return $query->where('user',$id);
   } 

}