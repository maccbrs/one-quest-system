<?php namespace App\Http\Models\ruby;

use Illuminate\Database\Eloquent\Model;
use Auth;

class AccountFieldsValues extends Model
{

   protected $connection = 'ruby';
   protected $table = 'account_fields_value';
   protected $fillable = ['account_id','field_name','value','createdAt','updatedAt'];
   protected $timestamp = false;


}