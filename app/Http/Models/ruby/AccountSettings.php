<?php namespace App\Http\Models\ruby;

use Illuminate\Database\Eloquent\Model;
use Auth;

class AccountSettings extends Model
{

   protected $connection = 'ruby';
   protected $table = 'account_settings';
   protected $fillable = ['bounds','ingroups','agents','created_at','updated_at'];


}