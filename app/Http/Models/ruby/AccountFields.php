<?php namespace App\Http\Models\ruby;

use Illuminate\Database\Eloquent\Model;
use Auth;

class AccountFields extends Model
{

   protected $connection = 'ruby';
   protected $table = 'account_fields';
   protected $fillable = ['field_name','type','key','label','createdAt','updatedAt'];


}