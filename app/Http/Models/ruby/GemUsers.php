<?php namespace App\Http\Models\ruby;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemUsers extends Model
{

   protected $connection = 'gem';
   protected $table = 'users';
   protected $fillable = ['name','email','department_id','password','options','user_type','access','avatar','routes','status','active','created_at','updated_at','auditor','is_representative'];


   public function scopeAgent($query)
   {
       return $query->where('is_representative',1);
   } 

} 