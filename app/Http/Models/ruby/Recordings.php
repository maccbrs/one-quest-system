<?php namespace App\Http\Models\ruby;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Recordings extends Model
{

   protected $connection = 'ruby';
   protected $table = 'recordings';
   protected $timestamp = false;
   protected $fillable = ['uniqueid','call_date','status','agent','length_in_min','location' ,'filename' ,'campaign_id','audited' ,'audit_id' ,'createdAt' ,'updatedAt','checked','week_no','month','year','group','task_type','monitoring_type','evaluator','is_complete' ,'week_days','options','locked','score','phone','term_reason'];

  
    public function scopeCampaigns($query)
   {
       return $query->select('campaign_id')->groupBy('campaign_id')->orderBy('campaign_id','asc');
   }  

    public function scopeWeek($query)
     {
         return $query->select('week_no','year')->where('locked',1)->groupBy('week_no','year');
     } 

   public function scopeAgent($query)
   {
       return $query->select('agent')->groupBy('agent')->orderBy('agent','asc');
   } 
   
   public function scopeNotin($query)
   {
       return $query->whereNotIn('agent', ['VDAD','VDCL']);
   } 

  public function scopeSearch($query,$search)
  {
      $s = '%'.$search.'%';
      return $query->where('agent','LIKE', $s)->orWhere('phone', 'LIKE', $s)->orderBy('call_date','desc');
  } 

}