<?php namespace App\Http\Models\ruby;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Campaigns extends Model
{

   protected $connection = 'ruby';
   protected $table = 'campaigns';
   protected $fillable = ['bound','title','campaigns','created_at','updated_at'];
  
}