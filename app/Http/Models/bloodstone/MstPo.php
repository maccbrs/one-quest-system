<?php namespace App\Http\Models\bloodstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstPo extends Model
{

    protected $connection = 'bloodstone';
    protected $table = 'mst_po';   
    protected $fillable = ['id', 'name','alias','remarks'];
   
   	public function po(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','purchase_id','id');
   	} 

    
	

}