<?php namespace App\Http\Models\bloodstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class BloodstoneAssets extends Model
{

   protected $connection = 'bloodstone';
   protected $table = 'assets';
   protected $fillable = ['unique_id','type','brand','model','serial_no','description','active','brand_id','dept_id','model_id','status_id','type_id','created_at','updated_at','created_by','updated_by','remarks'];

   public function createdby(){
   		return $this->hasOne('App\Http\Models\gem\GemUser','id','created_by');
   }

   public function updatedby(){
   		return $this->hasOne('App\Http\Models\gem\GemUser','id','updated_by');
   }

   public function assignedby(){
   		return $this->hasOne('App\Http\Models\gem\GemUser','id','assigned_by');
   }  

   public function assignedto(){
   		return $this->hasOne('App\Http\Models\gem\GemUser','id','assigned_to');
   }

   public function type(){
   		return $this->hasOne('App\Http\Models\bloodstone\MstType','id','type_id');
   }  

   public function brand(){
   		return $this->hasOne('App\Http\Models\bloodstone\MstBrand','id','brand_id');
   }  

   public function model(){
   		return $this->hasOne('App\Http\Models\bloodstone\MstModel','id','model_id');
   }  

   public function status(){
   		return $this->hasOne('App\Http\Models\bloodstone\MstStatus','id','status_id');
   }  

   public function dept(){
   		return $this->hasOne('App\Http\Models\bloodstone\MstDepartment','id','dept_id');
   }  

   public function po(){
         return $this->hasOne('App\Http\Models\bloodstone\MstDepartment','id','purchase_id');
   }

   public function or(){
         return $this->hasOne('App\Http\Models\bloodstone\MstDepartment','id','receipt_id');
   }

   public function vendor(){
         return $this->hasOne('App\Http\Models\bloodstone\MstDepartment','id','vendor_id');
   } 

} 