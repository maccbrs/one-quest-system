<?php namespace App\Http\Models\bloodstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstModel extends Model
{

    protected $connection = 'bloodstone';
    protected $table = 'mst_model';   
    protected $fillable = ['id', 'name','alias','remarks'];
   
   
   	public function model(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','model_id','id');
   	} 
    
	

}