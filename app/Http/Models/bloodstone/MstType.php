<?php namespace App\Http\Models\bloodstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstType extends Model
{

    protected $connection = 'bloodstone';
    protected $table = 'mst_type';   
    protected $fillable = ['id', 'name','alias','remarks'];
   
   	public function type(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','type_id','id');
   	}     

    
	

}