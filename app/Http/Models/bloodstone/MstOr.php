<?php namespace App\Http\Models\bloodstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstOr extends Model
{

    protected $connection = 'bloodstone';
    protected $table = 'mst_or';   
    protected $fillable = ['id', 'name','alias','remarks'];
   
   	public function or(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','receipt_id','id');
   	} 

    
	

}