<?php namespace App\Http\Models\bloodstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstVendor extends Model
{

    protected $connection = 'bloodstone';
    protected $table = 'mst_vendor';   
    protected $fillable = ['id', 'name','alias','remarks'];
   
   	public function vendor(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','vendor_id','id');
   	} 

    
	

}