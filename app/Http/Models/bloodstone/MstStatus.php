<?php namespace App\Http\Models\bloodstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstStatus extends Model
{

    protected $connection = 'bloodstone';
    protected $table = 'mst_status';   
    protected $fillable = ['id', 'name','alias','remarks'];
   
   	public function status(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','status_id','id');
   	} 

    
	

}