<?php namespace App\Http\Models\bloodstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstDepartment extends Model
{

    protected $connection = 'bloodstone';
    protected $table = 'mst_dept';   
    protected $fillable = ['id', 'name','alias','remarks'];
   
   
   	public function dept(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','dept_id','id');
   	}
    
	

}