<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Email extends Model
{

   protected $connection = 'poppy';
   protected $table = 'ost_email';
   protected $fillable = ['noautoresp','priority_id','dept_id','topic_id','email','name','userid','userpass','mail_active','mail_host','mail_protocol','mail_encryption','mail_port','notes','created','updated'];

}