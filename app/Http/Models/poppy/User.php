<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class User extends Model
{

   public $timestamps = false;
   protected $connection = 'poppy';
   protected $table = 'ost_user';
   protected $fillable = ['org_id','default_email_id','status','name','created','updated'];



}