<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Ticket extends Model
{

   public $timestamps = false;
   protected $connection = 'poppy';
   protected $table = 'ost_ticket';
   protected $fillable = ['number','user_id','user_email_id','status_id','dept_id','sla_id','topic_id','staff_id',
   'team_id','email_id','flags','ip_address','source','isoverdue','isanswered','duedate','reopened','closed','lastupdate','created','updated'];

   public function thread(){
   		return $this->hasOne('App\Http\Models\poppy\TicketThread','id','ticket_id')->with(['entries']);
   } 

   public function subject(){
 		return $this->hasOne('App\Http\Models\poppy\TicketCdata','ticket_id','ticket_id');  	
   }

   public function scopeAnswered($query)
   {
       return $query->where('isanswered', 1)->where('closed',null);
   } 

   public function scopeOpen($query)
   {
       return $query->where('isanswered', 0)->where('closed',null);
   } 

	public function scopeOwner($query,$user)
	{
	    return $query->where('user_id', $user);
	}  

	public function scopeOrder($query,$order)
	{
	    return $query->orderBy('lastupdate', $order);
	}  
}