<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ThreadEntry extends Model
{

   public $timestamps = false;
   protected $connection = 'poppy';
   protected $table = 'ost_thread_entry';
   protected $fillable = ['pid','thread_id','staff_id','user_id','type','flags','poster','editor','editor_type','source','title','body','format','ip_address','created','updated'];

}

