<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class UserEmail extends Model
{

   public $timestamps = false;
   protected $connection = 'poppy';
   protected $table = 'ost_user_email';
   protected $fillable = ['id','user_id','address'];

 
	public function user(){
		return $this->belongsTo('App\Http\Models\poppy\User','user_id');
	}

	public function tickets(){
		return $this->hasMany('App\Http\Models\poppy\Ticket','user_id','user_id')->with(['threads']);
	}

	public function scopeOwner($query,$email)
	{
	    return $query->where('address', $email);
	}   

} 