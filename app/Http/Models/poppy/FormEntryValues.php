<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class FormEntryValues extends Model
{

   protected $connection = 'poppy';
   protected $table = 'ost_form_entry_values';
   protected $fillable = ['entry_id','field_id','value','value_id'];

}