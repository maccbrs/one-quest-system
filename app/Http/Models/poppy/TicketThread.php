<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class TicketThread extends Model
{
   public $timestamps = false;
   protected $connection = 'poppy';
   protected $table = 'ost_thread';
   protected $fillable = ['id','object_id','lastresponse','lastmessage'];

   public function entries(){
   		return $this->hasMany('App\Http\Models\poppy\ThreadEntry','thread_id');
   }    

}  