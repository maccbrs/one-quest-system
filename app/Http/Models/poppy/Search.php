<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Search extends Model
{

   protected $connection = 'poppy';
   protected $table = 'ost__search';
   protected $fillable = ['object_type','object_id','title','content'];

}