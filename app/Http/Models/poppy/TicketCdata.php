<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class TicketCdata extends Model
{

   public $timestamps = false;
   protected $connection = 'poppy';
   protected $table = 'ost_ticket__cdata';
   protected $fillable = ['ticket_id','subject','priority'];

}