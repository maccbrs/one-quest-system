<?php namespace App\Http\Models\test;

use Illuminate\Database\Eloquent\Model;

class Areacodes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'gemstone';
    protected $table = 'areacodes';   
    protected $fillable = ['areacodes','state','phone','created_at','updated_at'];

    
}