<?php namespace App\Http\Models\test;

use Illuminate\Database\Eloquent\Model;

class TestB extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'gardenia';
    protected $table = 'content';   
    protected $fillable = ['lead_id','campaign_id','created_at','updated_at','content','lists','status','session_id','user','mb_read'];

    
}