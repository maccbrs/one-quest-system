<?php namespace App\Http\Models\test;

use Illuminate\Database\Eloquent\Model;

class TestA extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'gardenia';
    protected $table = 'testa';   
    protected $fillable = ['date','agents_name','clients_name','phone_number','store_name','shipping_address','follow_up_date','remarks','notes'];

    
}