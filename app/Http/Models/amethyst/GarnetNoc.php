<?php namespace App\Http\Models\amethyst;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetNoc extends Model
{

   protected $connection = 'garnet';
   protected $table = 'noc';
   protected $fillable = ['first_name','middle_name','last_name','type','status'];

    public function attendance(){
   		return $this->hasOne('App\Http\Models\amethyst\GarnetNocAttandace','noc_id','id');
   	}  


} 