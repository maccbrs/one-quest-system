<?php namespace App\Http\Models\amethyst;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemApprover extends Model
{

   protected $connection = 'gem';
   protected $table = 'approver';
   protected $fillable = ['level1','level2','level3','added_by','created_at','updated_at'];

   public function emp(){
   		return $this->hasOne('App\Http\Models\amethyst\GemUsers','id','level1');
   }   
 
   public function sup(){
   		return $this->hasOne('App\Http\Models\amethyst\GemUsers','id','level2');
   }

} 