<?php namespace App\Http\Models\amethyst;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetOpened extends Model
{

   protected $connection = 'garnet';
   protected $table = 'opened';
   protected $fillable = ['user_id','announcements_id','created_at','updated_at'];

   public function userObj(){
      return $this->hasOne('App\Http\Models\amethyst\GemUsers','id','user_id');
   } 
}   