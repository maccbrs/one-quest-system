<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class RecordingsCloser extends Model
{

   protected $connection = 'ruby';
   protected $table = 'recordings_closer';
   protected $timestamp = false;
   protected $fillable = ['uniqueid','call_date','account_id','archive_id','qa_results','status','agent','length_in_min','location' ,'filename' ,'lead_id','campaign_id','audited' ,'audit_id' ,'createdAt' ,'updatedAt','checked','closecallid','week_no','month','year','group','task_type','monitoring_type','customer_name','customer_phone','evaluator','is_complete' ,'week_days','options','locked','score','phone','term_reason','final','call_summary'];

  
   public function scopeCampaigns($query)
   {
       return $query->select('campaign_id')->where('is_active',1)->groupBy('campaign_id')->orderBy('campaign_id','asc');
   }    

    public function scopeWeek($query)
     {
         return $query->select('week_no','year')->where('locked',1)->groupBy('week_no','year');
     } 

   public function scopeAgent($query)
   {
       return $query->select('agent','campaign_id')->where('is_active',1)->groupBy('agent')->orderBy('agent','asc');
   } 


   public function scopeNotin($query)
   {
       return $query->whereNotIn('agent', ['VDAD','VDCL']);
   } 

  public function scopeSearch($query,$search)
  {
      $s = '%'.$search.'%';
      return $query->where('agent','LIKE', $s)->orWhere('phone', 'LIKE', $s)->orderBy('call_date','desc');
  }  

   public function account(){
    return $this->hasOne('App\Http\Models\agate\Campaigns','id','account_id');   
   }     

   public function evaluatorObj(){
    return $this->hasOne('App\Http\Models\agate\GemUsers','id','evaluator');   
   } 
   
}