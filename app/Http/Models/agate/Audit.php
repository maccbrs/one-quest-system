<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Audit extends Model
{

   protected $connection = 'ruby';
   protected $table = 'audits';
   protected $timestamp = false;
   protected $fillable = ['bound','audit_date','auditor','set_id','createdAt','updatedAt'];

   public function closer(){
 		return $this->hasMany('App\Http\Models\agate\RecordingsCloser','audit_id')->orderBy('agent');  	
   }   

   public function recordings(){
 		return $this->hasMany('App\Http\Models\agate\Recordings','audit_id')->orderBy('agent'); 	
   }  
   
   public function set(){
 		return $this->hasOne('App\Http\Models\agate\QaSettings','id','set_id');  	
   }     
}