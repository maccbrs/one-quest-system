<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Templates extends Model
{

   protected $connection = 'ruby';
   protected $table = 'templates';
   protected $timestamp = false;
   protected $fillable = ['title','keyfactors','user_id','created_at','updated_at'];  

 

}