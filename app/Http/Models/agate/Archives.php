<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Archives extends Model
{

   protected $connection = 'ruby';
   protected $table = 'archives';
   protected $fillable = ['name','campaign','user','year','month','date','created_at','updated_at'];

   public function campaignObj(){
      return $this->hasOne('App\Http\Models\agate\Campaigns','id','campaign');
   } 

   public function scopeOwned($query)
   {
       return $query->where('user',Auth::user()->id);
   } 

}