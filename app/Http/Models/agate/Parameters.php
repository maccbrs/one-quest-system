<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Parameters extends Model
{

   protected $connection = 'ruby';
   protected $table = 'parameters';
   protected $fillable = ['title','items','created_at','updated_at'];  

 

}