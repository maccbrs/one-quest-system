<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetAnnouncements extends Model
{

   protected $connection = 'garnet';
   protected $table = 'announcements';
   public $timestamps = false;
   protected $fillable = ['title','excerpt','content','source','destination','user_id','created_at','updated_at'];

   public function scopeSource($query,$id)
   {
       return $query->where('source',$id);
   } 

}   