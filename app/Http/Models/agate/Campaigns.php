<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Campaigns extends Model
{

   protected $connection = 'ruby';
   protected $table = 'campaigns';
   protected $fillable = ['title','customer_name','caller_id','phone_number','file_name','lms_disposition','logo','created_at','updated_at'];
  
}