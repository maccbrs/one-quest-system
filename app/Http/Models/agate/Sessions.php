<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Sessions extends Model
{

   protected $connection = 'ruby';
   protected $table = 'sessions';
   protected $fillable = ['token','content'];


}