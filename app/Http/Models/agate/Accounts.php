111<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Accounts extends Model
{

   protected $connection = 'ruby';
   protected $table = 'accounts';
   protected $fillable = ['project_name','project_alias',
   'fst_training_date',
   'product_training_date',
   'program_launch_date',
   'project_status',
   'billing_cycle',
   'commercial_arrangement',
   'type_of_program',
   'project_description',
   'agent_headcount',
   'operational_location',
   'contact_information',
   'service_coverage',
   'est_call_volume',
   'est_avg_handle_time',
   'documents_provided',
   'tech_requirements',
   'train_requirements',
   'recruitment_requirements',
   'operational_requirements',
   'createdAt',
   'updatedAt'];
   public $timestamps = false;


}   