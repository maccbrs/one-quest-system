<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class QaSettings extends Model
{

   protected $connection = 'ruby';
   protected $table = 'qa_settings';
   protected $timestamp = false;
   protected $fillable = ['account_id','title','bound','ingroups','agents','project_status','createdAt','updatedAt'];

   public function account(){
 		return $this->hasOne('App\Http\Models\agate\Accounts','id','account_id');  	
   }   

}