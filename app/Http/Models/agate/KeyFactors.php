<?php namespace App\Http\Models\agate;

use Illuminate\Database\Eloquent\Model;
use Auth;

class KeyFactors extends Model
{

   protected $connection = 'ruby';
   protected $table = 'key_factors';
   protected $fillable = ['title','description','parameters','created_at','updated_at'];  

 

}