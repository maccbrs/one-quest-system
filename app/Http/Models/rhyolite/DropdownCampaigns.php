<?php namespace App\Http\Models\rhyolite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DropdownCampaigns extends Model
{

   protected $connection = 'rhyolite';
   protected $table = 'dropdown_campaigns';
   protected $fillable = ['title','campaigns'];

}