<?php namespace App\Http\Models\rhyolite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class InteractionWrapup extends Model
{

   protected $connection = 'nexus';
   protected $table = 'InteractionWrapup';
   protected $timestamp = false;
   protected $fillable = ['WrapupCode'];



}