<?php namespace App\Http\Models\rhyolite;

use Illuminate\Database\Eloquent\Model;
use DB;

class GemUsers extends Model
{

    protected $connection = 'gem';
    protected $table = 'users';   
    protected $fillable = ['name', 'email', 'password','user_type','access'];
    protected $hidden = [
        'password', 'remember_token',
    ];


}