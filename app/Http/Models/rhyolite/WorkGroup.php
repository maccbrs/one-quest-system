<?php namespace App\Http\Models\rhyolite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class WorkGroup extends Model
{

   protected $connection = 'nexus';
   public $timestamps = false;
   protected $table = 'dbo.UserWorkgroups';
   protected $fillable = ['WorkGroup'];

}