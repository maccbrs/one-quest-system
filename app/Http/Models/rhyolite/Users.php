<?php namespace App\Http\Models\rhyolite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Users extends Model
{

   protected $connection = 'rhyolite';
   protected $table = 'users';
   protected $fillable = ['gem_id','type','allowed','name'];

}