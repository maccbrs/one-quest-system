<?php namespace App\Http\Models\rhyolite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Eod extends Model
{

   protected $connection = 'rhyolite';
   protected $table = 'eod';
   protected $fillable = ['name','cic_id','dummyboard_id','send_time','timezone','from','to','email_from','email_to','email_cc','converted_sendtime','cfrom','status','cto'];

}