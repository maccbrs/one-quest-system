<?php namespace App\Http\Models\rhyolite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DboCalldetailViw extends Model
{

   protected $connection = 'nexus';
   protected $table = 'dbo.calldetail_viw';
   protected $timestamp = false;
   protected $fillable = ['CallId','CallDirection'];

   	public function wrapup(){

      return $this->hasOne('App\Http\Models\rhyolite\InteractionWrapup','InteractionIDKey','CallId'); 
    
    } 

}