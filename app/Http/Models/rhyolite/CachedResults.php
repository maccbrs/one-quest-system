<?php namespace App\Http\Models\rhyolite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CachedResults extends Model
{

   protected $connection = 'rhyolite';
   protected $table = 'cached_results';
   protected $fillable = ['contents','user'];

}