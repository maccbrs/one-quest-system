<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Compliance extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'compliance';   
    protected $fillable = ['id','product_taken','sku','date_start_medication','still_medication','month_stopped','reason','medication','remarks','callnotes'];
    protected $hidden = [
        'password', 'remember_token',
    ];
	
/*	public function sku_details(){
        return $this->hasMany('App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl_Validated','sku','id');
   }*/

/*    public function upload(){

        return $this->hasMany('App\Http\Models\otsuka\Upload','connector_id','connector');

    }*/

    public function fetch_patient(){
        return $this->hasOne('App\Http\Models\otsuka\Patient_Validated','id','patientid')->with(['fetch_mr','fetch_firstredeem','fetch_sku_compliance','fetch_purchased_compliance','fetch_compliance_patient_count','fetch_receipt_encoded','fetch_mr','fetch_sku_vol_ref_compliance','fetch_receipt_encoded_compliance']);
   }

    public function fetch_callstatus(){
        return $this->hasOne('App\Http\Models\otsuka\Call_status','id','reason');
   }

    public function fetch_compliance_reason(){
        return $this->hasOne('App\Http\Models\otsuka\Compliance_reason','id','reason');
   }

    public function fetch_old(){
        return $this->hasOne('App\Http\Models\otsuka\Old_retrieval_enrollment','patientid','patientid')->where(function($query){$query->where('brandid',1)->orWhere('brandid',2)->orWhere('brandid',8);})->with(['fetch_doctors','fetch_product','fetch_sku']);
   }

    public function fetch_month(){
        return $this->hasOne('App\Http\Models\otsuka\Months','id','month_stopped');
   }

    public function fetch_patients_medinfo(){

        return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_id','patientid')->with(['fetch_prescribed','fetch_brand']);

    }
}