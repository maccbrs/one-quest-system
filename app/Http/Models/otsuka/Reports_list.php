<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Reports_list extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'reports_list';   
    protected $fillable = ['id', 'name','value', 'created_at', 'updated_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];


}