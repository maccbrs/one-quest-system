<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Daily_emails extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'daily_emails';   
    protected $fillable = ['id', 'from','to', 'cc', 'bcc', 'content', 'sent'];
    protected $hidden = [
        'password', 'remember_token',
    ];


}