<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Patient extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'patients';   
    protected $fillable = ['patient_code', 'enrollment_date','time_to_call','patient_kit_number','patient_consent','patient_lastname', 'patient_firstname', 'patient_middlename', 'date_encoded', 'patient_old_code', 'nickname','address','birth_date','age','gender','mobile_prefix','mobile_number','mobile_number_2','phone_prefix','phone_number'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fetch_verification(){

        return $this->hasOne('App\Http\Models\otsuka\Call_verification','cmid','id');

    }

    public function fetch_mr(){

        return $this->hasOne('App\Http\Models\otsuka\Patients_kit_allocation','one_quest_id','patient_kit_number');

    }

}