<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Compliance_reason extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'compliance_reason';   
    protected $fillable = ['id', 'name', 'created_at', 'updated_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];


}