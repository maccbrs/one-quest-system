<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Encoded_Purchases_Validated extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'encoded_purchases_validated';   
    protected $fillable = ['pid','cardnumberid','notcredited','patient_id','submissiondate','ordate','branchid','or_number','filepath','fordispatch','completiondate','dispatched','dispatcheddate','qtytabs','qtynonmed','channelid','active','etimestamp','remarks','added_by'];
    protected $hidden = [
        'password', 'remember_token',
    ];
 
 public function fetch_encoded_dtl(){
	   		return $this->hasMany('App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl_Validated','encoded_purchases_id','id')->with('sku_details');
	   	} 

public function fetch_upload(){
	return $this->hasOne('App\Http\Models\otsuka\Upload','filename','file_path')->with(['user_medrep']);
}

public function fetch_referral(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

	return $this->hasOne('App\Http\Models\otsuka\Patient_Validated','id','patient_id')
    ->whereBetween('created_at',[$first_day,$now])->with(['fetch_sku_abilify','fetch_sku_aminoleban','fetch_sku_pletaal']);;
}

}