<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Cmid extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'cmid';   
    protected $fillable = ['id', 'channel','calltype', 'purpose', 'caller', 'cardnumberid', 'patientid', 'reid','employeeid','remarks','active'];
    protected $hidden = [
        'password', 'remember_token',
    ];

   public function fetch_call(){
        return $this->hasMany('App\Http\Models\otsuka\Upload','cmid','id');
   }

   public function fetch_verification(){
        return $this->hasMany('App\Http\Models\otsuka\Call_verification','cmid','id')->where('tag_deleted',0)->orderBy('created_at','asc')->with(['fetch_status','fetch_name']);
   }

   public function fetch_verification_dispo(){
        return $this->hasOne('App\Http\Models\otsuka\Call_verification','cmid','id')->orderBy('created_at','desc')->with(['fetch_status','fetch_name']);
   }

   public function fetch_verification_queue_compliance(){
        return $this->hasOne('App\Http\Models\otsuka\Call_verification','cmid','id')->orderBy('attempt','asc')->with(['fetch_status']);
   }

   public function fetch_verification_queue(){
        return $this->hasOne('App\Http\Models\otsuka\Call_verification','cmid','id')->where('allowcallback',-1)->orderBy('attempt','desc')->with(['fetch_status','fetch_name']);
   }

   public function fetch_patient(){
        return $this->hasOne('App\Http\Models\otsuka\Patient_Validated','id','patientid')->with(['fetch_compliance_upload','fetch_mr','fetch_sku','old_retrieval_report']);
   }

   public function fetch_upload(){
        return $this->hasOne('App\Http\Models\otsuka\Upload','cmid','id')->with(['fetch_validated']);
   }

   public function fetch_redemption(){
        return $this->hasOne('App\Http\Models\otsuka\Upload','id','upload_id')->with(['fetch_redemption_validated','fetch_file']);
   }

   public function fetch_compliance_details(){
        return $this->hasOne('App\Http\Models\otsuka\Compliance','patientid','patientid')->orderBy('created_at','desc');
   }

}