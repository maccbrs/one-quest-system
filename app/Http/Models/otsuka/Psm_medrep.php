<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Psm_medrep extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'psm_medrep';   
    protected $fillable = ['empcode', 'teamid', 'name', 'area', 'smart', 'globe', 'email'];
    protected $hidden = [
        'password', 'remember_token',
    ];


}