<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Upload extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'upload';   
    protected $fillable = ['id', 'patient_name','trail_status','claimed_by','claimed','submit','verify_claimed_by','filename', 'original_filename','location','added_by', 'connector_id','uploaded_by','remarks','status','viber_queue','mobile_number','medicine','type','patient_kit_number','patientid','verification_remarks','redemption_remarks'];
    protected $hidden = [
        'password', 'remember_token',
    ];

   public function queue(){
        return $this->hasOne('App\Http\Models\otsuka\Queue','connector','connector_id');
   }

   public function user(){
        return $this->hasOne('App\Http\Models\gem\User','id','uploaded_by');
   }

   public function user_medrep(){
        return $this->hasOne('App\Http\Models\gem\User','id','uploaded_by')->with(['fetch_mrlist','fetch_allocation_april_invalid']);
   }

   public function encoded_by(){
        return $this->hasOne('App\Http\Models\gem\User','id','claimed_by');
   }

   public function verification_claimed_by(){
        return $this->hasOne('App\Http\Models\gem\User','id','verify_claimed_by');
   }

   public function fetch_user(){
        return $this->hasOne('App\Http\Models\gem\User','id','uploaded_by');
   }
   
	public function receipt_details(){
        return $this->hasOne('App\Http\Models\otsuka\Encoded_Purchases','id','encoded_purchases_id')->with('fetch_encoded_dtl');
   }

   public function fetch_call_queue(){
        return $this->hasOne('App\Http\Models\otsuka\Cmid','id','cmid')->with(['fetch_verification_queue']);
   }

   public function fetch_call(){
        return $this->hasOne('App\Http\Models\otsuka\Cmid','id','cmid')->with(['fetch_verification','fetch_verification_dispo']);
   }

   public function fetch_allocation(){
        return $this->hasOne('App\Http\Models\otsuka\Patients_kit_allocation','one_quest_id','patient_kit_number')->with(['fetch_doctor']);
   }

   public function fetch_allocation_april(){
        return $this->hasOne('App\Http\Models\otsuka\Patients_kit_allocation_april','one_quest_id','patient_kit_number')->with(['fetch_doctor']);
   }

   public function fetch_allocation_report(){
        return $this->hasOne('App\Http\Models\otsuka\Patients_kit_allocation','one_quest_id','patient_kit_number');
   }  

   public function fetch_status(){
        return $this->hasOne('App\Http\Models\otsuka\Call_status','id','verification_status');
   }

   public function fetch_trail_status(){
        return $this->hasOne('App\Http\Models\otsuka\Trail_Status','key','trail_status');
   }

   public function fetch_validated(){
        return $this->hasOne('App\Http\Models\otsuka\Patient_Validated','patient_kit_number','patient_kit_number')->with(['fetch_sku']);
   }

   public function fetch_redemption_validated(){
        return $this->hasOne('App\Http\Models\otsuka\Patient_Validated','id','redeem_assign_to');
   }

   public function fetch_mdinfo(){
        return $this->hasOne('App\Http\Models\otsuka\National_md_list','mdname','doctor_name');
   }

   public function fetch_file(){
        return $this->hasOne('App\Http\Models\otsuka\Encoded_Purchases','file_path','filename')->with(['fetch_encoded_dtl']);
   }

   public function fetch_medinfo(){
        return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_id','patientid')->with(['fetch_doctor']);
   }

   public function fetch_filename(){
        return $this->hasOne('App\Http\Models\otsuka\Encoded_Purchases_Validated','file_path','filename')->orderBy('created_at')->with(['fetch_referral']);
   }

   public function fetch_mdinfo_upload(){
        return $this->hasOne('App\Http\Models\otsuka\National_md_list','doctorid','doctor_id');
   }


/*    public function garnet($userId = null, $userType = null){

        switch ($userType) {

            case 'Manager':
                
                $ps_data = DB::connection('garnet')
                ->table('project_supervisors')
                ->where('user_id',$userId)
                ->first();

                $superior_data = $ps_data;

                break;

            case 'Team Leader':
                
                $tl_data = DB::connection('garnet')
                ->table('team_leads')
                ->where('user_id',$userId)
                ->first();

                $superior_data = $tl_data;

                break;

            case 'Quality Assurance':
                
                $superior_data = null;

                break;

            case 'Trainer':
                $superior_data = null;
                break;

            default:
                $superior_data = null;
        } 	

		return $superior_data;

   	}*/

/*    public function createdby(){

        return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','created_by','id');

    }

    public function updatedby(){

        return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','updated_by','id');

    }    

    public function assignedby(){

        return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','assinged_by','id');

    }  

    public function assignedto(){

        return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','assinged_to','id');

    }   */         


}