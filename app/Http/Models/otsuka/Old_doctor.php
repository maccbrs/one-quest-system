<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Old_doctor extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'national_md_list_copy';   
    protected $fillable = ['id', 'regioncode','areacode', 'etmscode', 'employeeid', 'empcode', 'mrname', 'employee','doctorid','mdname','specialty','mdclass','frequency','hospital'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fetch_doctor(){
        return $this->hasMany('App\Http\Models\otsuka\Patients_kit_allocation','emp_code','empcode');
    }

}