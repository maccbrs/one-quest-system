<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Queue extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'queue';   
    protected $fillable = ['id', 'queue_name', 'type', 'receipt_id','medicine', 'submit', 'claimed', 'connector','created_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function upload(){

        return $this->hasMany('App\Http\Models\otsuka\Upload','connector_id','connector');

    }

}