<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Patients_med_info extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'patients_med_info';   
    protected $fillable = ['patient_id','doctors_id','medrep_id','sku','no_of_tabs_prescribe','tabs_prescribe_remarks','no_of_days_prescribe','days_prescribe_remarks','total_no_of_tabs','total_tabs_remarks','purchased'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fetch_prescribed(){
        return $this->hasOne('App\Http\Models\otsuka\Sku','id','sku');
    }

    public function fetch_brand(){
    	return $this->hasOne('App\Http\Models\otsuka\Brand','id','sku');
    }
	
	public function fetch_brand_new(){
    	return $this->hasOne('App\Http\Models\otsuka\Brand','Brandname','brand');
    }

    public function fetch_doctor(){
        return $this->hasOne('App\Http\Models\otsuka\National_md_list','doctorid','doctors_id');
    }

    public function fetch_doctor_summary(){
        return $this->hasMany('App\Http\Models\otsuka\National_md_list','doctorid','doctors_id');
    }

    public function fetch_doctor_abilify(){
        return $this->hasOne('App\Http\Models\otsuka\National_md_list','doctorid','doctors_id','empcode','medrep_id')->where('product','=','abilify');
    }

    public function fetch_doctor_aminoleban(){
        return $this->hasOne('App\Http\Models\otsuka\National_md_list','doctorid','doctors_id','empcode','medrep_id')->where('product','=','aminoleban');
    }

    public function fetch_doctor_pletaal(){
        return $this->hasOne('App\Http\Models\otsuka\National_md_list','doctorid','doctors_id','empcode','medrep_id')->where('product','=','pletaal');
    }

    public function fetch_mr_vol(){
        return $this->hasOne('App\Http\Models\otsuka\Medrep','emp_code','medrep_id');
    }

}