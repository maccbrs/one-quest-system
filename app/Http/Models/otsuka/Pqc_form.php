<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pqc_form extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'pqc_form';   
    protected $fillable = ['pqc_date','oppi_name','phone_no','reporter_name','reporter_contact_details','is_hcp','hcp_detail','sku','dosage_form','dosage_form_other','qty_to_be_returned','batch_lot_no','patient_initial','patient_gender','patient_age','patient_contact_no','complaint_detailed','photo_provided','return_sample','is_product_adverse','created_by'];
    protected $hidden = [
        'password', 'remember_token',
    ];
	


   public function fetch_user(){
        return $this->hasOne('App\Http\Models\gem\User','id','created_by');
   }

}