<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class National_md_list extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'national_md_list';   
    protected $fillable = ['regioncode','areacode', 'etmscode', 'employeeid', 'empcode', 'mrname', 'employee','doctorid','mdname','specialty','mdclass','frequency','hospital','product'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fetch_doctor(){
        return $this->hasMany('App\Http\Models\otsuka\Patients_kit_allocation','emp_code','empcode');
    }

}