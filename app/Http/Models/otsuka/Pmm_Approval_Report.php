<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Pmm_Approval_Report extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'pmm_approval_report';   
    protected $fillable = [];
   
 

    
	public function fetch_patient_info(){
        return $this->hasOne('App\Http\Models\otsuka\Patient_Validated','patient_code','patient_code');
	}

	public function fetch_sku(){
        return $this->hasOne('App\Http\Models\otsuka\Sku','id','sku_purchased');
	}

	public function fetch_program(){
        return $this->hasOne('App\Http\Models\otsuka\Psp_Program','programname','program_name');
	}

	

}