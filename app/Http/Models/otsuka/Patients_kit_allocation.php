<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Patients_kit_allocation extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'patients_kit_allocation_consolidated';   
    protected $fillable = ['date_of_request','one_quest_id', 'year', 'month', 'team', 'area_code','emp_code','mr_name','product'  /*,'id'*/, 'time_requested', 'requested_by','date_forwarded_to_warehouse','time_forwareded_to_warehouse'/*,'created_at','created_by','updated_at','updated_by'*/];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fetch_doctor(){
        return $this->hasMany('App\Http\Models\otsuka\National_md_list','empcode','emp_code');

    }

    public function fetch_psm_list(){
        return $this->hasOne('App\Http\Models\otsuka\Psm_medrep','teamid','team');

    }

    public function fetch_upload(){
        return $this->hasMany('App\Http\Models\otsuka\Upload','patient_kit_number','one_quest_id');
    }

	public function fetch_mr_record(){
        return $this->hasOne('App\Http\Models\otsuka\National_md_list_mr_distinc','areacode','area_code');
    }	

}