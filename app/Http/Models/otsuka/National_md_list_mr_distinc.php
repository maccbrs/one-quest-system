<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class National_md_list_mr_distinc extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'national_md_list_distinc_mr';   
    protected $fillable = ['id', 'regioncode','areacode', 'etmscode', 'employeeid', 'empcode', 'mrname', 'employee','doctorid','mdname','specialty','mdclass','frequency','hospital'];
    protected $hidden = [
        'password', 'remember_token',
    ];



}