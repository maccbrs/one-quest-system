<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Old_retrieval_enrollment extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'old_retrieval_enrollment';   
    protected $fillable = ['id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
	
    public function fetch_product(){

        return $this->hasOne('App\Http\Models\otsuka\Brand','brandid','brandid')->where(function($query){$query->where('brandid',1)->orWhere('brandid',2)->orWhere('brandid',8);});

    }

   	public function fetch_sku(){

        return $this->hasOne('App\Http\Models\otsuka\Sku','skuid','skuid');

    }
   	public function fetch_doctors(){

        return $this->hasOne('App\Http\Models\otsuka\DoctorsModel','id','doctorid')->with(['fetch_mdlist']);

    }
    public function fetch_patient(){
        return $this->hasOne('App\Http\Models\otsuka\Patient_validated','id','patientid');
    }
}