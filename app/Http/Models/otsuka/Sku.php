<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Sku extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'sku';   
    protected $fillable = ['id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public function sku_details(){
        return $this->hasMany('App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl_Validated','sku','id');
   }

/*    public function fetch_prescribed(){

        return $this->hasOne('App\Http\Models\otsuka\Upload','connector_id','connector');

    }*/

}