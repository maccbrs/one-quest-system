<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class DoctorsModel extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'doctors';   
    protected $fillable = ['id','doctorname','active','timestamp','doctorcode','doctorfname','doctorlname','dubiousrecord','specializationid','remarks','prclicense','prclicensemirror','onlineenrollment'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fetch_mdlist(){

        return $this->hasOne('App\Http\Models\otsuka\National_md_list','doctorid','doctorcode');

    }

}