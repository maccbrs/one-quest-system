<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Retrieval extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'retrieval';   
    protected $fillable = ['id', 'filename', 'location','added_by'];
    protected $hidden = [
        'password', 'remember_token',
    ];

/*    public function garnet($userId = null, $userType = null){

        switch ($userType) {

            case 'Manager':
                
                $ps_data = DB::connection('garnet')
                ->table('project_supervisors')
                ->where('user_id',$userId)
                ->first();

                $superior_data = $ps_data;

                break;

            case 'Team Leader':
                
                $tl_data = DB::connection('garnet')
                ->table('team_leads')
                ->where('user_id',$userId)
                ->first();

                $superior_data = $tl_data;

                break;

            case 'Quality Assurance':
                
                $superior_data = null;

                break;

            case 'Trainer':
                $superior_data = null;
                break;

            default:
                $superior_data = null;
        } 	

		return $superior_data;

   	}*/

/*    public function createdby(){

        return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','created_by','id');

    }

    public function updatedby(){

        return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','updated_by','id');

    }    

    public function assignedby(){

        return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','assinged_by','id');

    }  

    public function assignedto(){

        return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','assinged_to','id');

    }   */         


}