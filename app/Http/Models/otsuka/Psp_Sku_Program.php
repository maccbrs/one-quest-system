<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Psp_Sku_Program extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'psp_skus_program';   
    protected $fillable = [];
   
   public function fetch_program_desc(){
   		return $this->hasOne('App\Http\Models\otsuka\Psp_Program','id','programid');
   }  

    
	

}