<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Call_verification extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'call_verification';   
    protected $fillable = ['id', 'attempt','cmid', 'allowcallback', 'remarks', 'remarks_others', 'callnotes', 'employeeid', 'timestamp','created_at','updated_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fetch_patient(){

        return $this->hasOne('App\Http\Models\otsuka\Patient_Validated','id','cmid');
    }

    public function fetch_name(){

        return $this->hasOne('App\Http\Models\gem\User','id','employeeid');
    }

    public function fetch_status(){

        return $this->hasOne('App\Http\Models\otsuka\Call_status','id','remarks');
    }

    public function fetch_compliance_cmid(){

        return $this->hasOne('App\Http\Models\otsuka\Cmid','id','cmid')->with(['fetch_compliance_details']);
    }

    public function fetch_cmid(){
        return $this->hasOne('App\Http\Models\otsuka\Cmid','id','cmid')->with(['fetch_patient','fetch_compliance_details']);
    }

/*    public function fetch_status(){

        return $this->hasOne('App\Http\Models\otsuka\call_status','id','remarks');
    }*/

}