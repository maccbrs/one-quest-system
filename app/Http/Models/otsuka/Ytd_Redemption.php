<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Ytd_Redemption extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'YTD_REDEMPTION_UPDATES';   
    protected $fillable = [
	'id',
	'patient_code',
	'OneQuestNumber',
	'date_of_redemption',
	'product_purchased',
	'sku_purchased',
	'no_tab',
	'prod_redemp',
	'sku_redemp',
	'no_tab_free',
	'redemption_status',
	'date_of_submission',
	'or_number',
	'created_by',

	'updated_by'
	
	];
   
 

    
	public function fetch_patient_info(){
        return $this->hasOne('App\Http\Models\otsuka\Patient_Validated','patient_code','patient_code')->with(['fetch_mr','old_retrieval','fetch_sku']);
   }

   //public function fetch_patient_kit_allocation(){
   //     return $this->hasOne('App\Http\Models\otsuka\Patients_kit_allocation','one_quest_id','OneQuestNumber');
   //}
   	public function fetch_patient_kit_allocation_reserve(){
        return $this->hasOne('App\Http\Models\otsuka\Patients_kit_allocation_reserve','one_quest_id','OneQuestNumber');
   }

}