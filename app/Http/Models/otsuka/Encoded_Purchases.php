<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Encoded_Purchases extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'encoded_purchases';   
    protected $fillable = ['pid','cardnumberid','notcredited','patientid','submissiondate','ordate','branchid','or_number','filepath','fordispatch','completiondate','dispatched','dispatcheddate','qtytabs','qtynonmed','channelid','active','etimestamp','remarks'
];
    protected $hidden = [
        'password', 'remember_token',
    ];
 
 public function fetch_encoded_dtl(){
	   		return $this->hasMany('App\Http\Models\otsuka\Encoded_Purchases_Item_Dtl','encoded_purchases_id','id')->with('sku_details');
	   	} 

}