<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use Auth;

class PatientBalance extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'PatientBalance';   
    protected $fillable = [];
   
   public function Details(){
   		return $this->hasOne('App\Http\Models\otsuka\Patient_Validated','patient_kit_number','PatientCode');
   }  

    
	

}