<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Txtconnect extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'txtconnect';   
    protected $fillable = ['id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
	

}