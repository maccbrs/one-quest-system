<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Medrep extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'medrep_distro';   
    protected $fillable = ['id','rgc', 'team', 'new_code', 'division', 'emp_code', 'pso_name', 'type','smart','globe','email','location','psm_empcode','psm_name','psm_team','psm_email'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function allocation(){

        return $this->hasMany('App\Http\Models\otsuka\Patients_kit_allocation','area_code','new_code')->withCount(['fetch_upload']);

    }

    public function connect_user(){



        return $this->hasOne('App\Http\Models\gem\User','email','emp_code')->with(['fetch_total_retrieval','fetch_abilify_new','fetch_abilify_finished','fetch_abilify_stopped','fetch_abilify_switched','fetch_abilify_total_verified','fetch_abilify_declined','fetch_abilify_wrong_number','fetch_abilify_noanswer_5','fetch_abilify_noanswer_4','fetch_abilify_noconsent','fetch_abilify_queue','fetch_abilify_total_unverified_retrieval','fetch_aminoleban_new','fetch_aminoleban_finished','fetch_aminoleban_stopped','fetch_aminoleban_switched','fetch_aminoleban_total_verified','fetch_aminoleban_declined','fetch_aminoleban_wrong_number','fetch_aminoleban_noanswer_5','fetch_aminoleban_noanswer_4','fetch_aminoleban_noconsent','fetch_aminoleban_queue','fetch_aminoleban_total_unverified_retrieval','fetch_pletaal_new','fetch_pletaal_finished','fetch_pletaal_stopped','fetch_pletaal_switched','fetch_pletaal_total_verified','fetch_pletaal_declined','fetch_pletaal_wrong_number','fetch_pletaal_noanswer_5','fetch_pletaal_noanswer_4','fetch_pletaal_noconsent','fetch_pletaal_queue','fetch_pletaal_total_unverified_retrieval','fetch_invalid_blurred','fetch_invalid_duplicate','fetch_invalid_already_enrolled','fetch_invalid_wrong_upload','fetch_invalid_total','fetch_referral_abilify','fetch_referral_aminoleban','fetch_referral_pletaal']);

    }    

/*    public function month($request)
    {
        //dd($request);
        $data = $request;
        return $data;
        
        //pre($test);
    }*/

    public function fetch_voluntary_abilify(){

        //dd($month);

    	$Encoded_purchases_validated = new \App\Http\Models\otsuka\Encoded_purchases_validated;
   		$Patient_Validated = $patient = new \App\Http\Models\otsuka\Patient_Validated;
   		$Users = $patient = new \App\Http\Models\gem\User;
   		$Patients_med_info = new \App\Http\Models\otsuka\Patients_med_info;
   		$National_md_list = new \App\Http\Models\otsuka\National_md_list;

            		$dt = date('Y-m-d');
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-02-01 00:00:00';
                    $now = date('Y-m-d').' 17:00:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        $agent = $Users->select('id')->where('user_type','=','agent')->get()->toArray();            

    	$encoded_vol = $Encoded_purchases_validated->select('patient_id')->whereIn('added_by',$agent)->groupBy('patient_id')->get()->toArray();

    	$encoded = $Encoded_purchases_validated->select('patient_id')->whereNotIn('added_by',$agent)->groupBy('patient_id')->get()->toArray();

    	$encoded_remover = $Encoded_purchases_validated->select('patient_id')->whereIn('added_by',$agent)->whereIn('patient_id',$encoded)->groupBy('patient_id')->get()->toArray();

    	$abilify_sku = array(1,2,3,4,5,6,7,8);
    	$amino = array(9,10,11,42);
    	$pletaal = array(20,21,22,23,24);

/*    	$encoded_vol_final = $Patients_med_info
    						->select('patient_id','sku')
    						->where(function($query) use ($encoded_vol, $encoded) {$query->whereIn('patient_id', $encoded_vol)->orWhereNotIn('patient_id',$encoded);})
    						->whereIn('sku',$abilify_sku)
    						->get()
    						->toArray();*/

    	$voluntary = $Patient_Validated
    				->select('patients_validated.id')
    				->join('patients_med_info', 'patients_validated.id', '=', 'patients_med_info.patient_id')
    				->where(function($query) use ($encoded_vol, $encoded) {$query->whereIn('patients_validated.id', $encoded_vol)->orWhereNotIn('patients_validated.id',$encoded);})
    				->whereNotIn('patients_validated.id',$encoded_remover)
    				->where('patients_med_info.doctors_id','!=', 0)
    				->where(function($query){$query->where('patients_validated.patient_type','=','referral')->orWhere('patients_validated.patient_type','=','Voluntary');})
    				->where(function($query) use ($abilify_sku) {$query->whereIn('patients_med_info.sku',$abilify_sku)->orWhere('patients_validated.patient_tagging_product_prescribe','=','Abilify');})
    				//->whereMonth('patients_validated.created_at','=','5')
    				->get()
    				->toArray();

    	$test = $National_md_list
    			->select('id')
    			->groupBy('doctorid')
    			->orderBy('id')
    			->get()
    			->toArray();

    				//pre($voluntary);


    	return $this->hasMany('App\Http\Models\otsuka\Patient_Validated', 'patient_tagging_mr_empcode','emp_code')->whereIn('id',$voluntary)->with(['fetch_sku']);

    }

public function fetch_voluntary_aminoleban(){

    	$Encoded_purchases_validated = new \App\Http\Models\otsuka\Encoded_purchases_validated;
   		$Patient_Validated = $patient = new \App\Http\Models\otsuka\Patient_Validated;
   		$Users = $patient = new \App\Http\Models\gem\User;
   		$Patients_med_info = new \App\Http\Models\otsuka\Patients_med_info;
   		$National_md_list = new \App\Http\Models\otsuka\National_md_list;

            		$dt = date('Y-m-d');
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-02-01 00:00:00';
                    $now = date('Y-m-d').' 17:00:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        $agent = $Users->select('id')->where('user_type','=','agent')->get()->toArray();            

    	$encoded_vol = $Encoded_purchases_validated->select('patient_id')->whereIn('added_by',$agent)->groupBy('patient_id')->get()->toArray();

    	$encoded = $Encoded_purchases_validated->select('patient_id')->whereNotIn('added_by',$agent)->groupBy('patient_id')->get()->toArray();

    	$encoded_remover = $Encoded_purchases_validated->select('patient_id')->whereIn('added_by',$agent)->whereIn('patient_id',$encoded)->groupBy('patient_id')->get()->toArray();

    	$abilify_sku = array(1,2,3,4,5,6,7,8);
    	$amino = array(9,10,11,42);
    	$pletaal = array(20,21,22,23,24);

/*    	$encoded_vol_final = $Patients_med_info
    						->select('patient_id','sku')
    						->where(function($query) use ($encoded_vol, $encoded) {$query->whereIn('patient_id', $encoded_vol)->orWhereNotIn('patient_id',$encoded);})
    						->whereIn('sku',$abilify_sku)
    						->get()
    						->toArray();*/

    	$voluntary = $Patient_Validated
    				->select('patients_validated.id')
    				->join('patients_med_info', 'patients_validated.id', '=', 'patients_med_info.patient_id')
    				->where(function($query) use ($encoded_vol, $encoded) {$query->whereIn('patients_validated.id', $encoded_vol)->orWhereNotIn('patients_validated.id',$encoded);})
    				->whereNotIn('patients_validated.id',$encoded_remover)
    				->where('patients_med_info.doctors_id','!=', 0)
    				->where(function($query){$query->where('patients_validated.patient_type','=','referral')->orWhere('patients_validated.patient_type','=','Voluntary');})
    				->where(function($query) use ($amino) {$query->whereIn('patients_med_info.sku',$amino)->orWhere('patients_validated.patient_tagging_product_prescribe','=','Aminoleban');})
    				//->whereBetween('patients_validated.created_at',[$first_day,$now])
    				->get()
    				->toArray();

    	$test = $National_md_list
    			->select('id')
    			->groupBy('doctorid')
    			->orderBy('id')
    			->get()
    			->toArray();

    				//pre($test);


    	return $this->hasMany('App\Http\Models\otsuka\Patient_Validated', 'patient_tagging_mr_empcode','emp_code')->whereIn('id',$voluntary)->with(['fetch_sku']);

    }

public function fetch_voluntary_pletaal(){

    	$Encoded_purchases_validated = new \App\Http\Models\otsuka\Encoded_purchases_validated;
   		$Patient_Validated = $patient = new \App\Http\Models\otsuka\Patient_Validated;
   		$Users = $patient = new \App\Http\Models\gem\User;
   		$Patients_med_info = new \App\Http\Models\otsuka\Patients_med_info;
   		$National_md_list = new \App\Http\Models\otsuka\National_md_list;

            		$dt = date('Y-m-d');
					$first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
                    $mtd_start = '2018-02-01 00:00:00';
                    $now = date('Y-m-d').' 17:00:00';
                    $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
                    $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        $agent = $Users->select('id')->where('user_type','=','agent')->get()->toArray();            

    	$encoded_vol = $Encoded_purchases_validated->select('patient_id')->whereIn('added_by',$agent)->groupBy('patient_id')->get()->toArray();

    	$encoded = $Encoded_purchases_validated->select('patient_id')->whereNotIn('added_by',$agent)->groupBy('patient_id')->get()->toArray();

    	$encoded_remover = $Encoded_purchases_validated->select('patient_id')->whereIn('added_by',$agent)->whereIn('patient_id',$encoded)->groupBy('patient_id')->get()->toArray();

    	$abilify_sku = array(1,2,3,4,5,6,7,8);
    	$amino = array(9,10,11,42);
    	$pletaal = array(20,21,22,23,24);

/*    	$encoded_vol_final = $Patients_med_info
    						->select('patient_id','sku')
    						->where(function($query) use ($encoded_vol, $encoded) {$query->whereIn('patient_id', $encoded_vol)->orWhereNotIn('patient_id',$encoded);})
    						->whereIn('sku',$abilify_sku)
    						->get()
    						->toArray();*/

    	$voluntary = $Patient_Validated
    				->select('patients_validated.id')
    				->join('patients_med_info', 'patients_validated.id', '=', 'patients_med_info.patient_id')
    				->where(function($query) use ($encoded_vol, $encoded) {$query->whereIn('patients_validated.id', $encoded_vol)->orWhereNotIn('patients_validated.id',$encoded);})
    				->whereNotIn('patients_validated.id',$encoded_remover)
    				->where('patients_med_info.doctors_id','!=', 0)
    				->where(function($query){$query->where('patients_validated.patient_type','=','referral')->orWhere('patients_validated.patient_type','=','Voluntary');})
                    ->where(function($query) use ($pletaal) {$query->whereIn('patients_med_info.sku',$pletaal)->orWhere('patients_validated.patient_tagging_product_prescribe','=','Pletaal');})
    				//->whereBetween('patients_validated.created_at',[$first_day,$now])
    				->get()
    				->toArray();

    	$test = $National_md_list
    			->select('id')
    			->groupBy('doctorid')
    			->orderBy('id')
    			->get()
    			->toArray();

    				//pre($encoded);


    	return $this->hasMany('App\Http\Models\otsuka\Patient_Validated', 'patient_tagging_mr_empcode','emp_code')->whereIn('id',$voluntary)->with(['fetch_sku']);

    }

}