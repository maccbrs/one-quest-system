<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Call_status extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'call_status';   
    protected $fillable = ['id', 'name', 'created_at', 'updated_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function fetch_status(){
	   		return $this->hasMany('App\Http\Models\otsuka\Call_verification','remarks','id');
	   	} 

}