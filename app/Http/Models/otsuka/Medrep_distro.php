<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Medrep_distro extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'medrep_distro';   
    protected $fillable = ['rgc', 'team', 'new_code', 'division', 'emp_code', 'pso_name', 'type','smart','globe','email','location','psm_empcode','psm_name','psm_team','psm_email'];
    protected $hidden = [
        'password', 'remember_token',
    ];

/*    public function upload(){

        return $this->hasMany('App\Http\Models\otsuka\Receipt','connector_id','connector');

    }*/

}