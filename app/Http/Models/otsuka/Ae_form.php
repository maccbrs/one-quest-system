<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Ae_form extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'ae_form';   
    protected $fillable = [
						'date_of_first_receipt',
						'patient_initial_f',
						'patient_initial_m',
						'patient_initial_l',
						'patient_data_of_birth',
						'patient_age',
						'patient_weight',
						'patient_height',
						'patient_gender',
						'patient_is_pregnant',
						'date_of_report',
						'onset_date',
						'report_type',
						'source',
						'source_details',
						'seriousness_criteria',
						'is_death',
						'is_medically_sign',
						'is_life_threat',
						'is_hospitalize',
						'is_congenital',
						'is_disability',
						'is_non_serious',
						'is_not_reported',
						'describe_safety_info',
						'medicines_taken',
						'outcome',
						'outcome_date',
						'related_to_drug',
						'is_expected',
						'suspected_1',
						'suspected_2',
						'indication_1',
						'indication_2',
						'dose_route_1',
						'dose_route_2',
						'start_date_1',
						'start_date_2',
						'stop_date_1',
						'stop_date_2',
						'reporter_name',
						'reporter_contact',
						'reporter_profession',
						'reported_through',
						'date_reported',
						'did_reporter_consent',
						'did_reporter_provide_info',
						'date_receive_lsm',
						'created_by'];
    protected $hidden = [
        'password', 'remember_token',
    ];
	


   public function fetch_user(){
        return $this->hasOne('App\Http\Models\gem\User','id','created_by');
   }

}