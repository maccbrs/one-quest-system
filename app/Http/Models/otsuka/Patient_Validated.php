<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Patient_Validated extends Model
{

    protected $connection = 'otsuka_db';
    //protected $table = 'patients_validated_UPDATES';   
    protected $table = 'patients_validated';   
    protected $fillable = ['id','patient_code', 'enrollment_date','time_to_call','patient_kit_number','patient_consent','patient_lastname', 'patient_firstname', 'patient_middlename', 'date_encoded', 'patient_old_code', 'nickname','address','birth_date','age','gender','mobile_prefix','mobile_number','mobile_number_2','phone_prefix','phone_number','patient_tagging_product_prescribe','patient_tagging_product_sku'];
    protected $hidden = [
        'password', 'remember_token',
    ];

	 public function tempfetch(){

        return $this->hasOne('App\Http\Models\otsuka\ExportDataTest','id','id');

    }
    public function fetch_verification(){

        return $this->hasOne('App\Http\Models\otsuka\Call_verification','cmid','id');

    }

    public function fetch_receipt_encoded(){

        return $this->hasOne('App\Http\Models\otsuka\Encoded_Purchases_Validated','patient_id','patient_id')->orderBy('created_at')->with(['fetch_upload']);

    }

    public function fetch_purchased_compliance(){
        return $this->hasOne('App\Http\Models\otsuka\Encoded_Purchases_Validated','patient_id','id')->orderBy('created_at')->with(['fetch_upload']);
    }

	public function fetch_receipt_encoded_compliance(){

        return $this->hasOne('App\Http\Models\otsuka\Encoded_Purchases_Validated','patient_id','id')->groupBy('patient_id')->with(['fetch_upload']);

    }

    public function fetch_user_test(){

        return $this->hasOne('App\Http\Models\gem\User','name','updated_by');

    }
	

	
    public function fetch_mr(){

        return $this->hasOne('App\Http\Models\otsuka\Patients_kit_allocation','one_quest_id','patient_kit_number')->with(['fetch_psm_list']);

    }
	public function old_retrieval(){

        return $this->hasOne('App\Http\Models\otsuka\Old_retrieval_enrollment','patientid','id')->with(['fetch_doctors']);

    }

    public function old_retrieval_report(){

        return $this->hasOne('App\Http\Models\otsuka\Old_retrieval_enrollment','patientid','id')->with(['fetch_product','fetch_sku','fetch_doctors']);

    }

	
    public function fetch_redeem_product(){

        return $this->hasMany('App\Http\Models\otsuka\Dispatch_status','patient_id','id')->where('tag_deleted',0)->with('sku_product')->orderby('id','desc');

    }

    public function fetch_compliance_upload(){
        return $this->hasOne('App\Http\Models\otsuka\Upload','patient_kit_number','patient_kit_number')->with(['fetch_call']);
    }

    public function fetch_firstredeem(){
        return $this->hasOne('App\Http\Models\otsuka\Dispatch_status','patient_id','id');
    }

    public function fetch_sku(){
        //return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
        return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_id','id')->with(['fetch_prescribed','fetch_doctor','fetch_doctor_summary']);
    }   

    public function fetch_sku_vol_ref(){
        //return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
        return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_id','patient_id')->with(['fetch_prescribed','fetch_doctor','fetch_doctor_abilify','fetch_doctor_aminoleban','fetch_doctor_pletaal','fetch_mr_vol']);
    }

    public function fetch_sku_vol_ref_compliance(){
        //return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
        return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_id','id')->with(['fetch_prescribed','fetch_doctor','fetch_doctor_abilify','fetch_doctor_aminoleban','fetch_doctor_pletaal']);
    }

    public function fetch_sku_aminoleban(){
        //return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
        return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','id','patient_id')->whereIn('sku',[9,10,11,41]);
    }

    public function fetch_sku_abilify(){
        //return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
        return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','id','patient_id')->whereBetween('sku',[1,8]);
    }

    public function fetch_sku_pletaal(){
        //return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
        return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','id','patient_id')->whereBetween('sku',[20,24]);
    }
    public function fetch_sku_compliance(){

        return $this->hasOne('App\Http\Models\otsuka\Sku','skuid','skuid');

    }

    public function fetch_sku_test(){
        //return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
        return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
    }

    public function fetch_medinfo(){
        //return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
        return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
    }

    public function fetch_cmid_test(){
       return $this->hasOne('App\Http\Models\otsuka\Cmid','patientid','id'); 
    }

    public function fetch_compliance_patient_count(){
        return $this->hasMany('App\Http\Models\otsuka\Compliance','patientid','id');
    }
	
	public function fetch_last_compliance(){
        return $this->hasOne('App\Http\Models\otsuka\Compliance','patientid','id')->orderBy('id','desc')->limit(1);
    }
	
	public function fetch_supp_docs(){
        //return $this->hasOne('App\Http\Models\otsuka\Patients_med_info','patient_code','patient_code')->with(['fetch_prescribed']);
        return $this->hasOne('App\Http\Models\otsuka\UploadSupportingDocs','patient_id','id')->where('tag_deleted',0)->orderby('id','desc');
    }
	
	
	

}