<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Queue_call_verification extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'queue_call_verification';   
    protected $fillable = ['id', 'patient_code', 'active', 'connector','created_at','updated_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fetch_patient(){

        return $this->hasMany('App\Http\Models\otsuka\Patient','patient_code','patient_code');

    }

    public function fetch_callverification(){

        return $this->hasMany('App\Http\Models\otsuka\Call_verification','cmid','cmid')->where('allowcallback',1);

    }

    public function fetch_callverification_last(){

        return $this->hasMany('App\Http\Models\otsuka\Call_verification','cmid','cmid')->where('allowcallback',-1)->orderby('updated_at','desc')->limit(1);

    }

    public function fetch_upload(){

        return $this->hasMany('App\Http\Models\otsuka\Upload','connector_id','connector');

    }

    public function fetch_status(){

        return $this->hasOne('App\Http\Models\otsuka\Call_status','id','remarks');

    }

}