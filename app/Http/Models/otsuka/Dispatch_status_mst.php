<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Dispatch_status_mst extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'psp_dispatch_mst';   
    protected $fillable = ['id','dsid','patient_id','completiondate','programid','brandid','skuid','qtytabs','qtynonmed','dispatched','dispatcheddate','active','etimestamp'
];
    protected $hidden = [
        'password', 'remember_token',
    ];
 


	public function sku_product(){

        return $this->hasOne('App\Http\Models\otsuka\Sku','id','skuid');

    }
		public function fetch_brand_desc(){

        return $this->hasOne('App\Http\Models\otsuka\Brand','id','brandid');

    }
	
	public function fetch_patient_details(){
        return $this->hasOne('App\Http\Models\otsuka\Patient_Validated','id','patient_id');
    }
	
	
}