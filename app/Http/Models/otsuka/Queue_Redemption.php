<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Queue_Redemption extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'queue_redemption';   
    protected $fillable = ['id', 'patient_code', 'active', 'connector','upload_id','created_at','updated_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fetch_patient(){

        return $this->hasMany('App\Http\Models\otsuka\Patient','patient_code','patient_code');

    }

    public function fetch_callverification(){

        return $this->hasMany('App\Http\Models\otsuka\Call_verification','cmid','cmid')->where('allowcallback',1);

    }

    public function fetch_callverification_last(){

        return $this->hasMany('App\Http\Models\otsuka\Call_verification','cmid','cmid')->where('allowcallback',-1)->orderby('updated_at','desc')->limit(1);

    }

	
	public function fetch_upload_id(){

        return $this->hasOne('App\Http\Models\otsuka\Upload','id','upload_id');

    }
	
    public function fetch_upload(){

        return $this->hasMany('App\Http\Models\otsuka\Queue','connector','connector');

    }

    public function fetch_status(){

        return $this->hasOne('App\Http\Models\otsuka\Call_status','id','remarks');

    }

}