<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Txt_templates extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'txt_templates';   
    protected $fillable = ['id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
	

}