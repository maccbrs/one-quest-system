<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Patients_kit_allocation_reserve extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'patients_kit_allocation_reserve';   
    protected $fillable = ['id', 'date_of_request','time_requested', 'one_quest_id', 'year', 'month', 'team', 'area_code','emp_code','mr_name','product','requested_by','date_forwarded_to_warehouse','time_forwareded_to_warehouse','created_at','created_by','updated_at','updated_by'];
    protected $hidden = [
        'password', 'remember_token',
    ];





}