<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use DB;

class Encoded_Purchases_Item_Dtl_Validated extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'encoded_purchases_item_dtl_validated';   
    protected $fillable = ['id','encoded_purchases_id','sku','qty','active','ds_id','remarks'];
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function fetch_encoded_purchase()
    {
        return $this->hasOne('App\Http\Models\otsuka\Encoded_Purchases_Validated');
    }
	
	 public function sku_details(){
        return $this->hasOne('App\Http\Models\otsuka\Sku','id','sku');
   }
  


}