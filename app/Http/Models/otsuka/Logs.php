<?php namespace App\Http\Models\otsuka;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Logs extends Model
{

    protected $connection = 'otsuka_db';
    protected $table = 'logs';   
    protected $fillable = ['description','user','user_id'];
   
   public function user(){
   		return $this->hasOne('App\Http\Models\gem\GemUser','id','user_id');
   }  

    
	

}