<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class FormFields extends Model
{

    protected $connection = 'sunstone';
    protected $table = 'form_fields';   
    public $timestamps = false;
    protected $fillable = ['key','label','required_status','type','option_values','label_values','status','size'];

    public function nullchecker($formfields = null, $request = null){

    	foreach ($formfields as $key => $value) {

    		if($value['required_status'] == 'required'){
    	
    			if(empty($request[$value['key']])){
    				
    				return 0;
    			}
  	
    		}
    	}

    	return 1;
    }

}