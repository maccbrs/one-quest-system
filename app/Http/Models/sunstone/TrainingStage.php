<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class TrainingStage extends Model
{

    protected $connection = 'magnetite';
    protected $table = 'training_stages';   
    public $timestamps = false;
    protected $fillable = ['trainee_id','stage','wave_id','status','camp_id'];


}