<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class Assessment extends Model
{
    protected $connection = 'sunstone';
    protected $table = 'assessments';
    protected $fillable = ['name', 'passing_rate'];


    public function positions(){
        return $this->belongsToMany('App\Http\Models\sunstone\Position');
    }

    public function questions(){
    return $this->belongsToMany('App\Http\Models\sunstone\Question');
    }

}