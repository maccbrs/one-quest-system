<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class Position extends Model
{

    protected $connection = 'sunstone';
    protected $table = 'positions';   
    public $timestamps = false;
    protected $fillable = ['name','status','type', 'assessment_id'];

    public function applicant()
    {
        return $this->hasMany('App\Http\Models\sunstone\Applicant');
    }

    public function assessments(){
        return $this->belongsToMany('App\Http\Models\sunstone\Assessment');
    }

}