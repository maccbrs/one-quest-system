<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class Exam extends Model
{
    protected $connection = 'sunstone';
    protected $table = 'exams';
    protected $fillable = ['name', 'done', 'passing_rate'];

    public function applicant(){
        return $this->belongsTo('App\Http\Models\sunstone\Applicant');
    }

    public function exam_questions(){
        return $this->hasMany('App\Http\Models\sunstone\ExamQuestion');
    }

    public function get_points(){
        $points = 0;
        foreach ($this->exam_questions as $question) {
            if ($question->applicant_answer == $question->correct_answer) {
                $points++;
            }
        }
        return $points;
    }

    public function get_score(){
        $points = 0;
        $question_count = count($this->exam_questions);
        foreach ($this->exam_questions as $question){
            if($question->applicant_answer == $question->correct_answer){
                $points++;
            }
        }
        return ($points / $question_count) * 100;
    }

    public function is_passed(){
        return ($this->get_score() < $this->passing_rate) ? false : true;
    }

}