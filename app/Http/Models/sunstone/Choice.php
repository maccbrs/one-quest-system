<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class Choice extends Model
{
    protected $connection = 'sunstone';
    protected $table = 'choices';
    protected $fillable = ['body', 'is_correct_answer'];

    public function question(){
        return $this->belongsTo('App\Http\Models\sunstone\Question');
    }
}