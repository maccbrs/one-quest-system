<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class ExamQuestion extends Model
{
    protected $connection = 'sunstone';
    protected $table = 'exam_questions';
    protected $fillable = ['body', 'image', 'correct_answer', 'time_limit', 'applicant_answer'];

    public function choices(){
        return $this->hasMany('App\Http\Models\sunstone\ExamChoice');
    }

    public function exam(){
        return $this->belongsTo('App\Http\Models\sunstone\Exam');
    }

}