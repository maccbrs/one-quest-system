<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class PendingNonAgent extends Model
{

    protected $connection = 'sunstone';
    protected $table = 'pending_non_agents';   
    public $timestamps = false;
    protected $fillable = ['applicant','status'];




}