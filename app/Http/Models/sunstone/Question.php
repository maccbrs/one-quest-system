<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class Question extends Model
{
    protected $connection = 'sunstone';
    protected $table = 'questions';
    protected $fillable = ['body', 'image', 'time_limit', 'correct_answer'];

    public function assessments(){
        return $this->belongsToMany('App\Http\Models\sunstone\Assessment');
    }

    public function choices(){
        return $this->hasMany('App\Http\Models\sunstone\Choice');
    }

}