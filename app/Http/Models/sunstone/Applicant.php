<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class Applicant extends Model
{
    protected $connection = 'sunstone';
    protected $table = 'applicants';
    protected $fillable = ['source_date', 'last_name', 'middle_name', 'first_name', 'position_id',
    'additional', 'status', 'user_id', 'attachment'];

    public function camp(){
      return $this->hasOne('App\Http\Models\sunstone\TrainingStage','trainee_id','id');
    }

    public function position(){
      return $this->hasOne('App\Http\Models\sunstone\Position','id','position_id');
    }


    public function exams(){
        return $this->hasMany('App\Http\Models\sunstone\Exam', 'applicant_id');
    }

    public function interviews(){
        return $this->hasMany('App\Http\Models\sunstone\Interview', 'applicant_id');
    }


}