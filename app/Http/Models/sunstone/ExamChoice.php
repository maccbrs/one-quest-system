<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class ExamChoice extends Model
{
    protected $connection = 'sunstone';
    protected $table = 'exam_choices';
    protected $fillable = ['body', 'is_correct_answer'];

    public function question(){
        return $this->belongsTo('App\Http\Models\sunstone\ExamQuestion', 'exam_question_id');
    }
}