<?php namespace App\Http\Models\sunstone;

use Illuminate\Database\Eloquent\Model;
use DB;

class Interview extends Model
{
    protected $connection = 'sunstone';
    protected $table = 'interviews';
    protected $fillable = ['remarks', 'interviewer_name', 'type'];

    public function applicant(){
        return $this->belongsTo('App\Http\Models\sunstone\Applicant');
    }

}