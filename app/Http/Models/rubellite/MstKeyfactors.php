<?php namespace App\Http\Models\rubellite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstKeyfactors extends Model
{

    protected $connection = 'rubellite';
    protected $table = 'mst_keyfactors';   
    protected $fillable = ['id', 'name','alias','remarks'];
	protected $displaycolumn = ['id','name','alias','remarks'];
   
/*    	public function dept(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','dept_id','id');
   	}
     */
	

}