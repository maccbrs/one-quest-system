<?php namespace App\Http\Models\rubellite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstCampaigns extends Model
{

    protected $connection = 'rubellite';
    protected $table = 'mst_campaigns';   
    protected $fillable = ['id', 'name','alias','remarks'];
	protected $displaycolumn = ['id','name','alias','remarks'];
   
  /*  	public function brand(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','brand_id','id');
   	} 
 */
    
	

}