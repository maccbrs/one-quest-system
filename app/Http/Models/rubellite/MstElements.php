<?php namespace App\Http\Models\rubellite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstElements extends Model
{

    protected $connection = 'rubellite';
    protected $table = 'mst_elements';   
    protected $fillable = ['id','mst_keyfactor_id','name','alias','remarks'];
	protected $displaycolumn = ['id','name','mst_keyfactor_id','alias','remarks'];
   
   
  	public function mstkeyfactor(){
   		return $this->hasMany('App\Http\Models\rubellite\MstKeyfactors','mst_keyfactor_id','id');
   	}
    
	

}