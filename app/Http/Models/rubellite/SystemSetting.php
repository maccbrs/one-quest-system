<?php namespace App\Http\Models\rubellite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SystemSetting extends Model
{

    protected $connection = 'rubellite';
    protected $table = 'system_setting';   
    protected $fillable = ['id', 'key','value','remarks'];
	protected $displaycolumn = ['id','key','value','remarks'];
   
   
/*    	public function dept(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','dept_id','id');
   	}
     */
	

}