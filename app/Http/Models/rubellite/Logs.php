<?php namespace App\Http\Models\rubellite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Logs extends Model
{

    protected $connection = 'bloodstone';
    protected $table = 'logs';   
    protected $fillable = ['description','user','user_id'];
	protected $displaycolumn = ['description','user','user_id'];
   
   public function user(){
   		return $this->hasOne('App\Http\Models\gem\GemUser','id','user_id');
   }  

    
	

}