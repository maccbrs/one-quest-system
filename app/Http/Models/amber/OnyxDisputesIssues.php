<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OnyxDisputesIssues extends Model
{

   protected $connection = 'onyx';
   protected $table = 'dispute_issues';
   protected $fillable = ['issue_name','status','created_at','updated_at'];

   // public function scopeSelf($query)
   // {
   //     return $query->where('user_id',Auth::user()->id);
   // } 

} 