<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use DB;

class GemUser extends Model
{

    protected $connection = 'gem';
    protected $table = 'users';   
    protected $fillable = ['name', 'email', 'password','user_type','access'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function garnet($userId = null){

    	$ps_data = DB::connection('garnet')
    		->table('project_supervisors')
    		->where('user_id',$userId)
    		->first();

    	$superior_data = $ps_data;


    	if(empty($ps_data)){

	    	$tl_data = DB::connection('garnet')
	    		->table('team_leads')
	    		->where('user_id',$userId)
	    		->first();

	    	$superior_data = $tl_data;

    	}

		return $superior_data;

   	} 

}