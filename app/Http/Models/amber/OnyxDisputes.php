<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OnyxDisputes extends Model
{

   protected $connection = 'onyx';
   protected $table = 'disputes';
   public $timestamps = false;
   protected $fillable = ['user','dispute_period','dispute_date','dispute_time','notes','issue','tl_response','tl_comments','ra_response','ra_comments','hr_response','hr_comments','created_at','updated_at'];


   public function scopeSelf($query)
   {
       return $query->where('user',Auth::user()->id);
   } 

   public function scopeRa($query)
   {
       return $query->where('user_type',1)->where('ra_response',1);
   } 

   public function scopeHr($query)
   {
       return $query->where('user_type',1)->where('hr_response',1);
   } 

   public function userObj(){
   		return $this->hasOne('App\Http\Models\amber\GemUser','id','user');
   }  

   public function period(){
   		return $this->hasOne('App\Http\Models\amber\OnyxDisputesDates','id','dispute_period');
   }  

   public function issueObj(){
   		return $this->hasOne('App\Http\Models\amber\OnyxDisputesIssues','id','issue');
   }  
} 