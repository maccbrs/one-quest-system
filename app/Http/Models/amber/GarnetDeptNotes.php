<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetDeptNotes extends Model
{

   protected $connection = 'garnet';
   protected $table = 'dept_notes';
   public $timestamps = false;

} 