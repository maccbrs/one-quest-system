<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetTeamLeads extends Model
{

   protected $connection = 'garnet';
   protected $table = 'team_leads';
   public $timestamps = false;
   protected $fillable = ['last_name','first_name','middle_name','status','user_id','createdAt','updatedAt'];

   public function scopeSelf($query)
   {
       return $query->where('user_id',Auth::user()->id);
   } 

   public function agents(){
   		return $this->hasMany('App\Http\Models\amber\GarnetAgents','tl_id')->where('status',1);
   }     



}  