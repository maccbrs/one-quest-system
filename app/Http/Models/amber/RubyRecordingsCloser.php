<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class RubyRecordingsCloser extends Model
{

   protected $connection = 'ruby';
   protected $table = 'recordings_closer';
   protected $timestamp = false;
   protected $fillable = ['uniqueid','call_date','status','agent','length_in_min','location' ,'filename' ,'lead_id','campaign_id','audited' ,'audit_id' ,'createdAt' ,'updatedAt','checked','closecallid','week_no','month','year','group','task_type','monitoring_type','evaluator','is_complete' ,'week_days','options','locked','score','phone','term_reason'];

  
   

}