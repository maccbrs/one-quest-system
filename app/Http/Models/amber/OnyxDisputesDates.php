<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OnyxDisputesDates extends Model
{

   protected $connection = 'onyx';
   protected $table = 'dispute_dates';
   public $timestamps = false;
   protected $fillable = ['dispute_label','from','to','status','created_at','updated_at'];

   // public function scopeSelf($query)
   // {
   //     return $query->where('user_id',Auth::user()->id);
   // } 

} 