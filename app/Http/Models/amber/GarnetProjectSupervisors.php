<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetProjectSupervisors extends Model
{

   protected $connection = 'garnet';
   protected $table = 'project_supervisors';
   public $timestamps = false;
   protected $fillable = ['last_name','first_name','middle_name','status','createdAt','updatedAt'];



}  