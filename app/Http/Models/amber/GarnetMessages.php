<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetMessages extends Model
{

   protected $connection = 'garnet';
   protected $table = 'messages';
   public $timestamps = false;
   protected $fillable = ['user','subject','content','to','type','status','page','created_at','updated_at'];

   public function get_manager_message($pro_sup,$userId)
   	{

		$messages_to = $this
			->where('to',$userId)
			->where('user',$pro_sup)
			->whereIn('page',['manager','agents']);

		$messages_user = $this
			->where('user',$userId)
			->where('to',$pro_sup)
			->whereIn('page',['manager']);

		$messages_list = $messages_to
			->union($messages_user)
			->orderBy('created_at','desc')
			->get();


		foreach ($messages_list as $key => $value) {

			$messages_list[$key]['content'] = json_decode($value['content']);
			
		}


       return $messages_list;
   	} 


  	public function get_tl_message($tl_sup,$userId)
   	{

		$messages_to = $this
				->where('to',$userId)
				->where('user',$tl_sup)
				->whereNotIn('page',['team_leader','agent']);

			$messages_user = $this
				->where('user',$userId)
				->where('to',$tl_sup)
				->whereIn('page',['team_leader']);;

			$messages_list = $messages_to
				->union($messages_user)
				->orderBy('created_at','desc')
				->get();


		foreach ($messages_list as $key => $value) {

			$messages_list[$key]['content'] = json_decode($value['content']);
			
		}


       return $messages_list;
   	} 

   	public function get_message($userId)
   	{

   		$messages_to = $this
			->where('to',$userId);
			
		$messages_user = $this
			->where('user',$userId);
	
		$messages_list = $messages_to
			->union($messages_user)
			->orderBy('id','desc')
			->get();

		foreach ($messages_list as $key => $value) {

			$messages_list[$key]['content'] = json_decode($value['content']);
			
		}


       return $messages_list;
   	} 

   	public function scopeSource($query,$id)
   {
   		
       return $query->where('to',$id);
   } 

    public function get_conversation($recipient,$userId)
    {

      $messages_list = $this
       ->whereIn('to',[$userId,$recipient])
       ->whereIn('user',[$userId,$recipient])
       ->whereNotIn('page',['group'])
       ->orderBy('id','desc')
       ->get();
    
      foreach ($messages_list as $key => $value) {

       $messages_list[$key]['content'] = json_decode($value['content']);
      
      }

        return $messages_list;
    } 

    public function get_group_conversation($recipient,$userId)
    {

    	//print_r($recipient); exit; 
      $messages_list = $this
       ->whereIn('to',[$recipient])
       ->whereIn('page',['group'])
       ->orderBy('id','desc')
       ->get();
    
      foreach ($messages_list as $key => $value) {

       $messages_list[$key]['content'] = json_decode($value['content']);
      
      }

        return $messages_list;
    } 


} 