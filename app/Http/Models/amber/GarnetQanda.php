<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetQanda extends Model
{

   protected $connection = 'garnet';
   protected $table = 'qanda';
   protected $fillable = ['type','inquiry','responses','created_at','updated_at','userid_from','userid_to','dept_from','dept_to'];

   public function user(){
   		return $this->hasOne('App\Http\Models\gem\GemUser','id','userid_from');
   }     



}  