<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetNewsFeeds extends Model
{

   protected $connection = 'garnet';
   protected $table = 'news_feeds';
   public $timestamps = false;
   protected $fillable = ['type','tags','status','reactions','created_for','created_at'];


}  