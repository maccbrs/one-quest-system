<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetMessageOpened extends Model
{

   protected $connection = 'garnet';
   protected $table = 'opened_messages';
   public $timestamps = false;
   protected $fillable = ['user_id','message_id'];


   public function scopeSelf($query)
   {
       return $query->where('user_id',Auth::user()->id);
   } 

   public function scopeSource($query,$source)
   {
       return $query->where('source',$source);
   } 

} 