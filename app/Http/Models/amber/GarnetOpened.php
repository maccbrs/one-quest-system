<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetOpened extends Model
{

   protected $connection = 'garnet';
   protected $table = 'opened';
   public $timestamps = false;
   protected $fillable = ['user_id','announcements_id'];


   public function scopeSelf($query)
   {
       return $query->where('user_id',Auth::user()->id);
   } 

   public function scopeSource($query,$source)
   {
       return $query->where('source',$source);
   } 

} 