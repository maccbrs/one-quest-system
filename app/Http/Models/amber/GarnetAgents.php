<?php namespace App\Http\Models\amber;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetAgents extends Model
{

   protected $connection = 'garnet';
   protected $table = 'agents';
   public $timestamps = false;
   protected $fillable = ['last_name','first_name','middle_name','ps_id','username','tl_id','status','evaluator_id','user_id','createdAt','updatedAt'];

   public function scopeSelf($query)
   {
       return $query->where('user_id',Auth::user()->id);
   } 

    public function get_superiors($userId)
   	{
       return $this->select('id','tl_id','ps_id')->where('user_id',$userId)->first();
   	} 

    public function get_agent($userId =null)
    {

      $tl_agents = $this
        ->select('id','first_name', 'last_name', 'user_id')
        ->where('tl_id',$userId)
        ->whereNotNull('user_id');

      $ps_agents =  $this
        ->select('id','first_name', 'last_name', 'user_id')
        ->where('ps_id',$userId)
        ->whereNotNull('user_id');

      $all_agents = $tl_agents
        ->union($ps_agents)
        ->orderBy('id','desc')
        ->get();  

      return  $all_agents;   
    } 


} 