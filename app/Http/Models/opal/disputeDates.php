<?php namespace App\Http\Models\opal;

use Illuminate\Database\Eloquent\Model;
use Auth;

class disputeDates extends Model
{
    protected $connection = 'onyx';
   	protected $table = 'dispute_dates';
   	public $timestamps = false;
   	protected $fillable = ['dispute_label','status','from','to'];

}
