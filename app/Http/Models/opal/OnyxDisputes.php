<?php namespace App\Http\Models\opal;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OnyxDisputes extends Model
{

   protected $connection = 'onyx';
   protected $table = 'disputes';
   public $timestamps = false;
   protected $fillable = ['user','user_type','sup_response','sup_comments','dispute_period','dispute_date','dispute_time','notes','issue','tl_response','tl_comments','created_at','updated_at'];


   public function userObj(){
      return $this->hasOne('App\Http\Models\gem\User','id','user');
   }  

   public function periodObj(){
      return $this->hasOne('App\Http\Models\gem\OnyxDisputesDates','id','dispute_period');
   }  

   public function issueObj(){
      return $this->hasOne('App\Http\Models\gem\OnyxDisputesIssues','id','issue');
   }     

   public function scopePeriod($query,$id){
          return $query->where('dispute_period',$id);
   }  

   public function scopeApproved($query){
          return $query->where('hr_response',2);
   }  

}  