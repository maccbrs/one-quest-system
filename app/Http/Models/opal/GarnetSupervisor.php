<?php namespace App\Http\Models\opal;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Searchable;

class GarnetSupervisor extends Model
{

   protected $connection = 'garnet';
   protected $table = 'project_supervisors';
   public $timestamps = false;
   protected $fillable = ['last_name','first_name','middle_name','status','createdAt','updatedAt','user_id','createdAt','updatedAt'];

    public function gem(){

      return $this->hasOne('App\Http\Models\opal\GemUsers','id','user_id')->select();
    
    } 
} 