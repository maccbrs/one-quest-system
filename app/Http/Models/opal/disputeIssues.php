<?php namespace App\Http\Models\opal;

use Illuminate\Database\Eloquent\Model;
use Auth;

class disputeIssues extends Model
{
    protected $connection = 'onyx';
   	protected $table = 'dispute_issues';
   	public $timestamps = false;
   	protected $fillable = ['issue_name','status'];

}
