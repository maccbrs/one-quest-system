<?php namespace App\Http\Models\opal;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetCampaign extends Model
{

   protected $connection = 'garnet';
   protected $table = 'campaigns';
   protected $fillable = ['title','created_at','updated_at'];


} 