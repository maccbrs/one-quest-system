<?php namespace App\Http\Models\opal;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OnyxClearance extends Model
{

   protected $connection = 'onyx';
   protected $table = 'clearances';
   public $timestamps = false;
   protected $fillable = ['admin','it','operations','hr','user_id'];

    public function gem(){

      return $this->hasOne('App\Http\Models\opal\GemUsers','id','user_id')->select();
    
    } 

}  