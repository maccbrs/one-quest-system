<?php namespace App\Http\Models\opal;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OpalAnnouncements extends Model
{

   protected $connection = 'garnet';
   protected $table = 'announcements';
   public $timestamps = false;
   protected $fillable = ['title','excerpt','content','source','destination','user_id','created_at','updated_at'];

   public function scopeSource($query,$id)
   {
       return $query->where('source',$id);
   } 

}  