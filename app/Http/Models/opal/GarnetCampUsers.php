<?php namespace App\Http\Models\opal;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetCampUsers extends Model
{

   protected $connection = 'garnet';
   protected $table = 'camp_users';
   protected $fillable = ['camp_id','agent_id','tl_id','sup_id','created_at','updated_at'];

   public function scopeSup($query,$id)
   {
       return $query->where('sup_id',$id);
   } 

   public function scopeCamp($query,$id)
   {
       return $query->where('camp_id',$id);
   } 


   public function scopeCampaignSup($query,$id)
   {
       return $query->where('camp_id',$id)->whereNotNull('sup_id');
   } 
   public function scopeCampaignTl($query,$id)
   {
       return $query->where('camp_id',$id)->whereNotNull('tl_id');
   } 
   public function scopeCampaignAgent($query,$id)
   {
       return $query->where('camp_id',$id)->whereNotNull('agent_id');
   } 
   public function supObj(){
   		return $this->hasOne('App\Http\Models\opal\GarnetSupervisor','id','sup_id');
   }    

   public function tlObj(){
   		return $this->hasOne('App\Http\Models\opal\GarnetTeamLeads','id','tl_id');
   } 

   public function agentObj(){
   		return $this->hasOne('App\Http\Models\opal\GarnetAgents','id','agent_id');
   } 
}  