<?php namespace App\Http\Models\opal;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OpalOpened extends Model
{

   protected $connection = 'garnet';
   protected $table = 'opened';
   protected $fillable = ['user_id','announcements_id','created_at','updated_at'];

   public function userObj(){
      return $this->hasOne('App\Http\Models\opal\GemUsers','id','user_id');
   } 
}   