<?php namespace App\Http\Models\opal;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Searchable;

class GarnetTeamLeads extends Model
{

   protected $connection = 'garnet';
   protected $table = 'team_leads';
   public $timestamps = false;
   protected $fillable = ['last_name','first_name','middle_name','ps_id','username','tl_id','status','evaluator_id','user_id','createdAt','updatedAt'];

   	public function gem(){

      return $this->hasOne('App\Http\Models\opal\GemUsers','id','user_id')->select();
    
    } 
} 