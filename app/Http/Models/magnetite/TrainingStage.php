<?php namespace App\Http\Models\magnetite;

use Illuminate\Database\Eloquent\Model;
use DB;

class TrainingStage extends Model
{

    protected $connection = 'magnetite';
    protected $table = 'training_stages';   
    public $timestamps = false;
    protected $fillable = ['trainee_id','stage','wave_id','status'];

    public function wave(){
      return $this->hasOne('App\Http\Models\magnetite\Waves','id','wave_id')->select('id','name','camp_id','trainer_id');
    }

    public function trainee(){
      return $this->hasOne('App\Http\Models\magnetite\Applicant','id','trainee_id')->select('id','last_name','middle_name','first_name','status');
    }  

    public function camp(){
      return $this->hasMany('App\Http\Models\magnetite\Campaign','id','camp_id')->select('alias','title');
    }  

    public function getcamp_waves($id){

   		$wave_data = new \App\Http\Models\magnetite\Waves;

    	$wave = $wave_data->where('id',$id)->orderBy('id', 'desc')->first(); 
    	
    	return $wave;
   	}	 

   	public function getagents($waves){

   		foreach ($waves as $key => $value) {
   			//	print_r($value); exit;
   			$waves_collection[] = $this->where('id','=',$value['wave_id'])->get();

   		}



   		$wave_data = new \App\Http\Models\magnetite\Waves;

    	$wave = $wave_data->where('id',$id)->orderBy('id', 'desc')->first(); 
    	
    	return $wave;
   	}



}