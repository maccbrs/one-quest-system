<?php namespace App\Http\Models\magnetite;

use Illuminate\Database\Eloquent\Model;
use DB;

class Campaign extends Model
{
    protected $connection = 'garnet';
    protected $table = 'campaigns';   
    public $timestamps = false;
    protected $fillable = ['title','status','alias'];


}