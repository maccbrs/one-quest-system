<?php namespace App\Http\Models\magnetite;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemUsers extends Model
{

   protected $connection = 'gem';
   protected $table = 'users';
   protected $fillable = ['name','email','department_id','password','options','user_type','access','avatar','routes','status','active','created_at','updated_at','auditor','is_representative'];

   public function scopeAgent($query)
   {
       return $query->where('is_representative',1);
   } 

    public function access_checker($access)
   {
   		$accept = null;

       	if($access['type'] == 'Administrator'){

			$accept = 'abled';

       	}else{

       		if($access['level'] == 2){

       			$accept = 'abled';
       		}else{

       			$accept = 'disabled';
       		}
       	}	

       	return $accept;

   } 

} 