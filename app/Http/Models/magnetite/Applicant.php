<?php namespace App\Http\Models\magnetite;

use Illuminate\Database\Eloquent\Model;
use DB;

class Applicant extends Model
{

    protected $connection = 'sunstone';
    protected $table = 'applicants';   
    public $timestamps = false;
    protected $fillable = ['last_name','middle_name','first_name','contact_number','email','address','additional','status','user_id'];

    public function stage(){

      return $this->hasOne('App\Http\Models\magnetite\TrainingStage','trainee_id','id')->select('id','trainee_id','stage','wave_id');
    
    } 

   	public function getcamp($id){

   		$camp = new \App\Http\Models\sunstone\CampUser;

    	$campaign = $camp->select('id','camp_id')->where('trainee_id',$id)->orderBy('id', 'desc')->first(); 
    	
    	return $campaign;
   	}	 

    public function getwave($id){

   		$stage = new \App\Http\Models\magnetite\TrainingStage;

    	$tr_stage = $stage->select('id','wave_id')->where('trainee_id',$id)->orderBy('id', 'desc')->first(); 

    	return $tr_stage;
   } 




}