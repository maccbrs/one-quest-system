<?php namespace App\Http\Models\magnetite;

use Illuminate\Database\Eloquent\Model;
use DB;

class CampUser extends Model
{
    protected $connection = 'garnet';
    protected $table = 'camp_users';   
    public $timestamps = false;
    protected $fillable = ['trainee_id','camp_id'];


}