<?php namespace App\Http\Models\nexus;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DboCalldetailViw extends Model
{

   protected $connection = 'nexus';
   protected $table = 'dbo.calldetail_viw';
   protected $timestamp = false;
   protected $fillable = ['CallId'];


}