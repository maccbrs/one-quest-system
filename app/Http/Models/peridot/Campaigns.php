<?php namespace App\Http\Models\ruby;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Campaigns extends Model
{

   protected $connection = 'diamond';
   protected $table = 'mst_campaigns';
   protected $fillable = ['bound','title','campaigns','created_at','updated_at'];
  
}