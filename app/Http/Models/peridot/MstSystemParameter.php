<?php namespace App\Http\Models\peridot;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstSystemParameter extends Model
{

   protected $connection = 'peridot';
   protected $table = 'mst_system_parameters';
   protected $fillable = ['id','parameter_key','parameter_val','updated_at','updated_by'];
   
  
  
}   