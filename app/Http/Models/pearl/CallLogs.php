<?php namespace App\Http\Models\pearl;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CallLogs extends Model
{

   protected $connection = 'pearl';
   protected $table = 'call_logs';
   protected $timestamp = false;
   protected $fillable = ['length_in_min','length_in_sec','end_time','start_time','number_dialed','uniqueid','createdAt','updatedAt'];


} 