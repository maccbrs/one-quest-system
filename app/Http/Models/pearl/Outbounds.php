<?php namespace App\Http\Models\pearl;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Outbounds extends Model
{

   protected $connection = 'pearl';
   protected $table = 'outbounds';
   protected $fillable = ['campaign_id','call_date','length_in_sec','status','phone_number','user','uniqueid','createdAt','updatedAt'];
   protected $hidden = [
	    'closecallid', 'lead_id','list_id','end_epoch','start_epoch'
   ];



} 