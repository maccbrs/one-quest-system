<?php namespace App\Http\Models\pearl;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CachedReports extends Model
{

   protected $connection = 'pearl';
   protected $table = 'cached_reports';
   protected $fillable = ['campaign_id','content','from','to','filters','created_at','updated_at'];


} 