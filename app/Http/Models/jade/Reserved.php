<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Reserved extends Model
{

   protected $connection = 'jade';
   protected $table = 'reserved';
   protected $fillable = ['subscriber_id','minutes','used','remaining','status','breakdown'];

   public function subscriber(){
      return $this->hasOne('App\Http\Models\jade\Subscribers','id','subscriber_id');
   }    
   
} 