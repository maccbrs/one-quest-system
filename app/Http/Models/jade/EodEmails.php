<?php namespace App\Http\Models\eod;

use Illuminate\Database\Eloquent\Model;
use Auth;

class EodEmails extends Model
{
   protected $connection = 'rhyolite';
   protected $table = 'eod_emails';
   protected $timestamp = false;
   protected $fillable = ['sent_time','timezone','timezone2','from','from2','to','to2','sender','receiver','cc','dialer_data','dboard_data','sent','title'];

}