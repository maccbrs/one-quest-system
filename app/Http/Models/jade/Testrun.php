<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Testrun extends Model
{

   protected $connection = 'jade';
   protected $table = 'testrun';
   protected $fillable = ['transaction_id','did','load_type','load_id','durations','subscriber_id','verified'];

  

}