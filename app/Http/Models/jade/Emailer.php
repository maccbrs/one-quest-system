<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Emailer extends Model
{

   protected $connection = 'jade';
   protected $table = 'emailer';
   protected $fillable = ['subscriber_id','to','cc'];

}