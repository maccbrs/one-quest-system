<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Loads extends Model
{

   protected $connection = 'jade';
   protected $table = 'loads';
   protected $fillable = ['id','subscriber_id','used','minutes','remaining','status','startdate','enddate','month','year','breakdown'];

   public function subscriber(){
      return $this->hasOne('App\Http\Models\jade\Subscribers','id','subscriber_id');
   }    
   public function overage(){
      return $this->hasOne('App\Http\Models\jade\Reserved','id','subscriber_id');
   }   
   public function scopeCurrent($query)
   {
       return $query->where('month',date('m'))->where('year',date('Y')); 
   } 

}