<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Users extends Model
{

   protected $connection = 'jade';
   protected $table = 'users';
   protected $fillable = ['gem_id','type','allowed','name'];

}