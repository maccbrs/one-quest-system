<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Subscribers extends Model
{

   protected $connection = 'jade';
   protected $table = 'subscribers';
   protected $fillable = ['client','email','alias','status','account_status'];

   public function dids(){
   		return $this->hasMany('App\Http\Models\jade\Connectors','subscriber_id')->where('status',1);
   } 

}