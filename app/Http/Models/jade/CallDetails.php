<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CallDetails extends Model
{

   protected $connection = 'jade';
   protected $table = 'call_details';
   protected $fillable = ['transaction_id','did','load_type','load_id','durations','subscriber_id','verified','campaign_id'];

  

}