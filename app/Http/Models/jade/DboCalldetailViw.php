<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DboCalldetailViw extends Model
{

   protected $connection = 'nexus';
   protected $table = 'dbo.calldetail_viw';
   protected $timestamp = false;
   protected $fillable = ['CallId'];

   public function dispo(){
   		return $this->hasOne('App\Http\Models\eod\InteractionWrapup','InteractionIDKey','CallId');
   } 



}