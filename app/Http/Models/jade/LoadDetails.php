<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class LoadDetails extends Model
{

   protected $connection = 'jade';
   protected $table = 'load_details';
   protected $fillable = ['load_id','uniqueid','transaction_id','is_reserved','phoneno','caller_id','updated_at','checked','duration'];

}