<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Eod extends Model
{

   protected $connection = 'rhyolite';
   protected $table = 'eod';
   protected $fillable = ['name','cic_id','dummyboard_id','send_time','converted_sendtime','timezone','from','ready_to_send','to','email_from','email_to','email_cc','cfrom','cto'];

}