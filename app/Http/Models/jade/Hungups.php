<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Hungups extends Model
{

   protected $connection = 'jade';
   protected $table = 'hangups';
   protected $fillable = ['did','message','subscriber_id','verified'];

  

}