<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CampaignData extends Model
{

	protected $connection = 'interface';
    protected $table = 'campaign_data';
    protected $fillable = ['status','campaign_id', 'lob','contents','active']; 



} 