<?php namespace App\Http\Models\lilac;

use Illuminate\Database\Eloquent\Model;
use Auth;

class User extends Model
{

   protected $connection = 'gardenia';
   protected $table = 'users';
   protected $fillable = ['name','username','options','email','user_type','created','updated','password'];
   protected $hidden = ['password', 'remember_token'];



}