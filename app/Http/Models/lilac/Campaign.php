<?php namespace App\Http\Models\lilac;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Campaign extends Model
{

   protected $connection = 'gardenia';
   protected $table = 'campaigns';
   protected $fillable = ['campaign_id','live_agents','disabled_fields','enabled_fields','user_type','created','updated'];




}