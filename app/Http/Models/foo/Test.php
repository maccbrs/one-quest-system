<?php namespace App\Http\Models\foo;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Test extends Model
{

   protected $connection = 'mytestdb';
   protected $table = 'test';
   protected $fillable = ['title','content','created_at','updated_at'];



} 