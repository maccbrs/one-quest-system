<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetDeptNotes extends Model
{

   protected $connection = 'garnet';
   protected $table = 'dept_notes';
   public $timestamps = false;
   protected $fillable = ['user','header','content','to','type','status','created_at','updated_at'];


} 