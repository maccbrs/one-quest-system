<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use DB;

class SunstoneApplicant extends Model
{

    protected $connection = 'sunstone';
    protected $table = 'applicants';   
    public $timestamps = false;
    protected $fillable = ['last_name','middle_name','first_name','contact_number','email','address','additional','status','user_id'];

  
}