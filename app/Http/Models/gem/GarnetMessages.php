<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetMessages extends Model
{

   	protected $connection = 'garnet';
   	protected $table = 'messages';
   	public $timestamps = false;
   	protected $fillable = ['user','subject','content','to','type','status','page','created_at','updated_at'];

   	public function get_message($userId)
   	{

   		$messages_to = $this
			 ->where('to',$userId);
			
		  $messages_user = $this
			 ->where('user',$userId);
	
		  $messages_list = $messages_to
			 ->union($messages_user)
			 ->orderBy('id','desc')
			 ->get();

		  foreach ($messages_list as $key => $value) {

			 $messages_list[$key]['content'] = json_decode($value['content']);
			
		  }

        return $messages_list;
   	} 

   	public function scopeSource($query,$id)
    {

       return $query->where('to',$id);

    } 


    public function get_conversation($recipient,$userId)
    {

      $messages_list = $this
       ->whereIn('to',[$userId,$recipient])
       ->whereIn('user',[$userId,$recipient])
       ->whereNotIn('page',['group'])
       ->orderBy('id','desc')
       ->get();
    
      foreach ($messages_list as $key => $value) {

       $messages_list[$key]['content'] = json_decode($value['content']);
      
      }

        return $messages_list;
    } 

    public function get_group_conversation($recipient,$userId)
    {

      $messages_list = $this
       ->whereIn('to',[$recipient])
       ->whereIn('page',['group'])
       ->orderBy('id','desc')
       ->get();
    
      foreach ($messages_list as $key => $value) {

       $messages_list[$key]['content'] = json_decode($value['content']);
      
      }

        return $messages_list;
    } 

} 