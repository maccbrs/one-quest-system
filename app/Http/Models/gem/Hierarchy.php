<?php namespace App\Http\Models\gem;
use Illuminate\Database\Eloquent\Model;
use Auth;


class Hierarchy extends Model
{

    protected $connection = 'gem';
    protected $table = 'hierarchy';   
    protected $fillable = ['level1', 'level2','created_at','updated_at'];

   public function scopeSubs($query)
   {
       return $query->where('level1',Auth::user()->id);
   } 

}