<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use DB;

class MagnetiteTrainingStage extends Model
{

    protected $connection = 'magnetite';
    protected $table = 'training_stages';   
    public $timestamps = false;
    protected $fillable = ['trainee_id','stage','wave_id','status'];

    public function wave(){
      return $this->hasOne('App\Http\Models\gem\MagnetiteWaves','id','wave_id')->select('id','name','camp_id','trainer_id');
    }

    public function trainee(){
      return $this->hasOne('App\Http\Models\gem\SunstoneApplicant','id','trainee_id')->select('id','last_name','middle_name','first_name','position_id','status');
    }  

}