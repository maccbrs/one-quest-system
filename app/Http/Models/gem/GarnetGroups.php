<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetGroups extends Model
{

   protected $connection = 'garnet';
   protected $table = 'user_groups';
   public $timestamps = false;
   protected $fillable = ['name','user','members','status'];


} 