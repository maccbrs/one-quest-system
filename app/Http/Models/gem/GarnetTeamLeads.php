<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetTeamLeads extends Model
{

   protected $connection = 'garnet';
   protected $table = 'team_leads';
   public $timestamps = false;
   protected $fillable = ['last_name','first_name','middle_name','status','user_id','ps_id','createdAt','updatedAt'];

   public function scopeSelf($query)
   {
       return $query->where('user_id',Auth::user()->id);
   } 

   public function agents(){
   		return $this->hasMany('App\Http\Models\gem\GarnetAgents','tl_id')->where('status',1);
   } 

   public function gem(){
      return $this->hasOne('App\Http\Models\gem\GemUser','id','user_id');
   }    

   public function supervisor(){
      return $this->hasOne('App\Http\Models\gem\GarnetProjectSupervisors','id','ps_id');
   } 

}  