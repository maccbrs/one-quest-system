<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use DB;

class MagnetiteWaves extends Model
{

    protected $connection = 'magnetite';
    protected $table = 'training_waves';   
    public $timestamps = false;
    protected $fillable = ['name','camp_id','trainer_id','status'];

    public function trainer(){
      return $this->hasMany('App\Http\Models\magnetite\GemUsers','id','trainer_id');
    } 

    public function camp(){
      return $this->hasOne('App\Http\Models\magnetite\Campaign','id','camp_id');
    }  

    public function stage(){
      return $this->hasMany('App\Http\Models\magnetite\TrainingStage','wave_id','id')->select('id','wave_id','stage','trainee_id');
    }  

    public function getagents($access){
   	
       	if($access['type'] == 'Administrator'){

			$waves = $this->get();

       	}else{

       		if($access['level'] == 2){

       			$waves = $this->get();
       			
       		}else{

       			$waves = $this->where('trainer_id','=',$access['id'])->get();

       		}
       	}	

       	return $waves;
   	}

}