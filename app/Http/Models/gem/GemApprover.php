<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemApprover extends Model
{

   protected $connection = 'gem';
   protected $table = 'approver';
   protected $fillable = ['level1','level2','level3','added_by','created_at','updated_at'];

   public function emp(){
   		return $this->hasOne('App\Http\Models\gem\GemUser','id','level1');
   }   
 
   public function sup(){
   		return $this->hasOne('App\Http\Models\gem\GemUser','id','level2');
   }   

   public function scopeSelf($query)
   {
       return $query->where('level2',Auth::user()->id);
   }   

   public function scopeSelf2($query)
   {
       return $query->where('level3',Auth::user()->id);
   }  
} 