<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemDids extends Model
{

   protected $connection = 'gem';
   protected $table = 'dids';
   protected $fillable = ['phone','added_by','created_at','status','campaign','updated_at'];


} 