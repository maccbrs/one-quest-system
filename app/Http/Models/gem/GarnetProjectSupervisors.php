<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GarnetProjectSupervisors extends Model
{

   protected $connection = 'garnet';
   protected $table = 'project_supervisors';
   public $timestamps = false;
   protected $fillable = ['last_name','first_name','middle_name','status','createdAt','updatedAt'];

   public function scopeSelf($query)
   {
 //  	print_r('<pre>');  	print_r($query);  	print_r('</pre>'); exit;
       return $query->where('user_id',Auth::user()->id);
   } 

   public function agents(){
   		return $this->hasMany('App\Http\Models\gem\GarnetAgents','ps_id')->where('status',1);
   } 

   public function gem(){
      return $this->hasOne('App\Http\Models\gem\GemUser','id','user_id');
   }    

}  