<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use DB;

class SunstonePosition extends Model
{

    protected $connection = 'sunstone';
    protected $table = 'positions';   
    public $timestamps = false;
    protected $fillable = ['name','status','type'];

  
}