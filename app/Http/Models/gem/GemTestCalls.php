<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemTestCalls extends Model
{

   protected $connection = 'gem';
   protected $table = 'test_calls';
   protected $fillable = ['created_at','updated_at','total','passed','failed'];


} 