<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use DB;

class GarnetCampaign extends Model
{
    protected $connection = 'garnet';
    protected $table = 'campaigns';   
    public $timestamps = false;
    protected $fillable = ['title','status','alias'];


}