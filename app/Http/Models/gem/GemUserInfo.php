<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemUserInfo extends Model
{
   	protected $connection = 'garnet';
   	protected $table = 'user_additional_info';
   	protected $fillable = ['user_id','information','motto'];

   	public function applicant(){

      return $this->hasOne('App\Http\Models\gem\SunstoneApplicant','user_id','user_id')->select();
    
    } 

} 