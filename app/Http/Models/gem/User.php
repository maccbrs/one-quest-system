<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'gem';
    protected $table = 'users';   
    protected $fillable = ['emp_code','name', 'email', 'password','user_type','access','avatar','is_representative','user_level','options','status','active','auditor'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    

    public function pass(){
      return $this->hasOne('App\Http\Models\gem\UserPassword','gem_id','id');
    }  

    public function fetch_mrlist(){
      return $this->hasOne('App\Http\Models\otsuka\Medrep_distro','emp_code','email');
    }

   public function fetch_allocation_april_invalid(){
        return $this->hasOne('App\Http\Models\otsuka\Patients_kit_allocation_april','emp_code','email');
   }

    public function fetch_mr_mdlist(){
      return $this->hasOne('App\Http\Models\otsuka\National_md_list','empcode','email');
    }

    public function fetch_referral_abilify(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        $abilify = array(1,2,3,4,5,6,7,8);


      return $this->hasMany('App\Http\Models\otsuka\Encoded_Purchases_Validated','added_by','id')
      ->orderBy('encoded_purchases_validated.created_at')
      ->join('patients_validated', 'patients_validated.id', '=', 'encoded_purchases_validated.patient_id')
      ->join('patients_med_info', 'patients_med_info.patient_id', '=', 'patients_validated.id')
      ->where(function($query){$query->where('patients_validated.patient_type','=','referral')->orWhere('patients_validated.patient_type','=','Voluntary');})
      //->whereBetween('patients_validated.created_at', [$first_day, $now])
      ->whereIn('patients_med_info.sku',$abilify)
      ->groupby('encoded_purchases_validated.patient_id');
    }

    public function fetch_referral_aminoleban(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

          $amino = array(9,10,11,42);


      return $this->hasMany('App\Http\Models\otsuka\Encoded_Purchases_Validated','added_by','id')
      ->orderBy('encoded_purchases_validated.created_at')
      ->join('patients_validated', 'patients_validated.id', '=', 'encoded_purchases_validated.patient_id')
      ->join('patients_med_info', 'patients_med_info.patient_id', '=', 'patients_validated.id')
      ->where(function($query){$query->where('patients_validated.patient_type','=','referral')->orWhere('patients_validated.patient_type','=','Voluntary');})
      //->whereBetween('patients_validated.created_at', [$first_day, $now])
      ->whereIn('patients_med_info.sku',$amino)
      ->groupby('encoded_purchases_validated.patient_id');
    }

    public function fetch_referral_pletaal(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

          $pletaal = array(20,21,22,23,24);


      return $this->hasMany('App\Http\Models\otsuka\Encoded_Purchases_Validated','added_by','id')
      ->orderBy('encoded_purchases_validated.created_at')
      ->join('patients_validated', 'patients_validated.id', '=', 'encoded_purchases_validated.patient_id')
      ->join('patients_med_info', 'patients_med_info.patient_id', '=', 'patients_validated.id')
      ->where(function($query){$query->where('patients_validated.patient_type','=','referral')->orWhere('patients_validated.patient_type','=','Voluntary');})
      //->whereBetween('patients_validated.created_at', [$first_day, $now])
      ->whereIn('patients_med_info.sku',$pletaal)
      ->groupby('encoded_purchases_validated.patient_id');
    }

   public function scopeEvaluator($query)
   {
       return $query->select('name','id')->where('auditor',1);
   }    

  public function scopeSearchByKeyword($query, $keyword)
  {
      if ($keyword!='') {
          $query->where(function ($query) use ($keyword) {
              $query->where("name", "LIKE","%$keyword%")
                  ->orWhere("email", "LIKE", "%$keyword%");
          });
      }
      return $query;
  }

    public function fetch_total_retrieval(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('type','=','Enroll')
        ->where(function($query){$query->where('verification_status',1)->orWhere('verification_status',5)->orWhere('verification_status',3)->orWhere('verification_status',4)->orWhere('verification_status',2)->orWhere('verification_status',7)
          ->orWhere(function($query){$query->whereBetween('verification_attempt',[4,5])->where('verification_status',6)
          ->orWhere(function($query){$query->where('trail_status',199)->whereNull('patient_consent')
          ->orWhere(function($query){$query->whereBetween('verification_attempt',[0,4])->where('trail_status','!=',199)->where(function($query){$query->where('verification_status',0)->orWhere('verification_status',6);});});});});})
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_new(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where('verification_status',1)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_finished(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where('verification_status',5)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_stopped(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where('verification_status',3)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_switched(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where('verification_status',4)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_total_verified(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where(function($query){$query->where('verification_status',1)->orWhere('verification_status',3)->orWhere('verification_status',5)->orWhere('verification_status',4);})
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_declined(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where('verification_status',2)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_wrong_number(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where('verification_status',7)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_noanswer_5(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where('verification_attempt',5)
        ->where('verification_status',6)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_noanswer_4(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where('verification_attempt',4)
        ->where('verification_status',6)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_noconsent(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where('trail_status',199)
        ->whereNull('patient_consent')
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_queue(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where('trail_status','!=',199)
        ->whereBetween('verification_attempt',[0,4])
        ->where(function($query){$query->where('verification_status',0)->orWhere('verification_status',6);})
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_abilify_total_unverified_retrieval(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Abilify')
        ->where('type','=','Enroll')
        ->where(function($query){$query->where('verification_status',2)->orWhere('verification_status',7)
          ->orWhere(function($query){$query->where('verification_attempt',5)->where('verification_status',6)
          ->orWhere(function($query){$query->where('verification_attempt',4)->where('verification_status',6)
          ->orWhere(function($query){$query->where('trail_status',199)->whereNull('patient_consent');});});});})
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_new(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where('verification_status',1)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_finished(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where('verification_status',5)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_stopped(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where('verification_status',3)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_switched(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where('verification_status',4)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_total_verified(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where(function($query){$query->where('verification_status',1)->orWhere('verification_status',3)->orWhere('verification_status',5)->orWhere('verification_status',4);})
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_declined(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where('verification_status',2)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_wrong_number(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where('verification_status',7)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_noanswer_5(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where('verification_attempt',5)
        ->where('verification_status',6)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_noanswer_4(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where('verification_attempt',4)
        ->where('verification_status',6)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_noconsent(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where('trail_status',199)
        ->whereNull('patient_consent')
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_queue(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where('trail_status','!=',199)
        ->whereBetween('verification_attempt',[0,4])
        ->where(function($query){$query->where('verification_status',0)->orWhere('verification_status',6);})
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_aminoleban_total_unverified_retrieval(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Aminoleban')
        ->where('type','=','Enroll')
        ->where(function($query){$query->where('verification_status',2)->orWhere('verification_status',7)
          ->orWhere(function($query){$query->where('verification_attempt',5)->where('verification_status',6)
          ->orWhere(function($query){$query->where('verification_attempt',4)->where('verification_status',6)
          ->orWhere(function($query){$query->where('trail_status',199)->whereNull('patient_consent');});});});})
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_new(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where('verification_status',1)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_finished(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where('verification_status',5)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_stopped(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where('verification_status',3)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_switched(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where('verification_status',4)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_total_verified(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where(function($query){$query->where('verification_status',1)->orWhere('verification_status',3)->orWhere('verification_status',5)->orWhere('verification_status',4);})
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_declined(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where('verification_status',2)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_wrong_number(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where('verification_status',7)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_noanswer_5(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where('verification_attempt',5)
        ->where('verification_status',6)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_noanswer_4(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where('verification_attempt',4)
        ->where('verification_status',6)
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_noconsent(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where('trail_status',199)
        ->whereNull('patient_consent')
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_pletaal_queue(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where('trail_status','!=',199)
        ->whereBetween('verification_attempt',[0,4])
        ->where(function($query){$query->where('verification_status',0)->orWhere('verification_status',6);})
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }    

    public function fetch_pletaal_total_unverified_retrieval(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('medicine','=','Pletaal')
        ->where('type','=','Enroll')
        ->where(function($query){$query->where('verification_status',2)->orWhere('verification_status',7)
          ->orWhere(function($query){$query->where('verification_attempt',5)->where('verification_status',6)
          ->orWhere(function($query){$query->where('verification_attempt',4)->where('verification_status',6)
          ->orWhere(function($query){$query->where('trail_status',199)->whereNull('patient_consent');});});});})
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_invalid_blurred(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('type','=','Enroll')
        ->where('trail_status',199)
        ->where('remarks','=','Blurred Photo')
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_invalid_duplicate(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('type','=','Enroll')
        ->where('trail_status',199)
        ->where('remarks','=','Duplicate Submission')
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_invalid_already_enrolled(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('type','=','Enroll')
        ->where('trail_status',199)
        ->where('remarks','=','Px Already Enrolled')
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_invalid_wrong_upload(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('type','=','Enroll')
        ->where('trail_status',199)
        ->where('remarks','=','Wrong Upload')
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }

    public function fetch_invalid_total(){

          $dt = date('Y-m-d');
          $first_day = date('Y-m-01', strtotime($dt)).' 00:00:00';
          $mtd_start = '2018-02-01 00:00:00';
          $now = date('Y-m-d').' 17:00:00';
          $yesterday = date('Y-m-d', strtotime($now.'-1 day')).' 17:00:01';
          $tomorrow = date('Y-m-d', strtotime($now.'+1 day'));

        return $this->hasMany('App\Http\Models\otsuka\Upload','added_by','id')
        ->where('active',1)
        ->where('type','=','Enroll')
        ->where('trail_status',199)
        ->where('remarks','!=','No Px Consent')
        ;
//->whereBetween('created_at',[$first_day, $now]);
    }    
}