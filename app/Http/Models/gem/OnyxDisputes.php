<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OnyxDisputes extends Model
{

   protected $connection = 'onyx';
   protected $table = 'disputes';
   public $timestamps = false;
   protected $fillable = ['user','user_type','sup_response','sup_comments','dispute_period','dispute_date','dispute_time','notes','issue','tl_response','tl_comments','created_at','updated_at','status'];


   public function scopeSelf($query)
   {
       return $query->where('user',Auth::user()->id);
   } 

   public function scopeRa($query)
   {
       return $query->where('user_type',1)->where('tl_response',2)->where('ra_response',1);
   } 
 
   public function scopeHr($query)
   {
       return $query->where('hr_response',1);
   } 

   public function scopeHr2($query)
   {
       return $query->where('hr_response',1)->where('ra_response',2);
   } 

   public function scopeAgents($query)
   {
       return $query->where('user_type',1);
   } 

   public function scopeNonagents($query)
   {
       return $query->where('user_type',2);
   } 

   public function scopeSubs($query,$arr)
   {
       return $query->whereIn('user',$arr);
   } 

   public function userObj(){
   		return $this->hasOne('App\Http\Models\gem\User','id','user');
   }  

   public function period(){
   		return $this->hasOne('App\Http\Models\gem\OnyxDisputesDates','id','dispute_period');
   }  

   public function issueObj(){
   		return $this->hasOne('App\Http\Models\gem\OnyxDisputesIssues','id','issue');
   }  
} 