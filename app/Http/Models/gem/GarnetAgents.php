<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class GarnetAgents extends Model
{
	
   protected $connection = 'garnet';
   protected $table = 'agents';
   public $timestamps = false;
   protected $fillable = ['last_name','first_name','middle_name','ps_id','username','tl_id','status','evaluator_id','user_id','createdAt','updatedAt'];

   public function scopeSelf($query)
   	{
       return $query->where('user_id',Auth::user()->id);
   	} 

    public function teamlead(){

      return $this->hasOne('App\Http\Models\gem\GarnetTeamLeads','id','tl_id');

    }     

    public function supervisor(){

      return $this->hasOne('App\Http\Models\gem\GarnetProjectSupervisors','id','ps_id');

    }  

    public function gem(){

     return $this->hasOne('App\Http\Models\gem\GemUser','id','user_id');

    }  

    public function get_agent($superiordata =null, $userType = null){

    	switch ($userType) {

            case 'Manager':

            	$tl_list = new \App\Http\Models\gem\GarnetTeamLeads;

      				$ps_agents =  $this
      					->select('id','first_name', 'last_name', 'user_id')
      					->where('ps_id',$superiordata->id)
      					->whereNotNull('user_id');

      				$tl_agents = $tl_list
      					->select('id','first_name', 'last_name', 'user_id')
      					->where('ps_id',$superiordata->id);

      				$all_agents = $tl_agents
      					->union($ps_agents)
      					->orderBy('id','desc')
      					->get();
            
                break;

            case 'Team Leader':

            	$ps_name = new \App\Http\Models\gem\GarnetProjectSupervisors;

      				$tl_agents =  $this
      					->select('id','first_name', 'last_name', 'user_id')
      					->where('tl_id',$superiordata->id)
      					->whereNotNull('user_id');

      				$ps_data = $ps_name
      					->select('id','first_name', 'last_name', 'user_id')
      					->where('id',$superiordata->ps_id);

      				$all_agents = $tl_agents
      					->union($ps_data)
      					->orderBy('id','desc')
      					->get();

                break;

            default:
              
              $all_agents = $this
                ->orderBy('first_name','asc')
                ->get();

        } 	

		return 	$all_agents;   

   	} 

} 