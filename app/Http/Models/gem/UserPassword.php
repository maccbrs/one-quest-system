<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;

class UserPassword extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'gem';
    protected $table = 'users_passwords';   
    protected $fillable = ['name', 'gem_id', 'password'];

  

}