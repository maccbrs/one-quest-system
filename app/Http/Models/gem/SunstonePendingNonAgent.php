<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;
use DB;

class SunstonePendingNonAgent extends Model
{

    protected $connection = 'sunstone';
    protected $table = 'pending_non_agents';   
    public $timestamps = false;
    protected $fillable = ['applicant_id','status'];

    public function trainee(){
      return $this->hasOne('App\Http\Models\gem\SunstoneApplicant','id','applicant_id')->select('id','last_name','middle_name','first_name','additional','position_id','status');
    } 
  
}