<?php namespace App\Http\Models\gem;

use Illuminate\Database\Eloquent\Model;


class Campaign extends Model
{

    protected $connection = 'garnet';
    protected $table = 'campaigns';   
    protected $fillable = ['name', 'campaign_id','user_id','created_at','updated_at'];


}