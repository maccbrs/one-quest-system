<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Bio extends Model
{

   protected $connection = 'gemstone';
   protected $table = 'bio';
   protected $fillable = ['name','detail','created_at','updated_at'];




} 