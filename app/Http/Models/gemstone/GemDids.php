<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemDids extends Model
{

   protected $connection = 'gem';
   protected $table = 'dids';
   protected $fillable = ['phone','added_by','created_at','campaign','updated_at','status'];

   	public function last_test_call(){
	 	return $this->hasOne('App\Http\Models\gemstone\GemTestCallItems','dids_id')->orderBy('created_at','desc');
	}  
} 