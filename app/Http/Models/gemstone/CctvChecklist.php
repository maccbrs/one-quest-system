<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CctvChecklist extends Model
{

   protected $connection = 'gemstone';
   protected $table = 'cctv_checklist';
   protected $fillable = ['is_done','user_id','created_at','updated_at'];

   public function cctv_statuses(){
   		return $this->hasMany('App\Http\Models\gemstone\CctvStatuses','cctv_checklist_id')->with(['cctv']);
   } 
   
   public function offline(){
   		return $this->hasMany('App\Http\Models\gemstone\CctvStatuses','cctv_checklist_id')->where('status',0);
   } 
   public function online(){
   		return $this->hasMany('App\Http\Models\gemstone\CctvStatuses','cctv_checklist_id')->where('status',1);
   }   

   public function user(){
      return $this->hasOne('App\Http\Models\gemstone\GemUser','id','user_id');
   } 

}