<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CctvStatuses extends Model
{

   protected $connection = 'gemstone';
   protected $table = 'cctv_statuses';
   protected $fillable = ['cctv_checklist_id','cctv_id','status','created_at','updated_at'];

   public function cctv(){
   		return $this->hasOne('App\Http\Models\gemstone\Cctv','id','cctv_id');
   } 

   public function checklist(){
   		return $this->hasOne('App\Http\Models\gemstone\CctvChecklist','id','cctv_checklist_id')->with(['user']);
   }

}