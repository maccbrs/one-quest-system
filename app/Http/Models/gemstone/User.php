<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'gem';
    protected $table = 'users';   
    protected $fillable = ['name', 'email', 'password','user_type','access','avatar','is_representative','user_level','options','status','active','auditor'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function pass(){
      return $this->hasOne('App\Http\Models\gem\UserPassword','gem_id','id');
    }  

   public function scopeEvaluator($query)
   {
       return $query->select('name','id')->where('auditor',1);
   }    

  public function scopeSearchByKeyword($query, $keyword)
  {
      if ($keyword!='') {
          $query->where(function ($query) use ($keyword) {
              $query->where("name", "LIKE","%$keyword%")
                  ->orWhere("email", "LIKE", "%$keyword%");
          });
      }
      return $query;
  }

}