<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;

class AddedUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'gem';
    protected $table = 'added_users';   
    protected $fillable = ['user_id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    
}