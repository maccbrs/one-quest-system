<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Options extends Model
{

   protected $connection = 'gemstone';
   protected $table = 'options';
   protected $fillable = ['name','lists','type','model','created_at','updated_at']; 




}