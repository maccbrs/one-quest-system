<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Cpu extends Model
{

   protected $connection = 'gemstone';
   protected $table = 'cpu';
   protected $fillable = ['sn','status','name','detail','processor','memory','hdd','history','created_at','updated_at'];




}