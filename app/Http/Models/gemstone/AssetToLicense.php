<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class AssetToLicense extends Model
{

   protected $connection = 'gemstone';
   protected $table = 'asset_to_license';
   protected $fillable = ['assets_id','license','licenses_id','user_id','created_at','updated_at'];




}