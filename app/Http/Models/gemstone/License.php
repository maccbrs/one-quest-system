<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class License extends Model
{

   protected $connection = 'gemstone';
   protected $table = 'licenses';
   protected $fillable = ['company','license',
   'product_key','seats','remaining_seats','purchase_date',
   'purchase_cost','manufacturer','created_at','updated_at'];




}