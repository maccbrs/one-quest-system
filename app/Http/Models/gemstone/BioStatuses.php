<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class BioStatuses extends Model
{

   protected $connection = 'gemstone';
   protected $table = 'bio_statuses';
   protected $fillable = ['bio_checklist_id','bio_id','status','created_at','updated_at'];

   public function bio(){
   		return $this->hasOne('App\Http\Models\gemstone\Bio','id','bio_id');
   } 

   public function checklist(){
   		return $this->hasOne('App\Http\Models\gemstone\BioChecklist','id','bio_checklist_id')->with(['user']);
   }

} 