<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemUser extends Model
{
    protected $connection = 'gem';
    protected $table = 'users';   
    protected $fillable = ['name', 'email', 'password','user_type','access'];
    protected $hidden = [
        'password', 'remember_token',
    ];

} 