<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Assets extends Model
{

   protected $connection = 'gemstone';
   protected $table = 'assets';
   protected $fillable = ['asset_tag','model','status','serial','asset_name','details','asset_type','workstation',
   'supplier','order_no','purchase_cost','warranty','notes','default_location','images',
   'purchase_date','created_at','updated_at'];

   public function ObjLicense(){
   		return $this->hasOne('App\Http\Models\gemstone\AssetToLicense','assets_id');
   } 


}