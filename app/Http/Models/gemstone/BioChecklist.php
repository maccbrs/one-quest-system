<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class BioChecklist extends Model
{

   protected $connection = 'gemstone';
   protected $table = 'bio_checklist';
   protected $fillable = ['is_done','user_id','created_at','updated_at'];

   public function bio_statuses(){
   		return $this->hasMany('App\Http\Models\gemstone\BioStatuses','bio_checklist_id')->with(['bio']);
   } 
   
   public function offline(){
   		return $this->hasMany('App\Http\Models\gemstone\BioStatuses','bio_checklist_id')->where('status',0);
   } 
   public function online(){
   		return $this->hasMany('App\Http\Models\gemstone\BioStatuses','bio_checklist_id')->where('status',1);
   }   

   public function user(){
      return $this->hasOne('App\Http\Models\gemstone\GemUser','id','user_id');
   } 

} 