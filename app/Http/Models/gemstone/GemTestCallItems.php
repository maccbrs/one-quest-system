<?php namespace App\Http\Models\gemstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GemTestCallItems extends Model
{

   protected $connection = 'gem';
   protected $table = 'test_call_items';
   protected $fillable = ['test_calls_id','dids_id','status','date','checked','notes','created_at','updated_at']; 


   public function obj_testcall(){
   		return $this->hasOne('App\Http\Models\gemstone\GemTestCalls','id','test_calls_id');
   }  

   public function obj_did(){
   		return $this->hasOne('App\Http\Models\gemstone\GemDids','id','dids_id');
   }  

} 