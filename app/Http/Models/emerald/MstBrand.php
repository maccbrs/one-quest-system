<?php namespace App\Http\Models\bloodstone;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MstBrand extends Model
{

    protected $connection = 'bloodstone';
    protected $table = 'mst_brand';   
    protected $fillable = ['id', 'name','alias','remarks'];
   
   	public function brand(){
   		return $this->hasMany('App\Http\Models\bloodstone\BloodstoneAssets','brand_id','id');
   	} 

    
	

}