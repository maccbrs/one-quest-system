<?php

namespace App\Http\Middleware;

use Closure;

class Agate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if($request->user()->auditor || $request->user()->user_type == 'administrator'): 
            
            $Templates= new \App\Http\Models\agate\Templates;
            $templates = $Templates->get();
            view()->share('templates',$templates);        
            return $next($request);

            return $next($request);
             
        endif;  

        return redirect()->route('gem.dashboard.index');       
    }
}
