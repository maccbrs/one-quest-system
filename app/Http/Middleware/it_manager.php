<?php

namespace App\Http\Middleware;

use Closure;

class it_manager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(  ($request->user()->user_level) != 2 || $request->user()->user_type != 'administrator'){

            return redirect()->route('gem.dashboard.index');

        }

        return $next($request);
    }
}
