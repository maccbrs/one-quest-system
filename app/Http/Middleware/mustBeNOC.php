<?php

namespace App\Http\Middleware;

use Closure;

class mustBeNOC
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = $request->user();

        if($user && $user->user_type == 'administrator'){

            return $next($request);

        }

        return response()->view('denied');

    }
}
