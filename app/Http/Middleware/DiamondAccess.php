<?php

namespace App\Http\Middleware;

use Closure;

class DiamondAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function __construct(){

        $this->middleware('diamond');

    }

    public function handle($request, Closure $next)
    {
        $user = $request->user();  

        $useraccess = json_decode($user['access']);

        if($user && in_array("diamond", $useraccess)){

            return $next($request);

        }

       abort(404, 'No Way!');
    }
}
