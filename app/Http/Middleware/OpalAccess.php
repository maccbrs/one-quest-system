<?php

namespace App\Http\Middleware;

use Closure;

class OpalAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = $request->user();  

        $useraccess = json_decode($user['access']);

        if($user && in_array("opal", $useraccess)){

            return $next($request);

        }

       abort(404, 'No Way!');

    }
}
