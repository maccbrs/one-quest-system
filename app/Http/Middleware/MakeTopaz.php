<?php

namespace App\Http\Middleware;

use Closure;

class MakeTopaz
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { //echo 3;die;
        return $next($request);
    }
}
