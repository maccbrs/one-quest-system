<?php

namespace App\Http\Middleware;


use Closure;
use Auth;

class MakeAmber
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $help = new \App\Http\Controllers\amber\Helper;
        $GarnetAgents = new \App\Http\Models\amber\GarnetAgents;
        $item = $GarnetAgents->where('user_id',Auth::user()->id)->first();
        $auth = Auth::user();
        $user = [
            'UserFname' => $auth->name,
            'UserLname' => $auth->name,
            'Default' => []
        ];
        
        if($item):
            $user = [
                'UserFname' => $item->first_name,
                'UserLname' => $item->last_name,
                'Default' => $item
            ];
        endif; 

        $user['UserImageSm'] = asset('uploads/sm-'.$auth->avatar);
        $user['UserImageM'] = asset('uploads/m-'.$auth->avatar);
        $user['UserImageL'] = asset('uploads/l-'.$auth->avatar);

        //$help->pre($user);
        $request->merge($user);
        return $next($request);


    }
}
