<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
         


            if (Auth::guard($guard)->guest()) {
                if ($request->ajax() || $request->wantsJson()) {
                    return response('Unauthorized.', 401);
                } else {
                    return redirect()->guest('login');
                }
            }

            if($request->user()->active == 0) {
                
                return redirect()->route('sorry');
              
            }

        

        if($request->user()->is_representative){
            $routeString = $request->route()->getName();
            //pre($request->user());
            $routename = explode('.',$routeString);
            if($routename[0] != 'otsuka'):
                $access = json_decode($request->user()->routes);
                //pre($access[0]);
                if($access[0] == 'otsuka.medrep'){
                    return redirect()->route('otsuka.index');
                }elseif($access[0] == 'otsuka.admin'){
                    return redirect()->route('otsuka.settings');
                }else{
                    return redirect()->route('otsuka.dashboard.queue');
                }
            endif;
        }else{
          
            $help = new \App\Http\Controllers\gem\Helper;
            $CampaignToAdmin = new \App\Http\Models\topaz\CampaignToAdmin;
            $ticket = false;
            $campaigns = [];
            $a = $CampaignToAdmin->where('user_id',Auth::user()->id)->with(['campaign'])->get();

            if($a->count()):
                foreach($a as $i):
                    $campaigns[$i->campaign_id] = $i->campaign->name;
                endforeach;
                $ticket = true; 
            endif;

            $data = [
                'UserImageSm' =>  asset('uploads/sm-'.Auth::user()->avatar),
                'UserImageM' =>  asset('uploads/m-'.Auth::user()->avatar),
                'UserImageL' =>  asset('uploads/l-'.Auth::user()->avatar),
                'ticket' => $ticket,
                'campaigns' => $campaigns
            ];
            $request->merge($data);          
        } 


        return $next($request);
    }
}
