<?php

namespace App\Http\Middleware;

use Closure;

class bloodstone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = $request->user(); 
        if($user['user_type'] == 'administrator') return $next($request); 
         
        $useraccess = json_decode($user['access']);
        if($user && in_array("rhyolite", $useraccess)){
            return $next($request);
        }

        return response()->view('denied');

    }
}
