<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => ['web','auth'],'namespace' => 'ruby','prefix' => 'ruby'], function () { 
	include(app_path().'/Http/Routers/ruby.php');
}); 

/*
Route::group(['middleware' => ['web','auth'],'namespace' => 'bloodstone','prefix' => 'bloodstone'], function () { 
	include(app_path().'/Http/Routers/bloodstone.php');
}); 
*/
/*
include(app_path().'/Http/Routers/bloodstone.php');
*/


include(app_path().'/Http/Routers/agate.php');


Route::group(['middleware' => ['web','auth'],'namespace' => 'otsuka','prefix' => 'otsuka'], function () { 
	include(app_path().'/Http/Routers/otsuka.php');
}); 

Route::group(['middleware' => ['web','auth'],'namespace' => 'bloodstone','prefix' => 'bloodstone'], function () { 
	include(app_path().'/Http/Routers/bloodstone.php');
}); 

	
Route::group(['middleware' => ['web','auth'],'namespace' => 'emerald','prefix' => 'emerald'], function () { 
	include(app_path().'/Http/Routers/emerald.php');
}); 


Route::group(['middleware' => ['web','auth'],'namespace' => 'rubellite','prefix' => 'rubellite'], function () { 
	include(app_path().'/Http/Routers/rubellite.php');
}); 


Route::group(['middleware' => ['web','auth'],'namespace' => 'diamond','prefix' => 'diamond'], function () {
	include(app_path().'/Http/Routers/diamond.php');
}); 

Route::group(['middleware' => ['web','auth'],'namespace' => 'pearl','prefix' => 'pearl'], function () {
	include(app_path().'/Http/Routers/pearl.php');
}); 

Route::group(['middleware' => ['web','auth'],'namespace' => 'poppy','prefix' => 'poppy'], function () {
	include(app_path().'/Http/Routers/poppy.php');
}); 

Route::group(['middleware' => ['web','auth'],'namespace' => 'sunstone','prefix' => 'sunstone'], function () {
	include(app_path().'/Http/Routers/sunstone.php');
}); 

Route::group(['middleware' => ['web','auth'],'namespace' => 'magnetite','prefix' => 'magnetite'], function () {
	include(app_path().'/Http/Routers/magnetite.php');
}); 

Route::group(['middleware' => ['web','auth'],'namespace' => 'lilac','prefix' => 'lilac'], function () {
	include(app_path().'/Http/Routers/lilac.php');
}); 

Route::group(['middleware' => ['web','auth','garnet'],'namespace' => 'garnet','prefix' => 'garnet'], function () {
	include(app_path().'/Http/Routers/garnet.php');
}); 

Route::group(['middleware' => ['web','auth'],'namespace' => 'opal','prefix' => 'opal'], function () {
	include(app_path().'/Http/Routers/opal.php');
}); 

Route::group(['middleware' => ['web','auth','amber'],'namespace' => 'amber','prefix' => 'amber'], function () {
	include(app_path().'/Http/Routers/amber.php');
}); 

Route::group(['middleware' => ['web','auth'],'namespace' => 'amethyst','prefix' => 'amethyst'], function () {
	include(app_path().'/Http/Routers/amethyst.php');
}); 

Route::group(['middleware' => ['web','auth','topaz'],'namespace' => 'topaz','prefix' => 'topaz'], function () {
	include(app_path().'/Http/Routers/topaz.php');
}); 

Route::group(['namespace' => 'moonstone','prefix' => 'moonstone'], function () {
	include(app_path().'/Http/Routers/moonstone.php');
}); 


Route::group(['middleware' => ['auth','rhyolite'],'namespace' => 'rhyolite','prefix' => 'rhyolite'], function () {
	include(app_path().'/Http/Routers/rhyolite.php');
}); 
Route::group(['middleware' => ['auth','jade'],'namespace' => 'jade','prefix' => 'jade'], function () {
	include(app_path().'/Http/Routers/jade.php');
}); 



Route::group(['middleware' => ['web','auth'],'namespace' => 'gem','prefix' => 'gem'],function(){
	include(app_path().'/Http/Routers/gem.php');
});


Route::get('/', function () {
    return redirect()->route('gem.dashboard.index');
});


Route::auth();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => ['web','auth','noc'],'namespace' => 'gemstone','prefix' => 'gemstone'],function(){
	include(app_path().'/Http/Routers/gemstone.php');
});


Route::group(['prefix' => 'foo'],function(){
	$this->get('/',['as' => 'foo','uses' => 'fooController@index']);

});

