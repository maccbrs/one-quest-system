<?php


$this->group(['prefix' => 'team'],function(){

	$this->get('/agents',['as' => 'garnet.team.agents','uses' => 'teamController@agents']);
	$this->get('/group',['as' => 'garnet.team.group','uses' => 'teamController@group']);
	$this->get('/notes',['as' => 'garnet.team.notes','uses' => 'teamController@notes']);

	$this->get('/mail/{Administrator}',['as' => 'garnet.team.noc','uses' => 'teamController@mail']);
	$this->get('/mail/{Human Resource}',['as' => 'garnet.team.hr','uses' => 'teamController@mail']);
	$this->get('/mail/{Trainer}',['as' => 'garnet.team.training','uses' => 'teamController@mail']);
	$this->get('/mail/{Quality Assurance}',['as' => 'garnet.team.qa','uses' => 'teamController@mail']);
	$this->get('/mail/{Operations}',['as' => 'garnet.team.operations','uses' => 'teamController@mail']);

// messaging

	$this->post('/new_message',['as' => 'garnet.team.new_message','uses' => 'teamController@new_message']);

// group
	$this->post('/new_group',['as' => 'garnet.team.new_group','uses' => 'teamController@new_group']);
	$this->post('/edit-group',['as' => 'garnet.team.edit_group','uses' => 'teamController@edit_group']);

});


$this->group(['prefix' => 'note'],function(){

	$this->get('/notes/{Administrator}',['as' => 'garnet.notes.noc','uses' => 'noteController@index']);
	$this->get('/notes/{Human Resource}',['as' => 'garnet.notes.hr','uses' => 'noteController@index']);
	$this->get('/notes/{Manager}',['as' => 'garnet.notes.manager','uses' => 'noteController@index']);
	$this->get('/notes/{Trainer}',['as' => 'garnet.notes.trainer','uses' => 'noteController@index']);
	$this->get('/notes/{Team Leader}',['as' => 'garnet.notes.teamleader','uses' => 'noteController@index']);
	$this->get('/notes/{Quality Assurance}',['as' => 'garnet.notes.qa','uses' => 'noteController@index']);
	
	$this->post('/add_notes/{id}',['as' => 'garnet.note.add_notes','uses' => 'noteController@add_notes']);	
	$this->post('/update_notes/{id}',['as' => 'garnet.note.update_notes','uses' => 'noteController@update_notes']);	

}); 

$this->group(['prefix' => 'profile'],function(){
	
	$this->get('/profile',['as' => 'garnet.profile.index','uses' => 'profileController@index']);

}); 

$this->group(['prefix' => 'profile'],function(){
	
	$this->post('/profile-setting',['as' => 'garnet.settings.updateimage','uses' => 'profileController@update_image']);
	$this->post('/add_info',['as' => 'garnet.settings.add_info','uses' => 'profileController@add_info']); 
	$this->post('/motto',['as' => 'garnet.settings.motto','uses' => 'profileController@motto']); 

}); 


