<?php

$this->group(['prefix' => 'trainee'],function(){

	$this->get('/index',['as' => 'magnetite.trainee.index','uses' => 'traineeController@index']);

	$this->post('/add',['as' => 'magnetite.trainee.add','uses' => 'traineeController@add']);
	$this->post('/assign',['as' => 'magnetite.trainee.assign_wave','uses' => 'traineeController@assign_wave']);
	$this->post('/update-wave',['as' => 'magnetite.trainee.update_wave','uses' => 'traineeController@update_wave']);

}); 


$this->group(['prefix' => 'wave'],function(){

	$this->get('/index',['as' => 'magnetite.wave.index','uses' => 'waveController@index']);

	$this->post('/add',['as' => 'magnetite.wave.add','uses' => 'waveController@add']);

}); 


$this->group(['prefix' => 'monitoring'],function(){

	$this->get('/index',['as' => 'magnetite.monitoring.index','uses' => 'monitoringController@index']);

	$this->post('/update',['as' => 'magnetite.monitoring.update_stage','uses' => 'monitoringController@update']);

}); 