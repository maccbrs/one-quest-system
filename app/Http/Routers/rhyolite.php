<?php

$this->get('/',['as' => 'rhyolite.index','uses' => 'rhyoliteController@index']);

$this->group(['prefix' => 'eod'],function(){

	$this->get('/index',['as' => 'rhyolite.eod.index','uses' => 'eodController@index']);
	$this->post('/create',['as' => 'rhyolite.eod.create','uses' => 'eodController@create']);
	$this->post('/update/{id}',['as' => 'rhyolite.eod.update','uses' => 'eodController@update']);
	$this->post('/update-cic/{id}',['as' => 'rhyolite.eod.update-cic','uses' => 'eodController@updateCic']);
	$this->post('/update-dboards/{id}',['as' => 'rhyolite.eod.update-dboards','uses' => 'eodController@updateDboards']);
	$this->post('/update-email/{id}',['as' => 'rhyolite.eod.update-email','uses' => 'eodController@updateEmail']);
}); 

$this->group(['prefix' => 'productivity'],function(){

	$this->get('/index',['as' => 'rhyolite.productivity.index','uses' => 'productivityController@index']);
	$this->get('/add-campaign',['as' => 'rhyolite.productivity.add-campaign','uses' => 'productivityController@addCampaign']);
	$this->post('/create-campaign',['as' => 'rhyolite.productivity.create-campaign','uses' => 'productivityController@createCampaign']);
	$this->post('/update-campaign/{id}',['as' => 'rhyolite.productivity.update-campaign','uses' => 'productivityController@updateCampaign']);
	$this->post('/delete-campaign',['as' => 'rhyolite.productivity.delete-campaign','uses' => 'productivityController@deleteCampaign']);
	$this->get('/generate',['as' => 'rhyolite.productivity.generate','uses' => 'productivityController@generate']);
	$this->post('/filter',['as' => 'rhyolite.productivity.filter','uses' => 'productivityController@filter']);
	$this->get('/excel/{id}',['as' => 'rhyolite.productivity.excel','uses' => 'productivityController@excel']);
	$this->get('/campaigns',['as' => 'rhyolite.productivity.campaigns','uses' => 'productivityController@campaigns']);
	
});

$this->group(['prefix' => 'users'],function(){
	$this->get('/index',['as' => 'rhyolite.users.index','uses' => 'usersController@index']);
	$this->post('/create',['as' => 'rhyolite.users.create','uses' => 'usersController@create']);
});

$this->group(['prefix' => 'calls'],function(){
	$this->get('/index',['as' => 'rhyolite.calls.index','uses' => 'callsController@index']);
	$this->get('/excel/{id}',['as' => 'rhyolite.calls.excel','uses' => 'callsController@excel']);
	$this->post('/filter',['as' => 'rhyolite.calls.filter','uses' => 'callsController@filter']);
});