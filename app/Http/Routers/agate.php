<?php 


Route::group(['middleware' => ['web','auth','agate'],'namespace' => 'agate','prefix' => 'agate'], function () { 

		$this->group(['prefix' => 'dashboard'],function(){
			$this->get('/index',['as' => 'agate.dashboard.index','uses' => 'dashboardController@index']);
		}); 

		$this->group(['prefix' => 'agents'],function(){

			$this->get('/',['as' => 'agate.agents.index','uses' => 'agentsController@index']);
			// $this->get('/index/{bound}',['as' => 'agate.agents.index','uses' => 'agentsController@index']);
			// $this->get('/results/{bound}',['as' => 'agate.agents.results','uses' => 'agentsController@results']);
			// $this->get('/results/{bound}/{agent}',['as' => 'agate.agents.agent','uses' => 'agentsController@agent']);
			// $this->get('/results/{bound}/{agent}/{weekno}',['as' => 'agate.agents.week','uses' => 'agentsController@week']);
			// $this->get('/view/{bound}/{agent}',['as' => 'agate.agents.view','uses' => 'agentsController@view']);
			// $this->get('/campaigns/{bound}/{campaign}/{user}',['as' => 'agate.agents.campaigns','uses' => 'agentsController@campaigns']);
			// $this->get('/campaign/{bound}/{user}/{id}',['as' => 'agate.agents.campaign','uses' => 'agentsController@campaign']);
			// $this->get('/campaign/{bound}/{user}/{id}/search',['as' => 'agate.agents.campaign-search','uses' => 'agentsController@search']);				
		});  

		$this->group(['prefix' => 'campaigns'],function(){
			$this->get('/',['as' => 'agate.campaigns.index','uses' => 'campaignsController@index']);
			$this->get('/audits/{id}',['as' => 'agate.campaigns.audits','uses' => 'campaignsController@audits']);
			// $this->get('/index/{bound}',['as' => 'agate.campaigns.index','uses' => 'campaignsController@index']);
			// $this->get('/users/{bound}/{campaign}',['as' => 'agate.campaigns.users','uses' => 'campaignsController@users']);
			// $this->get('/user/{bound}/{campaign}/{id}',['as' => 'agate.campaigns.user','uses' => 'campaignsController@user']);		
			// $this->get('/view/{bound}/{campaign}',['as' => 'agate.campaigns.view','uses' => 'campaignsController@view']);
			// $this->get('/user/{bound}/{campaign}/{id}/search',['as' => 'agate.campaigns.user-search','uses' => 'campaignsController@search']);
		}); 


		$this->group(['prefix' => 'search'],function(){
			$this->get('/index',['as' => 'agate.search.index','uses' => 'searchController@index']);
			$this->post('/result',['as' => 'agate.search.result','uses' => 'searchController@result']);
		}); 

		$this->group(['prefix' => 'randomizer'],function(){
			$this->get('/index',['as' => 'agate.randomizer.index','uses' => 'randomizerController@index']);
		}); 

		$this->group(['prefix' => 'set'],function(){
			$this->get('/{id}',['as' => 'agate.set.index','uses' => 'setController@index']);
		}); 

		$this->group(['prefix' => 'audits'],function(){ 
			$this->get('/{id}',['as' => 'agate.audits.index','uses' => 'auditsController@index']);
		});

		$this->group(['prefix' => 'results'],function(){
			$this->get('/',['as' => 'agate.results.index','uses' => 'resultsController@index']);
			$this->get('/{id}',['as' => 'agate.results.read','uses' => 'resultsController@read']);
			$this->get('/{id}/{weekno}',['as' => 'agate.results.lists','uses' => 'resultsController@lists']);	
		});

		$this->group(['prefix' => 'metrics'],function(){

			$this->get('/parameters',['as' => 'agate.metrics.parameters','uses' => 'metricsController@parameters']);
			$this->post('/parameter',['as' => 'agate.metrics.parameter','uses' => 'metricsController@postParameter']);
			$this->post('/parameter/{parameterid}',['as' => 'agate.metrics.parameter-item','uses' => 'metricsController@postParameterItem']);
			$this->post('/parameter/{parameterid}/delete',['as' => 'agate.metrics.delete-parameter','uses' => 'metricsController@deleteParameter']);
			$this->post('/parameter/{parameterid}/{item}',['as' => 'agate.metrics.parameter-item-edit','uses' => 'metricsController@editParameterItem']);
			$this->post('/parameter-delete/{parameterid}/{item}',['as' => 'agate.metrics.parameter-item-delete','uses' => 'metricsController@deleteParameterItem']);
				
			
			$this->get('/key-factors',['as' => 'agate.metrics.keyfactors','uses' => 'metricsController@keyFactors']);
			$this->post('/key-factors',['as' => 'agate.metrics.keyfactor','uses' => 'metricsController@postKeyFactor']);
			$this->get('/key-factor/edit/{id}',['as' => 'agate.metrics.edit-keyfactor','uses' => 'metricsController@editKeyFactor']);
			$this->post('/update-key-factors/{id}',['as' => 'agate.metrics.update-keyfactor','uses' => 'metricsController@updateKeyFactor']);
			$this->post('/key-factors/{id}/parameter',['as' => 'metrics.keyfactor-add-parameter','uses' => 'metricsController@keyfactorAddParameter']);
			$this->post('/key-factor/delete/{id}',['as' => 'agate.metrics.delete-keyfactor','uses' => 'metricsController@deleteKeyFactor']);
			$this->post('/key-factor/{keyfactorid}/parameter/{parameterid}/delete',['as' => 'agate.metrics.delete-keyfactor-parameter','uses' => 'metricsController@deleteKeyFactorParameter']);
			

			$this->get('/templates',['as' => 'agate.metrics.templates','uses' => 'metricsController@templates']);
			$this->post('/template',['as' => 'agate.metrics.template','uses' => 'metricsController@createTemplate']);
			$this->get('/template/edit/{id}',['as' => 'agate.metrics.edit-template','uses' => 'metricsController@editTemplate']);
			$this->post('/template/{id}/add-key-factor',['as' => 'agate.metrics.template-add-key-factor','uses' => 'metricsController@templateAddKeyFactor']);
			$this->post('/template/{templateid}/keyfactor/{keyfactorid}',['as' => 'agate.metrics.template-edit-keyfactor','uses' => 'metricsController@templateEditKeyFactor']);
			$this->post('/template/{templateid}/keyfactor/{keyfactorid}/parameter/{parameterid}',['as' => 'agate.metrics.template-edit-keyfactor-parameter','uses' => 'metricsController@templateEditKeyFactorParameter']);
			$this->post('/template/{id}/delete',['as' => 'agate.metrics.delete-template','uses' => 'metricsController@deleteTemplate']);
			$this->post('/template/{templateid}/keyfactor/{keyfactorid}/delete',['as' => 'agate.metrics.delete-template-keyfactor','uses' => 'metricsController@templateDeleteKeyFactor']);
			
		}); 

		$this->group(['prefix' => 'audits'],function(){
			
			$this->get('/audit/{bound}/{id}',['as' => 'agate.audits.audit','uses' => 'auditsController@audit']);
			$this->post('/lock/{bound}/{id}',['as' => 'agate.audits.lock','uses' => 'auditsController@lock']);
			$this->post('/score/{bound}/{id}',['as' => 'agate.audits.score','uses' => 'auditsController@score']);
			$this->post('/score/{bound}/{id}',['as' => 'agate.audits.score','uses' => 'auditsController@score']);
			$this->post('/audit/{bound}/{id}/account-update',['as' => 'agate.audits.audit-account-update','uses' => 'auditsController@auditAccountUpdate']);

		});

		$this->group(['prefix' => 'settings'],function(){

			$this->get('/campaigns',['as' => 'agate.settings.campaigns','uses' => 'settingsController@campaigns']);
			$this->post('/campaign',['as' => 'agate.settings.addcampaign','uses' => 'settingsController@addCampaign']);
			$this->get('/campaign/{id}/edit',['as' => 'agate.settings.edit-campaign','uses' => 'settingsController@editCampaign']);
			$this->post('/campaign/delete',['as' => 'agate.settings.deletecampaign','uses' => 'settingsController@deleteCampaign']);
			$this->post('/campaign/{id}/update',['as' => 'agate.settings.update-campaign','uses' => 'settingsController@updateCampaign']);
			$this->post('/campaign/{id}/update-logo',['as' => 'agate.settings.campaign-update-logo','uses' => 'settingsController@campaignUpdateLogo']);

			$this->get('/deactivate/{bound}/{type}',['as' => 'agate.settings.deactivate','uses' => 'settingsController@deactivate']);
			$this->post('/deactivate/{bound}/{type}',['as' => 'agate.settings.deactivate','uses' => 'settingsController@postDeactivate']);
			
		});	

		$this->group(['prefix' => 'archives'],function(){
			$this->get('/index',['as' => 'agate.archives.index','uses' => 'archivesController@index']);
			$this->post('/create',['as' => 'agate.archives.create','uses' => 'archivesController@create']);
			$this->get('/contents/{id}',['as' => 'agate.archives.contents','uses' => 'archivesController@contents']);
			$this->post('/isfinal',['as' => 'agate.archives.isfinal','uses' => 'archivesController@isfinal']);
			$this->get('/audit/{bound}/{id}',['as' => 'agate.archives.audit','uses' => 'archivesController@audit']);
		});


		$this->group(['prefix' => 'custom'],function(){
			$this->get('/',['as' => 'agate.custom.index','uses' => 'customController@index']);
			$this->get('/add',['as' => 'agate.custom.add','uses' => 'customController@add']);
			$this->post('/create',['as' => 'agate.custom.create','uses' => 'customController@create']);
			$this->get('/audit/{id}',['as' => 'agate.custom.audit','uses' => 'customController@audit']);
			$this->post('/score/{id}',['as' => 'agate.custom.score','uses' => 'customController@score']);
			
		}); 

});  