<?php 

//$this->get('/',['as' => 'opal.dashboard.index','uses' => 'opalController@index']);

 
$this->group(['prefix' => 'connector'],function(){

	$this->get('/refs',['as' => 'opal.connector.refs','uses' => 'connectorController@refs']);
	$this->post('/connect/{id}',['as' => 'opal.connector.connect','uses' => 'connectorController@connect']);

});


$this->group(['prefix' => 'settings'],function(){

// dispute period
	$this->get('/dispute-date',['as' => 'opal.settings.dispute-date','uses' => 'disputeSettingController@index']);
	$this->post('/dispute-date-edit/{id}',['as' => 'opal.settings.dispute-date-edit','uses' => 'disputeSettingController@update']);
	$this->post('/dispute-date-create',['as' => 'opal.settings.dispute-date-create','uses' => 'disputeSettingController@create']);
	$this->post('/dispute-date-export',['as' => 'opal.settings.dispute-date-export','uses' => 'disputeSettingController@export']);
	$this->post('/dispute-date-export-rejected',['as' => 'opal.settings.dispute-date-export-rejected','uses' => 'disputeSettingController@excel_rejected']);
	$this->post('/dispute-date-export-approved',['as' => 'opal.settings.dispute-date-export-approved','uses' => 'disputeSettingController@excel_approved']);
// end of dispute period

// dispute Issue
	$this->get('/dispute-issue',['as' => 'opal.settings.dispute-issue','uses' => 'disputeSettingController@index_issue']);
	$this->post('/dispute-issue-create',['as' => 'opal.settings.dispute-issue-create','uses' => 'disputeSettingController@create_issue']);
	$this->post('/dispute-issue-edit/{id}',['as' => 'opal.settings.dispute-issue-edit','uses' => 'disputeSettingController@edit_issue']);
// end of dispute issue

});

$this->group(['prefix' => 'disputes'],function(){
	$this->get('/',['as' => 'opal.disputes.index','uses' => 'disputesController@index']);
	$this->get('/agentapprover',['as' => 'opal.disputes.agentapprover','uses' => 'approverlistController@getindex']);
	$this->get('/approverlistagents',['as' => 'opal.disputes.approverlistagents','uses' => 'approverlistController@index']);
	$this->get('/approverlistnonagents',['as' => 'opal.disputes.approverlistnonagents','uses' => 'approverlistController@approverlistnonagents']);
	$this->get('/approved',['as' => 'opal.disputes.approved','uses' => 'disputesController@approved']);
	$this->get('/expected',['as' => 'opal.disputes.expected','uses' => 'disputesController@expected']);
	$this->get('/rejected',['as' => 'opal.disputes.rejected','uses' => 'disputesController@rejected']);
	$this->get('/period/{id}',['as' => 'opal.disputes.period','uses' => 'disputesController@period']);
}); 

$this->group(['prefix' => 'directory'],function(){
	$this->get('/operation/{id}',['as' => 'opal.directory.operation','uses' => 'directoryController@operation']);
	$this->get('/department/{id}',['as' => 'opal.directory.department','uses' => 'directoryController@department']);

	$this->post('/clearance',['as' => 'opal.department.clearance','uses' => 'directoryController@clearance']);
});

$this->group(['prefix' => 'campaigns'],function(){
	$this->get('/',['as' => 'opal.campaigns.index','uses' => 'campaignsController@index']);
	$this->post('/',['as' => 'opal.campaigns.create','uses' => 'campaignsController@create']);
	$this->get('/{id}',['as' => 'opal.campaigns.get','uses' => 'campaignsController@get']);
	$this->post('/',['as' => 'opal.campaigns.addsup','uses' => 'campaignsController@add_sup']);
	$this->post('/addtl',['as' => 'opal.campaigns.addtl','uses' => 'campaignsController@add_tl']);
	$this->post('/addagent',['as' => 'opal.campaigns.addagent','uses' => 'campaignsController@add_agent']);
}); 

$this->group(['prefix' => 'announcements'],function(){

	$this->get('/',['as' => 'opal.announcements.index','uses' => 'announcementsController@index']);
	$this->post('/',['as' => 'opal.announcements.create','uses' => 'announcementsController@create']);
	$this->get('/view/{id}',['as' => 'opal.announcements.get','uses' => 'announcementsController@get']);
	$this->get('/edit/{id}',['as' => 'opal.announcements.edit','uses' => 'announcementsController@edit']);
	$this->get('/seen/{id}',['as' => 'opal.announcements.seen','uses' => 'announcementsController@seen']);
	
}); 


$this->group(['prefix' => 'approval'],function(){

	$this->get('/nonagent',['as' => 'opal.approval.nonagent','uses' => 'approvalController@nonagent']);

}); 

$this->group(['prefix' => 'clearance'],function(){

	$this->get('/',['as' => 'opal.clearance.index','uses' => 'clearanceController@index']);
	$this->post('/update',['as' => 'opal.department.clearance_update','uses' => 'clearanceController@clearance_update']);
	$this->post('/export',['as' => 'opal.clearance.export','uses' => 'clearanceController@export']);
	$this->post('/close',['as' => 'opal.department.clearance_close','uses' => 'clearanceController@clearance_close']);

}); 
