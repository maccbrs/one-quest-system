<?php

$this->group(['prefix' => 'user'],function(){
	$this->get('/',['as' => 'lilac.user.index','uses' => 'userController@index']);
	$this->post('/update/{id}',['as' => 'lilac.user.update','uses' => 'userController@update']);
	$this->post('/create',['as' => 'lilac.user.create','uses' => 'userController@create']);   
	$this->post('/access_edit',['as' => 'lilac.user.access_edit','uses' => 'userController@access_edit']);  
	$this->post('/empcode_edit/{id}',['as' => 'lilac.user.empcode_edit','uses' => 'userController@empcode_edit']);  
	$this->post('/access_create',['as' => 'lilac.user.access_create','uses' => 'userController@access_create']);  
}); 

$this->group(['prefix' => 'campaign'],function(){
	$this->get('/',['as' => 'lilac.campaign.index','uses' => 'campaignController@index']);
	$this->post('/create',['as' => 'lilac.campaign.create','uses' => 'campaignController@create']);
	$this->post('/update/{id}',['as' => 'lilac.campaign.update','uses' => 'campaignController@update']);	
}); 

