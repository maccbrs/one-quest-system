<?php
 
	$this->group(['prefix' => '/'],function(){
		//$this->get('/',['as' => 'bloodstone.home','uses' => 'InventoryController@index']);
		/*$this->get('/mstbrand/create',['as' => 'bloodstone.home','uses' => 'InventoryController@index']);*/
		$this->get('/ajax/{unique_id}',['as' => 'bloodstone.ajax.home','uses' => 'AjaxController@displayloghistory']);
		$this->get('/ajaxresetpass/',['as' => 'bloodstone.ajax.home','uses' => 'AjaxController@resetpassword']);
		
	
		});

	$this->group(['prefix' => 'tpcast'],function(){
		
		$this->get('/',['as' => 'emerald.tpcast','uses' => 'CrudController@index']);
		$this->get('/form',['as' => 'emerald.tpcast.form','uses' => 'CrudController@formview']);
		$this->get('/dropdown',['as' => 'emerald.tpcast.dropdown','uses' => 'CrudController@dropdownview']);
		$this->get('/dropdownlist/{list}',['as' => 'emerald.tpcast.dropdownlist','uses' => 'CrudController@dropdownviewlist']);
		$this->post('/form/create',['as' => 'emerald.tpcast.form.create','uses' => 'CrudController@formstore']); 
		$this->post('/form/edit',['as' => 'emerald.tpcast.form.edit','uses' => 'CrudController@update']); 
		$this->post('/dropdown/create',['as' => 'emerald.tpcast.dropdown.create','uses' => 'CrudController@store']); 
		$this->post('/dropdown/edit',['as' => 'emerald.tpcast.dropdown.edit','uses' => 'CrudController@update']);
		//$this->post('/dropdown/create',['as' => 'emerald.tpcast.dropdown.create','uses' => 'CrudController@update']); 
		
	});
	
	
	$this->group(['prefix' => 'ajax'],function(){
	
		//$this->get('/{unique_id}',['as' => 'bloodstone.ajax.home','uses' => 'AjaxController@displayloghistory']);
	});
	
	$this->group(['prefix' => 'masterfile'],function(){
		//$this->get('/',['as' => 'bloodstone.home','uses' => 'InventoryController@index']);
		//$this->get('/masterfile/brand/create',['as' => 'bloodstone.masterfile.brand.create','uses' => 'InventoryController@index']);

		$this->get('/{masterfile}',['as' => 'bloodstone.masterfile.home','uses' => 'InventoryController@masterfile']);
		//$this->post('/{masterfile}/create',['as' => 'bloodstone.masterfile.create','uses' => 'InventoryController@store']); 
		
		$this->post('/{masterfile}/create',['as' => 'bloodstone.masterfile.create','uses' => 'CrudController@store']); 
		
		$this->post('/{masterfile}/edit',['as' => 'bloodstone.masterfile.edit','uses' => 'CrudController@update']); 
		$this->post('/edit',['as' => 'gemstone.system.edit','uses' => 'systemparamController@update']); 
		$this->post('/{masterfile}/batchcreate',['as' => 'bloodstone.masterfile.batchcreate','uses' => 'InventoryController@batch']); 
	
	});	

 ?> 