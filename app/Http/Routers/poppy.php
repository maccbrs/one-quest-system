<?php

$this->get('/',['as' => 'poppy.index','uses' => 'poppyController@index']);

$this->group(['prefix' => 'ticket'],function(){
	$this->get('/create',['as' => 'poppy.ticket.create','uses' => 'ticketController@create']);
	$this->post('/store',['as' => 'poppy.ticket.store','uses' => 'ticketController@store']);
	$this->post('/reply/{ticketId}/{userId}',['as' => 'poppy.ticket.reply','uses' => 'ticketController@reply']);
	$this->get('/open',['as' => 'poppy.ticket.open','uses' => 'ticketController@open']);
	$this->get('/answered',['as' => 'poppy.ticket.answered','uses' => 'ticketController@answered']);
	$this->get('/webdev',['as' => 'poppy.ticket.webdev','uses' => 'ticketController@webdev']);
}); 