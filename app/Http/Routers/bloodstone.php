<?php
 
	$this->group(['prefix' => '/'],function(){
		$this->get('/',['as' => 'bloodstone.home','uses' => 'InventoryController@index']);
		/*$this->get('/mstbrand/create',['as' => 'bloodstone.home','uses' => 'InventoryController@index']);*/
		$this->get('/ajax/{unique_id}',['as' => 'bloodstone.ajax.home','uses' => 'AjaxController@displayloghistory']);
		$this->get('/ajaxresetpass/',['as' => 'bloodstone.ajax.home','uses' => 'AjaxController@resetpassword']);
		
	
		});

	$this->group(['prefix' => 'forms'],function(){
		$this->get('/main-form',['as' => 'bloodstone.forms.main','uses' => 'InventoryFormController@main']);

		$this->post('/main-form',['as' => 'bloodstone.forms.main','uses' => 'InventoryFormController@store']); 
		$this->post('/main-form-update',['as' => 'bloodstone.forms.update','uses' => 'InventoryFormController@update']); 
		$this->post('/main-form-return',['as' => 'bloodstone.forms.return','uses' => 'InventoryFormController@returnAssets']); 
	});
	
	
	$this->group(['prefix' => 'ajax'],function(){
	
		//$this->get('/{unique_id}',['as' => 'bloodstone.ajax.home','uses' => 'AjaxController@displayloghistory']);
	});
	
	$this->group(['prefix' => 'masterfile'],function(){
		//$this->get('/',['as' => 'bloodstone.home','uses' => 'InventoryController@index']);
		//$this->get('/masterfile/brand/create',['as' => 'bloodstone.masterfile.brand.create','uses' => 'InventoryController@index']);

		$this->get('/{masterfile}',['as' => 'bloodstone.masterfile.home','uses' => 'InventoryController@masterfile']);
		//$this->post('/{masterfile}/create',['as' => 'bloodstone.masterfile.create','uses' => 'InventoryController@store']); 
		
		$this->post('/{masterfile}/create',['as' => 'bloodstone.masterfile.create','uses' => 'CrudController@store']); 
		
		$this->post('/{masterfile}/edit',['as' => 'bloodstone.masterfile.edit','uses' => 'CrudController@update']); 
		$this->post('/edit',['as' => 'gemstone.system.edit','uses' => 'systemparamController@update']); 
		$this->post('/{masterfile}/batchcreate',['as' => 'bloodstone.masterfile.batchcreate','uses' => 'InventoryController@batch']); 
	
	});	

 ?> 