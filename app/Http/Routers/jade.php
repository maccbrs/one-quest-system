<?php 

$this->group(['prefix' => 'dashboard'],function(){
	$this->get('/index',['as' => 'jade.dashboard.index','uses' => 'dashboardController@index']);
	$this->get('/load/{id}',['as' => 'jade.dashboard.load','uses' => 'dashboardController@load']);
	$this->get('/overage/{id}',['as' => 'jade.dashboard.overage','uses' => 'dashboardController@overage']);
	$this->get('/client/{id}',['as' => 'jade.dashboard.client','uses' => 'dashboardController@client']);
	$this->get('/hangups',['as' => 'jade.dashboard.hangups','uses' => 'dashboardController@hangups']);
});

$this->group(['prefix' => 'users'],function(){
	$this->get('/index',['as' => 'jade.users.index','uses' => 'usersController@index']);
	$this->post('/create',['as' => 'jade.users.create','uses' => 'usersController@create']);
	$this->get('/add',['as' => 'jade.users.add','uses' => 'usersController@add']);
});

$this->group(['prefix' => 'subscribers'],function(){
	$this->get('/index',['as' => 'jade.subscribers.index','uses' => 'subscribersController@index']);
	$this->post('/create',['as' => 'jade.subscribers.create','uses' => 'subscribersController@create']);
	$this->post('/{id}/delete',['as' => 'jade.subscribers.delete','uses' => 'subscribersController@delete']);
	$this->get('/add',['as' => 'jade.subscribers.add','uses' => 'subscribersController@add']);
	$this->get('/{id}/did-index',['as' => 'jade.subscribers.did-index','uses' => 'subscribersController@didIndex']);
	$this->get('/{id}/did-add',['as' => 'jade.subscribers.did-add','uses' => 'subscribersController@didAdd']);
	$this->post('/{id}/did-create',['as' => 'jade.subscribers.did-create','uses' => 'subscribersController@didCreate']);
	$this->post('/{id}/did-delete',['as' => 'jade.subscribers.did-delete','uses' => 'subscribersController@didDelete']);

	$this->get('/{id}/load-index',['as' => 'jade.subscribers.load-index','uses' => 'subscribersController@loadIndex']);
	$this->get('/{id}/load-add',['as' => 'jade.subscribers.load-add','uses' => 'subscribersController@loadAdd']);
	$this->get('/{id}/load-view',['as' => 'jade.subscribers.load-view','uses' => 'subscribersController@loadView']);
    $this->get('/{id}/load-edit',['as' => 'jade.subscribers.load-edit','uses' => 'subscribersController@loadEdit']);
	$this->post('/{id}/load-create',['as' => 'jade.subscribers.load-create','uses' => 'subscribersController@loadCreate']);
	$this->post('/{id}/load-delete',['as' => 'jade.subscribers.load-delete','uses' => 'subscribersController@loadDelete']);
	$this->post('/{id}/load-addminute',['as' => 'jade.subscribers.load-addminute','uses' => 'subscribersController@loadAddminute']);
	$this->get('/{id}/load-reserved',['as' => 'jade.subscribers.load-reserved','uses' => 'subscribersController@loadReserved']);	
	$this->post('/{id}/load-addreserved',['as' => 'jade.subscribers.load-addreserved','uses' => 'subscribersController@loadAddreserved']);
	$this->get('/settings/{id}',['as' => 'jade.subscribers.settings','uses' => 'subscribersController@settings']);
	$this->post('/settings-account-type-update/{id}',['as' => 'jade.subscribers.settings-account-type-update','uses' => 'subscribersController@settingsAccountTypeUpdate']);
});

$this->group(['prefix' => 'test'],function(){
	$this->get('/index',['as' => 'jade.test.index','uses' => 'testController@index']);
});