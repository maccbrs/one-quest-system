<?php
 
	$this->group(['prefix' => 'dashboard'],function(){
		$this->get('/',['as' => 'gem.dashboard.index','uses' => 'dashboardController@index']);
		//$this->get('/dateupdatehrd',['as' => 'gem.dashboard.index','uses' => 'dashboardController@dateupdatehrd']);
		
	});

	$this->group(['prefix' => 'tqa'],function(){
		$this->get('/',['as' => 'gem.tqa.index','uses' => 'tqaController@index']);
	});

	$this->group(['prefix' => 'announcements'],function(){
		$this->get('/lists/{source}/{status}',['as' => 'gem.announcements.lists','uses' => 'announcementsController@lists']);
		$this->get('/list/{id}',['as' => 'gem.announcements.list','uses' => 'announcementsController@lizt']);

		$this->post('/announcement-update',['as' => 'gem.announcements.announcement-update','uses' => 'announcementsController@update']);
		$this->post('/announcement-delete',['as' => 'gem.announcements.announcement-delete','uses' => 'announcementsController@delete']);
	});

	$this->group(['prefix' => 'messages'],function(){

		$this->get('/messages/{source}/{status}',['as' => 'gem.messages.lists','uses' => 'messagesController@listz']);
		$this->get('/list/{id}',['as' => 'gem.messages.list','uses' => 'messagesController@lizt']);
		$this->get('/grouplist/{id}',['as' => 'gem.messages.grouplist','uses' => 'messagesController@grouplist']);
	});

	$this->group(['prefix' => 'qanda'],function(){
		$this->get('/',['as' => 'gem.qanda.index','uses' => 'qandaController@index']);
		$this->get('/add',['as' => 'gem.qanda.add','uses' => 'qandaController@add']);
		$this->post('/create',['as' => 'gem.qanda.create','uses' => 'qandaController@create']);
		$this->get('/dept/{id}',['as' => 'gem.qanda.dept','uses' => 'qandaController@dept']);
		$this->get('/agentlist',['as' => 'gem.qanda.agentlist','uses' => 'qandaController@agentlist']);
		$this->get('/nonagentlist',['as' => 'gem.qanda.nonagentlist','uses' => 'qandaController@nonagentlist']);
	})
;
	$this->group(['prefix' => 'staff'],function(){
		$this->get('/',['as' => 'gem.staff.index','uses' => 'staffController@index']);
	});	

	$this->group(['prefix' => 'users'],function(){
		$this->get('/',['as' => 'gem.users.index','uses' => 'usersController@index']);
		$this->get('/pending-users',['as' => 'gem.users.pending_users','uses' => 'usersController@pending_users']);
		$this->get('/agents',['as' => 'gem.users.agents','uses' => 'usersController@agents']);

		$this->post('/gem-register',['as' => 'gem.users.register','uses' => 'usersController@register']);
 		$this->post('/gem-agent-register',['as' => 'gem.users.agent_register','uses' => 'usersController@agent_register']);
		$this->post('/gem-agent-edit',['as' => 'gem.users.agent_edit','uses' => 'usersController@agent_edit']);

		$this->post('/settings/{id}',['as' => 'gem.users.settings','uses' => 'usersController@settings']);
		$this->get('/connector/{id}',['as' => 'gem.users.connector','uses' => 'usersController@connector']);
		$this->post('/connect',['as' => 'gem.users.connect','uses' => 'usersController@connect']);
		$this->post('/search',['as' => 'gem.users.search','uses' => 'usersController@search']);

		
	});	

	$this->group(['prefix' => 'campaign'],function(){
		$this->get('/',['as' => 'gem.campaign.index','uses' => 'campaignController@index']);
		$this->post('/store',['as' => 'gem.campaign.store','uses' => 'campaignController@store']);
	});	

	$this->group(['prefix' => 'approval'],function(){
		$this->get('/disputes',['as' => 'gem.approval.disputes','uses' => 'approvalController@disputes']);
		$this->get('/disputes2',['as' => 'gem.approval.disputes2','uses' => 'approvalController@disputes2']);
		$this->get('/backup-approver',['as' => 'gem.approval.backup_approver','uses' => 'approvalController@backup_approver']);
		$this->post('/disputes-update',['as' => 'gem.approval.dispute-update','uses' => 'approvalController@disputes_update']);
		$this->post('/disputes-posted',['as' => 'gem.approval.dispute-posted','uses' => 'approvalController@disputes_posted']);
		$this->post('/disputes-update2',['as' => 'gem.approval.dispute-update2','uses' => 'approvalController@disputes_update2']);
	});	

	$this->group(['prefix' => 'request'],function(){
		$this->get('/disputes',['as' => 'gem.request.disputes','uses' => 'requestController@disputes']);
		$this->post('/disputes-create',['as' => 'gem.request.dispute-create','uses' => 'requestController@dispute_create']);
		$this->post('/disputes-delete',['as' => 'gem.request.dispute-delete','uses' => 'requestController@dispute_delete']);
		$this->post('/disputes-update',['as' => 'gem.request.dispute-update','uses' => 'requestController@dispute_update']);
	});			

	$this->group(['prefix' => 'campaign'],function(){
		$this->get('/add',['as' => 'gem.campaign.add','uses' => 'campaignController@add']);
		$this->post('/create',['as' => 'gem.campaign.create','uses' => 'campaignController@create']);
	});	

	$this->group(['prefix' => 'dids'],function(){
		$this->get('/index',['as' => 'gem.did.index','uses' => 'didController@index']);
		$this->post('/create',['as' => 'gem.did.create','uses' => 'didController@create']);
		$this->get('/testcalls',['as' => 'gem.did.testcalls','uses' => 'didController@testcalls']);
		$this->post('/create-tc',['as' => 'gem.did.createtc','uses' => 'didController@create_tescalls']);
		$this->get('/testcall/{id}',['as' => 'gem.did.testcall','uses' => 'didController@testcall']);
		$this->post('/mbvalid/{id}',['as' => 'gem.did.mbvalid','uses' => 'didController@mbvalid']);
		$this->post('/mbcheckupdate',['as' => 'gem.did.mbcheckupdate','uses' => 'didController@mbcheckupdate']);
		$this->post('/notes/{id}',['as' => 'gem.did.notes','uses' => 'didController@notes']);
	});

	$this->group(['prefix' => 'garnet'],function(){
		$this->get('/agents',['as' => 'gem.garnet.agents','uses' => 'garnetController@agents']);
		$this->post('/agent-add',['as' => 'gem.garnet.add_agent','uses' => 'garnetController@add_agent']);
		$this->post('/agent-edit/{id}',['as' => 'gem.garnet.edit_agent','uses' => 'garnetController@edit_agent']);
		$this->get('/teamleads',['as' => 'gem.garnet.teamleads','uses' => 'garnetController@teamleads']);
		
		$this->post('/teamlead-add',['as' => 'gem.garnet.add_teamlead','uses' => 'garnetController@add_teamlead']);
		$this->post('/teamlead-edit/{id}',['as' => 'gem.garnet.edit_teamlead','uses' => 'garnetController@edit_teamlead']);	
		$this->get('/supervisors',['as' => 'gem.garnet.supervisors','uses' => 'garnetController@supervisors']);	

		$this->post('/supervisors-add',['as' => 'gem.garnet.add_supervisors','uses' => 'garnetController@add_supervisors']);			
	});



 ?> 