<?php

$this->group(['prefix' => 'announcements'],function(){
	$this->get('/',['as' => 'amethyst.announcements.index','uses' => 'announcementsController@index']);
	$this->post('/',['as' => 'amethyst.announcements.create','uses' => 'announcementsController@create']);
	$this->get('/view/{id}',['as' => 'amethyst.announcements.get','uses' => 'announcementsController@get']);
	$this->get('/seen/{id}',['as' => 'amethyst.announcements.seen','uses' => 'announcementsController@seen']);

}); 


$this->group(['prefix' => 'approver'],function(){

	$this->get('/',['as' => 'amethyst.approver.index','uses' => 'approverController@index']);
	$this->post('/',['as' => 'amethyst.approver.create','uses' => 'approverController@create']);


}); 

$this->group(['prefix' => 'noc_stat'],function(){

	$this->get('/noc-stat',['middleware' => ['it_manager'],'as' => 'amethyst.noc_stat.index','uses' => 'nocStatController@index']);
	$this->post('/attendance',['middleware' => ['it_manager'],'as' => 'amethyst.noc_stat.attendance','uses' => 'nocStatController@attendance']);

});