<?php 

$this->group(['prefix' => 'you'],function(){

	$this->get('/index',['as' => 'topaz.you.index','uses' => 'youController@index']);
	$this->get('/create',['as' => 'topaz.you.create','uses' => 'youController@create']);
	$this->post('/store',['as' => 'topaz.you.store','uses' => 'youController@store']);
	$this->get('/show/{id}',['as' => 'topaz.you.show','uses' => 'youController@show']);
	$this->get('/show_cc/{id}',['as' => 'topaz.you.show_cc','uses' => 'youController@show_cc']);
	$this->post('/reply',['as' => 'topaz.you.reply','uses' => 'youController@reply']);
	$this->post('/close/{id}',['as' => 'topaz.you.close','uses' => 'youController@close']);
	$this->post('/reopen/{id}',['as' => 'topaz.you.reopen','uses' => 'youController@reopen']);
	$this->post('/add_cc',['as' => 'topaz.you.add_cc','uses' => 'youController@add_cc']); 

});  
 
$this->group(['prefix' => 'department'],function(){

	$this->get('/index',['as' => 'topaz.department.index','uses' => 'departmentController@index']);
	$this->post('/store',['as' => 'topaz.department.store','uses' => 'departmentController@store']);
	$this->get('/show/{id}',['as' => 'topaz.department.show','uses' => 'departmentController@show']);
	$this->get('/users/{id}',['as' => 'topaz.department.users','uses' => 'departmentController@users']);
	$this->post('/user_store',['as' => 'topaz.department.user_store','uses' => 'departmentController@user_store']);
	$this->get('/admin/{id}',['as' => 'topaz.department.admin','uses' => 'departmentController@admin']);
	$this->post('/admin_store',['as' => 'topaz.department.admin_store','uses' => 'departmentController@admin_store']);
	$this->get('/tickets/{id}',['as' => 'topaz.department.tickets','uses' => 'departmentController@tickets']);
	$this->get('/ticket/{id}',['as' => 'topaz.department.ticket','uses' => 'departmentController@ticket']);
 	$this->post('/status',['as' => 'topaz.department.status','uses' => 'departmentController@status']);
 	$this->post('/assign',['as' => 'topaz.department.assign','uses' => 'departmentController@assign']);

});   

$this->group(['prefix' => 'score'],function(){
	$this->get('/index/{id}',['as' => 'topaz.score.index','uses' => 'scoreController@index']);
});

$this->group(['prefix' => 'profile'],function(){
	$this->get('/picture',['as' => 'topaz.profile.picture','uses' => 'profileController@picture']);
	$this->post('/update_pic',['as' => 'topaz.profile.update_pic','uses' => 'profileController@update_pic']);
	$this->post('/password',['as' => 'topaz.profile.password','uses' => 'profileController@password']);
});