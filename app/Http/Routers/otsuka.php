<?php

	$this->group(['prefix' => '/'],function(){		
		$this->get('/',['as' => 'otsuka.index','uses' => 'otsukaController@index']);		
		$this->post('/',['as' => 'otsuka.index','uses' => 'otsukaController@update_image']);
		$this->get('/pqcform',['as' => 'otsuka.pqc','uses' => 'otsukaController@pqcformget']);		
		$this->post('/pqcform/save',['as' => 'otsuka.pqc.save','uses' => 'otsukaController@pqcsave']);						
		$this->post('/pqcform/update',['as' => 'otsuka.pqc.update','uses' => 'otsukaController@pqcupdate']);		
		$this->get('/pqcform/{id}',['as' => 'otsuka.pqc.view','uses' => 'otsukaController@viewpqc']);		
		$this->get('/pqcformpost/{id}',['as' => 'otsuka.pqc.post','uses' => 'otsukaController@postpqc']);
		$this->post('/disable',['as' => 'otsuka.disable','uses' => 'otsukaController@disable']);
		$this->get('/disable',['as' => 'otsuka.disable-upload','uses' => 'otsukaController@disable_upload']);
		$this->get('/logout',['as' => 'otsuka.logout','uses' => 'otsukaController@logout']);

		$this->group(['prefix' => 'patient'],function(){
		$this->get('/',['as' => 'otsuka.patient','uses' => 'patientController@index']);
		$this->get('/enroll_patient',['as' => 'otsuka.patient.enroll','uses' => 'patientController@enroll_patient']);
		$this->post('/enroll_patient',['as' => 'otsuka.patient.enroll','uses' => 'patientController@enroll_patient']);
		});
		
		
		$this->get('/aeform',['as' => 'otsuka.pv','uses' => 'otsukaController@aeformget']);
		$this->post('/aeform/save',['as' => 'otsuka.ae-form.save','uses' => 'otsukaController@aeformsave']);
		$this->post('/aeform/update',['as' => 'otsuka.ae-form.update','uses' => 'otsukaController@aeupdate']);
		$this->get('/aeform/{id}',['as' => 'otsuka.ae.view','uses' => 'otsukaController@aeview']);		
		$this->get('/aepost/{id}',['as' => 'otsuka.ae.post','uses' => 'otsukaController@postae']);
		
		$this->get('/pqc-ev-queue',['as' => 'otsuka.pqc-ev-report','uses' => 'otsukaController@pqcev_list']);				
	
		});
		

		$this->group(['prefix' => 'profile'],function(){
			$this->get('/',['as' => 'otsuka.profile.index','uses' => 'profileController@picture']);
			$this->post('/update_pic',['as' => 'otsuka.profile.update_pic','uses' => 'profileController@update_pic']);
			$this->post('/password',['as' => 'otsuka.profile.password','uses' => 'profileController@password']);
		});
		
		
	$this->group(['prefix' => 'uploader'],function(){
		$this->get('/',['as' => 'otsuka.uploader.index','uses' => 'uploaderController@index']);
		$this->post('/supporting-docs',['as' => 'otsuka.uploader.supportingdocs','uses' => 'uploaderController@uploadimage']);
		$this->post('/supporting-docs-update',['as' => 'otsuka.uploader.supportingdocs-update','uses' => 'uploaderController@update']);
		$this->get('/supporting-docs-view',['as' => 'otsuka.uploader.supportingdocs-view','uses' => 'uploaderController@view_upload_docs']);
		$this->get('/assign-patient-rx',['as' => 'otsuka.uploader.assign-patient-rx','uses' => 'uploaderController@assign_rx_to_patient']);
		$this->get('/send-max-rx',['as' => 'otsuka.uploader.send-max-rx','uses' => 'uploaderController@sendPMM']);
		$this->get('/all',['as' => 'otsuka.uploader.supportingdocs-all','uses' => 'uploaderController@view_all']);
		//$this->post('/password',['as' => 'otsuka.profile.password','uses' => 'profileController@password']);
	});

	$this->group(['prefix' => 'emailer'],function(){
		$this->get('/',['as' => 'otsuka.emailer.index','uses' => 'emailerController@index']);
		$this->get('/pmm-send',['as' => 'otsuka.emailer.pmm-send','uses' => 'emailerController@send_pmm']);
		//$this->post('/supporting-docs',['as' => 'otsuka.uploader.supportingdocs','uses' => 'uploaderController@uploadimage']);
		//$this->get('/supporting-docs-view',['as' => 'otsuka.uploader.supportingdocs-view','uses' => 'uploaderController@view_upload_docs']);
		//$this->post('/password',['as' => 'otsuka.profile.password','uses' => 'profileController@password']);
	});
	
	$this->group(['prefix' => 'agent'],function(){
		$this->get('/upload-queue',['as' => 'otsuka.dashboard.queue','uses' => 'queueController@index']);
		$this->get('/verification-queue',['as' => 'otsuka.dashboard.verification-queue','uses' => 'queueController@verification_queue']);
		$this->get('/verification-callback-queue',['as' => 'otsuka.dashboard.verification-callback-queue','uses' => 'queueController@verification_callback_queue']);
		$this->get('/redemption-queue',['as' => 'otsuka.dashboard.redemption-queue','uses' => 'queueController@redemption_queue']);
		$this->get('/supporting-docs-queue',['as' => 'otsuka.dashboard.supporting-docs-queue','uses' => 'queueController@supporting_docs_queue']);
		$this->get('/compliance-queue',['as' => 'otsuka.dashboard.compliance-queue','uses' => 'queueController@compliance_queue']);
		$this->get('/compliance-queue-v2',['as' => 'otsuka.dashboard.compliance-queue-v2','uses' => 'queueController@compliance_queue_v2']);
		$this->get('/compliance-ref-voluntary-queue',['as' => 'otsuka.dashboard.compliance-ref-voluntary-queue','uses' => 'queueController@compliance_ref_voluntary_queue']);
		$this->get('/encode',['as' => 'otsuka.encode','uses' => 'queueController@encode']);
		$this->post('/enroll',['as' => 'otsuka.enroll','uses' => 'queueController@enroll']);
		$this->post('/enroll-proceed',['as' => 'otsuka.enroll-proceed','uses' => 'queueController@enroll_proceed']);
		$this->post('/enroll-unclaim',['as' => 'otsuka.unclaim','uses' => 'queueController@unclaim']);
		$this->get('/enroll-reciept',['as' => 'otsuka.enroll-reciept','uses' => 'queueController@enroll_reciept']);
		$this->post('/enroll-reciept',['as' => 'otsuka.enroll-reciept','uses' => 'queueController@enroll_reciept']);
		$this->post('/reciept',['as' => 'otsuka.reciept','uses' => 'queueController@reciept']);

		$this->get('/upload-queue2',['as' => 'otsuka.dashboard.queue2','uses' => 'queue2Controller@getQueue']);
		$this->post('/upload-queue2',['as' => 'otsuka.dashboard.postqueue2','uses' => 'queue2Controller@postQueue']);
	});
	
	
	$this->group(['prefix' => 'receipt'],function(){
		$this->post('/encode',['as' => 'otsuka.receipt.create','uses' => 'receiptController@create']);
		$this->get('/encode-item-dtl',['as' => 'otsuka.receipt-item-dtl.create','uses' => 'receiptController@createdtl']);
		$this->get('/encode-item-dtl-remove',['as' => 'otsuka.receipt-item-dtl.remove','uses' => 'receiptController@removedtl']);
	});
	
	
	
	$this->group(['prefix' => 'ajax'],function(){
		$this->get('/searchperson',['as' => 'otsuka.ajax.searchperson','uses' => 'ajaxController@searchPerson']);
		$this->get('/searchpatient',['as' => 'otsuka.ajax.searchppatient','uses' => 'ajaxController@searchPatient']);
		$this->get('/searchPatient_only',['as' => 'otsuka.ajax.searchppatient','uses' => 'ajaxController@searchPatient_only']);
		$this->get('/displaypatient',['as' => 'otsuka.patient.displaypatient','uses' => 'ajaxController@displaypatient']);
		$this->get('/reject_upload',['as' => 'otsuka.ajax.rejectupload','uses' => 'ajaxController@reject_upload']);
		$this->get('/utility',['as' => 'otsuka.ajax.utility','uses' => 'utilityController@utility']);
		$this->get('/get_national_md_record',['as' => 'otsuka.ajax.get_national_md_record','uses' => 'ajaxController@get_national_md_record']);
		$this->get('/get_patient_kit_record',['as' => 'otsuka.ajax.get_patient_kit_record','uses' => 'ajaxController@get_patient_kit_record']);
		$this->get('/get_upload_record',['as' => 'otsuka.ajax.get_upload_record','uses' => 'ajaxController@get_upload_record']);
		$this->get('/update_upload',['as' => 'otsuka.ajax.update_upload','uses' => 'ajaxController@update_upload_info']);
		$this->get('/get_supp_upload',['as' => 'otsuka.ajax.get_supp_upload','uses' => 'ajaxController@get_supp_upload']);
		
		$this->get('/compliance_list',['as' => 'otsuka.json.compliance','uses' => 'apiController@compliance_list']);
		
		
		
	});
	
	$this->group(['prefix' => 'redemption'],function(){
		$this->get('/',['as' => 'otsuka.redemption.index','uses' => 'redemptionController@index']);
		$this->get('/enroll_patient',['as' => 'otsuka.redemption.enroll','uses' => 'redemptionController@enroll_patient']);
		$this->get('/update_patient_med_info',['as' => 'otsuka.redemption.patient-med-info','uses' => 'redemptionController@updated_patient_med_info']);
		$this->get('/assign_patient',['as' => 'otsuka.redemption.assign','uses' => 'redemptionController@assign_patient']);
		$this->get('/assign_receipt_to_patient',['as' => 'otsuka.redemption.assign_receipt','uses' => 'redemptionController@assign_receipt_to_patient']);
		$this->get('/done',['as' => 'otsuka.redemption.done','uses' => 'redemptionController@done']);
		$this->get('/revert-receipt',['as' => 'otsuka.redemption.revert-receipt','uses' => 'redemptionController@revert_receipt']);
		$this->get('/savecalllog',['as' => 'otsuka.redemption.savecalllog','uses' => 'redemptionController@save_call_log']);
		$this->get('/dispatch-save',['as' => 'otsuka.redemption.dispatch-save','uses' => 'redemptionController@Save_Dispatch']);
		$this->get('/dispatch-mst-save',['as' => 'otsuka.redemption.dispatch-mst-save','uses' => 'redemptionController@Save_Dispatchmst']);
		$this->get('/dispatch-post',['as' => 'otsuka.redemption.dispatch-post','uses' => 'redemptionController@Dispatch_post']);

		$this->get('/dispatch-report',['as' => 'otsuka.redemption.dispatch-report','uses' => 'redemptionreportController@dispatch_report']);
		$this->get('/zpc-dispatch-report',['as' => 'otsuka.redemption.zpc-dispatch-report','uses' => 'redemptionreportController@zpc_dispatch_report']);
		$this->get('/ytd-report',['as' => 'otsuka.redemption.ytd-report','uses' => 'ytdredemptionreportController@ytd_report']);
		$this->get('/mtd-report',['as' => 'otsuka.redemption.mtd-report','uses' => 'mtdredemptionreportController@mtd_report']);

		//$this->POST('/enroll_patient',['as' => 'otsuka.redemption.postenroll','uses' => 'redemptionController@post_enroll_patient']);
		
	});	
	
	$this->group(['prefix' => '/compliance'],function(){
		$this->get('/compliance-encode',['as' => 'otsuka.compliance','uses' => 'complianceController@index']);
		$this->get('/compliance-ref-voluntary-encode',['as' => 'otsuka.compliance-ref-voluntary','uses' => 'complianceController@ref_voluntary_index']);
		$this->get('/compliance-save',['as' => 'otsuka.compliance.save','uses' => 'complianceController@save']);
		$this->get('/compliance-ref-voluntary-save',['as' => 'otsuka.compliance.ref-voluntary-save','uses' => 'complianceController@save_ref_voluntary']);
	});
	
	$this->group(['prefix' => '/'],function(){
		/*$this->get('/verify',['as' => 'otsuka.verify','uses' => 'verifyController@index']);*/
		$this->get('/verify',['as' => 'otsuka.verify','uses' => 'verifyController@verify']);
		$this->post('/verify-unclaim',['as' => 'otsuka.verify.unclaim','uses' => 'verifyController@unclaim']);
		$this->post('/verify-save-details',['as' => 'otsuka.save-details','uses' => 'verifyController@encode_details']);
		$this->post('/verify-status',['as' => 'otsuka.save','uses' => 'verifyController@encode_verify']);
		$this->post('/encode-edit',['as' => 'otsuka.encode_edit','uses' => 'verifyController@encode_edit']);
		$this->post('/encode-proceed',['as' => 'otsuka.encode_proceed','uses' => 'verifyController@encode_proceed']);

	});
	
	$this->group(['prefix' => 'uploads'],function(){
		$this->get('/',['as' => 'otsuka.uploads','uses' => 'uploadController@index']);
		$this->post('/',['as' => 'otsuka.post.uploads','uses' => 'uploadController@postUpload']);

		$this->get('/uploadByMedRep',['as' => 'otsuka.uploads.bymedrep','uses' => 'uploadController@uploadByMedRep']);
		$this->post('/postuploadByMedRep',['as' => 'otsuka.uploads.postbymedrep','uses' => 'uploadController@postuploadByMedRep']);
	});

	$this->group(['prefix' => 'test'],function(){
		$this->get('/',['as' => 'otsuka.test','uses' => 'testController@index']);
	});

	$this->group(['prefix' => '/'],function(){
		$this->post('/send',['as' => 'otsuka.send','uses' => 'sendController@send']);
	});
	

	$this->group(['prefix' => '/reports'],function(){
		$this->get('/',['as' => 'otsuka.generate','uses' => 'reportsController@index']);
		$this->get('/eom',['as' => 'otsuka.generate-eom','uses' => 'reportsController@index_eom']);
		$this->get('/individual', ['as' => 'otsuka.daily-individual','uses' => 'reportsController@individual']);
		$this->get('/generate-report',['as' => 'otsuka.generate-report','uses' => 'reportsController@generate']);
		$this->get('/generate-report-eom',['as' => 'otsuka.generate-report-eom','uses' => 'reportsController@generate_eom']);
		$this->get('/generate-invalid',['as' => 'otsuka.generate-invalid','uses' => 'reportsController@generate_invalid']);
		$this->get('/generate-summary',['as' => 'otsuka.generate-summary','uses' => 'reportsController@generate_summary']);
		$this->get('/generate-summary-eom',['as' => 'otsuka.generate-summary-eom','uses' => 'reportsController@generate_summary_eom']);
		$this->get('/generate-report-daily-call',['as' => 'otsuka.generate-report-daily-call','uses' => 'reportsController@generate_daily_call']);
		$this->get('/individual-report', ['as' => 'otsuka.generate-daily-individual','uses' => 'reportsController@individual_report']);
		$this->get('/compliance-report', ['as' => 'otsuka.compliance-report', 'uses' => 'reportsController@generate_compliance_call']);




		$this->get('/pmm-approval-report', ['as' => 'otsuka.pmm-approval-report','uses' => 'pmmreportsController@pmm_eod_report']);




	});

	
	$this->group(['prefix' => '/api'],function(){
		$this->get('/upload',['as' => 'otsuka.api.upload','uses' => 'apiController@GetUpload']);
		$this->get('/records',['as' => 'otsuka.api.records','uses' => 'apiController@GetRecords']);
		
	});
	
	
	$this->group(['prefix' => '/masterfile'],function(){
		
		
		$this->get('/',['as' => 'otsuka.masterfile','uses' => 'MasterFileController@index']);
		$this->get('/sku-groupings',['as' => 'otsuka.sku-groupings','uses' => 'MasterFileController@index']);
		$this->get('/patient-kit-allocation',['as' => 'otsuka.masterfile.patient-kit-allocation','uses' => 'MasterFileController@patient_kit_allocation']);
		$this->post('/patient-kit-allocation-save',['as' => 'otsuka.masterfile.patient-kit-allocation-save','uses' => 'MasterFileController@patient_kit_allocation_save']);
		$this->get('/queues-done',['as' => 'otsuka.masterfile.queues-done','uses' => 'MasterFileController@queues_done_index']);
		$this->get('/queues-done-save',['as' => 'otsuka.masterfile.queues-done-save','uses' => 'MasterFileController@queues_done_save']);
		
		
		$this->get('/national-md-list',['as' => 'otsuka.masterfile.national-md-list','uses' => 'MasterFileController@national_md_list']);
		$this->post('/national_md_list-save',['as' => 'otsuka.masterfile.national_md_list-save','uses' => 'MasterFileController@national_md_list_save']);
		/*$this->get('/patient-kit-allocation-reserve',['as' => 'otsuka.masterfile.patient-kit-allocation-reserve','uses' => 'MasterFileController@patient_kit_allocation_reserve']);
		$this->post('/patient-kit-allocation-reserve-save',['as' => 'otsuka.masterfile.patient-kit-allocation-reserve-save','uses' => 'MasterFileController@patient_kit_allocation_reserve_save']);
		*/
		
	});
	
	
	
	$this->group(['prefix' => 'settings'],function(){
		$this->get('/',['as' => 'otsuka.settings','uses' => 'changePasswordController@index']);
		$this->post('/adduser',['as' => 'otsuka.settings.adduser','uses' => 'changePasswordController@addUser']);
		$this->post('/',['as' => 'otsuka.settings.resetpassword','uses' => 'changePasswordController@resetpassword']);
		$this->get('/system-settings',['as' => 'otsuka.settings.system','uses' => 'SystemSettingsController@index']);
		$this->post('/add',['as' => 'otsuka.settings.addsystem','uses' => 'SystemSettingsController@addSystem']);
		$this->post('/update',['as' => 'otsuka.settings.updatesystem','uses' => 'SystemSettingsController@updateSystem']);
		$this->post('/delete',['as' => 'otsuka.settings.deletesystem','uses' => 'SystemSettingsController@deleteSystem']);
	});


	$this->group(['prefix' => 'brands'],function(){
		$this->get('/',['as' => 'otsuka.brands','uses' => 'BrandController@index']);
		$this->post('/',['as' => 'otsuka.post.brands','uses' => 'BrandController@upload']);
	});

	$this->group(['prefix' => 'fileUpload'],function(){
		$this->get('/uploadExcelView',['as' => 'otsuka.uploadExcelView','uses' => 'UploadExcelController@uploadExcelView']);
		$this->post('/uploadExcel',['as' => 'otsuka.upload.uploadExcel','uses' => 'UploadExcelController@uploadExcel']);
		
		
		$this->post('/allocationFile',['as' => 'otsuka.upload.allocationFile','uses' => 'UploadExcelController@allocationFile']);

	});


 ?> 