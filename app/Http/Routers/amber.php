<?php

$this->group(['prefix' => 'dashboard'],function(){
	$this->get('/',['as' => 'amber.dashboard.index','uses' => 'dashboardController@index']);
}); 

$this->group(['prefix' => 'you'],function(){
	$this->get('/info',['as' => 'amber.you.info','uses' => 'youController@info']);
}); 

$this->group(['prefix' => 'calls'],function(){
	$this->get('/recordings/{bound}',['as' => 'amber.calls.recordings','uses' => 'callsController@recordings']);
	$this->get('/qa/{bound}',['as' => 'amber.calls.qa','uses' => 'callsController@qa']);
}); 

$this->group(['prefix' => 'dispute'],function(){
	
	$this->get('/',['as' => 'amber.dispute.index','uses' => 'disputeController@index']);
	$this->post('/create',['as' => 'amber.dispute.create','uses' => 'disputeController@create']);
	$this->post('/update',['as' => 'amber.dispute.update','uses' => 'disputeController@update']);
	$this->post('/delete',['as' => 'amber.dispute.delete','uses' => 'disputeController@delete']);

}); 

$this->group(['prefix' => 'announcements'],function(){
	$this->get('/lists/{source}/{status}',['as' => 'amber.announcements.lists','uses' => 'announcementsController@listz']);
	$this->get('/list/{id}',['as' => 'amber.announcements.list','uses' => 'announcementsController@lizt']);

});

$this->group(['prefix' => 'newsfeed'],function(){
	$this->post('/new_post',['as' => 'amber.newsfeed.new_post','uses' => 'newsfeedController@new_post']);
	
});

$this->group(['prefix' => 'messages'],function(){

	$this->get('/messages/{source}/{status}',['as' => 'amber.messages.lists','uses' => 'messagesController@listz']);
	$this->get('/list/{id}',['as' => 'amber.messages.list','uses' => 'messagesController@lizt']);
	$this->get('/grouplist/{id}',['as' => 'amber.messages.grouplist','uses' => 'messagesController@grouplist']);
});


$this->group(['prefix' => 'team'],function(){

	$this->get('/manager',['as' => 'amber.team.manager','uses' => 'teamController@manager']);
	$this->get('/team_leader',['as' => 'amber.team.team_leader','uses' => 'teamController@team_leader']);
	$this->get('/team_mates',['as' => 'amber.team.team_mates','uses' => 'teamController@team_mates']);

	$this->get('/mail/{Administrator}',['as' => 'amber.team.noc','uses' => 'teamController@mail']);
	$this->get('/mail/{Human Resource}',['as' => 'amber.team.hr','uses' => 'teamController@mail']);
	$this->get('/mail/{Trainer}',['as' => 'amber.team.training','uses' => 'teamController@mail']);
	$this->get('/mail/{Quality Assurance}',['as' => 'amber.team.qa','uses' => 'teamController@mail']);

// messaging

	$this->post('/new_message',['as' => 'amber.team.new_message','uses' => 'teamController@new_message']);
	$this->post('/test',['as' => 'amber.team.test','uses' => 'teamController@test']);

}); 

$this->group(['prefix' => 'notes'],function(){
	

	$this->get('/notes/{Administrator}',['as' => 'amber.notes.noc','uses' => 'notesController@index']);
	$this->get('/notes/{Human Resource}',['as' => 'amber.notes.hr','uses' => 'notesController@index']);
	$this->get('/notes/{Manager}',['as' => 'amber.notes.manager','uses' => 'notesController@index']);
	$this->get('/notes/{Trainer}',['as' => 'amber.notes.trainer','uses' => 'notesController@index']);
	$this->get('/notes/{Team Leader}',['as' => 'amber.notes.teamleader','uses' => 'notesController@index']);
	$this->get('/notes/{Quality Assurance}',['as' => 'amber.notes.qa','uses' => 'notesController@index']);

}); 

$this->group(['prefix' => 'qanda'],function(){
	$this->get('/{id}',['as' => 'amber.qanda.index','uses' => 'qandaController@index']);
	$this->post('/create',['as' => 'amber.qanda.create','uses' => 'qandaController@create']);
	
}); 

$this->group(['prefix' => 'settings'],function(){
	$this->get('/',['as' => 'amber.settings.index','uses' => 'settingsController@index']);
	$this->get('/edit/{id}',['as' => 'amber.settings.edit','uses' => 'settingsController@edit']);
	$this->post('/add_info',['as' => 'amber.settings.add_info','uses' => 'settingsController@add_info']); 
	$this->post('/updateimage',['as' => 'amber.settings.updateimage','uses' => 'settingsController@update_image']);
	$this->post('/motto',['as' => 'amber.settings.motto','uses' => 'settingsController@motto']);
}); 


$this->group(['prefix' => 'profile'],function(){
	$this->get('/picture',['as' => 'amber.profile.picture','uses' => 'profileController@picture']);
	$this->post('/update_pic',['as' => 'amber.profile.update_pic','uses' => 'profileController@update_pic']);
	$this->post('/password',['as' => 'amber.profile.password','uses' => 'profileController@password']);
});