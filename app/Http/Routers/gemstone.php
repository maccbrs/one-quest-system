<?php 

	$this->group(['prefix' => 'dashboard'],function(){
		$this->get('/',['as' => 'gemstone.dashboard.index','uses' => 'dashboardController@index']); 
	});
	
	$this->group(['prefix' => 'system'],function(){
		$this->get('/',['as' => 'gemstone.system.index','uses' => 'systemparamController@index']); 
		$this->post('/create',['as' => 'gemstone.system.create','uses' => 'systemparamController@store']); 
		$this->post('/edit',['as' => 'gemstone.system.edit','uses' => 'systemparamController@update']); 
	});

	$this->group(['prefix' => 'cctv'],function(){

		$this->get('/all',['as' => 'gemstone.cctv.all','uses' => 'cctvController@all']); 
		$this->post('/create',['as' => 'gemstone.cctv.create','uses' => 'cctvController@create']);
		$this->get('/check',['as' => 'gemstone.cctv.check','uses' => 'cctvController@check']);
		$this->post('/check-status/{id}',['as' => 'gemstone.cctv.check-status','uses' => 'cctvController@check_status']);
		$this->post('/update-status',['as' => 'gemstone.cctv.update-status','uses' => 'cctvController@update_status']); 
		$this->post('/submit-checklist',['as' => 'gemstone.cctv.submit-checklist','uses' => 'cctvController@submit_checklist']);

		$this->get('/results/all',['as' => 'gemstone.cctv.results','uses' => 'cctvController@results']);
		$this->get('/result/{checklist}/{status}',['as' => 'gemstone.cctv.result','uses' => 'cctvController@result']); 
	});	  

	$this->group(['prefix' => 'bio'],function(){

		$this->get('/all',['as' => 'gemstone.bio.all','uses' => 'bioController@all']); 
		$this->post('/create',['as' => 'gemstone.bio.create','uses' => 'bioController@create']);
		$this->get('/check',['as' => 'gemstone.bio.check','uses' => 'bioController@check']);
		$this->post('/check-status/{id}',['as' => 'gemstone.bio.check-status','uses' => 'bioController@check_status']);
		$this->post('/update-status',['as' => 'gemstone.bio.update-status','uses' => 'bioController@update_status']); 
		$this->post('/submit-checklist',['as' => 'gemstone.bio.submit-checklist','uses' => 'bioController@submit_checklist']);

		$this->get('/results/all',['as' => 'gemstone.bio.results','uses' => 'bioController@results']);
		$this->get('/result/{checklist}/{status}',['as' => 'gemstone.bio.result','uses' => 'bioController@result']); 
	});	

	$this->group(['prefix' => 'tazkie'],function(){

	});

	$this->group(['prefix' => 'users'],function(){
		$this->get('/',['as' => 'gemstone.users.index','uses' => 'usersController@index']);
		$this->get('/pending-users',['as' => 'gemstone.users.pending_users','uses' => 'usersController@pending_users']);
		$this->get('/agents',['as' => 'gemstone.users.agents','uses' => 'usersController@agents']);
		$this->get('/team_leaders',['as' => 'gemstone.users.tl','uses' => 'usersController@teamleads']);
		$this->get('/program_supervisors',['as' => 'gemstone.users.ps','uses' => 'usersController@program_supervisors']);

		$this->post('/gem-register',['as' => 'gemstone.users.register','uses' => 'usersController@register']);
 		$this->post('/gem-agent-register',['as' => 'gemstone.users.agent_register','uses' => 'usersController@agent_register']);
		$this->post('/gem-agent-edit',['as' => 'gemstone.users.agent_edit','uses' => 'usersController@agent_edit']);

		$this->post('/gem-register-tl',['as' => 'gemstone.users.register-tl','uses' => 'usersController@tl_register']);
		$this->post('/gem-update-tl/{id}',['as' => 'gemstone.users.update-tl','uses' => 'usersController@tl_update']);

		$this->post('/gem-register-ps',['as' => 'gemstone.users.register-ps','uses' => 'usersController@ps_register']);
		$this->post('/gem-update-ps/{id}',['as' => 'gemstone.users.update-ps','uses' => 'usersController@ps_update']);

		$this->post('/settings/{id}',['as' => 'gemstone.users.settings','uses' => 'usersController@settings']);
		$this->get('/connector/{id}',['as' => 'gemstone.users.connector','uses' => 'usersController@connector']);
		$this->post('/connect',['as' => 'gemstone.users.connect','uses' => 'usersController@connect']);
		$this->post('/search',['as' => 'gemstone.users.search','uses' => 'usersController@search']);
	}); 

	$this->group(['prefix' => 'dids'],function(){
		$this->get('/index',['as' => 'gemstone.did.index','uses' => 'didController@index']);
		$this->post('/create',['as' => 'gemstone.did.create','uses' => 'didController@create']);
		$this->get('/testcalls',['as' => 'gemstone.did.testcalls','uses' => 'didController@testcalls']);
		$this->post('/create-tc',['as' => 'gemstone.did.createtc','uses' => 'didController@create_tescalls']); 
		$this->get('/testcall/{id}',['as' => 'gemstone.did.testcall','uses' => 'didController@testcall']);
		$this->post('/mbvalid/{id}',['as' => 'gemstone.did.mbvalid','uses' => 'didController@mbvalid']);
		$this->post('/mbcheckupdate',['as' => 'gemstone.did.mbcheckupdate','uses' => 'didController@mbcheckupdate']);
		$this->post('/notes/{id}',['as' => 'gemstone.did.notes','uses' => 'didController@notes']);
		$this->get('/passed/{id}',['as' => 'gemstone.did.passed','uses' => 'didController@passed']);
		$this->get('/failed/{id}',['as' => 'gemstone.did.failed','uses' => 'didController@failed']);
		$this->get('/dids/{id}',['as' => 'gemstone.did.dids','uses' => 'didController@dids']);
		$this->post('/destroy/{id}',['as' => 'gemstone.did.destroy','uses' => 'didController@destroy']);
	}); 			


	$this->group(['prefix' => 'inventory'],function(){

		$this->get('/index',['as' => 'gemstone.inventory.index','uses' => 'inventoryController@index']);
		$this->get('/custom/pc/index',['as' => 'gemstone.inventory.costume-pc-index','uses' => 'inventoryController@costume_pc_index']);

	});


	$this->group(['prefix' => 'asset'],function(){
		$this->get('/index',['as' => 'gemstone.asset.index','uses' => 'assetController@index']);
		$this->get('/group/{field}/{value}',['as' => 'gemstone.asset.group','uses' => 'assetController@group']);
		$this->get('/add',['as' => 'gemstone.asset.add','uses' => 'assetController@add']);
		$this->get('/view/{id}',['as' => 'gemstone.asset.view','uses' => 'assetController@view']);
		$this->post('/create',['as' => 'gemstone.asset.create','uses' => 'assetController@create']);
		$this->post('/create-options/{type}',['as' => 'gemstone.asset.create-options','uses' => 'assetController@create_options']);
	});

	$this->group(['prefix' => 'license'],function(){
		$this->get('/index',['as' => 'gemstone.license.index','uses' => 'licenseController@index']);
		$this->get('/view/{id}',['as' => 'gemstone.license.view','uses' => 'licenseController@view']);
		$this->get('/group/{field}/{value}',['as' => 'gemstone.license.group','uses' => 'licenseController@group']);
		$this->get('/add',['as' => 'gemstone.license.add','uses' => 'licenseController@add']);
		$this->post('/create',['as' => 'gemstone.license.create','uses' => 'licenseController@create']);
		$this->post('/create-options/{type}',['as' => 'gemstone.license.create-options','uses' => 'licenseController@create_options']); 
		$this->post('/assign/{id}',['as' => 'gemstone.license.assign','uses' => 'licenseController@assign']);
		$this->post('/reassign/{id}',['as' => 'gemstone.license.reassign','uses' => 'licenseController@reassign']);
	
	});	

  	$this->group(['prefix' => 'approver'],function(){

		$this->get('/',['as' => 'gemstone.approver.index','uses' => 'approverController@index']);
		$this->post('/',['as' => 'gemstone.approver.create','uses' => 'approverController@create']);

	}); 

	$this->group(['prefix' => 'announcements'],function(){
		$this->get('/',['as' => 'gemstone.announcement.index','uses' => 'announcementsController@index']);
		$this->post('/',['as' => 'gemstone.announcements.create','uses' => 'announcementsController@create']);
		$this->get('/view/{id}',['as' => 'gemstone.announcements.get','uses' => 'announcementsController@get']);
		$this->get('/seen/{id}',['as' => 'gemstone.announcements.seen','uses' => 'announcementsController@seen']);

	}); 


	$this->group(['prefix' => 'department'],function(){

		$this->get('/index',['as' => 'gemstone.department.index','uses' => 'departmentController@index']);
		$this->post('/store',['as' => 'gemstone.department.store','uses' => 'departmentController@store']);
		$this->get('/show/{id}',['as' => 'gemstone.department.show','uses' => 'departmentController@show']);
		$this->get('/users/{id}',['as' => 'gemstone.department.users','uses' => 'departmentController@users']);
		$this->post('/user_store',['as' => 'gemstone.department.user_store','uses' => 'departmentController@user_store']);
		$this->get('/admin/{id}',['as' => 'gemstone.department.admin','uses' => 'departmentController@admin']);
		$this->post('/admin_store',['as' => 'gemstone.department.admin_store','uses' => 'departmentController@admin_store']);
		$this->get('/tickets/{id}',['as' => 'gemstone.department.tickets','uses' => 'departmentController@tickets']);
		$this->get('/ticket/{id}',['as' => 'gemstone.department.ticket','uses' => 'departmentController@ticket']);
	 	$this->post('/status',['as' => 'gemstone.department.status','uses' => 'departmentController@status']);
	 	$this->post('/assign',['as' => 'gemstone.department.assign','uses' => 'departmentController@assign']);

	});   

	$this->group(['prefix' => 'score'],function(){
		$this->get('/index/{id}',['as' => 'gemstone.score.index','uses' => 'scoreController@index']);
	});

