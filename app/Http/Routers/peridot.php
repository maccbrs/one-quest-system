<?php

$this->get('/',['as' => 'ruby.dashboard.index','uses' => 'dashboardController@index']);
//$this->get('/randomizer',['as' => 'ruby.randomizer.index','uses' => 'randomizerController@index']);
//$this->get('/metrics',['as' => 'ruby.metrics.index','uses' => 'metricsController@index']);

$this->group(['prefix' => 'campaigns'],function(){
		$this->get('/',['as' => 'ruby.campaigns.index','uses' => 'campaignsController@index']);
		$this->get('/inbound/{id}',['as' => 'ruby.campaigns.inbound','uses' => 'campaignsController@inbound']);
		$this->get('/outbound/{id}',['as' => 'ruby.campaigns.outbound','uses' => 'campaignsController@outbound']);
});
/*
$this->group(['prefix' => 'agents'],function(){
		$this->get('/',['as' => 'ruby.agents.index','uses' => 'agentsController@index']);
		$this->get('/inbound/{id}',['as' => 'ruby.agents.inbound','uses' => 'agentsController@inbound']);
		$this->get('/outbound/{id}',['as' => 'ruby.agents.outbound','uses' => 'agentsController@outbound']);	
		$this->get('/results',['as' => 'ruby.agents.results','uses' => 'agentsController@results']);
		$this->get('/results/{bound}/{agent}',['as' => 'ruby.agents.result','uses' => 'agentsController@result']);
		$this->get('/results/{bound}/{agent}/{weekno}',['as' => 'ruby.agents.week','uses' => 'agentsController@week']);	
});
$this->group(['prefix' => 'results'],function(){
	$this->get('/',['as' => 'ruby.results.index','uses' => 'resultsController@index']);
	$this->get('/{id}',['as' => 'ruby.results.read','uses' => 'resultsController@read']);
	$this->get('/{id}/{weekno}',['as' => 'ruby.results.lists','uses' => 'resultsController@lists']);
	
});


$this->group(['prefix' => 'account'],function(){
	$this->get('/{id}',['as' => 'ruby.account.get','uses' => 'accountController@get']); 
});

$this->group(['prefix' => 'settings'],function(){
	
	$this->get('/',['as' => 'ruby.settings.index','uses' => 'settingsController@index']);
	$this->get('/account-fields',['as' => 'ruby.settings.account-fields','uses' => 'settingsController@accountFields']);
	$this->get('/account-edit/{id}',['as' => 'ruby.settings.account-edit','uses' => 'settingsController@accountEdit']);
	$this->get('/account',['as' => 'ruby.settings.account','uses' => 'settingsController@accounts']);
	$this->post('/account',['as' => 'ruby.settings.account_create','uses' => 'settingsController@account_create']);
	$this->get('qa-settings/{id}',['as' => 'ruby.settings.accounts-qa-settings','uses' => 'settingsController@accountsQaSettings']);
	$this->get('set-edit/{id}',['as' => 'ruby.settings.set-edit','uses' => 'settingsController@accountsQaSettingsEdit']);
	$this->get('/auditors',['as' => 'ruby.settings.auditors','uses' => 'settingsController@auditors']);
	$this->get('/auditor/{id}',['as' => 'ruby.settings.auditor','uses' => 'settingsController@auditor']);

	$this->group(['prefix' => 'accounts'],function(){
		$this->get('/',['as' => 'ruby.settings.accounts_index','uses' => 'settingsController@accounts_index']);
		$this->get('/create',['as' => 'ruby.settings.accounts_create','uses' => 'settingsController@accounts_create']);
		$this->post('/post',['as' => 'ruby.settings.accounts_post','uses' => 'settingsController@accounts_post']);
		$this->get('/read/{id}',['as' => 'ruby.settings.accounts_read','uses' => 'settingsController@accounts_read']);
	});

});

$this->group(['prefix' => 'set'],function(){
	$this->get('/{id}',['as' => 'ruby.set.index','uses' => 'setController@index']);
});

$this->group(['prefix' => 'audits'],function(){
	$this->get('/{id}',['as' => 'ruby.audits.index','uses' => 'auditsController@index']);
});

$this->group(['prefix' => 'audit'],function(){
	$this->get('/{id}',['as' => 'ruby.audit.index','uses' => 'auditController@get']);
});

$this->group(['prefix' => 'templates'],function(){
	$this->get('/',['as' => 'ruby.templates.index','uses' => 'templatesController@two']);
});

$this->group(['prefix' => 'search'],function(){
	$this->get('/',['as' => 'ruby.audit.search','uses' => 'searchController@index']);
	$this->post('/',['as' => 'ruby.audit.search','uses' => 'searchController@search']);
	$this->get('/result/{key}/{daterange}/{bound}',['as' => 'ruby.audit.result','uses' => 'searchController@result']);
});

$this->group(['prefix' => 'compliance'],function(){
	$this->get('/',['as' => 'ruby.compliance.index','uses' => 'complianceController@index']);
	$this->get('/user/{id}',['as' => 'ruby.compliance.user','uses' => 'complianceController@user']);
	$this->get('/add/{id}',['as' => 'ruby.compliance.add','uses' => 'complianceController@add']);
	$this->post('/create/{id}',['as' => 'ruby.compliance.create','uses' => 'complianceController@create']);
	$this->get('/detail/{id}',['as' => 'ruby.compliance.detail','uses' => 'complianceController@detail']);
}); 

$this->group(['prefix' => 'announcements'],function(){
	$this->get('/',['as' => 'ruby.announcements.index','uses' => 'announcementsController@index']);
	$this->post('/',['as' => 'ruby.announcements.create','uses' => 'announcementsController@create']);
	$this->get('/view/{id}',['as' => 'ruby.announcements.get','uses' => 'announcementsController@get']);
	$this->get('/seen/{id}',['as' => 'ruby.announcements.seen','uses' => 'announcementsController@seen']);
}); 

*/