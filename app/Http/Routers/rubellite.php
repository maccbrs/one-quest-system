<?php
 
	$this->group(['prefix' => '/'],function(){
		$this->get('/',['as' => 'rubellite.home','uses' => 'CrudController@index']);
		
		$this->get('/ajax/{unique_id}',['as' => 'rubellite.ajax.home','uses' => 'AjaxController@displayloghistory']);
		
		
	
		});


	
	$this->group(['prefix' => 'ajax'],function(){
	
		//$this->get('/{unique_id}',['as' => 'bloodstone.ajax.home','uses' => 'AjaxController@displayloghistory']);
	});
	
 	$this->group(['prefix' => 'masterfile'],function(){
		$this->get('/',['as' => 'rubellite.masterfile.home','uses' => 'MasterfileController@index']);
		//$this->get('/{masterfile}',['as' => 'rubellite.masterfile.home','uses' => 'MasterfileController@masterfile']);
		$this->get('/MstElements',['as' => 'rubellite.masterfile.elements','uses' => 'MasterfileController@StaticViewerElement']);
		$this->get('/MstParameters',['as' => 'rubellite.masterfile.parameters','uses' => 'MasterfileController@StaticViewerParameter']);
		//$this->get('/MstKeyfactors',['as' => 'rubellite.masterfile.keyfactor','uses' => 'MasterfileController@masterfile']);
		$this->get('/{masterfile}',['as' => 'rubellite.masterfile.dynamic','uses' => 'MasterfileController@masterfile']);
		$this->post('/{masterfile}/create',['as' => 'rubellite.masterfile.create','uses' => 'MasterfileController@store']); 
	});	

 ?> 