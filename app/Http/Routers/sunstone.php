<?php

$this->group(['prefix' => 'applicant'],function(){
    $this->get('/application-form',['as' => 'sunstone.applicant.application-form','uses' => 'applicantController@application_form']);
    $this->post('/application-save',['as' => 'sunstone.applicant.application-save','uses' => 'applicantController@application_save']);

	$this->get('/index',['as' => 'sunstone.applicant.index','uses' => 'applicantController@index']);
	$this->get('/list/{status}',['as' => 'sunstone.applicant.list','uses' => 'applicantController@applicant_list']);

	$this->post('/add_fields',['as' => 'sunstone.applicant.add_info','uses' => 'applicantController@add']);
	$this->post('/update-applicant',['as' => 'sunstone.applicant.update_applicant','uses' => 'applicantController@update_applicant']);
    $this->post('/search',['as' => 'sunstone.applicant.search','uses' => 'applicantController@search']);

	$this->get('/list/{status}',['as' => 'sunstone.applicant.list','uses' => 'applicantController@applicant_list']);
	$this->get('/edit',['as' => 'sunstone.applicant.edit','uses' => 'applicantController@edit']);
	$this->get('/sort',['as' => 'sunstone.applicant.sort','uses' => 'applicantController@sort']);
    $this->get('/download_resume/{applicant_id}',['as' => 'sunstone.applicant.download_resume','uses' => 'applicantController@download_resume']);

	$this->post('/add_fields',['as' => 'sunstone.applicant.add_info','uses' => 'applicantController@add']);
	$this->post('/update-applicant',['as' => 'sunstone.applicant.update_applicant','uses' => 'applicantController@update_applicant']);
    $this->post('/attach-resume',['as' => 'sunstone.applicant.attach_resume','uses' => 'applicantController@attach_resume']);
	$this->post('/update-field',['as' => 'sunstone.applicant.update_field','uses' => 'applicantController@update_field']);
	$this->post('/sort-field',['as' => 'sunstone.applicant.sort_field','uses' => 'applicantController@sort_field']);

    $this->post('/interview-dispo',['as' => 'sunstone.applicant.interview-dispo','uses' => 'applicantController@interview_dispo']);
    $this->post('/handle-job-offer',['as' => 'sunstone.applicant.handle-job-offer','uses' => 'applicantController@handle_job_offer']);
    $this->post('/send-to-training',['as' => 'sunstone.applicant.send-to-training','uses' => 'applicantController@send_to_training']);
    $this->post('/back-to-pool',['as' => 'sunstone.applicant.back-to-pool','uses' => 'applicantController@back_to_pool']);

    $this->get('/pool',['as' => 'sunstone.applicant.pool','uses' => 'applicantController@pool']);
    $this->get('/for-training',['as' => 'sunstone.applicant.for-training','uses' => 'applicantController@for_training']);
    $this->get('/print-information/{applicant_id}',['as' => 'sunstone.applicant.print-information','uses' => 'applicantController@print_information']);

    $this->get('/active',['as' => 'sunstone.applicant.active','uses' => 'applicantController@active']);

	$this->post('/delete',['as' => 'sunstone.applicant.delete','uses' => 'applicantController@delete']);

}); 

$this->group(['prefix' => 'campaign'],function(){
	$this->get('/index',['as' => 'sunstone.campaign.index','uses' => 'campaignController@index']);
	$this->post('/add',['as' => 'sunstone.campaign.add','uses' => 'campaignController@add']);

    $this->post('/edit',['as' => 'sunstone.campaign.edit','uses' => 'campaignController@edit']);
}); 

$this->group(['prefix' => 'position'],function(){
	$this->get('/index',['as' => 'sunstone.position.index','uses' => 'positionController@index']);
	$this->post('/add',['as' => 'sunstone.position.add','uses' => 'positionController@add']);
    $this->post('/edit',['as' => 'sunstone.position.edit','uses' => 'positionController@edit']);
});

$this->group(['prefix' => 'assessment'],function(){
    $this->get('/index',['as' => 'sunstone.assessment.index','uses' => 'assessmentController@index']);
    $this->get('/{id}/manage_question',['as' => 'sunstone.assessment.manage_question','uses' => 'assessmentController@manage_question']);
    $this->get('/results',['as' => 'sunstone.assessment.results','uses' => 'assessmentController@results']);
    $this->get('/delete_question/{qid}/{aid}',['as' => 'sunstone.assessment.delete_question','uses' => 'assessmentController@delete_question']);

    $this->post('/edit',['as' => 'sunstone.assessment.edit','uses' => 'assessmentController@edit']);
    $this->post('/add',['as' => 'sunstone.assessment.add','uses' => 'assessmentController@add']);
    $this->post('/add_question',['as' => 'sunstone.assessment.add_question','uses' => 'assessmentController@add_question']);
    $this->post('/edit_question',['as' => 'sunstone.assessment.edit_question','uses' => 'assessmentController@edit_question']);
    $this->post('/search_applicant',['as' => 'sunstone.assessment.search_applicant','uses' => 'assessmentController@search_applicant']);

    $this->get('/exam-search',['as' => 'sunstone.assessment.exam-search', 'uses' => 'examController@search']);
    $this->get('/exam-search',['as' => 'sunstone.assessment.exam-search', 'uses' => 'examController@search']);

    $this->post('/exam-search',['as' => 'sunstone.assessment.exam-search', 'uses' => 'examController@search']);
    $this->post('/exam-start',['as' => 'sunstone.assessment.exam-start','uses' => 'examController@start']);
    $this->post('/exam-next',['as' => 'sunstone.assessment.exam-next','uses' => 'examController@next']);

});