<?php

$this->get('/',['as' => 'pearl.dashboard.index','uses' => 'dashboardController@index']);

$this->group(['prefix' => 'campaign'],function(){
	$this->get('/{id}',['as' => 'pearl.campaign.get','uses' => 'campaignController@get']);
});
 
$this->group(['prefix' => 'reports'],function(){
	$this->get('{id}',['as' => 'pearl.reports.get','uses' => 'reportsController@get']);
	$this->post('{id}',['as' => 'pearl.reports.get','uses' => 'reportsController@get']);
	$this->post('/filter/{id}',['as' => 'pearl.reports.filter','uses' => 'reportsController@filter']);
	$this->get('/excel/{id}',['as' => 'pearl.reports.excel','uses' => 'reportsController@excel']);
});