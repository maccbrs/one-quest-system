<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Http\Models\sunstone\Applicant::class, function (Faker\Generator $faker) {
    return [
        'source_date' => $faker->date('Y-m-d'),
        'last_name' => $faker->lastName,
        'middle_name' => $faker->lastName,
        'first_name' => $faker->firstName,
        'position_id' => $faker->numberBetween(0, 5),
        'status' => $faker->numberBetween(0, 5)
    ];
});

