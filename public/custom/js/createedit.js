;(function($){

//--------------------------------- update ------------------------------

    $(document).on('click','.mbtbleditbtn',function(){
        let row = [];
        $(this).parent().closest('tr').find('.data').map(function(){
            row.push($(this).text());
        });
        socket.emit('cts_getfielddata',row);
    });

	socket.on('stc_getfielddata',function(res){

        var append = function(){
            return new Promise(function(resolve,reject){
                console.log(res);
                $('.mbtbleditbtn-m').modal();             
                let a = $('#getfielddataedit').html();
                let b = ejs.render(a, {key:res.key,label:res.label,res:res});                
                $('#getfielddataedit-container').html(b);
                resolve(res.status); 
            });
        };
        
        var setdata = function(r){
            return new Promise(function(resolve,reject){
                if(r){
                  obj = JSON.parse(res.data[0].formvalue);
                  $("#selecttypeedit").find('option[value="'+obj.fieldtype+'"]').attr('selected',true);
                  if(obj.fieldtype != 'text' && obj.fieldtype != 'textarea'){
                     $('#optionstableedit').css('display','block');
                     $('td[data-key="optionvalue"]').text(obj.optionvalue);
                     $('td[data-key="optionlabel"]').text(obj.optionlabel);
                  }
                }
                resolve();
            });
        }

        append().then(function(r){
            setdata(r).then(function(){
                $('.previewfieldedit').trigger("click");
            });
        });
               
    });

    $(document).on('change',"#selecttypeedit",function () {
        let val = $(this).val();
        if(val == 'text' || val == 'textarea'){
            console.log('hello');
            $('#optionstableedit').css('display','none');
        }else{
            $('#optionstableedit').css('display','block');
            console.log('hi');
        }
    }); 

    $(document).on('click','.previewfieldedit',function(){

        let res = processpreview('edit');
        if(!res.length){
            $('.cnff-submit-edit').css('display','inline-block');
        }else{
            $('.cnff-submit-edit').css('display','none');
        }
    });

    $(document).on('click','.cnff-submit-edit',function(){
        let res = processpreview('edit');
        err = res.err;
        if(!err){
            socket.emit('cts_editformfield',{data:res.data});
        }
    });

    socket.on('stc_editformfield_res',function(res){
    	$('.mbsuccess-edit').append('<p>Successfully '+res.status+'!</p>');
    });

    let processpreview = function(type){

        	$('.errors-'+type).html('');
            let fields = [];

            $('.fieldinput-'+type).map(function(i,v){
                let arr = {};
                if($(this).val()){
                    arr.key = $(this).data('key');
                    arr.val = $(this).val();
                }else{
                    arr.key = $(this).data('key');
                    arr.val = $(this).text();
                }
                fields.push(arr);
            });

            let errs = [];
            let fieldtype = 'text';
            let optionvalue = [];
            let optionlabel = [];
            let fieldkey = '';

            fields.forEach(function(v){
                if(v.key == 'key'){
                    if(v.val == ''){
                        errs.push('key is empty');
                        $('.errors-'+type).append('<p>**Key is empty</p>');

                    }
                    fieldkey = v.val;
                }
                if(v.key == 'label'){
                    if(v.val == ''){
                        errs.push('label is empty');
                        $('.errors-'+type).append('<p>**label is empty</p>');
                    }else{
                        $('.preview-label-'+type).text(v.val);
                    }
                }

                if(v.key == 'fieldtype'){
                    fieldtype = v.val;
                }
                if(v.key == 'optionvalue'){
                    optionvalue = v.val.trim();
                }
                if(v.key == 'optionlabel'){
                    optionlabel = v.val.trim();
                }
            });

            if(fieldtype != 'text' && fieldtype != 'textarea'){
                if(optionvalue == '' || optionlabel == ''){
                    $('.errors-'+type).append('<p>**option value and label shouldn\'t be empty</p>');
                    errs.push('empty option value or label');
                }else{
                    let ovArr = optionvalue.split(',');
                    let olArr = optionlabel.split(',');
                    if(ovArr.length != olArr.length){
                        $('.errors-'+type).append('<p>**un equal no of option value and label</p>');
                        errs.push('un equal value of option value and label');                    
                    }else{
                        $('.fieldarea-'+type).html('');
                        if(fieldtype == 'radio'){
                            let id = $('#radiofield').html();
                            let res = ejs.render(id, { ov: ovArr,ol:olArr,name:fieldkey });
                            $('.fieldarea-'+type).html(res);
                        }
                        if(fieldtype == 'dropdown'){
                            let id = $('#dropdown').html();
                            let res = ejs.render(id, { ov: ovArr,ol:olArr,name:fieldkey});
                            $('.fieldarea-'+type).html(res);
                        }
                    }
                }
            }else{
                if(fieldtype == 'text'){
                    let id = $('#textfield').html();
                    let res = ejs.render(id, {name:fieldkey});
                    $('.fieldarea-'+type).html(res);
                }
                if(fieldtype == 'textarea'){
                    let id = $('#textareafield').html();
                    let res = ejs.render(id, {name:fieldkey});
                    $('.fieldarea-'+type).html(res);
                }                          
            }

            return {err:errs.length,data:fields};
    };


//-------------------------- create ---------------------------------




    let getdata = (s) => {
        let data = [];
        $(s).map((x,y)=>{
            let item = [];
            $(y).find('td.data').map((a,b) => { 
                item.push($(b).text());
            });
            data.push({key:item[0],label:item[1]});
        }); 

        if(s == '.enabled'){
            socket.emit('editlabelenabled',data);
        }
        if(s == '.disabled'){
            socket.emit('editlabeldisabled',data);
        }
        
    }

    $(document).on('blur',".enabled .editable",function(){
      getdata('.enabled');
    });
    $(document).on('blur',".disabled .editable",function(){
      getdata('.disabled');
    }); 


    $(document).on('change',"#selecttype",function () {
        let val = $(this).val();
        if(val == 'text' || val == 'textarea'){
            $('#optionstable').css('display','none');
        }else{
            $('#optionstable').css('display','block');
        }
    });    

    let appendTd = function(d){

        let id = $('#tdtemplate').html();
        let res = ejs.render(id, {key:d.key,label:d.label});
        $('.mbtblbody').append(res);   

    }

    let refreshform = function(){
        let id = $('#refreshformfield').html();
       let res = ejs.render(id);
        $('.modalformfield').html(res);     
    }

    $(document).on('click','.previewfield',function(){

        let res = mbvalidate();
        if(!res.length){
            $('.cnff-submit').css('display','inline-block');
        }else{
            $('.cnff-submit').css('display','none');
        }
    });


    let mbvalidate = function(){

        $('.errors').html('');
            let fields = [];
            $('.fieldinput').map(function(i,v){
                let arr = {};
                if($(this).val()){
                    arr.key = $(this).data('key');
                    arr.val = $(this).val();
                }else{
                    arr.key = $(this).data('key');
                    arr.val = $(this).text();
                }
                fields.push(arr);
            });
            let errs = [];
            let fieldtype = 'text';
            let optionvalue = [];
            let optionlabel = [];
            let fieldkey = '';

            fields.forEach(function(v){
                if(v.key == 'key'){
                    if(v.val == ''){
                        errs.push('key is empty');
                        $('.errors').append('<p>**Key is empty</p>');

                    }
                    fieldkey = v.val;
                }
                if(v.key == 'label'){
                    if(v.val == ''){
                        errs.push('label is empty');
                        $('.errors').append('<p>**label is empty</p>');
                    }else{
                        $('.preview-label').text(v.val);
                    }
                }

                if(v.key == 'fieldtype'){
                    fieldtype = v.val;
                }
                if(v.key == 'optionvalue'){
                    optionvalue = v.val.trim();
                }
                if(v.key == 'optionlabel'){
                    optionlabel = v.val.trim();
                }
            });

            if(fieldtype != 'text' && fieldtype != 'textarea'){
                if(optionvalue == '' || optionlabel == ''){
                    $('.errors').append('<p>**option value and label shouldn\'t be empty</p>');
                    errs.push('empty option value or label');
                }else{
                    let ovArr = optionvalue.split(',');
                    let olArr = optionlabel.split(',');
                    if(ovArr.length != olArr.length){
                        $('.errors').append('<p>**un equal no of option value and label</p>');
                        errs.push('un equal value of option value and label');                    
                    }else{
                        $('.fieldarea').html('');
                        if(fieldtype == 'radio'){
                            let id = $('#radiofield').html();
                            let res = ejs.render(id, { ov: ovArr,ol:olArr,name:fieldkey });
                            $('.fieldarea').html(res);
                        }
                        if(fieldtype == 'dropdown'){
                            let id = $('#dropdown').html();
                            let res = ejs.render(id, { ov: ovArr,ol:olArr,name:fieldkey});
                            $('.fieldarea').html(res);
                        }
                    }
                }
            }else{
                if(fieldtype == 'text'){
                    let id = $('#textfield').html();
                    let res = ejs.render(id, {name:fieldkey});
                    $('.fieldarea').html(res);
                }
                if(fieldtype == 'textarea'){
                    let id = $('#textareafield').html();
                    let res = ejs.render(id, {name:fieldkey});
                    $('.fieldarea').html(res);
                }                          
            }

            return {err:errs.length,data:fields};
    };

    $(document).on('click','.cnff-submit',function(){
        let res = mbvalidate();
        err = res.err;
        if(!err){
            console.log('im here');
            socket.emit('cts_createformfield',{data:res.data});
        }
    });

    socket.on('stc_createformfield',function(d){
        $('.mbsuccess').html('');
        if(d.status){
            refreshform();
            $('.mbsuccess').append('<p>Successfully saved!</p>');
            let row = d.data;
            appendTd(row);
        }else{
             $('.errors').append('<p>** unable to save, an error ocurred!!</p>');
        }
    });


}(jQuery));   




