var getRow = function(id){
	return new Promise(function(res,rej){
		socket.emit('cts_appDataIndex_getRow',{id:id});
		res();
	});
}

socket.on('stc_appDataIndex_getRow',function(data){

    let id = $('#tablemodal').html();
    let res = ejs.render(id, { lead:data.leadid,row:data.row});	
    $('#tablemodal-container').html(res);
	$('.mbtbltrmodal').modal();
});

$(document).on("click",'.mbtbltr',function(){
	getRow($(this).data('leadid')).then(function(){

	});
});


$(document).on('click','.mbsubmit',function(){
	var arr = [];
	$('.mb-data').find('tr').map(function(i,v){
		var key = $(this).find('th').data('key');
		var val = $(this).find('td').text();
		arr.push({key:key,value:val});
	});
	socket.emit('cts_appDataIndex_updateRow',{lead:$('.mb-data').data('lead'),data:arr});
}); 

socket.on('stc_appDataIndex_updateRow',function(data){
        noty({
            theme: 'app-noty',
            text: 'successfully saved',
            type: 'success',
            timeout: 3000,
            layout: 'topRight',
            closeWith: ['button', 'click'],
            animation: {
                open: 'animated fadeInDown',
                close: 'animated fadeOutUp'
            }
        }); 
});