$(document).ready(function() {
    $('#example').DataTable({
        lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
        pageLength: 25, 
		   
        scrollX: true,
        autoWidth: false,
        responsive: true,
        scrollY: '600px',
        scrollCollapse: true,
        dom: 'Blfrtip',
        stateSave: true,
        buttons: [
            {
                extend: 'copyHtml5',
                title: "Agents' Approver List" ,
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            {
                extend: 'excelHtml5',
                title: "Agents' Approver List" ,
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            'colvis'
            ]
			
            });
} );

   $(document).keydown(function(e){
      if(e.keyCode==17){$(".FormCreateNew").modal() ;$('.class_ModelName').val('TpCastForm');};
 });