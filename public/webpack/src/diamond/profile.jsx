import React from 'react';
import ReactDOM from 'react-dom';
var ss = require('socket.io-stream');

socketDiamond.emit('diamond.profile.index-init-to',{id:defaultData.userId});

class UploadProfile extends React.Component {

	  constructor(props) {
	    super(props);
	    this.handleChange = this.handleChange.bind(this);
	  }

	  handleChange(e) {

	    e.preventDefault();
	    var file = e.target.files[0];
	    var stream = ss.createStream();
	    ss(socketDiamond).emit('diamond.profile.index-UploadImage-to', stream, {name: file.name,id:defaultData.userId});
	    ss.createBlobReadStream(file).pipe(stream);

	  }

	  render() {
	    return (
            <form ref="uploadForm" className="uploader" encType="multipart/form-data" onChange={this.handleChange}>
            		<input className="btn btn-primary btn-sm" type="file" name="files[]" multiple="multiple"/> 
            </form>
	    );
	  }

}

ReactDOM.render(<UploadProfile/>, document.getElementById('upload-profile')); 



function ProfilePic(props) {
  return <img src={props.url+"/80x80"} alt=""/>;
}

socketDiamond.on('diamond.profile.index-profilepic-from',function(data){
	var url = defaultData.imagesUrl+data.profile;
	ReactDOM.render(<ProfilePic url={url}/>, document.getElementById('profilepic'));
});

 


