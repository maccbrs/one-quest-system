var file = [];

file['audit-get'] = {
    i: './public/rubyjs/audit/get.jsx',
    o: '../rubyjs/audit',
    f: 'get.js'
};

file['audits-index'] = {
    i: './public/rubyjs/audits/index.jsx',
    o: '../rubyjs/audits',
    f: 'index.js'
};

file['settings-accounts'] = {
    i: './public/rubyjs/settings/account.jsx',
    o: '../rubyjs/settings',
    f: 'account.js'
};

file['settings-accounts-auditor'] = {
    i: './public/rubyjs/settings/auditors.jsx',
    o: '../rubyjs/settings',
    f: 'auditors.js'
};

file['settings-accounts-dash'] = {
    i: './public/rubyjs/dash/index.jsx',
    o: '../rubyjs/dash',
    f: 'index.js'
};

file['settings-accounts-set'] = {
    i: './public/rubyjs/set/index.jsx',
    o: '../rubyjs/set',
    f: 'index.js'
};


var route = 'settings-accounts-set';

module.exports = {
     entry: file[route].i, 
     output: {
         path: file[route].o,
         filename: file[route].f
     },
     module: {
         loaders: [{
             test: /\.js$/,
             exclude: ['/node_modules/'],
             loader: 'babel-loader',
            query: {
               presets: ['es2015', 'react']
            }             
         },{
	        test : /\.jsx?/,
	        loader: 'babel',
            exclude: ['/node_modules/'],
            query: {
               presets: ['es2015', 'react']
            }	        
	      },
          { test: /\.css$/, loader: "style-loader!css-loader?importLoaders=1" },
          {
            test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
            loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
}
         ]
     }     
 }; 