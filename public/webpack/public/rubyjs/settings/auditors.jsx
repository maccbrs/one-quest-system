import React from 'react';
import ReactDOM from 'react-dom';

$(document).on('click','.addNewAuditor',function(){
	socket.emit('ruby.settings.auditors-getAllUsers-to');
});

socket.on('ruby.settings.auditors-getAllUsers-from',function(result){
	$('.addNewAuditorModal').modal();
	let a = $('#template-y').html();
	let b = ejs.render(a, {rows:result}); 
	$('#template-x').html(b);	
});

$(document).on('click','.formdata-submit',function(){
	var id = $('input[name="user"]:checked').val();
	if(id != undefined){
		socket.emit('ruby.settings.auditors-setNewAuditor-to',{id:id});
	}
});

socket.emit('ruby.settings.auditors-lists-to');


socket.on('ruby.settings.auditors-lists-from',function(data){

	var Auditors = function(props){

		const listsItems = props.data.map((list,i) =>
			 <tr key={i} className="tbllink" data-link={baseUrl+"/ruby/settings/auditor/"+list.id}>
			  <td key={list.id} ><a href='#' className="editable editable-click">{list.name}</a></td>
			 </tr>
		);	

		return  (
		      <table className="table table-bordered customized-table">
		          <thead>
		              <tr>
		                  <th>Name</th>
		              </tr>
		          </thead>
		          <tbody>
		          		{listsItems}
		          </tbody>	 
		      </table>	
		 );		
	}

	ReactDOM.render(
	  <Auditors data={data} />,
	  document.getElementById('auditor-list')
	);

	$('.addNewAuditorModal').modal('hide');

});





// socket.on('ruby.settings.auditors-setNewAuditor-from',function(data){
// 	$('.addNewAuditorModal').modal('hide');
// 	console.log(data);
// });
