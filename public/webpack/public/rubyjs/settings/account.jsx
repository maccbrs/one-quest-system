import React from 'react';
import ReactDOM from 'react-dom';

var AccountLists = function(props){

    const listsItems = props.data.map((list,i) =>
        <tr key={i}>
            <td className="tbllink datalink" data-link={base+"/settings/account-edit/"+list.id} >
            <a href='#' className="editable editable-click">{list.project_name}</a>
            </td>
            <td >
              <a href={base+"/settings/qa-settings/"+list.id} className="btn btn-sm btn-primary"><i className="fa fa-thumbs-o-up" aria-hidden="true"> QA Settings</i></a>
            </td>
        </tr>
    );  

    return  (
          <table className="table table-bordered customized-table">
              <thead>
                  <tr>
                      <th>Profile</th>
                      <th>#</th>
                  </tr>
              </thead>
              <tbody>
                {listsItems}
              </tbody>
          </table>
     );     
}

$(document).on('click','.addAcct',function(){
	$('.addAccntModal').modal();
});


$(document).on('click','.formdata-submit',function(){

	let val = $('.addAccount-formdata').val();
	socket.emit('ruby.settings.accounts-addAccount-to',{data:val});

});

socket.emit('ruby.accounts.index-to');
socket.on('ruby.accounts.index-from',function(res){
    console.log(res);
    ReactDOM.render(
      <AccountLists data={res} />,
      document.getElementById('accountLists')
    );
});

// socket.on('ruby.settings.accounts-addAccount-from',function(res){
// 	let a = $('#td-prj-tpl').html();
// 	let b = ejs.render(a, {row:res,url:base}); 
// 	$('#prj-tpl').append(b);
// 	$('.addAccntModal').modal('hide');
//     noty({
//         theme: 'app-noty',
//         text: 'successfully saved',
//         type: 'success',
//         timeout: 5000,
//         layout: 'topRight',
//         closeWith: ['button', 'click'],
//         animation: {
//             open: 'animated fadeInDown',
//             close: 'animated fadeOutUp'
//         }
//     }); 	
// });