import React from 'react';
import ReactDOM from 'react-dom';

var AccountLists = function(props){

    const listsItems = props.data.map((list,i) =>
        <tr key={i}>
            <td >{list.project_name}</td>
            <td className="tbllink datalink" data-link={base+"/set/"+list.id}>
              <a href='#' className="editable editable-click">{list.title}</a>
            </td>
        </tr>
    );  

    return  (
          <table className="table table-bordered customized-table">
              <thead>
                  <tr>
                      <th>Project Name</th>
                      <th>Audit Set</th>
                  </tr>
              </thead>
              <tbody>
                    {listsItems}
              </tbody>
          </table>
     );     
}

$(document).on('click','.addAcct',function(){
	$('.addAccntModal').modal();
});


$(document).on('click','.formdata-submit',function(){

	let val = $('.addAccount-formdata').val();
	socket.emit('ruby.settings.accounts-addAccount-to',{data:val});

});

socket.emit('dashAccntsTo',{id:UserId});
socket.on('dashAccntsFr',function(res){
    console.log(res);
    ReactDOM.render(
      <AccountLists data={res} />,
      document.getElementById('accountLists')
    );
});

socket.on('ruby.settings.accounts-addAccount-from',function(res){
	let a = $('#td-prj-tpl').html();
	let b = ejs.render(a, {row:res,url:base}); 
	$('#prj-tpl').append(b);
	$('.addAccntModal').modal('hide');
    noty({
        theme: 'app-noty',
        text: 'successfully saved',
        type: 'success',
        timeout: 5000,
        layout: 'topRight',
        closeWith: ['button', 'click'],
        animation: {
            open: 'animated fadeInDown',
            close: 'animated fadeOutUp'
        }
    }); 	
});