import React from 'react';
import ReactDOM from 'react-dom';


socket.emit('ruby.audit.get-to',{id:SetId,bound:bound,account:account});


var SetCallInfo = function(props){

    const info = props.data;
    return  (
        <blockquote className="card-blockquote">
            <p>listen to </p>
            <p> <audio id="player" controls crossorigin >
                  <source src={info.location}  type="audio/mp3" />
                </audio>
            </p>
            <p>Agent: <span> {info.agent}</span></p>
            <p>Date of Call: <span>{info.call_date}</span></p>
            <p>Call Duration: <span> {info.length_in_min}</span></p>
            <p>Disposition: <span> {info.status} </span></p>
        </blockquote>
     );     
}

var SetAccountInfo = function(props){

    const listsItems = props.data.map((list,i) => 
        <tr key={i}>
            <td width="20px">{list.title}</td>
            <td width="20px">{list.label}</td>
            <td width="30px">{list.value}</td>
        </tr>            
    );


    return  (
        <table className="table-bordered mbtbl">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>#</th>
                    <th>Value</th>
                </tr>
            </thead>
            <tbody>
                  {listsItems} 
            </tbody>
        </table>  
     );     
}
 



socket.on('ruby.audit.get-from',function(result){
    console.log(result);
    ReactDOM.render(
      <SetCallInfo data={result.call} />,
      document.getElementById('SetCallInfo')
    );
    ReactDOM.render(
      <SetAccountInfo data={result.account} />,
      document.getElementById('SetAccountInfo')
    );      	
});