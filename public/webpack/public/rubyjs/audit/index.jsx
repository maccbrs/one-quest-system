import React from 'react';
import ReactDOM from 'react-dom';

var http = location.protocol;
var slashes = http.concat("//");
var host = slashes.concat(window.location.hostname);

//cssloader = host.concat('/rubyjs/soundmanager/css/index-rollup.css');

var page = "/rubyjs/soundmanager/css/index-rollup.css";
var cssfile = host.concat(page);


require(cssfile); 



socket.emit('ruby.audit.index-to',{id:SetId});

var timeFormat = function(dt){
    var d = new Date(dt);

   function pad(x){return x<10 ? '0'+x : x}
  return d.getFullYear()+'-'
    + pad(d.getMonth()+1)+'-'
    + pad(d.getDate()) +' '
    + pad(d.getHours())+':'
    + pad(d.getMinutes())+':'
    + pad(d.getSeconds())    
}

var SetInfo = function(props){

    const info = props.data;
    return  (
        <div id="row">
            <div className="col-md-4">
                    <p>Titlesss: {info.title}</p>
                    <p>Bound: {info.bound}</p>
                    <p>Ingroups: {info.ingroups}</p>
                    <p>Disposition: {info.disposition}</p>
            </div>
            <div className="col-md-4">
                    <p>Agents: {info.agents}</p>
                    <p>Call count: {info.call_count}</p>
                    <p>Call duration (min): {info.duration_min}</p>
                    <p>Call duration (max): {info.duration_max}</p>
            </div>
            <div className="col-md-4">
                    <p>Time generate: {info.time_generate}</p>
                    <p>Next generate: {info.next_generate}</p>
                    <p>Frequency: {info.frequency}</p>
            </div>
        </div>
     );     
}

var AuditLists = function(props){

    const listsItems = props.data.data.map((list,i) => 
        <tr key={i}>
            <td>{list.agent}</td>
            <td>{timeFormat(list.call_date)}</td>
            <td>{list.campaign_id}</td>
            <td>{list.length_in_min}</td>
            <td>{list.status}</td>
            <td></td>
            <td className="tbllink" data-link={base+"/audit/"+list.id}>audit</td>
        </tr>            
    );
    
    return  (
          <table className="table table-bordered customized-table">
              <thead>
                  <tr>
                      <th>Agent</th>
                      <th>Call Date</th>
                      <th>Campaign Id</th>
                      <th>Call Length</th>
                      <th>Dispo</th>
                      <th>Listen</th>
                      <th>#</th>
                  </tr>
              </thead>
              <tbody>
                  {listsItems}
              
              </tbody>
          </table>
     );     

}



socket.on('ruby.audit.index-from',function(res){
	console.log(res);
    ReactDOM.render(
      <SetInfo data={res} />,
      document.getElementById('setInfo')
    );
    ReactDOM.render(
      <AuditLists data={res} />,
      document.getElementById('auditList')
    );    
});