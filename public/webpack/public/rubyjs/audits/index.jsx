import React from 'react';
import ReactDOM from 'react-dom';

// var  css_sm = './soundmanager/css/index-rollup.css';

//require('localhost:8000/webpack/public/audiojs/includes/index.css'); 



socket.emit('ruby.audit.index-to',{id:SetId});

var timeFormat = function(dt){
    var d = new Date(dt);

   function pad(x){return x<10 ? '0'+x : x}
  return d.getFullYear()+'-'
    + pad(d.getMonth()+1)+'-'
    + pad(d.getDate()) +' '
    + pad(d.getHours())+':'
    + pad(d.getMinutes())+':'
    + pad(d.getSeconds())    
}

var SetInfo = function(props){

    const info = props.data;
    var ingroup = info.ingroups.replace(/,/g, ", ");
    var agents = info.agents.replace(/,/g, ", ");
    
    return  (
      <div id="row">
        <table className = 'audittable'>

          <tr>
            <th><b>Title : </b></th>
            <td>{info.title}</td>
            <th><b>Call duration (min):</b></th>
            <td>{info.duration_min}</td>
            <th><b>Time generate: </b></th>
            <td>{info.time_generate}</td>
          </tr>

          <tr>
            <th><b>Bound : </b></th>
            <td>{info.bound}</td>
            <th><b>Call duration (max): </b></th>
            <td>{info.duration_max}</td>
            <th><b>Next generate:  </b></th>
            <td>{info.next_generate}</td>
          </tr>

          <tr>
            <th><b>Call count:  </b></th>
            <td>{info.call_count}</td>
            <td></td>
            <td></td>
            <th><b>Frequency:</b></th>
            <td>{info.frequency}</td>
          </tr>
        
        </table>
        <br/>

        <table className = 'audittable'>
          <tr>
            <th><b>Ingroups: </b></th>
            <td>{ingroup}</td>
          </tr>

          <tr>
            <th><b>Agents: </b></th>
            <td>{agents}</td>
          </tr>

          <tr>
            <th><b>Dispositions: </b></th>
            <td>{info.disposition}</td>
          </tr>
        </table>
      </div>
     );     
} 

var AuditLists = function(props){

    const listsItems = props.data.data.map((list,i) => 
        
        <tr key={i}>
            <td>{list.agent}</td>
            <td>{timeFormat(list.call_date)}</td>
            <td>{list.campaign_id}</td>
            <td>{list.length_in_min}</td>
            <td>{list.status}</td>
            <td>
            	<audio id="player" controls crossorigin >
				  <source src={list.location}  type="audio/mp3" />
				</audio>
			</td> 
            <td className="tbllink" data-link={base+"/audit/"+list.id+"?bound="+props.data.bound+"&account="+props.data.account_id}>
            <a href='#' className="editable editable-click">Audit</a></td>
        </tr> 

    );
    
    return  (
          <table className="table table-bordered ">
              <thead>
                  <tr>
                      <th>Agent</th>
                      <th>Call Date</th>
                      <th>Campaign Id</th>
                      <th>Call Length</th>
                      <th>Dispo</th>
                      <th>Listen</th>
                      <th>#</th>
                  </tr>
              </thead>
              <tbody>
                    {listsItems}
              </tbody>
          </table>
     );     

}



socket.on('ruby.audit.index-from',function(res){

    ReactDOM.render(
      <SetInfo data={res} />,
      document.getElementById('setInfo')
    );
    ReactDOM.render(
      <AuditLists data={res} />,
      document.getElementById('auditList')
    );    
});