import React from 'react';
import ReactDOM from 'react-dom';


var timeFormat = function(dt){
    var d = new Date(dt);

   function pad(x){return x<10 ? '0'+x : x}
  return d.getFullYear()+'-'
    + pad(d.getMonth()+1)+'-'
    + pad(d.getDate()) +' '
    + pad(d.getHours())+':'
    + pad(d.getMinutes())+':'
    + pad(d.getSeconds())    
}



socket.emit('ruby.set.index-to',{id:SetId});

var SetInfo = function(props){

    const info = props.data;
    var ingroup = info.ingroups.replace(/,/g, ", ");
    var agents = info.agents.replace(/,/g, ", ");
    return  (

      <div id="row">
        <table className = 'audittable'>
          <thead></thead>
          <tbody>
          <tr>
            <th> Title : </th>
            <td>{info.title}</td>
            <th> Call duration (min):</th>
            <td>{info.duration_min}</td>
            <th> Time generate: </th>
            <td>{info.time_generate}</td>
          </tr>

          <tr>
            <th> Bound : </th>
            <td>{info.bound}</td>
            <th> Call duration (max): </th>
            <td>{info.duration_max}</td>
            <th>Next generate:  </th>
            <td>{info.next_generate}</td>
          </tr>

          <tr>
            <th> Call count: </th>
            <td>{info.call_count}</td>
            <td></td>
            <td></td>
            <th> Frequency:</th>
            <td>{info.frequency}</td>
          </tr>
          </tbody>
        </table>
        <br/>

        <table className = 'audittable'>
          <thead></thead>
          <tbody>
          <tr>
            <th> Ingroups: </th>
            <td>{ingroup}</td>
          </tr>

          <tr>
            <th> Agents: </th>
            <td>{agents}</td>
          </tr>

          <tr>
            <th> Dispositions: </th>
            <td>{info.disposition}</td>
          </tr>
          </tbody>
        </table>
      </div>
     );     
}

var AuditLists = function(props){

    const listsItems = props.data.audits.map((list,i) => 
        <tr key={i}>
            <td className="tbllink datalink" data-link={base+"/audits/"+list.id} >{timeFormat(list.audit_date)}</td>
        </tr>            
    );
    
    return  (
          <table className="table table-bordered customized-table">
              <thead>
                <tr>
                    <th>Audit Date</th>
                </tr>
              </thead>
              <tbody>
                <a href='#' className="editable editable-click">{listsItems}</a>
              </tbody>
          </table>
     );     

}



socket.on('ruby.set.index-from',function(res){
    ReactDOM.render(
      <SetInfo data={res} />,
      document.getElementById('setInfo')
    );
    ReactDOM.render(
      <AuditLists data={res} />,
      document.getElementById('auditList')
    );    
});