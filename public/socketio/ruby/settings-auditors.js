$(document).on('click','.addNewAuditor',function(){
	socket.emit('ruby.settings.auditors-getAllUsers-to');
});

socket.on('ruby.settings.auditors-getAllUsers-from',function(result){
	$('.addNewAuditorModal').modal();
	let a = $('#template-y').html();
	let b = ejs.render(a, {rows:result}); 
	$('#template-x').html(b);	
});

$(document).on('click','.formdata-submit',function(){
	
	var id = $('input[name="user"]:checked').val();

	if(id != undefined){
		console.log(id);
		socket.emit('ruby.settings.auditors-setNewAuditor-to',{id:id});
	}
	
});

socket.on('ruby.settings.auditors-setNewAuditor-from',function(data){
	$('.addNewAuditorModal').modal('hide');
	console.log(data);
});