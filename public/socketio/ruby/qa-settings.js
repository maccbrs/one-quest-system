$(document).on('click','.addSet',function(){
	$('.addSetModal').modal();
});

socket.emit('ruby.settings.accounts-qa-settings-to',{id:accntId});

// socket.on('dashAccntsFr',function(res){

// 	let a = $('#td-prj-tpl').html();
// 	let b = ejs.render(a, {row:res,url:base}); 
// 	$('#prj-tpl').html(b);
	
// });

$('.formdata-submit').on('click',function(){
	var obj = {};
	$('.formdata').map(function(v,i,a){
		obj[$(i).attr('name')] = $(i).val();
	});
	obj['account_id'] = accntId;
	socket.emit('ruby.qa-settings.add-to',obj);
});

socket.on('ruby.settings.accounts-qa-settings-from',function(result){
	
	let a = $('#template').html();
	let b = ejs.render(a, {rows:result}); 
	$('#prj-tpl').html(b);	
});

socket.on('ruby.qa-settings.add-from',function(result){
	let a = $('#template2').html();
	let b = ejs.render(a, {row:result}); 
	$('#prj-tpl').append(b);
	$('.addSetModal').modal('hide');
    noty({
        theme: 'app-noty',
        text: 'successfully saved',
        type: 'success',
        timeout: 5000,
        layout: 'topRight',
        closeWith: ['button', 'click'],
        animation: {
            open: 'animated fadeInDown',
            close: 'animated fadeOutUp'
        }
    }); 	
});