
$(document).on('click','.addAcct',function(){
	$('.addAccntModal').modal();
});


$(document).on('click','.formdata-submit',function(){

	let val = $('.addAccount-formdata').val();
	socket.emit('ruby.settings.accounts-addAccount-to',{data:val});

});

socket.emit('dashAccntsTo');
socket.on('dashAccntsFr',function(res){

	let a = $('#td-prj-tpl').html();
	let b = ejs.render(a, {row:res,url:base}); 
	$('#prj-tpl').html(b);
	
});

socket.on('ruby.settings.accounts-addAccount-from',function(res){
	let a = $('#td-prj-tpl').html();
	let b = ejs.render(a, {row:res,url:base}); 
	$('#prj-tpl').append(b);
	$('.addAccntModal').modal('hide');
    noty({
        theme: 'app-noty',
        text: 'successfully saved',
        type: 'success',
        timeout: 5000,
        layout: 'topRight',
        closeWith: ['button', 'click'],
        animation: {
            open: 'animated fadeInDown',
            close: 'animated fadeOutUp'
        }
    }); 	
});