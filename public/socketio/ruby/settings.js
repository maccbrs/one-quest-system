$(document).on('click','.addAcctField',function(){
	$('.addAccntFieldModal').modal();
});

$(document).on('click','.formdata-submit',function(){
	var obj = {};
	$('.formdata').map(function(i,v,a){
		obj[$(v).attr('name')] = $(v).val();
	});
	socket.emit('settingsAccntFieldTo',obj);
});

socket.on('settingsAccntFieldFr',function(res){

	$('.addAccntFieldModal').modal('hide');
	let a = $('#td-acc-field-tpl').html();
	let b = ejs.render(a, {row:res}); 
	$('#acc-field-tpl').append(b);

	$('.formdata').map(function(i,v,a){
		$(v).val("");
	});

    noty({
        theme: 'app-noty',
        text: 'successfully saved',
        type: 'success',
        timeout: 5000,
        layout: 'topRight',
        closeWith: ['button', 'click'],
        animation: {
            open: 'animated fadeInDown',
            close: 'animated fadeOutUp'
        }
    }); 	

});



$(document).on('blur','.formdata',function(){
	socket.emit('settingsAccntEditTo',{data:$(this).data('ik'),value:$(this).text()});
});

socket.on('settingsAccntEditFr',function(res){
    // noty({
    //     theme: 'app-noty',
    //     text: 'successfully updated',
    //     type: 'success',
    //     timeout: 5000,
    //     layout: 'topRight',
    //     closeWith: ['button', 'click'],
    //     animation: {
    //         open: 'animated fadeInDown',
    //         close: 'animated fadeOutUp'
    //     }
    // }); 
});