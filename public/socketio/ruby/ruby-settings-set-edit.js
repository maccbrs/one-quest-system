$(document).on('click','.editIngroups',function(){
	$('.editIngroupsModal').modal();
});

$(document).on('change','.selectbound',function(){
	var type = $(this).val();
	if(type != ''){
		socket.emit('ruby.setting.set-edit.selectbound-to',{type:type});  
	}
});

socket.on('ruby.setting.set-edit.selectbound-from',function(result){
	let a = $('#template-y').html();
	let b = ejs.render(a, {rows:result}); 
	$('#template-x').html(b);	
});

$(document).on('click','.selectInbound-formdata-submit',function(){

	var items = [];
	$('.formdata-ingroups:checked').map(function(v,i,a){
		items.push($(i).val());
	});
	socket.emit('ruby.setting.set-edit.saveIngroups-to',{data:items,id:accntId});

});

socket.on('ruby.setting.set-edit.saveIngroups-from',function(result){
	// var temp = new Array();
	// temp = result.split(','); 

	$('.editIngroupsModal').modal('hide');
	$('#ingroup-value-x').text(result);
    noty({
        theme: 'app-noty',
        text: 'successfully saved',
        type: 'success',
        timeout: 5000,
        layout: 'topRight',
        closeWith: ['button', 'click'],
        animation: {
            open: 'animated fadeInDown',
            close: 'animated fadeOutUp'
        }
    }); 
});

$(document).on('click','.formdata-bound',function(){

	socket.emit('ruby.setting.set-edit.updateBound-to',{data:$(this).val(),id:accntId});

});

//--------------------------------------------------------------
$(document).on('click','.editAgents',function(){
	$('.editAgentsModal').modal();
	socket.emit('ruby.setting.set-edit.getAllAgents-to',{id:accntId});
});
socket.on('ruby.setting.set-edit.getAllAgents-from',function(result){
	let a = $('#template2-y').html();
	let b = ejs.render(a, {rows:result}); 
	$('#template2-x').html(b);	
});

//--------------------------------------------------------------
$(document).on('click','.editAuditor',function(){
	$('.editAuditorModal').modal();
	socket.emit('ruby.settings.auditors-lists-to');
});
socket.on('ruby.settings.auditors-lists-from',function(result){
	console.log(result);
	let a = $('#template3-y').html();
	let b = ejs.render(a, {rows:result}); 
	$('#template3-x').html(b);	
});

$(document).on('click','.auditor-formdata-submit',function(){

	var items = [];
	$('.formdata-auditor:checked').map(function(v,i,a){
		items.push($(i).val());
	});

	if(items.length){
		socket.emit('ruby.setting.set-edit.saveAuditor-to',{value:items[0],id:accntId});
	}

});
socket.on('ruby.setting.set-edit.saveAuditor-from',function(result){
	$('.editAuditorModal').modal('hide');
	console.log(result);
	$('#auditor-value-x').text(result);
    noty({
        theme: 'app-noty',
        text: 'successfully saved',
        type: 'success',
        timeout: 5000,
        layout: 'topRight',
        closeWith: ['button', 'click'],
        animation: {
            open: 'animated fadeInDown',
            close: 'animated fadeOutUp'
        }
    }); 
}); 

 
//--------------------------------------------------------------


$(document).on('click','.agents-formdata-submit',function(){

	var items = [];
	$('.formdata-agents:checked').map(function(v,i,a){
		items.push($(i).val());
	});
	socket.emit('ruby.setting.set-edit.saveAgents-to',{data:items,id:accntId});

});

socket.on('ruby.setting.set-edit.saveAgents-from',function(result){
	$('.editAgentsModal').modal('hide');
	console.log(result);
	$('#agents-value-x').text(result);
    noty({
        theme: 'app-noty',
        text: 'successfully saved',
        type: 'success',
        timeout: 5000,
        layout: 'topRight',
        closeWith: ['button', 'click'],
        animation: {
            open: 'animated fadeInDown',
            close: 'animated fadeOutUp'
        }
    }); 
}); 
 
$(document).on('click','.formdata-frequency',function(){
  socket.emit('ruby.setting.set-edit.updateFrequency-to',{data:$(this).val(),id:accntId});
});
$(document).on('blur','input[name="call_count"]',function(){
  socket.emit('ruby.setting.set-edit.updateCallCount-to',{data:$(this).val(),id:accntId});
});
$(document).on('blur','input[name="call_min"]',function(){
  socket.emit('ruby.setting.set-edit.updateCallMin-to',{data:$(this).val(),id:accntId});
});
$(document).on('blur','input[name="call_max"]',function(){
  socket.emit('ruby.setting.set-edit.updateCallMax-to',{data:$(this).val(),id:accntId});
});
$(document).on('blur','input[name="generate_time"]',function(){
	socket.emit('ruby.setting.set-edit.updateGenTime-to',{data:$(this).val(),id:accntId});
});


//------------------ Disposition -------------------------//
$(document).on('click','.editDispositions',function(){
	socket.emit('ruby.setting.set-edit.DispoCampaigns-to',{id:accntId});
});

socket.on('ruby.setting.set-edit.DispoCampaigns-from',function(result){
	$('.editDispositionsModal').modal();
	let a = $('#template-select-campaign-y').html();
	let b = ejs.render(a, {rows:result}); 
	$('#template-select-campaign-x').html(b);	
});

$(document).on('change','#template-select-campaign-x',function(){

	socket.emit('ruby.settings.dispositions-lists-to',{campaign_id:$(this).val()});		

});

socket.on('ruby.settings.dispositions-lists-from',function(result){
	console.log(result);
	$('.editDispositionsModal').modal();
	let a = $('#template4-y').html();
	let b = ejs.render(a, {rows:result}); 
	$('#template4-x').html(b);	
});

$(document).on('click','.dispo-formdata-submit',function(){

	var items = [];
	$('.formdata-dispo:checked').map(function(v,i,a){
		items.push($(i).val());
	});

	if(items.length){
		socket.emit('ruby.setting.set-edit.saveDispo-to',{data:items,id:accntId});
	}

});

socket.on('ruby.setting.set-edit.saveDispo-from',function(result){
	$('.editDispositionsModal').modal('hide');
	console.log(result);
	$('#dispo-value-x').text(result);
    noty({
        theme: 'app-noty',
        text: 'successfully saved',
        type: 'success',
        timeout: 5000,
        layout: 'topRight',
        closeWith: ['button', 'click'],
        animation: {
            open: 'animated fadeInDown',
            close: 'animated fadeOutUp'
        }
    }); 
}); 
//------------------ End Disposition -------------------------//




//auditor-formdata-submit
