var putContent = function(res){
	for(var i in res){
		$('.content-'+i).text(res[i]);
		$('.formdata-'+i).val(res[i]);
	}
}

var chkbxGetter = function(selector){
        var a = [];
        var b = '';
        var n = $(selector+":checked").length;
        if (n > 0){
            $(selector+":checked").each(function(){ 
                a.push($(this).val());
            });
            b = a.join();
        }
        return b;
}

var chkbxPutter = function(selector,value){
	if(arr){
	var arr = value.split(',');
	arr.map(function(x){
		$("input:checkbox[value="+x+"]").attr("checked", true);
	});
	}
}

var inputGetter = function(attrname){
	obj = {};
	attrname.forEach(function(a){
		obj[a] = $('input[name="'+a+'"]').val();
	});
	return obj;
}

var updateData = function(data,bound){

	var totalmin = (data.calls.sec/60).toFixed(2);
	$('.'+bound+'-totalmin-text').text(totalmin);
	$('.'+bound+'-totalcalls-text').text(data.calls.count);
	$('.date-coverage').text('from: '+data.coverage.from+' to: '+data.coverage.to);

	if(bound != 'transfer'){
		var dispo = data.dispo;
		var dispoHtml = '';
		for(var i in dispo){
	       dispoHtml += '<tr><th>'+i+'</th><td>'+dispo[i].count+'</td><td>'+dispo[i].sec+'</td></tr>';
		}
		$('.'+bound+'-dispo-append').html(dispoHtml);
	}

	var dates = data.dates;
	var datesHtml = '';
	for(var i in dates){
       datesHtml += '<tr><th>'+i+'</th><td>'+dates[i].count+'</td></tr>';
	}
	$('.'+bound+'-dates-append').html(datesHtml);

}

setInterval(function(){
	socket.emit('PearlCampaignGetTo',{id:cid});
	socket.emit('PearlCampaignGetRangedTo',{id:cid});
	console.log('hi');
},60000);

socket.emit('PearlCampaignGetTo',{id:cid});
socket.emit('PearlCampaignGetRangedTo',{id:cid});

socket.on('PearlCampaignGetRangedFrom',function(res){
	updateData(res,'inbound');
});
socket.on('PearlCampaignGetRangedOutboundFrom',function(res){
	updateData(res,'outbound');
});
socket.on('PearlCampaignGetRangedTransFrom',function(res){
	updateData(res,'transfer');
});


socket.on('PearlCampaignGetFrom',function(res){
	chkbxPutter('.account_type',res.account_type);
	putContent(res);
	if(res.account_type != ''){
		var bounds = res.account_type;
		var arr = bounds.split(',');
		$('.'+arr[0]+'-tpl').css('display','block');
		arr.map(function(v,i,a){
			$('.btn-'+v).css('display','block');
		});
	}
	
});

$(document).on('click','.editInfo',function(){
	$('.editInfoModal').modal();
});

$(document).on('click','.formdata-submit',function(){
	var data = [];
	$('.formdata').map(function(){
		data.push({key:$(this).attr('name'),value:$(this).val()});
	});
	var obj = {};
	data.forEach(function(v,i){
  		obj[v.key] = v.value;
    });
	socket.emit('PearlCampaignUpdateTo',{id:cid,data:obj});
});

socket.on('PearlCampaignUpdateFrom',function(res){
	putContent(res);
	$('.editInfoModal').modal('hide');
	$('.editTechInfoModal').modal('hide');
    noty({
        theme: 'app-noty',
        text: 'successfully saved',
        type: 'success',
        timeout: 5000,
        layout: 'topRight',
        closeWith: ['button', 'click'],
        animation: {
            open: 'animated fadeInDown',
            close: 'animated fadeOutUp'
        }
    }); 	
});

$(document).on('click','.editTechInfo',function(){
	$('.editTechInfoModal').modal();
});

$(document).on('click','.tech-formdata-submit',function(){

	var x = inputGetter(['dids','prefix','billing_cycle','ingroups']);
	x['account_type'] = chkbxGetter('.account_type');
	socket.emit('PearlCampaignUpdateTo',{id:cid,data:x});

}); 

;(function(){

	var bounds = ['inbound','outbound','transfer'];
	bounds.map(function(v,i,a){
		$('.btn-'+v).on('click',function(){
			bounds.map(function(v,i,a){
				$('.'+v+'-tpl').css('display','none');
			});
			$('.'+v+'-tpl').css('display','block');
		});
	});

})();




