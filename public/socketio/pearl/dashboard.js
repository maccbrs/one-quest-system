socket.emit('PearlDashboardIndexGetCampaignsTo',{user:1});
socket.on('PearlDashboardIndexGetCampaignsFrom',function(res){
	console.log(res);
    let a = $('#td-tpl').html();
    let b = ejs.render(a, {row:res,url:campaignurl});                
    $('#campaignTblBody').html(b);
});



$(document).on('click','.addCampaign',function(){
	$('.addCampaignModal').modal();
});

$(document).on('click','.formdata-submit',function(){
	var data = [];
	$('.formdata').map(function(){
		data.push({key:$(this).attr('name'),value:$(this).val()});
	});
	var obj = {};
	data.forEach(function(v,i){
  		obj[v.key] = v.value;
    });
	socket.emit('PearlDashboardIndexAddCampaignTo',obj);
});

socket.on('PearlDashboardIndexAddCampaignFrom',function(res){

    var tpl = `<tr class="tbllink" data-link="`+campaignurl+`/`+res.id+`">
        <td>`+res.campaign+`</td>
        <td>`+res.alias+`</td>
        <td>`+res.email+`</td>
        <td>`+res.contact_name+`</td>
        <td>`+res.contact_no+`</td>
    </tr>`;
 	$('#campaignTblBody').append(tpl);
	$('.formdata').map(function(){
		$(this).val('');
	}); 
	$('.addCampaignModal').modal('hide');
    noty({
        theme: 'app-noty',
        text: 'successfully saved',
        type: 'success',
        timeout: 3000,
        layout: 'topRight',
        closeWith: ['button', 'click'],
        animation: {
            open: 'animated fadeInDown',
            close: 'animated fadeOutUp'
        }
    }); 	

});



        // noty({
        //     theme: 'app-noty',
        //     text: 'successfully saved',
        //     type: 'success',
        //     timeout: 3000,
        //     layout: 'topRight',
        //     closeWith: ['button', 'click'],
        //     animation: {
        //         open: 'animated fadeInDown',
        //         close: 'animated fadeOutUp'
        //     }
        // }); 
