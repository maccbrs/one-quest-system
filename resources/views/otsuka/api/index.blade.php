<?php $asset = URL::asset('/'); $request = Request::instance(); ?> 
@extends('otsuka.fullview')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}

</style>
 
 
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header no-bg b-a-0">
			

            </div>
            <div class="card-block" >
			
            <div class="row media">
				
				<div class="col-md-12">
							@if (\Session::has('return_msg'))
							<div class="alert  return_msg <?php  if(\Session::get('return_result') == '1') {echo 'alert-success';} else {echo 'alert-danger';} ?>">
								<ul>
									<li>{!! \Session::get('return_msg') !!}</li>
								</ul>
							</div>
							@endif
					<div class="panel panel-default">
					  <div class="panel-heading"> <span>FETCH RECORDS {{count($fetch_records)}}</span> </div>
					  <div class="panel-body">		
						
						
						<div style="overflow: auto;">
						@if(!empty($fetch_records))
						@if(count($fetch_records) <= 1)
						
						<table class="datatable table table-bordered" style="font-size:9px">
							<thead>
								<tr>
									<th>Column</th>	
									
									
									<th>Value</th>
									
									

								</tr>
							</thead>
							<tbody>
									@if(!empty($fetch_records))
												
												
												@foreach($fetch_records as $key => $val)
												
													@if(!empty($columns))
													@foreach($columns as $column)
														<tr>
														<td>{{$column}}</td>
														<td>{{$val->$column}}</td>
														</tr>
														

													@endforeach
													@endif
												
												
												@endforeach
												
											
											
									
									
										@endif
							
										<tr>
											
											
									

										</tr>
								
								
								
							</tbody>

						</table>
						
							
							
						@elseif(count($fetch_records) > 1)
						
						<table class="datatable table table-bordered" style="font-size:9px">
							<thead>
							<tr>
								@if(!empty($columns))
								@foreach($columns as $column)
										
									<th>{{$column}}</th>
									
									

								@endforeach
								@endif
								</tr>		
									

							
							</thead>
							<tbody>
									@if(!empty($fetch_records))
												
												
												@foreach($fetch_records as $key => $val)
												<tr>
													@if(!empty($columns))
													@foreach($columns as $column)
														
														
														<td>{{$val->$column}}</td>
														
														

													@endforeach
													@endif
												</tr>
												
												@endforeach
												
											
											
									
									
										@endif
							
										<tr>
											
											
									

										</tr>
								
								
								
							</tbody>

						</table>
						
							@endif	
							
							
							
							
							
							
							
						@endif
						</div>
						
						</div>
					</div>
					
				</div>
				

            </div>

            </div>
        </div>
    </div>
</div>



 <!-- Modal -->
  <div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
		 @if (session('status') == 'success')
          <div class="alert alert-success">
        
				<strong>{{session('message')}}</strong> 
		
          </div>
		@elseif (session('status') == 'failed')
		 <div class="alert alert-danger">
        
				<strong>{{session('message')}}</strong> 
		
          </div>
        @endif   
          </div>         
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div> 
    </div>
  </div>
 
 
 
@endsection




@section('header-scripts')

@endsection

@section('footer-scripts')

<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>

<script>

setTimeout(function() {
    $('.return_msg').slideUp();
}, 2000); // <-- time in milliseconds

$('.datatable').DataTable();






@if (session('status'))
<script type="text/javascript">
  $('#success').modal('show');
</script>
@endif


@endsection