<?php 
//header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
//header("Cache-Control: post-check=0, pre-check=0", false);
//header("Pragma: no-cache");
//header('Content-type: application/json'); ?>
<?php $assets = URL::asset('/');?>
<?php $pic = URL::asset('/uploads/');?>
<?php $Auth = Auth::user()->id;?>
<?php $GemUser = new \App\Http\Models\gem\GemUser;?>
<?php $avatar =  $GemUser->where('id','=',$Auth)->get()?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @yield('header-css')

  <link rel="stylesheet" href="{{$assets}}inventory/css/bootstrap.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/css/buttons.dataTables.css">
  	    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style>
    hr.style1{
  border-top: 1px solid #D3D3D3;
    }

    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 667px;} 
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>


    	@yield('content')




</body>
      <script src="{{$assets}}inventory/js/jquery-3.2.1.js"></script>
      
	  
      <script src="{{$assets}}inventory/datatables/jquery-ui.js"></script>
      <script src="{{$assets}}inventory/datatables/bootstrap.min.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
      <script src="{{$assets}}inventory/datatables/jquery.dataTables.yadcf.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>
	 <script src="{{$assets}}JS/jquery.mask.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>

<script>
/*$(document).ready(function() 
{ 
 $('form').ajaxForm(function() 
 {
  alert("Uploaded SuccessFully");
 }); 
});*/
$(document).ready(function() {
	
$('#queue').DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
});

/*setTimeout(function() {
   location.reload();
   }, 10000);
*/
$('#example2').DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
});

$('#example3').DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
});
} );

$(function () {
    $('#pv').click(function () {
        window.open("{{ route('otsuka.pv') }}");
    });
});

$(function () {
    $('#pqc').click(function () {
        window.open("{{$assets}}uploads/PQC.html","","width=1280,height=1024");
    });
});
  $( function() {
    $( ".datepicker" ).datepicker();
  } );

</script>

@yield('footer-scripts')

</html>