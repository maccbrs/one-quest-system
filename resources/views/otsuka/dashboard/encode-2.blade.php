@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.bottomright {position:absolute; bottom:0;  margin-bottom:7px; margin:7px; right: 0;}

/**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
margin-left: 3vw;     
margin-right:3vw;  
}    

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(10);    
-moz-transform: scale(10);  
-webkit-transform: scale(10);  
-o-transform: scale(10);  
transform: scale(10);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
ul.gallery {      
margin-left: 15vw;       
margin-right: 15vw;
}

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}
</style>
      <div id="myModal" class="modal fade">
        <div class="modal-dialog">
        <div class="col-md-6">
        <div class="modal-content">
          <div class="modal-header">
            Enroll
          </div>
          <div class="modal-body">
        <div class="panel panel-default">
          <div class="panel-heading">Patient's Info</div>
          <div class="panel-body">
            Patient Code: <p style="color:green;">{{$px_code}}</p>
            <input type="hidden" class="form-control input-sm" name="patient_code" value="{{$px_code}}">
            Patient Last Name: <input type="text" class="form-control input-sm" name="patient_lastname" required="">
            Patient First Name: <input type="text" class="form-control input-sm" name="patient_firstname" required="">
            Patient Middle Name: <input type="text" class="form-control input-sm" name="patient_middlename" required="">
            Nickname: <input type="text" class="form-control input-sm" name="nickname" required="">
            Address: <input type="text" class="form-control input-sm" name="address" required="">
            Birth Date: <input type="date" class="form-control input-sm" name="birth_date" required="">
            Age: <input type="text" class="form-control input-sm" name="age" required="">
            Gender: <select class="form-control input-sm" name="gender" required="">
                      <option>*Please Select Gender</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
            Mobile Number: <input type="tel" class="form-control input-sm" name="mobile_number" required="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
            Mobile Number 2: <input type="tel" class="form-control input-sm" name="mobile_number_2" required="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
            Landline: <input type="tel" class="form-control input-sm" name="phone_number" required="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
          </div>
        </div>
      </div>
      @endif
      <div class="col-md-6" style="margin-top: 250px;">
        <div class="panel panel-default">
          <div class="panel-heading">Images</div>
          <div class="panel-body">  
            @foreach($item as $i)
              <img class="zoom" style="width: 10%; height: 10%;" src="{{$pic.'/'.$i->filename}}">
            @endforeach
          </div>
        </div>
      </div>
       </div>
       <div class="modal-footer">
<!--                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>

@endsection 

@section('footer-scripts')
<script>


</script>
@endsection

