@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
  text-align: center;
}

</style>



        <!-- Main content -->
<div class="panel panel-default">
  <div class="panel-heading"><b>MTD Summary Retrieval Report {{date('Ymd')}}</b></div>
  <div class="panel-body">

      <div class="col-md-12">
        <table class="display nowrap compact datatable">
          <thead>
            <tr>
              <th>TEAM</th>
              <th>AREA</th>
              <th>MR NAME</th>
              <th>TOTAL VALID RETRIEVAL</th>
              <th colspan="14">ABILFY</th>
              <th colspan="14">AMINOLEBAN</th>
              <th colspan="14">PLETAAL</th>
              <th></th>
              <th colspan="5">TOTAL INVALID RETRIEVAL</th>
            </tr>
            <tr>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th>NEW</th>
              <th>FINISHED</th>
              <th>STOPPED MED.</th>
              <th>SWITCHED MED.</th>
              <th>VOLUNTARY</th>
              <th>REFERRAL</th>
              <th>TOTAL VERIFIED RETRIEVAL</th>
              <th>DECLINED</th>
              <th>WRONG NUMBER</th>
              <th>NO ANSWER W/ 5 ATTEMPTS</th>
              <th>NO ANSWER W/IN 4 ATTEMPTS</th>
              <th>NO PX CONSENT</th>
              <th>RETRIEVAL ON QUEUE</th>
              <th>TOTAL UNVERIFIED RETRIEVAL</th>
              <th>NEW</th>
              <th>FINISHED</th>
              <th>STOPPED MED.</th>
              <th>SWITCHED MED.</th>
              <th>VOLUNTARY</th>
              <th>REFERRAL</th>
              <th>TOTAL VERIFIED RETRIEVAL</th>
              <th>DECLINED</th>
              <th>WRONG NUMBER</th>
              <th>NO ANSWER W/ 5 ATTEMPTS</th>
              <th>NO ANSWER W/IN 4 ATTEMPTS</th>
              <th>NO PX CONSENT</th>
              <th>RETRIEVAL ON QUEUE</th>
              <th>TOTAL UNVERIFIED RETRIEVAL</th>
              <th>NEW</th>
              <th>FINISHED</th>
              <th>STOPPED MED.</th>
              <th>SWITCHED MED.</th>
              <th>VOLUNTARY</th>
              <th>REFERRAL</th>
              <th>TOTAL VERIFIED RETRIEVAL</th>
              <th>DECLINED</th>
              <th>WRONG NUMBER</th>
              <th>NO ANSWER W/ 5 ATTEMPTS</th>
              <th>NO ANSWER W/IN 4 ATTEMPTS</th>
              <th>NO PX CONSENT</th>
              <th>RETRIEVAL ON QUEUE</th>
              <th>TOTAL UNVERIFIED RETRIEVAL</th>
              <th></th>
              <th>BLURRED PHOTO</th>
              <th>DUPLICATE SUBMISSIONS</th>
              <th>PX ALREADY ENROLLED</th>
              <th>WRONG UPLOAD</th>
              <th>TOTAL</th>

            </tr>
          </thead>
        <tbody>
          @foreach($list as $l)
          <tr>
            <td>{{$l->team}}</td>
            <td>{{$l->new_code}}</td>
            <td>{{$l->pso_name}}</td>
            <td>
              {{ 
                (!empty(($l->connect_user->fetch_abilify_new))?count($l->connect_user->fetch_abilify_new): 0) +
                (!empty(($l->connect_user->fetch_abilify_finished))?count($l->connect_user->fetch_abilify_finished): 0) +
                (!empty(($l->connect_user->fetch_abilify_stopped))?count($l->connect_user->fetch_abilify_stopped): 0) +
                (!empty(($l->connect_user->fetch_abilify_switched))?count($l->connect_user->fetch_abilify_switched): 0) +
                (!empty(($l->connect_user->fetch_abilify_declined))?count($l->connect_user->fetch_abilify_declined): 0) +
                (!empty(($l->connect_user->fetch_abilify_wrong_number))?count($l->connect_user->fetch_abilify_wrong_number): 0) +
                (!empty(($l->connect_user->fetch_abilify_noanswer_5))?count($l->connect_user->fetch_abilify_noanswer_5): 0) +
                (!empty(($l->connect_user->fetch_abilify_noanswer_4))?count($l->connect_user->fetch_abilify_noanswer_4): 0) +
                (!empty(($l->connect_user->fetch_abilify_noconsent))?count($l->connect_user->fetch_abilify_noconsent): 0) +
                (!empty(($l->connect_user->fetch_abilify_queue))?count($l->connect_user->fetch_abilify_queue): 0) +
                (!empty(($l->connect_user->fetch_aminoleban_new))?count($l->connect_user->fetch_aminoleban_new): 0) +
                (!empty(($l->connect_user->fetch_aminoleban_finished))?count($l->connect_user->fetch_aminoleban_finished): 0) +
                (!empty(($l->connect_user->fetch_aminoleban_stopped))?count($l->connect_user->fetch_aminoleban_stopped): 0) +
                (!empty(($l->connect_user->fetch_aminoleban_switched))?count($l->connect_user->fetch_aminoleban_switched): 0) +
                (!empty(($l->connect_user->fetch_aminoleban_declined))?count($l->connect_user->fetch_aminoleban_declined): 0) +
                (!empty(($l->connect_user->fetch_aminoleban_wrong_number))?count($l->connect_user->fetch_aminoleban_wrong_number): 0) +
                (!empty(($l->connect_user->fetch_aminoleban_noanswer_5))?count($l->connect_user->fetch_aminoleban_noanswer_5): 0) +
                (!empty(($l->connect_user->fetch_aminoleban_noanswer_4))?count($l->connect_user->fetch_aminoleban_noanswer_4): 0) +
                (!empty(($l->connect_user->fetch_aminoleban_noconsent))?count($l->connect_user->fetch_aminoleban_noconsent): 0) +
                (!empty(($l->connect_user->fetch_aminoleban_queue))?count($l->connect_user->fetch_aminoleban_queue): 0) +
                (!empty(($l->connect_user->fetch_pletaal_new))?count($l->connect_user->fetch_pletaal_new): 0) +
                (!empty(($l->connect_user->fetch_pletaal_finished))?count($l->connect_user->fetch_pletaal_finished): 0) +
                (!empty(($l->connect_user->fetch_pletaal_stopped))?count($l->connect_user->fetch_pletaal_stopped): 0) +
                (!empty(($l->connect_user->fetch_pletaal_switched))?count($l->connect_user->fetch_pletaal_switched): 0) +
                (!empty(($l->connect_user->fetch_pletaal_declined))?count($l->connect_user->fetch_pletaal_declined): 0) +
                (!empty(($l->connect_user->fetch_pletaal_wrong_number))?count($l->connect_user->fetch_pletaal_wrong_number): 0) +
                (!empty(($l->connect_user->fetch_pletaal_noanswer_5))?count($l->connect_user->fetch_pletaal_noanswer_5): 0) +
                (!empty(($l->connect_user->fetch_pletaal_noanswer_4))?count($l->connect_user->fetch_pletaal_noanswer_4): 0) +
                (!empty(($l->connect_user->fetch_pletaal_noconsent))?count($l->connect_user->fetch_pletaal_noconsent): 0) +
                (!empty(($l->connect_user->fetch_pletaal_queue))?count($l->connect_user->fetch_pletaal_queue): 0)
              }}
            </td>
            @if(!empty($l->connect_user->fetch_abilify_new))
            <td>{{$l->connect_user->fetch_abilify_new->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_finished))
            <td>{{$l->connect_user->fetch_abilify_finished->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_stopped))
            <td>{{$l->connect_user->fetch_abilify_stopped->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_switched))
            <td>{{$l->connect_user->fetch_abilify_switched->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->fetch_voluntary_abilify))
            <td>{{$l->fetch_voluntary_abilify->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_referral_abilify))
            <td>{{$l->connect_user->fetch_referral_abilify->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_total_verified) or !empty($l->fetch_voluntary_abilify) or !empty($l->connect_user->fetch_referral_abilify))
              @if(!empty($l->connect_user->fetch_abilify_total_verified) and empty($l->fetch_voluntary_abilify) and empty($l->connect_user->fetch_referral_abilify))
                <td>{{$l->connect_user->fetch_abilify_total_verified->count()}}</td>
              @elseif(!empty($l->connect_user->fetch_abilify_total_verified) and !empty($l->fetch_voluntary_abilify) and empty($l->connect_user->fetch_referral_abilify))
                <td>{{$l->connect_user->fetch_abilify_total_verified->count() + $l->fetch_voluntary_abilify->count()}}</td>
              @elseif(!empty($l->connect_user->fetch_abilify_total_verified) and !empty($l->fetch_voluntary_abilify) and !empty($l->connect_user->fetch_referral_abilify))
                <td>{{$l->connect_user->fetch_abilify_total_verified->count() + $l->fetch_voluntary_abilify->count() + $l->connect_user->fetch_referral_abilify->count()}}
              @elseif(empty($l->connect_user->fetch_abilify_total_verified) and empty($l->fetch_voluntary_abilify) and !empty($l->connect_user->fetch_referral_abilify))
                <td>{{$l->connect_user->fetch_referral_abilify->count()}}</td>
              @elseif(empty($l->connect_user->fetch_abilify_total_verified) and !empty($l->fetch_voluntary_abilify) and !empty($l->connect_user->fetch_referral_abilify))
                <td>{{$l->fetch_voluntary_abilify->count() + $l->connect_user->fetch_referral_abilify->count()}}
              @else
                <td>0</td>
              @endif
            @else
              <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_declined))
            <td>{{$l->connect_user->fetch_abilify_declined->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_wrong_number))
            <td>{{$l->connect_user->fetch_abilify_wrong_number->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_noanswer_5))
            <td>{{$l->connect_user->fetch_abilify_noanswer_5->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_noanswer_4))
            <td>{{$l->connect_user->fetch_abilify_noanswer_4->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_noconsent))
            <td>{{$l->connect_user->fetch_abilify_noconsent->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_queue))
            <td>{{$l->connect_user->fetch_abilify_queue->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_abilify_total_unverified_retrieval))
            <td>{{$l->connect_user->fetch_abilify_total_unverified_retrieval->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_new))
            <td>{{$l->connect_user->fetch_aminoleban_new->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_finished))
            <td>{{$l->connect_user->fetch_aminoleban_finished->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_stopped))
            <td>{{$l->connect_user->fetch_aminoleban_stopped->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_switched))
            <td>{{$l->connect_user->fetch_aminoleban_switched->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->fetch_voluntary_aminoleban))
            <td>{{$l->fetch_voluntary_aminoleban->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_referral_aminoleban))
            <td>{{count($l->connect_user->fetch_referral_aminoleban)}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_total_verified) or !empty($l->fetch_voluntary_aminoleban) or !empty($l->connect_user->fetch_referral_aminoleban))
              @if(!empty($l->connect_user->fetch_aminoleban_total_verified) and empty($l->fetch_voluntary_aminoleban) and empty($l->connect_user->fetch_referral_aminoleban))
                <td>{{$l->connect_user->fetch_aminoleban_total_verified->count()}}</td>
              @elseif(!empty($l->connect_user->fetch_aminoleban_total_verified) and !empty($l->fetch_voluntary_aminoleban) and empty($l->connect_user->fetch_referral_aminoleban))
                <td>{{$l->connect_user->fetch_aminoleban_total_verified->count() + $l->fetch_voluntary_aminoleban->count()}}</td>
              @elseif(!empty($l->connect_user->fetch_aminoleban_total_verified) and !empty($l->fetch_voluntary_aminoleban) and !empty($l->connect_user->fetch_referral_aminoleban))
                <td>{{$l->connect_user->fetch_aminoleban_total_verified->count() + $l->fetch_voluntary_aminoleban->count() + $l->connect_user->fetch_referral_aminoleban->count()}}
              @elseif(empty($l->connect_user->fetch_aminoleban_total_verified) and empty($l->fetch_voluntary_aminoleban) and !empty($l->connect_user->fetch_referral_aminoleban))
                <td>{{$l->connect_user->fetch_referral_aminoleban->count()}}</td>
              @elseif(empty($l->connect_user->fetch_aminoleban_total_verified) and !empty($l->fetch_voluntary_aminoleban) and !empty($l->connect_user->fetch_referral_aminoleban))
                <td>{{$l->fetch_voluntary_aminoleban->count() + $l->connect_user->fetch_referral_aminoleban->count()}}
              @else
                <td>0</td>
              @endif
            @else
              <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_declined))
            <td>{{$l->connect_user->fetch_aminoleban_declined->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_wrong_number))
            <td>{{$l->connect_user->fetch_aminoleban_wrong_number->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_noanswer_5))
            <td>{{$l->connect_user->fetch_aminoleban_noanswer_5->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_noanswer_4))
            <td>{{$l->connect_user->fetch_aminoleban_noanswer_4->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_noconsent))
            <td>{{$l->connect_user->fetch_aminoleban_noconsent->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_queue))
            <td>{{$l->connect_user->fetch_aminoleban_queue->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_aminoleban_total_unverified_retrieval))
            <td>{{$l->connect_user->fetch_aminoleban_total_unverified_retrieval->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_new))
            <td>{{$l->connect_user->fetch_pletaal_new->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_finished))
            <td>{{$l->connect_user->fetch_pletaal_finished->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_stopped))
            <td>{{$l->connect_user->fetch_pletaal_stopped->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_switched))
            <td>{{$l->connect_user->fetch_pletaal_switched->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->fetch_voluntary_pletaal))
            <td>{{$l->fetch_voluntary_pletaal->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_referral_pletaal))
            <td>{{count($l->connect_user->fetch_referral_pletaal)}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_total_verified) or !empty($l->fetch_voluntary_pletaal) or !empty($l->connect_user->fetch_referral_pletaal))
              @if(!empty($l->connect_user->fetch_pletaal_total_verified) and empty($l->fetch_voluntary_pletaal) and empty($l->connect_user->fetch_referral_pletaal))
                <td>{{$l->connect_user->fetch_pletaal_total_verified->count()}}</td>
              @elseif(!empty($l->connect_user->fetch_pletaal_total_verified) and !empty($l->fetch_voluntary_pletaal) and empty($l->connect_user->fetch_referral_pletaal))
                <td>{{$l->connect_user->fetch_pletaal_total_verified->count() + $l->fetch_voluntary_pletaal->count()}}</td>
              @elseif(!empty($l->connect_user->fetch_pletaal_total_verified) and !empty($l->fetch_voluntary_pletaal) and !empty($l->connect_user->fetch_referral_pletaal))
                <td>{{$l->connect_user->fetch_pletaal_total_verified->count() + $l->fetch_voluntary_pletaal->count() + $l->connect_user->fetch_referral_pletaal->count()}}
              @elseif(empty($l->connect_user->fetch_pletaal_total_verified) and empty($l->fetch_voluntary_pletaal) and !empty($l->connect_user->fetch_referral_pletaal))
                <td>{{$l->connect_user->fetch_referral_pletaal->count()}}</td>
              @elseif(empty($l->connect_user->fetch_pletaal_total_verified) and !empty($l->fetch_voluntary_pletaal) and !empty($l->connect_user->fetch_referral_pletaal))
                <td>{{$l->fetch_voluntary_pletaal->count() + $l->connect_user->fetch_referral_pletaal->count()}}
              @else
                <td>0</td>
              @endif
            @else
              <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_declined))
            <td>{{$l->connect_user->fetch_pletaal_declined->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_wrong_number))
            <td>{{$l->connect_user->fetch_pletaal_wrong_number->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_noanswer_5))
            <td>{{$l->connect_user->fetch_pletaal_noanswer_5->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_noanswer_4))
            <td>{{$l->connect_user->fetch_pletaal_noanswer_4->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_noconsent))
            <td>{{$l->connect_user->fetch_pletaal_noconsent->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_queue))
            <td>{{$l->connect_user->fetch_pletaal_queue->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_pletaal_total_unverified_retrieval))
            <td>{{$l->connect_user->fetch_pletaal_total_unverified_retrieval->count()}}</td>
            @else
            <td>0</td>
            @endif
            <td></td>
            @if(!empty($l->connect_user->fetch_invalid_blurred))
            <td>{{$l->connect_user->fetch_invalid_blurred->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_invalid_duplicate))
            <td>{{$l->connect_user->fetch_invalid_duplicate->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_invalid_already_enrolled))
            <td>{{$l->connect_user->fetch_invalid_already_enrolled->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_invalid_wrong_upload))
            <td>{{$l->connect_user->fetch_invalid_wrong_upload->count()}}</td>
            @else
            <td>0</td>
            @endif
            @if(!empty($l->connect_user->fetch_invalid_total))
            <td>{{$l->connect_user->fetch_invalid_total->count()}}</td>
            @else
            <td>0</td>
            @endif
          </tr>
          @endforeach
        </tbody>
        </table>
        </div>

    </div>
</div>

@endsection 

@section('footer-scripts')
<script>
function popupwindow(url) {
  
  title = "Images";
  w = 500;
  h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

$(document).ready(function() {


var today = new Date();
var dd = today.getDate();
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();


$(".datatable").DataTable({
  "ordering": false,
  "scrollX": true,
  dom: 'Blfrtip',
  buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: 'MTD Summary Retrieval Report '+yyyy+mm+dd,
            },
            {
              extend: 'csvHtml5',
              filename: 'MTD Summary Retrieval Report '+yyyy+mm+dd,
              customize: function (csv) {
                 return "TEAM,AREA,MR NAME,TOTAL VALID RETRIEVAL,,,,,,,ABILIFY,,,,,,,,,,,,,,AMINOLEBAN,,,,,,,,,,,,,,PLETAAL,,,,,,,,,,,TOTAL INVALID RETRIEVAL,,,\n" + csv;
              }
            },
            'colvis'
        ]
});

});

$(document).ready(function() {

var today = new Date();
var dd = today.getDate();
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();

$('#daily').DataTable({
  "ordering": false,
  "scrollX": true,
  dom: 'Blfrtip',
  buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: 'Daily Call Raw Data Report '+yyyy+mm+dd,
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            'colvis'
        ]
});

});
</script>
@endsection

