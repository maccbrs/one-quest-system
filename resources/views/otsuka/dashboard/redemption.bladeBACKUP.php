@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
    $baseurl = URL::asset('/'); 
   $Helper = new AppHelper;
 
 $skuDescripton =  json_decode($Helper->fetchTable("otsuka\Sku","id","sortBy"),true);  
 
 $sku_array = array();
	foreach($skuDescripton as $key => $val) 
	{
		 $sku_array[$val['id']] = $val['ipc'] ;
	}
	
  ?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}

</style>


<div class="panel panel-default">
  <div class="panel-heading"><strong>RECIEPT INFORMATION</strong><span style="float:right;font-size:15px">UPLOAD NO : <strong>{{!empty($basicinfo->id)? $basicinfo->id :""}}</strong></span></div>
    <div class="panel-body" style="font-size:12px;">
		<table class="table table-bordered col-md-12">
		<thead>
			<tr>
				<th>Patient Name</th>
				<th>Contact Number</th>
				<th>Medicine</th>
				<th>OR NO</th>
				<th>Transaction Date</th>
				<th>Uploaded</th>
				<th>SKU</th>
			</tr>	
			
		</thead>
		<tbody>
			<tr>
				<td>{{!empty($basicinfo->patient_name)? $basicinfo->patient_name :""}}</td>
				<td>{{!empty($basicinfo->mobile_number)? $basicinfo->mobile_number :""}}</td>
				<td>{{!empty($basicinfo->medicine)? $basicinfo->medicine :""}}</td>
				<td></td>
				<td></td>
				<td>{{!empty($basicinfo->id)? $basicinfo->id :""}}{{$basicinfo->uploaded_by}}</td>
				<td></td>
			</tr>
		</tbody>
		</table>
	  
	 
	</div>
</div>


 <div class="row">
		<div class="col-lg-12">
			
				<div class="panel panel-default">
					<div class="panel-heading">
						<label for="searchbox" >Filter :	</label>
						<input type="text" id="searchbox" class="form-control" style="display:inline;width:20%"/>
						<select class="form-control" style="display:inline;width:20%" id="person"><option value="patient">Patient<option value="official_reciept">OR</option></select>
						<button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc($('#searchbox').val(),$('#person').val())"></button>
					</div>
					
					<div class="panel-body" id="filterdisplay" style="overflow: scroll;text-align:center;max-height:400px">
						
						
					</div>
				</div>	
			
			
			
		</div>
	 </div>





<div class="col-md-12">
<div class="panel panel-default">
  <div class="panel-heading"><strong>PATIENT INFORMATIONS</strong><span style="float:right;font-size:15px">Patient Code : <strong>{{($patient_record[0]->patient_code)}}</strong></span></div>
    <div class="panel-body" style="font-size:12px;">

      <div class="row">
        <div class="col-md-3">
          Patient Last Name: <p style="color:green;">{{($patient_record[0]->patient_lastname)}}</p>
        </div>
        <div class="col-md-3">
          Patient First : <p style="color:green;">{{($patient_record[0]->patient_firstname)}}</p>
        </div>
		 <div class="col-md-3">
          Patient Middlen : <p style="color:green;">{{($patient_record[0]->patient_middlename)}}</p>
        </div>
		 <div class="col-md-3" style="border-left: 1px solid #000000">
         <!-- Maritial Status : <p style="color:green;">Single</p> -->
        </div>
      </div>
	  <hr style="margin-top:4px;margin-bottom:10px;"/>
	   <div class="row">
        <div class="col-md-2">
          Birth Date <p style="color:green;">{{($patient_record[0]->patient_middlename)}}</p>
        </div>
        <div class="col-md-2"  style="border-left: 1px solid #c1c1c1">
          Age : <p style="color:green;">{{($patient_record[0]->age)}}</p>
        </div>
		 <div class="col-md-2"  style="border-left: 1px solid #c1c1c1">
          Gender : <p style="color:green;">{{($patient_record[0]->gender)}}</p>
        </div>
		 <div class="col-md-2" style="border-left: 1px solid #c1c1c1">
          Mobile Number : <p style="color:green;">{{($patient_record[0]->mobile_number)}}</p>
        </div>
		 <div class="col-md-2" style="border-left: 1px solid #c1c1c1">
          Mobile Number 2 : <p style="color:green;">{{($patient_record[0]->mobile_number_2)}}</p>
        </div>
		 <div class="col-md-2" style="border-left: 1px solid #c1c1c1">
          Contact : <p style="color:green;">{{($patient_record[0]->phone_number)}}</p>
        </div>
      </div>
	  
	   <hr style="margin-top:5px;margin-bottom:10px;"/>
	   <div class="row">
        <div class="col-md-6">
         Address <p style="color:green;"><?php echo strtoupper($patient_record[0]->address) ;?></p>
        </div>
        <div class="col-md-2"  style="border-left: 1px solid #c1c1c1">
          Date Enrolled : <p style="color:green;"><?php echo strtoupper($patient_record[0]->date_enrolled) ;?></p>
        </div>
		 <div class="col-md-2"  style="border-left: 1px solid #c1c1c1">
          Patient Kit Number : <p style="color:green;"><?php echo strtoupper($patient_record[0]->patient_kit_number) ;?></p>
        </div>				
      </div>
	    <hr style="margin-top:5px;margin-bottom:10px;"/>
	   <div class="row">
        <div class="col-md-6">
         Note's <p style="color:green;"><?php echo strtoupper($patient_record[0]->remarks) ;?></p>
        </div>
       
		 <div class="col-md-2"  style="border-left: 1px solid #c1c1c1;background-color:#008828;color:#ffffff;">
          Active : <p style="">True</p>
        </div>
		
		
      </div>
	  
	  
	

    </div>
</div>
</div>

<div class="col-md-12">
<!--
<div class="panel panel-default">
  <div class="panel-heading"><strong>PATIENT MEDICAL INFORMATIONS</strong></div>
    <div class="panel-body">

	<div class="row">
        <div class="col-md-3" >
          Doctor's Name: <p style="color:green;"><?php echo strtoupper("Reyes") ;?> </p>
        </div>
        <div class="col-md-3"  style="border-left: 1px solid #000000">
          Hospital : <p style="color:green;">MAKATI MED</p>
        </div>
		 <div class="col-md-3"  style="border-left: 1px solid #000000">
          Med Rep : <p style="color:green;">Arcega</p>
        </div>
		 <div class="col-md-3" style="border-left: 1px solid #000000">
           : <p style="color:green;">test</p>
        </div>
      </div>
	  <hr style="margin-top:3px;margin-bottom:10px;"/>
	   <div class="row">
        <div class="col-md-2">
          Product  <p style="color:green;">Abilify</p>
        </div>
        <div class="col-md-2"  style="border-left: 1px solid #c1c1c1">
          # Tabs Prescribe : <p style="color:green;">20</p>
        </div>
		 <div class="col-md-2"  style="border-left: 1px solid #c1c1c1">
		  # Days Precibe : <p style="color:green;">Male</p>
        </div>
		 <div class="col-md-2"  style="border-left: 1px solid #c1c1c1;background-color:#008828;color:#ffffff;">
          Already Purchase : <p style="">Yes</p>
        </div>
		
      </div>


    </div>
</div>
</div> -->

<div class="col-md-12">
<div class="panel panel-default">


    <div class="panel-heading"><strong>REDEMPTION</strong></div>
    <div class="panel-body">

		<div class="row">
			<div class="col-md-12">
			
			
			  <h4>History Purchased </h4><br/>
			  
			  
						<table id="" class="display datatable table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>OR #</th>
									<th>OR Date</th>
									<th>Product</th>
									<th>SKU</th>
									<th>QTY</th>
									
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>OR #</th>
									<th>OR Date</th>
									<th>Product</th>
									<th>SKU</th>
									<th>QTY</th>
									
								</tr>
							</tfoot>
							<tbody>
							
								<?php foreach($Encoded_Purchases as $key => $val){ ?>
								<tr>
									<td>{{$val->or_number}}</td>
									<td>{{$val->or_date}}</td>
									
									<td>{{$val->id}}</td>
									<td>
									<?php foreach($val->fetch_encoded_dtl as $key => $val)
													{
														//echo '( ' . $val->qty . ' ) '  . $val->sku . '<br/>' ;
															echo "(" . $val->qty . ")" ;
															if (array_key_exists($val->sku,$sku_array)) {
																echo $sku_array[$val->sku] . '<br/>';
															}
															else{
															echo "UNKNOW_SKU(" . $val->sku . ")<br/>";
															}
														
													}?>
									
									</td>
									
									<td></td>
									
								</tr>
								<?php } ?>
							   
								
							</tbody>
						</table>
			  

			  
			</div>
			
		</div>
      <hr>
	<div class="row">
		<div class="col-md-12">
		
		<input type="text"/>
		
		</div>
	</div>		
	
	<hr>
	  
	  <div class="row">
		<div class="col-md-12">
			 
			  <input type="text" class="" style="width:60px" id="inputKey" readonly disabled value="99"> No. Of Redeemable  <br/>
			  <input type="text" class="" style="width:60px" id="inputKey" readonly disabled value="99">Redeemable for the Month<br/> 
			  <input type="text" class="" style="width:60px" id="inputKey" readonly disabled value="99">No. Redeemed for the Month <br/>
			  
			  <table>
				
			  </table>
		</div>

	  </div>

    </div>
</div>
</div>


      
      





@endsection 

@section('footer-scripts')
<script>

function loadDoc(filter,person) {
	$('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		$('.datatabledraw').DataTable().draw(); 
        document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchperson?person_filter=" + filter + "&person=" + person, true);
  xhttp.send();
}

$(document).ready(function() {








$(".datatable").DataTable({
  "ordering": false,
});

});

$(document).ready(function() {



$('#verify').DataTable({
  "ordering": false,
});

});
</script>
@endsection

