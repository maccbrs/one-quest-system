@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
	text-align: center;
}

</style>



<div class="col-md-12">
<div class="panel panel-default">


    <div class="panel-heading"><strong> UPLOADS FOR THE DAY</strong></div>
    <div class="panel-body">

		<div class="row">
			<div class="col-md-12">
				<form method="POST" action="{{ route('otsuka.post.uploads') }}">
					{{ csrf_field() }}
					<label>Select Date: </label>
					<input id="filter_date" class="form-group" type="text" name="filter_date">
					<button type="submit" class="btn btn-sm btn-success">Apply</button>
					{{-- <label>Daterange: </label>
					<input id="daterange" class="form-group" type="text" name="daterange">
					<button type="submit" class="btn btn-sm btn-success">Apply</button> --}}
				</form>
			</div>

			<div class="col-md-12">
						<table id="table_id" class="display datatable compact table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Date/Time</th>
<!-- 									<th>Filename</th> -->
									<th>Type</th>
									<th>Status</th>
									<th>Remarks</th>
									<th>Px Name</th>
									<th>Contact Number</th>
									<th>Image</th>
									<th>Action</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Date/Time</th>
<!-- 									<th>Filename</th> -->
									<th>Type</th>
									<th>Status</th>
									<th>Remarks</th>
									<th>Px Name</th>
									<th>Contact Number</th>
									<th>Image</th>
									<th>Action</th>
								</tr>
							</tfoot>
							<tbody>
								@foreach($myUpload as $d)
										<tr>
											<td>
												{{ $d->created_at }}
											</td>
											<td> @if($d->type == 'Enroll') Retrieval @else {{ $d->type }} @endif</td>
											<td>
											@if($d->claimed == 1 and $d->submit == 0 and $d->trail_status !== 199)In Process
											@elseif($d->claimed == 0 and $d->submit == 0 and $d->trail_status !== 199) On queue
											@elseif($d->claimed == 1 and $d->submit == 1 and $d->trail_status !== 199) Encoded
											@elseif($d->claimed == 1 and $d->submit == 1 and $d->trail_status == 199) Invalid - {{ $d->remarks }}
											@endif
											</td>
											<td>{{!empty($d->upload_remarks) ? $d->upload_remarks : '' }}</td>
											<td>{{!empty($d->patient_name) ? $d->patient_name : 'Not Given' }}</td>
											<td><b>Mobile No.: </b>{{!empty($d->mobile_number) ? $d->mobile_number : 'N/A'}}<br>
												<b>Landline No.: </b>{{!empty($d->phone_no) ? $d->phone_no : 'N/A'}}
											</td>
											<td nowrap>
												<a href="#" onclick="popupwindow('{{$pic.'/'.$d->filename}}');"><img src="{{$pic.'/'.$d->filename}}" class="img-responsive img-thumbnail" style="max-height: 120px "/></a>

											</td>
											<td>@if($d->trail_status == '100') REMOVE @endif</td>

										</tr>
								@endforeach
							</tbody>
						</table>
			</div>
			
		</div>
      <hr>
	


    </div>
</div>
</div>


@endsection 

@section('footer-scripts')
<script>
function popupwindow(url) {
	
	title = "Images";
	w = 500;
	h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

$(document).ready(function() {


$('#filter_date').datepicker();
$('#table_id').DataTable();
$('input[name="daterange"]').daterangepicker();


});

// $(document).ready(function() {



// $('#verify').DataTable({
//   "ordering": false,
// });

// });
</script>
@endsection

