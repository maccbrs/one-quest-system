@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
    <?php $baseurl = URL::asset('/'); ?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
.viber {
	color: 	white !important;
	
	background-color: #8f5db7 !important
}

.red {
	color: white !important;
	
	background-color: #CD5C5C!important
}
.yellow {
	color: black !important;
	background-color: yellow !important;
}
.orange {
	color: white !important;
	background-color: orange !important;
}
.default-color {
	color:#000000;
}

  input {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
  }

 input.noborder {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 0;
  }

  div.border {
      border: 1px solid black;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
  }

  div.padding {
      padding-top: 40px;
      padding-right: 65px;
      padding-bottom: 40px;
      padding-left: 60px;
  }

  h2.space {
    line-height: .25;
  }

  p.ind {
    text-indent: 50px;
    line-height: .1;
  }


</style>

<div class="panel panel-default">
  <div class="panel-heading"><b>Compliance Referral Voluntary Queue</b></div>
  <div class="panel-body">

      <div class="col-md-12">
			<form method="get" id="encode_form" action="{{route('otsuka.compliance-ref-voluntary')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
			<table id="compliance-queue" class="display compact datatable table-bordered table-hover" cellspacing="0" width="100%" style="font-size:10px;color:#000000">
					<thead>
						<tr>
							<th>Px Code</th>
							<th>Px Kit Number</th>
							<th>Px Name</th>
							<th>Date Created</th>
							<th>Encode</th>
						</tr>
					</thead>
					<tbody>
					@foreach($Data as $key => $val)
					<tr>
					  	<td>{{!empty($val->patient_code)?$val->patient_code: ''}}</td>
					  	<td>{{!empty($val->patient_kit_number)?$val->patient_kit_number: ''}}</td>
					  	<td>{{!empty($val->patient_lastname)?$val->patient_lastname: ''}}, {{!empty($val->patient_firstname)?$val->patient_firstname: ''}}</td>
					  	<td>{{!empty($val->created_at)?$val->created_at: ''}}</td>
					  	<td><a href="{{$baseurl}}otsuka/compliance/compliance-ref-voluntary-encode?patient_id={{$val->id}}" class="btn btn-sm btn-info"> Call </a>
						
					  	{{-- <td><button type="submit" name="encode" value="" class="btn btn-xs btn-primary">Callback</button><input type="hidden" name="px_kit" value=""></td> --}}
					  	
					  	
					  </tr>
					
					
					@endforeach 
					
					<?php /* @foreach($Queue_Compliance as $qc)
					  <tr>
					  	<td>{{!empty($qc->fetch_cmid->fetch_patient->patient_code)?$qc->fetch_cmid->fetch_patient->patient_code: ''}}</td>
					  	<td>{{!empty($qc->fetch_cmid->fetch_patient->patient_fullname)?$qc->fetch_cmid->fetch_patient->patient_fullname: ''}}</td>
					  	<td>{{!empty($qc->updated_at)?$qc->updated_at: ''}}</td>
					  	<td>
					  		<?php 
					  		$compliance_details = new \App\Http\Models\otsuka\Compliance;
					  		$compliance_cmid = $compliance_details
					  		->where('patientid',$qc->fetch_cmid->patientid)
					  		->where('created_at','like',date('Y-m-d', strtotime($qc->updated_at)).'%')->first();

					  		$compliance_reason = new \App\Http\Models\otsuka\Compliance_reason;
					  		$final_status = $compliance_reason->where('id',$compliance_cmid['reason'])->first();
					  		?>
					  		{{$final_status['name']}}
					  	</td>
					  	<td>{{!empty($qc->callnotes)?$qc->callnotes: ''}}</td>
					  	<td><button type="submit" name="encode" value="{{!empty($qc->cmid)?$qc->cmid: ''}}" class="btn btn-xs btn-primary">Callback</button><input type="hidden" name="px_kit" value=""></td>
					  </tr>
					@endforeach 
					*/ ?>
					</tbody>
			</table>
				</form>
		</div>
	</div>
</div> <!-- ENCODE QUEUEING -->	

<div class="panel panel-default">
  <div class="panel-heading"><b>Compliance Queue</b></div>
  <div class="panel-body">
  {{--	Tomorrow Count - <b>{{$tom_count}}</b><br>
  	Day After Tomorrow Count - <b>{{$day_after_count}}</b><br>
  	Total Compliance Count - <b>{{$total_count}}</b>
  --}}
  </div>
</div>
</div>			

@endsection 

@section('footer-scripts')
<script>


	function processRedemption(upload_id) {
   var str = "";
				 $(".upload_id_checkbox:checked").each(function() {
					str += $(this).val() + ",";
			  });
			  str = str.substr(0, str.length - 1);
			  alert(str);
				
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id=" + str);
		
		//var xhttp = new XMLHttpRequest();
		//xhttp.onreadystatechange = function() {
		//if (this.readyState == 4 && this.status == 200) {	
		
		//document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		//alert('Success');
		//document.getElementsByTagName("body").innerHTML = xhttp.responseText;
		//}
		//};
		//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id, true);
		//xhttp.send();					 
		}





function hideupload(id) {
	//alert(id);
 	var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/agent");
		//$('#upload_queue_id_' + id).remove();
		//$(".upload-datatable").DataTable();
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/disable?active=" + id , true);

  xhttp.send();	 
    
}



$(document).ready(function() {

/*            setTimeout(function() {
                document.location.reload(true);
            }, 3500);*/

$(".datatable").DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
});

});
</script>
@endsection

