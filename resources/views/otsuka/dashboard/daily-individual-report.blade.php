@extends('otsuka.blank')

@section('Individual Report')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>

<div class="col-md-12">	

	<div class="row">			
		<div class="col-md-12">
		Dear {{mrname}},
		</div>
	</div>

	<div class="row">			
		<div class="col-md-12">
		Sending you the summary of the retrievals today, {{date}}
		</div>
	</div>

	<div class="row">			
		<div class="col-md-12">
			<table class="nowrap compact">
				<thead>
					<th>Px Kit Number</th>
					<th>MD Name</th>
					<th>Remarks</th>
				</thead>
				<tbody>
					@foreach()
					<td>{{}}</td>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="row">			
		<div class="col-md-12">
			<br/>
			<br/>
			Your Daily No. of Valid Retrievals is {{daily_no}}
		</div>
	</div>

	<div class="row">			
		<div class="col-md-12">
			Your Daily No. of Invalid Retrievals is {{daily_invalid}}
		</div>
	</div>

	<div class="row">			
		<div class="col-md-12">
			<br/>
			<br/>
			Your MTD No. of Retrievals is {{total_no}}.
		</div>
	</div>

	<div class="row">			
		<div class="col-md-12">
			<br/>
			<br/>
			For inquiries, please fill-up a report form at the link provided below:
			<br/>
			<a href="http://oqs.onequest.com.ph:8080/index.php">http://oqs.onequest.com.ph:8080/index.php</a>
		</div>
	</div>

	<div class="row">			
		<div class="col-md-12">
			<br/>
			<br/>
			Thank you.
			<br/>
			Magellan Solutions Outsourcing Inc.
		</div>
	</div>

</div>

@endsection 

@section('footer-scripts')



@endsection

