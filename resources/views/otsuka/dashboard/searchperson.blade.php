<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type: application/json');

 $sku_array = array();
	$sku_array['1'] = 'ABILIFY 10 mg ODT tablet';
	$sku_array['2'] = 'ABILIFY 10 mg tablet';
	$sku_array['3'] = 'ABILIFY 15 mg ODT tablet';
	$sku_array['4'] = 'ABILIFY 15 mg tablet';
	$sku_array['5'] = 'ABILIFY 5 mg tablet';
	$sku_array['6'] = 'ABILIFY Discmelt 10 mg Compliance Pack';
	$sku_array['7'] = 'ABILIFY Discmelt 15 mg Compliance Pack';
	$sku_array['8'] = 'ABILIFY Oral Solution 1mg/ml 150ml';
	$sku_array['9'] = 'AMINOLEBAN COMPLIANCE PACK (3+1)';
	$sku_array['10'] = 'AMINOLEBAN INJ 500mL';
	$sku_array['11'] = 'AMINOLEBAN ORAL 50g';
	$sku_array['12'] = 'LORELCO 250mg tablets';
	$sku_array['13'] = 'MEPTIN 25 mcg tablet';
	$sku_array['14'] = 'MEPTIN 50 mcg tablet';
	$sku_array['15'] = 'MEPTIN SWINGHALER 10mcg/puff';
	$sku_array['16'] = 'MEPTIN SYRUP 60mL';
	$sku_array['17'] = 'MIKELAN 5mg tablet';
	$sku_array['18'] = 'MUCOSTA 100mg Tab';
	$sku_array['19'] = 'OBUCORT SWINGHALER 200mcg/puff';
	$sku_array['20'] = 'PLETAAL 100 mg tablet';
	$sku_array['21'] = 'PLETAAL 50 mg tablet';
	$sku_array['22'] = 'PLETAAL Powder 100mg .5grams';
	$sku_array['23'] = 'PLETAAL Powder 50mg .25grams';
	$sku_array['24'] = 'Pletaal tablet 100mg Compliance pack';
	$sku_array['25'] = 'SAMSCA 15mg';
	$sku_array['26'] = 'UKNWN PROD';
	$sku_array['27'] = 'SAMSCA-Urinal Male';
	$sku_array['28'] = 'Envelop';
	$sku_array['29'] = 'Sticker Paper';
	$sku_array['30'] = 'SAMSCA-Leaflet';
	$sku_array['31'] = 'Permanent Card';
	$sku_array['32'] = 'Welcome Letter';
	$sku_array['33'] = 'SAMSCA-Urinal Female';
	$sku_array['34'] = 'Newsletter 1Q';
	$sku_array['35'] = 'Newsletter 2Q';
	$sku_array['36'] = 'Newsletter 3Q';
	$sku_array['37'] = 'Newsletter 4Q';
	
   $pic = URL::asset('/uploads/');
   $base = URL::asset('/');
?>
		<?php if (isset($searching) && (strtolower($searching) == 'doctors')) {?>          
				
							   <table id="example2" class="datatabledraw table table-bordered table-hover table-responsive" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Use</th>
											<th>MR ID </th>
											<th>Area Code </th>
											<th>MR </th>
											<th>Doctor ID</th>
											<th>MD Name</th>
											<th>Specialty</th>
											<th>Hospital</th>
											<th>Product</th>
										</tr>
									</thead>
									 <tbody>
										  @foreach($doctors as $m)
										  <tr>
												<td><button class="btn-xs btn-info" 
												onclick="useMD({{$m->id}})"
												data-dismiss="modal">use</button></td>
												<td>{{$m->empcode}}</td>
												<td>{{$m->areacode}}</td>
												<td>{{$m->mrname}}</td>
												<td>{{$m->doctorid}}</td>
												<td>{{$m->mdname}}</td>
												<td>{{$m->specialty}}</td>
												<td>{{$m->hospital}}</td>
												<td>{{$m->product}}</td>
												
										  </tr> 
										  @endforeach
										</tbody>			
							</table>
						<?php }	?>


						<?php if (isset($searching) && (strtolower($searching) == 'globalmd')) {?>          
				
							   <table id="example2" class="datatabledraw table table-bordered table-hover table-responsive" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Use</th>
											<th>MR ID </th>
											<th>Area Code </th>
											<th>MR </th>
											<th>Doctor ID</th>
											<th>MD Name</th>
											<th>Specialty</th>
											<th>Hospital</th>
											<th>Product</th>
											
										</tr>
									</thead>
									 <tbody>
										  @foreach($globalmd as $gm)
										  <tr>
												<td><button class="btn-xs btn-info" 
												onclick="$('#md_name').val('{{$gm->mdname}}');$('#hospital').val('{{$gm->hospital}}');$('#doctor_id').val('{{$gm->doctorid}}')" data-dismiss="modal">use</button></td>
												<td>{{$gm->empcode}}</td>
												<td>{{$gm->areacode}}</td>
												<td>{{$gm->mrname}}</td>
												<td>{{$gm->doctorid}}</td>
												<td>{{$gm->mdname}}</td>
												<td>{{$gm->specialty}}</td>
												<td>{{$gm->hospital}}</td>
												<td>{{$gm->product}}</td>		
										  </tr> 
										  @endforeach
										</tbody>		
							</table>
						<?php }	?>


						<?php if (isset($searching) && (strtolower($searching) == 'filteredmd')) {?>          
				
							   <table id="example2" class="datatabledraw table table-bordered table-hover table-responsive" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Use</th>
											<th>MR ID </th>
											<th>Area Code </th>
											<th>MR </th>
											<th>Doctor ID</th>
											<th>MD Name</th>
											<th>Specialty</th>
											<th>Hospital</th>
											<th>Product</th>
											
										</tr>
									</thead>
									 <tbody>
										  @foreach($filteredmd as $fm)
										  <tr>
												<td><button class="btn-xs btn-info" 
												onclick="$('#md_name').val('{{$fm->mdname}}');$('#hospital').val('{{$fm->hospital}}');$('#doctor_id').val('{{$fm->doctorid}}')" data-dismiss="modal">use</button></td>
												<td>{{$gm->empcode}}</td>
												<td>{{$gm->areacode}}</td>
												<td>{{$gm->mrname}}</td>
												<td>{{$gm->doctorid}}</td>
												<td>{{$gm->mdname}}</td>
												<td>{{$gm->specialty}}</td>
												<td>{{$gm->hospital}}</td>
												<td>{{$gm->product}}</td>	
												
										  </tr> 
										  @endforeach
										</tbody>			
							</table>
						<?php }	?>
						
						<?php if (isset($searching) && (strtolower($searching) == 'globalmdv2')) {?>          
				
							   <table id="example2" class="display" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Use</th>
											<th>MR ID </th>
											<th>Area Code </th>
											<th>MR </th>
											<th>Doctor ID</th>
											<th>MD Name</th>
											<th>Specialty</th>
											<th>Hospital</th>
											<th>Product</th>
										</tr>
									</thead>
									 <tbody>
										  @foreach($globalmd as $gm)
										  <tr>
												<td><button class="btn-xs btn-info" 
												onclick="useMD({{$gm->id}})">use</button></td>
												<td>{{$gm->empcode}}</td>
												<td>{{$gm->areacode}}</td>
												<td>{{$gm->mrname}}</td>	
												<td>{{$gm->doctorid}}</td>
												<td>{{$gm->mdname}}</td>
												<td>{{$gm->specialty}}</td>
												<td>{{$gm->hospital}}</td>
												<td>{{$gm->product}}</td>
												
										  </tr> 
										  @endforeach
										</tbody>			
							</table>
						<?php }	?>

						<?php if (isset($searching) && (strtolower($searching) == 'globalmdv2queue')) {?>          
				
							   <table id="example3" class="display" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Use</th>
											<th>MR ID </th>
											<th>Area Code </th>
											<th>MR </th>
											<th>Doctor ID</th>
											<th>MD Name</th>
											<th>Specialty</th>
											<th>Hospital</th>
											<th>Product</th>
										</tr>
									</thead>
									 <tbody>
										  @foreach($globalmd as $gm)
										  <tr>
												<td><button class="btn-xs btn-info" 
												onclick="useMD({{$gm->id}})">use</button></td>
												<td>{{$gm->empcode}}</td>
												<td>{{$gm->areacode}}</td>
												<td>{{$gm->mrname}}</td>	
												<td>{{$gm->doctorid}}</td>
												<td>{{$gm->mdname}}</td>
												<td>{{$gm->specialty}}</td>
												<td>{{$gm->hospital}}</td>
												<td>{{$gm->product}}</td>
												
										  </tr> 
										  @endforeach
										</tbody>			
							</table>
						<?php }	?>

						<?php if (isset($searching) && (strtolower($searching) == 'filteredmdv2')) {?>          
				
							   <table id="example2" class="datatabledraw table table-bordered table-hover table-responsive" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Use</th>
											<th>MR ID </th>
											<th>Area Code </th>
											<th>MR </th>
											<th>Doctor ID</th>
											<th>MD Name</th>
											<th>Specialty</th>
											<th>Hospital</th>
											<th>Product</th>
										</tr>
									</thead>
									 <tbody>
										  @foreach($filteredmd as $fm)
										  <tr>
												<td><button class="btn-xs btn-info" 
												<?php /*onclick="$('#md_name').val('{{$fm->mdname}}');$('#hospital').val('{{$fm->hospital}}');$('#doctor_id').val('{{$fm->doctorid}}')"*/?>
												onclick="useMD({{$fm->id}})"
												data-dismiss="modal">use</button></td>
												<td>{{$fm->empcode}}</td>
												<td>{{$fm->areacode}}</td>
												<td>{{$fm->mrname}}</td>
												<td>{{$fm->doctorid}}</td>
												<td>{{$fm->mdname}}</td>
												<td>{{$fm->specialty}}</td>
												<td>{{$fm->hospital}}</td>
												<td>{{$fm->product}}</td>
												
										  </tr> 
										  @endforeach
										</tbody>		
							</table>
						<?php }	?>


				<?php if (isset($searching) && (strtolower($searching) == 'patient')) {?>			
							
          
				
							   <table id="example2" class="datatabledraw table table-bordered table-hover table-responsive"  width="100%">
									<thead>
										<tr>
											<th>Action</th>
											<th>Kit No.</th>
											<th>Patient Code</th>
											<th>LastName</th>
											<th>FirstName</th>
											<th>MiddleName</th>
											<th>NickName</th>
											<th>Contacts</th>
											<th>Birth Date</th>
											<th>Gender</th>
											<th>Address</th>
											
										</tr>
									</thead>
									 <tbody>
										  @foreach($Queue as $patient)
										  <tr>
												<td><button class="btn-xs btn-info" 
												onclick="alert('test');$('#patient_fullname').val('{{$patient->patient_fullname}}');
													$('#patient_code').val('{{$patient->patient_code}});
													$('#mobile_number').val('{{$patient->mobile_number}}');
													$('#mobile_number_2').val('{{$patient->mobile_number_2}}');
													$('#phone_number').val('{{$patient->phone_number}});
													$('#time_to_call').val('{{$patient->time_to_call}});
													$('#patient_consent').prop('checked', '{{$patient->patient_consent}}');
													$('#enrollment_date').val('{{$patient->time_to_call}});
													$('#doctor_name').val('{{$patient->doctor_name}});
													$('#doctor_consent').prop( 'checked', '{{$patient->doctor_consent}}');">use</button></td>
												<td nowrap>{{$patient->patient_kit_number}}</td>
												<td nowrap>{{$patient->patient_code}}</td>
												<td nowrap>{{$patient->patient_lastname}}</td>
												<td nowrap>{{$patient->patient_firstname}}</td>
												<td nowrap>{{$patient->patient_middlename}}</td>
												<td nowrap>{{$patient->nickname}}</td>
												<td nowrap>{{$patient->mobile_number}} / {{$patient->mobile_number_2}} / {{$patient->phone_number}}</td>
												<td nowrap>{{$patient->birth_date}}</td>
												<td nowrap>{{$patient->gender}}</td>
												<td nowrap>{{$patient->address}}</td>
												
										  </tr>
										  @endforeach
										</tbody>
									  
								
							</table>		

					<?php }	?>

					<?php if (isset($searching) && (strtolower($searching) == 'queue')) {?>			
							
				
							   <table id="example2" class="datatabledraw table table-bordered table-hover table-responsive"  width="100%">
									<thead>
										<tr>
											<th>Action</th>
											<th>Patient Code</th>
											<th>Kit No.</th>
											<th>Patient Name</th>
											<th>Mobile Number</th>
											<th>Mobile Number 2</th>
											<th>Landline</th>
											<th>Enrollment Date</th>
											<th>Doctor Name</th>	
											<th>Hospital</th>										
										</tr>
									</thead>
									 <tbody>
										  @foreach($Queue as $patient)
										  <tr>
												<td><button class="btn-xs btn-primary" 
												onclick="useMD2({{$patient->id}})"
												data-dismiss="modal" data-toggle="modal" data-target="#upload">UPDATE</button></td>
												<td>{{'OQPXD'.sprintf('%07d',$patient->id)}}</td>
												<td>{{!empty($patient->patient_kit_number)?$patient->patient_kit_number: ''}}</td>
												<td>{{!empty($patient->patient_name)?$patient->patient_name: ''}}</td>
												<td>{{!empty($patient->mobile_number)?$patient->mobile_number: ''}}</td>
												<td>{{!empty($patient->mobile_number_2)?$patient->mobile_number_2: ''}}</td>
												<td>{{!empty($patient->phone_no)?$patient->phone_no: ''}}</td>
												<td>{{!empty($patient->enrollment_date)?$patient->enrollment_date: ''}}</td>
												<td>{{!empty($patient->doctor_name)?$patient->doctor_name: ''}}</td>
												<td>{{!empty($patient->hospital_name)?$patient->hospital_name: ''}}</td>
										  </tr>
										  @endforeach
										</tbody>
							</table>		

					<?php }	?>

					<?php if (isset($searching) && (strtolower($searching) == 'upload')) {?>			
							
          
				
							   <table id="example2" class="datatabledraw table table-bordered table-hover table-responsive"  width="100%">
									<thead>
										<tr>
											<th>id</th>
											<th>Kit No.</th>
											<th>Name</th>
											<th>Contacts</th>
											<th>Encoded by</th>
											<th>Encode Status</th>
											<th>Verification Claimed by</th>
											<th>Verification Status</th>
											
										</tr>
									</thead>
									 <tbody>
										  @foreach($Queue as $patient)
										  <tr>
										  		<td nowrap>{{$patient->id}}</td>
												<td nowrap>{{$patient->patient_kit_number}}</td>
												<td nowrap>{{$patient->patient_name}}</td>
												<td nowrap>{{$patient->mobile_number}} / {{$patient->mobile_number_2}} / {{$patient->phone_no}}</td>
												<td nowrap>{{!empty($patient->encoded_by->name)?$patient->encoded_by->name: 'N/A'}}</td>
												<td nowrap>{{!empty($patient->remarks)?$patient->remarks: 'Encoded'}}
												<td nowrap>{{!empty($patient->verification_claimed_by->name)?$patient->verification_claimed_by->name: 'N/A'}}</td>
												@if($patient->trail_status == 199)
												<td nowrap>Invalid</td>
												@else
												<td nowrap>{{!empty($patient->fetch_status->name)?$patient->fetch_status->name: ''}}</td>
												@endif										
										  </tr>
										  @endforeach
										</tbody>
									  
							
							</table>		

					<?php }	?>
					
					<?php if (isset($searching) && (strtolower($searching) == 'official_reciept')) {?>			
							
          
				
							   <table id="example2" class="datatabledraw table table-bordered table-hover table-responsive"  width="100%">
									<thead>
										<tr>
											<th>#</th>
											<th>Sys Gen #</th>
											<th>Or NUmber</th>
											<th>Or Date</th>
											<th>Patient_ID</th>
											<th>List</th>
											<th>Images</th>
											
											
											
										</tr>
									</thead>
									 <tbody>
									 
										  @foreach($Queue as $Or_info)
										  <tr>
												<td><button class="btn-xs btn-info" 
												onclick="
												$('#or_no').val('{{$Or_info->or_number}}');
												$('#or_date').val('{{$Or_info->or_date}}');
												$('#doctor_name').val('test');
												
												$('#hospital_name').val('test');">use</button></td>
												<td nowrap>{{$Or_info->id}}</td>
												<td nowrap>{{$Or_info->or_number}}</td>
												<td nowrap>{{$Or_info->or_date}}</td>
												<td nowrap><a href="{{$base}}otsuka/redemption?action=redemption&upload_id=&patient_id={{$Or_info->patient_id}}">{{$Or_info->patient_id}}</a></td>
												<td >
													
													<?php foreach($Or_info->fetch_encoded_dtl as $key => $val)
													{
														//echo '( ' . $val->qty . ' ) '  . $val->sku . '<br/>' ;
															echo "(" . $val->qty . ")" ;
															if (array_key_exists($val->sku,$sku_array)) {
																echo $sku_array[$val->sku] . '<br/>';
															}
															else{
															echo "UNKNOW_SKU(" . $val->sku . ")<br/>";
															}
														
													}?>
											
												
												</td>
												<td nowrap><a href="#" onclick="popupwindow('{{$pic . '/' . $Or_info->file_path}}');">receipt Image</a></td>
												
												
										  </tr>
										  @endforeach
										</tbody>
									  
								
							</table>		

					<?php }	?>


<?php if (isset($searching) && (strtolower($searching) == 'upload_queue')) {?>			

			<form method="get" id="encode_form" action="{{route('otsuka.encode')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
			<table id="queue" class="display compact table-bordered datatabledraw table-hover" cellspacing="0" width="100%" style="font-size:10px">
					<thead>
						<tr>
							<th>ID</th>
							<th>Type</th>
							<th>Medicine</th>
							<th>Created At</th>
							<th>Patient Name</th>
							<th>Uploaded By</th>
							<th>Claimed</th>
							@if(Auth::user()->user_level == 2)<th>Status</th>@endif
							<th>Encode</th>
							@if(Auth::user()->user_level == 2)<th>Hide</th>@endif
						</tr>
					</thead>
					<tbody>
					  @if($Queue->count())
					  @foreach($Queue as $qvc)
					  @if($qvc->created_at->addHour(1) <= $tn and $qvc->viber_queue == '0')
					  <tr class="red" id="upload_queue_id_{{$qvc->id}}">
					   @elseif($qvc->created_at->addMinutes(45) <= $tn and $qvc->viber_queue == '0')
					  <tr class="orange" id="upload_queue_id_{{$qvc->id}}">	
					   @elseif($qvc->created_at->addMinutes(30) <= $tn and $qvc->viber_queue == '0')
					  <tr class="yellow" id="upload_queue_id_{{$qvc->id}}">
					  @elseif($qvc->viber_queue == '1')
					  <tr class="viber" id="upload_queue_id_{{$qvc->id}}">
					  @elseif($qvc->created_at->addMinutes(30) > $tn and $qvc->viber_queue == '0')
					  <tr class="default-color" id="upload_queue_id_{{$qvc->id}}">
					  @endif
					  <td>{{$qvc->id}}</td>
					  <td>{{$qvc->type}}</td>
					  <td>{{$qvc->medicine}}</td>
					  <td>{{$qvc->created_at}}</td>
					  <td>{{$qvc->patient_name}}</td>
					  <td>{{!empty($qvc->user->name)?$qvc->user->name: 'N/A'}}</td>
					  @if($qvc->claimed == 0)<td><b>N/A</b></td>@endif
					  @if($qvc->claimed == 1)<td><b>{{!empty($qvc->encoded_by->name)?$qvc->encoded_by->name: 'N/A'}}</b></td>@endif
					  @if(Auth::user()->user_level == 2)
					  <td>							
							{{!empty($qvc->fetch_trail_status->display_value)?$qvc->fetch_trail_status->display_value: ''}}
					  </td>
					  @endif	
					  <td><button type="submit" name="encode" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Encode</button></td>
					  @if(Auth::user()->user_level == 2)<td><a href="#" onclick="hideupload('{{$qvc->id}}')"  value="{{$qvc->id}}" class="btn btn-xs btn-primary">Hide</a></td>@endif
					  </tr>
					  @endforeach
					  @endif
					</tbody>
			</table>
				</form>
<?php }	?>	

<?php if (isset($searching) && (strtolower($searching) == 'verification_queue')) {?>	

			<form method="get" id="encode_form" action="{{route('otsuka.verify')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
			<table id="verify_queue" class="display compact datatable table-bordered table-hover" cellspacing="0" width="100%" style="font-size:10px">
					<thead>
						<tr>
							<th>Px Kit #</th>
							<th>Product</th>
							<th>Encoded by</th>
							<th>Claimed by</th>
							<th>Upload Date</th>
<!-- 							<th># of Calls</th>
							<th>Last Call Date</th>
							<th>Status</th>
							<th>Remarks</th> -->
							<th>Encode</th>
						</tr>
					</thead>
					<tbody>
					@if(!empty($Queue))
					  @foreach($Queue as $q)	
					  @if($q->created_at->addHour(1) <= $tn)				  
					  <tr class="red">
					  @elseif($q->created_at->addMinutes(45) <= $tn)
					  <tr class="orange">
					  @elseif($q->created_at->addMinutes(30) <= $tn)
					  <tr class="yellow">
					  @elseif($q->created_at->addMinutes(30) > $tn)
					  <tr class="default-color">
					  @endif
					  <td>{{!empty($q->patient_kit_number)?$q->patient_kit_number: 'Not Given'}}</td>
					  <td>{{!empty($q->medicine)?$q->medicine: 'N/A'}}</td>
					  <td>{{!empty($q->encoded_by->name)?$q->encoded_by->name: 'N/A'}}</td>
					  <td>{{!empty($q->verification_claimed_by->name)?$q->verification_claimed_by->name: 'N/A'}}</td>
					  <td>{{!empty($q->created_at)?$q->created_at: 'N/A'}}</td>
<!-- 					  <td>{{!empty($q->fetch_call->fetch_verification->attempt)?$q->fetch_call->fetch_verification->attempt: '0'}}</td>
					  <td>{{!empty($q->fetch_call->fetch_verification->created_at)?$q->fetch_call->fetch_verification->created_at: 'Not Yet Called'}}</td>
					  <td>{{!empty($q->fetch_call->fetch_verification->fetch_name->name)?$q->fetch_call->fetch_verification->remarks: 'Not Yet Called'}}</td>
					  <td>{{!empty($q->fetch_call->fetch_verification->callnotes)?$q->fetch_call->fetch_verification->callnotes: 'Not Yet Called'}}</td> -->
					  <td><button type="submit" name="encode" value="{{$q->id}}" class="btn btn-xs btn-primary">Verify</button><input type="hidden" name="px_kit" value="{{$q->patient_kit_number}}"></td>
					  </tr>	  
					  @endforeach
					 @endif
					</tbody>
			</table>
				</form>
<?php }	?>	

<?php if (isset($searching) && (strtolower($searching) == 'verification_callback')) {?>	

<form method="get" id="encode_form" action="{{route('otsuka.verify')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
			<table id="verify_callback" class="display compact table-bordered table-hover" cellspacing="0" width="100%" style="font-size:10px">
					<thead>
						<tr>
							<th>Px Kit #</th>
							<th>Product</th>
							<th>Encoded by</th>
							<th>Upload Date</th>
							<th>Claimed by</th>
							<th># of Calls</th>
							<th>Last Called by</th>
							<th>Last Call Date</th>
							<th>Status</th>
							<th>Remarks</th>
							<th>Encode</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Px Kit #</th>
							<th>Product</th>
							<th>Encoded by</th>
							<th>Upload Date</th>
							<th>Claimed by</th>
							<th># of Calls</th>
							<th>Last Called by</th>
							<th>Last Call Date</th>
							<th>Status</th>
							<th>Remarks</th>
							<th>Encode</th>
						</tr>
					</tfoot>
					<tbody>
					@if(!empty($Queue))
					  @foreach($Queue as $qvc)	
					  @if($qvc->created_at->addHour(1) <= $tn)				  
					  <tr class="red">
					  @elseif($qvc->created_at->addMinutes(45) <= $tn)
					  <tr class="orange">
					  @elseif($qvc->created_at->addMinutes(30) <= $tn)
					  <tr class="yellow">
					  @elseif($qvc->created_at->addMinutes(30) > $tn)
					  <tr class="default-color">
					  @endif
					  <td>{{!empty($qvc->patient_kit_number)?$qvc->patient_kit_number: 'Not Given'}}</td>
					  <td>{{!empty($qvc->medicine)?$qvc->medicine: 'N/A'}}</td>
					  <td>{{!empty($qvc->encoded_by->name)?$qvc->encoded_by->name: 'N/A'}}</td>
					  <td>{{!empty($qvc->created_at)?$qvc->created_at: 'N/A'}}</td>
					  <td>{{!empty($qvc->verification_claimed_by->name)?$qvc->verification_claimed_by->name: 'N/A'}}</td>
					  <td>{{!empty($qvc->fetch_call_queue->fetch_verification_queue->attempt)?$qvc->fetch_call_queue->fetch_verification_queue->attempt: '0'}}
					  <td>{{!empty($qvc->fetch_call_queue->fetch_verification_queue->fetch_name->name)?$qvc->fetch_call_queue->fetch_verification_queue->fetch_name->name: 'Not Yet Called'}}</td>
					  <td>{{!empty($qvc->updated_at)?$qvc->updated_at: 'Not Yet Called'}}</td>
					  <td>{{!empty($qvc->fetch_call_queue->fetch_verification_queue->fetch_status->name)?$qvc->fetch_call_queue->fetch_verification_queue->fetch_status->name: 'Not Yet Called'}}</td>
					  <td>{{!empty($qvc->fetch_call_queue->fetch_verification_queue->callnotes)?$qvc->fetch_call_queue->fetch_verification_queue->callnotes: 'Not Yet Called'}}</td>
					  <td><button type="submit" name="encode" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Callback</button><input type="hidden" name="px_kit" value="{{$qvc->patient_kit_number}}"></td>
					  </tr>	  
					  @endforeach
					 @endif
					</tbody>
			</table>
				</form>

<div class="row">
<div class="col-md-12">
<div class="panel panel-default">
  <div class="panel-heading"><b>Total Callback Queue</b></div>
  <div class="panel-body">
  	Callback Count - <b>{{$count}}</b>
  </div>
</div>
</div>
</div>
<?php }	?>	

      <script>
	 
/*		//  alert("test");
		$(document).ready(function() {
			$('.datatabledraw').DataTable();
		} );*/
		
	</script>

