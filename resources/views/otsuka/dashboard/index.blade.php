@extends('otsuka.master')

@section('Patient Dashboard')
Dashboard
@endsection

@section('content')
<?php $pic = URL::asset('/uploads/');?>
<?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
th { font-size: 12px; }
td { font-size: 11px; }

.med_logo img {
  width:100%;
  height:10%;
}

[hidden] {
  display: none !important;
}

.bar {
  height: 18px;
  background: green;
}
.modal-center {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

/**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
  margin-left: 3vw;     
  margin-right:3vw;  
}    

.zoom {      
  -webkit-transition: all 0.35s ease-in-out;    
  -moz-transition: all 0.35s ease-in-out;    
  transition: all 0.35s ease-in-out;     
  cursor: -webkit-zoom-in;      
  cursor: -moz-zoom-in;      
  cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(2.5);    
-moz-transform: scale(2.5);  
-webkit-transform: scale(2.5);  
-o-transform: scale(2.5);  
transform: scale(2.5);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
  ul.gallery {      
    margin-left: 15vw;       
    margin-right: 15vw;
  }

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}

.progress { position:relative; width:100%; height: 30px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar { background-color: #B4F5B4; width:0%; height:30px; border-radius: 3px; }
.percent { position:absolute; display:inline-block; top:3px; left:48%; }

input[type=radio]{
  transform:scale(1.5);
}
</style>

<!-- Modal -->
<div class="modal fade modal-center" id="loading" role="dialog">
  <div class="modal-dialog">
   Modal content
   <img src="{{$pic}}/loading.gif" width="50%" height="50%" style="border-radius: 10%;">
 </div>
</div>

<!-- Modal -->
<div class="modal fade" id="success" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Success!</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-success">
          <div class="row">
            <div class="col-md-12">
              <strong></strong> <span style="font-size:21px">Photo(s) successfully uploaded and now on queue. Please go to "Uploads for the Day" page to verify your  
               <?php if(!empty($type))						
               {echo strtolower($type).'.';} ?>  
               <br/><b>Thank you!</b></span>
             </div>
           </div>
         </div>
         <div class="row">
          <div class="col-md-12">
            <div class="col-md-12 well">
              <label for="images">Image(s)</label>
              <hr style="border-color: black;">
              @if(!empty($images))
              @foreach($images as $image)
              <div class="list-inline gallery">
                <div class='col-md-3 thumbnail zoom'>
                  <img src="{{$pic.'/'.$image->filename}}">
                </div>
              </div>
              @endforeach
              @endif 
            </div>
          </div>
        </div>         
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div> 
    </div>
  </div>
</div>

<form method="post" id="form_id" action="{{route('otsuka.index')}}" enctype="multipart/form-data">
  {{ csrf_field() }} 
  <div class="row">
    <input type="hidden" value="" id="product_submit"/>

    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="form-group">
            <div class="radio">
              <label><input id='abilify' class="medicine" name='medicine' type='radio' value='Abilify'/><b>ABILIFY</b></label>
            </div>
          </div>
        </div>
        <div class="panel-body">
          <div class="well well-sm med_logo">
            <img src="{{$pic}}/Abilify.png">

            <div id='abi' style='display:none;'>
              <div>
                <hr>
                <div class="radio">
                  <label><input id='abi_enroll' class="medicine_options" name='type' type='radio' value='Enroll'/><b> Retrieval</b></label>
                </div>
                <hr>
                <div id='abi_enr_upload' style='display:none;'>            
                  <fieldset class="form-group">

                    <fieldset class="form-group" id="fp">
                      <!--               <div id="image_preview" class="list-inline gallery"></div> -->
                    </fieldset>
                    <label for="content">Select File: </label><label class="btn-xs btn-primary pull-right">
                      Browse<input type="file" name="file[]" multiple="true" id="upload_file" accept="image/*" onchange="preview_image();" hidden>
                    </label>
                    <div class="progress">
                      <div class="bar"></div>
                      <div class="percent">0%</div>
                    </div>
                    <div id="abilifiy_enroll_submit" style="margin-top:20px;text-align:center"> </div>
                    <div class="statusmsg" id="status"></div>
                  </fieldset>                                  
                </div>
              </div>

              <div>
                <hr>
                <div class="radio">
                  <label><input id='abi_reciept' class="medicine_options" name='type' type='radio' value='Receipt'/><b> Receipt</b></label>
                </div>
                <hr>
                <div id='abi_rec_upload' style='display:none;'>
                  <fieldset class="form-group">

                    <fieldset class="form-group" id="fp2">
                      <!--               <div id="image_preview" class="list-inline gallery"></div> -->
                    </fieldset>

                    <label for="content">Select File: </label><label class="btn-xs btn-primary pull-right">
                      Browse<input type="file" name="file[]" multiple="true" id="upload_file2" accept="image/*" onchange="preview_image2();" hidden>
                    </label>

                    <div class="progress">
                      <div id="abi_rec_bar" class="bar"></div>
                      <div id="abi_rec_percent" class="percent">0%</div>
                    </div>
                    <div class="statusmsg" id="status1"></div>

                    <div id="abilifiy_reciept_submit" style="text-align:center"> </div>
                  </fieldset> 


                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>


    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="form-group">
            <div class="radio">
              <label><input id='aminoleban' class="medicine" name='medicine' type='radio' value='Aminoleban'/><b>AMINOLEBAN</b></label>
            </div>
          </div>
        </div>
        <div class="panel-body">
          <div class="well well-sm med_logo">
            <img src="{{$pic}}/Aminoleban.png">

            <div id='ami' style='display:none;'>
             <div>
              <hr>
              <div class="radio">
                <label><input id='ami_enroll' name='type' type='radio' value='Enroll'/><b> Retrieval</b></label>
              </div>
              <hr>
              <div id='ami_enr_upload' style='display:none;'>
                <fieldset class="form-group">

                  <fieldset class="form-group" id="fp3">
                    <!--               <div id="image_preview" class="list-inline gallery"></div> -->
                  </fieldset>
                  <label for="content">Select File: </label><label class="btn-xs btn-primary pull-right">
                    Browse<input type="file" name="file[]" multiple="true" id="upload_file3" accept="image/*" onchange="preview_image3();" hidden>
                  </label>
                  <div class="progress">
                    <div class="bar"></div>
                    <div class="percent">0%</div>
                  </div>
                  <div id="aminoleban_enroll_submit" style="margin-top:20px;text-align:center"> </div>
                  <div class="statusmsg" id="status2"></div>
                </fieldset> 
              </div>
            </div>

            <div>
              <hr>
              <div class="radio">
                <label><input id='ami_reciept' class="medicine_options" name='type' type='radio' value='Receipt' /><b> Receipt</b></label>
              </div>
              <hr>
              <div id='ami_rec_upload' style='display:none;'>
                <fieldset class="form-group">

                  <fieldset class="form-group" id="fp4">
                    <!--               <div id="image_preview" class="list-inline gallery"></div> -->
                  </fieldset>
                  <label for="content">Select File: </label><label class="btn-xs btn-primary pull-right">
                    Browse<input type="file" name="file[]" multiple="true" id="upload_file4" accept="image/*" onchange="preview_image4();" hidden>
                  </label><br/>
                   <div class="progress">
                    <div class="bar"></div>
                    <div class="percent">0%</div>
                  </div>
                  <div id="aminoleban_reciept_submit" style="margin-top:20px;text-align:center"> </div>
                  <div class="statusmsg" id="status3"></div>
                </fieldset>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="form-group">
          <div class="radio">
            <label><input id='pletal' class="medicine" name='medicine' type='radio' value='Pletaal'/><b>PLETAAL</b></label>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="well well-sm med_logo">
          <img src="{{$pic}}/Pletal.jpg">

          <div id='ple' style='display:none;'>
           <div>
            <hr>
            <div class="radio">
              <label><input id='ple_enroll' class="medicine_options" name='type' type='radio' value='Enroll' /><b> Retrieval</b></label>
            </div>
            <hr>
            <div id='ple_enr_upload' style='display:none;'>
              <fieldset class="form-group">

                <fieldset class="form-group" id="fp5">
                  <!--               <div id="image_preview" class="list-inline gallery"></div> -->
                </fieldset>
                <label for="content">Select File: </label><label class="btn-xs btn-primary pull-right">
                  Browse<input type="file" name="file[]" multiple="true" id="upload_file5" accept="image/*" onchange="preview_image5();" hidden>
                </label>
                <div class="progress">
                  <div class="bar"></div>
                  <div class="percent">0%</div>
                </div>
                <div id="pletaal_enroll_submit" style="margin-top:20px;text-align:center"> </div>
                <div class="statusmsg" id="status4"></div>
              </fieldset> 
            </div>
          </div>

          <div>
            <hr>
            <div class="radio">
              <label><input id='ple_reciept' class="medicine_options" name='type' type='radio' value='Receipt' /><b>Receipt</b></label>
            </div>
            <hr>
            <div id='ple_rec_upload' style='display:none;'>
              <fieldset class="form-group">

                <fieldset class="form-group" id="fp6">
                  <!--               <div id="image_preview" class="list-inline gallery"></div> -->
                </fieldset>
                <label for="content">Select File: </label><label class="btn-xs btn-primary pull-right">
                  Browse<input type="file" name="file[]" multiple="true" id="upload_file6" accept="image/*" onchange="preview_image6();" hidden>
                </label><br/>
                  <div class="progress">
                  <div class="bar"></div>
                  <div class="percent">0%</div>
                </div>
                <div id="pletaal_reciept_submit" style="margin-top:20px;text-align:center"> </div>
                <div class="statusmsg" id="status5"></div>
              </fieldset> 
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</form>
</div>
@endsection 

@section('footer-scripts')
<script>
  function ChecKInput(param) {

    if(($("#phone_no_" + param).val() == '') && ($("#mobile_no_" +  param).val() == ''))
    {


      $("#phone_no_" + param ).attr("required", "true");				
      $("#mobile_no_" +  param).attr("required", "true");		

    }
    else if(($("#phone_no_" + param).val() !== '') || ($("#mobile_no_" +  param).val() !== ''))
    {

      $("#phone_no_" + param ).removeAttr('required');	
      $("#mobile_no_" +  param).removeAttr('required');	


    }
		//alert($( "input[name='phone_no']" ).val()); 	$('#or_date').removeAttr('required');  $("#doctor_name").attr("required", "true");

		
  }



  $(document).ready(function(){


    $('form').on('submit', function(e){


      if($("#product_submit").val() == "abi")
      {
			//ChecKInput('ami');
			
			if(($("#phone_no_abi").val() == '') && ($("#mobile_no_abi").val() == ''))
			{

				$("#phone_no_" + $("#product_submit").val() ).attr("required", "true");				
				$("#mobile_no_" +  $("#product_submit").val()).attr("required", "true");		
				e.preventDefault();				

			}
			else if(($("#phone_no_abi").val() !== '') || ($("#mobile_no_abi").val() !== ''))
			{

				$("#phone_no_" + $("#product_submit").val() ).removeAttr('required');	
				$("#mobile_no_" +  $("#product_submit").val()).removeAttr('required');	

			}
			
			
			
			
		}
		else if($("#product_submit").val() == "ami")
		{
			//ChecKInput('ami');
			if(($("#phone_no_ami").val() == '') && ($("#mobile_no_ami").val() == ''))
			{

				$("#phone_no_" + $("#product_submit").val() ).attr("required", "true");				
				$("#mobile_no_" +  $("#product_submit").val()).attr("required", "true");		
				e.preventDefault();				

			}
			else if(($("#phone_no_abi").val() !== '') || ($("#mobile_no_abi").val() !== ''))
			{

				$("#phone_no_" + $("#product_submit").val() ).removeAttr('required');	
				$("#mobile_no_" +  $("#product_submit").val()).removeAttr('required');	

			}
		}
		else if($("#product_submit").val() == "ple")
		{
			//ChecKInput('ple');
			if(($("#phone_no_ple").val() == '') && ($("#mobile_no_ple").val() == ''))
			{

				$("#phone_no_" + $("#product_submit").val() ).attr("required", "true");				
				$("#mobile_no_" +  $("#product_submit").val()).attr("required", "true");		
				e.preventDefault();				

			}
			else if(($("#phone_no_abi").val() !== '') || ($("#mobile_no_abi").val() !== ''))
			{

				$("#phone_no_" + $("#product_submit").val() ).removeAttr('required');	
				$("#mobile_no_" +  $("#product_submit").val()).removeAttr('required');	

			}
		}
		
        //

      });
  });



  function InputToggle() {
   $( "div" ).each(function( i ) {
    if ( $(this).is(":visible"))  {
     $(this).find('input').prop( "disabled", false );
     $(this).find('textarea').prop( "disabled", false );
   } else {
    $(this).find('input').prop( "disabled", true );
    $(this).find('textarea').prop( "disabled", true );
  }
});
 }

 $(document).ready(function() {

   <?php if(isset($message)) { ?>

    $('#success').modal('show'); 

    <?php } ?>

    $('input[type="radio"]').click(function() {

     if($(this).attr('id') == 'aminoleban') {
      $('#ami').fadeIn();   
      $('#ple').fadeOut();
      $('#abi').fadeOut();    
    }
    else {


    }
    InputToggle();
  });
  });

 $(document).ready(function() {
   $('input[type="radio"]').click(function() {
     if($(this).attr('id') == 'ami_enroll') {
		  // document.getElementById('fp3').innerHTML =   
			//	'<b>Patient Name:</b><br/><input type="text" class="form-control input-sm" name="patient_name" required /><b>Phone #:</b><br/><input type="text" class="form-control input-sm" name="phone_no" required />';
      $('#ami_enr_upload').fadeIn(); 
      $('#ami_rec_upload').fadeOut();      
    }
    else {


    }
    InputToggle();
  });
 });

 $(document).ready(function() {
   $('input[type="radio"]').click(function() {
     if($(this).attr('id') == 'ami_reciept') {
       document.getElementById('fp4').innerHTML =
       '<b>Patient Name:</b><br/><input type="text" class="form-control input-sm" name="patient_name" required /><b>Mobile #:</b><br/><input type="text" class="form-control input-sm mobile_no" name="mobile_no" required  data-toggleInput="mobile_phone" id="mobile_no_ami"  onchange="ChecKInput('+"'ami'"+')"/><b>Landline #:</b><br/><input type="text" class="form-control input-sm phone_no" name="phone_no" id="phone_no_ami"  onchange="ChecKInput('+"'ami'"+')" /> ';

       $('#ami_rec_upload').fadeIn();
       $('#ami_enr_upload').fadeOut();       
     }
     else {


     }
     InputToggle();
   });
 });

 $(document).ready(function() {
   $('input[type="radio"]').click(function() {
     if($(this).attr('id') == 'pletal') {
      $('#ple').fadeIn(); 
      $('#ami').fadeOut();   
      $('#abi').fadeOut();           
    }

    else {

    }
    InputToggle();
  });
 });

 $(document).ready(function() {
   $('input[type="radio"]').click(function() {
     if($(this).attr('id') == 'ple_enroll') {
		   // document.getElementById('fp5').innerHTML =   
			//	'<b>Patient Name:</b><br/><input type="text" class="form-control input-sm" name="patient_name" required /><b>Phone #:</b><br/><input type="text" class="form-control input-sm" name="phone_no" required />';

      $('#ple_enr_upload').fadeIn(); 
      $('#ple_rec_upload').fadeOut();      
    }
    else {


    }
    InputToggle();
  });
 });

 $(document).ready(function() {
   $('input[type="radio"]').click(function() {
     if($(this).attr('id') == 'ple_reciept') {
      document.getElementById('fp6').innerHTML =

      '<b>Patient Name:</b><br/><input type="text" class="form-control input-sm" name="patient_name" required /><b>Mobile #:</b><br/><input type="text" class="form-control input-sm mobile_no" name="mobile_no" required id="mobile_no_ple" onchange="ChecKInput('+"'ple'"+');"/><b>Landline #:</b><br/><input type="text" class="form-control input-sm phone_no" name="phone_no" id="phone_no_ple" onchange="ChecKInput('+"'ple'"+')" /> ';


      $('#ple_rec_upload').fadeIn();
      $('#ple_enr_upload').fadeOut();       
    }
    else {


    }
    InputToggle();
  });
 });

 $(document).ready(function() {
   $('input[type="radio"]').click(function() {

     if($(this).attr('id') == 'abilify') {
      $('#abi').fadeIn();  
      $('#ple').fadeOut(); 
      $('#ami').fadeOut();      
    }

    else {

    }
    InputToggle();
  });
 });

 $(document).ready(function() {
   $('input[type="radio"]').click(function() {

     if($(this).attr('id') == 'abi_enroll') {
		  //   document.getElementById('fp').innerHTML =   
			//	'<b>Patient Name:</b><br/><input type="text" class="form-control input-sm" name="patient_name" required /><b>Phone #:</b><br/><input type="text" class="form-control input-sm" name="phone_no" required />';
      $('#abi_enr_upload').fadeIn(); 
      $('#abi_rec_upload').fadeOut();      
    }
    else {


    }
    InputToggle();
  });
 });

 $(document).ready(function() {
   $('input[type="radio"]').click(function() {

     if($(this).attr('id') == 'abi_reciept') {

      document.getElementById('fp2').innerHTML =

      '<b>Patient Name:</b><br/><input type="text" class="form-control input-sm" name="patient_name" required /><b>Mobile #:</b><br/><input type="text" class="form-control input-sm mobile_no" name="mobile_no" required id="mobile_no_abi"  onchange="ChecKInput('+"'abi'"+')"/><b>Landline #:</b><br/><input type="text" class="form-control input-sm phone_no"  id="phone_no_abi"  name="phone_no"   onchange="ChecKInput('+"'abi'"+')"/>  ';

      $('#abi_rec_upload').fadeIn();
      $('#abi_enr_upload').fadeOut();       
    }
    else {


    }
    InputToggle();
  });
 });

 $(document).ready(function (){

  $(".medicine, .medicine_options").change(function (){
    $('#upload_file, #upload_file2, #upload_file3, #upload_file4, #upload_file5, #upload_file6').val('');
    $('#abilifiy_enroll_submit').hide();
    $('#abilifiy_reciept_submit').hide();
    $('#aminoleban_enroll_submit').hide();
    $('#aminoleban_reciept_submit').hide();
    $('#pletaal_enroll_submit').hide();
    $('#pletaal_reciept_submit').hide();
    $('.statusmsg').hide();
    $('.bar').width(0);
    $('.percent').text('0%');
  });

  $('#modal_complete').on('hidden.bs.modal', function (e) {
      location.reload();
  });

});

 function preview_image() 
 {

   var total_file=document.getElementById("upload_file").files.length;
   for(var i=0;i<total_file;i++)
   {
	//document.getElementById('abilifiy_enroll_submit').innerHTML += '<div class="alert alert-info"><strong> File(s) Attached!</strong> Please input patient name and phone # and press the submit button.</div><button type="submit" data-toggle="" data-target="" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image">Submit</button>';
  $('#abilifiy_enroll_submit').show();
  document.getElementById('abilifiy_enroll_submit').innerHTML =
  '<div class="alert alert-info"><strong> File(s) Attached!</strong> Please press the submit button.</div><button style="margin-bottom: 10px;" type="submit" data-toggle="modal" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image">Submit</button>';

}

}

function preview_image2() 
{
 var total_file=document.getElementById("upload_file2").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#abilifiy_reciept_submit').show();
  document.getElementById('abilifiy_reciept_submit').innerHTML = '<div class="alert alert-info"><strong> File(s) Attached!</strong> Please input patient name and phone # and press the submit button.</div><button type="submit" data-toggle="modal" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image" onclick="ChecKInput('+ "'abi'"+');$('+ "'#product_submit'" + ').val('+"'abi'"+ ')">Submit</button>';

    /* document.getElementById('fp2').innerHTML =
    '<div class="alert alert-info"><strong> File(s) Attached!</strong> Please input patient name and phone # and press the submit button.</div><b>Patient Name:</b><br/><input type="text" class="form-control input-sm" name="patient_name" required /><b>Phone #:</b><br/><input type="text" class="form-control input-sm" name="phone_no" required /><button type="submit" data-toggle="modal" data-target="#loading" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image">Submit</button>'; */
    /*$('#image_preview2').append("<div class='col-md-4'><img src='"+URL.createObjectURL(event.target.files[i])+"'></div>");*/

  }
}


function preview_image3() 
{
 var total_file=document.getElementById("upload_file3").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#aminoleban_enroll_submit').show();
//	 document.getElementById('aminoleban_enroll_submit').innerHTML = '<div class="alert alert-info"><strong> File(s) Attached!</strong> Please input patient name and phone # and press the submit button.</div><button type="submit" data-toggle="" data-target="" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image">Submit</button>';
document.getElementById('aminoleban_enroll_submit').innerHTML =
'<div class="alert alert-info"><strong> File(s) Attached!</strong> Please press the submit button.</div><button type="submit" data-toggle="modal" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image">Submit</button>';
/*$('#image_preview3').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");*/
}
}
function preview_image4() 
{
 var total_file=document.getElementById("upload_file4").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#aminoleban_reciept_submit').show();
  document.getElementById('aminoleban_reciept_submit').innerHTML = '<div class="alert alert-info"><strong> File(s) Attached!</strong> Please input patient name and phone # and press the submit button.</div><button type="submit" data-toggle="modal" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image" onclick="ChecKInput('+ "'ami'"+');$('+ "'#product_submit'" + ').val('+"'ami'"+ ')">Submit</button>';
 //      document.getElementById('fp4').innerHTML =
 //               '<div class="alert alert-info"><strong> File(s) Attached!</strong> Please input patient name and phone # and press the submit button.</div><b>Patient Name:</b><br/><input type="text" class="form-control input-sm" name="patient_name" required /><b>Phone #:</b><br/><input type="text" class="form-control input-sm" name="phone_no" required /><button type="submit" data-toggle="modal" data-target="#loading" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image">Submit</button>';
 /*  $('#image_preview4').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");*/
}
}
function preview_image5() 
{
 var total_file=document.getElementById("upload_file5").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#pletaal_enroll_submit').show();
//	   document.getElementById('pletaal_enroll_submit').innerHTML = '<div class="alert alert-info"><strong> File(s) Attached!</strong> Please input patient name and phone # and press the submit button.</div><button type="submit" data-toggle="" data-target="" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image">Submit</button>';
document.getElementById('pletaal_enroll_submit').innerHTML =
'<div class="alert alert-info"><strong> File(s) Attached!</strong> Please press the submit button.</div><button type="submit" data-toggle="modal" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image">Submit</button>';
/*  $('#image_preview5').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");*/
}
}
function preview_image6() 
{
 var total_file=document.getElementById("upload_file6").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#pletaal_reciept_submit').show();
  document.getElementById('pletaal_reciept_submit').innerHTML = '<div class="alert alert-info"><strong> File(s) Attached!</strong> Please input patient name and phone # and press the submit button.</div><button type="submit" data-toggle="modal" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image" onclick="ChecKInput('+ "'ple'"+');$('+ "'#product_submit'" + ').val('+"'ple'"+ ')">Submit</button>';
 //      document.getElementById('fp6').innerHTML =
 //               '<div class="alert alert-info"><strong> File(s) Attached!</strong> Please input patient name and phone # and press the submit button.</div><b>Patient Name:</b><br/><input type="text" class="form-control input-sm" name="patient_name" required /><b>Phone #:</b><br/><input type="text" class="form-control input-sm" name="phone_no" required /><button type="submit" data-toggle="modal" data-target="#loading" class="btn-xs btn-primary pull-right" name="submit_image" value="Upload Image">Submit</button>';
 /*  $('#image_preview6').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");*/
}
}

$(function() {

  var bar = $('.bar');
  var percent = $('.percent');
  var status = $('.statusmsg');


  $('form').ajaxForm({
    beforeSend: function() {
      status.empty();
      var percentVal = '0%';
      bar.width(percentVal);
      percent.html(percentVal);
    },
    uploadProgress: function(event, position, total, percentComplete) {
      var percentVal = percentComplete + '%';
      bar.width(percentVal);
      percent.html(percentVal);
    },
    complete: function(xhr) {
      var data = xhr.response;
      $('#modal_complete').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
      });

      // status.html('<b>Photo(s) successfully uploaded and now on queue. Please go to "Uploads for the Day" page to verify your enrollment or reciept. Thank you!</b>').fadeIn();

      $(".validate").fadeOut();
    },

  });
}); 

function ChecKInput() {

 if(($("input[name='phone_no']" ).val() == '') && ($("input[name='mobile_no']" ).val() == ''))
 {

   $("input[name='mobile_no']" ).attr("required", "true");       
   $("input[name='phone_no']" ).attr("required", "true");        
 }
 else if(($("input[name='phone_no']" ).val() !== '') || ($("input[name='mobile_no']" ).val() !== ''))
 {

   $("input[name='phone_no']" ).removeAttr('required');  
   $("input[name='mobile_no']" ).removeAttr('required'); 
 }
   //alert($( "input[name='phone_no']" ).val());   $('#or_date').removeAttr('required');  $("#doctor_name").attr("required", "true");
   
 }


</script>

<!-- Modal -->
<div id="modal_complete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Success</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-success" style="margin-bottom: 0px !important;">
       Photo(s) successfully uploaded and now on queue. Please go to <b>"Uploads for the Day"</b> page to verify your enrollment or reciept. Thank you!
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@if(empty('images'))
<script type="text/javascript">
  $('#success').modal('show');
</script>
@endif

<!-- <script src="{{$jsupload}}/js/vendor/jquery.ui.widget.js"></script>
<script src="{{$jsupload}}/js/jquery.iframe-transport.js"></script>
<script src="{{$jsupload}}/js/jquery.fileupload.js"></script>

<script>
$(function () {
    $('#upload_file').fileupload({
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo(document.body);
            });
        }
    });
});

$(function () {
    $('#upload_file').fileupload({
        dataType: 'json',
        add: function (e, data) {
            data.context = $('<button/>').text('Upload')
                .appendTo(document.body)
                .click(function () {
                    data.context = $('<p/>').text('Uploading...').replaceAll($(this));
                    data.submit();
                });
        },
        done: function (e, data) {
            data.context.text('Upload finished.');
        }
    });
});
</script> -->


@endsection


