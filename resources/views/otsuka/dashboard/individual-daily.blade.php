@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
	text-align: center;
}

</style>

<div class="panel panel-default">
  <div class="panel-heading">Generate</div>
  <div class="panel-body">

  		<div class="col-md-12">
            <form method="get" action="{{route('otsuka.generate-daily-individual')}}" class="form-horizontal p-10">
                {{ csrf_field() }}            
                <div class="row">
                    <div class="col-md-4">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">From: 
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datetimepicker"  placeholder="from" name="from">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-4">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">To:
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datetimepicker"  placeholder="to" name="to">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group p-10">
                            <label class="col-md-3 control-label" for="example-select">Select:</label>
                            <div class="col-md-9">
                                <select name="medrep" class="form-control" size="1" required="">
                                		<option value="">* Please Select MedRep</option>
                                    @foreach($medreps as $mr)
                                        <option value="{{$mr->emp_code}}">{{trim($mr->pso_name,'"')}} - {{$mr->new_code}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-1">
                        <div class="form-group p-10 form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">Submit
                                </button>
                            </div>
                        </div>
                    </div>               
                </div>
            </form>
        </div>
    </div>
</div>

        <!-- Main content -->





      
      





@endsection 

@section('footer-scripts')
<script>
function popupwindow(url) {
	
	title = "Images";
	w = 500;
	h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

$(document).ready(function() {





$(".datatable").DataTable({
  "ordering": false,
});

});

$(document).ready(function() {



$('#verify').DataTable({
  "ordering": false,
});

});
</script>
@endsection

