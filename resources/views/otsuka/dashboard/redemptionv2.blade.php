@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php //$pic = 'http://oqs.onequest.com.ph/uploads/';?>
  <?php $jsupload = URL::asset('/jsupload/');
    $baseurl = URL::asset('/'); 
	
	 $AppHelper2 = new AppHelper; 
    //$baseurl = 'http://oqs.onequest.com.ph/';
  // $Helper = new AppHelper;
 
 //$skuDescripton =  json_decode($Helper->fetchTable("otsuka\Sku","id","sortBy"),true);  
 
 $sku_array = array();
	$sku_array['1'] = 'ABILIFY 10 mg ODT tablet';
	$sku_array['2'] = 'ABILIFY 10 mg tablet';
	$sku_array['3'] = 'ABILIFY 15 mg ODT tablet';
	$sku_array['4'] = 'ABILIFY 15 mg tablet';
	$sku_array['5'] = 'ABILIFY 5 mg tablet';
	$sku_array['6'] = 'ABILIFY Discmelt 10 mg Compliance Pack';
	$sku_array['7'] = 'ABILIFY Discmelt 15 mg Compliance Pack';
	$sku_array['8'] = 'ABILIFY Oral Solution 1mg/ml 150ml';
	$sku_array['9'] = 'AMINOLEBAN COMPLIANCE PACK (3+1)';
	$sku_array['10'] = 'AMINOLEBAN INJ 500mL';
	$sku_array['11'] = 'AMINOLEBAN ORAL 50g';
	$sku_array['12'] = 'LORELCO 250mg tablets';
	$sku_array['13'] = 'MEPTIN 25 mcg tablet';
	$sku_array['14'] = 'MEPTIN 50 mcg tablet';
	$sku_array['15'] = 'MEPTIN SWINGHALER 10mcg/puff';
	$sku_array['16'] = 'MEPTIN SYRUP 60mL';
	$sku_array['17'] = 'MIKELAN 5mg tablet';
	$sku_array['18'] = 'MUCOSTA 100mg Tab';
	$sku_array['19'] = 'OBUCORT SWINGHALER 200mcg/puff';
	$sku_array['20'] = 'PLETAAL 100 mg tablet';
	$sku_array['21'] = 'PLETAAL 50 mg tablet';
	$sku_array['22'] = 'PLETAAL Powder 100mg .5grams';
	$sku_array['23'] = 'PLETAAL Powder 50mg .25grams';
	$sku_array['24'] = 'Pletaal tablet 100mg Compliance pack';
	$sku_array['25'] = 'SAMSCA 15mg';
	$sku_array['26'] = 'UKNWN PROD';
	$sku_array['27'] = 'SAMSCA-Urinal Male';
	$sku_array['28'] = 'Envelop';
	$sku_array['29'] = 'Sticker Paper';
	$sku_array['30'] = 'SAMSCA-Leaflet';
	$sku_array['31'] = 'Permanent Card';
	$sku_array['32'] = 'Welcome Letter';
	$sku_array['33'] = 'SAMSCA-Urinal Female';
	$sku_array['34'] = 'Newsletter 1Q';
	$sku_array['35'] = 'Newsletter 2Q';
	$sku_array['36'] = 'Newsletter 3Q';
	$sku_array['37'] = 'Newsletter 4Q';
	
	/*foreach($skuDescripton as $key => $val) 
	{
		 $sku_array[$val['id']] = $val['ipc'] ;
		 
		 
	}*/
	
  ?>
<?php $ctr = 0; ;$total_qty = 0;$abilify_total ="";$amino_total="";$pleta_total=""; $array_brand = array();$brand_sku ="";$brand_sku_id ="";$brand_sku_desc ="";$brand_id = array()//array(["ABILIFY"=>"","AMINOLEBAN"=>"","PLETAAL"=>""]); ?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
div.panel-body {background-color: #ffffff;}
div.panel-heading {background-color: #497bff !important;

color:#ffffff!important;}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
    .panel{
    box-shadow: 0 5px 10px rgba(0, 0, 0, 1) !important;
	}
</style>



<div class="modal fade" id="myModal_Search_MD">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
        </button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
           <div class="panel panel-default">
            <div class="panel-heading">
              <label for="searchbox" >Search:  </label>
              <input type="text" id="searchbox2" class="form-control" style="display:inline;width:20%"/>
              <input type="hidden" id="filtered2" class="form-control" style="display:inline;width:20%" value="{{!empty($v->fetch_allocation->emp_code)?$v->fetch_allocation->emp_code: ''}}" />
              <select class="form-control" style="display:inline;width:20%" id="person2"><option value="filteredmdv2">MR Doctor</option><option value="globalmdv2">Global</option></select>
              <button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc2($('#searchbox2').val(),$('#person2').val())"></button>
            </div>     
            <div class="panel-body" id="filterdisplay2" style="overflow: scroll;text-align:center">
            </div>
           </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <form action="{{route('otsuka.redemption.enroll')}}" id="enroll-form">
  <!-- <input type="text" id="enroll_refid" name="enroll_refid" > -->
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2>Enroll  <span id="enroll_return_msg"></span> <label style="float:right;font-size:15px"><input type="checkbox" name="enroll_patient_consent"> Patient Consent</label></h2>     

      </div>
      <div class="modal-body">
       
		
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					  <label for="enroll_firstname">First:</label>
					  <input type="text" class="form-control" id="enroll_firstname"  name="enroll_firstname">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					  <label for="enroll_middlename">Middle:</label>
					  <input type="text" class="form-control" id="enroll_middlename"  name="enroll_middlename">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					  <label for="enroll_lastname">Lastname:</label>
					  <input type="text" class="form-control" id="enroll_lastname"  name="enroll_lastname">
					</div>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Mobile No:</label>
					  <input type="text" class="form-control" id="enroll_mobile_no"  name="enroll_mobile_no">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Mobile No 2:</label>
					  <input type="text" class="form-control" id="enroll_mobile_no_2"  name="enroll_mobile_no_2">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Landline:</label>
					  <input type="text" class="form-control" id="enroll_phone_no"  name="enroll_phone_no">
					</div>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">Birth Date:</label>
					  <input type="text" class="form-control" id="enroll_phone_no"  name="enroll_birthdate">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">Age:</label>
					  <input type="text" class="form-control" id="enroll_mobile_no"  name="enroll_age">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">NickName:</label>
					  <input type="text" class="form-control" id="enroll_mobile_no_2"  name="enroll_nickname">
					</div>
				</div>	
				
				<div class="col-md-3">
					<div class="form-group">			
					  <label for="email">Gender:</label>
					  <select >
						<option value="Px Won't Disclose">Px Won't Disclose</option><option value="Male">Male</option><option value="Female" >Female</option>
					  </select>
					 <!-- <input type="hidden" id="enroll_gender_text" name="enroll_gender_text"/>
					  <br/><input type="radio"   name="enroll_gender">Male
					  <br/><input type="radio"   name="enroll_gender">Female
					  <br/><input type="radio"   name="enroll_gender">Px Won't Disclose -->
					</div>
				</div>	
				
			</div>
				
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Doctor's Name:</label>
					  <input type="text" class="form-control" id="enroll_doctors_name"  name="email">
					 
					</div>
				</div>	
				<div class="col-md-4" style="margin-top: 30px;">
					<div class="form-group">
					 <label style="float:left;font-size:15px"><input type="checkbox" name="enroll_patient_consent"> Patient Consent</label>
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Hospital:</label>
					  <input type="text" class="form-control"  name="email">
					</div>
				</div>	
			</div>		
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
					  <label for="email">Address</label>
					  <textarea name="enroll_address" class="form-control"></textarea>
					  <!--<input type="text" class="form-control" id="enroll_doctors_name"  name="email">-->
					 
					</div>
				</div>	
				
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Landline:</label>
					  <input type="text" class="form-control"  name="email">
					</div>
				</div>	
			</div>		

			
		 
		
      </div>
      <div class="modal-footer"><div id="demo"></div>
		<button type="button" style="float:left; " id="btn_enroll_save" class="btn btn-info btn-default"  data-dismiss="modal">Save & Exit</button>
		<!--<button type="button" style="float:left; " id="btn_enroll_save" class="btn btn-primary btn-default">Save </button> -->
		
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
</form>
  </div>
</div>


<!--CALL DIV -->


<div class="panel panel-default">
  <div class="panel-heading"><strong>CALL DETAILS</strong></div>
    <div class="panel-body" style="font-size:12px;">

      <div class="row">
        <div class="col-md-12">
        <form method="post" id="redemption_call" action="{{route('otsuka.save')}}" enctype="multipart/form-data">
        {{ csrf_field() }}  
          <table class="table">
            <thead>
              <tr>
                <th>Call Attempt</th>
                <th>Call Date</th>
                <th>Status</th>
                <th>Remarks</th>
                <th>Call Notes</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>
			<tr>
				<td><input type="hidden" id="calllogs_id" name="callLogs"/>1</td>
				<td>(Auto Generated)</td>
				<td>
					<select class="form-control input-sm" name="remarks_call" required="">
					 @foreach($Call_status_list as $value)
                      <option value="{{$value->id}}">{{$value->name}}</option>
                      @endforeach
					</select>	
					 
				</td>
				<td><textarea class="form-control input-sm" name="remarks_others" id="remarks_others" style="height: 175px;"></textarea></td>
				<td><textarea class="form-control input-sm"  name="callnotes" id="callnotes" style="height: 175px;"></textarea></td>
				<td><a href="#" class="btn btn-success" onclick="SaveCallLOgs()" >Save Call Logs</button></td>
			</tr>
	

            </tbody>
          </table>
          </form>

        </div>
      </div>
</div>
</div>


<!--Call Div End -->













<div class="panel panel-default">
  <div class="panel-heading"><strong>RECIEPT INFORMATION</strong><span style="float:right;font-size:15px">UPLOAD NO : <input type="text" id="upload_id" readonly name="upload_id" value ='<?php echo $_GET["upload_id"]?>' /><strong>{{!empty($basicinfo->id)? $basicinfo->id :""}}</strong></span></div>
    <div class="panel-body" style="font-size:12px;">
		<table class="table table-bordered col-md-12">
		<thead>
			<tr>
				<th>Patient Name</th>
				<th>Contact Number</th>
				<th>Medicine</th>
				<th>OR NO</th>
				<th>OR Date</th>
				<th>Uploaded</th>
				<th>Encoded By</th>	
				<th>SKU</th>
				<th>Image</th>
			</tr>	
			
		</thead>
		<tbody>@foreach($basicinfo as $upload_details)
			<tr>
				
				<td>{{!empty($upload_details->patient_name)? $upload_details->patient_name :""}}</td>
				<td>{{!empty($upload_details->mobile_number)? $upload_details->mobile_number :""}} / {{!empty($upload_details->phone_no)? $upload_details->phone_no :""}}</td>
				<td>{{!empty($upload_details->medicine)? $upload_details->medicine :""}}</td>
				<td>{{!empty($upload_details->receipt_details->or_number)? $upload_details->receipt_details->or_number :""}}</td>
				<td>{{!empty($upload_details->receipt_details->or_date)? $upload_details->receipt_details->or_date :""}}</td>				
				<td>{{!empty($upload_details->user->name)? $upload_details->user->name :""}}</td>
				<td>{{!empty($upload_details->encoded_by->name)? $upload_details->encoded_by->name :""}}</td>
				
				
				<td>{{!empty($upload_details->receipt_details->fetch_encoded_dtl->qty)? $upload_details->receipt_details->fetch_encoded_dtl->qty :""}} 
						@foreach($upload_details->receipt_details->fetch_encoded_dtl as $receipt_sku_details => $receipt_val)
							@if(!empty($receipt_val->qty))
							({{$receipt_val->qty}}) {{$receipt_val->sku_details->skuname}}<br/>
							@endif
						@endforeach</td>
						<td><img src="{{!empty($upload_details->filename)? $baseurl . 'uploads/' . $upload_details->filename :''}}" style="width:75px; height:75px; margin-bottom: 25px;"></td>
				
				
			</tr>
			@endforeach
		</tbody>
		</table>
	  
	 
	</div>
</div>


 <div class="row">
		<div class="col-lg-12">
			
				<div class="panel panel-default">
					<div class="panel-heading" style="background-color:#e48585">
						<label for="searchbox" >Filter :	</label>
						<input type="text" id="searchbox" class="form-control" style="display:inline;width:20%"/>
						<select class="form-control" style="display:inline;width:20%" id="person"><option value="patient">Patient<option value="official_reciept">OR</option></select>
						<button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc($('#searchbox').val(),$('#person').val())"></button>
						<button class="btn-info btn-sm glyphicon glyphicon-remove" onclick="loadDoc('------------',$('#person').val())"></button>
						<button class="btn-info btn-sm glyphicon glyphicon-plus" style="float:right;display:none"  data-toggle="modal" data-target="#myModal">Register</button>
						
					</div>
					
					<div class="panel-body" id="filterdisplay" style="overflow: scroll;text-align:center;max-height:650px">
						
						
					</div>
				</div>	
			
			
			
		</div>
	 </div>
@if(!empty($patient_record[0]->id))
<form method="post" action="{{route('otsuka.save')}}" enctype="multipart/form-data" id="Edit-Patient-form">
<div class="row">
<div class="col-md-12">
			<div class="panel panel-default">
			  <div class="panel-heading">Patient Details  : # <span id="redemption_patient_id"><?php echo  !empty($patient_record[0]->id) ? $patient_record[0]->id :  '';?></span>
					<label class="switch" style="float:right"><a class="btn btn-info btn-xs" onclick="UpdatePatient_details();" id="update_patient_info_btn" style="display:none"/>UPDATE PATIENT INFO</a>UPDATE
					<input type="checkbox" id="check_box_updated" checked><span class="slider round"></span></label>
			  </div>
				<div class="panel-body">
				
			
				

				  <div class="row">
			
					<input type="hidden" name="patient_id" id="patient_id" value="<?php echo  !empty($patient_record[0]->id) ? $patient_record[0]->id :  '';?>"/>
					
					<div class="col-md-6">
					
						<div class="col-md-6">
						  <div class="well">
							  <label for="Patient Code">Patient Code:</label><p style="color:green;"><b></b></p>
							  <input type="text" name="p_id" class="form-control inputrecord" id="redemption_patient_code" value='<?php echo  !empty($patient_record[0]->patient_code) ? $patient_record[0]->patient_code :  "";?>' disabled>
							</div>
						  </div>

						<div class="col-md-6">
						  <div class="well">
							<label for="Patient Kit #">Patient Kit #:</label><p style="color:green;"><b>
							<input type="text" name="patient_kit_number" id="redemption_patient_kit_number"  class="form-control inputrecord" value='<?php echo  !empty($patient_record[0]->patient_kit_number) ? $patient_record[0]->patient_kit_number :  "";?>' disabled></b></p>
						  </div>
						</div>
						<div class="col-md-6"><?php $patient_type_legend = $AppHelper2->fetchPatientTypeLegend($patient_record[0]->patient_type);
							
							
							?>
						  <div class="well" style="background-color:<?php if(!empty($patient_type_legend)){echo $patient_type_legend->value;}?>">
							<label for="Patient Kit #">Patient Type:</label><p style="color:green;"><b>
							<input type="text" name="patient_kit_number" id="patient_type"  class="form-control inputrecord" value='<?php echo  !empty($patient_record[0]->patient_type) ? $patient_record[0]->patient_type :  "";?>' disabled></b></p>
						  </div>
						</div>
						<div class="col-md-6"><?php $patient_type_legend = $AppHelper2->fetch_uploader($patient_record[0]->id);
							
							
							?>
						  <div class="well" style="background-color:<?php if(!empty($patient_type_legend)){echo $patient_type_legend;}?>">
							<label for="Patient Kit #">Uploaded By</label><p style="color:green;"><b>
							<input type="text" name="patient_kit_number" id="patient_type"  class="form-control inputrecord" value='<?php echo  !empty($patient_type_legend) ? $patient_type_legend :  "";?>' disabled></b></p>
						  </div>
						</div>
						 <div class="col-md-12">
						 <div class="well">
						 
						 <?php  $patient_type_legend = $AppHelper2->fetchUpload($patient_record[0]->id);
							
							
							
							?>
							<a href="{{$pic.'/'}}{{$patient_type_legend}}" target="_blank"><img src="{{$pic.'/'}}{{$patient_type_legend}}" style="max-width:500px;border-radius: 8px;"></a>
						</div>
						</div>
					</div>
					<div class="col-md-6">
					  <div class="well" style="max-height:400px;overflow: scroll;">
						<label for="Patient Kit #">Images:</label>
						<p>
						<?php if(!empty($Encoded_Purchases)) {
						 foreach($Encoded_Purchases as $key => $val){ ?>
						<p><a href="{{$pic.'/'}}<?php	if( !empty($val->file_path) )	{echo $val->file_path;	}?>" target="_blank" onclick="popupwindow('{{$pic.'/'}}<?php	if( !empty($val->file_path) )	{echo $val->file_path;	}?>');"><img class="img-thumbnail" style="width: 10%; height: 10%;" src="{{$pic.'/'}}<?php	if( !empty($val->file_path) )	{echo $val->file_path;	}?>">
						</a>
						<td>
							<?php
							if( !empty($val->file_path) )
							{
								echo $val->file_path;
							}
							else if( !empty($val->or_number) )
							{
							echo $val->or_number;
							}
						
							?>
						
						</td>
						</p> <hr/>
						<?php }// foreach($Encoded_Purchases as $key => $val){?>
						<?php } // IF !empty($Encoded_Purchases) {?>
					
						</p>
					  </div>
					</div>

					</div>

				<div class="row">

				  <div class="col-md-12">
					<div class="form-group">
					  <label for="details">Patient Name:</label>
					  <input type="text" class="form-control input-sm inputrecord" placeholder="" name="patient_fullname" id="name" value='<?php echo  !empty($patient_record[0]->patient_fullname) ? $patient_record[0]->patient_fullname :  "";?>' disabled>
					</div>
				  </div>

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="first">Last Name</label>
					  <input type="text" class="form-control input-sm inputrecord" name="patient_lastname" value='<?php echo  !empty($patient_record[0]->patient_lastname) ? $patient_record[0]->patient_lastname :  "";?>' disabled placeholder="" id="last">
					</div>
				  </div>
				  <!--  col-md-6   -->

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="last">First Name</label>
					  <input type="text" class="form-control input-sm inputrecord" disabled value="<?php echo  !empty($patient_record[0]->patient_firstname) ? $patient_record[0]->patient_firstname :  "";?>" name="patient_firstname" placeholder="" id="first">
					</div>
				  </div>

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="last">Middle Name</label>
					  <input type="text" class="form-control input-sm inputrecord" disabled value='<?php echo  !empty($patient_record[0]->patient_middlename) ? $patient_record[0]->patient_middlename :  "";?>' name="patient_middlename" placeholder="" id="middle">
					</div>
				  </div>
				  <!--  col-md-6   -->
				</div>


				<div class="row">
				
				<!--<?php /*
					<div class="col-md-4">
					<div class="form-group">
					  <label for="birthdate">Birthdate </label>
					  
						<!--<?php /* $d = !empty($patient_record[0]->birth_date) ?  new DateTime($patient_record[0]->birth_date) :  "";*/?><?php /*  !empty($patient_record[0]->birth_date) ?   $d->format('Y-m-d') :  "";

					  <input type="date" class="form-control input-sm inputrecord" disabled value='<?php echo  !empty($patient_record[0]->birth_date) ?  $patient_record[0]->birth_date :  "";?>' placeholder="" name="birth_date" id="birthdate"> 
					</div>
			      </div> */?> -->
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Birth Date:</label>
					  <input type="text" class="form-control input-sm datepicker inputrecord" disabled value='<?php echo  !empty($patient_record[0]->birth_date) ?  date("m/d/Y",strtotime($patient_record[0]->birth_date)) :  "";?>' placeholder="" name="birth_date" id="birthdate">
					</div>
				</div>


				 
				  <!--  col-md-6   -->

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="gender">Gender</label>
					  <select class="form-control input-sm inputrecord" disabled  name="gender">
						<option value="">* Please Select Gender</option>
						<option value="Px Won't Disclose" <?php if(!empty($patient_record[0]->gender)){
								if($patient_record[0]->gender == "Px Won't Disclose")
								{echo 'selected';	}
						};?> >Px Won't Disclose</option>
						<option value="Male" 
						<?php if(!empty($patient_record[0]->gender)){
								if($patient_record[0]->gender == 'Male')
								{echo 'selected';	}
						};?> >Male</option>
						
						
						<option value="Female"
						<?php if(!empty($patient_record[0]->gender)){
								if($patient_record[0]->gender == 'Female')
								{echo 'selected';	}
						};?>>Female</option>
					  </select>
					</div>
				  </div>

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="gender">Age</label>
					  <input type="number" class="form-control input-sm inputrecord" disabled placeholder="Age" name="age" value="<?php echo  !empty($patient_record[0]->age) ? $patient_record[0]->age :  "";?>">
					</div>
				  </div>
				  <!--  col-md-6   -->
				<!--  row   -->

				  <div class="col-md-12">
					<div class="form-group">
					  <label for="address">Address</label>
					  <input type="text" class="form-control input-sm inputrecord" disabled id="address" name="address" placeholder="Address" value='<?php echo  !empty($patient_record[0]->address) ? $patient_record[0]->address :  "";?>'>
					</div>
					
					
				  </div>
				</div>
				  <!--  col-md-6   -->
				<hr>
				<div class="row">
				  <div class="col-md-4">
					<div class="form-group">
					  <label for="mobile1">Mobile No. 1</label>
					  <input type="number" class="form-control input-sm inputrecord" disabled  name="mobile_number" placeholder="Mobile 1" value='<?php echo  !empty($patient_record[0]->mobile_number) ? $patient_record[0]->mobile_number :  "";?>'>
					</div>
				  </div>

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="mobile">Mobile No. 2</label>
					  <input type="number" class="form-control input-sm inputrecord" disabled name="mobile_number_2" placeholder="Mobile 2" value='<?php echo  !empty($patient_record[0]->mobile_number_2) ? $patient_record[0]->mobile_number_2 :  "";?>'>
					</div>
				  </div>

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="landline">Landline</label>
					  <input type="number" class="form-control input-sm inputrecord" disabled  placeholder="Landline" name="phone_number" value='<?php echo  !empty($patient_record[0]->phone_number) ? $patient_record[0]->phone_number :  "";?>'>
					</div>
				  </div>
				  <!--  col-md-6   -->
				</div>
				<!--  row   -->
				<hr>
				<div class="row">
				  <div class="col-md-6">
				  <div class="form-group">
					  <label for="mobile1">Preferred Time to Call</label>
					  <input type="text" class="form-control input-sm inputrecord" name="time_to_call" placeholder="Preffered Time to Call" name="time_to_call" value="<?php echo  !empty($patient_record[0]->time_to_call) ? $patient_record[0]->time_to_call :  "";?>" disabled>
					</div>
				  </div>

				  <div class="col-md-6">
				  <div class="form-group">
					  <label for="mobile1">Remarks</label>
					  <textarea class="form-control inputrecord" disabled  rows="5" name="remarks" id="remarks" placeholder="Remarks"><?php echo  !empty($patient_record[0]->remarks) ? $patient_record[0]->remarks :  "";?></textarea>
					</div>
				  </div>
				</div>

			  </div>
			</div>		
</div>
</div>
</form>
@endif
<!-- /////////////PERSONAL INFO END HERE-->
	 
	 

@if(!empty($patient_record[0]->id))	 
<form method="post" action="{{route('otsuka.save')}}" enctype="multipart/form-data" id="Edit-Patient-med-form">
<div class="panel panel-default">
  <div class="panel-heading">Patient Medical Info <label class="switch" style="float:right"><a class="btn btn-info btn-xs" onclick="UpdatePatient_med_details();" id="update_patient_info_btn2" style="display:none"/>UPDATE PATIENT MED INFO</a>UPDATE
					<input type="checkbox"  id="check_box_updated_med" checked><span class="slider round"></span></label></div>
    <div class="panel-body" style="font-size:11px">
	
	   <div class="row">
          <div class="col-md-2">

            <label for="Medical Representative">MR Code</label><input type="text" disabled class="form-control input-sm inputrecord_med" id="patient_mr_id" name="patient_mr_id" value="<?php if(!empty($patient_record[0]->patient_mr_name)){echo $patient_record[0]->patient_mr_id;} ?>" >			

          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MR Representative </label><input type="text" disabled class="form-control input-sm inputrecord_med" id="patient_mr_name" name="patient_mr_name" value="<?php if(!empty($patient_record[0]->patient_mr_name)){echo $patient_record[0]->patient_mr_name;} ?>" >			
          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MR Area Code</label><input type="text" disabled class="form-control input-sm inputrecord_med" id="patient_tagging_area_code" name="patient_tagging_area_code" value="<?php if(!empty($patient_record[0]->patient_tagging_area_code)){echo $patient_record[0]->patient_tagging_area_code;} ?>" >			
          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MR Team </label><input type="text" disabled class="form-control input-sm inputrecord_med" id="patient_tagging_team" name="patient_tagging_team" value="<?php if(!empty($patient_record[0]->patient_tagging_team)){echo $patient_record[0]->patient_tagging_team;} ?>" >			
          </div>
		  <div class="col-md-4">
						
				<label for="Search">&nbsp; </label><br/><a class="btn btn-primary btn-sm" id="btnLaunch" style="display:none" role="button">Search</a>
					
			
          </div> 
         
      </div>	
	  
		<div class="row">
          <div class="col-md-2">
            <label for="Medical Representative">Doctor Id </label><input type="text" disabled class="form-control input-sm inputrecord_med" id="patient_tagging_doctorid" name="patient_tagging_doctorid" value="<?php if(!empty($patient_record[0]->patient_tagging_doctorid)){echo $patient_record[0]->patient_tagging_doctorid;} ?>" >			
          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MD Emp Code </label><input type="text" disabled class="form-control input-sm inputrecord_med" id="patient_tagging_emp_doctorid" name="patient_tagging_emp_doctorid" value="<?php if(!empty($patient_record[0]->patient_tagging_emp_doctorid)){echo $patient_record[0]->patient_tagging_emp_doctorid;} ?>">			
          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MD Name </label><input type="text" disabled class="form-control input-sm inputrecord_med" id="patient_tagging_doctor_name" name="patient_tagging_doctor_name" value="<?php if(!empty($patient_record[0]->patient_tagging_doctor_name)){echo $patient_record[0]->patient_tagging_doctor_name;} ?>" >			
          </div>
         <div class="col-md-3">
            <label for="Medical Representative">Hospital </label><input type="text" disabled class="form-control input-sm inputrecord_med" id="patient_tagging_hospital" name="patient_tagging_hospital" value="<?php if(!empty($patient_record[0]->patient_tagging_hospital)){echo $patient_record[0]->patient_tagging_hospital;} ?>" >			
          </div>
         <div class="col-md-1">
            <label for="Medical Representative">MD Class </label><input type="text" disabled class="form-control input-sm inputrecord_med" id="patient_tagging_md_class" name="patient_tagging_md_class" value="<?php if(!empty($patient_record[0]->patient_tagging_md_class)){echo $patient_record[0]->patient_tagging_md_class;} ?>" >			
          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MD Specialty </label><input type="text" disabled class="form-control input-sm inputrecord_med" id="patient_tagging_specialty" name="patient_tagging_specialty" value="<?php if(!empty($patient_record[0]->patient_tagging_specialty)){echo $patient_record[0]->patient_tagging_specialty;} ?>" >			
          </div>          
      </div>
	   <div class="row" style="">
        <div class="col-md-2" >
          <label for="doctor_wont_disclose">Patient won't disclose </label><input type="checkbox" disabled class="form-control input-xs inputrecord_med" onclick="disableDoctor()" id="doctor_wont_disclose" <?php if(!empty($patient_record[0]->doctor_wont_disclose)){ if($patient_record[0]->doctor_wont_disclose=="true") {echo "checked";}} ?> name="doctor_wont_disclose" placeholder="patient_wont_disclose" >
        </div>
      </div>
		
      <div class="row" style="">
          <div class="col-md-6">
            <label for="Medical Representative">Medical Representative </label><input type="text" id="mr_name" class="form-control input-sm inputrecord_med" name="p_name" value="" readonly=""><input type="hidden" id="mr_id" class="form-control input-sm inputrecord_med" name="medrep_id" value="">
			
          </div>
          <div class="col-md-5">
					<div class="col-md-8">
					<label for="Doctor">Doctor</label><input type="text" id="md_name" disabled class="form-control input-sm inputrecord_med" name="doctor" placeholder="Doctor" value="{{$patient_record[0]->doctor_name}}">
					
					</div>
					<div class="col-md-3">
						<label for="Doctor">Doctor ID</label><input type="text" disabled  class="form-control input-sm inputrecord_med"  name="doctor_id" id="doctor_id" value="<?php if(!empty($patient_record[0]->fetch_sku)){echo $patient_record[0]->fetch_sku->doctors_id;} ?>">
					</div>	
          </div>
          <div class="col-md-1">
            <label for="Search">&nbsp; </label><br/><a class="btn btn-primary btn-sm" id="btnLaunch2" style="display:none" role="button">Search</a>
          </div>
      </div>
            
      <div class="row" style="display:none">
        <div class="col-md-12">
          <label for="Hospital">Hospital </label><input type="text" disabled class="form-control input-sm inputrecord_med" id="hospital" name="hospital" placeholder="Hospital" value="{{$patient_record[0]->hospital}}">
        </div>
      </div>

      <div class="row">
          <div class="col-md-6">
            <label for="Product">Product </label>
              <select class="form-control input-sm inputrecord_med" disabled id="product" name="product">
                <option value=""></option>
                <option <?php if((!empty($patient_record[0]->fetch_sku)) && (strtoupper($patient_record[0]->fetch_sku->brand) == 'ABILIFY')) {echo "Selected";}?> value="Abilify">Abilify</option>
                <option <?php if((!empty($patient_record[0]->fetch_sku)) && (strtoupper($patient_record[0]->fetch_sku->brand) == 'AMINOLEBAN')) {echo "Selected";}?> value="Aminoleban">Aminoleban</option>
                <option <?php if((!empty($patient_record[0]->fetch_sku)) && (strtoupper($patient_record[0]->fetch_sku->brand) == 'PLETAAL')) {echo "Selected";}?> value="Pletaal">Pletaal</option>
              </select>
          </div>
        <div class="col-md-6">
          <label for="SKU">SKU </label><select class="form-control input-sm inputrecord_med" disabled id="product_sku" name="product_sku">
              <option value="">* Please Select SKU</option>
						  @foreach($Mgabilify as $mab)
						  <option value="{{$mab->id}}" <?php if((!empty($patient_record[0]->fetch_sku->sku)) && ($patient_record[0]->fetch_sku->sku == $mab->id)) {echo "Selected";}?>>{{$mab->skuname}}</option>
						  @endforeach
            </select>
      </div>
    </div>
<hr>
            <div class="row">
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Tabs Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="notabsprescribe" disabled class="form-control input-sm inputrecord_med" max="9999" 
					value="<?php if(!empty($patient_record[0]->fetch_sku)) {echo $patient_record[0]->fetch_sku->no_of_tabs_prescribe;}?>">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Days Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="nodaysprescribe" disabled class="form-control input-sm inputrecord_med" max="9999" 
					value="<?php if(!empty($patient_record[0]->fetch_sku)) {echo $patient_record[0]->fetch_sku->no_of_days_prescribe;}?>">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label">Total No. of Tabs</label>
                  <div class="col-md-4">
                    <input type="number" name="totalnotabs" disabled class="form-control input-sm inputrecord_med" max="9999" 
					value="<?php if(!empty($patient_record[0]->fetch_sku)) {echo $patient_record[0]->fetch_sku->total_no_of_tabs;}?>">
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" disabled class="inputrecord_med" 
				  <?php if(!empty($patient_record[0]->fetch_sku))
					{	if($patient_record[0]->fetch_sku->tabs_prescribe_remarks == "Px Won't Disclose")
						{echo "Checked";}
					}
				  ?> value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" disabled class="inputrecord_med" 
				  <?php if(!empty($patient_record[0]->fetch_sku))
					{	if($patient_record[0]->fetch_sku->tabs_prescribe_remarks == "No Available Data")
						{echo "Checked";}
					}
				  ?> value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" disabled class="inputrecord_med" 
				  <?php if(!empty($patient_record[0]->fetch_sku))
					{	if($patient_record[0]->fetch_sku->days_prescribe_remarks == "Px Won't Disclose")
						{echo "Checked";}
					}
				  ?> value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" disabled class="inputrecord_med" 
				  <?php if(!empty($patient_record[0]->fetch_sku))
					{	if($patient_record[0]->fetch_sku->days_prescribe_remarks == "No Available Data")
						{echo "Checked";}
					}
				  ?> value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="totaltabs" disabled class="inputrecord_med" 
				  <?php if(!empty($patient_record[0]->fetch_sku))
					{	if($patient_record[0]->fetch_sku->total_tabs_remarks == "Px Won't Disclose")
						{echo "Checked";}
					}
				  ?> value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="totaltabs" disabled class="inputrecord_med" 
				  <?php if(!empty($patient_record[0]->fetch_sku))
					{	if($patient_record[0]->fetch_sku->total_tabs_remarks == "No Available Data")
						{echo "Checked";}
					}
				  ?> value="No Available Data">No Available Data</label>
                </div>
              </div>
            </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
                <label>Already Purchased:</label>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" disabled class="inputrecord_med" <?php if(!empty($patient_record[0]->fetch_sku))
					{	if($patient_record[0]->fetch_sku->purchased == "Yes")
						{echo "Checked";}
					}
				  ?> value="Yes">Yes</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" disabled class="inputrecord_med" <?php if(!empty($patient_record[0]->fetch_sku))
					{	if($patient_record[0]->fetch_sku->purchased == "No")
						{echo "Checked";}
					}
				  ?> value="No">No</label>
                </div>
              </div>
            </div>
            <hr/>
		
	</div>		
</div>
</form>
@endif	 
	 
	 
	 
	 

@if(!empty($patient_record[0]->id))
<div class="panel panel-default">


    <div class="panel-heading"><strong>REDEMPTION</strong></div>
    <div class="panel-body">

		<div class="row">
			<div class="col-md-12">
			
			
			  <h4>History Purchased </h4><br/>
			  
			  
						<table id="" class="display datatable table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Date Process</th>
									<th>Process By</th>
									<th>OR #</th>
									<th>OR Date</th>
									<th>FilePath</th>
									<th>Redeemed Items</th>
									<th>QTY</th>
									
									
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Date Process</th>
									<th>Process By</th>	
									<th>OR #</th>
									<th>OR Date</th>
									<th>FilePath</th>
									<th>Redeemed Items</th>
									<th>QTY</th>
									
								</tr>
							</tfoot>
							<tbody>
								
								@if(!empty($Encoded_Purchases))
								<?php foreach($Encoded_Purchases as $key => $val){
								
								?>
								<tr >
									<td>@if(!empty($val->created_at))  
										{{$val->created_at}}
										@else
											PROCESS BY SANDMAND
										@endif
								
									</td>
									<td>{{$val->created_by}}</td>
									<td>{{$val->or_number}}</td>
									<td>{{$val->or_date}} </td>									
									<td>{{$val->file_path}}</td>
									<td>
									@if(!empty($val->fetch_encoded_dtl))
									<?php  foreach($val->fetch_encoded_dtl as $key2 => $val2)
													{	
														if(!empty($val2->sku_details->skuname)){
														echo $val2->sku_details->skuname . '<br/>';
														}
														/*if($val2->sku_details->is_non_med == "false")
														{
														//if(date('Y-m-d',strtotime($val->or_date)) > date('Y-m-d', strtotime("2018-02-01")))
														{
														$total_qty += $val2->qty ;
														}
														$array_brand[$val2->sku_details->brand] = $total_qty ;
														$brand_sku = $val2->sku_details->brandid ;
														$brand_sku_id = $val2->sku_details->id;
														$brand_sku_desc = $val2->sku_details->skuname;
													
														}*/		
				
													} ?>
									@endif
									
									</td>
									
									<td>
											<?php foreach($val->fetch_encoded_dtl as $key2 => $val2)
													{	
														echo '( ' . $val2->qty . ' ) ' . '<br/>';

													}?>								
									</td>
									
									
								</tr>
								<?php } ?>
							   @endif
								
							</tbody>
						</table>
			  

			  
			</div>
			
		</div>
		<?php 
				if(empty($brand_sku))
				{
				if(!empty($old_retrieval_enrollment_record)) 
				{ $brand_sku = $old_retrieval_enrollment_record->brandid;}
				}
 ?>
		<?php print_r($brand_sku);?>
</div>
</div>
@endif
      <hr>

	<div class="row">
		<div class="col-md-12">

@if(!empty($patient_record[0]->id))
<div class="panel panel-default">
  <div class="panel-heading"><strong>RECIEPT INFORMATION</strong><span style="float:right;font-size:15px">UPLOAD NO : <input type="hidden" id="upload_id_2" name="upload_id_2" value ='{{!empty($basicinfo->id)? $basicinfo->id :""}}' /><strong>{{!empty($basicinfo->id)? $basicinfo->id :""}}</strong></span></div>
    <div class="panel-body" style="font-size:12px;">
		<table class="table table-bordered col-md-12">
		<thead>
			<tr>
				<th>Patient Name</th>
				<th>Contact Number</th>
				<th>Medicine</th>
				<th>OR NO</th>
				<th>OR Date</th>
				<th>Uploaded</th>
				<th>SKU</th>
				<th>Action</th>
			</tr>	
			
		</thead>
		<tbody>@foreach($basicinfo as $upload_details)
			<tr>
				<td>{{!empty($upload_details->patient_name)? $upload_details->patient_name :""}}</td>
				<td>{{!empty($upload_details->mobile_number)? $upload_details->mobile_number :""}} / {{!empty($upload_details->phone_no)? $upload_details->phone_no :""}}</td>
				<td>{{!empty($upload_details->medicine)? $upload_details->medicine :""}}</td>
				<td>{{!empty($upload_details->receipt_details->or_number)? $upload_details->receipt_details->or_number :""}}</td>
				<td>{{!empty($upload_details->receipt_details->or_date)? $upload_details->receipt_details->or_date :""}}</td>				
				<td>{{!empty($upload_details->user->name)? $upload_details->user->name :""}}</td>
				
				
				<td>	{{!empty($upload_details->receipt_details->fetch_encoded_dtl->qty)? $upload_details->receipt_details->fetch_encoded_dtl->qty :""}}	
					@foreach($upload_details->receipt_details->fetch_encoded_dtl as $receipt_sku_details)
						@if(!empty($receipt_sku_details->qty))
						({{$receipt_sku_details->qty}}) {{$receipt_sku_details->sku_details->skuname}} <br/>
						@endif
					@endforeach

									
									</td>
				<td><?php if(!empty($upload_details->patient_name))
				{?>
				<a class="btn-info btn-sm glyphicon glyphicon-plus" onclick="add_receipt_to_patient($('#patient_id').val(),'{{$upload_details->id}}')" ></a> <span data-toggle="modal" onclick="$('#revert_receipt_id').val({{$upload_details->id}});" data-target="#revert_receipt"class="btn-info btn-sm glyphicon glyphicon-minus btn-danger pull-right"> </a></td>
				<?php } ?>
				
			</tr>
			@endforeach
		</tbody>
		</table>
	  
	 
	</div>
</div>
@endif


@if(!empty($patient_record[0]->id))
<div class="panel panel-default">
  <div class="panel-heading"><strong>History Redeem</strong><span style="float:right;font-size:15px"></span></div>
    <div class="panel-body" style="font-size:12px;">
		<table class="table table-bordered col-md-12 datatable">
		<thead>
			<tr>
				<th>CompletionDate</th>
				<th>Dispatch Date</th>				
				<th>Dispatch By</th>				
				<th>Product</th>
				<th>SKU</th>
				<th>Number Of Tabs</th>
				<th>Number Of Non-Med</th>
										
				<th>Date Redeem</th>
				
				
			</tr>	
			
		</thead>

							<tbody>
							<?php $total_redeem = "";?>
							<?php $number_redeem_for_the_month = "";?>
							@if(!empty($patient_record[0]))
							@foreach($patient_record[0]->fetch_redeem_product as $p)
							
								<tr>
									<td>{{!empty($p->completiondate)?$p->completiondate : ""}}</td>
									<td>{{!empty($p->dispatcheddate)?$p->dispatcheddate : ""}}</td>
									<td>{{!empty($p->created_by)?$p->created_by : ""}}</td>
									<td>{{!empty($p->sku_product->brand)?$p->sku_product->brand : ""}}</td>
									<td>{{!empty($p->sku_product->skuname)?$p->sku_product->skuname : ""}}</td>
									<td>{{!empty($p->qtytabs)?$p->qtytabs  : ""}}</td>
									<td>{{!empty($p->qtynonmed)?$p->qtynonmed : ""}}</td>
									<td>{{!empty($p->dispatcheddate)?$p->dispatcheddate : ""}}</td>	
												<?php /*@if(date('Y-m-d',strtotime($p->dispatcheddate)) > date('Y-m-d', strtotime("2018-01-31")))		*/ ?>
											<?php if(!empty($p->qtytabs)){$total_redeem +=  $p->qtytabs;
													
													$number_redeem_for_the_month += $p->qtytabs;
													} ?>													
												<?php /*		@endif */ ?>
								</tr>
								
								@endforeach
							 @endif  
								
							</tbody>
						</table>

	  
	 
	</div>
</div>
@endif	
		
		
		</div>
	</div>		
	
	<hr>
	  <?php $AppHelper = new AppHelper; ?>
		<?php //$psp_sku_program = $AppHelper->fetchTable( 'otsuka' . chr(92) . 'Psp_Program','id','sortBy'); ?>
		<?php $psp_skuprogram = $AppHelper->fetchSKUProgram($brand_sku_id); ?>
		

								<?php /*var_dump($psp_sku_program);// $ctr = 0; ;$total_qty = 0;$abilify_total ="";$amino_total="";$pleta_total=""; $array_brand = array();$brand_sku = array();//array(["ABILIFY"=>"","AMINOLEBAN"=>"","PLETAAL"=>""]); ?>
									@foreach ($psp_sku_program as $program)	
											<?php if($program->brandid == 1) //$brand_sku[5])
											{?>
											 {{$program}}
											<?php }?>
									@endforeach
								*/?>
								
								
								<?php 
									 $var_Total_redeemable="";
									 $var_programdesc ="";
									 $var_need_to_purchased ="";
									  $custome_var ="";
									 
								 if(!empty($psp_skuprogram))
								{

								 $var_sku_fee = $psp_skuprogram->fetch_program_desc->free;
								 $var_buy = $psp_skuprogram->fetch_program_desc->buy;
								 //$var_condition = $psp_skuprogram->fetch_program_desc->free;
								 $var_max_allow  = $psp_skuprogram->fetch_program_desc->max_alloc_to_patient;
									
									if(intval($total_qty / $var_buy) > 0 ){
									$var_Total_redeemable = ((intval($total_qty / $var_buy ) * $var_sku_fee)  - $total_redeem) ;//- ((($total_redeem / $var_sku_fee) * $var_buy))); 
									$custome_var = ($total_qty - ( 2 * $total_redeem ));
									}
								  if($var_buy < $total_qty ) 	{
								 $var_need_to_purchased = 0;//$var_buy - $total_qty;
									}
								  else 
									{$var_need_to_purchased = 0;}
								   
			
								 $var_excess = $var_Total_redeemable - $var_max_allow ;
								 $md_rd = true;
								 $pmm_approval = true;
								 $test_date = "";	
								 
										if(( $md_rd == true) && ($pmm_approval== true))
											{
												$var_ctr_limit	 = $var_Total_redeemable;
											}
										else if ($var_max_allow <= $var_Total_redeemable)
											{
												$var_ctr_limit	 = 	$var_max_allow;
											}
										else
											{
												$var_ctr_limit	 = $var_Total_redeemable;
											}
								 if($var_excess < 0 )
									{
										$var_excess = 0;
									}
								 $var_programdesc = $psp_skuprogram->fetch_program_desc->programname;
								 $var_programid  = $psp_skuprogram->fetch_program_desc->id;
								 $var_redeem_for_the_month = "";
								}
								else {
									
								$md_rd = false;	
								$pmm_approval ="";
								 $var_sku_fee = "";
								 $var_buy = "";							
								 $var_max_allow  = "";
								 $var_Total_redeemable = "" ;//- ((($total_redeem / $var_sku_fee) * $var_buy))); 
								 $var_need_to_purchased = "";
								 $var_ctr_limit	 = $var_Total_redeemable;	
								 $var_excess = "" ;
								 $test_date = "";	
								 $number_redeem_for_the_month = "";
									}?>
								





@if(!empty($patient_record[0]->id))
	 <div class="row" 
 
			style="<?php if (empty($patient_record[0]->fetch_supp_docs))
					{echo 'display:none';}?>"
				?>
 
		<div class="col-md-12" id="changeSKU"><?php //var_dump($psp_skuprogram->fetch_program_desc)?>
				<div class="panel panel-default">
				 <div class="panel-heading"><strong>GROUP BY PRODUCT</strong><span class="text-danger" style="font-size:12px;float:right"> *****NOTE AMINOLEBAN IS NOT APPLICABLE HERE</span>
					</div>
						<div class="panel-body" style="font-size:12px;">
							
							
							
						@if(!empty($List_of_product))
						@foreach($List_of_product as $key_product_list => $val_product_list) 
						
									<div class="col-md-12" >
										<div class="col-md-5"><h3>{{ $val_product_list->product_purchased}}</h3></div>
										
										
						
						
						
									</div>
						
									<div class="col-md-12" >
									
										 <div class="col-md-6">
												  <div class="form-group">
													
															
															
															<div class="col-sm-4">
															  <input type="text" class="form-control input-sm" value='{{$product_balance[$val_product_list->product_purchased]}}' readonly>
															</div>
															<label for="inputEmail3"  style="font-size: 22px;" class="col-sm-8 control-label">Balance</label>
															
															<div class="col-sm-4">
															  <input type="text" class="form-control input-sm" 
															  value=<?php echo $buy_array[$val_product_list->product_purchased] - $product_balance[$val_product_list->product_purchased]; ?>
															  
															  
															 readonly>
															</div>
															<label for="inputEmail3"  style="font-size: 22px;" class="col-sm-8 control-label">Need To Purchased</label>
															
													</div>
											
										  </div>
										  
										  <div class="col-md-6">
												  <div class="form-group">													
															<div class="col-sm-4">
															  <input type="text" class="form-control input-sm" value='{{$product_purchased_per_sku_list[$val_product_list->product_purchased]}}' readonly>
															</div>
															<label for="inputEmail3"  style="font-size: 14px;" class="col-sm-8 control-label ">Total Purchased</label>														
													</div>
													<div style="clear:both"></div>
													<div class="form-group">													
															<div class="col-sm-4">
															  <input type="text" class="form-control input-sm" value='{{$product_redeem_per_sku_list[$val_product_list->product_purchased]}}' readonly>
															</div>
															<label for="inputEmail3"  style="font-size: 14px;" class="col-sm-8 control-label ">Total Redeem</label>														
													</div>											
										  </div>
										  
										  
										
																	
										 <div class="col-md-8" style="margin-top:10px;">
												  <div class="form-group">
													
															<div class="col-sm-4">
															  <input type="text" class="form-control input-lg" value='{{$redeemable[$val_product_list->product_purchased]}}' readonly>
															</div>
															<label for="inputEmail3"  style="font-size: 22px;" class="col-sm-8 control-label ">Redeemble</label>
															<div style="clear:both"></div>
															<div class="col-sm-4">
															  <input type="text" class="form-control input-sm" value='{{$monthlimit_array[$val_product_list->product_purchased]}}' readonly>
															</div>
															<label for="inputEmail3"  style="font-size: 17px;" class="col-sm-8 control-label">Redeemable for the Month</label>
															<div style="clear:both"></div>
															<div class="col-sm-4">
															  <input type="text" class="form-control input-sm" value='{{$no_redeem_current_month[$val_product_list->product_purchased]}}' readonly>
															</div>
															<label for="inputEmail3"  style="font-size: 17px;" class="col-sm-8 control-label">No. Redeemed for the Month </label>

													</div>
											
										  </div>
										
										
									 </div>
									 
									
									
											
									
									
								
						
						
						
						@endforeach
						
						@endif
				
				
				
				
				
				
				
			
		
		
						</div>
				</div>
		</div>	
	</div>

	


	<?php $temp_var_total = 0; ?>	
	  <div class="row">
		<div class="col-md-12" id="programid_div"><?php //var_dump($psp_skuprogram->fetch_program_desc)?>
		
			<div class="panel panel-default">
			  <div class="panel-heading"><strong>Programs</strong><span style="float:right;font-size:15px" class="btn btn-warning btn-xs" onclick="$('#programid_div').removeClass('col-md-12').addClass('col-md-8');$('#support_div').show('slide', { direction: 'right' }, 1000);$('#toggle_span1').hide();$('#toggle_span2').show()" id="toggle_span1">View Supporting Docs</span>
					<span style="float:right;font-size:15px;display:none" class="btn btn-danger btn-xs" onclick="$('#programid_div').removeClass('col-md-8').addClass('col-md-12');$('#support_div').hide();$('#toggle_span1').show();$('#toggle_span2').hide()" id="toggle_span2">Hide Supporting Docs</span></div>
				<div class="panel-body" style="font-size:12px;">
				
						@if(!empty($List_of_Purchases_sku))
						@foreach($List_of_Purchases_sku as $key_purchased_list => $val_purchased_list) 
						<?php $var_new_balance =0;$var_new_balance_sandman=0;?>
							
							
						<div class="col-md-12" >
							<div class="col-md-5"><h3>{{$val_purchased_list->sku_purchased}} <input type="hidden" id="program_sku_purchases<?php if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id;}
											?>" value="{{$val_purchased_list->sku_purchased}}"></h3></div><div class="col-md-5"><h3><?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->max_alloc_to_patient))
														{echo $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->programname;}
											?></h3></div>
							<?php /* @if($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] > $product_redeem_per_sku_list[$val_purchased_list->sku_purchased]) */?>
							<div class="row" style="display:none">
									<div class="col-md-6">
										<h5><?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased . "SANDMAN"]->fetch_program_desc->max_alloc_to_patient))
														{echo $sku_program_dtl[$val_purchased_list->sku_purchased  . "SANDMAN"]->fetch_program_desc->programname;}
											?></h5>
										<div class="col-md-12"> <input type="text" value="{{$product_purchased_per_sku_list[$val_purchased_list->sku_purchased . 'SANDMAN']}}" disabled> SANDMAN Total Product Purchase<br/></div>						
										<div class="col-md-12"> <input type="text" value="{{$product_redeem_per_sku_list[$val_purchased_list->sku_purchased  . 'SANDMAN']}}" disabled> SANDMAN  Total Redeem QTY <br/></div>  
										<div class="col-md-12"> <input type="text" value="<?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased  . 'SANDMAN']->fetch_program_desc->buy))
																	{
																$var_new_balance_sandman = $product_purchased_per_sku_list[$val_purchased_list->sku_purchased . 'SANDMAN'] % $sku_program_dtl[$val_purchased_list->sku_purchased . "SANDMAN"]->fetch_program_desc->buy ;
																echo $product_purchased_per_sku_list[$val_purchased_list->sku_purchased . 'SANDMAN'] % $sku_program_dtl[$val_purchased_list->sku_purchased . "SANDMAN"]->fetch_program_desc->buy ;}
														?>" disabled> Balance<br/> <br/> <br/></div>  
									</div>
									<div class="col-md-6">
										<h5><?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->max_alloc_to_patient))
														{echo $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->programname;}
											?></h5>
										<div class="col-md-12"> <input type="text" value="{{$product_purchased_per_sku_list[$val_purchased_list->sku_purchased]}}" disabled>MSE Total Product Purchase<br/></div>													 
										 <div class="col-md-12"> <input type="text" value="{{$product_redeem_per_sku_list[$val_purchased_list->sku_purchased]}}" disabled>MSE  Total Redeem QTY <br/></div>  
										 <div class="col-md-12"> <input type="text" value="<?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy))
																{echo intval($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] % $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy) ;}
													?>" disabled>MSE  Balance <br/></div>  
									</div>	
							</div>
							
							<div class="row">
									<div class="col-md-9">
										<div class="col-md-12"> <input type="text" id="program_total_purchase<?php if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id;}
											?>" value="{{$product_purchased_per_sku_list[$val_purchased_list->sku_purchased . 'SANDMAN'] + $product_purchased_per_sku_list[$val_purchased_list->sku_purchased]}}" disabled> Total Product Purchase<br/></div>						
										<div class="col-md-12"> <input type="text" id="program_total_redeem<?php if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id;}
											?>" value="{{$product_redeem_per_sku_list[$val_purchased_list->sku_purchased  . 'SANDMAN'] + $product_redeem_per_sku_list[$val_purchased_list->sku_purchased] }}" disabled>  Total Redeem QTY <br/></div>  
										<div class="col-md-12" style="font-size:20px;"> <input type="text" style="font-size:20px;width:100px" id="program_newBalanace<?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->max_alloc_to_patient))
														{echo $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->id;}
											?>" value="<?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy))
															{
																	if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased  . 'SANDMAN']->fetch_program_desc->buy))
																	{
																	echo $product_purchased_per_sku_list[$val_purchased_list->sku_purchased . 'SANDMAN'] % $sku_program_dtl[$val_purchased_list->sku_purchased . "SANDMAN"]->fetch_program_desc->buy + intval($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] % $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy);
																	$var_new_balance = $product_purchased_per_sku_list[$val_purchased_list->sku_purchased . 'SANDMAN'] % $sku_program_dtl[$val_purchased_list->sku_purchased . "SANDMAN"]->fetch_program_desc->buy + intval($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] % $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy); 
																	}
																	else
																	{
																	
																	echo intval($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] % $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy) ;
																	intval($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] % $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy) ;
																	}//$var_new_balance = 0;}//intval($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] % $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy) ;}
																	
															}
															
															//{echo intval($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] % $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy);}
														?>" disabled> Balance<br/> <br/> <br/></div>  
									</div>
									<div class="col-md-3">
									@if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy))
										<span class="btn btn-info btn-xs" onclick="OpenViewUploadedDocs(<?php  if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id . ",'"   . $val_purchased_list->sku_purchased . "'" .",'" . $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->id . "'";} ?>)" data-toggle="modal" data-target="#assign_rx_modal">ATTACH MD RX </span><br/><br/>
										<!--<span class="btn btn-info btn-xs" onclick="SendPMM(<?php  if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id . ",'"   . $val_purchased_list->sku_purchased . "'" .",'" . $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->id . "'";} ?>)">Attached PMM Approval</span> -->
										<span class="btn btn-danger btn-xs" data-toggle="modal" onclick="UpdateFIELD(<?php  if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id . ",'"   . $val_purchased_list->sku_purchased . "'" .",'" . $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->id . "'";} ?>)" data-target="#SendPMM_Modal">SEND PMM APPROVAL</span>
									@endif
									</div>
							</div>
								<div class="col-md-5">
								 
								
								  <input type="text" class="" id="program_redeemable<?php if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id;}
											?>" style="width:60px;font-size:30px"  readonly disabled value="<?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy))
																{  
																	{
																		echo	(intval( ($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] + $var_new_balance_sandman) / $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy) * $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->free) - $product_redeem_per_sku_list[$val_purchased_list->sku_purchased];		
																		$redeemable_total  =	(intval( ($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] + $var_new_balance_sandman) / $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy) * $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->free) - $product_redeem_per_sku_list[$val_purchased_list->sku_purchased];

																		$temp_var_total +=	(intval( ($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] + $var_new_balance_sandman) / $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy) * $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->free) - $product_redeem_per_sku_list[$val_purchased_list->sku_purchased];		
																		$redeemable_total  =	(intval( ($product_purchased_per_sku_list[$val_purchased_list->sku_purchased] + $var_new_balance_sandman) / $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy) * $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->free) - $product_redeem_per_sku_list[$val_purchased_list->sku_purchased];	
																	} 
																	
																	
																}
													?>"> No. Of Redeemable  <br/>
								  <input type="text" class="" id="program_Redeemable_for_month<?php if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id;}
											?>" style="width:60px" readonly disabled value="<?php 
																if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->max_alloc_to_patient))
																{echo $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->max_alloc_to_patient - $product_redeem_per_sku_list_forthemonth[$val_purchased_list->sku_purchased] ;}
													?>"> Redeemable for the Month<br/> 
								  <input type="text" class="" id="program_redeeme_for_month<?php if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id;}
											?>" style="width:60px"  readonly disabled value="{{$product_redeem_per_sku_list_forthemonth[$val_purchased_list->sku_purchased]}}"> No. Redeemed for the Month <br/>
								 </div> 
						
								<div class="col-md-4">
								  <input type="text" class="" id="program_need<?php if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id;}
											?>" style="width:60px"  readonly disabled value="<?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy))
																{echo  $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->buy - $var_new_balance ;}
													?>"> Need To Purchase <br/>
									<input type="text" class="" id="program_allow<?php if(!empty($sku_details[$val_purchased_list->sku_purchased]->id)) {echo $sku_details[$val_purchased_list->sku_purchased]->id;}
											?>" style="width:60px" readonly disabled  
												value="<?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->max_alloc_to_patient))
																{echo $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->max_alloc_to_patient;}
													?>"> Allowed<br/> 
								  <input type="text" class="" style="width:60px"  readonly disabled value="<?php if(!empty($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->max_alloc_to_patient))
																{ if(($sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->max_alloc_to_patient) < $redeemable_total)
																	{echo $redeemable_total - $sku_program_dtl[$val_purchased_list->sku_purchased]->fetch_program_desc->max_alloc_to_patient;}
																  else{
																	echo "0";
																	}
																}
													?>"> Excess <br/>
								 </div> 
								 <?php /* @endif */?>

								
								<div class="col-md-3" >
									
								   <!--<input type="checkbox" class="" readonly  <?php if($md_rd == true) { echo "checked";}?>> <span  >MD RX </span>  <br/><br/> 
								   <input type="checkbox" class="" readonly  <?php if($pmm_approval == true) { echo "checked";}?> > <span  >PMM Approval</span>  <br/> <br/> 			
								  -->

									
									 			
								 </div> 
								
				
						

							</div>		


						@endforeach
						<div class="col-md-12" >
						<hr />
						<input type="text" class=""  style="width:90px;font-size:60px;margin-top:20px" readonly="" disabled="" value="{{$temp_var_total}}">
						<span style="width:90px;font-size:50px;margin-top:20px">TOTAL REDEEMABLE</span><p style="    color: #a94442;    background-color: #f2dede;    border-color: #ebccd1">  (note Not Applicable if Different Product)</p>
						</div>
						@endif


			</div>
			</div>

					
		</div>
		<div class="col-md-4" id="support_div" style="display:none">
			<div class="panel panel-default">
			  <div class="panel-heading"><strong>Supporting Docs</strong><span style="float:right;font-size:15px"></span></div>
				<div class="panel-body" style="font-size:12px;">
						<div style="border:1px solid;overflow:auto;max-height:987px" >
						@if(!empty($upload_supporting_docs))	
							@foreach($upload_supporting_docs as $supp_docs_key => $supp_docs_val)
							
							</p><img style="max-width:250px"src="{{$baseurl}}uploads/supportingdocs/{{$supp_docs_val->filename}}"/></p>
							@endforeach
						@endif


						</div>
				</div>
			</div>
		</div>


	  </div>
@endif
@if(!empty($patient_record[0]->id))
<h3 style="text-align:center"><span class="btn btn-sm btn-primary text-center" onclick="createddispatchmst();$('#dispatching_row').show();"/>Process Dispatching </span><input type="hidden" id="dispatchmst_id" /></h3>
	<hr/> 
		<div class="row" id="dispatching_row" style="display:none">
			<div class="col-md-12">
			<div class="panel panel-default">
			  <div class="panel-heading"><strong>DISPATCHING</strong><span style="float:right;font-size:15px"></span></div>
				<div class="panel-body" style="font-size:12px;">
				
					 
				<div class="col-md-4" >Non Medical Freebies <br/>
						@foreach($Sku_list as $skukey => $skuval)
								@if(($skuval->is_non_med == "true") && ($skuval->active == 1))
										
									<input type="checkbox" name="non-med-freebies[]" class="non-med-freebies" value="{{$skuval->id}}" data-skuname="{{$skuval->skuname}}"> {{$skuval->skuname}}	<br/>
								@endif		
							@endforeach																					
				 
				 </div> 
				<div class="col-md-8" >
					  <select class="input-lg" id="dispatching_sku" > 
							@if(!empty($List_of_Purchases_sku))
							@foreach($List_of_Purchases_sku as $key_purchased_list => $val_purchased_list)
						<option value="{{$sku_details[$val_purchased_list->sku_purchased]->id}}" >{{$val_purchased_list->sku_purchased}}</option>
							@endforeach
							@endif
					  </select>
					 			
					 <input type="number"class="input-lg" id="redeemable_tabs"> 		
					 <?php /* <select class="input-lg" id="redeemable_tabs"> 
						<?php for($var_ctr = 1; $var_ctr <= $var_ctr_limit;$var_ctr++ ){?>
						<option value="{{$var_ctr}}">{{$var_ctr}}</option>
						<?php }?>
					 </select>  */ ?>
					 <span href="#" class="btn btn-info" onclick="Add_Redeemable_tabs()" data-skudesc="{{$brand_sku_desc}}" data-skuid="{{$brand_sku_id}}" >ADD</span> 	
					<!--  <input type="number" min="1" max="5" style="width: 100px" class="input-lg"/> -->
					 <strong># OF TABS</strong>
					<div class="panel panel-default">
					  <div class="panel-heading">Redeem Details</div>
					  <div class="panel-body" style="max-height:250px;overflow:auto" >  
						<table class="table table-bordered" id="sku-table">
							<thead>
							<tr>
							<th>SKU</th>
							<th>QTY</th>
							<th>#</th>
							<thead>
							</tr>
							<tbody>

							</tbody>
							
						</table>
					  </div>					
				  </div>
				 
				 </div> 	
			
			</div>
		</div>
				

		</div>

		</div>
@endif
	

		<div class="row">
			@if(!empty($patient_record[0]->id))
			<div class="col-md-6">		
				  <div class="col-md-12"><span  class="btn btn-primary"  onclick="dispatch_post()">POST</span></div>
			</div>
			@endif
			<div class="col-md-3" style="position:fixed;bottom:0px;right:0px;background-color:#f2f2f2;padding:5px">
					<!--
					<div class="col-md-8" style="">
							<div class="form-group">
								<label for="sort" class="col-sm-2 control-label" style="margin-top:10px"> STATUS: </label>
								<div class="col-sm-10">
									<select class="form-control" name="sort" id="Redemption Status" onchange="alert('eeeee')">
										<option value="1">Process</option>
										<option value="1">MD RX UPLOADING</option>
										<option value="12">FOR PMM APPROVAL</option>

									</select>
								 </div>
							</div>
										
					</div>	
					-->
					<div class="col-md-4 col-md-offset-8">
						<a href="<?php echo $baseurl;?>otsuka/agent/redemption-queue" class="btn btn-info btn-sm"  >Save And Exit</a>		
					</div>
			</div> 
			
		</div>
		<br/><br/>
    </div>
</div>






	<!-- Modal -->
	<div id="assign_rx_modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			
		  </div>
		  <div class="modal-body" id="UploapSupportingDocsDiv">
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>


		<!-- Modal -->
	<div id="revert_receipt" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">Revert Receipt <input type="text" id="revert_receipt_id"/>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			
		  </div>
		  <div class="modal-body" id="RevertReceipts">
			
			{{--
			  <div class="form-group">
				<label for="exampleSelect1">Invalid</label>
				<select class="form-control" id="exampleSelect1">
					 <option>* Please Select Reason</option>
					<option value="Duplicated Receipts (Within The Day)">Duplicated Receipts (Within The Day)</option>
					<option value="Duplicated Receipts">Duplicated Receipts</option>
					<option value="Wrong Upload">Wrong Upload</option>
					<option value="No SKU">No SKU Encoded</option>
					<option value="Blurred Photo/ Unreadable">Blurred Photo/ Unreadable</option>
					<option value="No Transaction">No Transaction</option>
					<option value="No OR Number ">No OR Number </option>
				</select>
			  </div> --}}
			  
			  <div class="form-group">
				<label for="exampleSelect1">Remarks</label>
				<textarea class="form-control" name="revert_receipt_remarks" id="revert_receipt_remarks"> </textarea>
			  </div>
			
			
			
			
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="revertReceipt($('#revert_receipt_id').val())">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>






	<!-- Modal -->
	<div id="SendPMM_Modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		<form />
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">PMM APPROVAL MODULE
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			
		  </div>
		  <div class="modal-body" >
			<span>Attach Rx <input type="hidden" id="upload_supporting_docs_list"><input type="hidden" id="skuid_PMM" ><input type="hidden" id="progdesc_PMM" ><input type="hidden" id="programid_PMM" ></span>
			<table class="table table-bordered"  style="font-size:12px;">
				<thead>
					<tr>
			
						<th>[]</th>
						<th>Upload ID</th>
						<th>Uploaded To</th>
						<th>Remarks</th>
						<th>Images</th>
					</tr>
				</thead>
			<tbody>
						@if(!empty($upload_supporting_docs))	
							@foreach($upload_supporting_docs as $supp_docs_key => $supp_docs_val)
							<tr>
								<td><input type="checkbox" class="upload_supporting_docs_list" name="upload_supporting_docs_list" value="{{$supp_docs_val->id}}"/></td>
								<td>{{$supp_docs_val->id}}</td>
								<td>{{$supp_docs_val->uploaded_to}}</td>
								<td>{{$supp_docs_val->remarks}}</td>
								<td><a href="{{$baseurl}}uploads/supportingdocs/{{$supp_docs_val->filename}}" ><img style="max-width:200px" src="{{$baseurl}}uploads/supportingdocs/{{$supp_docs_val->filename}}"/></a></td>
								<!--<td><a href="{{$baseurl}}uploads/supportingdocs/{{$supp_docs_val->filename}}" >View</a></td> -->								
							</tr>	
							@endforeach
						@endif	
						
					
					
				
			</tbody>
			</table>
			
			<span>Attach Receipt <input type="hidden" id="encoded_validated_list"></span>
			<table id="" class="display datatable table-bordered" style="font-size:11px" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>[]</th>
									<th>Date Process</th>
									<th>OR #</th>
									<th>OR Date</th>
									<th>FilePath</th>
									<th>Redeemed Items</th>
									<th>QTY</th>
									
									
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>[]</th>
									<th>Date Process</th>
									<th>OR #</th>
									<th>OR Date</th>
									<th>FilePath</th>
									<th>Redeemed Items</th>
									<th>QTY</th>
									
								</tr>
							</tfoot>
							<tbody>
								
								@if(!empty($Encoded_Purchases))
								<?php foreach($Encoded_Purchases as $key => $val){
								
								?>
								<tr >
									<td><input type="checkbox" value="{{$val->id}}" class="encoded_validated_list" name="purchased_id_list"/></td>
									<td>@if(!empty($val->created_at))  
										{{$val->created_at}}
										@else
											PROCESS BY SANDMAND
										@endif
								
									</td>
									<td>{{$val->or_number}}</td>
									<td>{{$val->or_date}}</td>									
									<td>{{$val->file_path}}</td>
									<td>
									@if(!empty($val->fetch_encoded_dtl))
									<?php  foreach($val->fetch_encoded_dtl as $key2 => $val2)
													{	
														if(!empty($val2->sku_details->skuname)){
														echo $val2->sku_details->skuname . '<br/>';
														}
													} ?>
									@endif
									
									</td>
									
									<td>
											<?php foreach($val->fetch_encoded_dtl as $key2 => $val2)
													{	
														echo '( ' . $val2->qty . ' ) ' . '<br/>';
	
													}?>								
									</td>
									
									
								</tr>
								<?php } ?>
							   @endif
								
							</tbody>
						</table>
					<br/><br/>
					<div class="col-md-8" id="loading-page">
						 <!--<table class="table table-bordered" style="text-align:left">
							<thead>
								<th class="col-md-3">Label</th>
								<th class="col-md-9">Value</th>
								
							</thead>
							<tr>
								<td>Total Receipt</td>
								<td><input type="text" id="" value=""/></td>								
							</tr>
								
							<tr>
								<td>Total Receipt</td>
								<td><input type="text" id="" value=""/></td>							
							</tr>
									
						</table>
						-->
					</div>
					<br/><br/><br/><br/><br/><br/>		
		  </div>
		  <div class="modal-footer">

			

			<button type="button" class="btn btn-default btn-info" onclick="AttachReceipt();AttachRx();SENDPMM_with_Attach($('#skuid_PMM').val(),$('#progdesc_PMM').val(),$('#programid_PMM').val(),$('#encoded_validated_list').val(),$('#upload_supporting_docs_list').val())">SEND</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>












      






      





@endsection 

@section('footer-scripts')
<script>

		function RevertReceiptFunction_save( upload_id) {
			$("#revert_receipt_id").val(upload_id);
		  //var x = $("#redemption_call").serialize();		
			
		  /* var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				
				
				//alert('Call Logs has been Save');
				document.getElementById("UploapSupportingDocsDiv").innerHTML = xhttp.responseText;
				//UploadSupportingDocsTable
				$('.datatabledraw').DataTable(); 
				
			}
		  };
		//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/savecalllog?person_filter="  + "&person="  + x, true);
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/uploader/supporting-docs-view?patient_id=" + $('#patient_id').val() + "&skuid=" +skuid + "&programid=" + var3, true);
		  xhttp.send(); */
		}
			


	
	
	function UpdateFIELD(var1,var2,var3){
		$('#skuid_PMM').val(var1);
		$('#progdesc_PMM').val(var2);
		$('#programid_PMM').val(var3);
	}
	
		
	function SENDPMM_with_Attach(skuid,progdesc,programid,receipt_id_list,supp_docs_id) {
				patient_id = $("#patient_id").val();	
				//supp_docs_id = '19';//$("#patient_id").val();	
				//receipt_id_list = '131417';//$("#patient_id").val();	
				document.getElementById("loading-page").innerHTML = '<h2>Please Wait Sending Email</h2><img style="max-height:50px;" src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif">';
			  	upload_id = $("#upload_id").val();							
				var total_dispatch_for_the_month = $('#program_redeeme_for_month' + skuid).val();
				var total_free_goods_for_redemption = $('#program_redeemable'+ skuid).val(); 
				var total_qty_purchased = $('#program_total_purchase' + skuid).val(); 
				var total_total_redeem = $('#program_total_redeem' + skuid).val(); 
				var sku_purchases = $('#program_sku_purchases' + skuid).val() ;
				var program_name_id = programid;
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {	
				
				alert('Auto Email Has been Sent');
				$("#SendPMM_Modal .close").click()
				}
				};
				//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_receipt_to_patient?patient_id=" + patient_id + "&upload_id=" + upload_id + "&assign_upload_id=" + assign_upload_id, true);
				xhttp.open("GET", "<?php echo $baseurl;?>otsuka/uploader/send-max-rx?patient_id=" + patient_id + "&supp_docs_id=" + supp_docs_id + "&receipt_id_list="+ receipt_id_list  + "&skuid=" + skuid +   "&total_dispatch_for_the_month=" + total_dispatch_for_the_month + "&total_free_goods_for_redemption=" + total_free_goods_for_redemption + "&total_qty_purchased=" + total_qty_purchased + "&sku_purchases=" + sku_purchases + "&program_name_id=" + program_name_id + "&total_total_redeem=" + total_total_redeem , true);
				xhttp.send();		 
				
			
	}


	function AttachReceipt() {
		var str = "";
		var js_val = "";
				 $(".encoded_validated_list:checked").each(function() {
					str += $(this).val() + ",";
					js_val += $(this).data("") + ",";

			  });
			  str = str.substr(0, str.length - 1);
			  $('#encoded_validated_list').val(str);
			
				
		
			 
		}
	function AttachRx() {
		var str = "";
				 $(".upload_supporting_docs_list:checked").each(function() {
					str += $(this).val() + ",";
			  });
			  str = str.substr(0, str.length - 1);
			  $('#upload_supporting_docs_list').val(str);
			
				
		//$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id=" + str);
			 
		}









	

	function SaveRedemption() {
		
		upload_id = $("#upload_id").val();			
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {	
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/agent/redemption-queue");
		//document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		//alert('Success');
		//document.getElementsByTagName("body").innerHTML = xhttp.responseText;
		}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/done?upload_id=" + upload_id, true);
		xhttp.send();					 
		}
	
	
	function revertReceipt(receipt_upload_id) {
			//alert('test');
			  	upload_id = $("#upload_id").val();		
				patient_id = $("#patient_id").val();
				revert_remarks =  $("#revert_receipt_remarks").val();
				//$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?n&upload_id=" + upload_id + "&patient_id=" + patient_id  );
							
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {	
				
				alert('Receipt has Revert to the Upload Queue');
				$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemptionv2&upload_id=" + upload_id + "&patient_id=" + patient_id  );
				//$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id);
				}
				};
				xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/revert-receipt?patient_id=" + patient_id + "&upload_id=" + upload_id + "&receipt_upload_id=" + receipt_upload_id + "&revert_remarks=" + revert_remarks	, true);
				xhttp.send();		  		

	}



	function add_receipt_to_patient(patient_id,assign_upload_id) {
			//alert('test');
			  	upload_id = $("#upload_id").val();							
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {	
				//$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id);
				alert('Receipt has Been assign');
				$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemptionv2&upload_id=" + upload_id + "&patient_id=" + patient_id  );
				//$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id);
				}
				};
				xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_receipt_to_patient?patient_id=" + patient_id + "&upload_id=" + upload_id + "&assign_upload_id=" + assign_upload_id, true);
				xhttp.send();		  		

	}
	function saveRedemptions_Patient(patient_id,upload_id) {
		
		upload_id = $("#upload_id").val();			
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {	 
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemptionv2&upload_id=" + upload_id + "&patient_id=" + patient_id  );
		//$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id  + "&test" + upload_id);
		//document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		//alert('Success');
		//document.getElementsByTagName("body").innerHTML = xhttp.responseText;
		}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id, true);
		xhttp.send();					 
		}

	function UpdatePatient_details() {
		
		
		var x = $("#Edit-Patient-form").serialize();
		upload_id = $("#Edit-Patient-form").val();			
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {	 
	//	$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id=" + upload_id + "&patient_id=" + patient_id  );
		//$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id  + "&test" + upload_id);
		//document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		//alert('Success');
		alert('Record has been Updated');
		//document.getElementsByTagName("body").innerHTML = xhttp.responseText;
		}
		};
		//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id, true);
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/enroll_patient?action=edit&"   + x , true);
		xhttp.send();					 
		
		}		
		function UpdatePatient_med_details() {
		
		
		var x = $("#Edit-Patient-med-form").serialize();
		patient_id = $("#patient_id").val();			
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {	 
			alert('Updated Medical Information');
		}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/update_patient_med_info?action=edit&patient_id=" + patient_id + "&"   + x , true);
		xhttp.send();					 
		
		}		



	function assign_rx_upload(skuid,patient_id,assign_upload_id,programid) {
			//
				document.getElementById("UploapSupportingDocsDiv").innerHTML = '<h2>Please Wait Sending Email</h2><img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif">';
			  	upload_id = $("#upload_id").val();							
				var total_dispatch_for_the_month = $('#program_redeeme_for_month' + skuid).val();
				var total_free_goods_for_redemption = $('#program_redeemable'+ skuid).val(); 
				var total_qty_purchased = $('#program_total_purchase' + skuid).val(); 
				var total_total_redeem = $('#program_total_redeem' + skuid).val(); 
				var sku_purchases = $('#program_sku_purchases' + skuid).val() ;
				var program_name_id = programid;
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {	
				
				alert('Supporting Docs Has Been Attach and Auto Email Has been Sent');
				$("#assign_rx_modal .close").click()
								
				}
				};
				//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_receipt_to_patient?patient_id=" + patient_id + "&upload_id=" + upload_id + "&assign_upload_id=" + assign_upload_id, true);
				xhttp.open("GET", "<?php echo $baseurl;?>otsuka/uploader/assign-patient-rx?patient_id=" + patient_id + "&uploaded_id=" + assign_upload_id + "&total_dispatch_for_the_month=" + total_dispatch_for_the_month + "&total_free_goods_for_redemption=" + total_free_goods_for_redemption + "&total_qty_purchased=" + total_qty_purchased + "&sku_purchases=" + sku_purchases + "&program_name_id=" + program_name_id + "&total_total_redeem=" + total_total_redeem , true);
				xhttp.send();		  		
			
	}
	
	
	function SendPMM(skuid,progdesc,programid) {
				
				patient_id = $("#patient_id").val();	
				supp_docs_id = '19';//$("#patient_id").val();	
				receipt_id_list = '131417';//$("#patient_id").val();	
				document.getElementById("UploapSupportingDocsDiv").innerHTML = '<h2>Please Wait Sending Email</h2><img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif">';
			  	upload_id = $("#upload_id").val();							
				var total_dispatch_for_the_month = $('#program_redeeme_for_month' + skuid).val();
				var total_free_goods_for_redemption = $('#program_redeemable'+ skuid).val(); 
				var total_qty_purchased = $('#program_total_purchase' + skuid).val(); 
				var total_total_redeem = $('#program_total_redeem' + skuid).val(); 
				var sku_purchases = $('#program_sku_purchases' + skuid).val() ;
				var program_name_id = programid;
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {	
				
				alert('Auto Email Has been Sent');
				/*$("#assign_rx_modal .close").click()*/
								
				}
				};
				//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_receipt_to_patient?patient_id=" + patient_id + "&upload_id=" + upload_id + "&assign_upload_id=" + assign_upload_id, true);
				xhttp.open("GET", "<?php echo $baseurl;?>otsuka/uploader/send-max-rx?patient_id=" + "&supp_docs_id=" + "&receipt_id_list"+  patient_id + "&skuid=" + skuid +   "&total_dispatch_for_the_month=" + total_dispatch_for_the_month + "&total_free_goods_for_redemption=" + total_free_goods_for_redemption + "&total_qty_purchased=" + total_qty_purchased + "&sku_purchases=" + sku_purchases + "&program_name_id=" + program_name_id + "&total_total_redeem=" + total_total_redeem , true);
				xhttp.send();		  		
			
	}
		

$(function() {
  $('#btnLaunch').click(function() {
    $('#myModal_Search_MD').modal('show');
  });

  $('#btnSave').click(function() {
    var value = $('input').val();
    $('#doctor_name').val(value);;
    $('#myModal_Search_MD').modal('hide');
  });
});		
		

function loadDoc(filter,person) {
	$('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
        document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		$('.datatabledraw').DataTable(); 
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchpatient?person_filter=" + filter + "&person=" + person, true);
  xhttp.send();
}

function loadDoc2(filter,person) {
  var empcode = $('#filtered2').val();
  $('#filterdisplay2').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
        document.getElementById("filterdisplay2").innerHTML = xhttp.responseText;
    	$('.datatabledraw').DataTable(); 
    	


    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchperson?person_filter=" + filter + "&person=" + person + "&emp_code=" + empcode, true);
  xhttp.send();
}


function SaveCallLOgs() {
  var x = $("#redemption_call").serialize();	
  var	upload_id = $("#upload_id").val();			
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		//$('.datatabledraw').DataTable().draw(); 
		alert('Call Logs has been Save');
        //document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/savecalllog?person_filter="  + "&person="  + x + "&upload_id="  + upload_id, true);
  xhttp.send();
}



function OpenViewUploadedDocs(skuid , var2 , var3) {
  //var x = $("#redemption_call").serialize();		
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
		//alert('Call Logs has been Save');
        document.getElementById("UploapSupportingDocsDiv").innerHTML = xhttp.responseText;
		//UploadSupportingDocsTable
		$('.datatabledraw').DataTable(); 
		
    }
  };
//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/savecalllog?person_filter="  + "&person="  + x, true);
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/uploader/supporting-docs-view?patient_id=" + $('#patient_id').val() + "&skuid=" +skuid + "&programid=" + var3, true);
  xhttp.send();
}








function createddispatchmst() {
		
					
		//alert();
		
		// ********************************************************************************************* CREATE HERE THE YTD REDEMPTION
		 var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {	
		//$(location).attr('href', "<?php echo $baseurl;?>otsuka/agent");
		//document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		//alert('Success');
		$("#dispatchmst_id").val($.trim(xhttp.responseText));
		//document.getElementsByTagName("body").innerHTML = xhttp.responseText;
		}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-mst-save?", true);
		xhttp.send();			  
		
		}

function Add_Redeemable_tabs() {
	
var skudesc = $("#dispatching_sku option:selected").text() ;
var skuid = $("#dispatching_sku option:selected").val();
alert();	
//$( "#sku-table tbody" ).append( "<tr id='sku"+ dispatching_sku +"'><td>" + $("#dispatching_sku option:selected").text() + "</td><td>" + $("#redeemable_tabs").val() +"</td><td><a class='btn-info btn-xs' onclick='removeappendsku("+ skuid +")'>x</a></td></tr>" );		
// ********************************************************************************************* SAVING SKU TABS REDEMP
var x = $("#dispatchmst_id").val();		 
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {

		//$( "#sku-table tbody" ).append( "<tr id='sku"+ skuid +"'><td>" + skudesc + "</td><td>" + $("#redeemable_tabs").val() +"</td><td><a class='btn-info btn-xs' onclick='removeappendsku("+ skuid +")'>x</a></td></tr>" );		
		$( "#sku-table tbody" ).append( "<tr id='sku"+ x + skuid +"'><td>" + $("#dispatching_sku option:selected").text() + "</td><td>" + $("#redeemable_tabs").val() +"</td><td><a class='btn-info btn-xs' onclick='removeappendsku("+ skuid +")'>x</a></td></tr>" );		
		//$('.datatabledraw').DataTable().draw(); 
		//alert('Call Logs has been Save');
        //document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + ""+ "&sku_id=" + skuid +  "&qtytabs=" + $("#redeemable_tabs").val() + "&patient_id=" + $('#patient_id').val(), true);
  xhttp.send(); 
  
}



function dispatch_post() {
	
 //

var x = $("#dispatchmst_id").val();		 
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {

		//$( "#sku-table tbody" ).append( "<tr id='sku"+ skuid +"'><td>" + skudesc + "</td><td>" + $("#redeemable_tabs").val() +"</td><td><a class='btn-info btn-xs' onclick='removeappendsku("+ skuid +")'>x</a></td></tr>" );		
		
		//$('.datatabledraw').DataTable().draw(); 
			$(location).attr('href', "<?php echo $baseurl;?>otsuka/agent/redemption-queue");
		alert('Dispatching has been Posted');
        //document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-post?dsid_mst="  + x + ""+ "&patient_id=" + $('#patient_id').val(), true);
  xhttp.send(); 
}



function removeappendsku(skuid) {
		
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		$('#sku' + skuid).remove();
	
		
    }
  };
//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/savecalllog?person_filter="  + "&person="  + x, true);
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/receipt/encode-item-dtl-remove?id=" + id , true);
  xhttp.send(); 
}

function disableDoctor() {
  // Get the checkbox
  var checkBox = document.getElementById("doctor_wont_disclose");
  // Get the output text
  
  if (checkBox.checked == true){
		
		$("#patient_tagging_doctor_name").val("");
		
		$("#patient_tagging_hospital").val("");
		$("#patient_tagging_md_class").val("");
		$("#patient_tagging_specialty").val("");
		$("#patient_tagging_doctorid").val("");
		$("#patient_tagging_emp_doctorid").val("");
		//document.getElementById("md_name").value()

		$("#md_name").val("");
		$("#hospital").val("");
		$("#doctor_id").val("");
  } 
}


function useMD(id) {
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		//document.getElementById(xhttp.responseText['id']);
		
        //alert(xhttp.responseText['id']);
		var obj = JSON.parse(xhttp.responseText);
		
		
		$("#patient_mr_id").val(obj.empcode); 
		$("#patient_mr_name").val(obj.mrname);
		$("#patient_tagging_team").val(obj.etmscode);
		$("#patient_tagging_area_code").val(obj.areacode);
		$("#patient_tagging_doctor_name").val(obj.mdname);
		$("#doctor_wont_disclose").prop('checked', false);
		$("#patient_tagging_hospital").val(obj.hospital);
		$("#patient_tagging_md_class").val(obj.mdclass);
		$("#patient_tagging_specialty").val(obj.specialty);
		$("#patient_tagging_doctorid").val(obj.doctorid);
		$("#patient_tagging_emp_doctorid").val(obj.employee);
		//document.getElementById("md_name").value()

		$("#mr_id").val(obj.empcode);
		$("#mr_name").val(obj.mrname);
		$("#md_name").val(obj.mdname);
		$("#hospital").val(obj.hospital);
		$("#doctor_id").val(obj.doctorid);
		
		
		
		
		
        //alert(obj.mdname);
		
    }
  };


  //xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + ""+ "&sku_id=" + skuid +  "&qtytabs=" + $("#redeemable_tabs").val() + "&patient_id=" + $('#patient_id').val(), true);
  xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/get_national_md_record?national_md_id=" + id , true);
  xhttp.send(); 
}





$(document).ready(function() {
	
$('input#check_box_updated').click(function() {
  if ($(this).is(':checked')) {
		$(".inputrecord").attr('disabled', true); 	
		$("#update_patient_info_btn").slideUp();

  }
else
	{		
		$(".inputrecord").removeAttr("disabled");
		$("#update_patient_info_btn").slideDown();
		

	}
});


$('input#check_box_updated_med').click(function() {
  if ($(this).is(':checked')) {
		$(".inputrecord_med").attr('disabled', true); 	
		$("#update_patient_info_btn2").slideUp();
		$("#btnLaunch").slideUp();
		

  }
else
	{		
		$(".inputrecord_med").removeAttr("disabled");
		$("#update_patient_info_btn2").slideDown();
		$("#btnLaunch").slideDown();
		

	}
});




$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



	$("#btn_enroll_save").click(function(){
        var x = $("#enroll-form").serialize();

		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			
			//document.getElementById("enroll_return_msg").innerHTML = xhttp.responseText.split("|");
			document.getElementById("enroll_return_msg").innerHTML = "Save"; //xhttp.responseText;
			$("#enroll_return_msg").fadeOut( "slow" );
			//$("#enroll_refid").val($.trim(xhttp.responseText));
			
			}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/enroll_patient?action=save&"   + x , true);
		xhttp.send();
		  
		  
    });
	
		/*$("#update_patient_info_btn").click(function(){
        var x = $("#Edit-Patient-form").serialize();

		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			
			//document.getElementById("enroll_return_msg").innerHTML = xhttp.responseText.split("|");
			document.getElementById("enroll_return_msg").innerHTML = "Save"; //xhttp.responseText;
			$("#enroll_return_msg").fadeOut( "slow" );
			//$("#enroll_refid").val($.trim(xhttp.responseText));
			
			}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/enroll_patient?action=edit&"   + x , true);
		xhttp.send();
		  
		  
    });*/
	
	
	
		

	$(function()
    {
      $('.non-med-freebies').change(function()
      {
		
			
        if ($(this).is(':checked')) {
		 var checkboxvar = $(this);
			// Do something....
		 var x = $("#dispatchmst_id").val();			
        var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {	

			$( "#sku-table tbody" ).append( "<tr id='sku"+ checkboxvar.val() +"'><td>" + checkboxvar.data("skuname") + "</td><td>" + 1 +"</td><td><a class='btn-info btn-xs' onclick='removeappendsku("+ checkboxvar.val() +")'>x</a></td></tr>" );
		
		}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + "&sku_id=" + checkboxvar.val() + "&qtynonmed=1" + "&patient_id=" + $('#patient_id').val(), true);
		xhttp.send();	

          
        }
		else
		{
		$('#sku' + $(this).val()).remove();
		}

      });
    });
			 
			 


$(".datatable").DataTable({
        "order": []
    });

});

$(document).ready(function() {



$('#verify').DataTable({
  "ordering": false,
});

});
</script>
@endsection

