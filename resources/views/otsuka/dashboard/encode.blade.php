<?php clearstatcache(); ?>
<?php
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
?>

@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
  <?php $baseurl = URL::asset('/'); ?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.bottomright {position:absolute; bottom:0;  margin-bottom:7px; margin:7px; right: 0;}

/**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
margin-left: 3vw;     
margin-right:3vw;  
}    

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
margin-top: 50%;
-ms-transform: scale(3);    
-moz-transform: scale(3);  
-webkit-transform: scale(3);  
-o-transform: scale(3);  
transform: scale(3);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
ul.gallery {      
margin-left: 15vw;       
margin-right: 15vw;
}

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}

th {
    text-align: center !important;
}
td {
  text-align: center !important;
}

.form-group input[type="checkbox"] {
    display: none;
}

.form-group input[type="checkbox"] + .btn-group > label span {
    width: 20px;
}

.form-group input[type="checkbox"] + .btn-group > label span:first-child {
    display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span:last-child {
    display: inline-block;   
}

.form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
    display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
    display: none;   
}
</style>

  <div class="panel panel-default">
    <div class="panel-heading">Encode   @if($item[0]->type == 'Reciept') <span class="btn">GENERATE RECIEPT ID #  {{$item[0]->encoded_purchases_id}}</span> @endif</div>
    <div class="panel-body">
      <div class="row">
        @if($item[0]->type == 'Receipt') 
      <div class="col-md-6">
        <div class="panel panel-default">
      <form method="post" id="encode_form_2" action="{{route('otsuka.unclaim')}}" enctype="multipart/form-data">
      {{ csrf_field() }} 
          <div class="panel-heading">
            <button value="{{!empty($i->id)?$i->id: ''}}" name="unclaim" class="btn btn-primary">Un-claim</button>&nbsp;Patient's Info</div>
          </form>

      <div class="panel-body">
      <form method="post" id="encode_form" action="{{route('otsuka.receipt.create')}}" enctype="multipart/form-data">
      {{ csrf_field() }} 
            <input type="hidden" class="form-control input-sm" name="id" id="id" value="{{$item[0]->id}}">
			 <input type="hidden" class="form-control input-sm" name="encoded_purchases_id" id="encoded_purchases_id" value="{{$item[0]->encoded_purchases_id}}">
            Uploaded by: <p style="color:green;"><b>{{$i->user->name}} ({{$i->user->username}})</b></p>
            Product: <p style="color:green;"><b>{{$item[0]->medicine}}</b></p><br/>
            Patient Name: <input type="text" class="form-control input-sm" id="patient_name" name="patient_name" value="{{$item[0]->patient_name}}" disabled="" required="">
            <input type="hidden" class="form-control input-sm" id="patient_code" name="patient_code_encoded" value="{{$item[0]->patient_code}}">
			<input type="hidden" class="form-control input-sm" id="cmid" name="cmid" value="<?php echo $encoded_purchases->or_number; ?>">
            Mobile No.:<input type="text" class="form-control input-sm" id="mobile_no" name="mobile_no" value="{{$item[0]->mobile_number}}" readonly="">
            Contact No.: <input type="tel" class="form-control  input-sm" id="contact_no_	2" name="contact_no" value="{{$item[0]->phone_no}}" disabled="" required="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
            OR #: <input type="text" class="form-control input-sm" name="or_no" id="or_no" value="<?php echo $encoded_purchases->or_number; ?>" >
            Transaction Date: <input type="date" class="form-control input-sm" name="trans_date" id="or_date" value="<?php echo $encoded_purchases->or_date; ?>" required="">
			UPLOAD REMARKS:<textarea class="form-control input-sm" name="upload_remarks" id="upload_remarks" ><?php echo $item[0]->upload_remarks; ?></textarea>
          

			
			
			<div class="col-md-12 " style="padding:20px;border: 1px solid #000000; margin-top: 20px;">
				
			<select class="form-control input-sm" name="sku" id="sku" required="">
              <option>* Please Select SKU</option>
              @if($item[0]->medicine == 'Abilify')
              @foreach($Mgabilify as $mab)
              <option value="{{$mab->id}}">{{$mab->skuname}}</option>
              @endforeach
              @elseif($item[0]->medicine == 'Aminoleban')
              @foreach($Mgaminoleban as $mam)
              <option value="{{$mam->id}}">{{$mam->skuname}}</option>
              @endforeach
              @elseif($item[0]->medicine == 'Pletaal')
              @foreach($Mgpletal as $mp)
              <option value="{{$mp->id}}">{{$mp->skuname}}</option>
              @endforeach
              @endif
            </select>
			<div style="float:right;margin-top:10px;margin-bottom:10px;">x <input id="sku_qty" type="number"/></div>
				<br/><br/>
				<a class="form-control btn-info" style="text-align:center"  onclick="appendSKU($('#sku').val(),$('#sku option:selected').text(),$('#sku_qty').val());">Add SKU</a>
				
			</div>
          </div>
        </div>
      </div>
      @endif
      @if($item[0]->type == 'Enroll')

      @if(!empty(Session::get('validate_kit_upload')) or !empty(Session::get('validate_kit_validated')) or !empty(Session::get('validate_number')))
      @if ( count(Session::get('validate_kit_upload')) > 0 )
      <div class="modal fade" id="error" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content panel-danger">
            <div class="modal-header panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Duplicate Submission</h4>
            </div>
            <div class="modal-body">
              The Patient Kit Number <b>{{ !empty(old('patient_kit_number'))?old('patient_kit_number'): '' }}</b> is already on queue. <br/>
              <p style="color: red">Note: This is alert is on beta stage. Kindly use the filter below and double check. Alert us if there's a mistake. Thank you.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      @elseif ( count(Session::get('validate_kit_validated')) > 0 )
      <div class="modal fade" id="error" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content panel-danger">
            <div class="modal-header panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Px Already Enrolled</h4>
            </div>
            <div class="modal-body">
              The Patient Kit Number <b>{{ !empty(old('patient_kit_number'))?old('patient_kit_number'): '' }}</b> is already used. This patient kit number is allocated to patient 
              @foreach(Session::get('validate_kit_validated') as $validated)
              <b>{{$validated->patient_fullname}}</b>
              @endforeach.<br/>
              <p style="color: red">Note: This is alert is on beta stage. Kindly use the filter below and double check. Alert us if there's a mistake. Thank you.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      @elseif ( count(Session::get('validate_number')) > 0 )
      <div class="modal fade" id="error" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content panel-warning">
            <div class="modal-header panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Proceed with this Enrollment?</h4>
            </div>
            <div class="modal-body">
              <p class="text-warning">The number you encoded is already on the database. You can check below if its the same patient(s).</p>
              <hr/>
              <div class="col-md-12">
              <div class="col-md-6">
              <b>Encoded Patient Details</b>
              <br/>
              <p class="text-info">
              <b>Patient Name:</b> {{ !empty(old('patient_fullname'))?old('patient_fullname'): '' }}<br/>
              <b>Mobile Number:</b> {{ !empty(old('mobile_number'))?old('mobile_number'): '' }}<br/>
              <b>Mobile Number 2:</b> {{ !empty(old('mobile_number_2'))?old('mobile_number_2'): '' }}<br/>
              <b>Landline:</b> {{ !empty(old('phone_number'))?old('phone_number'): '' }}<br/>
              </p>
              </div>
              <div class="col-md-6">
                <div class="panel panel-info">
                  <div class="panel-heading panel-info">Images</div>
                  <div class="panel-body">  
                    @foreach($item as $i)
                    <div class="col-md-6">
                      <div class="zoom thumbnail">
                        <img class="img-responsive" src="{{$pic.'/'.$i->filename}}">
                        <input type="hidden" class="form-control" name="connector_id" value = "{{$i->connector_id}}"/>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="thumbnail">
                        <input type="hidden" class="form-control" value = "{{'/'.$i->filename}}"/>
                         <a href="#" onclick="popupwindow('{{$pic.'/'.$i->filename}}');"><img class="img-responsive" src="{{$pic.'/'.$i->filename}}"></a>
                       </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
              </div>
              <hr/>
              <div class="col-md-12">
                <table id="example3" class="display table-responsive table-hover table-condensed" width="100%">
                  <thead>
                    <tr>
                      <th>Kit No.</th>
                      <th>Fullname</th>
                      <th>NickName</th>
                      <th>Contact #</th>
                      <th>Birth Date</th>
                      <th>Gender</th>
                      <th>Address</th>                 
                    </tr>
                  </thead>
                   <tbody>
                      @foreach(Session::get('validate_number') as $validate_number)
                      <tr>
                        <td>{{$validate_number->patient_kit_number}}</td>
                        <td>{{$validate_number->patient_fullname}}</td>
                        <td>{{$validate_number->nickname}}</td>
                        <td>{{$validate_number->mobile_number}} @if(!empty($validate_number->mobile_number_2))/@endif {{$validate_number->mobile_number_2}} @if(!empty($validate_number->phone_number))/@endif {{$validate_number->phone_number}}</td>
                        <td>{{$validate_number->birth_date}}</td>
                        <td>{{$validate_number->gender}}</td>
                        <td>{{$validate_number->address}}</td>                        
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              <hr/>   
              <p class="text-danger">Note: This is alert is on beta stage. Kindly use the filter below and double check. Alert us if there's a mistake. Thank you.</p>
            </div>
            <div class="modal-footer">
          <form method="post" action="{{route('otsuka.enroll-proceed')}}" enctype="multipart/form-data">
          {{ csrf_field() }} 
            <input type="hidden" name="id" value="{{ !empty(old('id'))?old('id'): '' }}">
            <input type="hidden" name="patient_kit_number" value="{{ !empty(old('patient_kit_number'))?old('patient_kit_number'): '' }}">
            <input type="hidden" name="patient_fullname" value="{{ !empty(old('patient_fullname'))?old('patient_fullname'): '' }}">
            <input type="hidden" name="mobile_number" value="{{ !empty(old('mobile_number'))?old('mobile_number'): '' }}">
            <input type="hidden" name="mobile_number_2" value="{{ !empty(old('mobile_number_2'))?old('mobile_number_2'): '' }}">
            <input type="hidden" name="phone_number" value="{{ !empty(old('phone_number'))?old('phone_number'): '' }}">
            <input type="hidden" name="time_to_call" value="{{ !empty(old('time_to_call'))?old('time_to_call'): '' }}">
            <input type="hidden" name="patient_consent" value="{{ !empty(old('patient_consent'))?old('patient_consent'): '' }}">
            <input type="hidden" name="enrollment_date" value="{{ !empty(old('enrollment_date'))?old('enrollment_date'): '' }}">
            <input type="hidden" name="doctor_name" value="{{ !empty(old('doctor_name'))?old('doctor_name'): '' }}">
            <input type="hidden" name="doctor_id" value="{{ !empty(old('doctor_id'))?old('doctor_id'): '' }}">
            <input type="hidden" name="doctor_consent" value="{{ !empty(old('doctor_consent'))?old('doctor_consent'): '' }}">
            <input type="hidden" name="hospital_name" value="{{ !empty(old('hospital_name'))?old('hospital_name'): '' }}">
              <button type="submit" name="proceed" value="true" class="btn btn-primary">Yes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
          </form>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      @elseif ( count(Session::get('validate_kit_upload')) > 0 and count(Session::get('validate_kit_validated')) > 0 )
      <div class="modal fade" id="error" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content panel-danger">
            <div class="modal-header panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Duplicate Submission w/ Px Already Enrolled</h4>
            </div>
            <div class="modal-body">
              The Patient Kit Number <b>{{ !empty(old('patient_kit_number'))?old('patient_kit_number'): '' }}</b> is already used and also a duplicate submission. This patient kit number is allocated to patient 
              @foreach(Session::get('validate_kit_validated') as $validated)
              <b>{{$validated->patient_fullname}} </b><br/>
              @endforeach
              and Duplicate submission uploaded at 
              @foreach(Session::get('validate_kit_upload') as $upload)
              <b>{{$upload->created_at}} </b><br/>
              @endforeach<br/>
              <small style="color: red">Note: This is alert is on beta stage. Kindly use the filter below and double check. Alert us if there's a mistake. Thank you.</small>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      @endif
      @endif

      <div class="col-md-6">
        <div class="panel panel-default">
      <form method="post" id="encode_form" action="{{route('otsuka.unclaim')}}" enctype="multipart/form-data">
      {{ csrf_field() }} 
          <div class="panel-heading"><button value="{{!empty($i->id)?$i->id: ''}}" name="unclaim" class="btn btn-primary">Un-claim</button>&nbsp;Patient's Info {{$i->id}}</div>
        </form>
          <form method="post" id="encode_form_2" action="{{route('otsuka.enroll')}}" enctype="multipart/form-data">
          {{ csrf_field() }} 
          <div class="panel-body">
<!--             <div class="col-md-4">
            Patient Code: <p style="color:green;">{{$px_code}}</p>
            </div> -->
            <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 well">
            <div class="col-md-6">


            Uploaded by: <p style="color:green;"><b>{{$i->user->name}}<br/>{{$i->user->username}}</b></p><input type="hidden" name="user_level" value="{{$i->user->user_type}}"><input type="hidden" name="id" value="{{$i->id}}">
            </div>
            <div class="col-md-6">
              <div class="pull-right">
            Patient Kit #:<br/>
            <input type="text" class="autosearch_patient patient_kit_number form-control input-sm" name="patient_kit_number" id="patient_kit_number" value="{{ !empty(old('patient_kit_number'))?old('patient_kit_number'): '' }}" pattern="[0-9]{10}" required="" placeholder="2018000000"></p>
            <small style="color:red;">(10-digit only, type 10 zeroes <b>IF ONLY CAN'T READ THE PATIENT KIT NUMBER)</b></small>
              </div>
            </div>
            </div>
            </div>
            </div>

            <input type="hidden" class="form-control input-sm" name="patient_code" id="patient_code" value="{{$px_code}}">
            Product: <p style="color:green;"><b>{{$i->medicine}}</b></p><input type="hidden" name="product" id="product" value="{{$i->medicine}}">
			      Patient Name: <span style="font-size:10px;color:#b10a0a">(Required)</span> <input type="text" class="form-control input-sm autosearch_patient" name="patient_fullname" id="patient_fullname" required="" value="{{ !empty(old('patient_fullname'))?old('patient_fullname'): '' }}">
            Mobile Number: <span style="font-size:10px;color:#b10a0a">(Required)</span>  <input type="tel" class="form-control input-sm autosearch_patient" name="mobile_number" id="mobile_number" required="" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="{{ !empty(old('mobile_number'))?old('mobile_number'): '' }}">
            Mobile Number 2: <input type="tel" class="form-control input-sm autosearch_patient" name="mobile_number_2"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="mobile_number_2" value="{{ !empty(old('mobile_number_2'))?old('mobile_number_2'): '' }}">
            Landline: <input type="tel" class="form-control input-sm" name="phone_number"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="phone_number" value="{{ !empty(old('phone_number'))?old('phone_number'): '' }}">
            When is Your preferred time to receive the call verification?
      			<textarea name="time_to_call" class="form-control" id="time_to_call">{{ !empty(old('time_to_call'))?old('time_to_call'): '' }}</textarea>
      			<div class="checkbox">
                    <label style="display:none"><input type="hidden" name="patient_consent" value="true" id="patient_consent" @if(!empty(old('patient_consent'))) checked @endif>With Patient consent</label>
                  </div>
      			Enrollment Date: 
              <input onkeydown="return false" type="text" class="form-control input-sm" name="enrollment_date" id="enrollment_date" value="{{ !empty(old('enrollment_date'))?old('enrollment_date'): '' }}" pattern="\d{1,2}/\d{1,2}/\d{4}">
      			Doctors Complete Name: <input type="text" class="form-control input-sm" name="doctor_name" id="doctor_name" value="{{ !empty(old('doctor_name'))?old('doctor_name'): '' }}">
            <input type="hidden"" name="doctor_id" id="doctor_id" value="{{ !empty(old('doctor_id'))?old('doctor_id'): '' }}">
      			<div class="checkbox">
              <label><input type="checkbox" name="doctor_consent" value="true" id="doctor_consent" @if(!empty(old('doctor_consent'))) checked @endif>Doctor's Consent</label>
            </div>
            Hospital/Clinic's Name: <input type="text" class="form-control input-sm" name="hospital_name" id="hospital_name" value="{{ !empty(old('hospital_name'))?old('hospital_name'): '' }}">
			UPLOAD REMARKS:<textarea class="form-control input-sm" name="upload_remarks" id="upload_remarks" ><?php echo $item[0]->upload_remarks; ?></textarea>
          </div>
        </div>
      </div>
      @endif
      <div class="col-md-6">
        <div class="panel panel-info">
          <div class="panel-heading panel-info">Images</div>
          <div class="panel-body">  
            @foreach($item as $i)
            <div class="col-md-6">
              <div class="zoom thumbnail">
                <img class="img-responsive" src="{{$pic.'/'.$i->filename}}">
        			  <input type="hidden" class="form-control" name="connector_id" value = "{{$i->connector_id}}"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="thumbnail">
        			  <input type="hidden" class="form-control" value = "{{'/'.$i->filename}}"/>
        			   <a href="#" onclick="popupwindow('{{$pic.'/'.$i->filename}}');"><img class="img-responsive" src="{{$pic.'/'.$i->filename}}"></a>
               </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
	  
	     @if($item[0]->type == 'Receipt')
				   <div class="col-md-6">
					<div class="panel panel-info">
					  <div class="panel-heading panel-info">Receipt Details</div>
					  <div class="panel-body" style="max-height:250px;overflow:auto" >  
						<table class="table table-bordered table-responsive table-hover" id="sku-table">
							<thead>
							<tr>
							<th>Sys #</th>
							<th>SKU</th>
							<th>QTY</th>
							<th>#</th>
							<thead>
							</tr>
							<tbody>
							
							@if( count($sku_item_dtl) > 0)
							
								@foreach($sku_item_dtl as $key => $val)
						
								<tr id='sku{{$val->id}}'>
									<td>{{$val->sku}}</td>
									<td>@if(!empty($val->sku_details->skuname)) {{$val->sku_details->skuname}} @endif </td>
									<?php /* <td>{{$val->sku_details->skuname}}</td> */ ?>
									<td>{{$val->qty}}</td>
									<td> <a class='btn-info btn-xs' onclick='removeappendsku("{{$val->id}}")'>x</a></td>
								</tr>
								@endforeach
							 @endif
							
							</tbody>
							
						</table>
					  </div>
					</div>
				  </div>
				  
	   @endif
	  
      </div>
      <div class="row">
        <button type="submit" value="{{$i->id}}" style="margin-right: 15px;" name="done" class="btn btn-primary pull-right">Save</button>

        @if($item[0]->type == 'Enroll')
    <div class="pull-right" id="invalid_select_enroll"  style="display:none; margin-right: 10px;">  
    
    <select class="form-control" name="remarks">
      <option disabled="" selected="">* Please Select Reason</option>
      <option value="Px Already Enrolled">Px Already Enrolled</option>
      <option value="Duplicate Submission">Duplicate Submission</option>
      <option value="Wrong Upload">Wrong Upload</option>
      <option value="Blurred Photo">Blurred Photo</option>
      <option value="No Px Consent">No Px Consent</option>
    </select>
    </div>
    <div class="pull-right" style="margin-right: 10px;">  
        <div class="[ form-group ]">
            <input type="checkbox" name="invalid" id="fancy-checkbox-danger" class="invalid_enroll" value="true" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-danger" class="[ btn btn-default active ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="fancy-checkbox-danger" class="[ btn btn-danger ]">
                    INVALID
                </label>
            </div>
        </div>
    <!-- <input type="checkbox" id="invalid_enroll" name="invalid" value="true">INVALID -->
    </div>
        @endif
      
        @if($item[0]->type == 'Receipt')
		
<!--         <button type="button" class="btn btn-success pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#myModal">Enroll</button>     
 -->
		<div class="pull-right" id="invalid_select"  style="display:none; margin-right: 10px;">	
		
		<select class="form-control" name="remarks">
			<option disabled="" selected="">* Please Select Reason</option>
			<option value="Duplicated Receipts (Within The Day)">Duplicated Receipts (Within The Day)</option>
			<option value="Duplicated Receipts">Duplicated Receipts</option>
      <option value="Wrong Upload">Wrong Upload</option>
			<option value="Blurred Photo/ Unreadable">Blurred Photo/ Unreadable</option>
			<option value="No Transaction">No Transaction</option>
			<option value="No OR Number ">No OR Number </option>
		</select>
		</div>
		<div class="pull-right" style="margin-right: 10px;">	
        <div class="[ form-group ]">
            <input type="checkbox" name="invalid" id="fancy-checkbox-danger" class="invalid" value="true" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-danger" class="[ btn btn-default active ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="fancy-checkbox-danger" class="[ btn btn-danger ]">
                    INVALID
                </label>
            </div>
        </div>
		<!-- <input type="checkbox" id="invalid" name="invalid" value="true">INVALID -->
		</div>
        @endif
      </div>

  </form>
      <br/>
	 <div class="row">
		<div class="col-lg-12">
			
				<div class="panel panel-info">
					<div class="panel-heading panel-info">
						<label for="searchbox" >Filter :	</label>
						<input type="text" id="searchbox" class="form-control" style="display:inline;width:20%"/>
						<select class="form-control" style="display:inline;width:20%" id="person"><option value="upload">Upload</option><option value="patient">Patient</option><option value="doctors">Doctors</option><option value="official_reciept">OR</option></select>
						<button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc($('#searchbox').val(),$('#person').val())"></button>
					</div>
					
					<div class="panel-body" id="filterdisplay" style="overflow: scroll;text-align:center">
						
					</div>
				</div>	
			
			
			
		</div>
	 </div>

  </div>

@endsection 

@section('footer-scripts')
<script>
$(document).ready(function (){

      $(window).on('load',function(){
        $('#error').modal('show');
    });

  $('#enrollment_date').datepicker();
})

@if($item[0]->type == 'Receipt')
$(document).ready(function(){
	$(".invalid").change(function(){
		if ($(this).is(':checked')) {
		$("#invalid_select").fadeToggle();
		$('#doctor_name').removeAttr('required');
		$('#or_date').removeAttr('required');
		$('#no_of_tabs').removeAttr('required');
		
		}
		else
		{
			
		$("#invalid_select").fadeToggle();
		

		
		$("#doctor_name").attr("required", "true");
		$("#no_of_tabs").attr("required", "true");
		$("#or_date").attr("required", "true");
		$("#contact_no_2").attr("required", "true");
		$("#patient_name").attr("required", "true");
		
		}
      
    });
@endif

@if($item[0]->type == 'Enroll')
    $(document).ready(function(){
    $(".invalid_enroll").change(function(){
    if ($(this).is(':checked')) {
    $("#invalid_select_enroll").fadeToggle();
    }
    else
    {   
    $("#invalid_select_enroll").fadeToggle();    
    }
      
    });
@endif
	//$('.patient_kit_number').mask('0000-0000000');
   /*  $("#patient_kit_number").change(function(){
       loadDoc($(this).val(),"patient");
    }); */
	$(".autosearch_patient").change(function(){
		
       loadDoc($(this).val(),"patient");
    });
	$("#birth_date").blur(function(){
		
		var date = $('#birth_date').val().split('-');
		var year = date[0];
		var month = date[1];
		var day = date[2];
		var d = new Date();
		var n = d.getFullYear();
		var computed_age = (n - year) ;
		
       $("#age").val(computed_age);
    });
	
	
});

function appendSKU(sku, skudesc , qty) {
	
	var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
        //document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		$( "#sku-table tbody" ).append( "<tr id='sku"+ xhttp.responseText.trim() +"'><td>" + sku + "</td><td>" + skudesc + "</td><td>" + qty +"</td><td><a class='btn-info btn-xs' onclick='removeappendsku("+xhttp.responseText.trim() +")'>x</a></td></tr>" );
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/receipt/encode-item-dtl?encoded_purchases_id=" + $('#encoded_purchases_id').val() + "&sku=" + sku + "&qty=" + qty, true);
  xhttp.send();
	
	
    
}





function removeappendsku(id) {
	
	var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		$('#sku' + id).remove();
		
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/receipt/encode-item-dtl-remove?id=" + id , true);

  xhttp.send();	
    
}





function OpenNewWindow(url) {
    window.open(url);
}


function popupwindow(url) {
	
	title = "Images";
	w = 500;
	h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 


function loadDoc(filter,person) {
    var product = $('#product').val();
	$('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
        document.getElementById("filterdisplay").innerHTML = xhttp.responseText;

        $('#example2').DataTable();
		
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchperson?person_filter=" + filter + "&person=" + person + "&product=" + product, true);
  xhttp.send();
}


function useMD(id) {
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    //document.getElementById(xhttp.responseText['id']);
    
        //alert(xhttp.responseText['id']);
    var obj = JSON.parse(xhttp.responseText);
    //document.getElementById("md_name").value()
    $("#doctor_name").val(obj.mdname);
    $("#hospital_name").val(obj.hospital);
    $("#doctor_id").val(obj.doctorid);
    
    
    
    
    
        //alert(obj.mdname);
    
    }
  };


  //xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + ""+ "&sku_id=" + skuid +  "&qtytabs=" + $("#redeemable_tabs").val() + "&patient_id=" + $('#patient_id').val(), true);
  xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/get_national_md_record?national_md_id=" + id , true);
  xhttp.send(); 
}

</script>
@endsection

