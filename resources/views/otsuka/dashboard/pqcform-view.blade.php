@extends('otsuka.blank')

@section('Patient Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
    <style>
           
      input {
        border: 0;
        outline: 0;
        background: transparent;
        border-bottom: 1px solid black;
      }

     input.noborder {
        border: 0;
        outline: 0;
        background: transparent;
        border-bottom: 0;
      }

      div.border {
          border: 1px solid black;
          padding-top: 10px;
          padding-right: 10px;
          padding-bottom: 10px;
          padding-left: 10px;
      }

      div.padding {
          padding-top: 50px;
          padding-right: 150px;
          padding-bottom: 75px;
          padding-left: 150px;
      }

      h2.space {
        line-height: .25;
      }

      p.ind {
        text-indent: 50px;
        line-height: .1;
      }

      textarea {
        resize: none;
        border-style: none; 
        border-color: Transparent; 
        overflow: hidden;
      }

      .twen {
        width: 25%;
      }

      .thir {
        width: 30%;
      }

      .fort {
        width: 45%;
      }

      input[type=number]::-webkit-inner-spin-button, 
      input[type=number]::-webkit-outer-spin-button { 
      -webkit-appearance: none; 
      margin: 0; 
      }
    </style>
    <div class="padding">
      <div>
        <h3>Otsuka Pharmaceutical (Philippines), Inc. </h3>
      </div>

      <div align="center">
        <h2>Product Quality Complaint (PQC) Data Collection Form  </h2>
      </div>

      <table style="width: 100%">
        <tr align="center">
          <td style="text-align:left">
            <form action="{{route('otsuka.pqc.update')}}" method="POST" >
			
			  {{ csrf_field() }}
			
              <br><b>Reference #: </b> 
				<input type="text" name="refid" value="{{$pqclist->id}}"/>
              <br><b>Date Complaint Received: </b> <input type="text" class="datepicker"  name="pqc_date" value="{{$pqclist->pqc_date}}"/><br><br>
             
          </td>

          <td>
            (Please collect and include all information provided by the reporter)
          </td>
        </tr>
      </table>

      <table style="width: 100%" border="1">
        <tr>
          <td rowspan="2" class="twen">
            <b>OPPI Employee or <br>One Quest <br>Representative <br>Reporting the Complaint</b>
          </td>

          <td class="thir">
            Name:
          </td>

          <td class="fort">
            <input class="noborder" type="text" name="oppi_name" value="{{$pqclist->oppi_name}}" size="50">
          </td>
        </tr>

        <tr>
          <td class="thir">
            Phone No.:
          </td>

          <td class="fort">
            <input class="noborder" type="number" name="phone_no" value="{{$pqclist->phone_no}}" maxlength="11">           
          </td>
        </tr>

        <tr>
          <td rowspan="3" class="twen">
           <b>Reporter’s <br>Information;</b>
           <br><span style="font-weight: normal;">(Person who reported <br>the complaint to the<br>OPPI Employee or One<br> Quest)</span>
          </td>

          <td class="thir">
            Name:
          </td>

          <td class="fort">
            <input class="noborder" type="text" name="reporter_name" value="{{$pqclist->reporter_name}}" size="50">
          </td>
        </tr>

        <tr>
          <td class="thir">
            Contact Information:<br>
             Phone No. or E-mail
          </td>

          <td class="fort">
            <input class="noborder" type="text" name="reporter_contact_details" value="{{$pqclist->reporter_contact_details}}"size="50">
          </td>
        </tr>

       <tr>
          <td  class="thir">
            Is the reporter<br>an HCP?
          </td>

          <td  class="fort">
            <input type="radio" name="is_hcp" id="yes01" value="yes" 
			<?php if($pqclist->is_hcp == 'yes') echo " checked='checked'";?>/>Yes 
            <input type="radio" name="hcp_detail" class="btn sel01" id="sel01" value="doctor" >Doctor
            <input type="radio" name="hcp_detail" class="btn sel01" id="sel01" value="nurse" >Nurse
            <input type="radio" name="hcp_detail" class="btn sel01" id="sel01" value="pharmacist" >Pharmacist
            <br><input <?php if($pqclist->is_hcp == 'no') echo " checked='checked'";?> type="radio" name="is_hcp" id="no01" value="no">No
          </td>
        </tr>

        <tr>
          <td rowspan="4" class="twen">
           <b>Product Description</b>
          </td>

          <td class="thir">
            Product Name and <br>Strength: <i>: (e.g. Meptin<br>5mcg/mL, , Abilify 10 mg, etc)</i>
          </td>

          <td class="fort">
		  
		  <input type="text" name="sku" list="skuname" value="{{$pqclist->sku}}">
			<datalist id="skuname">
			
				@foreach($sku_list as $val)
				<option value="{{$val->skuname}}">
				@endforeach

			</datalist>
				
            <textarea rows="3" cols="55">
            </textarea>
          </td>
        </tr>

         <tr>
          <td>
            Dosage Form
          </td>

          <td>
          
          
              <input class="radioBtn" type="radio" name="dosage_form" <?php if($pqclist->dosage_form == 'tablet') echo " checked='checked'";?> id="radio_false" value="tablet">Tablet
              <input class="radioBtn" type="radio" name="dosage_form" <?php if($pqclist->dosage_form == 'inject') echo " checked='checked'";?> id="radio_false" value="inject">Injection/IV
              <input class="radioBtn" type="radio" name="dosage_form" <?php if($pqclist->dosage_form == 'syrup') echo " checked='checked'";?> id="radio_false" value="syrup">Syrup 
              <br>
              <input class="radioBtn" type="radio" name="dosage_form" id="radio_true" <?php if($pqclist->dosage_form == 'other') echo " checked='checked'";?> value="other">Other 
              <input type="text" name="dosage_form_other" class="input_field" id="input_field" value="{{$pqclist->dosage_form_other}}" disabled><br>
           
          </td>
        </tr>
        <tr>
          <td class="thir">
            Quantity to be returned 
          </td>

          <td class="fort">
            <input class="noborder" type="number" name="qty_to_be_returned" value="{{$pqclist->qty_to_be_returned}}"size="50">
          </td>
        </tr>

        <tr>
          <td>
            Batch/Lot No. 
          </td>

          <td>
            <input class="noborder" type="number" name="batch_lot_no" value="{{$pqclist->batch_lot_no}}" size="50">
          </td>
        </tr>        
        
        <tr>
          <td align="left" rowspan="4"  class="twen">
           <b>Patient information</b>
           <br>Provide the information<br>if patient who used the<br>complaint product is<br>provided by the reporter
          </td>

          <td>
            Patients Initial
          </td>

          <td>
            <input class="noborder" type="text" name="patient_initial" value="{{$pqclist->patient_initial}}" size="50">
          </td>
        </tr>
        
        <tr>
          <td>
           Gender:
          </td>

          <td>
            <input type="radio" name="patient_gender" <?php if($pqclist->patient_gender == 'Female') echo " checked='checked'";?> value="Female">Female&ensp;
            <input type="radio" name="patient_gender"<?php if($pqclist->patient_gender == 'Male') echo " checked='checked'";?>  value="Male">Male
          </td>
        </tr>

        <tr>
          <td>
            Age:
          </td>

          <td>
            <input class="noborder" type="number" name="patient_age" value="{{$pqclist->patient_age}}" size="50"  maxlength="3">
          </td>
        </tr>

        <tr>
          <td>
            Contact No.: 
          </td>

          <td>
            <input class="noborder" type="number" name="patient_contact_no" value="{{$pqclist->patient_contact_no}}"size="50">
          </td>
        </tr>

        <tr>
          <th align="left"  class="twen">
          Detailed description of<br>the complaint<br>including any safety<br>information:<br>
          <span style="font-weight: normal;">Use the guide questions below:<br>
            <ul>
              <li> What is the complaint about?</li>
              <li> When did it happen?</li>
              <li> How did it happen?</li>
              <li> Add other important information provided by the complaint reporter</li></span>
            </ul>
          </th>

          <td colspan="2" style="width: 75%"> 
            <textarea rows="15" name="complaint_detailed" cols="90">{{$pqclist->complaint_detailed}}
            </textarea>
          </td>
        </tr>

        <tr>
          <td colspan="2">
          Picture/photo of complaint sample will be provided: 
          </td>
          <td>&ensp; 
          <input type="radio" name="photo_provided" <?php if($pqclist->photo_provided == 'Yes') echo " checked='checked'";?>  value="Yes">Yes&ensp;
          <input type="radio" name="photo_provided" <?php if($pqclist->photo_provided == 'No') echo " checked='checked'";?> value="No">No
          </td>
        </tr>

        <tr>
          <td colspan="2">
            Will the complaint sample be returned ?  
          </td>
          <td>&ensp; 
            <input type="radio" name="return_sample" <?php if($pqclist->return_sample == 'Yes') echo " checked='checked'";?> value="Yes">Yes&ensp;
            <input type="radio" name="return_sample" <?php if($pqclist->return_sample == 'No') echo " checked='checked'";?> value="No">No
            </td>
        </tr>

        <tr>
          <td colspan="2">
            Is this product quality complaint an adverse event also? 
          </td>
          <td>&ensp; 
            <input type="radio" name="is_product_adverse" <?php if($pqclist->photo_provided == 'Yes') echo " checked='checked'";?> value="Yes" >Yes&ensp;
            <input type="radio" name="is_product_adverse" <?php if($pqclist->photo_provided == 'No') echo " checked='checked'";?> value="No">No
          </td>
        </tr>
      </table>
    

      <font color="blue">E-mail the report to: <a href="mailto:oppi-pqc@otsuka.com.ph"><u>oppi-pqc@otsuka.com.ph</u></a> Phone: 888 6774 (connect to QA or Medical)</font>
      <div>
        <h5>Form #02.00 Product Quality Complaint ( PQC) Data Collection Form<br>
        (Effective 15-Jul-2017)</h5>
      </div>
          <input class="btn btn-info" type="submit" value="Submit"/>
		  <!-- <a href="{{route('otsuka.dashboard.queue')}}" class="btn btn-primary pull-right">Submit</a> -->
    </div>
	</form>
@endsection 

@section('footer-scripts')
<script>





$('#radio_true').click(function()
{
  $('.input_field').removeAttr("disabled");
});

$('#radio_false').click(function()
{
  $('.input_field').attr("disabled","disabled");
});

$('#yes01').click(function()
{
  $('.sel01').removeAttr("disabled");
});

$('#no01').click(function()
{
  $('.sel01').attr("disabled","disabled");
});
</script>
@endsection

