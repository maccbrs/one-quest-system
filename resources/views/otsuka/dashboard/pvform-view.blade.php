@extends('otsuka.blank')

@section('Patient Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
th { font-size: 12px; }
td { font-size: 11px; }

.med_logo img {
    width:100%;
    height:10%;
}

[hidden] {
  display: none !important;
}

.bar {
    height: 18px;
    background: green;
}
.modal-center {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

/**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
margin-left: 3vw;     
margin-right:3vw;  
}    

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(.5);    
-moz-transform: scale(.5);  
-webkit-transform: scale(.5);  
-o-transform: scale(.5);  
transform: scale(.5);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
ul.gallery {      
margin-left: 15vw;       
margin-right: 15vw;
}

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}

  input {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
  }

 input.noborder {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 0;
  }

  div.border {
      border: 1px solid black;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
  }

  div.padding {
      padding-top: 40px;
      padding-right: 65px;
      padding-bottom: 40px;
      padding-left: 60px;
  }

  h2.space {
    line-height: .25;
  }

  p.ind {
    text-indent: 50px;
    line-height: .1;
  }

  @media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}

</style>

<div class="padding">

  <div align="center">
    <h2 class="space">Otsuka (Philippines) Pharmaceutical, Inc.</h2>
    <h3>Safety Report Form</h3>
  </div>
    <form method="post" action="{{route('otsuka.ae-form.update')}}" enctype="multipart/form-data">
      {{ csrf_field() }} 
  <div align="right">
		 <br><b>Reference #: </b> 
				<input type="text" name="refid" value="{{$aeform_details->id}}" readonly="" />
      <br><b>Date of First Receipt (DFR)  </b> <input type="text" class="datepicker" name="date_of_first_receipt" value="{{$aeform_details->date_of_first_receipt}}"/><br><br>
      
  </div>

  <div class="border">
    <div>
      <h4>A. PATIENT INFORMATION (PLEASE FILL-UP ALL INFORMATION) </h4>

      <table style="width: 100%">
        <tr align="left">
          <th>1. Patient's Initials</th>
          <th>2. Date of Birth</th> 
          <th>3. Age</th>
          <th>4. Weight (kg)</th>
          <th>5. Height (cm)</th>
          </tr>

          <tr align="left">
            <td>
                <input type="text" name="patient_initial_f" value="{{$aeform_details->patient_initial_f}}" maxlength="3" size="2" step="3" placeholder="First">
                <input type="text" name="patient_initial_m" value="{{$aeform_details->patient_initial_m}}" maxlength="1" size="2" placeholder="Middle">
                <input type="text" name="patient_initial_l" value="{{$aeform_details->patient_initial_l}}" maxlength="1" size="1" placeholder="Last">
            </td>

            <td>
              <input type="text"  class="datepicker" value="{{$aeform_details->patient_data_of_birth}}" name="patient_data_of_birth">
            </td>

            <td>
              <input type="number" value="{{$aeform_details->patient_age}}" name="patient_age" min="0" max="999">
            </td>

            <td>
              <input type="number" value="{{$aeform_details->patient_weight}}" name="patient_weight" min="0" max="999">
            </td>

            <td>
              <input type="number" name="patient_height" value="{{$aeform_details->patient_height}}"  min="0" max="999">
            </td>
        </tr>
      </table>


      <p><b>6. Gender: <label class="radio-inline"><input type="radio" name="patient_gender" <?php if($aeform_details->patient_gender == 'Male') echo " checked='checked'";?>  value="Male">M</label><label class="radio-inline"><input type="radio" name="patient_gender" value="Female" <?php if($aeform_details->patient_gender == 'Female') echo " checked='checked'";?> >F</label>, 
      is the patient pregnant? </b><label class="radio-inline"><input type="radio" <?php if($aeform_details->patient_is_pregnant == 'Yes') echo " checked='checked'";?>  name="patient_is_pregnant" value="Yes">Yes</label><label class="radio-inline"><input type="radio" name="patient_is_pregnant" <?php if($aeform_details->patient_is_pregnant == 'No') echo " checked='checked'";?>  value="No">No</label>
      <hr>
    </div>

    <div>
      <h4>B. SAFETY INFORMATION</h4>

      <table style="width: 100%">
        <tr align="left">
          <th>1. Date of this report 
            <input type="text" class="datepicker" name="date_of_report" value="{{$aeform_details->date_of_report}}">
          </th>

          <th>2. Onset Date 
            <input type="text" class="datepicker" name="onset_date" value="{{$aeform_details->onset_date}}">
          </th> 

          <th>3. Report Type &nbsp;
            <label class="radio-inline"><input type="radio" name="report_type" <?php if($aeform_details->report_type == 'Initial') echo " checked='checked'";?> value="Initial">Initial</label>
            <label class="radio-inline"><input type="radio" name="report_type" <?php if($aeform_details->report_type == 'Follow-up') echo " checked='checked'";?> value="Follow-up">Follow</label>
         </th>
        </tr>
      </table>

      <p><b>4. Source: 
            <label class="radio-inline"><input type="radio" name="source" <?php if($aeform_details->source == 'Solicited') echo " checked='checked'";?> value="Solicited">Solicited<i>(specify Title of Study) </i><input type="text" value="{{$aeform_details->source_details}}" name="source_details" size="35"></label>
            <label class="radio-inline"><input type="radio" name="source" <?php if($aeform_details->source == 'Spontaneous') echo " checked='checked'";?> value="Spontaneous">Spontaneous</label>

      <p><b>5. Seriousness Criteria </b><i>(check all that apply)</i></p>

      <table style="width: 100%">
        <tr align="left">
          <th><input type="checkbox" name="is_death" <?php if($aeform_details->is_death == 'death') echo " checked='checked'";?> value="death"><span style="font-weight: normal;">Death</span></th>
          <th><input type="checkbox" name="is_life_threat" <?php if($aeform_details->is_life_threat == 'lifethreatening') echo " checked='checked'";?> value="lifethreatening"><span style="font-weight: normal;">Life Threatening</span></th>
          <th><input type="checkbox" name="is_hospitalize" <?php if($aeform_details->is_hospitalize == 'hospitalization') echo " checked='checked'";?> value="hospitalization"><span style="font-weight: normal;">Hospitalization</span></th>
          <th><input type="checkbox" name="is_congenital" <?php if($aeform_details->is_congenital == 'congenitalanomaly') echo " checked='checked'";?> value="congenitalanomaly"><span style="font-weight: normal;">Congenital anomaly</span></th>
          <th><input type="checkbox" name="is_disability" <?php if($aeform_details->is_disability == 'disabilityincapacity') echo " checked='checked'";?> value="disabilityincapacity"><span style="font-weight: normal;">Disability/Incapacity</span></th>
        </tr>

        <tr align="left">
          <th><input type="checkbox" name="is_medically_sign"  <?php if($aeform_details->is_medically_sign == 'medicallysignificant') echo " checked='checked'";?> value="medicallysignificant" ><span style="font-weight: normal;">Medically Significant </span></th>
          <th><input type="checkbox" name="is_non_serious"  <?php if($aeform_details->is_non_serious == 'nonserious') echo " checked='checked'";?> value="nonserious" ><span style="font-weight: normal;">Non-Serious </span></th>
          <th><input type="checkbox" name="is_not_reported"  <?php if($aeform_details->is_not_reported == 'notreported') echo " checked='checked'";?> value="notreported" ><span style="font-weight: normal;">Not reported </span></th>
        </tr>
      </table>

        <p><b>6. Describe Safety Information (e.g. Adverse Event, Off-Label Use, Lack of Efficacy, etc.)</b></p>
        <textarea name="describe_safety_info" style="width:100%; height:100px;">{{$aeform_details->describe_safety_info}}</textarea>
        <br>

        <p><b>7. Were any medicines taken or procedures done to treat the event related to Item no. 6? (If available)</b></p>
        <textarea name="medicines_taken" style="width:100%; height:100px;">{{$aeform_details->medicines_taken}}</textarea>
        <br>

      <p>
        <b>8. What is the Outcome?</b>
            <label class="radio-inline"><input type="radio" name="outcome" <?php if($aeform_details->outcome == 'Fatal') echo " checked='checked'";?>  value="Fatal">Fatal</label>
            <label class="radio-inline"><input type="radio" name="outcome" <?php if($aeform_details->outcome == 'Resolved') echo " checked='checked'";?>  value="Resolved">Resolved</label>
            <label class="radio-inline"><input type="radio" name="outcome" <?php if($aeform_details->outcome == 'Resolved with sequelae') echo " checked='checked'";?>  value="Resolved with sequelae">Resolved with sequelae</label>
            <label class="radio-inline"><input type="radio" name="outcome" <?php if($aeform_details->outcome == 'Not Resolved') echo " checked='checked'";?> value="Not Resolved">Not Resolved</label>
            <label class="radio-inline"><input type="radio" name="outcome" <?php if($aeform_details->outcome == 'Unknown') echo " checked='checked'";?> value="Unknown">Unknown</label>
        <br>
        <p style="margin-left: 30px;">8.1. If the event was resolved, kindly provide the date the patient recovered from the event:
        <input type="text" class="datepicker" name="outcome_date" value="{{$aeform_details->outcome_date}}"></p>
      </p>
      
      <p>
        <b>9. Is the adverse event related to the drug?</b> (Reporter’s Causality)&nbsp;
            <label class="radio-inline"><input type="radio" name="related_to_drug"  <?php if($aeform_details->related_to_drug == 'Yes') echo " checked='checked'";?> value="Yes">Yes</label>
            <label class="radio-inline"><input type="radio" name="related_to_drug" <?php if($aeform_details->related_to_drug == 'No') echo " checked='checked'";?> value="No">No</label>
            <label class="radio-inline"><input type="radio" name="related_to_drug" <?php if($aeform_details->related_to_drug == 'Not assessable') echo " checked='checked'";?> value="Not assessable">Not assessable</label>
      </p>

      <p>
        <b>10. Is the adverse event expected by the patient? </b>
            <label class="radio-inline"><input type="radio" name="is_expected" <?php if($aeform_details->is_expected == 'Yes') echo " checked='checked'";?> value="Yes">Yes</label>
            <label class="radio-inline"><input type="radio" name="is_expected" <?php if($aeform_details->is_expected == 'No') echo " checked='checked'";?> value="No">No</label>
            <label class="radio-inline"><input type="radio" name="is_expected" <?php if($aeform_details->is_expected == 'Not assessable') echo " checked='checked'";?> value="Not assessable">Not assessable</label>
      </p>

      <table class="display" style="width: 100%" border=".5">
        <tr>
          <th align="left">11. Suspect Medication <br><span style="font-weight: normal;">(Please indicate strength and<br> manufacturer if known)</span></th>
          <th>Indication for which drug <br>is given</th>
          <th>Dose, Frequency and <br>Route</th>
          <th>Start Date</th>
          <th>Stop Date</th>
        </tr>

        <tr>
          <td>1. <input class="noborder" type="text" name="suspected_1" value="{{$aeform_details->suspected_1}}"></td>
          <td> <textarea name="indication_1" style="border-style: none;">{{$aeform_details->indication_1}}</textarea></td>
          <td> <textarea name="dose_route_1" style="border-style: none;">{{$aeform_details->dose_route_1}}</textarea></td>
          <td> <input type="text" class=" noborder"  placeholder="m/d/yyyy"  name="start_date_1" value="{{$aeform_details->start_date_1}}"></td>
          <td> <input type="text" class=" noborder"  placeholder="m/d/yyyy"  name="stop_date_1" value="{{$aeform_details->stop_date_1}}"></td>
        </tr>       
		<tr>
          <td>2. <input class="noborder" type="text" name="suspected_2" value="{{$aeform_details->suspected_2}}"></td>
          <td> <textarea name="indication_2" style="border-style: none;">{{$aeform_details->indication_2}}</textarea></td>
          <td> <textarea name="dose_route_2" style="border-style: none;">{{$aeform_details->dose_route_2}}</textarea></td>
          <td> <input  type="text" class=" noborder"  placeholder="m/d/yyyy"  name="start_date_2" value="{{$aeform_details->start_date_2}}"></td>
          <td> <input  type="text" class=" noborder"  placeholder="m/d/yyyy"  name="stop_date_2" value="{{$aeform_details->stop_date_2}}"></td>
        </tr>

       
      </table>
    </div>

    <hr>

    <div>
       <h4>C. REPORTER</h4>
       <table>
        <tr>
          <td>1. Name <textarea name="reporter_name" style="border-style: none; width: 500px; height: 15px;">{{$aeform_details->reporter_name}}</textarea></td>
          <td> Address/Contact details: <textarea name="reporter_contact" style="border-style: none; width: 700px; height: 15px;">{{$aeform_details->reporter_contact}}</textarea></td>
        </tr>

        <tr>
          <td>2. Health Profession <input type="text" value="{{$aeform_details->reporter_profession}}" name="reporter_profession" style="width:200px"></td>
        </tr>
       </table>

       <table style="width: 100%">
        <tr>
          <td>3. Reported through </td>
            <td><label class="radio-inline"><input type="radio" name="reported_through" <?php if($aeform_details->reported_through == 'Fax') echo " checked='checked'";?>  value="Fax">Fax</label></td>
            <td><label class="radio-inline"><input type="radio" name="reported_through" <?php if($aeform_details->reported_through == 'Telephone') echo " checked='checked'";?>  value="Telephone">Telephone</label></td>
            <td><label class="radio-inline"><input type="radio" name="reported_through" <?php if($aeform_details->reported_through == 'PSR') echo " checked='checked'";?>  value="PSR">PSR</label></td>
          <td><input type="text" name="psr_input" size="25"></td>
          <td>4. Date Reported <input type="text" class="datepicker" name="date_reported"> </td>
        </tr>

        <tr>
          <td>4. Did the reporter consent to follow up contact?</td>
            <td><label class="radio-inline"><input type="radio" name="did_reporter_consent" <?php if($aeform_details->did_reporter_consent == 'Yes') echo " checked='checked'";?>  value="Yes">Yes</label></td>
            <td><label class="radio-inline"><input type="radio" name="did_reporter_consent" <?php if($aeform_details->did_reporter_consent == 'No') echo " checked='checked'";?>  value="No">No</label></td>
        </tr>
      </table>

      <table>
        <tr>
          <td>5. Did the reporter consent to provide reporting information to Otsuka Contractor?</td>
            <td><label class="radio-inline"><input type="radio" name="did_reporter_provide_info" <?php if($aeform_details->did_reporter_provide_info == 'Yes') echo " checked='checked'";?>  value="Yes">Yes</label></td>
            <td><label class="radio-inline"><input type="radio" name="did_reporter_provide_info" <?php if($aeform_details->did_reporter_provide_info == 'No') echo " checked='checked'";?>  value="No">No</label></td>
        </tr>
      </table>

      <p><i>Note to the reporter: Please be informed that this case might be reported to Global Otsuka and local regulatory authority
      according to Otsuka requirements and local regulation, but rest assured that personal information will be highly protected.</i></p>
    </div>

    <div>
      <h4>D. OTSUKA (PHILIPPINES) PHARMACEUTICAL INC. USE ONLY</h4>
      <p>1. Date Received by LSM
        <input type="text" class="datepicker" value="{{$aeform_details->date_receive_lsm}}" name="date_receive_lsm"></p>
    </div>
  </div>

  <div align="center">
    <p><i><b>(Please fax accomplished report at 811-2279 or send via e-mail at oppi-pv@otsuka.com.ph)</b></i></p>
  </div>

  <div>
    <p align="left">Version 7.0</p>
    <p align="right">December 2017</p>
  </div>

  <div>
	<input type="submit" class="btn btn-primary pull-right no-print" value="submit"/>
    <?php /*<a href="{{route('otsuka.dashboard.queue')}}" class="btn btn-primary pull-right">Submit</a> <? */?>
  </div>
</div>

@endsection 

@section('footer-scripts')
<script>

</script>


@endsection

