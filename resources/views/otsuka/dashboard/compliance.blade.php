@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $old_pic = URL::asset('/uploads/card_uploads');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.bottomright {position:absolute; bottom:0;  margin-bottom:7px; margin:7px; right: 0;}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}

.font-red {
  color: red !important;
  
}


<style type="text/css">
  /**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
margin-left: 3vw;     
margin-right:3vw;  
}    

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(8);    
-moz-transform: scale(8);  
-webkit-transform: scale(8);  
-o-transform: scale(8);  
transform: scale(8);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
ul.gallery {      
margin-left: 15vw;       
margin-right: 15vw;
}

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}
</style>

</style>




<div class="col-md-12">
    <form id="myForm" method="get" action="{{route('otsuka.compliance.save')}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        <input type="hidden" name="v_id" value="{{!empty($pr->fetch_verification_queue_compliance->id)?$pr->fetch_verification_queue_compliance->id: ''}}">
      <div class="panel panel-default">

      <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">Retrieval Kit</div>
              <div class="panel-body">  
                <?php
                $Upload = new App\Http\Models\otsuka\Upload;
                $upload = $Upload->where('patientid',$pr->patientid)->where('trail_status','!=',199)->first();
                ?>
                <?php
                $Old_retrieval_enrollment = new App\Http\Models\otsuka\Old_retrieval_enrollment;
                $old_retrieval_enrollment = $Old_retrieval_enrollment->where('patientid',$pr->patientid)->first();
                ?>
             @if(!empty($upload))   
            <img class="zoom" style="width: 10%; height: 10%;" src="{{$pic.'/'.$upload->filename}}">
             <a href="#" onclick="popupwindow('{{$pic.'/'.$upload->filename}}');"><img class="" style="width: 10%; height: 10%;" src="{{$pic.'/'.$upload->filename}}"></a>
             @elseif(empty($upload) and !empty($old_retrieval_enrollment))
             @if(!empty($old_retrieval_enrollment->imagefilepath))
            <img class="zoom" style="width: 10%; height: 10%;" src="{{$old_pic.'/'.$old_retrieval_enrollment->imagefilepath}}">
             <a href="#" onclick="popupwindow('{{$old_pic.'/'.$old_retrieval_enrollment->imagefilepath}}');"><img class="" style="width: 10%; height: 10%;" src="{{$old_pic.'/'.$old_retrieval_enrollment->imagefilepath}}"></a>
             @else
             NO AVAILABLE DATA
             @endif
             @else
             NO AVAILABLE DATA
             @endif
              </div>
            </div>
          </div>
        </div>

        <div class="panel-heading">Patient Details  
          <label class="switch" style="float:right">UPDATE
          <input type="checkbox" checked><span class="slider round"></span></label>
        </div>
        <div class="panel-body">

          <div class="row">
          <div class="col-md-3">
            <div class="well">
              <label for="Patient Code">Patient Code:</label>
              <input type="text" name="p_id" class="form-control input-sm" style="color: green; font-weight: bold;" value="{{!empty($pr->fetch_patient->patient_code)?$pr->fetch_patient->patient_code: ''}}" readonly>
            </div>
            </div>
          <div class="col-md-3">
            <div class="well">
            <label for="Patient Kit #">Patient Kit #:</label>
            <input type="text" name="patient_kit_number" class="form-control input-sm" style="color: green; font-weight: bold;" value="{{!empty($pr->fetch_patient->patient_kit_number)?$pr->fetch_patient->patient_kit_number: ''}}" readonly="">
            </div>
          </div>

<!--          <div class="col-md-6">
            <div class="well">
            <label for="Patient Kit #">Images:</label>
            <p>

            </p>
            </div>
          </div> -->

          </div>

        <div class="row">

          <div class="col-md-12">
          <div class="form-group">
            <label for="details">Patient Name:</label>
            <input type="text" class="form-control input-sm" placeholder="" name="patient_fullname" id="name" value="{{!empty($pr->fetch_patient->patient_fullname)?$pr->fetch_patient->patient_fullname: ''}}">
          </div>
          </div>

          <div class="col-md-4">
          <div class="form-group">
            <label for="first">Last Name</label>
            <input type="text" class="form-control input-sm inputrecord" name="patient_lastname" value="{{!empty($pr->fetch_patient->patient_lastname)?$pr->fetch_patient->patient_lastname: ''}}" placeholder="Last Name" id="">
          </div>
          </div>
          <!--  col-md-6   -->

          <div class="col-md-4">
          <div class="form-group">
            <label for="last">First Name</label>
            <input type="text" class="form-control input-sm inputrecord" value="{{!empty($pr->fetch_patient->patient_firstname)?$pr->fetch_patient->patient_firstname: ''}}" name="patient_firstname" placeholder="First Name" id="">
          </div>
          </div>

          <div class="col-md-4">
          <div class="form-group">
            <label for="last">Middle Name</label>
            <input type="text" class="form-control input-sm inputrecord" value="{{!empty($pr->fetch_patient->patient_firstname)?$pr->fetch_patient->patient_firstname: ''}}" name="patient_middlename" placeholder="Middle Name" id="last">
          </div>
          </div>
          <!--  col-md-6   -->
        </div>


        <div class="row">
          <div class="col-md-4">
          <div class="form-group">
            <label for="birthdate">Birthdate </label>          
            <input type="date" class="form-control input-sm" value="{{!empty($pr->fetch_patient->birth_date)?date('Y-m-d', strtotime($pr->fetch_patient->birth_date)): ''}}" placeholder="Birthdate" name="birth_date" id="birthdate">
          </div>
          </div>
          <!--  col-md-6   -->

          <div class="col-md-4">
          <div class="form-group">
            <label for="gender">Gender</label>
            <select class="form-control input-sm"  name="gender">
            <option value="{{!empty($pr->fetch_patient->gender)?$pr->fetch_patient->gender: ''}}">{{!empty($pr->fetch_patient->gender)?$pr->fetch_patient->gender: '* Please Select Gender'}}</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            </select>
          </div>
          </div>

          <div class="col-md-4">
          <div class="form-group">
            <label for="gender">Age</label>
            <input type="number" class="form-control input-sm inputrecord" placeholder="Age" name="age" value="{{!empty($pr->fetch_patient->age)?$pr->fetch_patient->age: ''}}">
          </div>
          </div>
          <!--  col-md-6   -->
        <!--  row   -->

          <div class="col-md-12">
          <div class="form-group">
            <label for="address">Address</label>
            <input type="text" class="form-control input-sm inputrecord" id="address" name="address" placeholder="Address" value="{{!empty($pr->fetch_patient->address)?$pr->fetch_patient->address: ''}}">
          </div>
          </div>
        </div>
          <!--  col-md-6   -->
        <hr>
        <div class="row">
          <div class="col-md-4">
          <div class="form-group">
            <label for="mobile1">Mobile No. 1</label>
            <input type="number" class="form-control input-sm inputrecord" name="mobile_number" placeholder="Mobile 1" value="{{!empty($pr->fetch_patient->mobile_number)?$pr->fetch_patient->mobile_number: 'N/A'}}">
          </div>
          </div>

          <div class="col-md-4">
          <div class="form-group">
            <label for="mobile">Mobile No. 2</label>
            <input type="number" class="form-control input-sm inputrecord" name="mobile_number_2" placeholder="Mobile 2" value="{{!empty($pr->fetch_patient->mobile_number_2)?$pr->fetch_patient->mobile_number_2: 'N/A'}}">
          </div>
          </div>

          <div class="col-md-4">
          <div class="form-group">
            <label for="landline">Landline</label>
            <input type="number" class="form-control input-sm inputrecord" placeholder="Landline" name="phone_number" value="{{!empty($pr->fetch_patient->phone_number)?$pr->fetch_patient->phone_number: 'N/A'}}">
          </div>
          </div>
          <!--  col-md-6   -->
        </div>
        <!--  row   -->
<!--        <hr>

        <div class="row">
          <div class="col-md-6">
          <div class="form-group">
            <label for="mobile1">Preferred Time to Call</label>
            <input type="text" class="form-control input-sm" name="time_to_call" placeholder="Preffered Time to Call" name="time_to_call" value="{{!empty($pr->fetch_patient->time_to_call)?$pr->fetch_patient->time_to_call: ''}}" disabled="">

          </div>
          </div>

          <div class="col-md-6">
          <div class="form-group">
            <label for="mobile1">Remarks</label>
            <textarea class="form-control inputrecord" disabled  rows="5" name="remarks" id="remarks" placeholder="Remarks"></textarea>
          </div>
          </div>
        </div> -->

        </div>
      </div>    


<!-- <div class="col-md-12">
<div class="panel panel-default">
  <div class="panel-heading">Patient Medical Info</div>
  <div class="panel-body">

      <div class="row">
          <div class="col-md-6">
            <label for="Medical Representative">Medical Representative </label><input type="text" class="form-control input-sm" name="p_name" value="{{!empty($pr->fetch_patient->fetch_mr->mr_name)?$pr->fetch_patient->fetch_mr->mr_name: 'N/A'}}">
            <input type="hidden" name="medrep_id" value="">
          </div>
          <div class="col-md-6">
            <label for="Doctor">Doctor </label><input type="text" id="md_name" class="form-control input-sm" name="doctor" placeholder="Doctor" value="{{!empty($pr->fetch_patient->doctor_name)?$pr->fetch_patient->doctor_name: 'N/A'}}">
          </div>
      </div>
            
      <div class="row">
        <div class="col-md-12">
          <label for="Hospital">Hospital </label><input type="text" class="form-control input-sm" id="hospital" name="hospital" placeholder="Hospital" value="{{!empty($pr->fetch_patient->hospital)?$pr->fetch_patient->hospital: 'N/A'}}">
        </div>
      </div>

      <div class="row">
          <div class="col-md-6">
            <label for="Product">Product </label>
              <select class="form-control input-sm" id="product">
                <option value="{{!empty($pr->fetch_patient->fetch_patient_upload->medicine)?$pr->fetch_patient->fetch_patient_upload: 'N/A'}}">{{!empty($pr->fetch_patient->fetch_patient_upload->medicine)?$pr->fetch_patient->fetch_patient_upload->medicine: 'N/A'}}</option>
              </select>
          </div>
        <div class="col-md-6">
          <label for="SKU">SKU</label><select class="form-control input-sm" id="product" name="product">
              <option>{{!empty($pr->fetch_patient->fetch_sku->sku)?$pr->fetch_patient->fetch_sku->sku: 'N/A'}}</option>           
            </select>
      </div>
    </div>
<hr> -->
<!--             <div class="row">
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Tabs Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="notabsprescribe" class="form-control input-sm" max="9999" value="{{!empty($pr->fetch_patient->fetch_sku->no_of_tabs_prescribe)?$pr->fetch_patient->fetch_sku->no_of_tabs_prescribe: 'N/A'}}" disabled="">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Days Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="nodaysprescribe" class="form-control input-sm" max="9999" value="{{!empty($pr->fetch_patient->fetch_sku->no_of_days_prescribe)?$pr->fetch_patient->fetch_sku->no_of_days_prescribe: 'N/A'}}" disabled="">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label">Total No. of Tabs</label>
                  <div class="col-md-4">
                    <input type="number" name="totalnotabs" class="form-control input-sm" max="9999" value="{{!empty($pr->fetch_patient->fetch_sku->total_no_of_tabs)?$pr->fetch_patient->fetch_sku->total_no_of_tabs: 'N/A'}}" disabled="">
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="col-md-5"><label for="inputKey" class="col-md-8 control-label right-label">Remarks</label></div>
                <div class="col-md-7">
                  <input type="text" class="form-control input-sm" value="{{!empty($pr->fetch_patient->fetch_sku->tabs_prescribe_remarks)?$pr->fetch_patient->fetch_sku->tabs_prescribe_remarks: 'N/A'}}" disabled="">
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"><label for="inputKey" class="col-md-8 control-label right-label">Remarks</label></div>
                <div class="col-md-7">
                  <input type="text" class="form-control input-sm" value="{{!empty($pr->fetch_patient->fetch_sku->days_prescribe_remarks)?$pr->fetch_patient->fetch_sku->days_prescribe_remarks: 'N/A'}}" disabled="">
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"><label for="inputKey" class="col-md-8 control-label right-label">Remarks</label></div>
                <div class="col-md-7">
                  <input type="text" class="form-control input-sm" value="{{!empty($pr->fetch_patient->fetch_sku->total_tabs_remarks)?$pr->fetch_patient->fetch_sku->total_tabs_remarks: 'N/A'}}" disabled="">
              </div>
            </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                Already Purchased:
                <input type="text" class="form-control input-sm" value="{{!empty($pr->fetch_patient->fetch_sku->purchased)?$pr->fetch_patient->fetch_sku->purchased: 'N/A'}}" disabled="">
              </div>
            </div>

      <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-success pull-right">Save</button>
              </div>
            </div>

        </div>
     </div>
  </div>   -->
  
<!-- <div class="row">

<div class="col-md-12">

  <div class="panel panel-default">
    <div class="panel-heading">Verified Patient Information</div>
    <div class="panel-body">


        <div class="col-md-12">
          <table class="table">
            <thead>
              <tr>
                <th>Call Attempt</th>
                <th>Call Date</th>
                <th>Status</th>
                <th>Remarks</th>
                <th>Call Notes</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>
      @if(!empty($pr->fetch_patient->fetch_patient_upload->fetch_call->fetch_verification))
              @foreach($pr->fetch_patient->fetch_patient_upload->fetch_call->fetch_verification as $v_call)
              @if($v_call->allowcallback == 1 or $v_call->allowcallback == 2)
              <tr class="statuscheck">
                <input type="hidden" name="upload_id" value="{{$pr->id}}">
                <td>#{{!empty($v_call->attempt)?$v_call->attempt: ''}}<input type="hidden" name="v_attempt" value="{{!empty($v_call->attempt)?$v_call->attempt: ''}}"><input type="hidden" name="cmid" value="{{!empty($v_call->cmid)?$v_call->cmid: ''}}"></td>
                <td>{{!empty($v_call->actualdate)?$v_call->actualdate: '(Auto Generated)'}}</td>
                <td><select class="form-control input-sm" name="remarks_call">
                      <option value="{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: ''}}">{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: 'Please Select Status'}}</option>
                    </select>
                </td>
                <td><input type="text" class="form-control input-sm" placeholder="Remarks" name="remarks_others" value="{{!empty($v_call->remarks_others)?$v_call->remarks_others: ''}}"></td>
                <td><textarea class="form-control" rows="5" name="callnotes" id="callnotes" placeholder="Callnotes">{{!empty($v_call->callnotes)?$v_call->callnotes: ''}}</textarea><input type="hidden" name="v_id" value="{{!empty($v_call->id)?$v_call->id: ''}}"></td>
                <td><button type="submit" class="btn btn-success" id="test" value="{{!empty($v_call->id)?$v_call->id: ''}}">Update</button></td>
              </tr>
              @elseif(empty($v_call->allowcallback))
              <tr>
                <td>#{{!empty($v_call->attempt)?$v_call->attempt: ''}}<input type="hidden" name="v_attempt" value="{{!empty($v_call->attempt)?$v_call->attempt: ''}}"><input type="hidden" name="cmid" value="{{!empty($v_call->cmid)?$v_call->cmid: ''}}"></td>
                <td>{{!empty($v_call->actualdate)?$v_call->actualdate: '(Auto Generated)'}}</td>
                <td><select class="form-control input-sm" name="remarks_call" required="">
                      <option value="{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: ''}}">{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: 'N/A'}}</option>
                    </select>
                </td>
                <td><input type="text" class="form-control input-sm" placeholder="Remarks" name="remarks_others" value="{{!empty($v_call->remarks_others)?$v_call->remarks_others: ''}}"></td>
                <td><textarea class="form-control" rows="5" name="callnotes" id="callnotes" placeholder="Callnotes">{{!empty($v_call->callnotes)?$v_call->callnotes: ''}}</textarea><input type="hidden" name="v_id" value="{{!empty($v_call->id)?$v_call->id: ''}}"></td>
                <td><button type="submit" class="btn btn-success" id="test" value="{{!empty($v_call->id)?$v_call->id: ''}}">Update</button></td>
              </tr>
              @endif
              @endforeach
      @endif
            </tbody>
          </table>
    </div>
  </div>
</div>
 -->

  <div class="panel panel-default">
    <div class="panel-heading">Compliance</div>
    <div class="panel-body">
  <div class="col-md-12">
    <input type="hidden" name="cmid" value="{{!empty($pr->id)?$pr->id: ''}}">
    Patient Code: <b>{{!empty($pr->fetch_patient->patient_code)?$pr->fetch_patient->patient_code: ''}}</b><br/>
    Initial Compliance Date: {{!empty($pr->created_at)?$pr->created_at: ''}}<br/>
    Last Compliance Date: {{!empty($pr->updated_at)?$pr->updated_at: ''}}
    <hr/><input type="hidden" class="form-control input-sm" name="patientid" value="{{!empty($pr->fetch_patient->id)?$pr->fetch_patient->id: ''}}">
    
    <div class="col-md-6"> 

    @if(!empty($pr->fetch_patient->fetch_compliance_upload))
    <label>Product Taken: </label><input type="text" class="form-control input-sm" name="product_taken" value="{{!empty($pr->fetch_patient->fetch_compliance_upload->medicine)?$pr->fetch_patient->fetch_compliance_upload->medicine: ''}}" readonly="">
    @else
    <label>Product Taken: </label><input type="text" class="form-control input-sm" name="product_taken" value="{{!empty($od->fetch_product->brandname)?$od->fetch_product->brandname: ''}}" readonly="">
    @endif

    @if(!empty($pr->fetch_patient->fetch_compliance_upload))
    <label>Sku Taken: </label><input type="text" class="form-control input-sm" name="sku" value="{{!empty($pr->fetch_patient->fetch_compliance_upload->medicine)?$pr->fetch_patient->fetch_compliance_upload->medicine: ''}}" readonly="">
    @else
    <label>Sku Taken: </label><input type="text" class="form-control input-sm" name="sku" value="{{!empty($od->fetch_sku->skuname)?$od->fetch_sku->skuname: ''}}" readonly="">
    @endif

    @if(!empty($pr->fetch_patient->fetch_compliance_upload))
    <label>Date of Start Medication: </label><input onkeydown="return false" type="text" class="form-control input-sm" id="date_start_medication" name="date_start_medication" value="{{!empty($pr->fetch_compliance_details->date_start_medication)?date('m-d-Y', strtotime($pr->fetch_compliance_details->date_start_medication)): ''}}">
    @else
    @if(empty($od->startofmed) or $od->startofmed == '12/29/1899' or $od->startofmed == '01-01-1970')
    <label>Date of Start Medication: </label><input type="text" class="form-control input-sm" id="date_start_medication" name="date_start_medication" value="" placeholder="mm-dd-yyyy">
    @else
    <label>Date of Start Medication: </label><input type="text" class="form-control input-sm" id="date_start_medication" name="date_start_medication" value="{{!empty($od->startofmed)?date('m-d-Y', strtotime($od->startofmed)): ''}}" placeholder="mm-dd-yyyy">
    @endif
    @endif

    <label>Still on Medication: </label><select id="still_medication" class="form-control input-sm" name="still_medication">
      <option selected="" disabled="">* Please Select</option>
      <option value="Yes">Yes</option>
      <option value="No">No</option>
    </select>

    <div id="show_no"> 

    <label>Month Stopped </label><small class="font-red"> (*If Not on Medication)</small>: <select class="form-control input-sm stop" name="month_stopped" disabled="">
      <option selected="" disabled="">* Please Select Month</option>
      @foreach($drop_months as $m)
      <option value="{{!empty($m->id)?$m->id: ''}}">{{!empty($m->name)?$m->name: ''}}</option>
            @endforeach        
                    </select>

    <label>Medication Reason: </label><select class="form-control input-sm stop" name="reason_not_med" id="real_reason" required="" disabled="">
      <option selected="" disabled="">* Please Select Reason</option>
      @foreach($drop_real_reasons as $d)
      <option value="{{!empty($d->id)?$d->id: ''}}">{{!empty($d->name)?$d->name: ''}}</option>
            @endforeach        
                    </select>

    <div id="show_others">

      <label>Others: </label><textarea class="form-control other_reason" rows="5" name="others_reason_not_med" placeholder="Others"></textarea>

    </div>

    </div>

    <label>Call Status: </label><select class="form-control input-sm" name="reason" id="reason" required="">
      <option selected="" disabled="">* Please Select Reason</option>
      @foreach($drop_reasons as $d)
      <option value="{{!empty($d->id)?$d->id: ''}}">{{!empty($d->name)?$d->name: ''}}</option>
            @endforeach        
                    </select>

    <div id="show_new">

    <label>New Medication </label><small class="font-red"> (*if switched medication)</small>: <input type="text" class="form-control input-sm switch" name="medication" value="" placeholder="New Medication" disabled="">

    </div>

    <label>Remarks: </label><input type="text" class="form-control input-sm" value="" name="remarks" placeholder="Remarks">

  </div>

  <div class="col-md-6">

    <label>Notes: </label><textarea class="form-control" rows="5" name="callnotes" id="callnotes" placeholder="Notes"></textarea>

  </div>

    <hr/>
    <button type="submit" class="btn btn-success bottomright " id="submit_form" value="test">Submit</button>
  </div>
  </div>
</form>
</div>

</div>



      
      





@endsection 

@section('footer-scripts')
<script>
function popupwindow(url) {
  
  title = "Images";
  w = 500;
  h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
  
$(document).ready(function() {

$('#date_start_medication').datepicker({format: 'yyyy-mm-dd'});

$('#myForm').submit(function(){
  $(this).find(':submit').attr('disabled','disabled');
});


$(".datatable").DataTable({
  "ordering": false,
});

});

$(document).ready(function() {
  $('#show_no').hide();

    $("#still_medication").change(function() {
        var val = $('#still_medication').val();
        if(val == 'No') {
            $('#show_no').slideDown();
            $('.stop').prop('disabled', false);
        }
        else{
            $('#show_no').slideUp();
            $('.stop').prop('disabled', true);
        }
    });

  $('#show_new').hide();

    $("#reason").change(function() {
        var val = $('#reason').val();
        if(val == '7') {
            $('#show_new').slideDown();
            $('.switch').prop('disabled', false);
        }
        else{
            $('#show_new').slideUp();
            $('.switch').prop('disabled', true);
        }
    });


  $('#show_others').hide();

    $("#real_reason").change(function() {
        var val = $('#real_reason').val();
        if(val == '16') {
            $('#show_others').slideDown();
            $('.other_reason').prop('disabled', false);
        }
        else{
            $('#show_others').slideUp();
            $('.other_reason').prop('disabled', true);
        }
    });







$('#verify').DataTable({
  "ordering": false,
});

});


$("tr.statuscheck input, tr.statuscheck select, tr.statuscheck textarea, tr.statuscheck button").prop('disabled', true);


$('#myForm').bind('submit', function (e) {
    // Disable the submit button while evaluating if the form should be submitted
    $('#submit_form').prop('disabled', true);

    var valid = true;    
    var error = '';

    // If the email field is empty validation will fail
    if ($('#submit_form').val() === '') {
        valid = false;
        error = 'Remarks is required';
    }

    if (!valid) { 
        // Prevent form from submitting if validation failed
        e.preventDefault();

        // Reactivate the button if the form was not submitted
        $('#submit_form').prop('disabled', false);
        
        alert(error);
    }
});

</script>
@endsection

