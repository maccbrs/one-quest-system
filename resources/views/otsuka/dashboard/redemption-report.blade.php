@extends('otsuka.blank')

@section('Patient Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
th { font-size: 12px; }
td { font-size: 11px; }

.med_logo img {
    width:100%;
    height:10%;
}

[hidden] {
  display: none !important;
}

.bar {
    height: 18px;
    background: green;
}
.modal-center {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

/**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
margin-left: 3vw;     
margin-right:3vw;  
}    

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(.5);    
-moz-transform: scale(.5);  
-webkit-transform: scale(.5);  
-o-transform: scale(.5);  
transform: scale(.5);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
ul.gallery {      
margin-left: 15vw;       
margin-right: 15vw;
}

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}

  input {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
  }

 input.noborder {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 0;
  }

  div.border {
      border: 1px solid black;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
  }

  div.padding {
      padding-top: 40px;
      padding-right: 65px;
      padding-bottom: 40px;
      padding-left: 60px;
  }

  h2.space {
    line-height: .25;
  }

  p.ind {
    text-indent: 50px;
    line-height: .1;
  }

</style>

<div class="col-md-12">				
 <h2>For Dispatch</h2>
<div class="col-md-12">
<div class="panel panel-default">
  <div class="panel-heading">Report</div>
  <div class="panel-body">
<table id="example" class="display table table-bordered compact nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>

				<th >Tracking Number</th>
				<th >Booking Number</th>
				<th >PUR NO</th>
				<th >Origin Code</th>
				<th >Destination Code</th>	
				<th >Transaction Date *</th>
				<th >Shipment Mode</th>
				<th >Shipper	</th>
				<th >Department</th>
				<th >Shipper Address	</th>
				<th >Shipper Contact No (Mobile/Landline)</th>
				<th >Paymode</th>
				<th >Declared Value</th>
				<th >Service Mode</th>
				<th >Product</th>
				<th >Consignee</th>
				<th >Consignee Address</th>
				<th >Consignee Contact No (Mobile/Landline)	</th>
				<th >Doc Attachement</th>	
				<th >Document</th>
				<th >Reference No</th>
				<th >Delivery Instructions</th>
				<th >Amount to be Collected</th>
				<th >Special Instructions</th>
				<th >Product</th>
				<th >SKU</th>	
				<th >Quantity (per box)</th>
				<th >Quantity (per tablet)</th>
				<th >Product Inserted</th>
				<th >Redemption Letter</th>
				<th >Welcome Letter</th>
				<th >Newsletter 1Q 2018</th>

            </tr>
        </thead>
        <tfoot>
            <tr>
				<th >Tracking Number</th>
				<th >Booking Number</th>
				<th >PUR NO</th>
				<th >Origin Code</th>
				<th >Destination Code</th>	
				<th >Transaction Date *</th>
				<th >Shipment Mode</th>
				<th >Shipper	</th>
				<th >Department</th>
				<th >Shipper Address	</th>
				<th >Shipper Contact No (Mobile/Landline)</th>
				<th >Paymode</th>
				<th >Declared Value</th>
				<th >Service Mode</th>
				<th >Product</th>
				<th >Consignee</th>
				<th >Consignee Address</th>
				<th >Consignee Contact No (Mobile/Landline)	</th>
				<th >Doc Attachment</th>	
				<th >Document</th>
				<th >Reference No</th>
				<th >Delivery Instructions</th>
				<th >Amount to be Collected</th>
				<th >Special Instructions</th>
				<th >Product</th>
				<th >SKU</th>	
				<th >Quantity (per box)</th>
				<th >Quantity (per tablet)</th>
				<th >Product Inserted</th>
				<th >Redemption Letter</th>
				<th >Welcome Letter</th>
				<th >Newsletter 1Q 2018</th>
				<th >Newsletter 3Q 2017</th>
				<th >Newsletter 4Q 2017</th>
            </tr>
        </tfoot>
        <tbody>
           

                <?php foreach($dispatch_record as $key => $val)
					{?>
						@if(!empty($val->sku_product->skuname))
						@if(($val->sku_product->is_non_med) == "false")
						  <tr>
						<td ><!--Tracking Number --></td>
						<td ><!--Booking Number --></td>
						<td ><!--PUR NO --></td>
						<td ><!--Origin Code --></td>
						<td ><!--Destination Code --></td>
						<td >{{$val->dispatcheddate}}</td>
						<td ></td>
						<td >OTSUKA PHILIPPINES INC</td>
						<td ><!--DEPARTMENT --></td>
						<td >3rd Floor King’s Court II Bldg. 2129, Chino Roces Ave, Makati, 1231</td>
						<td >09998869838/028886774</td>
						<td >COLLECT SHIPPER</td>
						<td ><!--Declared Value --></td>
						<td ><!--Service Mode --></td>
						<td ><!--Product --></td>
						<td >
									<?php if(!empty($val->fetch_patient_details->patient_fullname))
										{
										echo strtoupper(trim($val->fetch_patient_details->patient_fullname, '"'));
										}
									?>
						</td>
						<td >
									<?php if(!empty($val->fetch_patient_details->address))
										{
										echo strtoupper(trim($val->fetch_patient_details->address, '"'));
										}
									?>

						</td>
						<td >
									<?php if(!empty($val->fetch_patient_details->mobile_number))
										{
										echo trim($val->fetch_patient_details->mobile_number, '"');
										}
									?>
									<?php if(!empty($val->fetch_patient_details->mobile_number_2))
										{
											if(!empty($val->fetch_patient_details->mobile_number))
											{
												echo "/ ";
											}
										echo trim($val->fetch_patient_details->mobile_number_2, '"');
										}
									?> 
									<?php if(!empty($val->fetch_patient_details->phone_number))
										{
											if(!empty($val->fetch_patient_details->mobile_number) or !empty($val->fetch_patient_details->mobile_number_2))
											{
												echo "/ ";
											}
										echo trim($val->fetch_patient_details->phone_number,'"');
										}
									?>

						</td>

						<td ><!--Doc Attachement --></td>
						<td ><!--Document --></td>
						<td ><!--Reference No --></td>
						<td ><!--Delivery Instructions --></td>
						<td ><!--Amount to be Collected --></td>
						<td ><!--Special Instructions --></td>
						<td >
							@if(!empty($val->fetch_brand_desc->brandname))
							{{!empty($val->fetch_brand_desc->brandname)?$val->fetch_brand_desc->brandname: ''}}
							@else
							{{!empty($val->sku_product->brand)?$val->sku_product->brand: ''}}
							@endif
						</td>
						<td >
									<?php if(!empty($val->sku_product->skuname))
										{
										echo trim($val->sku_product->skuname,'"');
										}
									?>

						</td>

						@if(($val->sku_product->per_box_zpc != 0) && ($val->sku_product->per_box_zpc != NULL))
						<td ><?php echo $val->qtytabs / $val->sku_product->per_box_zpc ;?></td>
						@else
						<td ></td>
						@endif
						<td >{{$val->qtytabs}}</td>
						<td >YES</td>
						<td >YES</td>
						<td ><!--Product --></td>
						<td ><!--Product --></td>

						 </tr>
						@endif
						@endif

					<?php }
				?>
               
           
            
           
        </tbody>
    </table>
</div>
</div>
</div>
</div>



@endsection 

@section('footer-scripts')
<script>
$(document).ready(function() {


    $('#example').DataTable({
        lengthMenu: [[10, 25, 50, 75, 100, -1], [10,25, 50, 75, 100, "All"]],
        pageLength: 215, 
        "scrollX": true,
        autoWidth: false,
        responsive: true,
        
        scrollCollapse: true,
        dom: 'Blfrtip',
        stateSave: true,
        buttons: [
            {
                extend: 'copyHtml5',
                title: "Reports" + " " + " ",
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            {
                extend: 'excelHtml5',
                title: "Reports" + " " + " ",
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            'colvis'
            ]
            });

 });
</script>


@endsection

