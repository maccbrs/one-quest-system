@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
	text-align: center;
}

</style>


<div class="panel panel-default">
  <div class="panel-heading"><b>Daily Call Raw Data Report {{date('Ymd')}}</b></div>
  <div class="panel-body">

      <div class="col-md-12">
        <table class="display compact nowrap" id="daily">
          <thead>
            <tr>
              <th>Patient Code</th>
              <th>Gender</th>
              <th>Mobile #1</th>
              <th>Call Phase</th>
              <th>(Call Attempt) 1st Call Note</th>
              <th>1st Call (Agent)</th>
              <th>(Call Attempt) 2nd Call Note</th>
              <th>2nd Call (Agent)</th>
              <th>(Call Attempt) 3rd Call Note</th>
              <th>3rd Call (Agent)</th>
              <th>(Call Attempt) 4th Call Note</th>
              <th>4th Call (Agent)</th>
              <th>(Call Attempt) 5th Call Note</th>
              <th>5th Call (Agent)</th>
              <th>Product Prescribed</th>
              <th>SKU Prescribed</th>
              </td>
            </tr>
          </thead>
        <tbody>
          @foreach($data as $d)
          <tr>
            @if($d->calltype == 1)
            <td>{{!empty($d->fetch_upload->fetch_validated->patient_code)?$d->fetch_upload->fetch_validated->patient_code: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_upload->fetch_validated->gender)?$d->fetch_upload->fetch_validated->gender: 'NO AVAILABLE DATA'}}</td>
            @if(!empty($d->fetch_upload->fetch_validated->mobile_number))
            <td>{{!empty($d->fetch_upload->fetch_validated->mobile_number)?$d->fetch_upload->fetch_validated->mobile_number: 'NO AVAILABLE DATA'}}</td>
            @else
            <td>{{!empty($d->fetch_upload->mobile_number)?$d->fetch_upload->mobile_number: 'NO AVAILABLE DATA'}}</td>
            @endif
            <td>RETRIEVAL</td>
            @elseif($d->calltype == 2)
            <td>{{!empty($d->fetch_patient->patient_code)?$d->fetch_patient->patient_code: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->gender)?$d->fetch_patient->gender: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->mobile_number)?$d->fetch_patient->mobile_number: 'NO AVAILABLE DATA'}}</td>
            <td>COMPLIANCE</td>
            @elseif($d->calltype == 4)
            <td>{{!empty($d->fetch_redemption->fetch_redemption_validated->patient_code)?$d->fetch_redemption->fetch_redemption_validated->patient_code: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_redemption->fetch_redemption_validated->gender)?$d->fetch_redemption->fetch_redemption_validated->gender: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_redemption->fetch_redemption_validated->mobile_number)?$d->fetch_redemption->fetch_redemption_validated->mobile_number: 'NO AVAILABLE DATA'}}</td>
            <td>REDEMPTION</td>
            @endif
            <td>{{!empty($d->fetch_verification[0]->callnotes)?$d->fetch_verification[0]->callnotes: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_verification[0]->fetch_name->name)?$d->fetch_verification[0]->fetch_name->name: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_verification[1]->callnotes)?$d->fetch_verification[1]->callnotes: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_verification[1]->fetch_name->name)?$d->fetch_verification[1]->fetch_name->name: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_verification[2]->callnotes)?$d->fetch_verification[2]->callnotes: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_verification[2]->fetch_name->name)?$d->fetch_verification[2]->fetch_name->name: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_verification[3]->callnotes)?$d->fetch_verification[3]->callnotes: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_verification[3]->fetch_name->name)?$d->fetch_verification[3]->fetch_name->name: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_verification[4]->callnotes)?$d->fetch_verification[4]->callnotes: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_verification[4]->fetch_name->name)?$d->fetch_verification[4]->fetch_name->name: 'NO AVAILABLE DATA'}}</td>
            @if($d->calltype == 1)
            <td>{{!empty($d->fetch_upload->medicine)?$d->fetch_upload->medicine: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_upload->fetch_validated->fetch_sku->fetch_prescribed->skuname)?$d->fetch_upload->fetch_validated->fetch_sku->fetch_prescribed->skuname: 'NO AVAILABLE DATA'}}</td>
            @elseif($d->calltype == 2)
            <td>{{!empty($d->fetch_patient->old_retrieval_report->fetch_product->brandname)?$d->fetch_patient->old_retrieval_report->fetch_product->brandname: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->old_retrieval_report->fetch_sku->skuname)?$d->fetch_patient->old_retrieval_report->fetch_sku->skuname: 'NO AVAILABLE DATA'}}</td>
            @elseif($d->calltype == 4)
            <td>{{!empty($d->fetch_redemption->fetch_file->fetch_encoded_dtl->sku_details->brand)?$d->fetch_redemption->fetch_file->fetch_encoded_dtl->sku_details->brand: 'NO AVAILABLE DATA'}}
            <td>{{!empty($d->fetch_redemption->fetch_file->fetch_encoded_dtl->sku_details->skuname)?$d->fetch_redemption->fetch_file->fetch_encoded_dtl->sku_details->skuname: 'NO AVAILABLE DATA'}}</td>
            @endif

          </tr>
          @endforeach
        </tbody>
        </table>
        </div>

    </div>
</div>


      
      





@endsection 

@section('footer-scripts')
<script>
function popupwindow(url) {
	
	title = "Images";
	w = 500;
	h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 


$(document).ready(function() {

var today = new Date();
var dd = today.getDate();
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();

$('#daily').DataTable({
  "ordering": false,
  "scrollX": true,
  dom: 'Blfrtip',
  buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: 'Daily Call Raw Data Report '+yyyy+mm+dd,
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            'colvis'
        ]
});

});
</script>
@endsection

