  <?php $pic = URL::asset('/uploads/');?>

<!-- Modal -->
<div class="modal fade" id="encode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Enroll</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
          <div class="panel panel-default">
              <div class="panel-heading">Encode</div>
              <div class="panel-body">
                <b>MR: </b><br/><input type="text" class="form-control input-sm mr" name="mr" required />
                <b>MD Name: </b><br/><input type="text" class="form-control input-sm" name="mdname" required />
                <b>Product: </b><br/><input type="text" class="form-control input-sm" name="product" required />
                <b>Hospital: </b><br/><input type="text" class="form-control input-sm" name="hospital" required />
              </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Image</h3>
              </div>
              <div class="panel-body">
              	@foreach($images as $image)
                <img src="{{$pic.'/'.$image->filename}}">
                @endforeach
              </div>
            </div>
            </div>
      </div>
      <div class="row">
                    <div class="col-md-12">
          <div class="panel panel-default">
              <div class="panel-heading">Search</div>
              <div class="panel-body">

              </div>
            </div>
            </div>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

</div>
