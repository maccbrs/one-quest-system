@extends('otsuka.master')

@section('Patient MasterFile')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
	$baseurl = URL::asset('/'); 
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
	text-align: center;
}

</style>

<div id="updatepatient" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" id="modalupdate">
  
  </div>
 </div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
        </button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
           <div class="panel panel-default">
            <div class="panel-heading">
              <label for="searchbox" >Search:  </label>
              <input type="text" id="searchbox2" class="form-control" style="display:inline;width:20%"/>
              <input type="hidden" id="filtered2" class="form-control" style="display:inline;width:20%" value="{{!empty($v->fetch_allocation->emp_code)?$v->fetch_allocation->emp_code: ''}}" />
              <select class="form-control" style="display:inline;width:20%" id="person"><option value="globalmd">Global</option></select>
              <button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc2($('#searchbox2').val(),$('#person').val())"></button>
            </div>     
            <div class="panel-body" id="filterdisplay2" style="overflow: scroll;text-align:center">
            </div>
           </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<div class="col-md-12">
<div class="panel panel-default">
    <div class="panel-heading"><strong> Patients <span id="enroll_return_msg"></span></strong></div>
    <div class="panel-body">
		<div class="row">
			<div class="col-md-12">		
						<label for="searchbox" >Filter :	</label>
						<input type="text" id="searchbox" class="form-control" style="display:inline;width:20%"/>
						<select class="form-control" style="display:none;width:20%" id="person2"><option value="patient">Patient</option></select>
						<button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc($('#searchbox').val(),$('#person2').val())"></button>					
						<button class="btn-info btn-sm glyphicon glyphicon-plus" style="float:right"  data-toggle="modal" data-target="#myModal" onclick="$('#action_field').val('save');">Enroll</button>
			  <br/> 
					<div style="overflow-x: scroll;" >			
						<div class="panel-body" id="filterdisplay" >
								</div>						
					</div>				  
			</div>		
		</div>
    </div>
</div>
</div>


<div class="col-md-12">
<div class="panel panel-default">
  <div class="panel-heading">Enroll Patient</div>
  	<div class="panel-body">
      
    <div class="row">

      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Last Name</label>
          <input type="text" class="form-control input-sm" name="patient_lastname" placeholder="Lastname" id="first" value="">
        </div>
      </div>
      <!--  col-md-6   -->

      <div class="col-md-4">
        <div class="form-group">
          <label for="last">First Name</label>
          <input type="text" class="form-control input-sm" name="patient_firstname" placeholder="Firstname" id="last" value="">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="last">Middle Name</label>
          <input type="text" class="form-control input-sm" name="patient_middlename" placeholder="Middlename" id="last" value="">
        </div>
      </div>
      <!--  col-md-6   -->
    </div>

    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="birthdate">Birthdate</label>
          <input type="date" class="form-control input-sm" placeholder="" name="birth_date" id="birthdate" value="">
        </div>
      </div>
      <!--  col-md-6   -->

      <div class="col-md-4">
        <div class="form-group">
          <label for="gender">Gender</label>
          <select class="form-control input-sm" name="gender">
            <option value="">* Please Select Gender</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="gender">Age</label>
          <input type="number" class="form-control input-sm" placeholder="Age" name="age" value="">
        </div>
      </div>
      <!--  col-md-6   -->
    <!--  row   -->

      <div class="col-md-12">
        <div class="form-group">
          <label for="address">Address</label>
          <textarea class="form-control input-sm" name="address" placeholder="Address"></textarea>
        </div>
      </div>
    </div>
      <!--  col-md-6   -->
    <hr>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="mobile1">Mobile No. 1</label>
          <input type="number" class="form-control input-sm" name="mobile_number" placeholder="Mobile 1" value="">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="mobile">Mobile No. 2</label>
          <input type="number" class="form-control input-sm" name="mobile_number_2" placeholder="Mobile 2" value="">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="landline">Landline</label>
          <input type="number" class="form-control input-sm" placeholder="Landline" name="phone_number" value="">
        </div>
      </div>
      <!--  col-md-6   -->
    </div>

  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">Patient Medical Info</div>
    <div class="panel-body">

      <div class="row">
          <div class="col-md-6">
            <label for="Medical Representative">Medical Representative </label><input type="text" class="form-control input-sm" name="p_name" value="" readonly="">
          </div>
          <div class="col-md-5">
            <label for="Doctor">Doctor </label><input type="text" id="md_name" class="form-control input-sm" name="doctor" placeholder="Doctor" value="">
            <input type="hidden" name="doctor_id" id="doctor_id" value="">
          </div>
          <div class="col-md-1">
            <label for="Search">&nbsp; </label><a class="btn btn-primary btn-sm" id="btnLaunch" role="button">Search</a>
          </div>
      </div>
            
      <div class="row">
        <div class="col-md-12">
          <label for="Hospital">Hospital </label><input type="text" class="form-control input-sm" id="hospital" name="hospital" placeholder="Hospital" value="">
        </div>
      </div>

      <div class="row">
          <div class="col-md-6">
            <label for="Product">Product </label>
              <select class="form-control input-sm" id="product">
                <option value="">{{!empty($v->medicine)?$v->medicine: '*Select Product'}}</option>
                <option value="Abilify">Abilify</option>
                <option value="Aminoleban">Aminoleban</option>
                <option value="Pletaal">Pletaal</option>
              </select>
          </div>
        <div class="col-md-6">
          <label for="SKU">SKU</label><select class="form-control input-sm" id="product" name="enroll_product">
							<option>*Please Select Product</option>
			<hr/>
			<div class="row"><div class="col-md-12"><h3>Medical Information</h3></div></div>
				
				<div class="row">
					  <div class="col-md-6">
						<label for="Product">Product </label>
						  <select class="form-control input-sm" id="enroll_product" name="enroll_product" required="">
							<option>*Please Select Product</option>
							<option value="Abilify">Abilify</option>
							<option value="Aminoleban">Aminoleban</option>
							<option value="Pletaal">Pletaal</option>
						  </select>
            </select>
      </div>
    </div>
<hr>
            <div class="row">
					  </div>
					<div class="col-md-6">
					  <label for="SKU">SKU</label><select class="form-control input-sm" id="enroll_sku" name="sku" required="">
						  <option>* Please Select SKU</option>
						  @foreach($Mgabilify as $mab)
						  <option value="{{$mab->id}}">{{$mab->skuname}}</option>
						  @endforeach
						 
						
						</select>
				  </div>
				</div>

			<div class="row">
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Tabs Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="notabsprescribe" class="form-control input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Days Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="nodaysprescribe" class="form-control input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label">Total No. of Tabs</label>
                  <div class="col-md-4">
                    <input type="number" name="totalnotabs" class="form-control input-sm" max="9999">
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="totaltabs" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="totaltabs" value="No Available Data">No Available Data</label>
                </div>
              </div>
            </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
                <label>Already Purchased:</label>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" value="Yes">Yes</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" value="No">No</label>
                </div>

              </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
                <label>Patient Type:</label>
                <div class="radio-inline">
                  <label><input type="radio" name="patient_type" id="purchased_y" value="voluntary">Voluntary</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="patient_type" id="purchased_n" value="referral">Referral</label>
                </div>
              </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
               <!-- <label><input type="checkbox" name="dnc" value="yes">DNC</label> -->
               <button type="submit" class="btn btn-success pull-right" name="encode" value="">Save</button>
              </div>
            </div>

        </div>

</div>








@endsection 

@section('footer-scripts')
<script>
function loadDoc(filter,person) {
	$('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif" style="width:100px;"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
        document.getElementById("filterdisplay").innerHTML = xhttp.responseText;

		$('#patient_search').DataTable();
	
			
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchPatient_only?person_filter=" + filter + "&person=" + person, true);
  xhttp.send();
}

function loadDoc2(filter,person) {
  var empcode = $('#filtered2').val();
  $('#filterdisplay2').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    
    $('.datatabledraw').DataTable(); 
        document.getElementById("filterdisplay2").innerHTML = xhttp.responseText;
    
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchperson?person_filter=" + filter + "&person=" + person + "&emp_code=" + empcode, true);
  xhttp.send();
}

function updatepatient(refid) {	
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
        document.getElementById("modalupdate").innerHTML = xhttp.responseText;
		 
		$( "#birthdate_edit" ).datepicker();
 
		//$('#patient_search').DataTable();
	
			
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/displaypatient?refid=" + refid, true);
  xhttp.send();
}



function popupwindow(url) {
	
	title = "Images";
	w = 500;
	h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}



$(document).ready(function() {

$("#btn_enroll_save").click(function(){
		//$('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
        var x = $("#enroll-form").serialize();
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {

			document.getElementById("enroll_return_msg").innerHTML = "<span style='background-color:#red'>Save</span>"; //xhttp.responseText;
			
			$(location).attr('href', "<?php echo $baseurl;?>otsuka/patient");
			
			}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/patient/enroll_patient?action=save&"   + x , true);
		xhttp.send();
		  
		  
    });



$(".datatable").DataTable();

});


$(function() {
  $('#btnLaunch').click(function() {
    $('#myModal').modal('show');
  });

  $('#btnSave').click(function() {
    var value = $('input').val();
    $('#doctor_name').val(value);;
    $('#myModal').modal('hide');
  });
});

</script>
@endsection

