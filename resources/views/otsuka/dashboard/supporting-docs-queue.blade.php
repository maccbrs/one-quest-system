@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
    <?php $baseurl = URL::asset('/'); ?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
.viber {
	color: 	white !important;
	
	background-color: #8f5db7 !important
}

.red {
	color: white !important;
	
	background-color: #CD5C5C!important
}
.yellow {
	color: black !important;
	background-color: yellow !important;
}
.orange {
	color: white !important;
	background-color: orange !important;
}
.default-color {
	color:#000000;
}

  input {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
  }

 input.noborder {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 0;
  }

  div.border {
      border: 1px solid black;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
  }

  div.padding {
      padding-top: 40px;
      padding-right: 65px;
      padding-bottom: 40px;
      padding-left: 60px;
  }

  h2.space {
    line-height: .25;
  }

  p.ind {
    text-indent: 50px;
    line-height: .1;
  }


</style>

<div class="panel panel-default">
  <div class="panel-heading"><b>Supporting Docs Queue</b></div>
  <div class="panel-body">

      <div class="col-md-12">
			<form method="post" id="encode_form" action="{{route('otsuka.encode')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
						
						
						<table class="display compact datatable table-bordered table-hover" style="font-size:12px;color:#000000">
							<thead>
								<tr>
									<th>UPLOADID</th>	
									<th>UPLOAD DATE</th>
									<th>Uploaded To</th>
									<th>Remarks</th>
									<th>Status</th>
									<th>Link</th>
									

								</tr>
							</thead>
							<tbody>
								
									@if(!empty($supporting_docs_list))
										@foreach($supporting_docs_list as $key => $val)
										<tr>
										<td>{{$val->id}}</td>
										<td>{{$val->created_at}}</td>
										<td>{{$val->uploaded_to}}</td>
										<td>{{$val->remarks}}</td>
										<td>{{$val->frontend_status}}</td>
										<td><a href="{{$pic.'/supportingdocs/' . $val->filename}}" target="_blank" class="btn btn-xs btn-primary">View Image</a></td>
										</tr>
										@endforeach	
									@endif
									
								
								
							</tbody>

						</table>
						
						
						
						
						
						
					<?php /*	<table id="redemption-queue" class="display compact datatable table-bordered table-hover" cellspacing="0" width="100%" style="font-size:10px">
								<thead>
									<tr>
										<th>Px Code</th>
										<th>Px Name</th>
										<th>Product</th>
										<th>SKU</th>
										<th>Transaction Date</th>
										<th>Status</th>
										<th>Remarks</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								  @if($redemption->count())
										@foreach($redemption as $qvcueue_redemption)	
									<?php// pre($qvcueue_redemption)?>
										<tr style="color:#000000">
											<td>{{!empty($qvcueue_redemption->patient_code)?$qvcueue_redemption->patient_code: 'N/A'}}</td>
											<td>{{!empty($qvcueue_redemption->fetch_upload_id->patient_name)?$qvcueue_redemption->fetch_upload_id->patient_name: 'N/A'}}</td>
											<td>{{!empty($qvcueue_redemption->fetch_upload_id->medicine)?$qvcueue_redemption->fetch_upload_id->medicine: 'N/A'}}</td>
											<td>{{!empty($qvcueue_redemption->patient_code)?$qvcueue_redemption->patient_code: 'N/A'}}</td>
											<td>{{!empty($qvcueue_redemption->status)?$qvcueue_redemption->status: 'N/A'}}</td>								
											<td>{{!empty($qvcueue_redemption->patient_code)?$qvcueue_redemption->patient_code: 'N/A'}}</td>
											<td>{{!empty($qvcueue_redemption->fetch_upload_id->remarks)?$qvcueue_redemption->fetch_upload_id->remarks: 'N/A'}}</td>
											  <td><a href="<?php echo $baseurl;?>otsuka/redemption" name="encode" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Verify</a></td>
										</tr>
										@endforeach
								  @endif
										
								</tbody>
						</table> */?>
				</form>
		</div>
	</div>
</div> <!-- REDEMPTION QUEUEING -->		

@endsection 

@section('footer-scripts')
<script>


	function processRedemption(upload_id) {
   var str = "";
				 $(".upload_id_checkbox:checked").each(function() {
					str += $(this).val() + ",";
			  });
			  str = str.substr(0, str.length - 1);
			  alert(str);
				
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id=" + str);
		
		//var xhttp = new XMLHttpRequest();
		//xhttp.onreadystatechange = function() {
		//if (this.readyState == 4 && this.status == 200) {	
		
		//document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		//alert('Success');
		//document.getElementsByTagName("body").innerHTML = xhttp.responseText;
		//}
		//};
		//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id, true);
		//xhttp.send();					 
		}





function hideupload(id) {
	//alert(id);
 	var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/agent");
		//$('#upload_queue_id_' + id).remove();
		//$(".upload-datatable").DataTable();
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/disable?active=" + id , true);

  xhttp.send();	 
    
}



$(document).ready(function() {

/*            setTimeout(function() {
                document.location.reload(true);
            }, 3500);*/

$(".datatable").DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
});

});
</script>
@endsection

