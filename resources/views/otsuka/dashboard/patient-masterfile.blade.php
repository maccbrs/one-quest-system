@extends('otsuka.master')

@section('Patient MasterFile')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
	$baseurl = URL::asset('/'); 
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
	text-align: center;
}

</style>


<div class="modal fade" id="myModal_Search_MD">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
        </button>
        <h4 class="modal-title">Doctor Info</h4>
      </div>
      <div class="modal-body">
           <div class="panel panel-default">
            <div class="panel-heading">
              <label for="searchbox" >Search:  </label>
              <input type="text" id="searchbox2" class="form-control" style="display:inline;width:20%"/>
              <input type="hidden" id="person2" value = "globalmdv2">
              <button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc2($('#searchbox2').val(),$('#person2').val())"></button>
            </div>     
            <div class="panel-body" id="filterdisplay2" style="overflow: scroll;text-align:center">
            </div>
           </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<div class="col-md-12">
<div class="panel panel-default">
		

    <div class="panel-heading">
    	<strong> Patients <span id="enroll_return_msg"></span></strong>
    	<button class="btn-info btn-xs glyphicon glyphicon-plus pull-right" data-toggle="collapse" data-target="#show" onclick="$('#searchpatient').fadeToggle();">Enroll</button>
    </div>
    <div class="panel-body" id="searchpatient">

		<div class="row">
			<div class="col-md-12">
			
						<label for="searchbox" >Filter :	</label>
						<input type="text" id="searchbox" class="form-control input-sm" style="display:inline;width:20%"/>
						<select class="form-control input-sm" style="display:none;width:20%" id="person"><option value="patient">Patient</option></select>
						<button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc($('#searchbox').val(),$('#person').val())"></button>
<!-- 						<button class="btn-info btn-sm glyphicon glyphicon-plus" style="float:right"  data-toggle="modal" data-target="#myModal" onclick="$('#action_field').val('save');">Enroll</button> -->
			  <br/>
			  
					<div style="overflow-x: scroll;" >
						
						<div class="panel-body" id="filterdisplay" >
								
						
						</div>
						
					</div>	

			  
			</div>
			
		</div>
      <hr>
	
	


    </div>
</div>
</div>


<!-- Modal -->
<div id="updatepatient" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" id="modalupdate">
  
  </div>
 </div>




  <!-- <input type="text" id="enroll_refid" name="enroll_refid" > -->
<!--         <h2>Patient Kit Number : <input type="text" id="patient_kit_reserve" name="enroll_patient_kit_number" list="browsers"/>
		  <datalist id="browsers" name="browser">
			
			@foreach($Reserve_Patient_Kit as $reserve => $val)
			<option value="{{$val->one_quest_id}}">
			@endforeach
		  </datalist> -->
	
<!-- 		<label style="float:right;font-size:15px"><input type="checkbox" name="enroll_patient_consent" id="enroll_patient_consent"> Patient Consent</label></h2>    --> 

<div class="col-md-12 collapse" id="show"> 
<div class="panel panel-default">
   <div class="panel-heading"><strong>Enroll Patient<span id="enroll_return_msg"></span></strong></div>
    <div class="panel-body">
	 <form action="{{route('otsuka.patient.enroll')}}" id="enroll-form">
	{{ csrf_field() }}
       <div class="col-md-12">
			<div class="row">

				
				<div class="col-md-4">
					<div class="form-group">
					  <label for="enroll_firstname">First:</label>
					  <input type="text" class="form-control input-sm" id="enroll_firstname"  name="enroll_firstname">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					  <label for="enroll_middlename">Middle:</label>
					  <input type="text" class="form-control input-sm" id="enroll_middlename"  name="enroll_middlename">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					  <label for="enroll_lastname">Lastname:</label>
					  <input type="text" class="form-control input-sm" id="enroll_lastname"  name="enroll_lastname">
					</div>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Mobile No:</label>
					  <input type="number" class="form-control input-sm" id="enroll_mobile_no" name="enroll_mobile_no">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Mobile No 2:</label>
					  <input type="number" class="form-control input-sm" id="enroll_mobile_no_2"  name="enroll_mobile_no_2">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Landline:</label>
					  <input type="number" class="form-control input-sm" id="enroll_phone_no"  name="enroll_phone_no">
					</div>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">Birth Date:</label>
					  <input type="text" class="form-control input-sm datepicker"  id="enroll_birthdate"  name="enroll_birthdate">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">Age:</label>
					  <input type="number" class="form-control input-sm" id="enroll_age"  name="enroll_age">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">NickName:</label>
					  <input type="text" class="form-control input-sm" id="enroll_nickname"  name="enroll_nickname">
					</div>
				</div>	
				
				<div class="col-md-3">
					<div class="form-group">			
					  <label for="email">Gender:</label>
					  <select name="enroll_gender" class="form-control input-sm">
						<option value="Px Won't Disclose">Px Won't Disclose</option><option value="Male">Male</option><option value="Female" >Female</option>
					  </select>
					 <!-- <input type="hidden" id="enroll_gender_text" name="enroll_gender_text"/>
					  <br/><input type="radio"   name="enroll_gender">Male
					  <br/><input type="radio"   name="enroll_gender">Female
					  <br/><input type="radio"   name="enroll_gender">Px Won't Disclose -->
					</div>
				</div>	
				
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					  <label for="email">Address</label>
					  <textarea name="enroll_address" class="form-control input-sm"></textarea>
					  <!--<input type="text" class="form-control input-sm" id="enroll_doctors_name"  name="email">-->
					</div>
				</div>	
			</div>	

			<hr/>
		


 <div class="row">
          <div class="col-md-2">

            <label for="Medical Representative">MR Code</label><input type="text"   class="form-control input-sm inputrecord_med" id="patient_mr_id" name="patient_mr_id" value="<?php if(!empty($patient_record[0]->patient_mr_name)){echo $patient_record[0]->patient_mr_id;} ?>" >			

          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MR Representative </label><input type="text"   class="form-control input-sm inputrecord_med" id="patient_mr_name" name="patient_mr_name" value="<?php if(!empty($patient_record[0]->patient_mr_name)){echo $patient_record[0]->patient_mr_name;} ?>" >			
          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MR Area Code</label><input type="text"   class="form-control input-sm inputrecord_med" id="patient_tagging_area_code" name="patient_tagging_area_code" value="<?php if(!empty($patient_record[0]->patient_tagging_area_code)){echo $patient_record[0]->patient_tagging_area_code;} ?>" >			
          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MR Team </label><input type="text"   class="form-control input-sm inputrecord_med" id="patient_tagging_team" name="patient_tagging_team" value="<?php if(!empty($patient_record[0]->patient_tagging_team)){echo $patient_record[0]->patient_tagging_team;} ?>" >			
          </div>
		  <div class="col-md-4">
						
				<label for="Search">&nbsp; </label><br/><a class="btn btn-primary btn-sm" id="btnLaunch" style="" role="button">Search</a>
					
			
          </div> 
         
      </div>	
	  
		<div class="row">
          <div class="col-md-2">
            <label for="Medical Representative">Doctor Id </label><input type="text"   class="form-control input-sm inputrecord_med" id="patient_tagging_doctorid" name="patient_tagging_doctorid" value="<?php if(!empty($patient_record[0]->patient_tagging_doctorid)){echo $patient_record[0]->patient_tagging_doctorid;} ?>" >			
          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MD Emp Code </label><input type="text"   class="form-control input-sm inputrecord_med" id="patient_tagging_emp_doctorid" name="patient_tagging_emp_doctorid" value="<?php if(!empty($patient_record[0]->patient_tagging_emp_doctorid)){echo $patient_record[0]->patient_tagging_emp_doctorid;} ?>">			
          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MD Name </label><input type="text"   class="form-control input-sm inputrecord_med" id="patient_tagging_doctor_name" name="patient_tagging_doctor_name" value="<?php if(!empty($patient_record[0]->patient_tagging_doctor_name)){echo $patient_record[0]->patient_tagging_doctor_name;} ?>" >			
          </div>
         <div class="col-md-3">
            <label for="Medical Representative">Hospital </label><input type="text"   class="form-control input-sm inputrecord_med" id="patient_tagging_hospital" name="patient_tagging_hospital" value="<?php if(!empty($patient_record[0]->patient_tagging_hospital)){echo $patient_record[0]->patient_tagging_hospital;} ?>" >			
          </div>
         <div class="col-md-1">
            <label for="Medical Representative">MD Class </label><input type="text" =""  class="form-control input-sm inputrecord_med" id="patient_tagging_md_class" name="patient_tagging_md_class" value="<?php if(!empty($patient_record[0]->patient_tagging_md_class)){echo $patient_record[0]->patient_tagging_md_class;} ?>" >			
          </div>
         <div class="col-md-2">
            <label for="Medical Representative">MD Specialty </label><input type="text"  class="form-control input-sm inputrecord_med" id="patient_tagging_specialty" name="patient_tagging_specialty" value="<?php if(!empty($patient_record[0]->patient_tagging_specialty)){echo $patient_record[0]->patient_tagging_specialty;} ?>" >			
          </div>          
      </div>	
	   <div class="row" style="">
        <div class="col-md-2" >
          <label for="doctor_wont_disclose">Patient won't disclose </label><input type="checkbox"  class="form-control input-xs inputrecord_med" onclick="disableDoctor()" id="doctor_wont_disclose" <?php if(!empty($patient_record[0]->doctor_wont_disclose)){ if($patient_record[0]->doctor_wont_disclose=="true") {echo "checked";}} ?> name="doctor_wont_disclose" placeholder="patient_wont_disclose" >
        </div>
      </div>	  
	
		
      <div class="row" style="display:none">

          <div class="col-md-4">
            <label for="Medical Representative">Medical Representative </label><input type="text" readonly="" class="form-control input-sm inputrecord_med" name="medrep_name" id="mr_name" value="" readonly="" placeholder="Medical Representative">
            <input type="hidden" readonly="" class="form-control input-sm inputrecord_med" name="medrep_id" id="mr_id" value="" readonly="" placeholder="Medical Representative">
          </div>
		  <div class="col-md-3">
			  <label for="Doctorid">Doctor ID</label><input type="text" readonly=""  class="form-control input-sm inputrecord_med"  name="doctor_id" id="doctor_id" value="" placeholder="Doctor ID">
		  </div>
		  <div class="col-md-4">
			  <label for="Doctor">Doctor Name</label><label style="margin-left: 20px;"><input type="checkbox" name="doctor_wont_disclose" id="doctor_wont_disclose" value="true"> Px Won't Disclose</label><input type="text" id="md_name" class="form-control input-sm inputrecord_med" name="enroll_doctors_name" placeholder="Doctor" value="">
		  </div>
		  <div class="col-md-1">
			  <label for="Search">&nbsp; </label><br/><a class="btn btn-primary btn-sm" id="btnLaunch2" role="button">Search</a>
		  </div>

      </div>
            
      <div class="row">
        <div class="col-md-12" style="display:none">
          <label for="Hospital">Hospital </label><input type="text" readonly="" class="form-control input-sm inputrecord_med" id="hospital" name="hospital" placeholder="Hospital" value="">
        </div>
      </div>
				
<!-- 			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Doctor's Name:</label>
					  <input type="text" class="form-control input-sm" id="enroll_doctors_name"  name="enroll_doctors_name">
					</div>
				</div>
	          <div class="col-md-1">
	            <label for="Search">&nbsp; </label><br/><a class="btn btn-primary btn-sm" id="btnLaunch" role="button">Search</a>
	          </div>	
				<div class="col-md-2">
					<div class="form-group">
					 <label><input type="checkbox" name="enroll_doctor_consent" id="enroll_doctor_consent" value="true"> Doctor's Consent</label>
					 <label><input type="checkbox" name="doctor_wont_disclose" value="true"> Px Won't Disclose</label>
					</div>
				</div>	
				<div class="col-md-5">
					<div class="form-group">
					  <label for="email">Hospital:</label>
					  <input type="text" class="form-control input-sm"  name="email">
					</div>
				</div>	
			</div>	 -->		

			<hr/>

			<div class="row">
				<div class="col-md-12">
					<h4>Medical Information</h4>
				</div>
			</div>
				
				<div class="row">
					  <div class="col-md-6">
						<label for="Product">Product </label>
						  <select class="form-control input-sm input-sm" id="enroll_product" name="enroll_product" required="">
							<option value="">*Please Select Product</option>
							<option value="Abilify">Abilify</option>
							<option value="Aminoleban">Aminoleban</option>
							<option value="Pletaal">Pletaal</option>
						  </select>
					  </div>
					<div class="col-md-6">
					  <label for="SKU">SKU</label><select class="form-control input-sm input-sm" id="enroll_sku" name="sku" required="">
						  <option value="">* Please Select SKU</option>
						  @foreach($Mgabilify as $mab)
						  <option value="{{$mab->id}}">{{$mab->skuname}}</option>
						  @endforeach
						 
						
						</select>
				  </div>
				</div>

<hr/>

			<div class="row">
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Tabs Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="notabsprescribe" id="notabsprescribe" class="form-control input-sm input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Days Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="nodaysprescribe" id="nodaysprescribe" class="form-control input-sm input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label">Total No. of Tabs</label>
                  <div class="col-md-4">
                    <input type="number" name="totalnotabs" id="nodaysprescribe" class="form-control input-sm input-sm" max="9999">
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" id="tabsprescribe_disclose" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" id="tabsprescribe_no_data" value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" id="daysprescribe_disclose" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" id="daysprescribe_no_data" value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="totaltabs" id="totaltabs_disclose" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="totaltabs" id="totaltabs_nodata" value="No Available Data">No Available Data</label>
                </div>
              </div>
            </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
                <label>Already Purchased:</label>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" id="purchased_y" value="Yes">Yes</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" id="purchased_n" value="No">No</label>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <label>Patient Type:</label>
                <div class="radio-inline">
                  <label><input type="radio" name="patient_type" id="purchased_y" value="voluntary" required="">Voluntary</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="patient_type" id="purchased_n" value="referral">Referral</label>
                </div>
              </div>
            </div>

        </form>

<!--            <div class="panel panel-default">
            <div class="panel-heading">
              <label for="searchbox" >Search:  </label>
              <input type="text" id="searchbox2" class="form-control input-sm" style="display:inline;width:20%"/>
              <input type="hidden" id="filtered2" class="form-control input-sm" style="display:inline;width:20%" value="{{!empty($v->fetch_allocation->emp_code)?$v->fetch_allocation->emp_code: ''}}" />
              <select class="form-control input-sm" style="display:inline;width:20%" id="person2"><option value="filteredmd">MR Doctor</option><option value="globalmd">Global</option></select>
              <button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc2($('#searchbox2').val(),$('#person2').val())"></button>
            </div>     
            <div class="panel-body" id="filterdisplay2" style="overflow: scroll;text-align:center">
            </div>
           </div>  --> 
          <hr/>
		<button type="button" id="btn_enroll_save" class="btn btn-info btn-default">Save</button>
		
      </div>
		<!--<button type="button" style="float:left; " id="btn_enroll_save" class="btn btn-primary btn-default">Save </button> -->
      </div>





@endsection 

@section('footer-scripts')
<script>
function loadDoc(filter,person) {
	$('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif" style="width:100px;"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
        document.getElementById("filterdisplay").innerHTML = xhttp.responseText;

		$('#patient_search').DataTable();
	
			
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchPatient_only?person_filter=" + filter + "&person=" + person, true);
  xhttp.send();
}

function loadDoc2(filter,person) {
  $('#filterdisplay2').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif" style="width:100px;"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    
    
        document.getElementById("filterdisplay2").innerHTML = xhttp.responseText;
    
    	$('#example2').DataTable(); 
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchperson?person_filter=" + filter + "&person=" + person, true);
  xhttp.send();
}

function disableDoctor() {
  // Get the checkbox
  var checkBox = document.getElementById("doctor_wont_disclose");
  // Get the output text
  
  if (checkBox.checked == true){
		
		$("#patient_tagging_doctor_name").val("");
		
		$("#patient_tagging_hospital").val("");
		$("#patient_tagging_md_class").val("");
		$("#patient_tagging_specialty").val("");
		$("#patient_tagging_doctorid").val("");
		$("#patient_tagging_emp_doctorid").val("");
		//document.getElementById("md_name").value()

		$("#md_name").val("");
		$("#hospital").val("");
		$("#doctor_id").val("");
  } 
}


function useMD(id) {
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		//document.getElementById(xhttp.responseText['id']);
		
	
        //alert(xhttp.responseText['id']);
		var obj = JSON.parse(xhttp.responseText);
				
		$("#patient_mr_id").val(obj.empcode); 
		$("#patient_mr_name").val(obj.mrname);
		$("#patient_tagging_team").val(obj.etmscode);
		$("#patient_tagging_area_code").val(obj.areacode);
		$("#patient_tagging_doctor_name").val(obj.mdname);
		$("#doctor_wont_disclose").prop('checked', false);
		$("#patient_tagging_hospital").val(obj.hospital);
		$("#patient_tagging_md_class").val(obj.mdclass);
		$("#patient_tagging_specialty").val(obj.specialty);
		$("#patient_tagging_doctorid").val(obj.doctorid);
		$("#patient_tagging_emp_doctorid").val(obj.employee);	
		//document.getElementById("md_name").value()
		$("#mr_id").val(obj.empcode);
		$("#mr_name").val(obj.mrname);
		$("#md_name").val(obj.mdname);
		$("#hospital").val(obj.hospital);
		$("#doctor_id").val(obj.doctorid);
		
		$('#myModal_Search_MD').modal('hide');
		
		
		
        //alert(obj.mdname);
		
    }
  };


  //xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + ""+ "&sku_id=" + skuid +  "&qtytabs=" + $("#redeemable_tabs").val() + "&patient_id=" + $('#patient_id').val(), true);
  xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/get_national_md_record?national_md_id=" + id , true);
  xhttp.send(); 
}

function updatepatient(refid) {	
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
        document.getElementById("modalupdate").innerHTML = xhttp.responseText;
		 
		$( "#birthdate_edit" ).datepicker();
 
		//$('#patient_search').DataTable();
	
			
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/displaypatient?refid=" + refid, true);
  xhttp.send();
}



function popupwindow(url) {
	
	title = "Images";
	w = 500;
	h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}



$(document).ready(function() {

  $('#btnLaunch').click(function() {
    $('#myModal_Search_MD').modal('show');
  });	

  $('#doctor_wont_disclose').click(function(){
    if($('#md_name').prop('disabled'))
    {
     $('#md_name').prop('disabled', false)
    }
    else{
         $('#md_name').prop('disabled', true)
      }
    });

$("#btn_enroll_save").click(function(){
		//$('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
        var x = $("#enroll-form").serialize();
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {

			document.getElementById("enroll_return_msg").innerHTML = "<span style='background-color:#red'>Save</span>"; //xhttp.responseText;
			
			$(location).attr('href', "<?php echo $baseurl;?>otsuka/patient");
			
			}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/patient/enroll_patient?action=save&"   + x , true);
		xhttp.send();
		  
		  
    });



$(".datatable").DataTable();

});

</script>
@endsection

