@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $old_pic = URL::asset('/uploads/card_uploads');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}

.font-red {
  color: red !important;
  
}


<style type="text/css">
  /**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
margin-left: 3vw;     
margin-right:3vw;  
}    

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(8);    
-moz-transform: scale(8);  
-webkit-transform: scale(8);  
-o-transform: scale(8);  
transform: scale(8);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
ul.gallery {      
margin-left: 15vw;       
margin-right: 15vw;
}

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}
</style>

</style>




<div class="col-md-12">
    <form method="get" id="verify_encode" action="{{route('otsuka.compliance.ref-voluntary-save')}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        <input type="hidden" name="v_id" value="{{!empty($pr->fetch_verification_queue_compliance->id)?$pr->fetch_verification_queue_compliance->id: ''}}">
			<div class="panel panel-default">

      <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">Retrieval Kit</div>
              <div class="panel-body">  
                <?php
                $Upload = new App\Http\Models\otsuka\Upload;
                $upload = $Upload->where('patientid',$patient_record->id)->where('trail_status','!=',199)->first();
                ?>
                <?php
                $Old_retrieval_enrollment = new App\Http\Models\otsuka\Old_retrieval_enrollment;
                $old_retrieval_enrollment = $Old_retrieval_enrollment->where('patientid',$patient_record->id)->first();
                ?>
             @if(!empty($upload))   
            <img class="zoom" style="width: 10%; height: 10%;" src="{{$pic.'/'.$upload->filename}}">
             <a href="#" onclick="popupwindow('{{$pic.'/'.$upload->filename}}');"><img class="" style="width: 10%; height: 10%;" src="{{$pic.'/'.$upload->filename}}"></a>
             @elseif(empty($upload) and !empty($old_retrieval_enrollment))
             @if(!empty($old_retrieval_enrollment->imagefilepath))
            <img class="zoom" style="width: 10%; height: 10%;" src="{{$old_pic.'/'.$old_retrieval_enrollment->imagefilepath}}">
             <a href="#" onclick="popupwindow('{{$old_pic.'/'.$old_retrieval_enrollment->imagefilepath}}');"><img class="" style="width: 10%; height: 10%;" src="{{$old_pic.'/'.$old_retrieval_enrollment->imagefilepath}}"></a>
             @else
             NO AVAILABLE DATA
             @endif
             @else
             NO AVAILABLE DATA
             @endif
              </div>
            </div>
          </div>
        </div>

			  <div class="panel-heading">Patient Details  
					<label class="switch" style="float:right">UPDATE
					<input type="checkbox" checked><span class="slider round"></span></label>
			  </div>
				<div class="panel-body">

				  <div class="row">
					<div class="col-md-3">
					  <div class="well">
						  <label for="Patient Code">Patient Code:</label>
						  <input type="text" name="p_id" class="form-control input-sm" style="color: green; font-weight: bold;" value="{{!empty($patient_record->patient_code)?$patient_record->patient_code: ''}}" readonly>
						</div>
					  </div>
					<div class="col-md-3">
					  <div class="well">
						<label for="Patient Kit #">Patient Kit #:</label>
						<input type="text" name="patient_kit_number" class="form-control input-sm" style="color: green; font-weight: bold;" value="{{!empty($patient_record->patient_kit_number)?$patient_record->patient_kit_number: ''}}" readonly="">
					  </div>
					</div>



					</div>

				<div class="row">

				  <div class="col-md-12">
					<div class="form-group">
					  <label for="details">Patient Name:</label>
					  <input type="text" class="form-control input-sm" placeholder="" name="patient_fullname" id="name" value="{{!empty($patient_record->patient_fullname)?$patient_record->patient_fullname: ''}}">
					</div>
				  </div>

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="first">Last Name</label>
					  <input type="text" class="form-control input-sm inputrecord" name="patient_lastname" value="{{!empty($patient_record->patient_lastname)?$patient_record->patient_lastname: ''}}" placeholder="Last Name" id="">
					</div>
				  </div>
				  <!--  col-md-6   -->

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="last">First Name</label>
					  <input type="text" class="form-control input-sm inputrecord" value="{{!empty($patient_record->patient_firstname)?$patient_record->patient_firstname: ''}}" name="patient_firstname" placeholder="First Name" id="">
					</div>
				  </div>

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="last">Middle Name</label>
					  <input type="text" class="form-control input-sm inputrecord" value="{{!empty($patient_record->patient_firstname)?$patient_record->patient_firstname: ''}}" name="patient_middlename" placeholder="Middle Name" id="last">
					</div>
				  </div>
				  <!--  col-md-6   -->
				</div>


				<div class="row">
				  <div class="col-md-4">
					<div class="form-group">
					  <label for="birthdate">Birthdate </label>					 
					  <input type="date" class="form-control input-sm" value="{{!empty(date('Y-m-d', strtotime($patient_record->birth_date)))?date('Y-m-d', strtotime($patient_record->birth_date)): ''}}" placeholder="Birthdate" name="birth_date" id="birthdate">
					</div>
				  </div>
				  <!--  col-md-6   -->

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="gender">Gender</label>
					  <select class="form-control input-sm"  name="gender">
						<option value="{{!empty($patient_record->gender)?$patient_record->gender: ''}}">{{!empty($patient_record->gender)?$patient_record->gender: '* Please Select Gender'}}</option>
						<option value="Male">Male</option>
						<option value="Female">Female</option>
					  </select>
					</div>
				  </div>

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="gender">Age</label>
					  <input type="number" class="form-control input-sm inputrecord" placeholder="Age" name="age" value="{{!empty($patient_record->age)?$patient_record->age: ''}}">
					</div>
				  </div>
				  <!--  col-md-6   -->
				<!--  row   -->

				  <div class="col-md-12">
					<div class="form-group">
					  <label for="address">Address</label>
					  <input type="text" class="form-control input-sm inputrecord" id="address" name="address" placeholder="Address" value="{{!empty($patient_record->address)?$patient_record->address: ''}}">
					</div>
				  </div>
				</div>
				  <!--  col-md-6   -->
				<hr>
				<div class="row">
				  <div class="col-md-4">
					<div class="form-group">
					  <label for="mobile1">Mobile No. 1</label>
					  <input type="number" class="form-control input-sm inputrecord" name="mobile_number" placeholder="Mobile 1" value="{{!empty($patient_record->mobile_number)?$patient_record->mobile_number: 'N/A'}}">
					</div>
				  </div>

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="mobile">Mobile No. 2</label>
					  <input type="number" class="form-control input-sm inputrecord" name="mobile_number_2" placeholder="Mobile 2" value="{{!empty($patient_record->mobile_number_2)?$patient_record->mobile_number_2: 'N/A'}}">
					</div>
				  </div>

				  <div class="col-md-4">
					<div class="form-group">
					  <label for="landline">Landline</label>
					  <input type="number" class="form-control input-sm inputrecord" placeholder="Landline" name="phone_number" value="{{!empty($patient_record->phone_number)?$patient_record->phone_number: 'N/A'}}">
					</div>
				  </div>
				  <!--  col-md-6   -->
				</div>


			  </div>
			</div>		



  <div class="panel panel-default">
    <div class="panel-heading">Compliance</div>
    <div class="panel-body">
	<div class="col-md-12">
		<input type="hidden" name="cmid" value="{{!empty($pr->id)?$pr->id: ''}}">
		Patient Code: <b>{{!empty($patient_record->patient_code)?$patient_record->patient_code: ''}}</b><br/>
    Initial Compliance Date: {{!empty($pr->created_at)?$pr->created_at: ''}}<br/>
    Last Compliance Date: {{!empty($pr->updated_at)?$pr->updated_at: ''}}
    <hr/><input type="hidden" class="form-control input-sm" name="patientid" value="{{!empty($patient_record->id)?$patient_record->id: ''}}">
    

		<label>Product Taken: </label><input type="text" class="form-control input-sm" name="product_taken" value="{{!empty($patient_record->patient_tagging_product_prescribe)?$patient_record->patient_tagging_product_prescribe: ''}}" readonly="">


   
		<label>Sku Taken: </label><input type="text" class="form-control input-sm" name="sku" value="{{!empty($patient_record->patient_tagging_sku_prescribe)?$patient_record->patient_tagging_sku_prescribe: ''}}" readonly="">

    

    @if(!empty($patient_record->created_at))
		<label>Date of Start Medication: </label><input type="text" class="form-control input-sm" name="date_start_medication" value="{{!empty($patient_record->created_at)?date('m/d/Y',strtotime($patient_record->created_at)): ''}}" readonly="">
   
    @endif

		<label>Still on Medication: </label><select class="form-control input-sm" name="still_medication">
      <option value="">* Select</option>
      <option value="Yes">Yes</option>
      <option value="No">No</option>
    </select>

    <label>Status: </label><select class="form-control input-sm" name="reason" required="">
      <option value="">* Select Reason</option>
      @foreach($drop_reasons as $d)
      <option value="{{!empty($d->id)?$d->id: ''}}">{{!empty($d->name)?$d->name: ''}}</option>
            @endforeach        
                    </select>

					{{--<label>Reason of Not on Medication </label><small class="font-red"> (*If Not on Medication)</small>: <input type="text" class="form-control input-sm" name="reason_not_med" value="" placeholder="Reason">--}}
					
	<label>Reason of Not on Medication: </label>
	<select class="form-control input-sm" name="reason_not_med" id="reason_not_med">
      <option value="">* Select</option>
      <option value="Financial Problem">Financial Problem</option>
      <option value="Doctor's Advice">Doctor's Advice</option>
      <option value="Px Disposition">Px Disposition</option>
      <option value="Others">Others</option>
    </select>
	<p id ="p_toggle_reason" style="display:hidden">
	<label>Other Reason of Not on Medication: </label>
	<input type="text" class="toggle_reason" name="others_reason_not_med" id ="others_reason_not_med"/>
	</p>
	
	
	
		<label>Month Stopped </label><small class="font-red"> (*If Not on Medication)</small>: <select class="form-control input-sm" name="month_stopped">
			<option value="">* Select Month</option>
			@foreach($drop_months as $m)
			<option value="{{!empty($m->id)?$m->id: ''}}">{{!empty($m->name)?$m->name: ''}}</option>
            @endforeach        
                    </select>

		<label>New Medication </label><small class="font-red"> (*if switched medication)</small>: <input type="text" class="form-control input-sm" name="medication" value="" placeholder="New Medication">
		<label>Remarks: </label><input type="text" class="form-control input-sm" value="" name="remarks" placeholder="Remarks">
		<label>Notes: </label><textarea class="form-control" rows="5" name="callnotes" id="callnotes" placeholder="Notes"></textarea>
		<hr/>
		<button type="submit" class="btn btn-success pull-right" id="test" value="">Submit</button>
	</div>
	</div>
</form>
</div>

</div>



      
      





@endsection 

@section('footer-scripts')
<script>
function popupwindow(url) {
  
  title = "Images";
  w = 500;
  h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
  
$(document).ready(function() {


$('#reason_not_med').change(function() {
    if ($(this).val() === 'Others') {
      $("#others_reason_not_med").removeAttr("disabled", "disabled");
	   $("#p_toggle_reason").show();
	   
    }
	else{
		 $("#others_reason_not_med").attr( "disabled", "disabled" );
		 $("#p_toggle_reason").hide();
	}
});



$(".datatable").DataTable({
  "ordering": false,
});

});

$(document).ready(function() {



$('#verify').DataTable({
  "ordering": false,
});

});


$("tr.statuscheck input, tr.statuscheck select, tr.statuscheck textarea, tr.statuscheck button").prop('disabled', true);

</script>
@endsection

