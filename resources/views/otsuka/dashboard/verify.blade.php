@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('../uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
  <?php $baseurl = URL::asset('/'); ?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #CD5C5C!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}

label.right-label {
        text-align: right;
}

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
margin-top: 50%;
-ms-transform: scale(3);    
-moz-transform: scale(3);  
-webkit-transform: scale(3);  
-o-transform: scale(3);  
transform: scale(3);    
position:relative;      
z-index:100;  
}

.form-group input[type="checkbox"] {
    display: none;
}

.form-group input[type="checkbox"] + .btn-group > label span {
    width: 20px;
}

.form-group input[type="checkbox"] + .btn-group > label span:first-child {
    display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span:last-child {
    display: inline-block;   
}

.form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
    display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
    display: none;   
}

.nav-tabs.wizard {
  background-color: transparent;
  padding: 0;
  width: 100%;
  margin: 1em auto;
  border-radius: .25em;
  clear: both;
  border-bottom: none;
}

.nav-tabs.wizard li {
  width: 100%;
  float: none;
  margin-bottom: 3px;
}

.nav-tabs.wizard li>* {
  position: relative;
  padding: 1em .8em .8em 2.5em;
  color: #999999;
  background-color: #dedede;
  border-color: #dedede;
}

.nav-tabs.wizard li.completed>* {
  color: #fff !important;
  background-color: #96c03d !important;
  border-color: #96c03d !important;
  border-bottom: none !important;
}

.nav-tabs.wizard li.active>* {
  color: #fff !important;
  background-color: #2c3f4c !important;
  border-color: #2c3f4c !important;
  border-bottom: none !important;
}

.nav-tabs.wizard li::after:last-child {
  border: none;
}

.nav-tabs.wizard > li > a {
  opacity: 1;
  font-size: 14px;
}

.nav-tabs.wizard a:hover {
  color: #fff;
  background-color: #2c3f4c;
  border-color: #2c3f4c
}

span.nmbr {
  display: inline-block;
  padding: 10px 0 0 0;
  background: #ffffff;
  width: 35px;
  line-height: 100%;
  height: 35px;
  margin: auto;
  border-radius: 50%;
  font-weight: bold;
  font-size: 16px;
  color: #555;
  margin-bottom: 10px;
  text-align: center;
}

@media(min-width:992px) {
  .nav-tabs.wizard li {
    position: relative;
    padding: 0;
    margin: 4px 4px 4px 0;
    width: 19.6%;
    float: left;
    text-align: center;
  }
  .nav-tabs.wizard li.active a {
    padding-top: 15px;
  }
  .nav-tabs.wizard li::after,
  .nav-tabs.wizard li>*::after {
    content: '';
    position: absolute;
    top: 1px;
    left: 100%;
    height: 0;
    width: 0;
    border: 45px solid transparent;
    border-right-width: 0;
    /*border-left-width: 20px*/
  }
  .nav-tabs.wizard li::after {
    z-index: 1;
    -webkit-transform: translateX(4px);
    -moz-transform: translateX(4px);
    -ms-transform: translateX(4px);
    -o-transform: translateX(4px);
    transform: translateX(4px);
    border-left-color: #fff;
    margin: 0
  }
  .nav-tabs.wizard li>*::after {
    z-index: 2;
    border-left-color: inherit
  }
  .nav-tabs.wizard > li:nth-of-type(1) > a {
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
  }
  .nav-tabs.wizard li:last-child a {
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .nav-tabs.wizard li:last-child {
    margin-right: 0;
  }
  .nav-tabs.wizard li:last-child a:after,
  .nav-tabs.wizard li:last-child:after {
    content: "";
    border: none;
  }
  span.nmbr {
    display: block;
  }
}

</style>

<!-- <div class="tabbable">
  <ul class="nav nav-tabs wizard">
    <li class="active"><a href="#i9" data-toggle="tab" aria-expanded="false"><span class="nmbr">1</span>Encode</a></li>
    <li><a href="#w4" data-toggle="tab" aria-expanded="false"><span class="nmbr">2</span>Verify</a></li>
    <li><a href="#stateinfo" data-toggle="tab" aria-expanded="false"><span class="nmbr">3</span>Call Status</a></li>
    <li><a href="#companydoc" data-toggle="tab" aria-expanded="false"><span class="nmbr">4</span>Review</a></li>
  </ul>
 -->



@if(empty($v->callback_date))

      @if(!empty(Session::get('validate_kit_upload')) or !empty(Session::get('validate_kit_validated')) or !empty(Session::get('validate_number')))
      @if ( count(Session::get('validate_kit_upload')) > 0 )
      <div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content panel-danger">
            <div class="modal-header panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Duplicate Submission</h4>
            </div>
            <div class="modal-body">
              The Patient Kit Number <b>{{ !empty(old('patient_kit_number'))?old('patient_kit_number'): '' }}</b> is already on queue. <br/>
              <small style="color: red">Note: This is alert is on beta stage. Kindly use the filter below and double check. Alert us if there's a mistake. Thank you.</small>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      @elseif ( count(Session::get('validate_kit_validated')) > 0 )
      <div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content panel-danger">
            <div class="modal-header panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Px Already Enrolled</h4>
            </div>
            <div class="modal-body">
              The Patient Kit Number <b>{{ !empty(old('patient_kit_number'))?old('patient_kit_number'): '' }}</b> is already used. This patient kit number is allocated to patient 
              @foreach(Session::get('validate_kit_validated') as $validated)
              <b>{{$validated->patient_fullname}} </b>
              @endforeach.<br/>
              <small style="color: red">Note: This is alert is on beta stage. Kindly use the filter below and double check. Alert us if there's a mistake. Thank you.</small>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      @elseif ( count(Session::get('validate_number')) > 0 )
      <div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content panel-warning">
            <div class="modal-header panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Proceed?</h4>
            </div>
            <div class="modal-body">
              <p class="text-warning">The number you encoded is already on the database. You can check below if its the same patient.</p>
              <hr/>
              <div class="col-md-12">
              <div class="col-md-6">
              <b>Encoded Patient Details</b>
              <br/>
              <p class="text-info">
              <b>Patient Name:</b> {{ !empty(old('patient_fullname'))?old('patient_fullname'): '' }}<br/>
              <b>Mobile Number:</b> {{ !empty(old('mobile_number'))?old('mobile_number'): '' }}<br/>
              <b>Mobile Number 2:</b> {{ !empty(old('mobile_number_2'))?old('mobile_number_2'): '' }}<br/>
              <b>Landline:</b> {{ !empty(old('phone_number'))?old('phone_number'): '' }}<br/>
              </p>
              </div>
              <div class="col-md-6">
                <div class="panel panel-info">
                  <div class="panel-heading">Images</div>
                  <div class="panel-body">  
                    <img class="zoom" style="width: 10%; height: 10%;" src="{{$pic.'/'.$v->filename}}">
                    <a href="#" onclick="popupwindow('{{$pic.'/'.$v->filename}}');"><img class="" style="width: 10%; height: 10%;" src="{{$pic.'/'.$v->filename}}"></a>
                  </div>
                </div>
              </div>
              </div>
              <hr/>
              <div class="col-md-12">
                <table id="example3" class="display table-responsive table-hover table-condensed" width="100%">
                  <thead>
                    <tr>
                      <th>Kit No.</th>
                      <th>Fullname</th>
                      <th>NickName</th>
                      <th>Contact #</th>
                      <th>Birth Date</th>
                      <th>Gender</th>
                      <th>Address</th>                 
                    </tr>
                  </thead>
                   <tbody>
                      @foreach(Session::get('validate_number') as $validate_number)
                      <tr>
                        <td>{{$validate_number->patient_kit_number}}</td>
                        <td>{{$validate_number->patient_fullname}}</td>
                        <td>{{$validate_number->nickname}}</td>
                        <td>{{$validate_number->mobile_number}} @if(!empty($validate_number->mobile_number_2))/@endif {{$validate_number->mobile_number_2}} @if(!empty($validate_number->phone_number))/@endif {{$validate_number->phone_number}}</td>
                        <td>{{$validate_number->birth_date}}</td>
                        <td>{{$validate_number->gender}}</td>
                        <td>{{$validate_number->address}}</td>                        
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              <hr/>   
              <p class="text-danger">Note: This is alert is on beta stage. Kindly use the filter below and double check. Alert us if there's a mistake. Thank you.</p>
            </div>
            <div class="modal-footer">
          <form method="post" action="{{route('otsuka.encode_proceed')}}" enctype="multipart/form-data">
          {{ csrf_field() }} 
            <input type="hidden" name="id" value="{{ !empty(old('id'))?old('id'): '' }}">
            <input type="hidden" name="patient_kit_number" value="{{ !empty(old('patient_kit_number'))?old('patient_kit_number'): '' }}">
            <input type="hidden" name="patient_fullname" value="{{ !empty(old('patient_fullname'))?old('patient_fullname'): '' }}">
            <input type="hidden" name="mobile_number" value="{{ !empty(old('mobile_number'))?old('mobile_number'): '' }}">
            <input type="hidden" name="mobile_number_2" value="{{ !empty(old('mobile_number_2'))?old('mobile_number_2'): '' }}">
            <input type="hidden" name="phone_number" value="{{ !empty(old('phone_number'))?old('phone_number'): '' }}">
            <input type="hidden" name="time_to_call" value="{{ !empty(old('time_to_call'))?old('time_to_call'): '' }}">
            <input type="hidden" name="patient_consent" value="{{ !empty(old('patient_consent'))?old('patient_consent'): '' }}">
            <input type="hidden" name="enrollment_date" value="{{ !empty(old('enrollment_date'))?old('enrollment_date'): '' }}">
            <input type="hidden" name="doctor_name" value="{{ !empty(old('doctor_name'))?old('doctor_name'): '' }}">
            <input type="hidden" name="doctor_id" value="{{ !empty(old('doctor_id'))?old('doctor_id'): '' }}">
            <input type="hidden" name="doctor_consent" value="{{ !empty(old('doctor_consent'))?old('doctor_consent'): '' }}">
            <input type="hidden" name="hospital_name" value="{{ !empty(old('hospital_name'))?old('hospital_name'): '' }}">
              <button type="submit" name="proceed" value="true" class="btn btn-primary">Yes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
          </form>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      @elseif ( count(Session::get('validate_kit_upload')) > 0 and count(Session::get('validate_kit_validated')) > 0 )
      <div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content panel-danger">
            <div class="modal-header panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Duplicate Submission w/ Px Already Enrolled</h4>
            </div>
            <div class="modal-body">
              The Patient Kit Number <b>{{ !empty(old('patient_kit_number'))?old('patient_kit_number'): '' }}</b> is already used and also a duplicate submission. This patient kit number is allocated to patient 
              @foreach(Session::get('validate_kit_validated') as $validated)
              <b>{{$validated->patient_fullname}} </b>
              @endforeach
              and Duplicate submission uploaded at 
              @foreach(Session::get('validate_kit_upload') as $upload)
              <b>{{$upload->created_at}} </b>
              @endforeach<br/>
              <small style="color: red">Note: This is alert is on beta stage. Kindly use the filter below and double check. Alert us if there's a mistake. Thank you.</small>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      @endif
      @endif

      @if (\Session::has('success'))
      <div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content panel-success">
            <div class="modal-header panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Success!</h4>
            </div>
            <div class="modal-body">
              @foreach(Session::get('success') as $success)
              <b>✓ {{$success}}</b>
              @endforeach.<br/>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      @endif

      <div class="col-md-6">
        <div class="panel panel-default">
          <form method="post" action="{{route('otsuka.encode_edit')}}" enctype="multipart/form-data">
          {{ csrf_field() }} 
          <div class="panel-body">

            <div class="row">
              <div class="col-md-12">
                <div class="col-md-12 well">

                  <div class="col-md-6">
                    Uploaded by: <p style="color:green;"><b>{{$v->user->name}}<br/>{{$v->user->username}}</b></p><input type="hidden" name="user_level" value="{{$v->user->user_type}}"><input type="hidden" name="id" value="{{$v->id}}">
                  </div>

                  <div class="col-md-6">
                    <div class="pull-right">
                    Patient Kit #: <small style="color: red;">(10-digit, if invalid type 10 zeroes)</small><p style="color:green;"><input type="text" class="form-control input-sm" name="patient_kit_number" id="patient_kit_number" value="{{!empty($v->patient_kit_number)?$v->patient_kit_number: ''}}" pattern="[0-9]{10}" required="" placeholder="2018000000"/></p>
                    </div>
                  </div>

                </div>
              </div>
            </div>

            Product: <p style="color:green;"><b>{{$v->medicine}}</b></p>
            Patient Name: <span style="font-size:10px;color:#b10a0a">(Required)</span> <input type="text" class="form-control input-sm autosearch_patient" name="patient_fullname" id="patient_fullname" required="" value="{{!empty($v->patient_name)?$v->patient_name: ''}}">
            Mobile Number: <span style="font-size:10px;color:#b10a0a">(Required)</span>  <input type="tel" class="form-control input-sm autosearch_patient" name="mobile_number" id="mobile_number" required="" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="{{!empty($v->mobile_number)?$v->mobile_number: ''}}">
            Mobile Number 2: <input type="tel" class="form-control input-sm autosearch_patient" name="mobile_number_2"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="mobile_number_2" value="{{!empty($v->mobile_number_2)?$v->mobile_number_2: ''}}">
            Landline: <input type="tel" class="form-control input-sm" name="phone_number"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="phone_number" value="{{!empty($v->phone_no)?$v->phone_no: ''}}">
            When is Your preferred time to receive the call verification?
            <textarea name="time_to_call" class="form-control input-sm" id="time_to_call">{{!empty($v->time_to_call)?$v->time_to_call: ''}}</textarea>
            <div class="checkbox">
                    <label style="display:none"><input type="hidden" name="patient_consent" value="true" id="patient_consent">With Patient consent</label>
                  </div>
            Enrollment Date: 
              <input onkeydown="return false" type="text" class="form-control input-sm" name="enrollment_date" id="enrollment_date" value="{{date('Y-m-d', strtotime(!empty($v->enrollment_date)))?date('Y-m-d', strtotime($v->enrollment_date)): ''}}">
            Doctors Complete Name: <input type="text" class="form-control input-sm" name="doctor_name" id="doctor_name" value="{{!empty($v->doctor_name)?$v->doctor_name: ''}}">
            <input type="hidden"" name="doctor_id" id="doctor_id" value="{{ !empty(old('doctor_id'))?old('doctor_id'): '' }}">
            <div class="checkbox">
              <label><input type="checkbox" name="doctor_consent" value="true" id="doctor_consent" @if(!empty($v->doctor_consent)) checked @endif>Doctor's Consent</label>
            </div>
            Hospital/Clinic's Name: <input type="text" class="form-control input-sm" name="hospital_name" id="hospital_name" value="{{!empty($v->hospital_name)?$v->hospital_name: ''}}">
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">Images</div>
          <div class="panel-body">  
        <img class="zoom" style="width: 10%; height: 10%;" src="{{$pic.'/'.$v->filename}}">
         <a href="#" onclick="popupwindow('{{$pic.'/'.$v->filename}}');"><img class="" style="width: 10%; height: 10%;" src="{{$pic.'/'.$v->filename}}"></a>
          </div>
        </div>
      </div>

      <div class="row">
        <button type="submit" value="{{$v->id}}" style="margin-right: 15px;" name="done" class="btn btn-primary pull-right">Save</button>

    <div class="pull-right" id="invalid_select_enroll"  style="display:none; margin-right: 10px;">  
    
    <select class="form-control" name="remarks">
      <option disabled="" selected="">* Please Select Reason</option>
      <option value="Px Already Enrolled">Px Already Enrolled</option>
      <option value="Duplicate Submission">Duplicate Submission</option>
      <option value="Wrong Upload">Wrong Upload</option>
      <option value="Blurred Photo">Blurred Photo</option>
      <option value="No Px Consent">No Px Consent</option>
    </select>

    </div>

    <div class="pull-right" style="margin-right: 10px;">  
        <div class="[ form-group ]">
            <input type="checkbox" name="invalid" id="fancy-checkbox-danger" class="invalid_enroll" value="true" @if($v->status == 'Reject') checked @endif/>
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-danger" class="[ btn btn-default active ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="fancy-checkbox-danger" class="[ btn btn-danger ]">
                    INVALID
                </label>
            </div>
        </div>
    <!-- <input type="checkbox" id="invalid_enroll" name="invalid" value="true">INVALID -->
    </div>

<!--     <div class="pull-right" style="width:100px">  
    <input type="checkbox" id="invalid_enroll" name="invalid" value="true"  @if($v->status == 'Reject') checked @endif >INVALID
    </div> -->

      </form>
    </div>
@endif



<div class="modal fade" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
        </button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
           <div class="panel panel-default">
            <div class="panel-heading">
              <label for="searchbox" >Search:  </label>
              <input type="text" id="searchbox" class="form-control input-sm" style="display:inline;width:20%"/>
              <input type="hidden" id="filtered" class="form-control input-sm" style="display:inline;width:20%" value="{{!empty($v->fetch_allocation->emp_code)?$v->fetch_allocation->emp_code: ''}}" />
              <input type="hidden" value="{{$v->medicine}}" id="product">
              <select class="form-control input-sm" style="display:inline;width:20%" id="person"><option value="filteredmd">MR Doctor</option><option value="globalmd">Global</option></select>
              <button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc($('#searchbox').val(),$('#person').val())"></button>
            </div>     
            <div class="panel-body" id="filterdisplay" style="overflow: scroll;text-align:center">
            </div>
           </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div id="updatepatient" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" id="modalupdate">
  
  </div>
 </div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <form action="{{route('otsuka.patient.enroll')}}" id="enroll-form">
{{ csrf_field() }}
  <!-- <input type="text" id="enroll_refid" name="enroll_refid" > -->
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header"><input type="hidden" name="refid" id="refid"/><input type="hidden" name="action" id="action_field"/>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2>Patient Kit Number : <input type="text" id="patient_kit_reserve" name="enroll_patient_kit_number" list="browsers"/>
  
    <label style="float:right;font-size:15px"><input type="checkbox" name="enroll_patient_consent" id="enroll_patient_consent"> Patient Consent</label></h2>     

      </div>
      <div class="modal-body">
       
      <div class="row"><div class="col-md-12"><h3>Basic Information</h3></div></div>
      <div class="row">
        
        <div class="col-md-4">
          <div class="form-group">
            <label for="enroll_firstname">First:</label>
            <input type="text" class="form-control input-sm" id="enroll_firstname"  name="enroll_firstname">
          </div>
        </div>  
        <div class="col-md-4">
          <div class="form-group">
            <label for="enroll_middlename">Middle:</label>
            <input type="text" class="form-control input-sm" id="enroll_middlename"  name="enroll_middlename">
          </div>
        </div>  
        <div class="col-md-4">
          <div class="form-group">
            <label for="enroll_lastname">Lastname:</label>
            <input type="text" class="form-control input-sm" id="enroll_lastname"  name="enroll_lastname">
          </div>
        </div>  
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="email">Mobile No:</label>
            <input type="text" class="form-control input-sm" id="enroll_mobile_no"  name="enroll_mobile_no">
          </div>
        </div>  
        <div class="col-md-4">
          <div class="form-group">
            <label for="email">Mobile No 2:</label>
            <input type="text" class="form-control input-sm" id="enroll_mobile_no_2"  name="enroll_mobile_no_2">
          </div>
        </div>  
        <div class="col-md-4">
          <div class="form-group">
            <label for="email">Landline:</label>
            <input type="text" class="form-control input-sm" id="enroll_phone_no"  name="enroll_phone_no">
          </div>
        </div>  
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="email">Birth Date:</label>
            <input type="text" class="form-control input-sm datepicker"  id="enroll_birthdate"  name="enroll_birthdate">
          </div>
        </div>  
        <div class="col-md-3">
          <div class="form-group">
            <label for="email">Age:</label>
            <input type="text" class="form-control input-sm" id="enroll_age"  name="enroll_age">
          </div>
        </div>  
        <div class="col-md-3">
          <div class="form-group">
            <label for="email">NickName:</label>
            <input type="text" class="form-control input-sm" id="enroll_nickname"  name="enroll_nickname">
          </div>
        </div>  
        
        <div class="col-md-3">
          <div class="form-group">      
            <label for="email">Gender:</label>
            <select name="enroll_gender">
            <option value="Px Won't Disclose">Px Won't Disclose</option><option value="Male">Male</option><option value="Female" >Female</option>
            </select>
           <!-- <input type="hidden" id="enroll_gender_text" name="enroll_gender_text"/>
            <br/><input type="radio"   name="enroll_gender">Male
            <br/><input type="radio"   name="enroll_gender">Female
            <br/><input type="radio"   name="enroll_gender">Px Won't Disclose -->
          </div>
        </div>  
        
      </div>
        
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="email">Doctor's Name:</label>
            <input type="text" class="form-control input-sm" id="enroll_doctors_name"  name="enroll_doctors_name">
           
          </div>
        </div>  
        <div class="col-md-4" style="margin-top: 30px;">
          <div class="form-group">
           <label style="float:left;font-size:15px"><input type="checkbox" name="enroll_doctor_consent" id="enroll_doctor_consent"> Doctor's Consent</label>
          </div>
        </div>  
        <div class="col-md-4">
          <div class="form-group">
            <label for="email">Hospital:</label>
            <input type="text" class="form-control input-sm"  name="email">
          </div>
        </div>  
      </div>    
      <div class="row">
        <div class="col-md-8">
          <div class="form-group">
            <label for="email">Address</label>
            <textarea name="enroll_address" class="form-control input-sm"></textarea>
            <!--<input type="text" class="form-control input-sm" id="enroll_doctors_name"  name="email">-->
           
          </div>
        </div>  
        
        <div class="col-md-4">
          <div class="form-group">
            <label for="email">Landline:</label>
            <input type="text" class="form-control input-sm"  name="enroll_hospital" id="enroll_hospital">
          </div>
        </div>  
      </div>    

      <hr/>
      <div class="row"><div class="col-md-12"><h3>Medical Information</h3></div></div>
        
        <div class="row">
            <div class="col-md-6">
            <label for="Product">Product </label>
              <select class="form-control input-sm" id="enroll_product" name="enroll_product">
              
              <option value="Abilify">Abilify</option>
              <option value="Aminoleban">Aminoleban</option>
              <option value="Pletaal">Pletaal</option>
              </select>
            </div>
          <div class="col-md-6">
            <label for="SKU">SKU</label><select class="form-control input-sm" id="enroll_sku" name="sku">
              <option>* Please Select SKU</option>
             
              @foreach($Mgabilify as $mab)
              <option value="{{$mab->id}}">{{$mab->skuname}}</option>
              @endforeach
             
            
            </select>
          </div>
        </div>

      <div class="row">
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Tabs Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="notabsprescribe" id="notabsprescribe" class="form-control input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Days Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="nodaysprescribe" id="nodaysprescribe" class="form-control input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label">Total No. of Tabs</label>
                  <div class="col-md-4">
                    <input type="number" name="totalnotabs" id="nodaysprescribe" class="form-control input-sm" max="9999">
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" id="tabsprescribe_disclose" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" id="tabsprescribe_no_data" value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" id="daysprescribe_disclose" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" id="daysprescribe_no_data" value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="totaltabs" id="totaltabs_disclose" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="totaltabs" id="totaltabs_nodata" value="No Available Data">No Available Data</label>
                </div>
              </div>
            </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
                <label>Already Purchased:</label>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" id="purchased_y" value="Yes">Yes</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" id="purchased_n" value="No">No</label>
                </div>
              </div>
            </div>

    
      </div>
      <div class="modal-footer"><div id="demo"></div>

    <button type="button" style="float:left; " id="btn_enroll_save" class="btn btn-info btn-default"  data-dismiss="modal">Save & Exit</button>
    <!--<button type="button" style="float:left; " id="btn_enroll_save" class="btn btn-primary btn-default">Save </button> -->
    
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
</form>
  </div>
</div>


<div class="col-md-12">

<div class="panel panel-default">

  <div class="panel-heading">Verification</div>
    <div class="panel-body"> 
      <div class="row">
        <div class="col-md-12">
        <div class="row">
          <div class="col-md-12">
            <form method="post" id="encode_form" action="{{route('otsuka.verify.unclaim')}}" enctype="multipart/form-data">
      {{ csrf_field() }} 
          <input type="hidden" name="v_count" value="{{!empty($v->verification_attempt)?$v->verification_attempt: '0'}}">
          <label for="Retrieval"><button value="{{!empty($v->id)?$v->id: ''}}" name="unclaim" class="btn btn-primary">Un-claim</button>
          </form>&nbsp;Retrieval Basic Information:</label>
          <hr/>
          </div>
        </div>
        <div class="col-md-3">
          <div class="col-md-12 well">
          <label>Name: </label><p style="color: green;"><b>{{!empty($v->patient_name)?$v->patient_name: 'N/A'}}</b></p>
        </div>
        </div>
        <div class="col-md-3">
          <div class="col-md-12 well">
          <label>Mobile No.: </label><p style="color: green;"><b>{{!empty($v->mobile_number)?$v->mobile_number: 'N/A'}}</b></p>
        </div>
        </div>
        <div class="col-md-3">
          <div class="col-md-12 well">
          <label>Patient Kit Number </label><p style="color: green;"><b>{{!empty($v->patient_kit_number)?$v->patient_kit_number: ''}}</b></p>
        </div>
        </div>
        <div class="col-md-3">
          <div class="col-md-12 well">
          <label>Landline No.2: </label><p style="color: green;"><b>{{!empty($v->phone_no)?$v->phone_no: 'N/A'}}</b></p>
        </div>
        </div> 
      </div>
      </div>
		
	

		<div class="col-md-12">
			<div class="col-md-12">
			<form id="upload_form"  method="get" > 
			<input type="hidden" name="ref_upload_id" value="{{$v->id}}"/>
			<h3>Patient Information</h3>
			</div>
		
			<div class="col-md-2">
			
				<div class="form-group">
				  <label for="usr">First name:</label>
				  <input type="text" class="form-control input-sm upload_input" name="upload_first_name" value="{{$v->firstname}}">
				</div>
				
			</div>
			
			<div class="col-md-2">
			
				<div class="form-group">
				  <label for="usr">Last Name:</label>
				  <input type="text" class="form-control input-sm upload_input" name="upload_last_name" value="{{$v->lastname}}">
				</div>
				
			</div>
			
			<div class="col-md-2">
			
				<div class="form-group">
				  <label for="usr">Middle Name:</label>
				  <input type="text" class="form-control input-sm upload_input" name="upload_middle_name" value="{{$v->middlename}}">
				</div>
				
			</div>
			<div class="col-md-2">
			
				<div class="form-group">
				  <label for="usr">NickName:</label>
				  <input type="text" class="form-control input-sm upload_input" name="upload_nickname" value="{{$v->nickname}}">
				</div>
				
			</div>
			<div class="col-md-4">
			
				<div class="col-md-5">
				<div class="form-group">
				  <label for="usr">Gender:</label>
				  <select class="form-control input-sm upload_input" name="upload_gender">
						<option selected disabled>Please Select </option>
						<option value="Male" <?php if($v->gender == 'Male') echo "Selected";?> >Male</option>
						<option value="Female"  <?php if($v->gender == 'Female') echo "Selected";?>>Female</option>
				  </select>
				</div>
				</div>
				
				<div class="col-md-4">
					<div class="form-group">
					  <label for="usr">Birthdate:</label>
					  <input type="text" class="form-control input-sm datepicker upload_input" name="upload_birthdate" value="{{$v->birthdate}}">
					</div>		
				</div>
				<div class="col-md-3">
					<div class="form-group">
					  <label for="usr">Age:</label>
					  <input type="number" class="form-control input-sm upload_input " style="padding: 0px 3px; " name="upload_age" value="{{$v->age}}">
					</div>		
				</div>
				
			</div>
			
						
			
		</div>
		<div class="col-md-12">
			<div class="col-md-4">
				<div class="form-group">
				  <label for="usr">Address:</label>
				  <textarea class="form-control input-sm upload_input" name="upload_address" >{{$v->address}}</textarea>
				 
				</div>				
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
				  <label for="usr">Mobile No. 2:</label>
				   <input type="text" class="form-control input-sm upload_input " name="upload_mobile_number_2" value="{{$v->mobile_number_2}}">
				 
				</div>				
			</div>
			<div class="col-md-3">
				<div class="form-group">
				  <label for="usr">Preferred Time to Call:</label>
				   <textarea class="form-control input-sm upload_input" name="upload_time_to_call" >{{$v->time_to_call}}</textarea>
				 
				</div>				
			</div>
			<div class="col-md-3">
				<div class="form-group">
				  <label for="usr">Remarks:</label>
				   <textarea class="form-control input-sm upload_input" name="upload_verification_remarks" >{{$v->verification_remarks}}</textarea>
				 
				</div>				
			</div>
			
		</div>
		
		</form>
	
      <hr/>

      <div class="row">
        <div class="col-md-12">
        <form method="post" id="verify_call" action="{{route('otsuka.save')}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        <input type="hidden" name="mobile_number" value="{{!empty($v->mobile_number)?$v->mobile_number: ''}}">
          <table class="table">
            <thead>
              <tr>
                <th>Call Attempt</th>
                <th>Call Date</th>
                <th>Status</th>
                <th>Remarks</th>
                <th>Call Notes</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>
      @if(!empty($v->fetch_call->fetch_verification))
              @foreach($v->fetch_call->fetch_verification as $v_call)
              @if($v_call->allowcallback == 1 or $v_call->allowcallback == 2 or $v_call->allowcallback == -1)
              <tr class="statuscheck">
                <td>#{{!empty($v_call->attempt)?$v_call->attempt: ''}}<input type="hidden" name="v_attempt" value="{{!empty($v_call->attempt)?$v_call->attempt: ''}}"><input type="hidden" name="cmid" value="{{!empty($v_call->cmid)?$v_call->cmid: ''}}"><input type="hidden" name="upload_id" value="{{!empty($v->id)?$v->id: ''}}"></td>
                <td>{{!empty($v_call->actualdate)?$v_call->actualdate: '(Auto Generated)'}}</td>
                <td><select class="form-control input-sm" name="remarks_call" required="">
                      <option value="{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: ''}}">{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: 'Please Select Status'}}</option>
                      @foreach($status as $value)
                      <option value="{{$value->id}}">{{$value->name}}</option>
                      @endforeach
                    </select>
                </td>
                <td><input type="text" class="form-control input-sm" placeholder="Remarks" name="remarks_others" value="{{!empty($v_call->remarks_others)?$v_call->remarks_others: ''}}"></td>
                <td><textarea class="form-control input-sm" rows="5" name="callnotes" id="callnotes" placeholder="Callnotes">{{!empty($v_call->callnotes)?$v_call->callnotes: ''}}</textarea><input type="hidden" name="v_id" value="{{!empty($v_call->id)?$v_call->id: ''}}"></td>
                <td><button type="submit" class="btn btn-success" id="test" value="{{!empty($v_call->id)?$v_call->id: ''}}">Update</button></td>
              </tr>
              @elseif(empty($v_call->allowcallback))
              <tr>
                <td>#{{!empty($v_call->attempt)?$v_call->attempt: ''}}<input type="hidden" name="v_attempt" value="{{!empty($v_call->attempt)?$v_call->attempt: ''}}"><input type="hidden" name="cmid" value="{{!empty($v_call->cmid)?$v_call->cmid: ''}}"><input type="hidden" name="upload_id" value="{{!empty($v->id)?$v->id: ''}}"></td>
                <td>{{!empty($v_call->actualdate)?$v_call->actualdate: '(Auto Generated)'}}</td>
                <td><select class="form-control input-sm" name="remarks_call" required="">
                      <option value="{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: ''}}">{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: 'Please Select Status'}}</option>
                      @foreach($status as $value)
                      <option value="{{$value->id}}">{{$value->name}}</option>
                      @endforeach
                    </select>
                </td>
                <td><input type="text" class="form-control input-sm" placeholder="Remarks" name="remarks_others" value="{{!empty($v_call->remarks_others)?$v_call->remarks_others: ''}}"></td>
                <td><textarea class="form-control input-sm" rows="5" name="callnotes" id="callnotes" placeholder="Callnotes">{{!empty($v_call->callnotes)?$v_call->callnotes: ''}}</textarea><input type="hidden" name="v_id" value="{{!empty($v_call->id)?$v_call->id: ''}}"></td>
                <td><button type="submit" class="btn btn-success" id="test2" value="{{!empty($v_call->id)?$v_call->id: ''}}">Update</button></td>
              </tr>
              @endif
              @endforeach
      @endif
            </tbody>
          </table>
          </form>
      <hr/>
              <div class="row">
      <div class="col-md-12">
    <div class="checkbox">
      <label><input type="checkbox" data-toggle="collapse" data-target="#show">Show Detailed</label>
    </div>
  </div>
  </div>
        </div>
      </div>
    </div>

  </div>

<div class="collapse" id="show">
<div class="panel panel-default">
  <div class="panel-heading">Patient Details</div>
        <form method="post" id="verify_encode" action="{{route('otsuka.save-details')}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
    <div class="panel-body">

      <div class="row">
        <!-- <input type="hidden" name="cmid" value="{{!empty($v->cmid)?$v->cmid: '0'}}"> -->
<!--         <input type="hidden" name="date" value="{{!empty($v->date)?$v->date: ''}}"> -->
        <input type="hidden" name="patient_consent" value="{{!empty($v->patient_consent)?$v->patient_consent: ''}}">
        <input type="hidden" name="doctor_consent" value="{{!empty($v->patient_consent)?$v->doctor_consent: ''}}">
        <input type="hidden" name="medrep_id" value="{{!empty($v->fetch_allocation->emp_code)?$v->fetch_allocation->emp_code: ''}}">
        @foreach($v->fetch_call->fetch_verification as $v_call)
        <input type="hidden" name="v_id" value="{{!empty($v_call->id)?$v_call->id:''}}">
        @endforeach
        <div class="col-md-3">
          <div class="well">
              <label for="Patient Code">Patient Code:</label><p style="color:green;"><b>{{!empty($v->patient_code)?$v->patient_code: 'Not yet Verified'}}</b></p>
              <input type="hidden" name="patient_code" value="{{!empty($v->patient_code)?$v->patient_code: ''}}">
            </div>
          </div>

        <div class="col-md-3">
          <div class="well">
            <label for="Patient Kit #">Patient Kit #:</label><p style="color:green;"><b>{{!empty($v->patient_kit_number)?$v->patient_kit_number: 'Not Given'}}</b></p><input type="hidden" name="patient_kit_number" value="{{!empty($v->patient_kit_number)?$v->patient_kit_number: ''}}">
          </div>
        </div>

        <div class="col-md-6">
          <div class="well">
            <label for="Patient Kit #">Images:</label>
            <p>
            <a href="#" onclick="popupwindow('{{$pic . '/' . !empty($v->filename)?$pic . '/' .$v->filename: ''}}');"><img class="img-thumbnail" style="width: 10%; height: 10%;" src="{{$pic . '/' . !empty($v->filename)?$pic . '/' .$v->filename: ''}}"></a><input type="hidden" name="filename" value="{{!empty($v->filename)?$v->filename: ''}}">
            </p>
          </div>
        </div>

        </div>

    <div class="row">

      <div class="col-md-12">
        <div class="form-group">
          <label for="details">Patient Name:</label>
          <input type="text" class="form-control input-sm" placeholder="" name="patient_fullname" id="name" value="{{!empty($v->patient_name)?$v->patient_name: 'N/A'}}" readonly="">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Last Name</label>
          <input type="text" class="form-control input-sm" name="patient_lastname" placeholder="" id="first" value="{{$v->firstname}}">
        </div>
      </div>
      <!--  col-md-6   -->

      <div class="col-md-4">
        <div class="form-group">
          <label for="last">First Name</label>
          <input type="text" class="form-control input-sm" name="patient_firstname" placeholder="" id="last" value="{{$v->lastname}}">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="last">Middle Name</label>
          <input type="text" class="form-control input-sm" name="patient_middlename" placeholder="" id="middle" value="{{$v->middle}}">
        </div>
      </div>
      <!--  col-md-6   -->
    </div>


    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="birthdate">Birthdate</label>
          <input type="text" class="form-control input-sm datepicker" placeholder="" name="birth_date" id="birthdate" value="{{$v->birthdate}}">
        </div>
      </div>
      <!--  col-md-6   -->

      <div class="col-md-4">
        <div class="form-group">
          <label for="gender">Gender</label>
          <select class="form-control input-sm" name="gender" id="ref_gender">
            <option value="">* Please Select Gender</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="gender">Age</label>
          <input type="number" class="form-control input-sm" placeholder="Age" name="age" id="age"value="">
        </div>
      </div>
      <!--  col-md-6   -->
    <!--  row   -->

      <div class="col-md-12">
        <div class="form-group">
          <label for="address">Address</label>
          <input type="text" class="form-control input-sm" id="address" name="address" placeholder="Address" value="">
        </div>
      </div>
    </div>
      <!--  col-md-6   -->
    <hr>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="mobile1">Mobile No. 1</label>
          <input type="number" class="form-control input-sm" name="mobile_number" placeholder="Mobile 1" value="{{!empty($v->mobile_number)?$v->mobile_number: ''}}">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="mobile">Mobile No. 2</label>
          <input type="number" class="form-control input-sm" name="mobile_number_2" placeholder="Mobile 2" value="{{!empty($v->mobile_number_2)?$v->mobile_number_2: ''}}">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="landline">Landline</label>
          <input type="number" class="form-control input-sm" placeholder="Landline" name="phone_number" value="{{!empty($v->phone_no)?$v->phone_no: ''}}">
        </div>
      </div>
      <!--  col-md-6   -->
    </div>
    <!--  row   -->
    <hr>
    <div class="row">
      <div class="col-md-6">
      <div class="form-group">
          <label for="mobile1">Preferred Time to Call</label>
          <input type="text" class="form-control input-sm" name="time_to_call" id="ref_time_to_call" placeholder="Preffered Time to Call" name="time_to_call" value="{{!empty($v->time_to_call)?$v->time_to_call: ''}}">
        </div>
      </div>

      <div class="col-md-6">
      <div class="form-group">
          <label for="mobile1">Remarks</label>
          <textarea class="form-control input-sm" rows="5" name="remarks" id="remarks" placeholder="Remarks"></textarea>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">Patient Medical Info</div>
    <div class="panel-body">

      <div class="row">
          <div class="col-md-6">
            <label for="Medical Representative">Medical Representative </label><input type="text" class="form-control input-sm" name="p_name" value="{{!empty($v->fetch_allocation->mr_name)?$v->fetch_allocation->mr_name: ''}}" readonly="">
          </div>
          <div class="col-md-5">
            <label for="Doctor">Doctor </label><label style="margin-left: 20px;"><input type="checkbox" name="doctor_wont_disclose" id="doctor_wont_disclose" value="true"> Px Won't Disclose</label><input type="text" id="md_name" class="form-control input-sm" name="doctor" placeholder="Doctor" value="{{!empty($v->doctor_name)?$v->doctor_name: ''}}">
            <input type="hidden" name="d_id" id="d_id" value="{{!empty($v->doctor_id)?$v->doctor_id: ''}}">
          </div>
          <div class="col-md-1">
            <label for="Search">&nbsp; </label><a class="btn btn-primary btn-sm" id="btnLaunch" role="button">Search</a>
          </div>
      </div>
            
      <div class="row">
        <div class="col-md-12">
          <label for="Hospital">Hospital </label><input type="text" class="form-control input-sm" id="hospital" name="hospital" placeholder="Hospital" value="{{!empty($v->hospital_name)?$v->hospital_name: ''}}">
        </div>
      </div>

      <div class="row">
          <div class="col-md-6">
            <label for="Product">Product </label>
              <select class="form-control input-sm" id="product">
                <option value="{{!empty($v->medicine)?$v->medicine: ''}}">{{!empty($v->medicine)?$v->medicine: '*Select Product'}}</option>
                <option value="Abilify">Abilify</option>
                <option value="Aminoleban">Aminoleban</option>
                <option value="Pletaal">Pletaal</option>
              </select>
          </div>
        <div class="col-md-6">
          <label for="SKU">SKU</label><select class="form-control input-sm" id="product" name="product">
              <option value="">* Please Select SKU</option>
              @if($v->medicine == 'Abilify')
              @foreach($Mgabilify as $mab)
              <option value="{{$mab->id}}">{{$mab->skuname}}</option>
              @endforeach
              @elseif($v->medicine == 'Aminoleban')
              @foreach($Mgaminoleban as $mam)
              <option value="{{$mam->id}}">{{$mam->skuname}}</option>
              @endforeach
              @elseif($v->medicine == 'Pletaal')
              @foreach($Mgpletal as $mp)
              <option value="{{$mp->id}}">{{$mp->skuname}}</option>
              @endforeach
              @endif
            </select>
      </div>
    </div>
<hr>
            <div class="row">
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Tabs Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="notabsprescribe" class="form-control input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Days Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="nodaysprescribe" class="form-control input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label">Total No. of Tabs</label>
                  <div class="col-md-4">
                    <input type="number" name="totalnotabs" class="form-control input-sm" max="9999">
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="totaltabs" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="totaltabs" value="No Available Data">No Available Data</label>
                </div>
              </div>
            </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
                <label>Already Purchased:</label>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" value="Yes">Yes</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" value="No">No</label>
                </div>
              </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
               <!-- <label><input type="checkbox" name="dnc" value="yes">DNC</label> -->
			   <a  class="btn btn-info pull-right" name="update_record" style="display:none" onclick="alert('test')" value="{{$v->id}}">Update</a>
			   
               <button type="submit" class="btn btn-success pull-right" name="encode" value="{{$v->id}}">Save</button>
              </div>
            </div>

        </div>

</div>
</form>
</div>
</div>
</div>



@endsection 

@section('footer-scripts')
<script>
    $(document).ready(function(){

      $(window).on('load',function(){
        $('#error').modal('show');
    });

      $('#enrollment_date').datepicker();

    $(".invalid_enroll").change(function(){
    if ($(this).is(':checked')) {
    $("#invalid_select_enroll").fadeToggle();
    }
    else
    {   
    $("#invalid_select_enroll").fadeToggle();    
    }
      
    });
  });

/*$('#test').click(function () {
    var ids = $.map(table.rows('.selected').data(), function (item) {
        return item[0]
    });
    console.log(ids)
    alert(table.rows('.selected').data().length + ' row(s) selected');
});*/

$("tr.statuscheck input, tr.statuscheck select, tr.statuscheck textarea, tr.statuscheck button").prop('disabled', true);

$('#verify_call').bind('submit', function (e) {
    // Disable the submit button while evaluating if the form should be submitted
    $('#test2').prop('disabled', true);

    var valid = true;    
    var error = '';

    // If the email field is empty validation will fail
    if ($('[name=remarks_call]').val() === '') {
        valid = false;
        error = 'Remarks is required';
    }

    if (!valid) { 
        // Prevent form from submitting if validation failed
        e.preventDefault();

        // Reactivate the button if the form was not submitted
        $('#test2').prop('disabled', false);
        
        alert(error);
    }
});


$(document).ready(function() {


$(".datatable").DataTable({
  "ordering": false,
});

});

$(document).ready(function() {



$('#verify').DataTable({
  "ordering": false,
});

});

$(function() {
  $('#btnLaunch').click(function() {
    $('#myModal').modal('show');
  });

  $('#btnSave').click(function() {
    var value = $('input').val();
    $('#doctor_name').val(value);;
    $('#myModal').modal('hide');
  });
});

$('#product').on('change', function() {
  this.value
})

function popupwindow(url) {
  
  title = "Images";
  w = 500;
  h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

function loadDoc(filter,person) {
  var empcode = $('#filtered').val();
  var product = $('#product').val();
  $('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {

      document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
    
    $('#example2').DataTable(); 
        
    
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchperson?person_filter=" + filter + "&person=" + person + "&emp_code=" + empcode + "product=" + product, true);
  xhttp.send();
}

function loadDoc2(filter,person) {
  $('#filterdisplay2').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif" style="width:100px;"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    
    
        document.getElementById("filterdisplay2").innerHTML = xhttp.responseText;

    $('#patient_search2').DataTable();
  
      
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchPatient_only?person_filter=" + filter + "&person=" + person, true);
  xhttp.send();
}


function updatepatient(refid) { 
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    
    
        document.getElementById("modalupdate").innerHTML = xhttp.responseText;
     
    $( "#birthdate_edit" ).datepicker();
 
    //$('#patient_search').DataTable();
  
      
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/displaypatient?refid=" + refid, true);
  xhttp.send();
}

function update_upload_record() { 
  var upload_list = $("#upload_form").serialize();
  console.log(upload_list);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    
		console.log( xhttp.responseText);
        //document.getElementById("modalupdate").innerHTML = xhttp.responseText;
     
    $( "#birthdate_edit" ).datepicker();
 
    //$('#patient_search').DataTable();
  
      
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/update_upload?mode=update" + "&"+ upload_list , true);
  xhttp.send();
}


function GetUploadRecord(id) {
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		//document.getElementById(xhttp.responseText['id']);
		
        //alert(xhttp.responseText['id']);
		var obj = JSON.parse(xhttp.responseText);
		//document.getElementById("md_name").value()
		$("#last").val(obj.lastname);
		$("#first").val(obj.firstname);
		$("#middle").val(obj.middlename);
		$("#birthdate").val(obj.birthdate);
		$("#age").val(obj.age);
		$("#address").val(obj.address);
		$("#Landline").val(obj.phone_no);
		$("#Remarks").val(obj.verification_remarks);
		$("#ref_time_to_call").val(obj.time_to_call);
		$("#ref_gender").val(obj.gender);
	
        //alert(obj.mdname);
		
    }
  };


  //xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + ""+ "&sku_id=" + skuid +  "&qtytabs=" + $("#redeemable_tabs").val() + "&patient_id=" + $('#patient_id').val(), true);
  xhttp.open("GET", "<?php echo $baseurl;?>otsuka/api/upload?upload_id=" + id , true);
  xhttp.send(); 
}

function useMD(id) {
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    //document.getElementById(xhttp.responseText['id']);
    
        //alert(xhttp.responseText['id']);
    var obj = JSON.parse(xhttp.responseText);
    //document.getElementById("md_name").value()
    $("#md_name").val(obj.mdname);
    $("#hospital").val(obj.hospital);
    $("#d_id").val(obj.doctorid);
    
    }
  };

  //xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + ""+ "&sku_id=" + skuid +  "&qtytabs=" + $("#redeemable_tabs").val() + "&patient_id=" + $('#patient_id').val(), true);
  xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/get_national_md_record?national_md_id=" + id , true);
  xhttp.send(); 
}



</script>
@endsection

