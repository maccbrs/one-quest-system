@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
	text-align: center;
}

</style>

<div class="panel panel-default">
  <div class="panel-heading">Generate</div>
  <div class="panel-body">

      <div class="col-md-12">
            <form method="get" action="{{route('otsuka.generate-daily-individual')}}" class="form-horizontal p-10">
                {{ csrf_field() }}            
                <div class="row">
                    <div class="col-md-4">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">From: 
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datetimepicker"  placeholder="from" name="from">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-4">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">To:
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datetimepicker"  placeholder="to" name="to">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group p-10">
                            <label class="col-md-3 control-label" for="example-select">Select:</label>
                            <div class="col-md-9">
                                <select name="medrep" class="form-control" size="1" required="">
                                    <option value="">* Please Select MedRep</option>
                                        <option value="all">All</option>
                                    @foreach($medreps as $mr)
                                        <option value="{{$mr->emp_code}}">{{trim($mr->pso_name,'"')}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-1">
                        <div class="form-group p-10 form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">Submit
                                </button>
                            </div>
                        </div>
                    </div>               
                </div>
            </form>
        </div>
    </div>
</div>

        <!-- Main content -->

<div class="panel panel-default">
  <div class="panel-heading">Generated Report</div>
  <div class="panel-body">

      <div class="col-md-12">
        <table class="display nowrap compact datatable">
          <thead>
            <tr>
              <th>Patient Kit #</th>
              <th>MD Name</th>
              <th>Remarks</th>
              <th>Date Uploaded</th>
              </td>
            </tr>
          </thead>
        <tbody>
          @foreach($data2 as $d)
          <tr>
            <td>@if(!empty($d->fetch_validated->patient_kit_number)){{!empty(strtoupper($d->fetch_validated->patient_kit_number))?strtoupper($d->fetch_validated->patient_kit_number): 'NO AVAILABLE DATA'}}@else{{!empty(strtoupper($d->patient_kit_number))?strtoupper($d->patient_kit_number): 'NO AVAILABLE DATA'}}@endif</td>
            <td>{{!empty(strtoupper($d->doctor_name))?strtoupper($d->doctor_name): 'NO AVAILABLE DATA'}}</td>
            @if($d->status == 'On Queue' and !empty($d->patient_consent))
            <td>FOR VERIFICATION</td>
            @elseif($d->status == 'On Queue' and empty($d->patient_consent))
            <td>NO PX CONSENT</td>
            @elseif($d->status == 'verified' and empty($d->patient_consent))
            <td>NO PX CONSENT</td>
            @elseif($d->status == 'verified' and !empty($d->patient_consent))
            <td>FOR VERIFICATION</td>
            @elseif($d->status == 'Reject')
            <td>{{!empty(strtoupper($d->remarks))?strtoupper($d->remarks): ''}}
            @endif
            <td>{{!empty(date('m-d-Y h:i:s', strtotime($d->created_at)))?date('m-d-Y h:i:s', strtotime($d->created_at)): 'NO AVAILABLE DATA'}}</td>
          </tr>
          @endforeach
        </tbody>
        </table>
        </div>

    </div>
</div>

@if($r->medrep !== 'all')
<div class="panel panel-default">
  <div class="panel-heading">Generated Report</div>
  <div class="panel-body">

      <div class="col-md-12">
        <div class="row">
          Your Daily No. of Valid Retrievals is {{$verified_count}}.
        </div>
        <div class="row">
          Your Daily No. of Invalid Retrievals is {{$notverified_count}}.
        </div>
        <div class="row">
          <br/>
          Your MTD No. of Retrievals is {{$mtd_count}}.
        </div>
        <div class="row">
          <br/>
          For inquiries, please fill-up a report form at the link provided below.
          <br/>
          <a href="http://oqs.onequest.com.ph:8080/index.php">http://oqs.onequest.com.ph:8080/index.php</a>
        </div>
        <div class="row">
          <br/>
          <br/>
          Thank you. Magellan Solutions Outsourcing, Inc.
        </div>
        </div>

    </div>
</div>
@endif


      
      





@endsection 

@section('footer-scripts')
<script>
function popupwindow(url) {
	
	title = "Images";
	w = 500;
	h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

$(document).ready(function() {
$(".datatable").DataTable({
  "ordering": false,
  dom: 'Blfrtip',
  buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: '{{$r->medrep}}({{date("m/d/Y", strtotime($r->from))}}-{{date("m/d/Y", strtotime($r->to))}})',
                exportOptions: {
                columns: ':visible'
                }
            },
            'colvis'
        ]
});

});

$(document).ready(function() {



$('#verify').DataTable({
  "ordering": false,
});

});
</script>
@endsection

