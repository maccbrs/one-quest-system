@extends('otsuka.master')

@section('Patient MasterFile')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
	$baseurl = URL::asset('/'); 
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
	text-align: center;
}

</style>




<div class="show" style="display: none !important;">
<div class="panel panel-default">
  <div class="panel-heading">Patient Details</div>
        <form method="post" id="verify_encode" action="{{route('otsuka.save-details')}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
    <div class="panel-body">

      <div class="row">
        <!-- <input type="hidden" name="cmid" value="{{!empty($v->cmid)?$v->cmid: '0'}}"> -->
<!--         <input type="hidden" name="date" value="{{!empty($v->date)?$v->date: ''}}"> -->
        <input type="hidden" name="patient_consent" value="{{!empty($v->patient_consent)?$v->patient_consent: ''}}">
        <input type="hidden" name="doctor_consent" value="{{!empty($v->patient_consent)?$v->doctor_consent: ''}}">
        <input type="hidden" name="medrep_id" value="{{!empty($v->fetch_allocation->emp_code)?$v->fetch_allocation->emp_code: ''}}">
        @foreach($v->fetch_call->fetch_verification as $v_call)
        <input type="hidden" name="v_id" value="{{!empty($v_call->id)?$v_call->id:''}}">
        @endforeach

        <div class="col-md-3">
          <div class="well">
            <label for="Patient Kit #">Patient Kit #:</label><p style="color:green;"><b>{{!empty($v->patient_kit_number)?$v->patient_kit_number: 'Not Given'}}</b></p><input type="hidden" name="patient_kit_number" value="{{!empty($v->patient_kit_number)?$v->patient_kit_number: ''}}">
          </div>
        </div>



        </div>

    <div class="row">



      <div class="col-md-4">
        <div class="form-group">
          <label for="first">Last Name</label>
          <input type="text" class="form-control input-sm" name="patient_lastname" placeholder="" id="first" value="">
        </div>
      </div>
      <!--  col-md-6   -->

      <div class="col-md-4">
        <div class="form-group">
          <label for="last">First Name</label>
          <input type="text" class="form-control input-sm" name="patient_firstname" placeholder="" id="last" value="">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="last">Middle Name</label>
          <input type="text" class="form-control input-sm" name="patient_middlename" placeholder="" id="last" value="">
        </div>
      </div>
      <!--  col-md-6   -->
    </div>


    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="birthdate">Birthdate</label>
          <input type="date" class="form-control input-sm" placeholder="" name="birth_date" id="birthdate" value="">
        </div>
      </div>
      <!--  col-md-6   -->

      <div class="col-md-4">
        <div class="form-group">
          <label for="gender">Gender</label>
          <select class="form-control input-sm" name="gender">
            <option value="">* Please Select Gender</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="gender">Age</label>
          <input type="number" class="form-control input-sm" placeholder="Age" name="age" value="">
        </div>
      </div>
      <!--  col-md-6   -->
    <!--  row   -->

      <div class="col-md-12">
        <div class="form-group">
          <label for="address">Address</label>
          <input type="text" class="form-control input-sm" id="address" name="address" placeholder="Address" value="">
        </div>
      </div>
    </div>
      <!--  col-md-6   -->
    <hr>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="mobile1">Mobile No. 1</label>
          <input type="number" class="form-control input-sm" name="mobile_number" placeholder="Mobile 1" value="{{!empty($v->mobile_number)?$v->mobile_number: ''}}">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="mobile">Mobile No. 2</label>
          <input type="number" class="form-control input-sm" name="mobile_number_2" placeholder="Mobile 2" value="{{!empty($v->mobile_number_2)?$v->mobile_number_2: ''}}">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="landline">Landline</label>
          <input type="number" class="form-control input-sm" placeholder="Landline" name="phone_number" value="{{!empty($v->phone_no)?$v->phone_no: ''}}">
        </div>
      </div>
      <!--  col-md-6   -->
    </div>
    <!--  row   -->
    <hr>
    <div class="row">
      <div class="col-md-6">
      <div class="form-group">
          <label for="mobile1">Preferred Time to Call</label>
          <input type="text" class="form-control input-sm" name="time_to_call" placeholder="Preffered Time to Call" name="time_to_call" value="{{!empty($v->time_to_call)?$v->time_to_call: ''}}" readonly="">
        </div>
      </div>

      <div class="col-md-6">
      <div class="form-group">
          <label for="mobile1">Remarks</label>
          <textarea class="form-control" rows="5" name="remarks" id="remarks" placeholder="Remarks"></textarea>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">Patient Medical Info</div>
    <div class="panel-body">

      <div class="row">
          <div class="col-md-6">
            <label for="Medical Representative">Medical Representative </label><input type="text" class="form-control input-sm" name="p_name" value="{{!empty($v->fetch_allocation->mr_name)?$v->fetch_allocation->mr_name: ''}}" readonly="">
          </div>
          <div class="col-md-5">
            <label for="Doctor">Doctor </label><input type="text" id="md_name" class="form-control input-sm" name="doctor" placeholder="Doctor" value="{{$v->doctor_name}}">
            <input type="hidden" name="doctor_id" id="doctor_id" value="">
          </div>
          <div class="col-md-1">
            <label for="Search">&nbsp; </label><a class="btn btn-primary btn-sm" id="btnLaunch" role="button">Search</a>
          </div>
      </div>
            
      <div class="row">
        <div class="col-md-12">
          <label for="Hospital">Hospital </label><input type="text" class="form-control input-sm" id="hospital" name="hospital" placeholder="Hospital" value="{{!empty($v->hospital_name)?$v->hospital_name: ''}}">
        </div>
      </div>

      <div class="row">
          <div class="col-md-6">
            <label for="Product">Product </label>
              <select class="form-control input-sm" id="product">
                <option value="{{!empty($v->medicine)?$v->medicine: ''}}">{{!empty($v->medicine)?$v->medicine: '*Select Product'}}</option>
                <option value="Abilify">Abilify</option>
                <option value="Aminoleban">Aminoleban</option>
                <option value="Pletaal">Pletaal</option>
              </select>
          </div>
        <div class="col-md-6">
          <label for="SKU">SKU</label><select class="form-control input-sm" id="product" name="product">
              <option value="">* Please Select SKU</option>
              @if($v->medicine == 'Abilify')
              @foreach($Mgabilify as $mab)
              <option value="{{$mab->id}}">{{$mab->skuname}}</option>
              @endforeach
              @elseif($v->medicine == 'Aminoleban')
              @foreach($Mgaminoleban as $mam)
              <option value="{{$mam->id}}">{{$mam->skuname}}</option>
              @endforeach
              @elseif($v->medicine == 'Pletaal')
              @foreach($Mgpletal as $mp)
              <option value="{{$mp->id}}">{{$mp->skuname}}</option>
              @endforeach
              @endif
            </select>
      </div>
    </div>
<hr/>
            <div class="row">
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Tabs Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="notabsprescribe" class="form-control input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Days Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="nodaysprescribe" class="form-control input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label">Total No. of Tabs</label>
                  <div class="col-md-4">
                    <input type="number" name="totalnotabs" class="form-control input-sm" max="9999">
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe" value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="daysprescribe" value="No Available Data">No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="totaltabs" value="Px Won't Disclose">Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="totaltabs" value="No Available Data">No Available Data</label>
                </div>
              </div>
            </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
                <label>Already Purchased:</label>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" value="Yes">Yes</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" value="No">No</label>
                </div>
              </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-success pull-right" name="encode" value="{{$v->id}}">Save</button>
              </div>
            </div>

        </div>

</div>
</form>
</div>





@endsection 

@section('footer-scripts')
<script>
function loadDoc(filter,person) {
	$('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
        document.getElementById("filterdisplay").innerHTML = xhttp.responseText;

		$('#patient_search').DataTable();
	
			
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchPatient_only?person_filter=" + filter + "&person=" + person, true);
  xhttp.send();
}




function popupwindow(url) {
	
	title = "Images";
	w = 500;
	h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}



$(document).ready(function() {

$("#btn_enroll_save").click(function(){
        var x = $("#enroll-form").serialize();
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			
			document.getElementById("enroll_return_msg").innerHTML = "Save"; //xhttp.responseText;
			
			
			}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/enroll_patient?action=save&"   + x , true);
		xhttp.send();
		  
		  
    });



$(".datatable").DataTable();

});

</script>
@endsection

