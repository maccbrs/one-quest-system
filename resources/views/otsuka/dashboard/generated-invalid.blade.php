@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
  text-align: center;
}

</style>



        <!-- Main content -->
<div class="panel panel-default">
  <div class="panel-heading"><b>MTD Retrieval Raw Data Invalid Report {{date('Ymd')}}</b></div>
  <div class="panel-body">

      <div class="col-md-12">
        <table class="display nowrap compact datatable">
          <thead>
            <tr>
              <th>Patient Kit Number</th>
              <th>(Tagging) Team</th>
              <th>(Tagging) Area Code</th>
              <th>(Tagging) MR Name</th>
              <th>Product</th>
              <th>Remarks</th>
              </td>
            </tr>
          </thead>
          <tbody>
            @foreach($data as $d)
            <tr>
              <td>{{!empty($d->patient_kit_number)?$d->patient_kit_number: 'NO AVAILABLE DATA'}}</td>

                @if(!empty($d->user_medrep->fetch_allocation_april_invalid))
                  
                  <td>{{!empty($d->user_medrep->fetch_allocation_april_invalid->team)?$d->user_medrep->fetch_allocation_april_invalid->team: 'NO AVAILABLE DATA'}}</td>
                  <td>{{!empty($d->user_medrep->fetch_allocation_april_invalid->area_code)?$d->user_medrep->fetch_allocation_april_invalid->area_code: 'NO AVAILABLE DATA'}}</td>
                  <td>{{!empty($d->user_medrep->fetch_allocation_april_invalid->mr_name)?$d->user_medrep->fetch_allocation_april_invalid->mr_name: 'NO AVAILABLE DATA'}}</td>

                @else

                  <td>{{!empty($d->user_medrep->fetch_mrlist->team)?$d->user_medrep->fetch_mrlist->team: 'NO AVAILABLE DATA'}}</td>
                  <td>{{!empty($d->user_medrep->fetch_mrlist->new_code)?$d->user_medrep->fetch_mrlist->new_code: 'NO AVAILABLE DATA'}}</td>
                  <td>{{!empty($d->user_medrep->fetch_mrlist->pso_name)?$d->user_medrep->fetch_mrlist->pso_name: 'NO AVAILABLE DATA'}}</td>

                @endif

              <td>{{!empty($d->medicine)?$d->medicine: 'NO AVAILABLE DATA'}}</td>
              <td>{{!empty($d->remarks)?$d->remarks: 'NO AVAILABLE DATA'}}</td>
  			    </tr>
  			    @endforeach 
          </tbody>
        </table>		
        </div>

    </div>
</div>

@endsection 

@section('footer-scripts')
<script>
function popupwindow(url) {
  
  title = "Images";
  w = 500;
  h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

$(document).ready(function() {


var today = new Date();
var dd = today.getDate();
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();


$(".datatable").DataTable({
  "ordering": false,
  dom: 'Blfrtip',
  buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: 'MTD Retrieval Invalid Raw Data Report '+yyyy+mm+dd,
            },
            'colvis'
        ]
});

});
</script>
@endsection

