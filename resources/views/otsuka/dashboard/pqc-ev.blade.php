@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
     $baseurl = URL::asset('/'); 
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
	text-align: center;
}

</style>



<div class="col-md-12">
<div class="panel panel-default">


    <div class="panel-heading"><strong> Adverse Event</strong></div>
    <div class="panel-body">

		<div class="row">
			<div class="col-md-12">
			
			
			  <h4><?php //echo date('Y-m-d'); ?> </h4><br/>
			  
			  
						<table id="" class="display datatable compact table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Date of First Receipt</th>
									<th>Created By</th>
									<th>Patient Initials</th>
									<th>Reporter Name</th>
									<?php if(Auth::user()->user_level == 2) {?>
									<th>Action</th>
									<th>Print</th>
									<?php }?>
									
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Date of First Receipt</th>
									<th>Created By</th>
									<th>Patient Initials</th>
									<th>Reporter Name</th>
									<?php if(Auth::user()->user_level == 2) {?>
									<th>Action</th>
									<th>Print</th>
									<?php }?>
								</tr>
							</tfoot>
							<tbody>
							
								<?php foreach($aelist as $key2 => $val2) { ?>
								<tr id="ae-row{{$val2->id}}" <?php if($val2->status =='invalid') echo "style='background-color:#d9534f'"; ?>>
								
								
								<td>{{$val2->date_of_first_receipt}}</td>
								<td>{{!empty($val2->fetch_user->name)?$val2->fetch_user->name: ''}}</td>
								<td>{{$val2->patient_initial_f}} </td>
								<td>{{$val2->reporter_name}}</td>
								<?php if(Auth::user()->user_level == 2) {?>
								<td><a href="#" onclick="postAE('{{$val2->id}}')" class='btn btn-success btn-sm'>Done</td>
								<td><a href="{{$baseurl}}otsuka/aeform/{{$val2->id}}" target="_blank" class='btn btn-primary btn-sm'>Print</td>
								<?php }?>
									

								</tr>
								<?php }?>
								
							</tbody>
						</table>
			  

			  
			</div>
			
		</div>
      <hr>
	
	


    </div>
</div>
</div>



<div class="col-md-12">
<div class="panel panel-default">


    <div class="panel-heading"><strong> Product Quality Complaint List</strong></div>
    <div class="panel-body">

		<div class="row">
			<div class="col-md-12">
			
			
			  <h4><?php //echo date('Y-m-d'); ?> </h4><br/>
			  
			  
						<table id="" class="display datatable compact table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Date Complaint Received</th>
									<th>Created By</th>
									<th>Phone No</th>
									<th>Reporter Name</th>
									<th>Product Name </th>
									<?php if(Auth::user()->user_level == 2) {?>
									<th>Action</th>
									<th>Print</th>
									<?php }?>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Date Complaint Received</th>
									<th>Created By</th>
									<th>Phone No</th>
									<th>Reporter Name</th>
									<th>Product Name </th>
									<?php if(Auth::user()->user_level == 2) {?>
									<th>Action</th>
									<th>Print</th>
									<?php }?>
								</tr>
							</tfoot>
							<tbody>
							
								<?php foreach($pqclist as $key => $val) { ?>
								<tr id="pqc-row{{$val->id}}" <?php if($val->status =='invalid') echo "style='background-color:#d9534f'"; ?>>
								
								
									<td>{{$val->pqc_date}}</td>
									<td>{{!empty($val->fetch_user->name)?$val->fetch_user->name: ''}}</td>
									<td>{{$val->phone_no}}</td>
									<td>{{$val->reporter_name}}</td>
									<td>{{$val->sku}}</td>
									<?php if(Auth::user()->user_level == 2) {?>
								<td><a href="#" onclick="postPQC('{{$val->id}}')" class='btn btn-info'>Done</td>
								<td><a href="{{$baseurl}}otsuka/pqcform/{{$val->id}}" target="_blank"class='btn btn-warning'>Print</td>
								<?php }?>

								</tr>
								<?php }?>
								
							</tbody>
						</table>
			  

			  
			</div>
			
		</div>
      <hr>
	
	


    </div>
</div>
</div>


      
      





@endsection 

@section('footer-scripts')
<script>

		function postAE(patient_id) {
			//alert('test');
			  	upload_id = $("#upload_id").val();							
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {	
				$('#ae-row' + patient_id).remove();
				}
				};
				xhttp.open("GET", "<?php echo $baseurl;?>otsuka/aepost/" + patient_id, true);
				xhttp.send();		  		

	}
	

	function postPQC(patient_id) {
			//alert('test');
			  	upload_id = $("#upload_id").val();							
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {	
				$('#pqc-row' + patient_id).remove();
				}
				};
				xhttp.open("GET", "<?php echo $baseurl;?>otsuka/pqcformpost/" + patient_id, true);
				xhttp.send();		  		

	}




function popupwindow(url) {
	
	title = "Images";
	w = 500;
	h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

$(document).ready(function() {









$(".datatable").DataTable({
  "ordering": false,
});

});

$(document).ready(function() {



$('#verify').DataTable({
  "ordering": false,
});

});
</script>
@endsection

