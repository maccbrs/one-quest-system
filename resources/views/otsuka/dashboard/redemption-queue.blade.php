@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
    <?php $baseurl = URL::asset('/'); ?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
.viber {
	color: 	white !important;
	
	background-color: #8f5db7 !important
}

.red {
	color: 000000 !important;
	
	background-color: #ffdede!important
}
.yellow {
	color: black !important;
	background-color: yellow !important;
}
.orange {
	color: white !important;
	background-color: orange !important;
}
.default-color {
	color:#000000;
}

  input {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
  }

 input.noborder {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 0;
  }

  div.border {
      border: 1px solid black;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
  }

  div.padding {
      padding-top: 40px;
      padding-right: 65px;
      padding-bottom: 40px;
      padding-left: 60px;
  }

  h2.space {
    line-height: .25;
  }

  p.ind {
    text-indent: 50px;
    line-height: .1;
  }


</style>			

<div class="panel panel-default">
  <div class="panel-heading"><b>Redemption Queue</b></div>
  <div class="panel-body">

      <div class="col-md-12">
			<div class="well well-sm"><a class="btn btn-primary btn-sm" style="display:none" onclick="processRedemption();"/>Batch Process</a> <a class="btn btn-danger btn-sm" onclick="processRedemptionv2();"/>Batch Process V2</a><a class="btn btn-primary btn-sm"  href="<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id=&patient_id=" style="float:right;display:none"/>View Record</a><a class="btn btn-danger btn-sm"  href="<?php echo $baseurl;?>otsuka/redemption?action=redemptionv2&upload_id=&patient_id=" style="float:right;margin-right:20px"/>Redemption V2 (Beta)</a></div>
			
			<form method="post" id="encode_form" action="{{route('otsuka.encode')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
						
						
							<table class="display compact datatable table-bordered table-hover" cellspacing="0" width="100%" style="font-size:10px">
					<thead>
						<tr>
							<!--<th>Queue</th>
							<th>Type</th>
							<th>Medicine</th>
							<th>Created At</th>
							<th>Claimed</th>
							<th>Encode</th> -->
							
									<tr>
										<th><input type="checkbox" id="checkall" onclick="CheckAll()"></th>
										<th>Px Code</th>
										<th>Px Name</th>
										<th>OR #</th>
										<th>OR Date</th>
										<th>Product</th>
										<th>Uploaded By</th>
										<th>Encoded By</th>										
										<th>Transaction Date</th>
									
										<th>Remarks</th>
										<th>Action</th>
										@if(Auth::user()->user_level == 2)<th>Hide</th>@endif
									</tr>
							
							
							
						</tr>
					</thead>
					<tbody>
					  @if($Queue_Remdeption->count())
					  @foreach($Queue_Remdeption as $qvc_redemption)
					  @if($qvc_redemption->created_at->addHour(1) <= $tn and $qvc_redemption->viber_queue == '0')
					  <tr class="red">
					  @elseif($qvc_redemption->created_at->addMinutes(45) <= $tn and $qvc_redemption->viber_queue == '0')
					  <tr class="orange">	
					  @elseif($qvc_redemption->created_at->addMinutes(30) <= $tn and $qvc_redemption->viber_queue == '0')
					  <tr class="yellow">
					  @elseif($qvc_redemption->viber_queue == '1')
					  <tr class="viber">
					  @elseif($qvc_redemption->created_at->addMinutes(30) > $tn and $qvc_redemption->viber_queue == '0')
					  <tr class="default-color">
					  @endif
					  <td><input type="checkbox" name="upload_id_checkbox" class="upload_id_checkbox" value="{{$qvc_redemption->id}}"/></td>
					  <td>{{$qvc_redemption->id}}</td>
					  <td>{{$qvc_redemption->patient_name}}</td>					  
					  <td>{{$qvc_redemption->receipt_details->or_number}}</td>					  
					  <td>{{$qvc_redemption->receipt_details->or_date}}</td>					  

					  <td>{{$qvc_redemption->medicine}}</td>
					  <td>{{$qvc_redemption->user->name}}</td>
					 			 
					  <td>@if(!empty($qvc_redemption->encoded_by->name)){{$qvc_redemption->encoded_by->name}} @endif</td>		
					  <td>{{$qvc_redemption->created_at}}</td>
					  
					  <td>{{$qvc_redemption->redemption_remarks}}</td>
					  
					  <!--
					  <td><a href="<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id={{$qvc_redemption->id}}&patient_id={{$qvc_redemption->redeem_by}}" name="encode" value="" class="btn btn-xs btn-primary">process</a></td> -->
					  <td><a href="<?php echo $baseurl;?>otsuka/redemption?action=redemptionv2&upload_id={{$qvc_redemption->id}}&patient_id={{$qvc_redemption->redeem_by}}" name="encode" value="" class="btn btn-xs btn-danger">process</a></td>
					   @if(Auth::user()->user_level == 2)<td><a href="#" onclick="hideupload('{{$qvc_redemption->id}}')"  value="{{$qvc_redemption->id}}" style="display:none" class="btn btn-xs btn-primary">Hide</a></td>@endif
					  </tr>
					  
					
					 
					  @endforeach
					  @endif
					</tbody>
			</table>
						
						
						
						
						
			
				</form>
		</div>
	</div>
</div>		 <!-- REDEMPTION QUEUEING -->			

@endsection 

@section('footer-scripts')
<script>
	
		function CheckAll() {
				
				

				if ($('#checkall').is(':checked')) {
				$(".upload_id_checkbox").each(function() {
					$(this).prop( "checked", true );
				});				 
				}
				else
				{
				$(".upload_id_checkbox").each(function() {
					$(this).prop( "checked", false );
				});	
				}
		}

	function processRedemption(upload_id) {
   var str = "";
				 $(".upload_id_checkbox:checked").each(function() {
					str += $(this).val() + ",";
			  });
			  str = str.substr(0, str.length - 1);
			  alert(str);
				
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id=" + str);
		
		//var xhttp = new XMLHttpRequest();
		//xhttp.onreadystatechange = function() {
		//if (this.readyState == 4 && this.status == 200) {	
		
		//document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		//alert('Success');
		//document.getElementsByTagName("body").innerHTML = xhttp.responseText;
		//}
		//};
		//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id, true);
		//xhttp.send();					 
		}

		
	
	function processRedemptionv2(upload_id) {
   var str = "";
				 $(".upload_id_checkbox:checked").each(function() {
					str += $(this).val() + ",";
			  });
			  str = str.substr(0, str.length - 1);
			  alert(str);
				
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemptionv2&upload_id=" + str);
		
		//var xhttp = new XMLHttpRequest();
		//xhttp.onreadystatechange = function() {
		//if (this.readyState == 4 && this.status == 200) {	
		
		//document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		//alert('Success');
		//document.getElementsByTagName("body").innerHTML = xhttp.responseText;
		//}
		//};
		//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id, true);
		//xhttp.send();					 
		}	




function hideupload(id) {
	//alert(id);
 	var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/agent");
		//$('#upload_queue_id_' + id).remove();
		//$(".upload-datatable").DataTable();
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/disable?active=" + id , true);

  xhttp.send();	 
    
}



$(document).ready(function() {

/*            setTimeout(function() {
                document.location.reload(true);
            }, 3500);*/

$(".datatable").DataTable({
  
  "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
});

});
</script>
@endsection

