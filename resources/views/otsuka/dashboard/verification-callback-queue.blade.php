@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
    <?php $baseurl = URL::asset('/'); ?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
.viber {
	color: 	white !important;
	
	background-color: #8f5db7 !important
}

.red {
	color: white !important;
	
	background-color: #CD5C5C!important
}
.yellow {
	color: black !important;
	background-color: yellow !important;
}
.orange {
	color: white !important;
	background-color: orange !important;
}
.default-color {
	color:#000000;
}

  input {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
  }

 input.noborder {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 0;
  }

  div.border {
      border: 1px solid black;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
  }

  div.padding {
      padding-top: 40px;
      padding-right: 65px;
      padding-bottom: 40px;
      padding-left: 60px;
  }

  h2.space {
    line-height: .25;
  }

  p.ind {
    text-indent: 50px;
    line-height: .1;
  }

    .center {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 10%;
}


</style>

<div class="panel panel-default">
  <div class="panel-heading"><b>Verification Callback Queue</b></div>
  <div class="panel-body" id="verification_callback">

	</div>
</div> <!-- ENCODE QUEUEING -->		

@endsection 

@section('footer-scripts')
<script>


	function processRedemption(upload_id) {
   var str = "";
				 $(".upload_id_checkbox:checked").each(function() {
					str += $(this).val() + ",";
			  });
			  str = str.substr(0, str.length - 1);
			  alert(str);
				
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id=" + str);
		
		//var xhttp = new XMLHttpRequest();
		//xhttp.onreadystatechange = function() {
		//if (this.readyState == 4 && this.status == 200) {	
		
		//document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		//alert('Success');
		//document.getElementsByTagName("body").innerHTML = xhttp.responseText;
		//}
		//};
		//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id, true);
		//xhttp.send();					 
		}





function hideupload(id) {
	//alert(id);
 	var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/agent");
		//$('#upload_queue_id_' + id).remove();
		//$(".upload-datatable").DataTable();
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/disable?active=" + id , true);

  xhttp.send();	 
    
}



$(document).ready(function() {

/*            setTimeout(function() {
                document.location.reload(true);
            }, 3500);*/

$(".datatable").DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
});

});


$(document).ready(function() {
$('#verification_callback').html('<img class="center" src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif">');
setInterval(function () {	
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
        document.getElementById("verification_callback").innerHTML = xhttp.responseText;
		$('#verify_callback').DataTable({
  			"ordering": false,
  			"searching": false,
  			"stateSave": true,
  			"lengthChange": false,
  			    "columnDefs": [
  			      {"className": "dt-center", "targets": "_all"}
  			    ],
			});
	
			
    }
  };
xhttp.open("GET", "{{route('otsuka.ajax.searchperson')}}?person=verification_callback", true);
  xhttp.send();
  }, 1000);

    $('#verication-queue tfoot th').each( function (i) {
        var title = $('#verication-queue thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" data-index="'+i+'" />' );
    } );

var today = new Date();
var dd = ("0" + (today.getDate())).slice(-2);
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();

var table = $('#verication-queue').DataTable({
  "ordering": false,
  "scrollX": true,
  "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
  "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
  "search": {
  "caseInsensitive": false
  },
});
   $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
        table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );

});
</script>
@endsection

