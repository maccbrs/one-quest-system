@extends('otsuka.master')

@section('Patient Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
th { font-size: 12px; }
td { font-size: 11px; }

.med_logo img {
    width:100%;
    height:10%;
}

[hidden] {
  display: none !important;
}

.bar {
    height: 18px;
    background: green;
}
.modal-center {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

/**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
margin-left: 3vw;     
margin-right:3vw;  
}    

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(.5);    
-moz-transform: scale(.5);  
-webkit-transform: scale(.5);  
-o-transform: scale(.5);  
transform: scale(.5);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
ul.gallery {      
margin-left: 15vw;       
margin-right: 15vw;
}

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}

  input {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
  }

 input.noborder {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 0;
  }

  div.border {
      border: 1px solid black;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
  }

  div.padding {
      padding-top: 40px;
      padding-right: 65px;
      padding-bottom: 40px;
      padding-left: 60px;
  }

  h2.space {
    line-height: .25;
  }

  p.ind {
    text-indent: 50px;
    line-height: .1;
  }

</style>

<div class="padding">

  <div align="center">
    <h2 class="space">Otsuka (Philippines) Pharmaceutical, Inc.</h2>
    <h3>Safety Report Form</h3>
  </div>
    <form method="post" action="{{route('otsuka.ae-form.save')}}" enctype="multipart/form-data">
      {{ csrf_field() }} 
      <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
  <div align="right">

      <br><b>Date of First Receipt (DFR) </b> <input type="text" class="datepicker" name="date_of_first_receipt"/><br><br>
      
  </div>

  <div class="border">
    <div>
      <h4>A. PATIENT INFORMATION (PLEASE FILL-UP ALL INFORMATION)</h4>

      <table style="width: 100%">
        <tr align="left">
          <th>1. Patient's Initials</th>
          <th>2. Date of Birth</th> 
          <th>3. Age</th>
          <th>4. Weight (kg)</th>
          <th>5. Height (cm)</th>
          </tr>

          <tr align="left">
            <td>
                <input type="text" name="patient_initial_f" maxlength="3" size="2" step="3" placeholder="First">
                <input type="text" name="patient_initial_m" maxlength="1" size="2" placeholder="Middle">
                <input type="text" name="patient_initial_l" maxlength="1" size="1" placeholder="Last">
            </td>

            <td>
              <input type="text" class="datepicker" name="patient_data_of_birth">
            </td>

            <td>
              <input type="number" name="patient_age" min="0" max="999">
            </td>

            <td>
              <input type="number" name="patient_weight" min="0" max="999">
            </td>

            <td>
              <input type="number" name="patient_height" min="0" max="999">
            </td>
        </tr>
      </table>


      <p><b>6. Gender: <label class="radio-inline"><input type="radio" name="patient_gender" value="Male">M</label><label class="radio-inline"><input type="radio" name="patient_gender" value="Female">F</label>, 
      is the patient pregnant? </b><label class="radio-inline"><input type="radio" name="patient_is_pregnant" value="Yes">Yes</label><label class="radio-inline"><input type="radio" name="patient_is_pregnant" value="No">No</label>
      <hr>
    </div>

    <div>
      <h4>B. SAFETY INFORMATION</h4>

      <table style="width: 100%">
        <tr align="left">
          <th>1. Date of this report 
            <input type="text" class="datepicker"  name="date_of_report">
          </th>

          <th>2. Onset Date 
            <input type="text" class="datepicker" name="onset_date">
          </th> 

          <th>3. Report Type &nbsp;
            <label class="radio-inline"><input type="radio" name="report_type" value="Initial">Initial</label>
            <label class="radio-inline"><input type="radio" name="report_type" value="Follow-up">Follow</label>
         </th>
        </tr>
      </table>

      <p><b>4. Source: 
            <label class="radio-inline"><input type="radio" name="source" value="Solicited">Solicited<i>(specify Title of Study) </i><input type="text" name="source_details" size="35"></label>
            <label class="radio-inline"><input type="radio" name="source" value="Spontaneous">Spontaneous</label>

      <p><b>5. Seriousness Criteria </b><i>(check all that apply)</i></p>

      <table style="width: 100%">
        <tr align="left">
          <th><input type="checkbox" name="is_death" value="death"><span style="font-weight: normal;">Death</span></th>
          <th><input type="checkbox" name="is_life_threat" value="lifethreatening"><span style="font-weight: normal;">Life Threatening</span></th>
          <th><input type="checkbox" name="is_hospitalize" value="hospitalization"><span style="font-weight: normal;">Hospitalization</span></th>
          <th><input type="checkbox" name="is_congenital" value="congenitalanomaly"><span style="font-weight: normal;">Congenital anomaly</span></th>
          <th><input type="checkbox" name="is_disability" value="disabilityincapacity"><span style="font-weight: normal;">Disability/Incapacity</span></th>
        </tr>

        <tr align="left">
          <th><input type="checkbox" name="is_medically_sign" value="medicallysignificant" ><span style="font-weight: normal;">Medically Significant </span></th>
          <th><input type="checkbox" name="is_non_serious" value="nonserious" ><span style="font-weight: normal;">Non-Serious </span></th>
          <th><input type="checkbox" name="is_not_reported" value="notreported" ><span style="font-weight: normal;">Not reported </span></th>
        </tr>
      </table>

        <p><b>6. Describe Safety Information (e.g. Adverse Event, Off-Label Use, Lack of Efficacy, etc.)</b></p>
        <textarea name="describe_safety_info" style="width:100%; height:100px;"></textarea>
        <br>

        <p><b>7. Were any medicines taken or procedures done to treat the event related to Item no. 6? (If available)</b></p>
        <textarea name="medicines_taken" style="width:100%; height:100px;"></textarea>
        <br>

      <p>
        <b>8. What is the Outcome?</b>
            <label class="radio-inline"><input type="radio" name="outcome" value="Fatal">Fatal</label>
            <label class="radio-inline"><input type="radio" name="outcome" value="Resolved">Resolved</label>
            <label class="radio-inline"><input type="radio" name="outcome" value="Resolved with sequelae">Resolved with sequelae</label>
            <label class="radio-inline"><input type="radio" name="outcome" value="Not Resolved">Not Resolved</label>
            <label class="radio-inline"><input type="radio" name="outcome" value="Unknown">Unknown</label>
        <br>
        <p style="margin-left: 30px;">8.1. If the event was resolved, kindly provide the date the patient recovered from the event:
        <input type="text" class="datepicker" name="outcome_date"></p>
      </p>
      
      <p>
        <b>9. Is the adverse event related to the drug?</b> (Reporter’s Causality)&nbsp;
            <label class="radio-inline"><input type="radio" name="related_to_drug" value="Yes">Yes</label>
            <label class="radio-inline"><input type="radio" name="related_to_drug" value="No">No</label>
            <label class="radio-inline"><input type="radio" name="related_to_drug" value="Not assessable">Not assessable</label>
      </p>

      <p>
        <b>10. Is the adverse event expected by the patient? </b>
            <label class="radio-inline"><input type="radio" name="is_expected" value="Yes">Yes</label>
            <label class="radio-inline"><input type="radio" name="is_expected" value="No">No</label>
            <label class="radio-inline"><input type="radio" name="is_expected" value="Not assessable">Not assessable</label>
      </p>

      <table class="display" style="width: 100%" border=".5">
        <tr>
          <th align="left">11. Suspect Medication <br><span style="font-weight: normal;">(Please indicate strength and<br> manufacturer if known)</span></th>
          <th>Indication for which drug <br>is given</th>
          <th>Dose, Frequency and <br>Route</th>
          <th>Start Date</th>
          <th>Stop Date</th>
        </tr>

        <tr>
          <td>1. <input class="noborder" type="text" name="suspected_1"></td>
          <td> <input class="noborder" type="text" name="indication_1"></td>
          <td> <input class="noborder" type="text" name="dose_route_1"></td>
          <td> <input  type="text" class="" name="start_date_1"></td>
          <td> <input  type="text" class="" name="stop_date_1"></td>
        </tr>       
		<tr>
          <td>2. <input class="noborder" type="text" name="suspected_2"></td>
          <td> <input class="noborder" type="text" name="indication_2"></td>
          <td> <input class="noborder" type="text" name="dose_route_2"></td>
          <td> <input  type="text" class="" placeholder="m/d/yyyy" name="start_date_2"></td>
          <td> <input  type="text" class="" placeholder="m/d/yyyy"name="stop_date_2"></td>
        </tr>

       
      </table>
    </div>

    <hr>

    <div>
       <h4>C. REPORTER</h4>
       <table>
        <tr>
          <td>1. Name <input type="text" name="reporter_name" size="40"></td>
          <td> Address/Contact details: <input type="text" name="reporter_contact"  style="width:420px"></td>
        </tr>

        <tr>
          <td>2. Health Profession <input type="text" name="reporter_profession" style="width:200px"></td>
        </tr>
       </table>

       <table style="width: 100%">
        <tr>
          <td>3. Reported through </td>
            <td><label class="radio-inline"><input type="radio" name="reported_through" value="Fax">Fax</label></td>
            <td><label class="radio-inline"><input type="radio" name="reported_through" value="Telephone">Telephone</label></td>
            <td><label class="radio-inline"><input type="radio" name="reported_through" value="PSR">PSR</label></td>
          <td><input type="text" name="psr_input" size="25"></td>
          <td>4. Date Reported <input type="text" class="datepicker" name="date_reported"> </td>
        </tr>

        <tr>
          <td>4. Did the reporter consent to follow up contact?</td>
            <td><label class="radio-inline"><input type="radio" name="did_reporter_consent" value="Yes">Yes</label></td>
            <td><label class="radio-inline"><input type="radio" name="did_reporter_consent" value="No">No</label></td>
        </tr>
      </table>

      <table>
        <tr>
          <td>5. Did the reporter consent to provide reporting information to Otsuka Contractor?</td>
            <td><label class="radio-inline"><input type="radio" name="did_reporter_provide_info" value="Yes">Yes</label></td>
            <td><label class="radio-inline"><input type="radio" name="did_reporter_provide_info" value="No">No</label></td>
        </tr>
      </table>

      <p><i>Note to the reporter: Please be informed that this case might be reported to Global Otsuka and local regulatory authority
      according to Otsuka requirements and local regulation, but rest assured that personal information will be highly protected.</i></p>
    </div>

    <div>
      <h4>D. OTSUKA (PHILIPPINES) PHARMACEUTICAL INC. USE ONLY</h4>
      <p>1. Date Received by LSM
        <input type="text" class="datepicker" name="date_receive_lsm"></p>
    </div>
  </div>

  <div align="center">
    <p><i><b>(Please fax accomplished report at 811-2279 or send via e-mail at oppi-pv@otsuka.com.ph)</b></i></p>
  </div>

  <div>
    <p align="left">Version 7.0</p>
    <p align="right">December 2017</p>
  </div>

  <div>
	<input type="submit" class="btn btn-primary pull-right" value="submit"/>
    <?php /*<a href="{{route('otsuka.dashboard.queue')}}" class="btn btn-primary pull-right">Submit</a> <? */?>
  </div>
</div>

@endsection 

@section('footer-scripts')
<script>
</script>
@endsection

