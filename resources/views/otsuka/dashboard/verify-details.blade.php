@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
  <?php $baseurl = URL::asset('/'); ?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}

label.right-label {
        text-align: right;
}

.green
{
  color: green;
}
</style>


<div class="col-md-12">

  <div class="panel panel-default">
    <div class="panel-heading">Verified Patient Information</div>
    <div class="panel-body">

        <div class="col-md-12">
        <form method="post" id="verify_encode" action="{{route('otsuka.save')}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        <input type="hidden" name="mobile_number" value="{{!empty($vc->mobile_number)?$vc->mobile_number: ''}}">
          <table class="table">
            <thead>
              <tr>
                <th>Call Attempt</th>
                <th>Call Date</th>
                <th>Status</th>
                <th>Remarks</th>
                <th>Call Notes</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>
      @if(!empty($vc->fetch_call->fetch_verification))
              @foreach($vc->fetch_call->fetch_verification as $v_call)
              @if($v_call->allowcallback == 1 or $v_call->allowcallback == 2 or $v_call->allowcallback == -1)
              <tr class="statuscheck">
                                <input type="hidden" name="upload_id" value="{{$vc->id}}">
                <input type="hidden" name="cmid" value="{{$vc->cmid}}">
                <td>#{{!empty($v_call->attempt)?$v_call->attempt: ''}}<input type="hidden" name="v_attempt" value="{{!empty($v_call->attempt)?$v_call->attempt: ''}}"><input type="hidden" name="cmid" value="{{!empty($v_call->cmid)?$v_call->cmid: ''}}"></td>
                <td>{{!empty($v_call->actualdate)?$v_call->actualdate: '(Auto Generated)'}}</td>
                <td><select class="form-control input-sm" name="remarks_call" required="">
                      <option value="{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: ''}}">{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: 'Please Select Status'}}</option>
                      @foreach($status as $value)
                      <option value="{{$value->id}}">{{$value->name}}</option>
                      @endforeach
                    </select>
                </td>
                <td><input type="text" class="form-control input-sm" placeholder="Remarks" name="remarks_others" value="{{!empty($v_call->remarks_others)?$v_call->remarks_others: ''}}"></td>
                <td><textarea class="form-control" rows="5" name="callnotes" id="callnotes" placeholder="Callnotes">{{!empty($v_call->callnotes)?$v_call->callnotes: ''}}</textarea><input type="hidden" name="v_id" value="{{!empty($v_call->id)?$v_call->id: ''}}"></td>
                <td><button type="submit" class="btn btn-success" id="test" value="{{!empty($v_call->id)?$v_call->id: ''}}">Update</button></td>
              </tr>
              @elseif(empty($v_call->allowcallback))
              <tr>
                <td>#{{!empty($v_call->attempt)?$v_call->attempt: ''}}<input type="hidden" name="v_attempt" value="{{!empty($v_call->attempt)?$v_call->attempt: ''}}"><input type="hidden" name="cmid" value="{{!empty($v_call->cmid)?$v_call->cmid: ''}}"><input type="hidden" name="upload_id" value="{{!empty($vc->id)?$vc->id: ''}}"></td>
                <td>{{!empty($v_call->actualdate)?$v_call->actualdate: '(Auto Generated)'}}</td>
                <td><select class="form-control input-sm" name="remarks_call" required="">
                      <option value="{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: ''}}">{{!empty($v_call->fetch_status->name)?$v_call->fetch_status->name: 'Please Select Status'}}</option>
                      @foreach($status as $value)
                      <option value="{{$value->id}}">{{$value->name}}</option>
                      @endforeach
                    </select>
                </td>
                <td><input type="text" class="form-control input-sm" placeholder="Remarks" name="remarks_others" value="{{!empty($v_call->remarks_others)?$v_call->remarks_others: ''}}"></td>
                <td><textarea class="form-control" rows="5" name="callnotes" id="callnotes" placeholder="Callnotes">{{!empty($v_call->callnotes)?$v_call->callnotes: ''}}</textarea><input type="hidden" name="v_id" value="{{!empty($v_call->id)?$v_call->id: ''}}"></td>
                <td><button type="submit" class="btn btn-success" id="test2" value="{{!empty($v_call->id)?$v_call->id: ''}}">Update</button></td>
              </tr>
              @endif
              @endforeach
      @endif
            </tbody>
          </table>
          </form>
        </div>

    </div>
  </div>

</div>





@endsection 

@section('footer-scripts')
<script>
$('#verify_encode').bind('submit', function (e) {
    // Disable the submit button while evaluating if the form should be submitted
    $('#test2').prop('disabled', true);

    var valid = true;    
    var error = '';

    // If the email field is empty validation will fail
    if ($('[name=remarks_call]').val() === '') {
        valid = false;
        error = 'Remarks is required';
    }

    if (!valid) { 
        // Prevent form from submitting if validation failed
        e.preventDefault();

        // Reactivate the button if the form was not submitted
        $('#test2').prop('disabled', false);
        
        alert(error);
    }
});

  $("tr.statuscheck select, tr.statuscheck input, tr.statuscheck textarea, tr.statuscheck input").prop('disabled', true);
  $("tr.statuscheck2 select").prop('readonly', true);

</script>
@endsection

