@extends('otsuka.master')

@section('Compliance Call Report')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
	text-align: center;
}

</style>

<?php /*
<!-- <div class="panel panel-default">
  <div class="panel-heading"><b>Daily Call Raw Data Report {{date('Ymd')}}</b></div>
  <div class="panel-body">

      <div class="col-md-12">
        <table class="display compact nowrap" id="daily">
          <thead>
            <tr>
              <th>Patient Code</th>
              <th>Patient Kit #</th>
              <th>Month of Compliance Call</th>
              <th>Date of Compliance Call</th>
              <th>(Tagging) Team</th>
              <th>(Tagging) Area Code</th>
              <th>(Tagging) MR Name</th>
              <th>Enrollment Type</th>
              <th>(Enrollment) Date</th>
              <th>Px Class</th>
              <th>MD ID</th>
              <th>MD Name</th>
              <th>Hospital/Instution Name</th>
              <th>Product Prescribed</th>
              <th>SKU Prescribed</th>
              <th>No. of Tabs Prescribed</th>
              <th>No. of Days Prescribed</th>
              <th>Purchased (Y/N)</th>
              <th>Date of 1st Redemption</th>
              <th>Product SKU Taken </th>
              <th>Date of Start Medication</th>
              <th>Still On Medication (Y/N)</th>
              <th>Month Stopped</th>
              <th>Final Outcome of Compliance Call Verification</th>
              <th>Reason</th>
              <th>New Medication</th>
              <th>Call Notes</th>
              </td>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Patient Code</th>
              <th>Patient Kit #</th>
              <th>Month of Compliance Call</th>
              <th>Date of Compliance Call</th>
              <th>(Tagging) Team</th>
              <th>(Tagging) Area Code</th>
              <th>(Tagging) MR Name</th>
              <th>Enrollment Type</th>
              <th>(Enrollment) Date</th>
              <th>Px Class</th>
              <th>MD ID</th>
              <th>MD Name</th>
              <th>Hospital/Instution Name</th>
              <th>Product Prescribed</th>
              <th>SKU Prescribed</th>
              <th>No. of Tabs Prescribed</th>
              <th>No. of Days Prescribed</th>
              <th>Purchased (Y/N)</th>
              <th>Date of 1st Redemption</th>
              <th>Product SKU Taken </th>
              <th>Date of Start Medication</th>
              <th>Still On Medication (Y/N)</th>
              <th>Month Stopped</th>
              <th>Final Outcome of Compliance Call Verification</th>
              <th>Reason</th>
              <th>Call Remarks</th>
              <th>Call Notes</th>
              </td>
            </tr>
          </tfoot>
        <tbody>
          @foreach($data as $d)
          <tr>
            <td>{{!empty($d->fetch_patient->patient_code)?$d->fetch_patient->patient_code: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->patient_kit_number)?$d->fetch_patient->patient_kit_number: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty(date('M', strtotime($d->created_at)))?date('M', strtotime($d->created_at)): 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty(date('m/d/Y', strtotime($d->created_at)))?date('m/d/Y', strtotime($d->created_at)): 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->fetch_mr->team)?$d->fetch_patient->fetch_mr->team: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->fetch_mr->area_code)?$d->fetch_patient->fetch_mr->area_code: 'NO AVAILABLE DATA'}}</td>
<<<<<<< HEAD
            <td>{{!empty($d->fetch_patient->fetch_mr->mr_name)?$d->fetch_patient->fetch_mr->mr_name: 'NO AVAILABLE DATA'}}</td>
            @if($d->fetch_old->enrollment_type == 1)
            <td>RETRIEVAL</td>
            @elseif($d->fetch_old->enrollment_type == 2)
            <td>VOLUNTARY</td>
            @else
            <td>REFERRAL</td>
            @endif
            <td>{{!empty(date('m/d/Y', strtotime($d->fetch_old->created_at)))?date('m/d/Y', strtotime($d->fetch_old->created_at)): 'NO AVAILABLE DATA'}}</td>
=======
			<td>{{!empty($d->fetch_patient->fetch_mr->fetch_mr_record->mrname)?$d->fetch_patient->fetch_mr->fetch_mr_record->mrname: 'NO AVAILABLE DATA'}}</td>
				{{--<td>{{!empty($d->fetch_patient->fetch_mr->mr_name)?$d->fetch_patient->fetch_mr->mr_name: 'NO AVAILABLE DATA'}}</td> --}}
            <td>{{!empty($d->fetch_callstatus->name)?$d->fetch_callstatus->name: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->remarks)?$d->remarks: 'NO AVAILABLE DATA'}}</td>
<<<<<<< HEAD
            <td>{{!empty($d->fetch_patient->patient_type)?$d->fetch_patient->patient_type: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->date_encoded)?$d->fetch_patient->date_encoded: 'NO AVAILABLE DATA'}}</td>            
            <td>NEW</td>           
            <td>{{!empty($d->fetch_patient->patient_mr_id)?$d->fetch_patient->patient_mr_id: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->patient_mr_name)?$d->fetch_patient->patient_mr_name: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->patient_tagging_hospital)?$d->fetch_patient->patient_tagging_hospital: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->patient_tagging_product_prescribe)?$d->fetch_patient->patient_tagging_product_prescribe: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->patient_tagging_sku_prescribe)?$d->fetch_patient->patient_tagging_sku_prescribe: 'NO AVAILABLE DATA'}}</td>
            
            <td>{{!empty($d->fetch_patient->fetch_sku_compliance->no_of_tabs_prescribe)?$d->fetch_patient->fetch_sku_compliance->no_of_days_prescribe: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->fetch_sku_compliance->total_no_of_tabs)?$d->fetch_patient->fetch_sku_compliance->total_no_of_tabs: 'NO AVAILABLE DATA'}}</td>
            
            <td>{{!empty($d->fetch_patient->fetch_sku_compliance->purchased)?$d->fetch_patient->fetch_sku_compliance->purchased: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->fetch_firstredeem->etimestamp)?$d->fetch_patient->fetch_firstredeem->etimestamp: 'NO AVAILABLE DATA'}}</td>

            @if($d->created_by !== '')
            <td>OLD</td>
            @else
            <td>NEW</td>
            @endif
            @if(!empty($d->fetch_old->fetch_doctors->fetch_mdlist))
            <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->doctorid)?$d->fetch_old->fetch_doctors->fetch_mdlist->doctorid: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->mdname)?$d->fetch_old->fetch_doctors->fetch_mdlist->mdname: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->hospital)?$d->fetch_old->fetch_doctors->fetch_mdlist->hospital: 'NO AVAILABLE DATA'}}</td>
            @else
            <td>{{!empty($d->fetch_old->fetch_doctors->doctorcode)?$d->fetch_old->fetch_doctors->doctorcode: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_old->fetch_doctors->doctorname)?$d->fetch_old->fetch_doctors->doctorname: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_old->fetch_doctors->hospital)?$d->fetch_old->fetch_doctors->hospital: 'NO AVAILABLE DATA'}}</td>
            @endif
            <td>{{!empty($d->fetch_old->fetch_product->brandname)?$d->fetch_old->fetch_product->brandname: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_old->fetch_sku_compliance->skuname)?$d->fetch_old->fetch_sku_compliance->skuname: 'NO AVAILABLE DATA'}}</td>
            @if($d->fetch_old->quantity_per_day != -1)
            <td>{{!empty($d->fetch_old->quantity_per_day)?$d->fetch_old->quantity_per_day: ''}}</td>
            @else
            <td>NO AVAILABLE DATA</td>
            @endif
            @if($d->fetch_old->prescribedays != -1)
            <td>{{!empty($d->fetch_old->prescribedays)?$d->fetch_old->prescribedays: ''}}</td>
            @else
            <td>NO AVAILABLE DATA</td>
            @endif
            <td>NO AVAILABLE DATA</td>
            @if(!empty($d->fetch_patient->fetch_firstredeem))
            <td>{{!empty(date('m/d/Y', strtotime($d->fetch_patient->fetch_firstredeem->etimestamp)))?date('m/d/Y', strtotime($d->fetch_patient->fetch_firstredeem->etimestamp)): 'NO AVAILABLE DATA'}}</td>
            @else
            <td>NO AVAILABLE DATA</td>
            @endif
>>>>>>> 397184635aa0d7a7e8dc8dd087e39fd737a24022
            <td>{{!empty($d->sku)?$d->sku: 'NO AVAILABLE DATA'}}</td>
            @if($d->date_start_medication == '12/29/1899')
            <td>NO AVAILABLE DATA</td>
            @else
            <td>{{!empty($d->date_start_medication)?$d->date_start_medication: 'NO AVAILABLE DATA'}}</td>
            @endif
            <td>{{!empty($d->still_medication)?$d->still_medication: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_month->name)?$d->fetch_month->name: 'NO AVAILABLE DATA'}}</td>
<<<<<<< HEAD
            <td>{{!empty($d->reason_not_med)?$d->reason_not_med: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->reason_not_med)?$d->reason_not_med: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->reason_not_med)?$d->reason_not_med: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->medication)?$d->medication: 'NO AVAILABLE DATA'}}</td>
=======
            <td>{{!empty($d->fetch_compliance_reason->name)?$d->fetch_compliance_reason->name: 'NO AVAILABLE DATA'}}</td>
            @if($d->still_medication == 'No')
            <td>{{!empty($d->reason_not_med)?$d->reason_not_med: 'NO AVAILABLE DATA'}}</td>
            @elseif(empty($d->still_medication) and $d->fetch_compliance_reason->name == 'Switched Medication')
            <td>{{!empty($d->medication)?$d->medication: 'NO AVAILABLE DATA'}}</td>
            @else
            <td>NO AVAILABLE DATA</td>
            @endif
            <td>{{!empty($d->medication)?$d->medication: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->callnotes)?$d->callnotes: 'NO AVAILABLE DATA'}}</td>
>>>>>>> 397184635aa0d7a7e8dc8dd087e39fd737a24022
          </tr>
          @endforeach
        </tbody>
        </table>
        </div>

    </div>
</div> -->

 */ ?>


<div class="panel panel-default">
  <div class="panel-heading"><b>Compliance Call Data Report {{date('Ymd')}}</b></div>
  <div class="panel-body">

      <div class="col-md-12">
        <table class="display compact nowrap" id="daily">
          <thead>
            <tr>
              <th>Patient Code</th>
              <th>Patient Kit #</th>
              <th>Month of Compliance Call</th>
              <th>Date of Compliance Call</th>
              <th>(Tagging) Team</th>
              <th>(Tagging) Area Code</th>
              <th>(Tagging) MR Name</th>
              <th>Enrollment Type</th>
              <th>(Enrollment) Date</th>
              <th>Px Class</th>
              <th>MD ID</th>
              <th>MD Name</th>
              <th>Hospital/Instution Name</th>
              <th>Product Prescribed</th>
              <th>SKU Prescribed</th>
              <th>No. of Tabs Prescribed</th>
              <th>No. of Days Prescribed</th>
              <th>Purchased (Y/N)</th>
              <th>Date of 1st Redemption</th>
              <th>Product SKU Taken </th>
              <th>Date of Start Medication</th>
              <th>Still On Medication (Y/N)</th>
              <th>Month Stopped</th>
              <th>Final Outcome of Compliance Call Verification</th>
              <th>Reason</th>
              <th>New Medication</th>
              <th>Call Notes</th>
            </tr>
          </thead>
        <tbody>
          @foreach($data as $d)
          <tr>

            @if(!empty($d->fetch_patient) or !empty($d->fetch_old))

            <td>{{!empty($d->fetch_patient->patient_code)?$d->fetch_patient->patient_code: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->patient_kit_number)?$d->fetch_patient->patient_kit_number: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty(date('M', strtotime($d->created_at)))?date('M', strtotime($d->created_at)): 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty(date('m/d/Y', strtotime($d->created_at)))?date('m/d/Y', strtotime($d->created_at)): 'NO AVAILABLE DATA'}}</td>

            @if(!empty($d->fetch_old))
            <td>{{!empty($d->fetch_patient->fetch_mr->team)?$d->fetch_patient->fetch_mr->team: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->fetch_mr->area_code)?$d->fetch_patient->fetch_mr->area_code: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->fetch_mr->mr_name)?$d->fetch_patient->fetch_mr->mr_name: 'NO AVAILABLE DATA'}}</td>
            @else
            @if($d->fetch_patient->patient_type == 'Retrieval')
              @if(!empty($d->fetch_patient->fetch_mr))
                <td>{{!empty($d->fetch_patient->fetch_mr->team)?$d->fetch_patient->fetch_mr->team: 'NO AVAILABLE DATA'}}</td>
    
                <td>{{!empty($d->fetch_patient->fetch_mr->area_code)?$d->fetch_patient->fetch_mr->area_code: 'NO AVAILABLE DATA'}}</td>
    
                <td>{{!empty($d->fetch_patient->fetch_mr->mr_name)?$d->fetch_patient->fetch_mr->mr_name: 'NO AVAILABLE DATA'}}</td>
              @else
                <td>{{!empty($d->fetch_patient->fetch_mr->team)?$d->fetch_patient->fetch_mr->team: 'NO AVAILABLE DATA'}}</td>
    
                <td>{{!empty($d->fetch_patient->fetch_mr->area_code)?$d->fetch_patient->fetch_mr->area_code: 'NO AVAILABLE DATA'}}</td>
    
                <td>{{!empty($d->fetch_patient->fetch_mr->mr_name)?$d->fetch_patient->fetch_mr->mr_name: 'NO AVAILABLE DATA'}}</td>
              @endif
            @else
              @if(!empty($d->fetch_patient->fetch_receipt_encoded_compliance->fetch_upload->user_medrep))
                 @if($d->fetch_patient->fetch_receipt_encoded_compliance->fetch_upload->user_medrep->user_type == 'medrep')
                 <td>{{!empty($d->fetch_patient->fetch_receipt_encoded_compliance->fetch_upload->user_medrep->fetch_mrlist->team)?$d->fetch_patient->fetch_receipt_encoded_compliance->fetch_upload->user_medrep->fetch_mrlist->team: 'NO AVAILABLE DATA'}}</td>
                 <td>{{!empty($d->fetch_patient->fetch_receipt_encoded_compliance->fetch_upload->user_medrep->fetch_mrlist->new_code)?$d->fetch_patient->fetch_receipt_encoded_compliance->fetch_upload->user_medrep->fetch_mrlist->new_code: 'NO AVAILABLE DATA'}}</td>
                 <td>{{!empty($d->fetch_patient->fetch_receipt_encoded_compliance->fetch_upload->user_medrep->fetch_mrlist->pso_name)?$d->fetch_patient->fetch_receipt_encoded_compliance->fetch_upload->user_medrep->fetch_mrlist->pso_name: 'NO AVAILABLE DATA'}}</td>
                 @elseif($d->fetch_patient->fetch_receipt_encoded_compliance->fetch_upload->user_medrep->user_type == 'agent')
                  @if(!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_prescribed))
                    @if($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_prescribed->brand == 'ABILIFY')
                      @if(!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_abilify))
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_abilify->etmscode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_abilify->etmscode: 'NO AVAILABLE DATA'}}</td>
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_abilify->areacode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_abilify->areacode: 'NO AVAILABLE DATA'}}</td>    
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_abilify->mrname)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_abilify->mrname: 'NO AVAILABLE DATA'}}</td>
                      @else
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->etmscode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->etmscode: 'NO AVAILABLE DATA'}}</td>
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->areacode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->areacode: 'NO AVAILABLE DATA'}}</td>    
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->mrname)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->mrname: 'NO AVAILABLE DATA'}}</td>
                      @endif
                    @elseif($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_prescribed->brand == 'AMINOLEBAN')
                      @if(!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_aminoleban))
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_aminoleban->etmscode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_aminoleban->etmscode: 'NO AVAILABLE DATA'}}</td>
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_aminoleban->areacode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_aminoleban->areacode: 'NO AVAILABLE DATA'}}</td>    
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_aminoleban->mrname)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_aminoleban->mrname: 'NO AVAILABLE DATA'}}</td>
                      @else
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->etmscode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->etmscode: 'NO AVAILABLE DATA'}}</td>
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->areacode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->areacode: 'NO AVAILABLE DATA'}}</td>    
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->mrname)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->mrname: 'NO AVAILABLE DATA'}}</td>
                      @endif
                    @else
                      @if(!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_pletaal))
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_pletaal->etmscode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_pletaal->etmscode: 'NO AVAILABLE DATA'}}</td>
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_pletaal->areacode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_pletaal->areacode: 'NO AVAILABLE DATA'}}</td>    
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_pletaal->mrname)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor_pletaal->mrname: 'NO AVAILABLE DATA'}}</td>
                      @else
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->etmscode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->etmscode: 'NO AVAILABLE DATA'}}</td>
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->areacode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->areacode: 'NO AVAILABLE DATA'}}</td>    
                      <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->mrname)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->mrname: 'NO AVAILABLE DATA'}}</td>
                      @endif
                    @endif
                  @else
                    <td>No AVAILABLE DATA</td>   
                    <td>NO AVAILABLE DATA</td>
                    <td>NO AVAILABLE DATA</td>
                  @endif
                 @else
                 <td>{{!empty($d->fetch_patient->patient_tagging_team )? $d->fetch_patient->patient_tagging_team: 'NO AVAILABLE DATA'}} </td>
                 <td>{{!empty($d->fetch_patient->patient_tagging_area_code )? $d->fetch_patient->patient_tagging_area_code: 'NO AVAILABLE DATA'}}</td>
                 <td>{{!empty($d->fetch_patient->patient_mr_name )? $d->fetch_patient->patient_mr_name: 'NO AVAILABLE DATA'}}</td>
                 @endif
               @elseif((!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->doctors_id) and $d->fetch_patient->fetch_sku_vol_ref_compliance->doctors_id != '0'))
                 <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->etmscode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->etmscode: 'NO AVAILABLE DATA'}}</td> 
                 <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->areacode)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->areacode: 'NO AVAILABLE DATA'}}</td>    
                 <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->mrname)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->mrname: 'NO AVAILABLE DATA'}}</td> 
               @elseif(!empty($d->fetch_patient->patient_mr_id))
                 <td>{{!empty($d->fetch_patient->patient_tagging_team)?$d->fetch_patient->patient_tagging_team: 'NO AVAILABLE DATA'}}</td>
                 <td>{{!empty($d->fetch_patient->patient_tagging_area_code)?$d->fetch_patient->patient_tagging_area_code: 'NO AVAILABLE DATA'}}</td>
                 <td>{{!empty($d->fetch_patient->patient_mr_name)?$d->fetch_patient->patient_mr_name: 'NO AVAILABLE DATA'}}</td>
               @else
                 <td>{{!empty($d->fetch_patient->fetch_mr->team)?$d->fetch_patient->fetch_mr->team: 'NO AVAILABLE DATA'}}</td>
                 <td>{{!empty($d->fetch_patient->fetch_mr->area_code)?$d->fetch_patient->fetch_mr->area_code: 'NO AVAILABLE DATA'}}</td>
                 <td>{{!empty($d->fetch_patient->fetch_mr->mr_name)?$d->fetch_patient->fetch_mr->mr_name: 'NO AVAILABLE DATA'}}</td>
               @endif
            @endif
            @endif


            


            @if(!empty($d->fetch_old))
              @if($d->fetch_old->enrollment_type == 1)
                <td>RETRIEVAL</td>
                @elseif($d->fetch_old->enrollment_type == 2)
                <td>VOLUNTARY</td>
                @else
                <td>REFERRAL</td>
              @endif
            @elseif(!empty($d->fetch_patient))
            @if($d->fetch_patient->patient_type == 'Retrieval')
              <td>RETRIEVAL</td>
            @else
             @if(!empty($d->fetch_patient->fetch_purchased_compliance->fetch_upload->user_medrep))
              @if($d->fetch_patient->fetch_purchased_compliance->fetch_upload->user_medrep->user_type == 'medrep')
                <td>REFERRAL</td>
              @elseif($d->fetch_patient->fetch_purchased_compliance->fetch_upload->user_medrep->user_type == 'agent')
                <td>VOLUNTARY</td>
              @else
                <td>VOLUNTARY</td>
              @endif
             @else
             <td>VOLUNTARY</td>
             @endif
            @endif
            @endif
            @if(!empty($d->fetch_old->created_at))
              <td>{{!empty(date('m/d/Y', strtotime($d->fetch_old->created_at)))?date('m/d/Y', strtotime($d->fetch_old->created_at)): 'NO AVAILABLE DATA'}}</td>
            @else
              <td>{{!empty(date('m/d/Y', strtotime($d->fetch_patient->created_at)))?date('m/d/Y', strtotime($d->fetch_patient->created_at)): 'NO AVAILABLE DATA'}}</td>
            @endif 
            @if(count($d->fetch_patient->fetch_compliance_patient_count) == 1)
            <td>NEW</td>
            @else
            <td>OLD</td>
            @endif
            @if(!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->doctors_id) and $d->fetch_patient->fetch_sku_vol_ref_compliance->doctors_id != '0')
            <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->doctorid)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->doctorid: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->mdname)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->mdname: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->hospital)?$d->fetch_patient->fetch_sku_vol_ref_compliance->fetch_doctor->hospital: 'NO AVAILABLE DATA'}}</td>
            @elseif($d->fetch_patient->doctor_wont_disclose == 'on' or $d->fetch_patient->doctor_wont_disclose == 'true')
              @if(!empty($d->fetch_old->fetch_doctors->fetch_mdlist))
              <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->doctorid)?$d->fetch_old->fetch_doctors->fetch_mdlist->doctorid: 'NO AVAILABLE DATA'}}</td>
              <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->mdname)?$d->fetch_old->fetch_doctors->fetch_mdlist->mdname: 'NO AVAILABLE DATA'}}</td>
              <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->hospital)?$d->fetch_old->fetch_doctors->fetch_mdlist->hospital: 'NO AVAILABLE DATA'}}</td>
              @elseif(!empty($d->fetch_old->fetch_doctors->fetch_mdlist))
              <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->doctorid)?$d->fetch_old->fetch_doctors->fetch_mdlist->doctorid: 'NO AVAILABLE DATA'}}</td>
              <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->mdname)?$d->fetch_old->fetch_doctors->fetch_mdlist->mdname: 'NO AVAILABLE DATA'}}</td>
              <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->hospital)?$d->fetch_old->fetch_doctors->fetch_mdlist->hospital: 'NO AVAILABLE DATA'}}</td>
              @elseif(!empty($d->fetch_old->fetch_doctors->doctorcode))
              <td>{{!empty($d->fetch_old->fetch_doctors->doctorcode)?$d->fetch_old->fetch_doctors->doctorcode: 'NO AVAILABLE DATA'}}</td>
              <td>{{!empty($d->fetch_old->fetch_doctors->doctorname)?$d->fetch_old->fetch_doctors->doctorname: 'NO AVAILABLE DATA'}}</td>
              <td>{{!empty($d->fetch_old->fetch_doctors->hospital)?$d->fetch_old->fetch_doctors->hospital: 'NO AVAILABLE DATA'}}</td>
              @elseif(!empty($d->fetch_patient->patient_tagging_doctorid))
              <td>{{!empty($d->fetch_patient->patient_tagging_doctorid)?$d->fetch_patient->patient_tagging_doctorid: 'NO AVAILABLE DATA'}}</td>
              <td>{{!empty($d->fetch_patient->patient_tagging_doctor_name)?$d->fetch_patient->patient_tagging_doctor_name: 'NO AVAILABLE DATA'}}</td>
              <td>{{!empty($d->fetch_patient->patient_tagging_hospital)?$d->fetch_patient->patient_tagging_hospital: 'NO AVAILABLE DATA'}}</td>
              @else
              <td>Px Won't Disclose</td>                
              <td>Px Won't Disclose</td>
              <td>Px Won't Disclose</td>
              @endif
            @elseif(!empty($d->fetch_old->fetch_doctors->fetch_mdlist))
            <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->doctorid)?$d->fetch_old->fetch_doctors->fetch_mdlist->doctorid: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->mdname)?$d->fetch_old->fetch_doctors->fetch_mdlist->mdname: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_old->fetch_doctors->fetch_mdlist->hospital)?$d->fetch_old->fetch_doctors->fetch_mdlist->hospital: 'NO AVAILABLE DATA'}}</td>
            @elseif(!empty($d->fetch_old->fetch_doctors->doctorcode))
            <td>{{!empty($d->fetch_old->fetch_doctors->doctorcode)?$d->fetch_old->fetch_doctors->doctorcode: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_old->fetch_doctors->doctorname)?$d->fetch_old->fetch_doctors->doctorname: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_old->fetch_doctors->hospital)?$d->fetch_old->fetch_doctors->hospital: 'NO AVAILABLE DATA'}}</td>
            @else
            <td>{{!empty($d->fetch_patient->patient_tagging_doctorid)?$d->fetch_patient->patient_tagging_doctorid: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->patient_tagging_doctor_name)?$d->fetch_patient->patient_tagging_doctor_name: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patient->patient_tagging_hospital)?$d->fetch_patient->patient_tagging_hospital: 'NO AVAILABLE DATA'}}</td>
            @endif
            @if(!empty($d->fetch_old->fetch_product))
            <td>{{!empty($d->fetch_old->fetch_product->brandname)?$d->fetch_old->fetch_product->brandname: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_old->fetch_sku_compliance->skuname)?$d->fetch_old->fetch_sku_compliance->skuname: 'NO AVAILABLE DATA'}}</td>
            @else
            <td>{{!empty($d->fetch_patients_medinfo->fetch_prescribed->brand)?$d->fetch_patients_medinfo->fetch_prescribed->brand: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->fetch_patients_medinfo->fetch_prescribed->skuname)?$d->fetch_patients_medinfo->fetch_prescribed->skuname: 'NO AVAILABLE DATA'}}</td>
            @endif
            @if(!empty($d->fetch_old->quantity_per_day))
              @if($d->fetch_old->quantity_per_day != -1)
              <td>{{!empty($d->fetch_old->quantity_per_day)?$d->fetch_old->quantity_per_day: 'NO AVAILABLE DATA'}}</td>
              @else
              <td>NO AVAILABLE DATA</td>
              @endif
            @else
              @if(!empty($d->fetch_patient->fetch_sku_compliance))
                @if($d->fetch_patient->fetch_sku_compliance->no_of_tabs_prescribe != 0 and empty($d->fetch_patient->fetch_sku_compliance->tabs_prescribe_remarks))
                <td>{{!empty($d->fetch_patient->fetch_sku_compliance->no_of_tabs_prescribe)?$d->fetch_patient->fetch_sku_compliance->no_of_tabs_prescribe: ''}}</td>
                @elseif($d->fetch_patient->fetch_sku_compliance->no_of_tabs_prescribe == 0 and !empty($d->fetch_patient->fetch_sku_compliance->tabs_prescribe_remarks))
                <td>{{!empty($d->fetch_patient->fetch_sku_compliance->tabs_prescribe_remarks)?$d->fetch_patient->fetch_sku_compliance->tabs_prescribe_remarks: ''}}</td>
                @else
                <td>NO AVAILABLE DATA</td>
                @endif
              @else
                <td>NO AVAILABLE DATA</td>
              @endif
            @endif



            @if(!empty($d->fetch_old->prescribedays))

              @if($d->fetch_old->prescribedays != -1)
              <td>{{!empty($d->fetch_old->prescribedays)?$d->fetch_old->prescribedays: 'NO AVAILABLE DATA'}}</td>
              @else
              <td>NO AVAILABLE DATA</td>
              @endif

            @elseif(!empty($d->fetch_patient->fetch_sku_compliance))

              @if($d->fetch_patient->fetch_sku_compliance->no_of_days_prescribe != 0 and empty($d->fetch_patient->fetch_sku_compliance->days_prescribe_remarks))
              <td>{{!empty($d->fetch_patient->fetch_sku_compliance->no_of_days_prescribe)?$d->fetch_patient->fetch_sku_compliance->no_of_days_prescribe: ''}}</td>
              @elseif($d->fetch_patient->fetch_sku_compliance->no_of_days_prescribe == 0 and !empty($d->fetch_patient->fetch_sku_compliance->tabs_prescribe_remarks))
              <td>{{!empty($d->fetch_patient->fetch_sku_compliance->tabs_prescribe_remarks)?$d->fetch_patient->fetch_sku_compliance->tabs_prescribe_remarks: ''}}</td>
              @else
              <td>NO AVAILABLE DATA</td>
              @endif

			      @else

            <td>NO AVAILABLE DATA</td>   

            @endif		
            


            @if(!empty($d->fetch_patient->fetch_purchased_compliance))
              <td>YES</td>	
            @elseif(!empty($d->fetch_compliance_reason))
              @if($d->fetch_compliance_reason->name == 'Switched Medication' or $d->fetch_compliance_reason->name == 'Still On Medication' or $d->fetch_compliance_reason->name == 'Finished Medication' or $d->fetch_compliance_reason->name == 'Stopped Medication')
              <td>YES</td>
              @else
              <td>NO AVAILABLE DATA</td>
              @endif
            @elseif(!empty($d->fetch_patient->fetch_sku_compliance))
            <td>{{!empty($d->fetch_patient->fetch_sku_compliance->purchased)?strtoupper($d->fetch_patient->fetch_sku_compliance->purchased): 'NO AVAILABLE DATA'}}</td>
            @else
            <td>NO AVAILABLE DATA</td>
            @endif

            @if(!empty($d->fetch_patient->fetch_firstredeem))
            <td>{{!empty(date('m/d/Y', strtotime($d->fetch_patient->fetch_firstredeem->etimestamp)))?date('m/d/Y', strtotime($d->fetch_patient->fetch_firstredeem->etimestamp)): 'NO AVAILABLE DATA'}}</td>
            @else
            <td>NO AVAILABLE DATA</td>
            @endif
            @if(!empty($d->fetch_old->fetch_product))
            <td>{{!empty($d->fetch_old->fetch_sku_compliance->skuname)?$d->fetch_old->fetch_sku_compliance->skuname: 'NO AVAILABLE DATA'}}</td>
            @else
            <td>{{!empty($d->fetch_patients_medinfo->fetch_prescribed->skuname)?$d->fetch_patients_medinfo->fetch_prescribed->skuname: 'NO AVAILABLE DATA'}}</td>
            @endif
            @if($d->date_start_medication == '1899-12-29' or $d->date_start_medication == '1970-01-01' or empty($d->date_start_medication))
            <td>NO AVAILABLE DATA</td>
            @else
            <td>{{!empty($d->date_start_medication)?date('m/d/Y', strtotime($d->date_start_medication)): 'NO AVAILABLE DATA'}}</td>
            @endif
            @if(!empty($d->fetch_compliance_reason))
            @if($d->fetch_compliance_reason->name == 'Still On Medication')
            <td>YES</td>
            @else
            <td>NO AVAILABLE DATA</td>
            @endif
            @else
            <td>{{!empty($d->still_medication)?$d->still_medication: 'NO AVAILABLE DATA'}}</td>
            @endif
            <td>{{!empty($d->fetch_month->name)?$d->fetch_month->name: 'NO AVAILABLE DATA'}}</td>
            @if($d->reason == '16')
            <td>OTHERS - {{!empty($d->others_reason_not_med)?$d->others_reason_not_med: 'NO AVAILABLE DATA'}}</td>
            @else
            <td>{{!empty($d->fetch_compliance_reason->name)?$d->fetch_compliance_reason->name: 'NO AVAILABLE DATA'}}</td>
            @endif
            @if($d->still_medication == 'No')
            <td>{{!empty($d->reason_not_med)?$d->reason_not_med: 'NO AVAILABLE DATA'}}</td>
            @elseif(empty($d->still_medication) and !empty($d->fetch_compliance_reason) and $d->fetch_compliance_reason->name == 'Switched Medication')
            <td>{{!empty($d->medication)?$d->medication: 'NO AVAILABLE DATA'}}</td>
            @else
            <td>NO AVAILABLE DATA</td>
            @endif
            <td>{{!empty($d->medication)?$d->medication: 'NO AVAILABLE DATA'}}</td>
            <td>{{!empty($d->callnotes)?$d->callnotes: 'NO AVAILABLE DATA'}}</td>

            @else
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            @endif
          </tr>
          @endforeach
        </tbody>
        </table>
        </div>

    </div>
</div>
      
      





@endsection 

@section('footer-scripts')
<script>
$(document).ready(function() {

var today = new Date();
var dd = ("0" + (today.getDate())).slice(-2);
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();

var table = $('#daily').DataTable({
  "ordering": false,
  "scrollX": true,
  "search": {
  "caseInsensitive": false
  },
  dom: 'Blfrtip',
  buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: 'Daily Call Compliance Report '+yyyy+mm+dd,
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            'colvis'
        ]
});

});
</script>
@endsection

