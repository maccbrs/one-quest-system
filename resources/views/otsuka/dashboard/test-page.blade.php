@extends('otsuka.master')

@section('Test')
Dashboard
@endsection

@section('content')
<button type="button" onclick="loadXMLDoc()">Change Content</button>

<input type="text" name="" id="test" value="" disabled>

{{$url}}

@endsection 

@section('footer-scripts')
<script>
function loadXMLDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("test").innerHTML =
      this.responseText;
    }
  };
  xhttp.open("GET", "https://lc-txtconnect5.globe.com.ph/sms?token={{$token->token}}&txtid=onequest&recipients=+639277705441&message=test+message", true);

  //https://lc-txtconnect5.globe.com.ph/login?username=onequest&password=3dc2d8f42470f4a22018a9d84e48837b047d09bb7ffddd8caf07d4aedb54faf2&app_id=oqs
  xhttp.send();
}
</script>
@endsection

