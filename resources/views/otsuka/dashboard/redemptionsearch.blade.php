<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type: application/json');
 $sku_array = array();
	$sku_array['1'] = 'ABILIFY 10 mg ODT tablet';
	$sku_array['2'] = 'ABILIFY 10 mg tablet';
	$sku_array['3'] = 'ABILIFY 15 mg ODT tablet';
	$sku_array['4'] = 'ABILIFY 15 mg tablet';
	$sku_array['5'] = 'ABILIFY 5 mg tablet';
	$sku_array['6'] = 'ABILIFY Discmelt 10 mg Compliance Pack';
	$sku_array['7'] = 'ABILIFY Discmelt 15 mg Compliance Pack';
	$sku_array['8'] = 'ABILIFY Oral Solution 1mg/ml 150ml';
	$sku_array['9'] = 'AMINOLEBAN COMPLIANCE PACK (3+1)';
	$sku_array['10'] = 'AMINOLEBAN INJ 500mL';
	$sku_array['11'] = 'AMINOLEBAN ORAL 50g';
	$sku_array['12'] = 'LORELCO 250mg tablets';
	$sku_array['13'] = 'MEPTIN 25 mcg tablet';
	$sku_array['14'] = 'MEPTIN 50 mcg tablet';
	$sku_array['15'] = 'MEPTIN SWINGHALER 10mcg/puff';
	$sku_array['16'] = 'MEPTIN SYRUP 60mL';
	$sku_array['17'] = 'MIKELAN 5mg tablet';
	$sku_array['18'] = 'MUCOSTA 100mg Tab';
	$sku_array['19'] = 'OBUCORT SWINGHALER 200mcg/puff';
	$sku_array['20'] = 'PLETAAL 100 mg tablet';
	$sku_array['21'] = 'PLETAAL 50 mg tablet';
	$sku_array['22'] = 'PLETAAL Powder 100mg .5grams';
	$sku_array['23'] = 'PLETAAL Powder 50mg .25grams';
	$sku_array['24'] = 'Pletaal tablet 100mg Compliance pack';
	$sku_array['25'] = 'SAMSCA 15mg';
	$sku_array['26'] = 'UKNWN PROD';
	$sku_array['27'] = 'SAMSCA-Urinal Male';
	$sku_array['28'] = 'Envelop';
	$sku_array['29'] = 'Sticker Paper';
	$sku_array['30'] = 'SAMSCA-Leaflet';
	$sku_array['31'] = 'Permanent Card';
	$sku_array['32'] = 'Welcome Letter';
	$sku_array['33'] = 'SAMSCA-Urinal Female';
	$sku_array['34'] = 'Newsletter 1Q';
	$sku_array['35'] = 'Newsletter 2Q';
	$sku_array['36'] = 'Newsletter 3Q';
	$sku_array['37'] = 'Newsletter 4Q';
?>




<form>
				<?php if (isset($searching) && (strtolower($searching) == 'patient')) {?>			
							
					<h4 class="modal-title custom_align"><?php echo "" ; ?></h4>
          
				
							   <table  class="datatabledraw table table-bordered table-hover table-responsive" style="font-size:10px" width="100%">
									<thead>
										<tr>
											<th>Action</th>
											<th>Remarks</th>
											<th>Kit No.</th>
											<th>Patient Code</th>
											<th>LastName</th>
											<th>FirstName</th>
											<th>MiddleName</th>
											<th>NickName</th>
											<th>Contacts</th>
											<th>Birth Date</th>
											<th>Gender</th>
											<th>Address</th>
											
										</tr>
									</thead>
								
									 <tbody>
										  @foreach($Queue as $patient)
											@if($patient->tag_deleted == '0')
										  <tr>
												<td><a class="btn btn-info btn-sm use_patients" 
												onclick="saveRedemptions_Patient('{{$patient->id}}');">use</a></td>
												<td >{{$patient->remarks}}</td>
												<td nowrap>{{$patient->patient_kit_number}}</td>
												<td nowrap>{{$patient->patient_code}}</td>
												<td nowrap>{{$patient->patient_lastname}}</td>
												<td nowrap>{{$patient->patient_firstname}}</td>
												<td nowrap>{{$patient->patient_middlename}}</td>
												<td nowrap>{{$patient->nickname}}</td>
												<td nowrap>{{$patient->mobile_number}} / {{$patient->mobile_number_2}} / {{$patient->phone_number}}</td>
												<td nowrap>{{$patient->birth_date}}</td>
												<td nowrap>{{$patient->gender}}</td>
												<td nowrap>{{$patient->address}}</td>
												
										  </tr>
										  @endif
										  @endforeach
										</tbody>
									  
								
							</table>		

					<?php }	?>
					
					<?php if (isset($searching) && (strtolower($searching) == 'official_reciept')) {?>			
							
					<h4 class="modal-title custom_align"><?php echo "" ; ?></h4>
          
				
							   <table  class="datatabledraw table table-bordered table-hover table-responsive"  width="100%">
									<thead>
										<tr>
											<th>#</th>
											<th>Sys Gen #</th>
											<th>Or NUmber</th>
											<th>Or Date</th>
											<th>Patient_Code</th>
											<th>List</th>
											<th>Images</th>
											
											
										</tr>
									</thead>
								<tbody>
									 <tbody>
									 
										  @foreach($Queue as $Or_info)
										  <tr>
												<td><a class="btn-xs btn-info" 
												onclick="
												$('#or_no').val('{{$Or_info->or_number}}');
												$('#or_date').val('{{$Or_info->or_date}}');
												$('#doctor_name').val('test');
												
												$('#hospital_name').val('test');">use</button></td>
												<td nowrap>{{$Or_info->id}}</td>
												<td nowrap>{{$Or_info->or_number}}</td>
												<td nowrap>{{$Or_info->or_date}}</td>
												<td nowrap>{{$Or_info->patient_code}}</td>
												<td >
													
													<?php foreach($Or_info->fetch_encoded_dtl as $key => $val)
													{
														//echo '( ' . $val->qty . ' ) '  . $val->sku . '<br/>' ;
															echo "(" . $val->qty . ")" ;
															if (array_key_exists($val->sku,$sku_array)) {
																echo $sku_array[$val->sku] . '<br/>';
															}
															else{
															echo "UNKNOW_SKU(" . $val->sku . ")<br/>";
															}
														
													}?>
											
												
												</td>
												<td nowrap><a href="#" onclick="popupwindow('{{$pic . '/' . $Or_info->file_path}}');">receipt Image</a></td>
												
												
										  </tr>
										  @endforeach
										</tbody>
									  
								
							</table>	

							

					<?php }	?>
                 
	 <script>
	 
		//  alert("test");
		$(".datatabledraw").DataTable({}).draw();
		
		
		function myscript() {
			
			//alert('yesy');
		}
	</script>
	

