@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
  text-align: center;
}

</style>



        <!-- Main content -->
<div class="panel panel-default">
  <div class="panel-heading"><b>MTD Retrieval Raw Data Report {{date('Ymd')}}</b></div>
  <div class="panel-body">

      <div class="col-md-12">
        <table class="display nowrap compact datatable">
          <thead>
            <tr>
              <th>Patient ID</th>
              <th>Patient Kit #</th>
              <th>Encoded by</th>
              <th>Patient Name</th>
              <th>Month Uploaded</th>
              <th>Date Uploaded</th>
              <th>Month of Enrollment</th>
              <th>Date of Enrollment</th>
              <th>Platform</th>
              <th>(Tagging) Team</th>
              <th>(Tagging) Area Code</th>
              <th>(Tagging) MR Name</th>
              <th>Patient Signature (Px Consent)</th>
              <th>(Call Attempt) Date of 1st Call</th>
              <th>(Call Attempt) Date of 2nd Call</th>
              <th>(Call Attempt) Date of 3rd Call</th>
              <th>(Call Attempt) Date of 4th Call</th>
              <th>(Call Attempt) Date of 5th Call</th>
              <th>Final Outcome of Call Verification (Last Call)</th>
<!--               <th>CMID</th> -->
              <th>Enrollment Type</th>
              <th>MD Name</th>
              <th>Hospital/Institution Name</th>
              <th>MD Class</th>
              <th>Specialty</th>
              <th>Doctor ID</th>
              <th>Employee Doctor ID</th>
              <th>Product Prescribed</th>
              <th>SKU Prescribed</th>
              <th>No. of Tabs Prescribed</th>
              <th>No. of Days Prescribed</th>
              <th>Total No. of Tabs Prescribed</th>
              <th>Purchased (Y/N)</th>
              <th>Mobile #</th>
              <th>(Call Attempt) 1st Call Note</th>
              <th>1st Call (Agent)</th>
              <th>(Call Attempt) 2nd Call Note</th>
              <th>2nd Call (Agent)</th>
              <th>(Call Attempt) 3rd Call Note</th>
              <th>3rd Call (Agent)</th>
              <th>(Call Attempt) 4th Call Note</th>
              <th>4th Call (Agent)</th>
              <th>(Call Attempt) 5th Call Note</th>
              <th>5th Call (Agent)</th>
              </td>
            </tr>
          </thead>
        <tbody>
          @foreach($data as $d)
          <tr>
            @if(!empty($d->fetch_validated->patient_code))
            <td>{{$d->fetch_validated->patient_code}}</td>
            @else
            <td>{{'OQPXD'.sprintf('%07d',$d->id)}}</td>
            @endif
            <td>@if(!empty($d->fetch_validated->patient_kit_number)){{!empty($d->fetch_validated->patient_kit_number)?$d->fetch_validated->patient_kit_number: 'NO AVAILABLE DATA'}}@else{{!empty($d->patient_kit_number)?$d->patient_kit_number: 'NO AVAILABLE DATA'}}@endif</td>

            <td>{{!empty($d->encoded_by->name)?$d->encoded_by->name: 'NO AVAILABLE DATA'}}</td>

            <td>@if(!empty($d->fetch_validated)){{!empty(trim($d->fetch_validated->patient_fullname, '"'))?trim($d->fetch_validated->patient_fullname, '"'): 'NO AVAILABLE DATA'}}@else{{!empty($d->patient_name)?$d->patient_name: 'NO AVAILABLE DATA'}}@endif</td>

            <td>{{!empty(date('M', strtotime($d->created_at)))?date('M', strtotime($d->created_at)): 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->created_at)?date('m/d/Y', strtotime($d->created_at)): 'NO AVAILABLE DATA'}}</td>

            @if($d->patient_consent == '' or $d->remarks == 'No Px Consent')
            <td>NO AVAILABLE DATA</td>
            <td>NO AVAILABLE DATA</td>
            @else
              @if($d->enrollment_date != '' or $d->enrollment_date != NULL)
                @if($d->enrollment_date == '1970-01-01')
                <td>NO AVAILABLE DATA</td>
                <td>NO AVAILABLE DATA</td>
                @else
                <td>{{date('M', strtotime($d->enrollment_date))}}</td>
                <td>{{date('m/d/Y', strtotime($d->enrollment_date))}}</td>
                @endif
              @else
              <td>NO AVAILABLE DATA</td>
              <td>NO AVAILABLE DATA</td>
              @endif
            @endif

            <td>@if($d->fetch_user->user_type == 'medrep')OQS @elseif($d->fetch_user->user_type == 'agent')VIBER @endif</td>

            @if(!empty($d->user) and !empty($d->fetch_allocation_april))
            @if($d->user->username == $d->fetch_allocation_april->emp_code)

            @if(!empty($d->fetch_allocation_april))
            <td>{{!empty($d->fetch_allocation_april->team)?$d->fetch_allocation_april->team: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_allocation_april->area_code)?$d->fetch_allocation_april->area_code: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_allocation_april->mr_name)?$d->fetch_allocation_april->mr_name: 'NO AVAILABLE DATA'}}</td>
            @else
            <td>{{!empty($d->fetch_allocation->fetch_mrlist)?$d->fetch_allocation->team: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_allocation->area_code)?$d->fetch_allocation->area_code: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_allocation->mr_name)?$d->fetch_allocation->mr_name: 'NO AVAILABLE DATA'}}</td>
            @endif

            @else

            <td>{{!empty($d->user_medrep->fetch_mrlist->team)?$d->user_medrep->fetch_mrlist->team: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->user_medrep->fetch_mrlist->new_code)?$d->user_medrep->fetch_mrlist->new_code: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->user_medrep->fetch_mrlist->pso_name)?$d->user_medrep->fetch_mrlist->pso_name: 'NO AVAILABLE DATA'}}</td>

            @endif
            @else

            <td>{{!empty($d->user_medrep->fetch_mrlist->team)?$d->user_medrep->fetch_mrlist->team: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->user_medrep->fetch_mrlist->new_code)?$d->user_medrep->fetch_mrlist->new_code: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->user_medrep->fetch_mrlist->pso_name)?$d->user_medrep->fetch_mrlist->pso_name: 'NO AVAILABLE DATA'}}</td>

            @endif


            <td>@if($d->patient_consent == 'true' and ($d->remarks !== 'No Px Consent'))YES @elseif($d->patient_consent == '' and $d->remarks == 'No Px Consent')NO @else NO @endif</td>

            @if(empty($d->fetch_call->fetch_verification[0]->remarks) or $d->patient_consent == '' or $d->remarks == 'No Px Consent')
            <td>NO AVAILABLE DATA</td>
            @else
            <td>{{!empty(date('m/d/Y', strtotime($d->fetch_call->fetch_verification[0]->updated_at)))?date('m/d/Y', strtotime($d->fetch_call->fetch_verification[0]->updated_at)): 'NO AVAILABLE DATA'}}</td>
            @endif

            @if(empty($d->fetch_call->fetch_verification[1]->remarks) or $d->patient_consent == '' or $d->remarks == 'No Px Consent')
            <td>NO AVAILABLE DATA</td>
            @else
            <td>{{!empty(date('m/d/Y', strtotime($d->fetch_call->fetch_verification[1]->updated_at)))?date('m/d/Y', strtotime($d->fetch_call->fetch_verification[1]->updated_at)): 'NO AVAILABLE DATA'}}</td>
            @endif

            @if(empty($d->fetch_call->fetch_verification[2]->remarks) or $d->patient_consent == '' or $d->remarks == 'No Px Consent')
            <td>NO AVAILABLE DATA</td>
            @else
            <td>{{!empty(date('m/d/Y', strtotime($d->fetch_call->fetch_verification[2]->updated_at)))?date('m/d/Y', strtotime($d->fetch_call->fetch_verification[2]->updated_at)): 'NO AVAILABLE DATA'}}</td>
            @endif

            @if(empty($d->fetch_call->fetch_verification[3]->remarks) or $d->patient_consent == '' or $d->remarks == 'No Px Consent')
            <td>NO AVAILABLE DATA</td>
            @else
            <td>{{!empty(date('m/d/Y', strtotime($d->fetch_call->fetch_verification[3]->updated_at)))?date('m/d/Y', strtotime($d->fetch_call->fetch_verification[3]->updated_at)): 'NO AVAILABLE DATA'}}</td>
            @endif

            @if(empty($d->fetch_call->fetch_verification[4]->remarks) or $d->patient_consent == '' or $d->remarks == 'No Px Consent')
            <td>NO AVAILABLE DATA</td>
            @else
            <td>{{!empty(date('m/d/Y', strtotime($d->fetch_call->fetch_verification[4]->updated_at)))?date('m/d/Y', strtotime($d->fetch_call->fetch_verification[4]->updated_at)): 'NO AVAILABLE DATA'}}</td>
            @endif

            @if($d->patient_consent == '' or $d->remarks == 'No Px Consent')
            <td>NO CONSENT</td>
              @else
                @if(!empty($d->fetch_call->fetch_verification[0]->remarks) and empty($d->fetch_call->fetch_verification[1]->remarks) and empty($d->fetch_call->fetch_verification[2]->remarks) and empty($d->fetch_call->fetch_verification[3]->remarks) and empty($d->fetch_call->fetch_verification[4]->remarks))
                <td>@if($d->fetch_call->fetch_verification[0]->fetch_status->name == 'verified') NEW @else{{!empty(strtoupper($d->fetch_call->fetch_verification[0]->fetch_status->name))?strtoupper($d->fetch_call->fetch_verification[0]->fetch_status->name): 'NO AVAILABLE DATA'}}@endif</td>
                  
                  @elseif(!empty($d->fetch_call->fetch_verification[0]->remarks) and !empty($d->fetch_call->fetch_verification[1]->remarks) and empty($d->fetch_call->fetch_verification[2]->remarks) and empty($d->fetch_call->fetch_verification[3]->remarks) and empty($d->fetch_call->fetch_verification[4]->remarks))
                  <td>@if($d->fetch_call->fetch_verification[1]->fetch_status->name == 'verified') NEW @else{{!empty(strtoupper($d->fetch_call->fetch_verification[1]->fetch_status->name))?strtoupper($d->fetch_call->fetch_verification[1]->fetch_status->name): 'NO AVAILABLE DATA'}}@endif</td>
                  
                  @elseif(!empty($d->fetch_call->fetch_verification[0]->remarks) and !empty($d->fetch_call->fetch_verification[1]->remarks) and !empty($d->fetch_call->fetch_verification[2]->remarks) and empty($d->fetch_call->fetch_verification[3]->remarks) and empty($d->fetch_call->fetch_verification[4]->remarks))
                  <td>@if($d->fetch_call->fetch_verification[2]->fetch_status->name == 'verified') NEW @else{{!empty(strtoupper($d->fetch_call->fetch_verification[2]->fetch_status->name))?strtoupper($d->fetch_call->fetch_verification[2]->fetch_status->name): 'NO AVAILABLE DATA'}}@endif</td>
                  
                  @elseif(!empty($d->fetch_call->fetch_verification[0]->remarks) and !empty($d->fetch_call->fetch_verification[1]->remarks) and !empty($d->fetch_call->fetch_verification[2]->remarks) and !empty($d->fetch_call->fetch_verification[3]->remarks) and empty($d->fetch_call->fetch_verification[4]->remarks))
                  <td>@if($d->fetch_call->fetch_verification[3]->fetch_status->name == 'verified') NEW @else{{!empty(strtoupper($d->fetch_call->fetch_verification[3]->fetch_status->name))?strtoupper($d->fetch_call->fetch_verification[3]->fetch_status->name): 'NO AVAILABLE DATA'}}@endif</td>

                  @elseif(!empty($d->fetch_call->fetch_verification[0]->remarks) and !empty($d->fetch_call->fetch_verification[1]->remarks) and !empty($d->fetch_call->fetch_verification[2]->remarks) and !empty($d->fetch_call->fetch_verification[3]->remarks) and !empty($d->fetch_call->fetch_verification[4]->remarks))
                  <td>@if($d->fetch_call->fetch_verification[4]->fetch_status->name == 'verified') NEW @else{{!empty(strtoupper($d->fetch_call->fetch_verification[4]->fetch_status->name))?strtoupper($d->fetch_call->fetch_verification[4]->fetch_status->name): 'NO AVAILABLE DATA'}}@endif</td>
                  
                  @elseif(empty($d->fetch_call->fetch_verification[0]->remarks) and empty($d->fetch_call->fetch_verification[1]->remarks) and empty($d->fetch_call->fetch_verification[2]->remarks) and empty($d->fetch_call->fetch_verification[3]->remarks) and empty($d->fetch_call->fetch_verification[4]->remarks))
                  <td>NO AVAILABLE DATA</td>

                  @else
                  <td>NO AVAILABLE DATA</td>
                @endif
            @endif

<!--             <td>{{!empty($d->fetch_call->id)?$d->fetch_call->id: 'NO AVAILABLE DATA'}}</td>
 -->
            <td>RETRIEVAL</td>

            <td>@if(!empty($d->fetch_medinfo->fetch_doctor->mdname)){{!empty($d->fetch_medinfo->fetch_doctor->mdname)?$d->fetch_medinfo->fetch_doctor->mdname: 'NO AVAILABLE DATA'}} @elseif(!empty($d->fetch_mdinfO_upload->mdname)){{!empty($d->fetch_mdinfO_upload->mdname)?$d->fetch_mdinfO_upload->mdname: 'NO AVAILABLE DATA'}} @elseif(!empty($d->doctor_name)) {{!empty($d->doctor_name)?$d->doctor_name: 'NO AVAILABLE DATA'}} @else {{!empty($d->fetch_validated->doctor_name)?$d->fetch_validated->doctor_name: 'NO AVAILABLE DATA'}}  @endif</td>

            <td>@if(!empty($d->fetch_medinfo->fetch_doctor->hospital)){{!empty($d->fetch_medinfo->fetch_doctor->hospital)?$d->fetch_medinfo->fetch_doctor->hospital: 'NO AVAILABLE DATA'}} @elseif(!empty($d->fetch_mdinfO_upload->hospital)){{!empty($d->fetch_mdinfO_upload->hospital)?$d->fetch_mdinfO_upload->hospital: 'NO AVAILABLE DATA'}} @elseif(!empty($d->hospital_name)) {{!empty($d->hospital_name)?$d->hospital_name: 'NO AVAILABLE DATA'}} @else {{!empty($d->fetch_validated->hospital)?$d->fetch_validated->hospital: 'NO AVAILABLE DATA'}} @endif</td>

            <td>@if(!empty($d->fetch_medinfo->fetch_doctor->mdclass)){{!empty($d->fetch_medinfo->fetch_doctor->mdclass)?$d->fetch_medinfo->fetch_doctor->mdclass: 'NO AVAILABLE DATA'}} @elseif(!empty($d->fetch_mdinfO_upload->hospital)){{!empty($d->fetch_mdinfO_upload->mdclass)?$d->fetch_mdinfO_upload->mdclass: 'NO AVAILABLE DATA'}} @else NO AVAILABLE DATA @endif</td>

            <td>@if(!empty($d->fetch_medinfo->fetch_doctor->specialty)){{!empty($d->fetch_medinfo->fetch_doctor->specialty)?$d->fetch_medinfo->fetch_doctor->specialty: 'NO AVAILABLE DATA'}} @elseif(!empty($d->fetch_mdinfO_upload->hospital)){{!empty($d->fetch_mdinfO_upload->specialty)?$d->fetch_mdinfO_upload->specialty: 'NO AVAILABLE DATA'}} @else NO AVAILABLE DATA @endif</td>

            <td>@if(!empty($d->fetch_medinfo->fetch_doctor->doctorid)){{!empty($d->fetch_medinfo->fetch_doctor->doctorid)?$d->fetch_medinfo->fetch_doctor->doctorid: 'NO AVAILABLE DATA'}} @elseif(!empty($d->fetch_mdinfO_upload->hospital)){{!empty($d->fetch_mdinfO_upload->doctorid)?$d->fetch_mdinfO_upload->doctorid: 'NO AVAILABLE DATA'}} @else NO AVAILABLE DATA @endif</td>

            <td>@if(!empty($d->fetch_medinfo->fetch_doctor->employee)){{!empty($d->fetch_medinfo->fetch_doctor->employee)?$d->fetch_medinfo->fetch_doctor->employee: 'NO AVAILABLE DATA'}} @elseif(!empty($d->fetch_mdinfO_upload->hospital)){{!empty($d->fetch_mdinfO_upload->employee)?$d->fetch_mdinfO_upload->employee: 'NO AVAILABLE DATA'}} @else NO AVAILABLE DATA @endif</td>

            <td>{{!empty(strtoupper($d->medicine))?strtoupper($d->medicine): 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_validated->fetch_sku->fetch_prescribed->skuname)?$d->fetch_validated->fetch_sku->fetch_prescribed->skuname: 'NO AVAILABLE DATA'}}</td>

            <td>@if(!empty($d->fetch_validated->fetch_sku->no_of_tabs_prescribe)){{!empty($d->fetch_validated->fetch_sku->no_of_tabs_prescribe)?$d->fetch_validated->fetch_sku->no_of_tabs_prescribe: 'NO AVAILABLE DATA'}} @else{{!empty($d->fetch_validated->fetch_sku->tabs_prescribe_remarks)?$d->fetch_validated->fetch_sku->tabs_prescribe_remarks: 'NO AVAILABLE DATA'}} @endif</td>

            <td>@if(!empty($d->fetch_validated->fetch_sku->no_of_days_prescribe)){{!empty($d->fetch_validated->fetch_sku->no_of_days_prescribe)?$d->fetch_validated->fetch_sku->no_of_days_prescribe: 'NO AVAILABLE DATA'}} @else{{!empty($d->fetch_validated->fetch_sku->days_prescribe_remarks)?$d->fetch_validated->fetch_sku->days_prescribe_remarks: 'NO AVAILABLE DATA'}} @endif</td>

            <td>@if(!empty($d->fetch_validated->fetch_sku->total_no_of_tabs)){{!empty($d->fetch_validated->fetch_sku->total_no_of_tabs)?$d->fetch_validated->fetch_sku->total_no_of_tabs: 'NO AVAILABLE DATA'}} @else{{!empty($d->fetch_validated->fetch_sku->total_tabs_remarks)?$d->fetch_validated->fetch_sku->total_tabs_remarks: 'NO AVAILABLE DATA'}} @endif</td>

            <td>{{!empty($d->fetch_validated->fetch_sku->purchased)?$d->fetch_validated->fetch_sku->purchased: 'NO AVAILABLE DATA'}}</td>

            <td>@if(!empty($d->fetch_validated)) {{!empty($d->fetch_validated->mobile_number)?$d->fetch_validated->mobile_number: 'NO AVAILABLE DATA'}} / {{!empty($d->fetch_validated->mobile_number_2)?$d->fetch_validated->mobile_number_2: ''}} / {{!empty($d->fetch_validated->phone_no)?$d->fetch_validated->phone_no: ''}} @else {{!empty($d->mobile_number)?$d->mobile_number: 'NO AVAILABLE DATA'}} / {{!empty($d->mobile_number_2)?$d->mobile_number_2: ''}} / {{!empty($d->phone_no)?$d->phone_no: ''}} @endif</td>

            <td>{{!empty($d->fetch_call->fetch_verification[0]->callnotes)?$d->fetch_call->fetch_verification[0]->callnotes: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_call->fetch_verification[0]->fetch_name->name)?$d->fetch_call->fetch_verification[0]->fetch_name->name: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_call->fetch_verification[1]->callnotes)?$d->fetch_call->fetch_verification[1]->callnotes: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_call->fetch_verification[1]->fetch_name->name)?$d->fetch_call->fetch_verification[1]->fetch_name->name: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_call->fetch_verification[2]->callnotes)?$d->fetch_call->fetch_verification[2]->callnotes: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_call->fetch_verification[2]->fetch_name->name)?$d->fetch_call->fetch_verification[2]->fetch_name->name: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_call->fetch_verification[3]->callnotes)?$d->fetch_call->fetch_verification[3]->callnotes: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_call->fetch_verification[3]->fetch_name->name)?$d->fetch_call->fetch_verification[3]->fetch_name->name: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_call->fetch_verification[4]->callnotes)?$d->fetch_call->fetch_verification[4]->callnotes: 'NO AVAILABLE DATA'}}</td>

            <td>{{!empty($d->fetch_call->fetch_verification[4]->fetch_name->name)?$d->fetch_call->fetch_verification[4]->fetch_name->name: 'NO AVAILABLE DATA'}}</td>
          </tr>
          @endforeach

       @foreach($data2 as $d2)
       
        <tr>
         <td>{{!empty($d2->patient_code )? $d2->patient_code: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->patient_kit_number )? $d2->patient_kit_number: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->created_by )? $d2->created_by: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->patient_fullname )? $d2->patient_fullname: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty(date('M', strtotime($d2->created_at)))?date('M', strtotime($d2->created_at)): 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty(date('m/d/Y', strtotime($d2->created_at)))?date('m/d/Y', strtotime($d2->created_at)): 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty(date('M', strtotime($d2->created_at)))?date('M', strtotime($d2->created_at)): 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty(date('m/d/Y', strtotime($d2->created_at)))?date('m/d/Y', strtotime($d2->created_at)): 'NO AVAILABLE DATA'}}</td>        
         <td>OQS</td>
         @if(!empty($d2->fetch_receipt_encoded->fetch_upload->user_medrep))
           @if($d2->fetch_receipt_encoded->fetch_upload->user_medrep->user_type == 'medrep')
           <td>{{!empty($d2->fetch_receipt_encoded->fetch_upload->user_medrep->fetch_mrlist->team)?$d2->fetch_receipt_encoded->fetch_upload->user_medrep->fetch_mrlist->team: 'REFERRAL'}}</td>
           <td>{{!empty($d2->fetch_receipt_encoded->fetch_upload->user_medrep->fetch_mrlist->new_code)?$d2->fetch_receipt_encoded->fetch_upload->user_medrep->fetch_mrlist->new_code: 'REFERRAL'}}</td>
           <td>{{!empty($d2->fetch_receipt_encoded->fetch_upload->user_medrep->fetch_mrlist->pso_name)?$d2->fetch_receipt_encoded->fetch_upload->user_medrep->fetch_mrlist->pso_name: 'REFERRAL'}}</td>
           @else
           <td>{{!empty($d2->patient_tagging_team )? $d2->patient_tagging_team: 'VOLUNTARY'}} </td>
           <td>{{!empty($d2->patient_tagging_area_code )? $d2->patient_tagging_area_code: 'VOLUNTARY'}}</td>
           <td>{{!empty($d2->patient_mr_name )? $d2->patient_mr_name: 'VOLUNTARY'}}</td>
           @endif
         @else
           <td>{{!empty($d2->patient_tagging_team)?$d2->patient_tagging_team: 'VOLUNTARY'}}</td>
           <td>{{!empty($d2->patient_tagging_area_code)?$d2->patient_tagging_area_code: 'VOLUNTARY'}}</td>
           <td>{{!empty($d2->patient_mr_name)?$d2->patient_mr_name: 'VOLUNTARY'}}</td>
         @endif
         <td>YES</td>
         <td>{{!empty(date('M', strtotime($d2->created_at)))?date('m/d/Y', strtotime($d2->created_at)): 'NO AVAILABLE DATA'}}</td>
         @if(!empty($d2->fetch_receipt_encoded->fetch_upload->user_medrep))
          @if($d2->fetch_receipt_encoded->fetch_upload->user_medrep->user_type == 'medrep')
            <td>REFERRAL</td>
            <td>REFERRAL</td>
            <td>REFERRAL</td>
            <td>REFERRAL</td>
          @elseif($d2->fetch_receipt_encoded->fetch_upload->user_medrep->user_type == 'agent')
            <td>VOLUNTARY</td>
            <td>VOLUNTARY</td>
            <td>VOLUNTARY</td>
            <td>VOLUNTARY</td>
          @else
            <td>VOLUNTARY</td>
            <td>VOLUNTARY</td>
            <td>VOLUNTARY</td>
            <td>VOLUNTARY</td>
          @endif
         @else
         <td>VOLUNTARY</td> 
         <td>VOLUNTARY</td>
         <td>VOLUNTARY</td> 
         <td>VOLUNTARY</td>
         @endif
         @if(!empty($d2->fetch_receipt_encoded->fetch_upload->user_medrep))
          @if($d2->fetch_receipt_encoded->fetch_upload->user_medrep->user_type == 'medrep')
            <td>REFERRAL</td>
            <td>REFERRAL</td>
          @elseif($d2->fetch_receipt_encoded->fetch_upload->user_medrep->user_type == 'agent')
            <td>VOLUNTARY</td>
            <td>VOLUNTARY</td>
          @else
            <td>VOLUNTARY</td>
            <td>VOLUNTARY</td>
          @endif
         @else
         <td>VOLUNTARY</td> 
         <td>VOLUNTARY</td>
         @endif
         @if(!empty($d2->fetch_sku_vol_ref->doctors_id) and $d2->fetch_sku_vol_ref->doctors_id != '0')
         <td>{{!empty($d2->fetch_sku_vol_ref->fetch_doctor->mdname)?$d2->fetch_sku_vol_ref->fetch_doctor->mdname: 'NO AVAILABLE DATA'}}</td>                
         <td>{{!empty($d2->fetch_sku_vol_ref->fetch_doctor->hospital)?$d2->fetch_sku_vol_ref->fetch_doctor->hospital: 'NO AVAILABLE DATA'}}</td>    
         <td>{{!empty($d2->fetch_sku_vol_ref->fetch_doctor->mdclass)?$d2->fetch_sku_vol_ref->fetch_doctor->mdclass: 'NO AVAILABLE DATA'}}</td>                    
         <td>{{!empty($d2->fetch_sku_vol_ref->fetch_doctor->specialty)?$d2->fetch_sku_vol_ref->fetch_doctor->specialty: 'NO AVAILABLE DATA'}}</td>    
         <td>{{!empty($d2->fetch_sku_vol_ref->fetch_doctor->doctorid)?$d2->fetch_sku_vol_ref->fetch_doctor->doctorid: 'NO AVAILABLE DATA'}}</td>                
         <td>{{!empty($d2->fetch_sku_vol_ref->fetch_doctor->employee)?$d2->fetch_sku_vol_ref->fetch_doctor->employee: 'NO AVAILABLE DATA'}}</td>
         @elseif(!empty($d2->doctor_wont_disclose))  
         <td>Px Won't Disclose</td>                
         <td>Px Won't Disclose</td>
         <td>Px Won't Disclose</td>                
         <td>Px Won't Disclose</td>
         <td>Px Won't Disclose</td>                
         <td>Px Won't Disclose</td>
         @else
         <td>{{!empty($d2->patient_tagging_doctor_name )? $d2->patient_tagging_doctor_name: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->patient_tagging_hospital )? $d2->patient_tagging_hospital: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->patient_tagging_md_class )? $d2->patient_tagging_md_class: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->patient_tagging_specialty )? $d2->patient_tagging_specialty: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->patient_tagging_doctorid )? $d2->patient_tagging_doctorid: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->patient_tagging_emp_doctorid )? $d2->patient_tagging_emp_doctorid: 'NO AVAILABLE DATA'}}</td>
         @endif 
         @if(!empty($d2->fetch_sku_vol_ref->fetch_prescribed->brand))
         <td>{{!empty($d2->fetch_sku_vol_ref->fetch_prescribed->brand )? $d2->fetch_sku_vol_ref->fetch_prescribed->brand: 'NO AVAILABLE DATA'}}</td>
         @else
           @if($d2->patient_tagging_product_prescribe == '* Please Select Product')
           <td>NO AVAILABLE DATA</td>
           @else
           <td>{{!empty(strtoupper($d2->patient_tagging_product_prescribe))?strtoupper($d2->patient_tagging_product_prescribe): 'NO AVAILABLE DATA'}}</td>
           @endif
         @endif
         <td>{{!empty($d2->fetch_sku_vol_ref->fetch_prescribed->skuname )? $d2->fetch_sku_vol_ref->fetch_prescribed->skuname: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->fetch_sku_vol_ref->no_of_tabs_prescribe )? $d2->fetch_sku_vol_ref->no_of_tabs_prescribe: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->fetch_sku_vol_ref->no_of_days_prescribe )? $d2->fetch_sku_vol_ref->no_of_days_prescribe: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2->fetch_sku_vol_ref->total_no_of_tabs )? $d2->fetch_sku_vol_ref->total_no_of_tabs: 'NO AVAILABLE DATA'}}</td>
         @if(!empty($d2->fetch_receipt_encoded->fetch_upload->user_medrep))
          @if($d2->fetch_receipt_encoded->fetch_upload->user_medrep->user_type == 'medrep')
            <td>YES</td>
          @elseif($d2->fetch_receipt_encoded->fetch_upload->user_medrep->user_type == 'agent')
            <td>{{!empty($d2->fetch_sku_vol_ref->purchased )? $d2->fetch_sku_vol_ref->purchased: 'NO AVAILABLE DATA'}}</td>
          @else
            <td>{{!empty($d2->fetch_sku_vol_ref->purchased )? $d2->fetch_sku_vol_ref->purchased: 'NO AVAILABLE DATA'}}</td>
          @endif
         @else
         <td>{{!empty($d2->fetch_sku_vol_ref->purchased )? $d2->fetch_sku_vol_ref->purchased: 'NO AVAILABLE DATA'}}</td>
         @endif
         <td>{{!empty($d2->mobile_number )? $d2->mobile_number: ''}} / {{!empty($d2->mobile_number_2 )? $d2->mobile_number_2: ''}} / {{!empty($d2->phone_no )? $d2->phone_no: ''}}</td>
         <td>NO AVAILABLE DATA</td>
         <td>NO AVAILABLE DATA</td>
         <td>NO AVAILABLE DATA</td>
         <td>NO AVAILABLE DATA</td>
         <td>NO AVAILABLE DATA</td>
         <td>NO AVAILABLE DATA</td>
         <td>NO AVAILABLE DATA</td>
         <td>NO AVAILABLE DATA</td>
         <td>NO AVAILABLE DATA</td>
         <td>NO AVAILABLE DATA</td>
        {{--
        <td>{{!empty($d2->patient_code )? $d2->patient_code: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
        
        
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
         <td>{{!empty($d2 )? $d2: 'NO AVAILABLE DATA'}}</td>
        --}}  
         
         
         
      </tr>
      @endforeach 
      
        </tbody>
        </table>
  
     
    
        </div>

    </div>
</div>

@endsection 

@section('footer-scripts')
<script>
function popupwindow(url) {
  
  title = "Images";
  w = 500;
  h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

$(document).ready(function() {


var today = new Date();
var dd = today.getDate();
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();


$(".datatable").DataTable({
  "ordering": false,
  "scrollX": true,
  dom: 'Blfrtip',
  buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: 'EOM Retrieval Raw Data Report '+yyyy+mm+dd,
                exportOptions: {
                columns: [0,1,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
                }
            },
            'colvis'
        ]
});

});
</script>
@endsection

