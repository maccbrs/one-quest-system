@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
  text-align: center;
}

</style>



        <!-- Main content -->
<div class="panel panel-default">
  <div class="panel-heading"><b>EOM Reports</b></div>
  <div class="panel-body">
<form method="get" action="{{route('otsuka.generate-report-eom')}}">
<div class="col-md-12">

  <div class="col-md-6">
<div class="form-group">
    <label>Select Month:</label>
    <select class="form-control input-sm" name="month" required="">
      <option readonly>* Please Select Month</option>
      @foreach($list_months as $lm)
      <option value="{{$lm->month}}">{{$lm->name}}</option>
      @endforeach
    </select>
</div>
</div>

<div class="col-md-6">
<div class="form-group">
    <label>Select Report:</label>
    <select class="form-control input-sm" name="report" required="">
      <option readonly>* Please Select Report</option>
      @foreach($list_reports as $lr)
      <option value="{{$lr->id}}">{{$lr->name}}</option>
      @endforeach
    </select>
</div>
  </div>
</div>

<input type="submit" class="btn btn-primary pull-right" value="Submit">

</form>
    </div>
</div>

@endsection 

@section('footer-scripts')
<script>

function popupwindow(url) {
  
  title = "Images";
  w = 500;
  h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

$(document).ready(function() {

var today = new Date();
var dd = today.getDate();
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();


$(".datatable").DataTable({
  "ordering": false,
  "scrollX": true,
  dom: 'Blfrtip',
  buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: 'MTD Retrieval Raw Data Report '+yyyy+mm+dd,
                exportOptions: {
                columns: [0,1,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
                }
            },
            'colvis'
        ]
});

});

$(document).ready(function() {

var today = new Date();
var dd = today.getDate();
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();

$('#daily').DataTable({
  "ordering": false,
  "scrollX": true,
  dom: 'Blfrtip',
  buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: 'Daily Call Raw Data Report '+yyyy+mm+dd,
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            'colvis'
        ]
});

});
</script>
@endsection

