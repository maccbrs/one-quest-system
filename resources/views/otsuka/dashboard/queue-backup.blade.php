@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
    <?php $baseurl = URL::asset('/'); ?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
.viber {
	color: 	white !important;
	
	background-color: #8f5db7 !important
}

.red {
	color: white !important;
	
	background-color: #d9534f!important
}
.yellow {
	color: black !important;
	background-color: yellow !important;
}
.orange {
	color: white !important;
	background-color: orange !important;
}
.default-color {
	color:#000000;
}

  input {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
  }

 input.noborder {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 0;
  }

  div.border {
      border: 1px solid black;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
  }

  div.padding {
      padding-top: 40px;
      padding-right: 65px;
      padding-bottom: 40px;
      padding-left: 60px;
  }

  h2.space {
    line-height: .25;
  }

  p.ind {
    text-indent: 50px;
    line-height: .1;
  }


</style>


<div class="col-md-12" style="background-color:#0275d8;color:#ffffff" >
			<hr/>
			<h3>Upload</h3>
			
			<form method="post" id="encode_form" action="{{route('otsuka.encode')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
			<table id="queue" class="display compact table-bordered upload-datatable table-hover" cellspacing="0" width="100%" style="font-size:10px">
					<thead>
						<tr>
							<th>ID</th>
							<th>Type</th>
							<th>Medicine</th>
							<th>Created At</th>
							<th>Patient Name</th>
							<th>Uploaded By</th>
							<th>Claimed</th>
							@if(Auth::user()->user_level == 2)<th>Status</th>@endif
							<th>Encode</th>
							@if(Auth::user()->user_level == 2)<th>Hide</th>@endif
						</tr>
					</thead>
					<tbody>
					  @if($Queue->count())
					  @foreach($Queue as $qvc)
					  @if($qvc->created_at->addHour(1) <= $tn and $qvc->viber_queue == '0')
					  <tr class="red" id="upload_queue_id_{{$qvc->id}}">
					   @elseif($qvc->created_at->addMinutes(45) <= $tn and $qvc->viber_queue == '0')
					  <tr class="orange" id="upload_queue_id_{{$qvc->id}}">	
					   @elseif($qvc->created_at->addMinutes(30) <= $tn and $qvc->viber_queue == '0')
					  <tr class="yellow" id="upload_queue_id_{{$qvc->id}}">
					  @elseif($qvc->viber_queue == '1')
					  <tr class="viber" id="upload_queue_id_{{$qvc->id}}">
					  @elseif($qvc->created_at->addMinutes(30) > $tn and $qvc->viber_queue == '0')
					  <tr class="default-color" id="upload_queue_id_{{$qvc->id}}">
					  @endif
					  <td>{{$qvc->id}}</td>
					  <td>{{$qvc->type}}</td>
					  <td>{{$qvc->medicine}}</td>
					  <td>{{$qvc->created_at}}</td>
					  <td>{{$qvc->patient_name}}</td>
					  <td>{{!empty($qvc->user->name)?$qvc->user->name: 'N/A'}}</td>
					  @if($qvc->claimed == 0)<td><b>N/A</b></td>@endif
					  @if($qvc->claimed == 1)<td><b>{{!empty($qvc->encoded_by->name)?$qvc->encoded_by->name: 'N/A'}}</b></td>@endif
					  @if(Auth::user()->user_level == 2)
					  <td>							
							{{$qvc->fetch_trail_status->display_value}}
					  </td>
					  @endif	
					  <td><button type="submit" name="encode" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Encode</button></td>
					  @if(Auth::user()->user_level == 2)<td><a href="#" onclick="hideupload('{{$qvc->id}}')"  value="{{$qvc->id}}" class="btn btn-xs btn-primary">Hide</a></td>@endif
					  </tr>
					  <?php /*
					  @elseif($qvc->created_at->addMinutes(45) <= $tn and $qvc->viber_queue == '0')
					  <tr class="orange" id="upload_queue_id_{{$qvc->id}}">
					  <td>{{$qvc->connector_id}}</td>
					  <td>{{$qvc->type}}</td>
					  <td>{{$qvc->medicine}}</td>
					  <td>{{$qvc->created_at}}</td>
					  @if($qvc->claimed == 0)<td><b>No</b></td>@endif
					  @if($qvc->claimed == 1)<td><b>Yes</b></td>@endif
					  @if(Auth::user()->user_level == 2)<td>Status</td>@endif	
					  <td><button type="submit" name="encode" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Encode</button></td>
					  @if(Auth::user()->user_level == 2)<form method="post"  action="{{route('otsuka.disable')}}" enctype="multipart/form-data">{{ csrf_field() }} <td><button type="submit" name="active" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Hide</button></form></td>@endif
					  </tr>
					  @elseif($qvc->created_at->addMinutes(30) <= $tn and $qvc->viber_queue == '0')
					  <tr class="yellow" id="upload_queue_id_{{$qvc->id}}">
					  <td>{{$qvc->connector_id}}</td>
					  <td>{{$qvc->type}}</td>
					  <td>{{$qvc->medicine}}</td>
					  <td>{{$qvc->created_at}}</td>
					  @if($qvc->claimed == 0)<td><b>No</b></td>@endif
					  @if($qvc->claimed == 1)<td><b>Yes</b></td>@endif
					  @if(Auth::user()->user_level == 2)<td>Status</td>	@endif
					  <td><button type="submit" name="encode" value="{{$qvc->id}}" class="btn btn-xs  btn-primary">Encode</button></td>
					  @if(Auth::user()->user_level == 2)<form method="post" action="{{route('otsuka.disable')}}" enctype="multipart/form-data">{{ csrf_field() }} <td><button type="submit" name="active" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Hide</button></form></td>@endif
					  </tr>
					  @elseif($qvc->viber_queue == '1')
					  <tr class="viber" id="upload_queue_id_{{$qvc->id}}">
					  <td>{{$qvc->connector_id}}</td>
					  <td>{{$qvc->type}}</td>
					  <td>{{$qvc->medicine}}</td>
					  <td>{{$qvc->created_at}}</td>
					  @if($qvc->claimed == 0)<td><b>No</b></td>@endif
					  @if($qvc->claimed == 1)<td><b>Yes</b></td>@endif
					  @if(Auth::user()->user_level == 2)<td>Status</td>@endif	
					  <td><button type="submit" name="encode" value="{{$qvc->id}}" class="btn btn-xs  btn-primary">Encode</button></td>
					  @if(Auth::user()->user_level == 2)<form method="post" action="{{route('otsuka.disable')}}" enctype="multipart/form-data">{{ csrf_field() }} <td><button type="submit" name="active" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Hide</button></form></td>@endif
					  </tr>
					  @elseif($qvc->created_at->addMinutes(30) > $tn and $qvc->viber_queue == '0')
					  <tr class="default-color" id="upload_queue_id_{{$qvc->id}}">
					  <td>{{$qvc->connector_id}}</td>
					  <td>{{$qvc->type}}</td>
					  <td>{{$qvc->medicine}}</td>
					  <td>{{$qvc->created_at}}</td>
					  @if($qvc->claimed == 0)<td><b>No</b></td>@endif
					  @if($qvc->claimed == 1)<td><b>Yes</b></td>@endif
					  @if(Auth::user()->user_level == 2)<td>Status</td>@endif	
					  <td><button type="submit" name="encode" value="{{$qvc->id}}" class="btn btn-xs  btn-primary">Encode</button></td>
					  @if(Auth::user()->user_level == 2)<form method="post" action="{{route('otsuka.disable')}}" enctype="multipart/form-data">{{ csrf_field() }} <td><button type="submit" name="active" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Hide</button></form></td>@endif
					  </tr>
					  @endif
					  */ ?>
					  @endforeach
					  @endif
					</tbody>
			</table>
				</form>
</div> <!-- ENCODE QUEUEING -->				


<div class="col-md-12" style="background-color:#5cb85c!important;color:#ffffff;margin-top:20px" >
<hr/>
			<h3>Verification</h3>
			
			<form method="post" id="encode_form" action="{{route('otsuka.verify')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
			<table id="verication-queue" class="display compact datatable table-bordered table-hover" cellspacing="0" width="100%" style="font-size:10px">
					<thead>
						<tr>
							<th>Px Kit #</th>
							<th>Product</th>
							<th>Encoded by</th>
							<th>Claimed by</th>
							<th># of Calls</th>
							<th>Last Call Date</th>
							<th>Status</th>
							<th>Remarks</th>
							<th>Encode</th>
						</tr>
					</thead>
					<tbody>
					@if(!empty($Queue_Verification))
					  @foreach($Queue_Verification as $q)	
					  @if($q->created_at->addHour(1) <= $tn)				  
					  <tr class="red">
					  @elseif($q->created_at->addMinutes(45) <= $tn)
					  <tr class="orange">
					  @elseif($q->created_at->addMinutes(30) <= $tn)
					  <tr class="yellow">
					  @elseif($q->created_at->addMinutes(30) > $tn)
					  <tr class="default-color">
					  @endif
					  <td>{{!empty($q->patient_kit_number)?$q->patient_kit_number: 'Not Given'}}</td>
					  <td>{{!empty($q->medicine)?$q->medicine: 'N/A'}}</td>
					  <td>{{!empty($q->encoded_by->name)?$q->encoded_by->name: 'N/A'}}</td>
					  <td>{{!empty($q->verification_claimed_by->name)?$q->verification_claimed_by->name: 'N/A'}}</td>
					  <td>{{!empty($q->fetch_call->fetch_verification->attempt)?$q->fetch_call->fetch_verification->attempt: '0'}}</td>
					  <td>{{!empty($q->fetch_call->fetch_verification->created_at)?$q->fetch_call->fetch_verification->created_at: 'Not Yet Called'}}</td>
					  <td>{{!empty($q->fetch_call->fetch_verification->fetch_name->name)?$q->fetch_call->fetch_verification->remarks: 'Not Yet Called'}}</td>
					  <td>{{!empty($q->fetch_call->fetch_verification->callnotes)?$q->fetch_call->fetch_verification->callnotes: 'Not Yet Called'}}</td>
					  <td><button type="submit" name="encode" value="{{$q->id}}" class="btn btn-xs btn-primary">Verify</button><input type="hidden" name="px_kit" value="{{$q->patient_kit_number}}"></td>
					  </tr>	  
					  @endforeach
					 @endif
					</tbody>
			</table>
				</form>
</div> <!-- ENCODE QUEUEING -->

<div class="col-md-12" style="background-color:#5cb85c!important;color:#ffffff;margin-top:20px" >
<hr/>
			<h3>Verification Callback</h3>
			
			<form method="post" id="encode_form" action="{{route('otsuka.verify')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
			<table id="verication-queue" class="display compact datatable table-bordered table-hover" cellspacing="0" width="100%" style="font-size:10px">
					<thead>
						<tr>
							<th>Px Kit #</th>
							<th>Product</th>
							<th>Encoded by</th>
							<th>Claimed by</th>
							<th># of Calls</th>
							<th>Last Called by</th>
							<th>Last Call Date</th>
							<th>Status</th>
							<th>Remarks</th>
							<th>Encode</th>
						</tr>
					</thead>
					<tbody>
					@if(!empty($Queue_Verification_Callback))
					  @foreach($Queue_Verification_Callback as $qvc)	
					  @if($qvc->created_at->addHour(1) <= $tn)				  
					  <tr class="red">
					  @elseif($qvc->created_at->addMinutes(45) <= $tn)
					  <tr class="orange">
					  @elseif($qvc->created_at->addMinutes(30) <= $tn)
					  <tr class="yellow">
					  @elseif($qvc->created_at->addMinutes(30) > $tn)
					  <tr class="default-color">
					  @endif
					  <td>{{!empty($qvc->patient_kit_number)?$qvc->patient_kit_number: 'Not Given'}}</td>
					  <td>{{!empty($qvc->medicine)?$qvc->medicine: 'N/A'}}</td>
					  <td>{{!empty($qvc->encoded_by->name)?$qvc->encoded_by->name: 'N/A'}}</td>
					  <td>{{!empty($qvc->verification_claimed_by->name)?$qvc->verification_claimed_by->name: 'N/A'}}</td>
					  <td>{{!empty($qvc->fetch_call_queue->fetch_verification_queue->attempt)?$qvc->fetch_call_queue->fetch_verification_queue->attempt: '0'}}
					  <td>{{!empty($qvc->fetch_call_queue->fetch_verification_queue->callnotes)?$qvc->fetch_call_queue->fetch_verification_queue->callnotes: 'Not Yet Called'}}</td>
					  <td>{{!empty($qvc->updated_at)?$qvc->updated_at: 'Not Yet Called'}}</td>
					  <td>{{!empty($qvc->fetch_call_queue->fetch_verification_queue->fetch_status->name)?$qvc->fetch_call_queue->fetch_verification_queue->fetch_status->name: 'Not Yet Called'}}</td>
					  <td>{{!empty($qvc->fetch_call_queue->fetch_verification_queue->callnotes)?$qvc->fetch_call_queue->fetch_verification_queue->callnotes: 'Not Yet Called'}}</td>
					  <td><button type="submit" name="encode" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Callback</button><input type="hidden" name="px_kit" value="{{$qvc->patient_kit_number}}"></td>
					  </tr>	  
					  @endforeach
					 @endif
					</tbody>
			</table>
				</form>
</div> <!-- ENCODE QUEUEING -->				

<div class="col-md-12" style="background-color:#5bc0de!important;color:#ffffff;margin-top:20px" >
<hr/>
			<h3>Redemption <a class="btn btn-primary" onclick="processRedemption();"/>Batch Process</a> <a class="btn btn-primary"  href="<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id=&patient_id=" style="float:right"/>View Record</a></h3>
			
			<form method="post" id="encode_form" action="{{route('otsuka.encode')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
						
						
							<table class="display compact datatable table-bordered table-hover" cellspacing="0" width="100%" style="font-size:10px">
					<thead>
						<tr>
							<!--<th>Queue</th>
							<th>Type</th>
							<th>Medicine</th>
							<th>Created At</th>
							<th>Claimed</th>
							<th>Encode</th> -->
							
									<tr>
										<th>#</th>
										<th>Px Code</th>
										<th>Px Name</th>
										<th>Product</th>
										<th>Uploaded By</th>
										<th>Encoded By</th>										
										<th>Transaction Date</th>
										<th>Status</th>
										<th>Remarks</th>
										<th>Action</th>
										@if(Auth::user()->user_level == 2)<th>Hide</th>@endif
									</tr>
							
							
							
						</tr>
					</thead>
					<tbody>
					  @if($Queue_Remdeption->count())
					  @foreach($Queue_Remdeption as $qvc_redemption)
					  @if($qvc_redemption->created_at->addHour(1) <= $tn and $qvc_redemption->viber_queue == '0')
					  <tr class="red">
					  @elseif($qvc_redemption->created_at->addMinutes(45) <= $tn and $qvc_redemption->viber_queue == '0')
					  <tr class="orange">	
					  @elseif($qvc_redemption->created_at->addMinutes(30) <= $tn and $qvc_redemption->viber_queue == '0')
					  <tr class="yellow">
					  @elseif($qvc_redemption->viber_queue == '1')
					  <tr class="viber">
					  @elseif($qvc_redemption->created_at->addMinutes(30) > $tn and $qvc_redemption->viber_queue == '0')
					  <tr class="default-color">
					  @endif
					  <td><input type="checkbox" name="upload_id_checkbox" class="upload_id_checkbox" value="{{$qvc_redemption->id}}"/></td>
					  <td>{{$qvc_redemption->id}}</td>
					  <td>{{$qvc_redemption->patient_name}}</td>					  

					  <td>{{$qvc_redemption->medicine}}</td>
					  <td>{{$qvc_redemption->user->name}}</td>
					  <td>{{$qvc_redemption->encoded_by->name}}</td>					 
					  <td>{{$qvc_redemption->created_at}}</td>
					  <td>{{$qvc_redemption->status}}</td>
					  <td>{{$qvc_redemption->redemption_remarks}}</td>
					  
					   <td><a href="<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id={{$qvc_redemption->id}}&patient_id={{$qvc_redemption->redeem_by}}" name="encode" value="" class="btn btn-xs btn-primary">process</a></td>
					   @if(Auth::user()->user_level == 2)<td><a href="#" onclick="hideupload('{{$qvc_redemption->id}}')"  value="{{$qvc_redemption->id}}" class="btn btn-xs btn-primary">Hide</a></td>@endif
					  </tr>
					  
					
					 
					  @endforeach
					  @endif
					</tbody>
			</table>
						
						
						
						
						
			
				</form>
</div> <!-- REDEMPTION QUEUEING -->			



<div class="col-md-12" style="background-color:#5bc0de!important;color:#ffffff;" >
<hr/>
			<h3>Supporting Docs </h3>
			
			<form method="post" id="encode_form" action="{{route('otsuka.encode')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
						
						
						<table class="datatable table table-bordered" style="font-size:12px;color:#000000">
							<thead>
								<tr>
									<th>UPLOADID</th>	
									<th>UPLOAD DATE</th>
									<th>Uploaded To</th>
									<th>Remarks</th>
									<th>Status</th>
									<th>Link</th>
									

								</tr>
							</thead>
							<tbody>
								
									@if(!empty($supporting_docs_list))
										@foreach($supporting_docs_list as $key => $val)
										<tr>
										<td>{{$val->id}}</td>
										<td>{{$val->created_at}}</td>
										<td>{{$val->uploaded_to}}</td>
										<td>{{$val->remarks}}</td>
										<td>{{$val->frontend_status}}</td>
										<td><a href="{{$pic.'/supportingdocs/' . $val->filename}}" target="_blank">View Image</a></td>
										</tr>
										@endforeach	
									@endif
									
								
								
							</tbody>

						</table>
						
						
						
						
						
						
					<?php /*	<table id="redemption-queue" class="display compact datatable table-bordered table-hover" cellspacing="0" width="100%" style="font-size:10px">
								<thead>
									<tr>
										<th>Px Code</th>
										<th>Px Name</th>
										<th>Product</th>
										<th>SKU</th>
										<th>Transaction Date</th>
										<th>Status</th>
										<th>Remarks</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								  @if($redemption->count())
										@foreach($redemption as $qvcueue_redemption)	
									<?php// pre($qvcueue_redemption)?>
										<tr style="color:#000000">
											<td>{{!empty($qvcueue_redemption->patient_code)?$qvcueue_redemption->patient_code: 'N/A'}}</td>
											<td>{{!empty($qvcueue_redemption->fetch_upload_id->patient_name)?$qvcueue_redemption->fetch_upload_id->patient_name: 'N/A'}}</td>
											<td>{{!empty($qvcueue_redemption->fetch_upload_id->medicine)?$qvcueue_redemption->fetch_upload_id->medicine: 'N/A'}}</td>
											<td>{{!empty($qvcueue_redemption->patient_code)?$qvcueue_redemption->patient_code: 'N/A'}}</td>
											<td>{{!empty($qvcueue_redemption->status)?$qvcueue_redemption->status: 'N/A'}}</td>								
											<td>{{!empty($qvcueue_redemption->patient_code)?$qvcueue_redemption->patient_code: 'N/A'}}</td>
											<td>{{!empty($qvcueue_redemption->fetch_upload_id->remarks)?$qvcueue_redemption->fetch_upload_id->remarks: 'N/A'}}</td>
											  <td><a href="<?php echo $baseurl;?>otsuka/redemption" name="encode" value="{{$qvc->id}}" class="btn btn-xs btn-primary">Verify</a></td>
										</tr>
										@endforeach
								  @endif
										
								</tbody>
						</table> */?>
				</form>
</div> <!-- REDEMPTION QUEUEING -->		





<div class="col-md-12"  style="background-color:#f0ad4e!important;color:#ffffff;margin-top:20px" >
<hr/>
			<h3>Compliance</h3>
			
			<form method="post" id="encode_form" action="{{route('otsuka.compliance')}}" enctype="multipart/form-data">
						{{ csrf_field() }} 
			<table id="compliance-queue" class="display compact datatable table-bordered table-hover" cellspacing="0" width="100%" style="font-size:10px;color:#000000">
					<thead>
						<tr>
							<th>Px Code</th>
							<th>Px Name</th>
							<th>Compliance Date</th>
							<th>Encode</th>
						</tr>
					</thead>
					<tbody>
					  @foreach($Queue_Compliance as $qc)
					  <tr>
					  	<td>{{!empty($qc->fetch_cmid->fetch_patient->patient_code)?$qc->fetch_cmid->fetch_patient->patient_code: ''}}</td>
					  	<td>{{!empty($qc->fetch_cmid->fetch_patient->patient_fullname)?$qc->fetch_cmid->fetch_patient->patient_fullname: ''}}</td>
					  	<td>{{!empty($qc->updated_at)?$qc->updated_at: ''}}</td>
					  	<td><button type="submit" name="encode" value="{{!empty($qc->cmid)?$qc->cmid: ''}}" class="btn btn-xs btn-primary">Callback</button><input type="hidden" name="px_kit" value=""></td>
					  </tr>
					  @endforeach
					</tbody>
			</table>
				</form>
</div> <!-- ENCODE QUEUEING -->				





@endsection 

@section('footer-scripts')
<script>


	function processRedemption(upload_id) {
   var str = "";
				 $(".upload_id_checkbox:checked").each(function() {
					str += $(this).val() + ",";
			  });
			  str = str.substr(0, str.length - 1);
			  alert(str);
				
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/redemption?action=redemption&upload_id=" + str);
		
		//var xhttp = new XMLHttpRequest();
		//xhttp.onreadystatechange = function() {
		//if (this.readyState == 4 && this.status == 200) {	
		
		//document.getElementById("filterdisplay").innerHTML = xhttp.responseText;
		//alert('Success');
		//document.getElementsByTagName("body").innerHTML = xhttp.responseText;
		//}
		//};
		//xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/assign_patient?patient_id=" + patient_id + "&upload_id=" + upload_id, true);
		//xhttp.send();					 
		}





function hideupload(id) {
	//alert(id);
 	var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		$(location).attr('href', "<?php echo $baseurl;?>otsuka/agent");
		//$('#upload_queue_id_' + id).remove();
		//$(".upload-datatable").DataTable();
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/disable?active=" + id , true);

  xhttp.send();	 
    
}



$(document).ready(function() {

/*            setTimeout(function() {
                document.location.reload(true);
            }, 3500);*/

$(".datatable").DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
});

});
</script>
@endsection

