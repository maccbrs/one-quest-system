@extends('otsuka.blank')

@section('Patient Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');ini_set('max_execution_time', 180);?>
  <?php $jsupload = URL::asset('/jsupload/');
  	 $AppHelper2 = new AppHelper; 
  ?>
<style type="text/css">
th { font-size: 12px; }
td { font-size: 11px; }

.med_logo img {
    width:100%;
    height:10%;
}

[hidden] {
  display: none !important;
}

.bar {
    height: 18px;
    background: green;
}
.modal-center {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

/**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
margin-left: 3vw;     
margin-right:3vw;  
}    

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(.5);    
-moz-transform: scale(.5);  
-webkit-transform: scale(.5);  
-o-transform: scale(.5);  
transform: scale(.5);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
ul.gallery {      
margin-left: 15vw;       
margin-right: 15vw;
}

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}

  input {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
  }

 input.noborder {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 0;
  }

  div.border {
      border: 1px solid black;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
  }

  div.padding {
      padding-top: 40px;
      padding-right: 65px;
      padding-bottom: 40px;
      padding-left: 60px;
  }

  h2.space {
    line-height: .25;
  }

  p.ind {
    text-indent: 50px;
    line-height: .1;
  }

</style>

<div class="col-md-12">				
 <h2>MTD REDEMPTION REPORT</h2>
<div class="col-md-12">
<div class="panel panel-default">
  <div class="panel-heading">Report</div>
  <div class="panel-body">
<table id="example" class="display table table-bordered compact nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>

				
				<TH >PATIENT CODE</TH>				
				
				<TH >PATIENT_KIT_NUMBER</TH>	
				<TH >MONTH OF REDEMPTION</TH>
				<TH >DATE_OF_REDEMPTION</TH>
				<TH >(TAGGING) TEAM</TH>
				<TH >(TAGGING) AREA_CODE</TH>
				<TH >(TAGGING) MR_NAME</TH>
				<TH >ENROLLMENT_YEAR</TH>
				<TH >ENROLLMENT_DATE</TH>
				<TH >ENROLLMENT_TYPE</TH>
				<TH >PX CLASS</TH>	
				<TH >ENROLLMENT_MD_NAME</TH>
				<TH >HOSPITAL INSTUTION NAME</TH>
				<TH >MD CLASS</TH>
				<TH >SPECIALTY</TH>
				<TH >DOCTOR ID</TH>
				<TH >EMPLOYEE DOCTOR ID</TH>
				{{-- <TH >PRODUCT PRESCRIBED 22</TH> --}}
				<TH >PRODUCT PRESCRIBED</TH>
				<TH >SKU PRESCRIBED</TH>
				<TH >PRODUCT_PURCHASED</TH>
				<TH >SKU PURCHASED</TH>
				<TH >NO_TABS</TH>	
				<TH >PROD_REDEMP</TH>
				<TH >SKU_REDEMP</TH>
				<TH >NO_TAB_FREE</TH>
				<TH> REDEMPTION STATUS </TH>					
				<TH >Date of 1st purchase</TH>
				<TH >DATE_OF_SUBMISSION</TH>				
				<TH >OR_NUMBER</TH>
				
            </tr>
        </thead>
        <tfoot>
            <tr>

				<TH >PATIENT CODE</TH>				
				
				<TH >PATIENT_KIT_NUMBER</TH>	
				<TH >MONTH OF REDEMPTION</TH>
				<TH >DATE_OF_REDEMPTION</TH>
				<TH >(TAGGING) TEAM</TH>
				<TH >(TAGGING) AREA_CODE</TH>
				<TH >(TAGGING) MR_NAME</TH>
				<TH >ENROLLMENT_YEAR</TH>
				<TH >ENROLLMENT_DATE</TH>
				<TH >ENROLLMENT_TYPE</TH>
				<TH >PX CLASS</TH>	
				<TH >ENROLLMENT_MD_NAME</TH>
				<TH >HOSPITAL INSTUTION NAME</TH>
				<TH >MD CLASS</TH>
				<TH >SPECIALTY</TH>
				<TH >DOCTOR ID</TH>
				<TH >EMPLOYEE DOCTOR ID</TH>
				{{-- <TH >PRODUCT PRESCRIBED 22</TH> --}}
				<TH >PRODUCT PRESCRIBED</TH>
				<TH >SKU PRESCRIBED</TH>
				<TH >PRODUCT_PURCHASED</TH>
				<TH >SKU PURCHASED</TH>
				<TH >NO_TABS</TH>	
				<TH >PROD_REDEMP</TH>
				<TH >SKU_REDEMP</TH>
				<TH >NO_TAB_FREE</TH>
				<TH> REDEMPTION STATUS </TH>					
				<TH >Date of 1st purchase</TH>
				<TH >DATE_OF_SUBMISSION</TH>				
				<TH >OR_NUMBER</TH>
				
			
            </tr>
        </tfoot>
        <tbody>
			 <?php foreach($ytd_redemtion_report as $key => $val)
				{?>
				<tr>
				<td>{{$val->patient_code}} <?php /*/ @if(!empty($val->fetch_patient_info))
					{{$val->fetch_patient_info->created_by}} 
					@endif */?>
					
				</td>	<!-- <TH >PATIENT CODE</TH>	-->			
				
				<td>{{$val->fetch_patient_info->patient_kit_number}} </td> <!-- {{$val->OneQuestNumber}}<TH >PATIENT_KIT_NUMBER</TH>	-->
				<td>{{date("M",strtotime($val->created_at_date))}} </td><!-- <TH >MONTH OF REDEMPTION</TH> -->
				<td>{{date("m/d/Y",strtotime($val->created_at_date))}}</td> <!--<TH >DATE_OF_REDEMPTION</TH>-->
				
				
				<td>@if(!empty($val->fetch_patient_info))
						@if($val->fetch_patient_info->patient_tagging_team != "")
							{{$val->fetch_patient_info->patient_tagging_team}}
						@else
							NO AVAILABLE DATA
						@endif						
					@endif
				</td> <!-- <TH >(TAGGING) TEAM</TH>-->
				<td>@if(!empty($val->fetch_patient_info))
						@if($val->fetch_patient_info->patient_tagging_area_code != "")
							{{$val->fetch_patient_info->patient_tagging_area_code}}
						@else
							NO AVAILABLE DATA
						@endif							
					@endif</td> <!-- <TH >(TAGGING) AREA_CODE</TH> -->
				<td>@if(!empty($val->fetch_patient_info))
						@if($val->fetch_patient_info->patient_mr_name != "")
							{{$val->fetch_patient_info->patient_mr_name}}
						@else
							NO AVAILABLE DATA
						@endif
						
					@endif</td><!-- <TH >(TAGGING) MR_NAME</TH> -->
					
					
					<td>

					@if(!empty($val->fetch_patient_info))
						@if($val->fetch_patient_info->patient_enrollment_date != "")
								@if(date("m/d/Y",strtotime($val->fetch_patient_info->patient_enrollment_date)) == "01/01/1970")
										{{date("Y",strtotime($val->fetch_patient_info->date_encoded))}}
								@else
								{{date("Y",strtotime($val->fetch_patient_info->patient_enrollment_date))}}
								@endif

						@elseif(!empty($val->fetch_patient_info->date_encoded))
								{{date("Y",strtotime($val->fetch_patient_info->date_encoded))}}

						@else
							NO AVAILABLE DATA
						@endif
						
					@endif
				
					</td><!-- <TH >YEAR</TH> -->
				<td>

					@if(!empty($val->fetch_patient_info))
						@if($val->fetch_patient_info->patient_enrollment_date != "")
								@if(date("m/d/Y",strtotime($val->fetch_patient_info->patient_enrollment_date)) == "01/01/1970")
										{{date("m/d/Y",strtotime($val->fetch_patient_info->date_encoded))}}
								@else
								{{date("m/d/Y",strtotime($val->fetch_patient_info->patient_enrollment_date))}}
								@endif

						@elseif(!empty($val->fetch_patient_info->date_encoded))
								{{date("m/d/Y",strtotime($val->fetch_patient_info->date_encoded))}}

						@else
							NO AVAILABLE DATA
						@endif
						
					@endif
				
					</td><!-- <TH >ENROLLMENT_DATE</TH> -->
					
				<td>@if(!empty($val->fetch_patient_info))
						@if($val->fetch_patient_info->patient_type != "")
									<?php 
									echo strtoupper($val->fetch_patient_info->patient_type);
									?>
								
						@else
							NO AVAILABLE DATA
						@endif
				
						
					@endif</td> <!-- <TH >ENROLLMENT_TYPE</TH> -->
					<td>{{$val->px_class_3}}</td><!-- patient Class -->
					
				<td>@if(!empty($val->fetch_patient_info)) 
						@if($val->fetch_patient_info->patient_tagging_doctor_name != "")
							{{ucwords(strtolower($val->fetch_patient_info->patient_tagging_doctor_name))}}
						@else
							NO AVAILABLE DATA
						@endif
						
					
					@endif</td> <!-- <TH >ENROLLMENT_MD_NAME</TH> -->
				<td>@if(!empty($val->fetch_patient_info)) 
						@if($val->fetch_patient_info->patient_tagging_hospital != "")
							{{$val->fetch_patient_info->patient_tagging_hospital}}
						@else
							NO AVAILABLE DATA
						@endif

						
							
					@endif</td> <!-- <TH >HOSPITAL INSTUTION NAME</TH> -->
				<td>@if(!empty($val->fetch_patient_info)) 
						@if($val->fetch_patient_info->patient_tagging_md_class != "")
							{{$val->fetch_patient_info->patient_tagging_md_class}}
						@else
							NO AVAILABLE DATA
						@endif
						
						
					@endif</td> <!--<TH >MD CLASS</TH> --> 
				<td>@if(!empty($val->fetch_patient_info)) 						
						@if($val->fetch_patient_info->patient_tagging_specialty != "")
							{{$val->fetch_patient_info->patient_tagging_specialty}}
						@else
							NO AVAILABLE DATA
						@endif


							{{--
						@if($val->fetch_patient_info->created_by =="SANDMAN")
							@if(!empty($val->fetch_patient_info->old_retrieval->fetch_doctors->specialty))
								{{$val->fetch_patient_info->old_retrieval->fetch_doctors->specialty}}
							@endif
						@else
								{{$val->fetch_patient_info->specialty}}
						@endif
						--}}
					@endif</td><!-- <TH >SPECIALTY</TH> -->
				<td>@if(!empty($val->fetch_patient_info)) 
						@if($val->fetch_patient_info->patient_tagging_doctorid != "")
							{{$val->fetch_patient_info->patient_tagging_doctorid}}
						@else
							NO AVAILABLE DATA
						@endif	
						{{--	
						{{$val->fetch_patient_info->patient_tagging_emp_doctorid}}
						
						@if($val->fetch_patient_info->created_by =="SANDMAN")
							@if(!empty($val->fetch_patient_info->old_retrieval->fetch_doctors->doctorid))
								{{$val->fetch_patient_info->old_retrieval->fetch_doctors->doctorid}}
							@endif
						@else
								{{$val->fetch_patient_info->doctorid}}
						@endif
						--}}
					@endif</td> <!-- <TH >DOCTOR ID</TH> -->
				<td>@if(!empty($val->fetch_patient_info)) 
						@if($val->fetch_patient_info->patient_tagging_emp_doctorid != "")
							{{$val->fetch_patient_info->patient_tagging_emp_doctorid}}
						@else
							NO AVAILABLE DATA
						@endif		
							
							
						
						{{--
						{@if($val->fetch_patient_info->created_by =="SANDMAN")
							@if(!empty($val->fetch_patient_info->old_retrieval->fetch_doctors->employee))
								{{$val->fetch_patient_info->old_retrieval->fetch_doctors->employee}}
							@endif
						@else
								{{$val->fetch_patient_info->employee}}
						@endif
						--}}
					@endif</td> <!-- <TH >EMPLOYEE DOCTOR ID</TH> -->
					{{--<td>@if(!empty($val->fetch_patient_info)) 
						@if($val->fetch_patient_info->patient_tagging_product_prescribe != "")
							{{$val->fetch_patient_info->patient_tagging_product_prescribe}}
						@else
							NO AVAILABLE DATA
						@endif		
					
					@endif</td> --}}
				<td>{{$val->product_purchased}}
					{{--
					@if(!empty($val->fetch_patient_info)) 
						
						@if($val->fetch_patient_info->created_by =="SANDMAN")
							@if(!empty($val->fetch_patient_info->old_retrieval->fetch_sku->brand))
								{{$val->fetch_patient_info->old_retrieval->fetch_sku->brand}}
							@endif
						@else
								@if(!empty($val->fetch_patient_info->fetch_sku->brand))
								{{$val->fetch_patient_info->fetch_sku->brand}}
								@endif
								
						@endif
					
					@endif --}}</td>	<!-- <TH >PRODUCT PRESCRIBED</TH>-->
				<td>{{$val->sku_purchased}}
				{{--
				@if(!empty($val->fetch_patient_info)) 
						@if($val->fetch_patient_info->created_by =="SANDMAN")
							@if(!empty($val->fetch_patient_info->old_retrieval->fetch_sku->skuname))
								{{$val->fetch_patient_info->old_retrieval->fetch_sku->skuname}}
							@endif
						@else
								@if(!empty($val->fetch_patient_info->fetch_sku->fetch_prescribed->skuname))
								{{$val->fetch_patient_info->fetch_sku->fetch_prescribed->skuname}}
								@endif
								
						@endif
					
				@endif 
				--}}</td> <!-- <TH >SKU PRESCRIBED</TH> -->
				<td>{{$val->product_purchased}}</td> <!-- <TH >PRODUCT_PURCHASED< /TH> -->
				<td>{{$val->sku_purchased}}</td> <!-- <TH >SKU PURCHASED< /TH> -->
				<td>{{$val->no_tab}}</td> <!-- <TH >NO_TABS< /TH> -->
				<td>{{$val->prod_redemp}}</td> <!-- <TH >PROD_REDEMP< /TH> -->
				<td>{{$val->sku_redemp}}</td> <!-- <TH >PRODUCT_PURCHASED< /TH> -->
				<td>{{$val->no_tab_free}}</td><!-- NO_TAB_FREE -->
				
				<td>{{$val->redemption_status}}</td><!-- NO_TAB_FREE -->
			
				 <!--<td>{{date("m/d/Y",strtotime($val->fetch_patient_info->date_of_first_purchase))}}</td> <TH >Date of 1st purchase</TH> -->
				<td>{{date("m/d/Y",strtotime($val->fetch_patient_info->date_of_first_purchase))}}</td> <!-- <TH >Date of 1st purchase</TH> -->
				<td>{{date("m/d/Y",strtotime($val->date_of_submission))}}</td> <!-- <TH >DATE_OF_SUBMISSION</TH> --> 
						
				
				<td>{{$val->or_number}}</td> <!-- <TH >OR_NUMBER</TH> -->
				
				</tr>
				
				<?php }?>
				
		   
				
               
           
            
           
        </tbody>
    </table>
</div>
</div>
</div>
</div>



@endsection 

@section('footer-scripts')
<script>
$(document).ready(function() {

var today = new Date();
var dd = ("0" + (today.getDate())).slice(-2);
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();


    $('#example').DataTable({
        lengthMenu: [[10, 25, 50, 75, 100, -1], [10,25, 50, 75, 100, "All"]],
        pageLength: 215, 
        "scrollX": true,
        autoWidth: false,
        responsive: true,
        
        scrollCollapse: true,
        dom: 'Blfrtip',
        stateSave: true,
         buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: 'MTD REDEMPTION Report '+yyyy+mm+dd,
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            'colvis'
        ]
            });

 });
</script>


@endsection

