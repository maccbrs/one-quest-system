
  <?php $pic = URL::asset('/uploads/');?>
  <?php $old_pic = URL::asset('/uploads/card_uploads');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
	$baseurl = URL::asset('/'); 
   ?>
  




<style type="text/css">
	/**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
margin-left: 3vw;     
margin-right:3vw;  
}    

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(8);    
-moz-transform: scale(8);  
-webkit-transform: scale(8);  
-o-transform: scale(8);  
transform: scale(8);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
ul.gallery {      
margin-left: 15vw;       
margin-right: 15vw;
}

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}


</style>


      
      
<!-- Modal -->

  <form action="{{route('otsuka.patient.enroll')}}" method="POST" id="update-form">
  <!-- <input type="text" id="enroll_refid" name="enroll_refid" > -->
{{ csrf_field() }}
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">Edit Patient<input type="hidden" name="refid" id="refid" value="{{$patient_record->id}}"/><input type="hidden" name="action"  value="edit"/>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">

			<div class="row">
		      <div class="col-md-12">
		        <div class="panel panel-default">
		          <div class="panel-heading">Retrieval Kit</div>
		          <div class="panel-body">  
		          	<?php
		          	$Upload = new App\Http\Models\otsuka\Upload;
		          	$upload = $Upload->where('patientid',$patient_record->id)->where('trail_status','!=',199)->first();
		          	?>
		          	<?php
		          	$Old_retrieval_enrollment = new App\Http\Models\otsuka\Old_retrieval_enrollment;
		          	$old_retrieval_enrollment = $Old_retrieval_enrollment->where('patientid',$patient_record->id)->first();
		          	?>
		         @if(!empty($upload)) 	
		        <img class="zoom" style="width: 10%; height: 10%;" src="{{$pic.'/'.$upload->filename}}">
		         <a href="#" onclick="popupwindow('{{$pic.'/'.$upload->filename}}');"><img class="" style="width: 10%; height: 10%;" src="{{$pic.'/'.$upload->filename}}"></a>
		         @elseif(empty($upload) and !empty($old_retrieval_enrollment))
		         @if(!empty($old_retrieval_enrollment->imagefilepath))
		        <img class="zoom" style="width: 10%; height: 10%;" src="{{$old_pic.'/'.$old_retrieval_enrollment->imagefilepath}}">
		         <a href="#" onclick="popupwindow('{{$old_pic.'/'.$old_retrieval_enrollment->imagefilepath}}');"><img class="" style="width: 10%; height: 10%;" src="{{$old_pic.'/'.$old_retrieval_enrollment->imagefilepath}}"></a>
		         @else
		         NO AVAILABLE DATA
		         @endif
		         @else
		         NO AVAILABLE DATA
		         @endif
		          </div>
		        </div>
		      </div>
		    </div>

       
			<div class="row"><div class="col-md-12"><h3>Basic Information</h3></div></div>

			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					  <label for="enroll_patient_kit_number">Patient Kit Number: </label>
					  <input type="text" class="form-control input-sm" name="enroll_patient_kit_number" value="{{$patient_record->patient_kit_number}}" pattern="[0-9]{10}">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					  <label for="enroll_firstname">First:</label>
					  <input type="text" class="form-control input-sm"  name="enroll_firstname" value="{{$patient_record->patient_firstname}}">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
					  <label for="enroll_middlename">Middle:</label>
					  <input type="text" class="form-control input-sm"   name="enroll_middlename" value="{{$patient_record->patient_middlename}}">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
					  <label for="enroll_lastname">Lastname:</label>
					  <input type="text" class="form-control input-sm"   name="enroll_lastname" value="{{$patient_record->patient_lastname}}">
					</div>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">Mobile No:</label>
					  <input type="text" class="form-control input-sm"   name="enroll_mobile_no" value="{{$patient_record->mobile_number}}">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">Mobile No 2:</label>
					  <input type="text" class="form-control input-sm"   name="enroll_mobile_no_2" value="{{$patient_record->mobile_number_2}}">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">Landline:</label>
					  <input type="text" class="form-control input-sm"   name="enroll_phone_no" value="{{$patient_record->phone_number}}">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
						<label><input type="checkbox" name="enroll_patient_consent" <?php if($patient_record->patient_consent == 'true') echo " checked='checked'";?> > Patient Consent</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">Birth Date:</label>
					  <input type="text" class="form-control input-sm "   name="enroll_birthdate" id="birthdate_edit" value="{{date('m/d/Y', strtotime($patient_record->birth_date))}}">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">Age:</label>
					  <input type="text" class="form-control input-sm"   name="enroll_age" value="{{$patient_record->age}}">
					</div>
				</div>	
				<div class="col-md-3">
					<div class="form-group">
					  <label for="email">NickName:</label>
					  <input type="text" class="form-control input-sm"  name="enroll_nickname" value="{{$patient_record->nickname}}">
					</div>
				</div>	
				
				<div class="col-md-3">
					<div class="form-group">			
					  <label for="enroll_gender">Gender:</label>
					  <select class="form-control input-sm" name="enroll_gender">
					  	<option value="{{!empty($patient_record->gender)?$patient_record->gender: ''}}" required>{{!empty($patient_record->gender)?$patient_record->gender: '* Please Select Gender'}}</option>
						<option value="Px Won't Disclose">Px Won't Disclose</option>
						<option value="Male">Male</option>
						<option value="Female">Female</option>
					  </select>

					</div>
				</div>	
				
			</div>
				
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Doctor's Name:</label>
					  <input type="text" class="form-control input-sm"  name="enroll_doctors_name" value="{{$patient_record->doctor_name}}">
					</div>
				</div>	
				<div class="col-md-4" style="margin-top: 15px;">
					<div class="form-group">
					 <label style="float:left;font-size:15px"><input type="checkbox" name="enroll_doctor_consent" value="true" @if($patient_record->doctor_consent == 'true') checked @endif> Doctor's Consent</label>
					</div>
					<div class="form-group">
					 <label style="float:left;font-size:15px"><input type="checkbox" name="doctor_wont_disclose" value="true" @if($patient_record->doctor_wont_disclose == 'true') checked @endif> Px Won't Disclose</label>
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Hospital:</label>
					  <input type="text" class="form-control input-sm"  name="enroll_hospital" value="{{$patient_record->hospital}}">
					</div>
				</div>	
			</div>		
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					  <label for="email">Address</label>
					  <textarea name="enroll_address" class="form-control input-sm">{{$patient_record->address}}</textarea>
					  <!--<input type="text" class="form-control input-sm" id="enroll_doctors_name"  name="email">-->
					 
					</div>
				</div>	
				
				<!--<div class="col-md-4">
					<div class="form-group">
					  <label for="email">Landline:</label>
					  <input type="text" class="form-control input-sm"  name="enroll_hospital" id="enroll_hospital">
					</div>
				</div>	-->
			</div>		

			<hr/>
			<div class="row"><div class="col-md-12"><h3>Medical Information</h3></div></div>
				
				<div class="row">
					  <div class="col-md-6">
						<label for="Product">Product</label>
						  <select class="form-control input-sm input-sm"   name="enroll_product">
							<option>* Please Select Product</option>
							<option value="Abilify"  <?php 
							if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->brand == 'Abilify') echo " selected";}?>>Abilify</option>
							<option value="Aminoleban" <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->brand == 'Aminoleban') echo " selected";}?>>Aminoleban</option>
							<option value="Pletaal" <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->brand == 'Pletaal') echo " selected";}?>>Pletaal</option>
						  </select>
					  </div>
					<div class="col-md-6">
					  <label for="SKU">SKU</label><select class="form-control input-sm input-sm"  name="sku">
						  <option>* Please Select SKU</option>
						 
						  @foreach($Mgabilify as $mab)
						  <option value="{{$mab->id}}" 
						  <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->sku == $mab->id) echo " selected";}?>
						  >{{$mab->skuname}}</option>
						  @endforeach
						 
						
						</select>
				  </div>
				</div>
<hr/>

			<div class="row">
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Tabs Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="notabsprescribe"  <?php if(!empty($patient_record->fetch_sku)){ echo "value='" . $patient_record->fetch_sku->no_of_tabs_prescribe ."'";}?> class="form-control input-sm input-sm" max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label"># of Days Prescribe</label>
                  <div class="col-md-4">
                    <input type="number" name="nodaysprescribe"  class="form-control input-sm input-sm" <?php if(!empty($patient_record->fetch_sku)){ echo "value='" . $patient_record->fetch_sku->no_of_days_prescribe ."'";}?> max="9999">
                  </div>
              </div>
              <div class="col-md-4">
                <label for="inputKey" class="col-md-8 control-label right-label">Total No. of Tabs</label>
                  <div class="col-md-4">
                    <input type="number" name="totalnotabs"  <?php if(!empty($patient_record->fetch_sku)){ echo "value='" . $patient_record->fetch_sku->total_no_of_tabs ."'";}?> class="form-control input-sm input-sm" max="9999">
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe"  value="Px Won't Disclose" <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->tabs_prescribe_remarks == "Px Won't Disclose") echo "  checked='checked'";}?>>Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="tabsprescribe"  value="No Available Data" <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->tabs_prescribe_remarks == "No Available Data") echo "  checked='checked'";}?>>No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="daysprescribe"  value="Px Won't Disclose" <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->days_prescribe_remarks == "Px Won't Disclose") echo "  checked='checked'";}?>>Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="daysprescribe"  value="No Available Data" <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->days_prescribe_remarks == "No Available Data") echo "  checked='checked'";}?>>No Available Data</label>
                </div>
              </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-5"></div>
                <div class="col-md-7">
                <div class="radio">
                  <label><input type="radio" name="totaltabs"  value="Px Won't Disclose" <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->total_tabs_remarks == "Px Won't Disclose") echo "  checked='checked'";}?>>Px Won't Disclose</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="totaltabs"  value="No Available Data" <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->total_tabs_remarks == "No Available Data") echo "  checked='checked'";}?>>No Available Data</label>
                </div>
              </div>
            </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-md-12">
                <label>Already Purchased:</label>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased"  <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->purchased == "Yes") echo "  checked='checked'";}?> value="Yes">Yes</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="purchased" <?php if(!empty($patient_record->fetch_sku)){if($patient_record->fetch_sku->purchased == "No") echo "  checked='checked'";}?> id="purchased_n" value="No">No</label>
                </div>
              </div>
            </div>

		
      </div>
      <div class="modal-footer"><div id="demo"></div>
		<input type="submit" class="btn btn-info btn-default" style="float:left; value="Save & Exit"/>
		<!--<button type="button" style="float:left; " id="" class="btn btn-info btn-default"  data-dismiss="modal">Save & Exit</button>-->
		<!--<button type="button" style="float:left; " id="btn_enroll_save" class="btn btn-primary btn-default">Save </button> -->
		
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
</form>
<script>

</script>


