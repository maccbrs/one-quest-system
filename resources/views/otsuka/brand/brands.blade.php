@extends('otsuka.settings.master')


@section('content')
<div class="container-fluid">
	<div class="row">
		@if(session()->has('success'))
		<div id="success" class="col-sm-12">
			<div class="alert alert-success">
				{{ session()->get('success') }}
			</div>
		</div>
		@endif
<form method="POST" action="{{ route('otsuka.post.brands') }}" enctype="multipart/form-data">
		<div class="col-md-6">
			<div class="input-group-prepend">
				<label class="form-group" for="inputGroupSelect01">Brands</label>
			</div>
			<select name="brandname" class="form-control" id="inputGroupSelect01">
				<option selected>Choose...</option>
				@foreach($data as $d)
				<option value="{{ $d->brandname }}">{{ $d->brandname }}</option>

				@endforeach
			</select>
		</div>
	</div>
	<hr>
	<div class="row">

		<div class="col-md-6">
			<div class="input-group-prepend">
				<label class="form-group" for="inputGroupSelect01">Options</label>
			</div>
			<select name="type" class="form-control" id="inputGroupSelect01">
				<option selected>Choose...</option>
				<option value="Retrieval">Retrieval</option>
				<option  value="Receipt">Receipt</option>
			</select>
		</div>

	</div>
<hr>
	<div class="row">
		<div class="col-md-6">

							<div id="forreceipt" class="forreceipt">
									
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="form-group">
											<label>Patien Name: </label>
											<input type="text" name="patient_name" class="form-control" placeholder="Name" required autocomplete="off">
										</div>
										<div class="form-group">
											<label>Mobile #: </label>
											<input type="text" name="mobile_no" class="form-control" placeholder="Mobile No.">
										</div>
										<div class="form-group">
											<label>Landline #: </label>
											<input type="text" name="landline_no" class="form-control" placeholder="Landline No.">

										</div>

										<div class="form-group"><label for="exampleFormControlFile1">Upload File</label>
											<input type="file" class="form-control-file" name="fileUpload" id="fileupload">
										</div>
										<button type="submit" class="btn btn-sm btn-primary">Submit</button>
								
								</div>

		</div>
	</div>
</div>
	</form>

@endsection

@section('footer-scripts')

@endsection