<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".edit-image-modal">
  <i class="material-icons" aria-hidden="true">Upload</i>
</button>
<div class="modal fade edit-image-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" id="form_id" action="{{route('otsuka.index')}}" enctype="multipart/form-data" novalidate>
        {{ csrf_field() }}                       

          <div class="modal-body">            
            <fieldset class="form-group">
                <label for="content">Select File</label>
                <input type="file" name="file[]" class="btn btn-sm btn-default" multiple="true" id="upload_file" accept="image/*" onchange="preview_image();"> </span>
<div id="image_preview"></div>
            </fieldset>                                  
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" name='submit_image' value="Upload Image">Post</button>
          </div>
        </form>
      </div>
  </div>
</div> 