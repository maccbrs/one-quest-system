<button class="btn btn-success btn-sm" data-toggle="modal" data-target=".edit-image-modal">
  <i class="glyphicon glyphicon-upload" aria-hidden="true"> UPLOAD </i>
</button>
<div class="modal fade edit-image-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <form method="post" action="{{route('otsuka.uploader.supportingdocs')}}" enctype="multipart/form-data" novalidate>
        {{ csrf_field() }}   
        <input type="hidden" name="dept_to" value="">
        <input type="hidden" name="type" value="1">                      

          <div class="modal-body">    
			<br/><br/>
			<div class="col-md-5">
			<!--
            <fieldset class="form-group">
                <label for="content">Select File</label> <span style="font-size:10px" class="text-danger"> NOTE: (.png or .jpg) are accepted</span>
                <input type="file" name="file" class="btn btn-sm btn-default">
            </fieldset>              
			-->
			
			{{--<input type='file' id="fileUpload" />
			<canvas id="canvas" width="900" height="600"></canvas>   --}}
			
            <fieldset class="form-group">
                <label for="content">Name Of The Patient <span style="font-size:10px" class="text-danger"> (Required)</span> </label> 
                <input type="text" name="uploaded_to" class="form-control" id="uploaded_to" required="required" >
            </fieldset>   

			<fieldset class="form-group">
                <label for="content">Name Of the Doctor <span style="font-size:10px" class="text-danger"></span> </label> 
                <div class="col-md-10"><input type="text" name="doctorname" class="form-control" id="doctorname" ></div> <span class=" col-md-2 btn btn-sm btn-info"
				 id="btnLaunch"
				>search</span>
            </fieldset>   
			
			<fieldset class="form-group">
                <label for="content">Doctor's Emp ID <span style="font-size:10px" class="text-danger"></span> </label> 
                <input type="text" name="doctor_emp_id" class="form-control" id="doctor_emp_id" >
            </fieldset>  
			
			<fieldset class="form-group">
                <label for="content">Number Of tabs<span style="font-size:10px" class="text-danger"></span> </label> 
                <input type="number" name="number_of_tabs" class="form-control" id="number_of_tabs" >
            </fieldset>   			
					
			<fieldset class="form-group">
                <label for="content">Product Prescribe<span style="font-size:10px" class="text-danger"></span> </label> 
					<select >
					<option readonly>Please Select</option>
					<option value="ABILIFY">ABILIFY</option>
					<option value="AMINOLEBAN">AMINOLEBAN</option>
					<option value="PLETAAL">PLETAAL</option>

					</select>

            
			</fieldset>   			
					
			<fieldset class="form-group">
                <label for="content">Date<span style="font-size:10px" class="text-danger"></span> </label> 
                <input type="text" name="rx_date" class="form-control datepicker" id="rx_date" >
            </fieldset>   			
		

            <fieldset class="form-group">
                <label for="content">Remarks/NOTES</label> 
               <textarea name="remarks" rows="10" class="form-control"></textarea>
            </fieldset>     
			</div>	
			
			<div class="col-md-7">
			 <label for="content">Select File</label> <span style="font-size:10px" class="text-danger"> NOTE: (.png or .jpg) are accepted</span>
			<input type='file' id="fileUpload" name="file"/>
			<canvas id="canvas" width="480" height="512"></canvas>
			</div>
			
                    
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Upload</button>
          </div>
        </form>
      </div>
  </div>
</div> 