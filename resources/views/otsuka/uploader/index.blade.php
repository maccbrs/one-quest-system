<?php $asset = URL::asset('/'); $request = Request::instance(); ?> 
@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');
      $baseurl = URL::asset('/'); 
  ?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
#canvas {
  border: 1px solid black;
  margin-top: 10px;
}
</style>
 
 
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header no-bg b-a-0">
			

            </div>
            <div class="card-block" >
			
            <div class="row media">
				
				<div class="col-md-8">
							@if (\Session::has('return_msg'))
							<div class="alert  return_msg <?php  if(\Session::get('return_result') == '1') {echo 'alert-success';} else {echo 'alert-danger';} ?>">
								<ul>
									<li>{!! \Session::get('return_msg') !!}</li>
								</ul>
							</div>
							@endif
					<div class="panel panel-default">
					  <div class="panel-heading"> <span>Upload Supporting Docs {!!view('otsuka.uploader.upload_docs')!!}</span> <span style="font-size:10px" class="text-danger">NOTE: (.png or .jpg) are accepted</span>
					  <a href="{{route('otsuka.uploader.supportingdocs-all')}}"><span style="float:right" class="btn btn-danger  btn-xs">All</span></a>
					  <a href="{{route('otsuka.uploader.index')}}"><span style="float:right" class="btn btn-warning btn-xs">Un-Assign</span></a>
					  </div>
					  <div class="panel-body">		
						
						<table class="datatable table table-bordered" style="font-size:9px">
							<thead>
								<tr>
									<th>UPLOADID</th>	
									<th>UPLOAD DATE</th>
									<th>Uploaded To</th>
									<th>Remarks</th>
									<th>Status</th>
									<th>Action</th>
									<th>Link</th>
									

								</tr>
							</thead>
							<tbody>
								
									@if(!empty($supporting_docs_list))
										@foreach($supporting_docs_list as $key => $val)
										<tr>
										<td>{{$val->id}}</td>
										<td>{{$val->created_at}}</td>
										<td>{{$val->uploaded_to}}</td>
										<td>{{$val->remarks}}</td>
										<td>{{$val->frontend_status}}</td>
										<td><button class="btn btn-primary btn-xs" data-toggle="modal" data-target=".update-upload-modal"
										
										
												onclick="loadDetails({{$val->id}})">
										Update</button></td>
										<td><a href="{{$pic.'/supportingdocs/' . $val->filename}}" target="_blank">View Image</a></td>
										</tr>
										@endforeach	
									@endif
									
								
								
							</tbody>

						</table>
						</div>
					</div>
					
				</div>
				
                <div class="col-md-4  " >
					<h3>SAMPLE RX </h3>
					<hr/>
				
					<img  style="border:1px solid #000" class="img-fluid center-block" src="{{$pic.'/supportingdocs/'}}sample.jpg">
                   
                    <div class="text-xs-center">
                      
                    </div>
					  <p class="m-b-0 bold"><span>Upload Supporting Documents</span></p>
                </div>
            </div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade update-upload-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <form method="post" action="{{route('otsuka.uploader.supportingdocs-update')}}" enctype="multipart/form-data" novalidate>
        {{ csrf_field() }}   
		
        <input type="hidden" name="id" id="id2" value="">
        <input type="hidden" name="dept_to" value="">
        <input type="hidden" name="type" value="1">                      

          <div class="modal-body">    
			<br/><br/>
			<div class="col-md-5">
			<!--
            <fieldset class="form-group">
                <label for="content">Select File</label> <span style="font-size:10px" class="text-danger"> NOTE: (.png or .jpg) are accepted</span>
                <input type="file" name="file" class="btn btn-sm btn-default">
            </fieldset>              
			-->
			
			{{--<input type='file' id="fileUpload" />
			<canvas id="canvas" width="900" height="600"></canvas>   --}}
			
            <fieldset class="form-group">
                <label for="content">Name Of The Patient <span style="font-size:10px" class="text-danger"> (Required)</span> </label> 
                <input type="text" name="uploaded_to" class="form-control" id="uploaded_to2" required="required" >
            </fieldset>   

			<fieldset class="form-group">
                <label for="content">Name Of the Doctor <span style="font-size:10px" class="text-danger"></span> </label> 
                
				<div class="col-md-10"><input type="text" name="doctorname" class="form-control" id="doctorname2" ></div> <span class=" col-md-2 btn btn-sm btn-info"
				 id="btnLaunch2"
				>search</span>
            </fieldset>   
			
			<fieldset class="form-group">
                <label for="content">Doctor's Emp ID <span style="font-size:10px" class="text-danger"></span> </label> 
                
				<input type="text" name="doctor_emp_id" class="form-control" id="doctor_emp_id2" >
            </fieldset>   
			
			<fieldset class="form-group">
                <label for="content">Number Of tabs<span style="font-size:10px" class="text-danger"></span> </label> 
                <input type="number" name="number_of_tabs" class="form-control" id="number_of_tabs2" >
            </fieldset>   			
					
			<fieldset class="form-group">
                <label for="content">Product Prescribe<span style="font-size:10px" class="text-danger"></span> </label> 
					<select name="product_prescribe" id = "product_prescribe2">
					<option readonly>Please Select</option>
					<option value="ABILIFY">ABILIFY</option>
					<option value="AMINOLEBAN">AMINOLEBAN</option>
					<option value="PLETAAL">PLETAAL</option>

					</select>

            
			</fieldset>   			
					
			<fieldset class="form-group">
                <label for="content">Date<span style="font-size:10px" class="text-danger"></span> </label> 
                <input type="text" name="rx_date" class="form-control datepicker" id="rx_date2" >
            </fieldset>   				
		

            <fieldset class="form-group">
                <label for="content">Remarks/NOTES</label> 
               <textarea name="remarks" id = "remarks2" rows="10" class="form-control"></textarea>
            </fieldset>     
			</div>	
			
			<div class="col-md-7">
			<!-- <label for="content">Select File</label> <span style="font-size:10px" class="text-danger"> NOTE: (.png or .jpg) are accepted</span>
			<input type='file' id="fileUpload" name="file"/> -->
			<div id='loadimg' width="480" height="512" style="border-style: double;overflow: hidden;display:block;max-height: 70px;"/></div>
			<img src="" id="loadimage" style="max-width:480px"/>
			</div>
			
                    
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
  </div>
</div> 







<div class="modal fade" id="myModal_Search_MD">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
        </button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
           <div class="panel panel-default">
            <div class="panel-heading">
              <label for="searchbox" >Search:  </label>
              <input type="text" id="searchbox2" class="form-control" style="display:inline;width:20%"/>
              <input type="hidden" id="filtered2" class="form-control" style="display:inline;width:20%" value="{{!empty($v->fetch_allocation->emp_code)?$v->fetch_allocation->emp_code: ''}}" />
              <select class="form-control" style="display:inline;width:20%" id="person2"><option value="filteredmdv2">MR Doctor</option><option value="globalmdv2">Global</option></select>
              <button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc2($('#searchbox2').val(),$('#person2').val())"></button>
            </div>     
            <div class="panel-body" id="filterdisplay2" style="overflow: scroll;text-align:center">
            </div>
           </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->




 <!-- Modal -->
  <div class="modal fade" id="searchdoctor" role="dialog">
    <div class="modal-dialog modal-l">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
			<input type="text"/> 
			<span class="btn btn-sm btn-primary">Search</span>
		
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
			<div>
				<table class="table table-bordered datatable">
					<thead>
					<tr>
						<th>Action</th>
						<th>MD Name</th>
						<th>Hospital</th>
						<th>Class</th>
						<th>Specialization</th>
						<th>Product</th>
					
					</tr>
					</head>
					
				<tbody>
					<?php for($x=1; $x <= 50;$x++){ ?>
					<tr>
						<td><span class="btn btn-xs btn-info">Use</span></td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
					
					
					</tr>
					<?php }?>
					
				
				
				
				</tbody>
				
				
				</table>
				
				
				
			
			
			</div>

		</div>         
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div> 
    </div>
  </div>





 <!-- Modal -->
  <div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
		 @if (session('status') == 'success')
          <div class="alert alert-success">
        
				<strong>{{session('message')}}</strong> 
		
          </div>
		@elseif (session('status') == 'failed')
		 <div class="alert alert-danger">
        
				<strong>{{session('message')}}</strong> 
		
          </div>
        @endif   
          </div>         
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div> 
    </div>
  </div>
 
 
 
@endsection




@section('header-scripts')

@endsection

@section('footer-scripts')

<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>

<script>


var fileUpload = document.getElementById('fileUpload');
var canvas  = document.getElementById('canvas');
var ctx = canvas.getContext("2d");

function readImage() {
    if ( this.files && this.files[0] ) {
        var FR= new FileReader();
        FR.onload = function(e) {
           var img = new Image();
           img.src = e.target.result;
           img.onload = function() {
             ctx.drawImage(img, 0, 0, 512, 512);
           };
        };       
        FR.readAsDataURL( this.files[0] );
    }
}

fileUpload.onchange = readImage;

canvas.onclick = function(e) {
  var x = e.offsetX;
  var y = e.offsetY;
  ctx.beginPath();
  ctx.fillStyle = 'black';
  ctx.arc(x, y, 5, 0, Math.PI * 2);
  ctx.fill();
};



setTimeout(function() {
    $('.return_msg').slideUp();
}, 2000); // <-- time in milliseconds

$('.datatable').DataTable();




function myFunction() {
    var pass1 = document.getElementById("new_password1").value;
    var pass2 = document.getElementById("new_password2").value;
    var ok = true;
    if (pass1 != pass2) {
        //alert("Passwords Do not match");
        document.getElementById("new_password1").style.borderColor = "#E34234";
        document.getElementById("new_password2").style.borderColor = "#E34234";
        ok = false;
    }
    else {
        alert("Passwords Match!!!");
    }
    return ok;
}




function loadDetails(id) {
	
	
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		//document.getElementById(xhttp.responseText['id']);
		
        //alert(xhttp.responseText['id']);
		var obj = JSON.parse(xhttp.responseText);
	
		$("#id2").val(obj.id);
		$("#uploaded_to2").val(obj.uploaded_to);
		$("#remarks2").val(obj.remarks);
		$("#doctorname2").val(obj.doctorname);
		$("#doctor_emp_id2").val(obj.doctor_emp_id);
		$("#number_of_tabs2").val(obj.number_of_tabs);
		$("#product_prescribe2").val(obj.product_prescribe);
		$("#rx_date2").val(obj.rx_date); 
		
		$('#loadimage').attr('src', "{{$pic.'/supportingdocs/'}}" + obj.filename);
		
		//alert('yesy');
		
		/*
		uploaded_to
		remarks
		doctorname
		number_of_tabs
		product_prescribe
		rx_date
		*/
        //alert(obj.mdname);
		
    }
  };
  
    xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/get_supp_upload?id=" + id , true);
  xhttp.send(); 

}



function loadDoc2(filter,person) {
  var empcode = $('#filtered2').val();
  $('#filterdisplay2').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
        document.getElementById("filterdisplay2").innerHTML = xhttp.responseText;
    	$('.datatabledraw').DataTable(); 
    	


    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchperson?person_filter=" + filter + "&person=" + person + "&emp_code=" + empcode, true);
  xhttp.send();
}



$(function() {
  $('#btnLaunch').click(function() {
    $('#myModal_Search_MD').modal('show');
  });

  $('#btnSave').click(function() {
    var value = $('input').val();
    $('#doctor_name').val(value);;
    $('#myModal_Search_MD').modal('hide');
  });
});		
		
		

$(function() {
  $('#btnLaunch2').click(function() {
    $('#myModal_Search_MD').modal('show');
  });


});		
		
		
function useMD(id) {
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		//document.getElementById(xhttp.responseText['id']);
		
        //alert(xhttp.responseText['id']);
		var obj = JSON.parse(xhttp.responseText);
		
		
		$("#doctorname").val(obj.mdname);
		$("#doctorname2").val(obj.mdname);
		$("#doctor_emp_id").val(obj.employee);
		$("#doctor_emp_id2").val(obj.employee);
		
		
		/*$("#patient_tagging_md_class").val(obj.mdclass);
		$("#patient_tagging_specialty").val(obj.specialty);
		$("#patient_tagging_doctorid").val(obj.doctorid);
		$("#patient_tagging_emp_doctorid").val(obj.employee);
		//document.getElementById("md_name").value()
		*/

		
		
		
		
		
        //alert(obj.mdname);
		
    }
  };


  //xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + ""+ "&sku_id=" + skuid +  "&qtytabs=" + $("#redeemable_tabs").val() + "&patient_id=" + $('#patient_id').val(), true);
  xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/get_national_md_record?national_md_id=" + id , true);
  xhttp.send(); 
}		
		
		

</script>











@if (session('status'))
<script type="text/javascript">
  $('#success').modal('show');
</script>
@endif


@endsection