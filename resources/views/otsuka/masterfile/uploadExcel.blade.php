@extends('otsuka.settings.master')


@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10">
			<div class="panel panel-default">
				<div class="panel-heading">File Upload</div>
				<div class="panel-body">
					<form method="POST" action="{{ route('otsuka.upload.uploadExcel') }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group">
							<label>Upload Excel File: </label>
							<input type="file" name="uploadExcel" id="uploadExcel" required>
						</div>
						<!-- <div class="progress">
							<div id="progress_bar" class="progress-bar progress-bar-stripped">
								<div id="percentage">0%</div>
							</div>
						</div> -->
						<button id="submit" class="btn btn-primary btn-sm" type="submit">Submit</button>
					</form> 
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer-script')

@endsection