@extends('otsuka.master')

@section('Patient MasterFile')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
	$baseurl = URL::asset('/'); 
   ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}
th {
    text-align: center;
}
td {
	text-align: center;
}

</style>

  <!-- Modal -->
  <div class="modal fade" id="upload" role="dialog">
    <div class="modal-dialog modal-xl">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header modal-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Encoding</h4>
        </div>
        <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
          	<div class="col-md-4">

                <div class="panel panel-info">
                  <div class="panel-heading panel-info">Images</div>
                  <div class="panel-body">  
                    <div class="col-md-6">
                      <div class="zoom thumbnail">
                        <img class="img-responsive" id="img-zoom" src="">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="thumbnail">
                         <img id="img-click" class="img-responsive img-popup" src="" onclick="clicked(this)">
                       </div>
                    </div>
                  </div>
                </div>

          	<form action="{{route('otsuka.masterfile.queues-done-save')}}" id="enroll-form">
			{{ csrf_field() }}
          	<input type="hidden" id="upload_id" name="upload_id" value="">
            Patient Kit Number: <input type="text" class="form-control input-sm" id="patient_kit_number" name="patient_kit_number" value="">
          	Product: <input type="text" class="form-control input-sm" id="medicine" readonly="" name="medicine" value="">
			     Patient Name: <input type="text" class="form-control input-sm" name="patient_fullname" id="patient_fullname" required="" value="">
            Mobile Number: <input type="tel" class="form-control input-sm" name="mobile_number" id="mobile_number" required="" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="">
            Mobile Number 2: <input type="tel" class="form-control input-sm " name="mobile_number_2"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="mobile_number_2" value="">
            Landline: <input type="tel" class="form-control input-sm" name="phone_number"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="phone_number" value="">
      		Doctors Complete Name: <input type="text" class="form-control input-sm" name="doctor_name" id="doctor_name" value="">
            <input type="hidden"" name="doctor_id" id="doctor_id" value="">
            Hospital/Clinic's Name: <input type="text" class="form-control input-sm" name="hospital_name" id="hospital_name" value="">
           </div>
           <div class="col-md-8">
           <div class="panel panel-info">
            <div class="panel-heading panel-info">
              <label for="searchbox" >Search Doctor:  </label>
              <input type="text" id="searchbox2" class="form-control" style="display:inline;width:20%"/>
              <input type="hidden" id="person2" value = "globalmdv2">
              <a href="#" class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc2($('#searchbox2').val(),$('#person2').val())"></a>
            </div>     
            <div class="panel-body" id="filterdisplay2" style="overflow: scroll;text-align:center">
            </div>
           </div> 
           </div>
       </div>
       	</div>
        </div>
        <div class="modal-footer modal-info">
          <button type="submit" class="btn btn-primary">Save changes</button>	
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      	</form>
        </div>
      </div>
      
    </div>
  </div>



<div class="col-md-12">
<div class="panel panel-default">
		

    <div class="panel-heading">
    	<strong> Edit Queue's Doctor <span id="enroll_return_msg"></span></strong>
    </div>
    <div class="panel-body" id="searchpatient">

		<div class="row">
			<div class="col-md-12">
			
						<label for="searchbox" >Search :	</label>
						<input type="text" id="searchbox" class="form-control input-sm" style="display:inline;width:20%"/>
						<select class="form-control input-sm" style="display:none;width:20%" id="person"><option value="queue">queues_done</option></select>
						<button class="btn-info btn-sm glyphicon glyphicon-search" onclick="loadDoc($('#searchbox').val(),$('#person').val())"></button>
<!-- 						<button class="btn-info btn-sm glyphicon glyphicon-plus" style="float:right"  data-toggle="modal" data-target="#myModal" onclick="$('#action_field').val('save');">Enroll</button> -->
			  <br/>
			  
					<div style="overflow-x: scroll;" >
						
						<div class="panel-body" id="filterdisplay" >
								
						
						</div>
						
					</div>	

			  
			</div>
			
		</div>
      <hr>
	
	


    </div>
</div>
</div>


@endsection 

@section('footer-scripts')
<script>

function clicked(address) {

title = "Images";
w = 500;
h = 600;
var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);

    popup = window.open(address.src, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left); // display popup // This is where the image url goes which will just open up the image

}

function loadDoc(filter,person) {
	$('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif" style="width:100px;"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
        document.getElementById("filterdisplay").innerHTML = xhttp.responseText;

		$('#patient_search').DataTable();
	
			
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchperson?person_filter=" + filter + "&person=" + person, true);
  xhttp.send();
}

function loadDoc2(filter,person) {
  $('#filterdisplay2').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif" style="width:100px;"> loading...');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    
    
        document.getElementById("filterdisplay2").innerHTML = xhttp.responseText;
    
    	$('#example2').DataTable(); 
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/searchperson?person_filter=" + filter + "&person=" + person, true);
  xhttp.send();
}

function useMD2(id) {
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		//document.getElementById(xhttp.responseText['id']);
		
        //alert(xhttp.responseText['id']);
		var obj = JSON.parse(xhttp.responseText);
		//document.getElementById("md_name").value()
    $("#patient_kit_number").val(obj.patient_kit_number);
		$("#patient_fullname").val(obj.patient_name);
		$("#mobile_number").val(obj.mobile_number);
		$("#mobile_number_2").val(obj.mobile_number_2);
		$("#phone_number").val(obj.phone_no);
		$("#hospital_name").val(obj.hospital_name);
		$("#doctor_name").val(obj.doctor_name);
		$("#doctor_id").val(obj.doctor_id);
		$("#medicine").val(obj.medicine);
		$("#upload_id").val(obj.id);
    $("#img-zoom").attr('src', "{{$pic}}/" + obj.filename);
    $("#img-click").attr('src', "{{$pic}}/" + obj.filename);
    $("a.filename").atrr('href', "{{$pic}}/" + obj.filename);
		
		
		
		
		
        //alert(obj.mdname);
		
    }
  };


  //xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + ""+ "&sku_id=" + skuid +  "&qtytabs=" + $("#redeemable_tabs").val() + "&patient_id=" + $('#patient_id').val(), true);
  xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/get_upload_record?id=" + id , true);
  xhttp.send(); 
}

  function useMD(id) {
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		//document.getElementById(xhttp.responseText['id']);
		
        //alert(xhttp.responseText['id']);
		var obj = JSON.parse(xhttp.responseText);
		//document.getElementById("md_name").value()

		$("#hospital_name").val(obj.hospital);
		$("#doctor_name").val(obj.mdname);
		$("#doctor_id").val(obj.doctorid);

		
		
		
		
		
        //alert(obj.mdname);
		
    }
  };



  //xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + ""+ "&sku_id=" + skuid +  "&qtytabs=" + $("#redeemable_tabs").val() + "&patient_id=" + $('#patient_id').val(), true);
  xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/get_national_md_record?national_md_id=" + id , true);
  xhttp.send(); 
}

function updatepatient(refid) {	
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		
		
        document.getElementById("modalupdate").innerHTML = xhttp.responseText;
		 
		$( "#birthdate_edit" ).datepicker();
 
		//$('#patient_search').DataTable();
	
			
    }
  };
xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/displaypatient?refid=" + refid, true);
  xhttp.send();
}



function popupwindow(url) {
	
	title = "Images";
	w = 500;
	h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}



$(document).ready(function() {

  $('#btnLaunch').click(function() {
    $('#myModal_Search_MD').modal('show');
  });	

  $('#doctor_wont_disclose').click(function(){
    if($('#md_name').prop('disabled'))
    {
     $('#md_name').prop('disabled', false)
    }
    else{
         $('#md_name').prop('disabled', true)
      }
    });

$("#btn_enroll_save").click(function(){
		//$('#filterdisplay').html('<img src="http://gifimage.net/wp-content/uploads/2017/08/loading-animated-gif-17.gif"> loading...');
        var x = $("#enroll-form").serialize();
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {

			document.getElementById("enroll_return_msg").innerHTML = "<span style='background-color:#red'>Save</span>"; //xhttp.responseText;
			
			$(location).attr('href', "<?php echo $baseurl;?>otsuka/patient");
			
			}
		};
		xhttp.open("GET", "<?php echo $baseurl;?>otsuka/patient/enroll_patient?action=save&"   + x , true);
		xhttp.send();
		  
		  
    });



$(".datatable").DataTable();

});

</script>
@endsection

