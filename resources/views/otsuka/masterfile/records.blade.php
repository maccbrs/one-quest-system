<?php $asset = URL::asset('/'); $request = Request::instance(); ?> 
@extends('otsuka.fullview')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
      $baseurl = URL::asset('/'); 
  ?>
  
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}

table.dataTable tbody th, table.dataTable tbody td {
    padding: 5px 10px;
}

</style>

	<!-- Modal -->
	<div id="upload_allocation_file" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			
		  </div>
		  <div class="modal-body" id="UploapSupportingDocsDiv">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">File Upload</div>
						<div class="panel-body">
							<form method="POST" action="{{ route('otsuka.upload.allocationFile') }}" enctype="multipart/form-data">
								{{ csrf_field() }}
								<div class="form-group">
									<label>Upload Excel File: </label>
									<input type="file" name="uploadExcel" id="uploadExcel" required>
								</div>

								<button id="submit" class="btn btn-primary btn-sm" type="submit">Submit</button>
							</form> 
						</div>
					</div>
				</div>
			</div>
			
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>
 
 	<!-- Modal UPDATE-->
	<div id="update_alloc" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		<form method="POST" action="{{ route('otsuka.masterfile.patient-kit-allocation-save') }}" enctype="multipart/form-data">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			
		  </div>
		  <div class="modal-body" >
			<div class="row">
				<div class="col-md-12">
							
							{{ csrf_field() }}
							
							@foreach($columns as $key => $val)
							<div class="col-md-3">
							  <div class="form-group">
							 
								<label for="email" style="font-size: 10px;">{{strtoupper(str_replace("_"," ",$val))}}:</label>
								<input type="text" class="form-control input-sm" name="{{$val}}"  id="{{$val}}">
								
							  </div>
							 </div> 
							 @endforeach
							
							
					
					
						
					
				</div>
			</div>
			
			
		  </div>
		  <div class="modal-footer">
			<input type="hidden" class="form-control input-sm" name="id"  id="id">
			<button id="submit" class="btn btn-primary btn-sm" type="submit">Submit</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
			</form>
	  </div>
	</div>
 
 
 
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header no-bg b-a-0">
			

            </div>
            <div class="card-block" >
			
            <div class="row media">
				
				<div class="col-md-12">
							@if (\Session::has('return_msg'))
							<div class="alert  return_msg <?php  if(\Session::get('return_result') == '1') {echo 'alert-success';} else {echo 'alert-danger';} ?>">
								<ul>
									<li>{!! \Session::get('return_msg') !!}</li>
								</ul>
							</div>
							@endif
					<div class="panel panel-default">
					  <div class="panel-heading"> <span  class="btn btn-info" onclick="filter_alloc()">Filter  </span> 
						<div class="col-md-3">
						<select name="filterby" id="filterby" class="form-control">
											<option 
											<?php 
											if(!empty($filter)) 
											{
												if($filter == 'all') 
												{
													echo 'selected';
												}
											} 
											
											?> value="all">All</option>
											@foreach($columns as $key => $val)
											<option 
											<?php 
											if(!empty($filter)) 
											{
												if($filter == $val) 
												{
													echo 'selected';
												}
											} 
											
											?> 
											
											value="{{$val}}">{{strtoupper(str_replace("_"," ",$val))}}</option>
											@endforeach
						</select>
						</div>
						<div class="col-md-3">
						<input type="text" name="filter_search" id="filter_search" class="form-control" value="<?php if(!empty($filter_search)) {echo $filter_search;} ?>"/>
						</div>
						<span href="#"   class="btn btn-sm btn-primary float-sm-right" style="float:right" data-toggle="modal" data-target="#upload_allocation_file">Upload</span> 
						<a href="http://oqs.onequest.com.ph/template/Allocation_Kit_Template.xlsx" style="float:right;"><img  src="http://oqs.onequest.com.ph/Images/excel.png" style="height:25px;width:25px;float:right;margin-right:15px;"><span style="margin-right:5px;"> Download Template</span></a>
					  
							
							
							
					  </div>
					 
					
					  <div class="panel-body">		
						
							<div style="overflow:auto">
								<table id="datatable" class=" table table-bordered" style="font-size:9px"> 
									<thead>
										<tr>
											<th>#</th>
											@foreach($columns as $key => $val)
											<th>{{strtoupper($val)}}</th>
											@endforeach
											
											

										</tr>
									</thead>
									<tbody>
										
											@foreach($data as $key2 => $record)
										<tr>
											<td>
												
												<a href="#" data-toggle="modal" data-target="#update_alloc"  onclick="update_allocation_kit({{$record->id}})" class="">Update</a></td>
											
												@foreach($columns as $key => $column)
													
													<td nowrap>{{$record->$column}}</td>
												
												@endforeach
											
										</tr>
										@endforeach
										
									</tbody>
									

								</table>
							</div>	
						</div>
					</div>
					
				</div>
				

            </div>

            </div>
        </div>
    </div>
</div>



 <!-- Modal -->
  <div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
		 @if (session('status') == 'success')
          <div class="alert alert-success">
        
				<strong>{{session('message')}}</strong> 
		
          </div>
		@elseif (session('status') == 'failed')
		 <div class="alert alert-danger">
        
				<strong>{{session('message')}}</strong> 
		
          </div>
        @endif   
          </div>         
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div> 
    </div>
  </div>
 
 
 
@endsection




@section('header-scripts')

@endsection

@section('footer-scripts')

<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>

<script>

$(document).ready(function() {
	
	

    $('#datatable tfoot th').each( function (i) {
        var title = $('#datatable thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" data-index="'+i+'" />' );
    } );

var today = new Date();
var dd = ("0" + (today.getDate())).slice(-2);
var mm = ("0" + (today.getMonth() + 1)).slice(-2);
var yyyy = today.getFullYear();

var table = $('#datatable').DataTable({
	lengthMenu: [[10, 50, 100, 200, 500, -1], [10,50, 100, 200, 500, "All"]],
    stateSave: true,
  
  "search": {
  
  },
  dom: 'Blfrtip',
  buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                filename: 'Patient Kit Allocation '+yyyy+mm+dd,
                exportOptions: {
                columns: [ ':visible' ]
                }
            },
            'colvis'
        ]
});

   $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
        table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );

});

setTimeout(function() {
    $('.return_msg').slideUp();
}, 2000); // <-- time in milliseconds

$('.datatable').DataTable();

function filter_alloc() {


$(location).attr('href', "<?php echo $baseurl;?>otsuka/masterfile/patient-kit-allocation?filter=" + $("#filterby").val() + "&filter_search=" + $("#filter_search").val() );

}

function update_allocation_kit(id)  {
 var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		//document.getElementById(xhttp.responseText['id']);
		
        //alert(xhttp.responseText['id']);
		var obj = JSON.parse(xhttp.responseText);
		
							$("#id").val(id); 
							@foreach($columns as $key => $val)
							
							 
								//{{strtoupper(str_replace("_"," ",$val))}}
								$("#{{$val}}").val(obj.{{$val}}); 
							
							 @endforeach
		
		
		
		
        //alert(obj.mdname);
		
    }
  };


  //xhttp.open("GET", "<?php echo $baseurl;?>otsuka/redemption/dispatch-save?person_filter="  + "&dsid_mst="  + x + ""+ "&sku_id=" + skuid +  "&qtytabs=" + $("#redeemable_tabs").val() + "&patient_id=" + $('#patient_id').val(), true);
  xhttp.open("GET", "<?php echo $baseurl;?>otsuka/ajax/get_patient_kit_record?national_md_id=" + id , true);
  xhttp.send(); 
}



</script>





@if (session('status'))
<script type="text/javascript">
  $('#success').modal('show');
</script>
@endif


@endsection