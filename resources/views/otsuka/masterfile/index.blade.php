<?php $asset = URL::asset('/'); $request = Request::instance(); ?> 
@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}

</style>
 
 
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header no-bg b-a-0">
			

            </div>
            <div class="card-block" >
			
            <div class="row media">
				
				<div class="col-md-12">
							@if (\Session::has('return_msg'))
							<div class="alert  return_msg <?php  if(\Session::get('return_result') == '1') {echo 'alert-success';} else {echo 'alert-danger';} ?>">
								<ul>
									<li>{!! \Session::get('return_msg') !!}</li>
								</ul>
							</div>
							@endif
					<div class="panel panel-default">
					  <div class="panel-heading"> <span>Master File </span> </div>
					  <div class="panel-body">		
						
						<table class="datatable display compact table-bordered" style="font-size:9px">
							<thead>
								<tr>
									<th>Master File</th>										
									<th>Details</th>
									<th>#</th>
									
									

								</tr>
							</thead>
							<tbody>					
									
										<tr>
										<td>Patient Kit Allocation</td>
										<td>Use this to update MR on specific patient kit number.</td>
										<td><a href="{{$asset}}otsuka/masterfile/patient-kit-allocation" class="btn btn-primary btn-sm">Go</a></td>
										</tr>

										<tr>
										<td>Patient Kit Allocation Reserve</td>
										<td></td>
										<td><a href="{{$asset}}otsuka/masterfile/patient-kit-allocation-reserve" class="btn btn-primary btn-sm">Go</a></td>
										</tr>

										<tr>
										<td>National MD List</td>
										<td>Use this to update National MD List.</td>
										<td><a href="{{$asset}}otsuka/masterfile/national-md-list" class="btn btn-primary btn-sm">Go</a></td>
										</tr>

										<tr>
										<td>OQPXD SERIES (NOT YET VERIFIED)</td>
										<td>Use this to update wrong encoding for non-verified patients.</td>
										<td><a href="{{route('otsuka.masterfile.queues-done')}}" class="btn btn-primary btn-sm">Go</a></td>
										</tr>

										<tr>
										<td>OQPXN SERIES (VERIFIED)</td>
										<td>Use this to update wrong encoding for verified patients.</td>
										<td><a href="#" class="btn btn-primary btn-sm">Go</a></td>
										</tr>
										
										<tr>
										<td>SKU Groupings</td>
										<td><a href="{{route('otsuka.sku-groupings')}}">{{route('otsuka.sku-groupings')}}</a></td>
										<td></td>

										</tr>
								
								
							</tbody>

						</table>
						</div>
					</div>
					
				</div>
				

            </div>

            </div>
        </div>
    </div>
</div>



 <!-- Modal -->
  <div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
		 @if (session('status') == 'success')
          <div class="alert alert-success">
        
				<strong>{{session('message')}}</strong> 
		
          </div>
		@elseif (session('status') == 'failed')
		 <div class="alert alert-danger">
        
				<strong>{{session('message')}}</strong> 
		
          </div>
        @endif   
          </div>         
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div> 
    </div>
  </div>
 
 
 
@endsection




@section('header-scripts')

@endsection

@section('footer-scripts')

<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>

<script>

setTimeout(function() {
    $('.return_msg').slideUp();
}, 2000); // <-- time in milliseconds

$('.datatable').DataTable({
	"ordering": false,
});




function myFunction() {
    var pass1 = document.getElementById("new_password1").value;
    var pass2 = document.getElementById("new_password2").value;
    var ok = true;
    if (pass1 != pass2) {
        //alert("Passwords Do not match");
        document.getElementById("new_password1").style.borderColor = "#E34234";
        document.getElementById("new_password2").style.borderColor = "#E34234";
        ok = false;
    }
    else {
        alert("Passwords Match!!!");
    }
    return ok;
}
</script>





@if (session('status'))
<script type="text/javascript">
  $('#success').modal('show');
</script>
@endif


@endsection