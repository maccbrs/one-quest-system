@extends('otsuka.master')

@section('content')
 <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');
   $Helper = new AppHelper;
   ?>

<div class="container-fluid">
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><strong> UPLOADS OF MEDREP</strong></div>
  		<div class="panel-body">
  			<div class="row">
			<div class="col-md-6">
				<form method="POST" action="{{ route('otsuka.uploads.postbymedrep') }}">
					{{ csrf_field() }}
					<div class="form-group">
					<label class="">Select User: </label>
					<select id="filter_name" name="filter_name" class="form-control" required>
						<option>Select</option>
						@foreach($users as $user)
						<option value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
					<hr>

					<label>Select Date: </label>
					<div class="row">
						<div class="col-md-6">
					<input id="filter_from" class="form-control" type="text" name="filter_from" placeholder="From" autocomplete="off">
						</div>
						<div class="col-md-6">
					<input id="filter_to" class="form-control" type="text" name="filter_to" placeholder="To" autocomplete="off">
						</div>
					</div>
					<br>
					<button type="submit" class="btn btn-success">Apply</button>
					</div>
				</form>
			</div>

			</div>

				<div class="col-md-12">
						<table id="table_id" class="display datatable compact table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>ID</th>
									<th>Date/Time</th>
<!-- 									<th>Filename</th> -->
									<th>Type</th>
									<th>Status</th>
									<th>Px Name</th>
									<th>Contact Number</th>
									<th>Image</th>
								
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Date/Time</th>
<!-- 									<th>Filename</th> -->
									<th>Type</th>
									<th>Status</th>
									<th>Px Name</th>
									<th>Contact Number</th>
									<th>Image</th>
									
								</tr>
							</tfoot>
							<tbody>
								@if(isset($myUpload))
								@foreach($myUpload as $d)
										<tr>
											<td> {{ $d->uploaded_by}}</td>
											<td>
												{{ $d->created_at }}
											</td>
											<td> @if($d->type == 'Enroll') Retrieval @else {{ $d->type }} @endif</td>
											<td>
											@if($d->claimed == 1 and $d->submit == 0 and $d->trail_status !== 199)In Process
											@elseif($d->claimed == 0 and $d->submit == 0 and $d->trail_status !== 199) On queue
											@elseif($d->claimed == 1 and $d->submit == 1 and $d->trail_status !== 199) Encoded
											@elseif($d->claimed == 1 and $d->submit == 1 and $d->trail_status == 199) Invalid - {{ $d->remarks }}
											@endif
											</td>
											<td>{{!empty($d->patient_name) ? $d->patient_name : 'Not Given' }}</td>
											<td><b>Mobile No.: </b>{{!empty($d->mobile_number) ? $d->mobile_number : 'N/A'}}<br>
												<b>Landline No.: </b>{{!empty($d->phone_no) ? $d->phone_no : 'N/A'}}
											</td>
											<td nowrap>
												<a href="#" onclick="popupwindow('{{$pic.'/'.$d->filename}}');"><img src="{{$pic.'/'.$d->filename}}" class="img-responsive img-thumbnail" style="max-height: 120px "/></a>

											</td>

										</tr>
								@endforeach
								@endif
							</tbody>
						</table>
			</div>
			
  		</div>
	</div>
</div>
</div>

@endsection

@section('footer-scripts')

<script type="text/javascript">
	$(document).ready(function (){
		$('#filter_from, #filter_to').datepicker();
		$('#table_id').DataTable();

	})

</script>

@endsection