@extends('otsuka.settings.master')


@section('content')
<div class="container-fluid">
	<div class="row">
		@if(session()->has('addsuccess'))
		<div id="add" class="col-sm-12">
			<div class="alert alert-success">
				{{ session()->get('addsuccess') }}
			</div>
		</div>
		@endif
		@if(session()->has('updatesuccess'))
		<div id="update" class="col-sm-12">
			<div class="alert alert-success">
				{{ session()->get('updatesuccess') }}
			</div>
		</div>
		@endif
		@if(session()->has('deletesuccess'))
		<div id="delete" class="col-sm-12">
			<div class="alert alert-danger">
				{{ session()->get('deletesuccess') }}
			</div>
		</div>
		@endif

		<div class="col-md-12">
			<h3 class="pull-left">System Settings</h3>
			<button class="btn btn-info pull-right btn-sm" data-toggle="modal" data-target="#addmodal">Add</button>
		</div>
			<hr>
			<div class="col-md-12">
				<div class="table-responsive">
				<table id="systems_table" class="table-bordered">
				<thead>
					<tr>
						<th style="text-align: center;">Action</th>
						<th style="text-align: center">Key</th>
						<th style="text-align: center">Value</th>
						<th style="text-align: center">Remarks</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $d)  
					<tr>
							<td>
							<center>
							<a id="modalupdate" class="btn btn-warning btn-xs updatemodal" data-toggle="modal" data-target="#updatemodal" data-key="{{ $d->key }}" data-value="{{ $d->value }}" data-remarks="{{ $d->remarks }}" data-id="{{ $d->id }}" title="Update">
									<span class="glyphicon glyphicon-edit"></span>
							</a>
							<a id="modaldelete" class="btn btn-danger btn-xs updatemodal" data-toggle="modal" data-target="#deletemodal" data-name="" data-id="{{ $d->id }}" title="Delete">
									<span class="glyphicon glyphicon-trash"></span>
							</a>
						</center>
						</td>
						<td>{{ $d->key }}</td>
						<td style="width: 10px;">{{ $d->value }}</td>
						<td>{{ $d->remarks }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				</div>
			</div>
	</div>

		<!-- Update Modal -->
		<div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Update</h5>
					</div>
					<div class="modal-body">
						<form action="{{ route('otsuka.settings.updatesystem') }}" method="POST">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									<input type="hidden" id="hidden_id" name="hidden_id">
									<label>Key: </label><input type="text" name="update_key" id="key" class="form-control">
									<label>Value: </label><input type="text" name="update_value" id="value" class="form-control">
									<label>Remarks: </label><input type="text" name="update_remarks" id="remarks" class="form-control">
									</div>
								</div>
							</div>		
						</div>
						<div class="modal-footer">
							<div id="buttons">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary">Save</button>
							</div>

						</div>
						
					</form>
				</div>
			</div>
		</div>

		<!-- Add Modal -->
		<div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Add</h5>
					</div>
					<div class="modal-body">
						<form action="{{ route('otsuka.settings.addsystem') }}" method="POST">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									
									<label>Key: </label><input autocomplete="off" type="text" name="key" class="form-control" required autofocus>
									<label>Value: </label><input autocomplete="off" type="text" name="value" class="form-control">
									<label>Remarks: </label><input autocomplete="off" type="text" name="remarks" class="form-control">
									</div>
								</div>
							</div>		
						</div>
						<div class="modal-footer">
							<div id="buttons">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary">Add</button>
							</div>

						</div>
						
					</form>
				</div>
			</div>
		</div>

		<!-- Delete Modal -->
		<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Delete</h5>
					</div>
					<div class="modal-body">
						<form action="{{ route('otsuka.settings.deletesystem') }}" method="POST">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									<input type="hidden" id="delete_id" name="delete_id">
									<label>Delete this item?</label>
									</div>
								</div>
							</div>		
						</div>
						<div class="modal-footer">
							<div id="buttons">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
								<button type="submit" class="btn btn-primary">Yes</button>
							</div>

						</div>
						
					</form>
				</div>
			</div>
		</div>

</div> <!-- container -->
@endsection

@section('footer-scripts')
	<script type="text/javascript">
		$(document).ready(function (){
			$('#systems_table').DataTable();

			$('#updatemodal').on('show.bs.modal', function(e) {

				var key = $(e.relatedTarget).data('key');
				var value = $(e.relatedTarget).data('value');
				var remarks = $(e.relatedTarget).data('remarks');
				var id = $(e.relatedTarget).data('id');
				
				$('#hidden_id').val(id);
				$('#value').val(value);
				$('#key').val(key);
				$('#remarks').val(remarks);
			});

			$('#deletemodal').on('show.bs.modal', function(e) {

				var id = $(e.relatedTarget).data('id');
				$('#delete_id').val(id);
			});


			setTimeout(function() {
				$('#add,#update,#delete').slideUp();
}, 2500); // <-- time in milliseconds



		})	
	</script>

@endsection