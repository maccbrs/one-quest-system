@extends('otsuka.master')


@section('content')


<div class="container-fluid">
	<div class="row">

		@if(session()->has('success'))
		<div id="success" class="col-sm-12">
			<div class="alert alert-success">
				{{ session()->get('success') }}
			</div>
		</div>
		@endif
		@if(session()->has('add'))
		<div id="success" class="col-sm-12">
			<div class="alert alert-success">
				{{ session()->get('add') }}
			</div>
		</div>
		@endif

		<div class="col-md-12">
			<button style="display:none" class="btn btn-success pull-left btn-sm">Upload Excel</button>
			<button class="btn btn-info pull-right btn-sm" data-toggle="modal" data-target="#addmodal">Add User</button>
		</div>
		<hr>
		<div class="col-md-12">
			<table id="users" class="table table-bordered global_datatable">
				<thead>
					<tr>
						<th>Name</th>
						<th>Username</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $d)
					<tr>
						<td>{{$d->name}}</td>
						<td>{{$d->username}}</td>
						<td>{{$d->email}}</td>
						<td>
							<center>
								<a id="modalupdate" class="btn btn-primary btn-sm updatemodal" data-toggle="modal" data-target="#updatemodal" data-name="{{$d->name}}" data-id="{{$d->id}}" title="Reset Password">
									<span class="glyphicon glyphicon-edit"></span>
								</a>
								
							</td></center>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

		<!-- Update Modal -->
		<div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Reset Password</h5>
					</div>
					<div class="modal-body">
						<form action="{{ route('otsuka.settings.resetpassword') }}" method="POST">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12">
									<input type="hidden" id="hidden_id" name="hidden_id">
									<p class="form-group">Are you sure you want to reset password for <strong><span id="name"></span></strong>?</p> 
								</div>
							</div>		
						</div>
						<div class="modal-footer">
							<div id="buttons">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
								<button type="submit" class="btn btn-primary">Yes</button>
							</div>

						</div>
						
					</form>
				</div>
			</div>
		</div>

		<!-- Add Modal -->
		<div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Add User</h5>
					</div>
					<form action="{{ route('otsuka.settings.adduser') }}" method="POST">
						{{ csrf_field() }}
						<div class="modal-body">
							<div class="container-fluid">
							<div class="row">
								<div class="form-group">
										<label>Employee Code:</label>
										<input type="text" class="form-control" name="empcode" placeholder="Emp Code" required autofocus>
										<label>First Name:</label>
										<input type="text" class="form-control" name="firstname" placeholder="First Name" required>
										<label>Middle Name:</label>
										<input type="text" class="form-control" name="middlename" placeholder="Middle Name">
										<label>Last Name:</label>
										<input type="text" class="form-control" name="lastname" placeholder="Last Name" required>
								</div>
							</div>

						</div>
					</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div> <!-- container -->

	@endsection

	@section('footer-scripts')
	<script type="text/javascript">
		$(document).ready(function (){

			$('#updatemodal').on('show.bs.modal', function(e) {

				var name = $(e.relatedTarget).data('name');
				var id = $(e.relatedTarget).data('id');
				$('#name').text(name);
				$('#hidden_id').val(id);
			});

			$('#updatemodal').on('hidden.bs.modal', function () {
				$('#buttons').show();
				$('#confirmation').hide();
			});


			setTimeout(function() {
				$('#success').slideUp();
}, 2500); // <-- time in milliseconds

		})

	</script>

	@endsection