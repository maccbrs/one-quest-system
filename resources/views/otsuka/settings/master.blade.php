<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type: application/json'); ?>
<?php $assets = URL::asset('/');?>
<?php $pic = URL::asset('/uploads/');?>
<?php $Auth = Auth::user()->id;?>
<?php $GemUser = new \App\Http\Models\gem\GemUser;?>
<?php $avatar =  $GemUser->where('id','=',$Auth)->get()?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @yield('header-css')

  <link rel="stylesheet" href="{{$assets}}inventory/css/bootstrap.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/css/buttons.dataTables.css">
  	    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 
  <style>
    hr.style1{
  border-top: 1px solid #D3D3D3;
    }

    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 667px;} 
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#0066ff;"><img style="max-width: 60px; margin-top:-5px;" src="{{$pic.'/OQS.jpg'}}"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
   
   
      </ul>
      <ul class="nav navbar-nav navbar-right">
		<li><a href={{url('/otsuka/profile')}}><span class="glyphicon glyphicon-user"></span> Profile</a></li>
        <li><a href={{url('/logout')}}><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>

    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">

      <div class="user-info">
        <div class="admin-image"><img style="width:100px; height:100px; margin-bottom: 25px;" src="{{$pic.'/users/l-'.$avatar[0]['avatar']}}" alt=""></div>
          <div class="admin-action-info"></div>
      </div>

      <div class="col-sm-12" style="margin-bottom:50px; background-color: white; border-bottom: solid #D3D3D3;  border-top: solid #D3D3D3;">
      <span><br><br><b>Welcome to OQS!</b>
        <br><br>{{Auth::user()->name}}<br><br>
        @if(Auth::user()->user_type == 'medrep')<p style="font-size:12px;"><b>Employee ID:</b></p>
        @elseif(Auth::user()->user_type == 'agent')Agent ID: 
        @endif<p>{{Auth::user()->username}}</p></span><br><br><br>
      </div>
    
     
    </div>
    <div class="col-sm-10 text-left custom" style="margin-top: 20px;"> 
    	@yield('content')
    </div>
  </div>
</div>

<!-- <footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer> -->




<!-- Modal -->
<div id="dispatchedreportmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Generate Report</h4>
      </div>
      <div class="modal-body">
			     <form method="get" action="{{route('otsuka.redemption.dispatch-report')}}" class="form-horizontal p-10">
                {{ csrf_field() }}          
                <div class="row">
                    <div class="col-md-5">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">From: 
                                </label>
                                <div class="col-md-10">
                                    <input type="datetime" class="form-control datetimepicker"  placeholder="YYYY-MM-DD HH:MM:SS" id="filterfrom" name="dispatcheddatefrom">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-5">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">To:
                                </label>
                                <div class="col-md-10">
                                    <input type="datetime" class="form-control datetimepicker"  placeholder="YYYY-MM-DD HH:MM:SS" id="filterto" name="dispatcheddateto">
                                </div>
                            </div>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="form-group p-10 form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">Submit
                                </button>
                            </div>
                        </div>
                    </div>               
                </div>
           </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"  data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<!-- Modal -->
<div id="MTD_REDEMPTION" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Generate Report MTD REDEMPTION REPORT</h4>
      </div>
      <div class="modal-body">
			     <form method="get" action="{{route('otsuka.redemption.mtd-report')}}" class="form-horizontal p-10">
                {{ csrf_field() }}          
                <div class="row">
                    <div class="col-md-5">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">From: 
                                </label>
                                <div class="col-md-10">
                                    <input type="datetime" class="form-control datetimepicker"  placeholder="YYYY-MM-DD HH:MM:SS" id="filterfrom" name="from">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-5">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">To:
                                </label>
                                <div class="col-md-10">
                                    <input type="datetime" class="form-control datetimepicker"  placeholder="YYYY-MM-DD HH:MM:SS" id="filterto" name="to">
                                </div>
                            </div>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="form-group p-10 form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">Submit
                                </button>
                            </div>
                        </div>
                    </div>               
                </div>
           </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"  data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





</body>
      <script src="{{$assets}}inventory/js/jquery-3.2.1.js"></script>
      
	  
      <script src="{{$assets}}inventory/datatables/jquery-ui.js"></script>
      <script src="{{$assets}}inventory/datatables/bootstrap.min.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
      <script src="{{$assets}}inventory/datatables/jquery.dataTables.yadcf.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>
	 <script src="{{$assets}}JS/jquery.mask.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>

<script>
/*$(document).ready(function() 
{ 
 $('form').ajaxForm(function() 
 {
  alert("Uploaded SuccessFully");
 }); 
});*/
$(document).ready(function() {
	
  $('#users').DataTable({
  "ordering": true,
  "autoWidth" : true, 

});

$('#queue').DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
});

/*setTimeout(function() {
   location.reload();
   }, 10000);
*/
$('#example2').DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
});

$('#example3').DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
});
} );

$(function () {
    $('#pv').click(function () {
        window.open("{{ route('otsuka.pv') }}");
    });
});

$(function () {
    $('#pqc').click(function () {
        window.open("{{$assets}}uploads/PQC.html","","width=1280,height=1024");
    });
});

  $( function() {
    $( ".datepicker" ).datepicker();
  } );
function dispatchedreport() {
	alert($("#filterfrom").val());
	//window.open("{{$assets}}otsuka/redemption/dispatch-report?dispatcheddatefrom=" + $("#filterfrom").val() + "&dispatcheddateto=" +  + $("#filterfrom").val());
}


</script>

@yield('footer-scripts')

</html>