<button class="btn btn-default btn-sm" data-toggle="modal" data-target=".edit-image-modal">
  <i class="material-icons" aria-hidden="true">edit </i>
</button>
<div class="modal fade edit-image-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('otsuka.profile.update_pic')}}" enctype="multipart/form-data" novalidate>
        {{ csrf_field() }}   
        <input type="hidden" name="dept_to" value="">
        <input type="hidden" name="type" value="1">                      

          <div class="modal-body">            
            <fieldset class="form-group">
                <label for="content">Select File</label>
                <input type="file" name="file" class="btn btn-sm btn-default"> </span>
            </fieldset>                                  
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Post</button>
          </div>
        </form>
      </div>
  </div>
</div> 