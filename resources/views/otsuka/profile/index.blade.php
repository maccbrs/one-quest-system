<?php $asset = URL::asset('/'); $request = Request::instance(); ?> 
@extends('otsuka.master')

@section('Agent Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

.red {
  color: white !important;
  
  background-color: #d9534f!important
}
.yellow {
  background-color: yellow !important;
}
.orange {
  color: white !important;
  background-color: orange !important;
}

</style>
 
 
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header no-bg b-a-0">
              <h4>Edit Profile Picture {!!view('otsuka.profile.update_pic')!!}</h4>
            </div>
            <div class="card-block" >

            <div class="row media">
                <div class="col-md-3 col-sm-4 col-xs-6 media-item">
                    <a class="card card-block text-xs-center" href="javascript:;" title="caption image 1"><img class="img-fluid center-block" src="{{$pic.'/users/l-'}}{{Auth::user()->avatar}}">
                    </a>
                    <div class="text-xs-center">
                        <p class="m-b-0 bold">Profile Picture<span></span></p>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-header no-bg b-a-0"> 
              <h4>Change Password {!!view('otsuka.profile.password')!!} 
            </div>
            <div class="card-block" >

            </div>
        </div>
    </div>
</div>

 <!-- Modal -->
  <div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
		 @if (session('status') == 'success')
          <div class="alert alert-success">
        
				<strong>{{session('message')}}</strong> 
		
          </div>
		@elseif (session('status') == 'failed')
		 <div class="alert alert-danger">
        
				<strong>{{session('message')}}</strong> 
		
          </div>
        @endif   
          </div>         
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div> 
    </div>
  </div>
 
 
 
@endsection




@section('header-scripts')

@endsection

@section('footer-scripts')

<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>

<script>



function myFunction() {
    var pass1 = document.getElementById("new_password1").value;
    var pass2 = document.getElementById("new_password2").value;
    var ok = true;
    if (pass1 != pass2) {
        //alert("Passwords Do not match");
        document.getElementById("new_password1").style.borderColor = "#E34234";
        document.getElementById("new_password2").style.borderColor = "#E34234";
        ok = false;
    }
    else {
        alert("Passwords Match!!!");
    }
    return ok;
}
</script>
@if (session('status'))
<script type="text/javascript">
  $('#success').modal('show');
</script>
@endif


@endsection