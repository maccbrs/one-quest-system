<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type: application/json');

 $pic = URL::asset('/uploads/');?>
  
	@if(!empty($supporting_docs_list))





	
					
					<div class="col-md-12">
								@if (\Session::has('return_msg'))
								<div class="alert  return_msg <?php  if(\Session::get('return_result') == '1') {echo 'alert-success';} else {echo 'alert-danger';} ?>">
									<ul>
										<li>{!! \Session::get('return_msg') !!}</li>
									</ul>
								</div>
								@endif
						<div class="panel panel-default">
						  <div class="panel-heading"> <span>Upload Supporting Docs</span> <span style="font-size:10px" class="text-danger">NOTE: (.png or .jpg) are accepted</span></div>
						  <div class="panel-body" style="overflow:auto">		
							
							<table class="table table-bordered datatabledraw  compact nowrap" id="UploadSupportingDocsTable" style="font-size:9px"  >
								<thead>
									<tr>
										<th>Action</th>
										<th>UPLOADID</th>	
										<th>UPLOAD DATE</th>
										<th>Uploaded To</th>
										<th>Link</th>
										<th>Status</th>
										<th>Remarks</th>
										
										
										

									</tr>
								</thead>
								<tbody>
									
										@if(!empty($supporting_docs_list))
											@foreach($supporting_docs_list as $key => $val)
											<tr>
											<td><span  class="btn-sm btn btn-info UseUploadDocs" data-dismiss="modal" data-supp_docs = "{{$val->id}}"  onclick="assign_rx_upload('{{$skuid}}',$('#patient_id').val(),'{{$val->id}}','{{$programid}}')">Assign</span></td>
											<td>{{$val->id}}</td>
											<td>{{$val->created_at}}</td>
											<td>{{$val->uploaded_to}}</td>
											<td><a href="{{$pic.'/supportingdocs/' . $val->filename}}" target="_blank">View Image</a></td>
											<td>{{$val->frontend_status}}</td>
											<td>{{$val->remarks}}</td>
											</tr>
											@endforeach	
										@endif																												
								</tbody>

							</table>
							</div>
						</div>
						
					</div>



	
	@endif

					
					
                 
	 <script>

		
		function myscript() {
			
			//alert('yesy');
		}
	</script>
	

