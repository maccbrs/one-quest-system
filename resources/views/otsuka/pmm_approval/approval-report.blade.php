@extends('otsuka.blank')

@section('Patient Dashboard')
Dashboard
@endsection

@section('content')
  <?php $pic = URL::asset('/uploads/');ini_set('max_execution_time', 180);?>
  <?php $jsupload = URL::asset('/jsupload/');?>
<style type="text/css">
th { font-size: 12px; }
td { font-size: 11px; }

.med_logo img {
    width:100%;
    height:10%;
}

[hidden] {
  display: none !important;
}

.bar {
    height: 18px;
    background: green;
}
.modal-center {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

/**THE SAME CSS IS USED IN ALL 3 DEMOS**/    
/**gallery margins**/  
ul.gallery{    
margin-left: 3vw;     
margin-right:3vw;  
}    

.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(.5);    
-moz-transform: scale(.5);  
-webkit-transform: scale(.5);  
-o-transform: scale(.5);  
transform: scale(.5);    
position:relative;      
z-index:100;  
}

/**To keep upscaled images visible on mobile, 
increase left & right margins a bit**/  
@media only screen and (max-width: 768px) {   
ul.gallery {      
margin-left: 15vw;       
margin-right: 15vw;
}

/**TIP: Easy escape for touch screens,
give gallery's parent container a cursor: pointer.**/
.DivName {cursor: pointer}
}

  input {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
  }

 input.noborder {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 0;
  }

  div.border {
      border: 1px solid black;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 10px;
  }

  div.padding {
      padding-top: 40px;
      padding-right: 65px;
      padding-bottom: 40px;
      padding-left: 60px;
  }

  h2.space {
    line-height: .25;
  }

  p.ind {
    text-indent: 50px;
    line-height: .1;
  }

</style>

<div class="col-md-12">				
 <h2>Approval Report</h2>
<div class="col-md-12">
<div class="panel panel-default">
  <div class="panel-heading"></div>
  <div class="panel-body">
<table id="example" class="display table table-bordered compact nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>

	
				<TH >PMM NAME</TH>								
				<TH >PROGRAM NAME</TH>	
				<TH >MAXIMUM ALLOCATION</TH>
				<TH >TOTAL FREE GOODS ALREADY DISPATCHED</TH>
				<TH >PATIENT ID</TH>
				<TH >PATIENT NAME</TH>
				<TH >SKU PURCHASED</TH>
				<TH >TOTAL QUANTITY REMAINING PURCHASED</TH>
				<TH >REMAINING FREE GOODS TOTAL COUNT</TH>
				<TH >MD NAME</TH>	
				<TH >NO OF TAB PRESCRIBE</TH>					
				<TH >PRODUCT PRESCIBE</TH>				
				
				<!--<TH >O.R. TRANSACTION DATE</TH>
				<TH >O.R. NUMBER</TH>
				<TH >RECEIPT SUBMISSION DATE</TH> -->
				<TH >DATE OF SUBMISSION</TH>
				<TH >DATE OF APPROVAL</TH>
				<TH >2ND FOLLOW-UP</TH>
				<TH >3ND FOLLOW-UP</TH>
				<TH >DATE APPROVED</TH>
				<TH >DATE DISPATCHED</TH>
				
				
            </tr>
        </thead>
       
        </tfoot>
        <tbody>
			@if(!empty($lists))
			 @foreach($lists as $key => $val)
				
				<tr onclick="alert({{$val->id}})">
				

				<td>{{trim($val->pmm_name,'"')}} <?php /*/ @if(!empty($val->fetch_patient_info))
					{{$val->fetch_patient_info->created_by}} 
					@endif */?>
					
				</td><!-- <TH >PATIENT CODE</TH>	-->			
				<!--<td>{{$val->fetch_patient_info->patient_fullname}}  -->
				</td>			
				<td>{{$val->program_name}} </td> 
				<td>{{$val->max_allocation}}</td>
				<td>{{$val->total_free_goods_already_dispatch}}</td> 
				<td>{{$val->patient_code}}</td> <!-- <TH >(TAGGING) TEAM</TH>-->
				<td>{{trim($val->patient_name,'"')}}</td> <!-- <TH >(TAGGING) AREA_CODE</TH> -->
				<td>{{$val->sku_purchased}}</td>
				<!--<td>{{$val->total_quantity_remaining_purchased}}</td> -->
				<td>{{$val->total_receipt_qty}}</td>
				<td>{{intval($val->total_receipt_qty / $val->fetch_program->buy) * $val->fetch_program->free}}</td>
				<td>{{$val->fetch_patient_info->fetch_supp_docs->doctorname}} </td> 
				<td>{{$val->fetch_patient_info->fetch_supp_docs->number_of_tabs}} </td> 
				<td>{{$val->fetch_patient_info->fetch_supp_docs->product_prescribe}} </td> 		
				
				<!-- <td>{{$val->remaining_free_goods_total_count}}</td>-->
				<td>{{$val->created_at}}</td>
				<td>{{$val->date_dispatched}}</td>
				<td></td><!-- <TH >(TAGGING) MR_NAME</TH> -->	
				<td></td><!-- <TH >(TAGGING) MR_NAME</TH> -->
				<td></td><!-- <TH >(TAGGING) MR_NAME</TH> -->
				<td></td><!-- <TH >(TAGGING) MR_NAME</TH> -->
					
					
					
				
				
				@endforeach
				@endif
		   
				
               
           
            
           
        </tbody>
    </table>
</div>
</div>
</div>
</div>



@endsection 

@section('footer-scripts')
<script>
$(document).ready(function() {


    $('#example').DataTable({
        lengthMenu: [[10, 25, 50, 75, 100, -1], [10,25, 50, 75, 100, "All"]],
        pageLength: 215, 
        "scrollX": true,
        autoWidth: false,
        responsive: true,
        
        scrollCollapse: true,
        dom: 'Blfrtip',
        stateSave: true,
        buttons: [
            {
                extend: 'copyHtml5',
                title: "Reports" + " " + " ",
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            {
                extend: 'excelHtml5',
                title: "Reports" + " " + " ",
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            'colvis'
            ]
            });

 });
</script>


@endsection

