<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type: application/json'); ?>
<?php $assets = URL::asset('/');?>
<?php $pic = URL::asset('/uploads/');?>
<?php $Auth = Auth::user()->id;?>
<?php $GemUser = new \App\Http\Models\gem\GemUser;?>
<?php $avatar =  $GemUser->where('id','=',$Auth)->get()?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  @yield('header-css')

  <link rel="stylesheet" href="{{$assets}}inventory/css/bootstrap.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/css/buttons.dataTables.css">
  	    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 
  <style>
    hr.style1{
  border-top: 1px solid #D3D3D3;
    }

    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 667px;} 
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#0066ff;"><img style="max-width: 60px; margin-top:-5px;" src="{{$pic.'/OQS.jpg'}}"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
	      @if(Auth::user()->user_level != '3')
        <li><a href="{{route('otsuka.index')}}"><b>Enrollments</b></a></li>
        @if(Auth::user()->user_type == 'agent')
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Queue</b>
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li class="dropdown-header">UPLOAD</li>
            <li><a href="{{route('otsuka.dashboard.queue')}}">Upload Queue</a></li>
            <li class="divider"></li>
          <li class="dropdown-header">VERIFICATION</li>
            <li><a href="{{route('otsuka.dashboard.verification-queue')}}">Verification Queue</a></li>
            <li><a href="{{route('otsuka.dashboard.verification-callback-queue')}}">Verification Callback Queue</a></li> 
            <li class="divider"></li>
          <li class="dropdown-header">REDEMPTION</li>
            <li><a href="{{route('otsuka.dashboard.redemption-queue')}}">Redemption Queue</a></li> 
            <li><a href="{{route('otsuka.dashboard.supporting-docs-queue')}}">Supporting Docs Queue</a></li> 
            <li class="divider"></li>
          <li class="dropdown-header">COMPLIANCE</li> 
            <li><a href="{{route('otsuka.dashboard.compliance-queue')}}">Compliance Queue</a></li>
			<li><a href="{{route('otsuka.dashboard.compliance-ref-voluntary-queue')}}">Compliance Voluntary & Referral Queue</a></li>    			
        </ul>
    </li> 
    @endif
		 @if(Auth::user()->user_level == '2')<li><a href="{{route('otsuka.pqc-ev-report')}}"><b>AE/PQC</b></a></li>@endif
        <li><a href="{{route('otsuka.uploads')}}"><b>Uploads for the Day</b></a></li>
		@if(Auth::user()->user_type == 'agent')<li><a href="{{route('otsuka.patient')}}"><b>Patients</b></a></li>@endif
        <li><a href="#" onClick="MyWindow=window.open('http://oqs.onequest.com.ph:8080/index.php','MyWindow',width=600,height=300); return false;"><span class="glyphicon glyphicon-envelope"></span><b> STS</b></a></li>
		@if(Auth::user()->user_level == '2')
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Report</b>
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li class="dropdown-header">MTD RAW DATA</li>
           <li><a href="{{route('otsuka.generate-report')}}">MTD RETRIEVAL RAW DATA REPORT</a></li>
           <li><a href="{{route('otsuka.generate-invalid')}}">MTD RETRIEVAL RAW DATA INVALID REPORT</a></li>
           <li><a href="{{route('otsuka.generate-summary')}}">MTD SUMMARY RETRIEVAL REPORT</b></a></li> 
           <!--<li><a href="{{route('otsuka.redemption.mtd-report')}}">MTD REDEMPTION REPORT</a></li> -->
		   <li><a href="#" data-toggle="modal" data-target="#MTD_REDEMPTION">MTD REDEMPTION REPORT</a></li>
           <li class="divider"></li>
          <li class="dropdown-header">DISPATCH</li>
			     <li><a href="#" data-toggle="modal" data-target="#zpc_dispatch">ZPC - DISPATCH REPORT</a></li>	
           <li><a href="#" data-toggle="modal" data-target="#dispatchedreportmodal">DISPATCH REPORT</a></li>
           <li class="divider"></li>
          <li class="dropdown-header">DAILY</li>
           <li><a href="{{route('otsuka.generate-report-daily-call')}}">DAILY CALL RAW SUMMARY REPORT</a></li> 
           <li class="divider"></li>
          <li class="dropdown-header">COMPLIANCE</li>
           <li><a href="{{route('otsuka.compliance-report')}}">COMPLIANCE CALL REPORT</a></li> 
		   
		   <li class="divider"></li>
		   <li class="dropdown-header">PMM </li>
           <li><a href="{{route('otsuka.pmm-approval-report')}}">PMM REPORT</a></li> 
            <!-- <li><a href="{{route('otsuka.daily-individual')}}"><b>DAILY INDIVIDUAL REPORT</b></a></li>     -->
        <li class="divider"></li>
        <li class="dropdown-header">EOM</li>
        <li><a href="{{route('otsuka.generate-eom')}}">EOM REPORTS</a></li>
        </ul>
		</li> 
		@endif
<!--         <li><a href="#">Contact</a></li> -->
	  @if(Auth::user()->user_type == 'agent')<li><a href="{{route('otsuka.uploader.index')}}"><b>Rx Upload</b></a></li>@endif
	  @if(Auth::user()->user_type == 'agent')<li><a href="{{route('otsuka.masterfile')}}"><b>Master File</b></a></li>@endif	
	@endif
	 @if(Auth::user()->user_level == '3')<li><a href="{{route('otsuka.settings')}}"><b>Settings</b></a></li>
   <li><a href="{{route('otsuka.uploads.bymedrep')}}"><b>Uploads of Medrep</b></a></li>
   @endif

      </ul>
      <ul class="nav navbar-nav navbar-right">
		<li><a href={{url('/otsuka/profile')}}><span class="glyphicon glyphicon-user"></span> Profile</a></li>
        <li><a href={{route('otsuka.logout')}}><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">

      <div class="user-info">
        <div class="admin-image"><img style="width:100px; height:100px; margin-bottom: 25px;" src="{{$pic.'/users/l-'.$avatar[0]['avatar']}}" alt=""></div>
          <div class="admin-action-info"></div>
      </div>


<!--       <div class="col-sm-12" style="background-color:#D3D3D3">
        	<span class="col-sm-12"><b>@yield('title')</b></span>
      </div> -->
      <div class="col-sm-12" style="margin-bottom:50px; background-color: white; border-bottom: solid #D3D3D3;  border-top: solid #D3D3D3;">
      <div class="container-fluid">
      <span><b>Welcome to OQS!</b>
        <br><br>{{Auth::user()->name}}<br><br>
        @if(Auth::user()->user_type == 'medrep')<p style="font-size:12px;"><b>Employee ID:</b></p>
        @elseif(Auth::user()->user_type == 'agent')Agent ID: 
        @endif<p><b>{{Auth::user()->username}}</b></p></span>
      </div>
      </div>
     <!--  <p><a href="#">Link</a></p> -->
	@if(Auth::user()->user_level != '3')
      @if(Auth::user()->user_type == 'agent')
      <div class="row">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-12">
            <p style="background-color: gray; color: white;"><b>AE/PQC Forms</b></p>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
              <a href="{{ route('otsuka.pv') }}" class="btn btn-success btn-sm" role="button">AE Form</a>
            </div>
            <div class="col-sm-6">
              <a href="{{ route('otsuka.pqc') }}" class="btn btn-success btn-sm" role="button">PQC Form</a>
            </div>
        </div>
      </div>
      </div>
      <hr/>

      <div class="row">
      <div class="container-fluid">
      <form method="post" id="verify_encode" action="{{route('otsuka.send')}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
      <p style="background-color: gray; color: white;"><b>How to Claim Free Goods?</b></p>
      <p><label>Input Mobile #:</label><input type="number" class="form-control input-sm" name="mobile_number" placeholder="Mobile Number" value=""></p>
      <button type="submit" class="btn btn-primary btn-xs pull-right">Send</button>
      </form>
      </div>
      </div>
	  @endif
	@endif
    </div>
    <div class="col-sm-10 text-left custom" style="margin-top: 20px;"> 
    	@yield('content')
    </div>
</div>

<!-- <footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer> -->




<!-- Modal -->
<div id="dispatchedreportmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Generate Report</h4>
      </div>
      <div class="modal-body">
			     <form method="get" action="{{route('otsuka.redemption.dispatch-report')}}" class="form-horizontal p-10">
                {{ csrf_field() }}          
                <div class="row">
                    <div class="col-md-5">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">From: 
                                </label>
                                <div class="col-md-10">
                                    <input type="datetime" class="form-control datetimepicker"  placeholder="YYYY-MM-DD HH:MM:SS" id="filterfrom" name="dispatcheddatefrom">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-5">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">To:
                                </label>
                                <div class="col-md-10">
                                    <input type="datetime" class="form-control datetimepicker"  placeholder="YYYY-MM-DD HH:MM:SS" id="filterto" name="dispatcheddateto">
                                </div>
                            </div>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="form-group p-10 form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">Submit
                                </button>
                            </div>
                        </div>
                    </div>               
                </div>
           </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"  data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<!-- Modal -->
<div id="zpc_dispatch" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ZPC - Generate Report</h4>
      </div>
      <div class="modal-body">
			     <form method="get" action="{{route('otsuka.redemption.zpc-dispatch-report')}}" class="form-horizontal p-10">
                {{ csrf_field() }}          
                <div class="row">
                    <div class="col-md-5">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">From: 
                                </label>
                                <div class="col-md-10">
                                    <input type="datetime" class="form-control datetimepicker"  placeholder="YYYY-MM-DD HH:MM:SS"  name="dispatcheddatefrom">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-5">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">To:
                                </label>
                                <div class="col-md-10">
                                    <input type="datetime" class="form-control datetimepicker"  placeholder="YYYY-MM-DD HH:MM:SS"name="dispatcheddateto">
                                </div>
                            </div>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="form-group p-10 form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">Submit
                                </button>
                            </div>
                        </div>
                    </div>               
                </div>
           </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"  data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>









<!-- Modal -->
<div id="MTD_REDEMPTION" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Generate Report MTD REDEMPTION REPORT</h4>
      </div>
      <div class="modal-body">
			     <form method="get" action="{{route('otsuka.redemption.mtd-report')}}" class="form-horizontal p-10">
                {{ csrf_field() }}          
                <div class="row">
                    <div class="col-md-5">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">From: 
                                </label>
                                <div class="col-md-10">
                                    <input type="datetime" class="form-control datetimepicker"  placeholder="YYYY-MM-DD HH:MM:SS" id="filterfrom" name="from">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-5">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">To:
                                </label>
                                <div class="col-md-10">
                                    <input type="datetime" class="form-control datetimepicker"  placeholder="YYYY-MM-DD HH:MM:SS" id="filterto" name="to">
                                </div>
                            </div>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="form-group p-10 form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">Submit
                                </button>
                            </div>
                        </div>
                    </div>               
                </div>
           </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"  data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





</body>
      <script src="{{$assets}}inventory/js/jquery-3.2.1.js"></script>
      
	  
      <script src="{{$assets}}inventory/datatables/jquery-ui.js"></script>
      <script src="{{$assets}}inventory/datatables/bootstrap.min.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
      <script src="{{$assets}}inventory/datatables/jquery.dataTables.yadcf.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>
	 <script src="{{$assets}}JS/jquery.mask.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>

<script>
/*$(document).ready(function() 
{ 
 $('form').ajaxForm(function() 
 {
  alert("Uploaded SuccessFully");
 }); 
});*/
$(document).ready(function() {
	
$('#queue').DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
});

/*setTimeout(function() {
   location.reload();
   }, 10000);
*/
$('#example2').DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
});

$('#example3').DataTable({
  "ordering": false,
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
});
} );

$(function () {
    $('#pv').click(function () {
        window.open("{{ route('otsuka.pv') }}");
    });
});

$(function () {
    $('#pqc').click(function () {
        window.open("{{$assets}}uploads/PQC.html","","width=1280,height=1024");
    });
});

$('.global_datatable').DataTable();

  $( function() {
    $( ".datepicker" ).datepicker();
  } );
function dispatchedreport() {
	alert($("#filterfrom").val());
	//window.open("{{$assets}}otsuka/redemption/dispatch-report?dispatcheddatefrom=" + $("#filterfrom").val() + "&dispatcheddateto=" +  + $("#filterfrom").val());
}


</script>

@yield('footer-scripts')

</html>