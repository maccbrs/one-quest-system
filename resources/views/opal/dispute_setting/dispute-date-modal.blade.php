<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".conn-modal-{{$id}}">
  <i class="material-icons " aria-hidden="true">mode_edit </i>
</button>
<div class="modal fade conn-modal-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('opal.settings.dispute-date-edit',$dispute_details->id)}}" novalidate>
        {{ csrf_field() }}    

          <input type="hidden" value = "{{$dispute_details->id}}"  name = "id" > 

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Edit Dispute Date</h4>
          </div>
          <div class="modal-body">

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label for="recipient-name" class="control-label">Dispute Label</label>
                </div>

                <div class="col-xs-10">
                  <input type="text" class="form-control disputelabelinput2 " value = "{{$dispute_details->dispute_label}}"  name = "dispute_label" disabled >
                  <small>Dispute label will be the same to Date Range.</small>
                </div>
              </div> 
             </div> 

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label for="recipient-name" class="control-label">Date Range</label>
                </div>
                <div class="col-xs-10">
                  <div class="input-prepend input-group m-b-1"><span class="add-on input-group-addon"><i class="material-icons">date_range </i></span>
                    <input type="text" name="daterange" class="form-control drp daterangeinput2" value="" placeholder="Date range picker">
                  </div>
                </div>
              </div> 
            </div> 

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label for="recipient-name" class="control-label">Status</label>
                </div>

                <div class="col-xs-10">
                  <select name = "status" value = "{{($dispute_details->status == 1) ? 'Active' : 'Not Active' }}" class="form-control" >
                    <option value = '1' >Active</option>
                    <option  value = '0' >Inactive</option>
                  </select>
                </div>
              </div> 
             </div> 

            </div>  
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
  </div>
</div> 