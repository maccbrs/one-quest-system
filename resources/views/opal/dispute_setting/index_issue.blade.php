<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">
         <div class="card-block">
            <div>{!!view('opal.dispute_setting.dispute-issue-create-modal')!!} <br> <br>
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>Issue Name</th> 
                        <th>Status</th>
                        <th>Created at</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                    @foreach($date_issues as $d)
                      <tr>
                        <td>{{$d->issue_name}}</td>
                        <td>{{($d->status == 1) ? 'Active' : 'Inactive'}}</td>
                        <td>{{$d->created_at}}</td>
                        <td> {!!view('opal.dispute_setting.dispute-issue-edit-modal',['id'=>$d->id,'dispute_details'=>$d])!!}</td>
                      </tr>
                    @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')
 <link rel="stylesheet" href="{{$asset}}milestone/vendor/bootstrap-daterangepicker/daterangepicker.css">
 
@endsection

@section('footer-scripts')

    <script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

    <script type="text/javascript">
      $('.drp').daterangepicker({
          timePicker: true,
          locale: {
              format: 'YYYY/MM/DD'
          }
      });

      $( ".daterangeinput" ).change(function() {

          daterangevalue = $( ".daterangeinput" ).val();
          $( ".disputelabelinput" ).val(daterangevalue);
          
      });

    </script>
@endsection


