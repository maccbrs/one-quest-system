<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>{!!view('opal.dispute_setting.dispute-create-modal')!!} <br> <br>
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>Dispute Label</th> 
                        <th>From</th>
                        <th>To</th>
                        <th>Status</th>
                        <th>Created at</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                    @foreach($datelists as $d)
                      <tr>
                        <td>{{$d->dispute_label}}</td>
                        <td>{{$d->from}}</td>
                        <td>{{$d->to}}</td>
                        <td>{{($d->status == 1) ? 'Active' : 'Inactive'}}</td>
                        <td>{{$d->created_at}}</td>
                        <td> {!!view('opal.dispute_setting.dispute-date-modal',['id'=>$d->id,'dispute_details'=>$d])!!}</td>
                      </tr>
                    @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')
 <link rel="stylesheet" href="{{$asset}}milestone/vendor/bootstrap-daterangepicker/daterangepicker.css">
 
@endsection

@section('footer-scripts')

    <script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<style type="text/css">
.daterangepicker_input .calendar-time{
  background-color: #333;
}
</style>
    <script type="text/javascript">

      $('.drp').daterangepicker({
          timePicker: true,
          locale: {
              format: 'YYYY/MM/DD'
          }
      }); 

      $( ".daterangeinput" ).change(function() {
          daterangevalue = $( ".daterangeinput" ).val();

          $( ".disputelabelinput" ).val(daterangevalue);
      });

      $( ".daterangeinput2" ).change(function() {
         
          var daterangevalue2 = $( this ).val();

          $( ".disputelabelinput2" ).val(daterangevalue2);
      });

    </script>
@endsection


