<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target=".conn-modal-create">
  <i class="material-icons" aria-hidden="true">add </i>
</button>
<div class="modal fade conn-modal-create" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">

        <form method="post" action="{{route('opal.settings.dispute-issue-create')}}" novalidate>

        {{ csrf_field() }}      

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Create Dispute Issue</h4>
          </div>

          <div class="modal-body">

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label for="recipient-name" class="control-label">Name of Issue</label>
                </div>

                <div class="col-xs-10">
                  <input type="text" class="form-control disputelabelinput"  name = "issue_name" >
                </div>
              </div> 
             </div> 

          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
  </div>
</div> 