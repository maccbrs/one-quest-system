<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".conn-modal-{{$id}}">
  <i class="material-icons " aria-hidden="true">mode_edit </i>
</button>
<div class="modal fade conn-modal-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('opal.settings.dispute-issue-edit',$dispute_details->id)}}" novalidate>
        {{ csrf_field() }} 

          <input type="hidden" value = "{{$dispute_details->id}}"  name = "id" >                       
          
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Edit Dispute Date</h4>
          </div>
          <div class="modal-body">

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-6">
                  <label for="recipient-name" class="control-label">Issue Name</label>
                </div>

                <div class="col-xs-6">
                  <input type="text" class="form-control" value = "{{$dispute_details->issue_name}}"  name = "issue_name" >
                </div>
              </div> 
             </div> 

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-6">
                  <label for="recipient-name" class="control-label">Status</label>
                </div>

                <div class="col-xs-6">
                  <select name = "status" value = "{{($dispute_details->status == 1) ? 'Active' : 'Not Active' }}" class="form-control" >
                    <option value = '1' >Active</option>
                    <option  value = '0' >Inactive</option>
                  </select>
                </div>
              </div> 
             </div> 

            </div>  

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            
        </form>
      </div>
  </div>
</div> 