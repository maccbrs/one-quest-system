<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>Add Campaign</h3>
              {!!view('opal.campaigns.add-modal')!!}
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>Name</th> 
                        <th>#</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if($items->count())
                        @foreach($items as $item)
                        <tr>
                           <td>{{$item->title}}</td>
                           <td><a href="{{route('opal.campaigns.get',$item->id)}}" class="btn btn-primary">view</a></td>
                        </tr>
                        @endforeach
                     @endif
                  </tbody>
               </table>
               @include('pagination.default', ['paginator' => $items])
            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')


@endsection