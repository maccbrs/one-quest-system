<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target=".add-tl">
 Add Team Leader
</button>
<div class="modal fade add-tl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('opal.campaigns.addtl')}}" novalidate>
        {{ csrf_field() }}
        <input type="hidden" name="camp_id" value="{{$id}}">                        
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" ></h4>
          </div>
          <div class="modal-body">
            <fieldset class="form-group">
                <label for="exampleSelect1">Select Team Lead</label>
                <select class="form-control" name='tl_id'>
                    @foreach($teamleads as $t)
                      <option value="{{$t->id}}">{{$t->first_name}} {{$t->middle_name}} {{$t->last_name}}</option>
                    @endforeach
                </select>
            </fieldset>              
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Add</button>
          </div>
        </form>
      </div>
  </div>
</div> 