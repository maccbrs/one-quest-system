<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target=".add-modal">
  <i class="material-icons" aria-hidden="true">add </i>
</button>
<div class="modal fade add-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="" novalidate>
        {{ csrf_field() }}                        
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" ></h4>
          </div>
          <div class="modal-body">
            <fieldset class="form-group @if($errors->has('title')) has-danger @endif ">
               <label for="username">title</label>
               <input type="title" class="form-control" name="title" value="{{ old('title') }}"> 
                @if ($errors->has('title'))
                    <label class="form-control-label">{{ $errors->first('title') }}</label>
                @endif                      
            </fieldset>              
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Add</button>
          </div>
        </form>
      </div>
  </div>
</div> 