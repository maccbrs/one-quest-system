<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>{{$item->title}}

                {!!view('opal.campaigns.addagents-modal',compact('agents','id'))!!}
                {!!view('opal.campaigns.addtl-modal',compact('teamleads','id'))!!}
                {!!view('opal.campaigns.addsup-modal',compact('supervisors','id'))!!}
              </h3>
              <p><a class="btn btn-info" href="{{ URL::previous() }}">back</a></p>
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>First Name</th> 
                        <th>Middle Name</th>
                        <th>LastName</th>
                        <th>Role</th>
                     </tr>
                  </thead>
                  <tbody>
                    @foreach($dataSups as $data)
                      <tr>
                        <td>{{$data->supObj->first_name}}</td>
                        <td>{{$data->supObj->middle_name}}</td>
                        <td>{{$data->supObj->last_name}}</td>
                        <td>Supervisor</td>
                      </tr>
                    @endforeach
                    @foreach($dataTl as $tl)
                      <tr>
                        <td>{{$tl->tlObj->first_name}}</td>
                        <td>{{$tl->tlObj->middle_name}}</td>
                        <td>{{$tl->tlObj->last_name}}</td>
                        <td>Team Leader</td>
                      </tr>
                    @endforeach   
                    @foreach($dataAgents as $a)
                      <tr>
                        <td>{{$a->agentObj->first_name}}</td>
                        <td>{{$a->agentObj->middle_name}}</td>
                        <td>{{$a->agentObj->last_name}}</td>
                        <td>Agent</td>
                      </tr>
                    @endforeach                                       
                  </tbody>
               </table>
            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')


@endsection