<button class="btn btn-success btn-sm btn-sm pull-right" data-toggle="modal" data-target=".conn-modal-{{$id}}">
  <i class="material-icons" aria-hidden="true">add </i>
</button>
<div class="modal fade conn-modal-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('opal.connector.connect',$agent->id)}}" novalidate>
        {{ csrf_field() }}                        
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">{{$agent->first_name}} {{$agent->middle_name}} {{$agent->last_name}}</h4>
          </div>
          <div class="modal-body">
            <fieldset class="form-group">
                <label for="exampleSelect1">#ID</label>
                <select class="form-control" name='user_id'>
                  	@foreach($users as $u)
                  		<option value="{{$u->id}}">{{$u->name}}</option>
                  	@endforeach
                </select>
            </fieldset>                  
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
  </div>
</div> 