<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>First Name</th> 
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>#</th>
                     </tr>
                  </thead>
                  <tbody>
                    @foreach($agents as $a)
                      <tr>
                        <td>{{$a->first_name}}</td>
                        <td>{{$a->middle_name}}</td>
                        <td>{{$a->last_name}}</td>
                        <td>
                          {!!view('opal.connector.refs-modal',['id'=>$a->id,'agent' => $a,'users' => $users])!!}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>


@endsection 

@section('footer-scripts')

@endsection