<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".detail-modal-{{$dispute->id}}" title="View Details"><i class="material-icons" aria-hidden="true">folder_open </i></button>

    <div class="modal fade detail-modal-{{$dispute->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                            <div class="card">
                                <div class="card-header no-bg b-a-0">Dispute Detail</div>
                                <div class="card-block">

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Date Created</label>
                                            <p>{{$dispute->created_at}}</p>
                                        </fieldset>

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Dispute Period</label>
                                            <p>{{$dates[$dispute->dispute_period]}}</p>
                                        </fieldset>                                     

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Dispute Date</label>
                                            <p>{{$dispute->dispute_date}}</p>
                                        </fieldset>   

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Dispute Time</label>
                                            <p>{{$dispute->dispute_time}}</p>
                                        </fieldset>  

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Issue</label>
                                            <p>{{$issues[$dispute->issue]}}</p>
                                        </fieldset>

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Notes</label>
                                            <p>{{$dispute->notes}}</p>
                                        </fieldset>


                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">HR approval</label>
                                            <p>{{$help->fetchStatus($dispute->hr_response)}}</p>
                                        </fieldset>  

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">HR Notes</label>
                                            <p>{{$help->fetchComments($dispute->hr_comments)}}</p>
                                        </fieldset>                                           

                                </div>
                            </div>

                </div>
            </div>
        </div>
    </div>