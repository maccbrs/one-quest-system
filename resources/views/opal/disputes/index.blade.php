\<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

   <script type="text/javascript" src="{{$asset}}bootstrap-daterangepicker/jquery.min.js"></script>
   <script type="text/javascript" src="{{$asset}}bootstrap-daterangepicker/moment.min.js"></script>

   <script type="text/javascript" src="{{$asset}}bootstrap-daterangepicker/daterangepicker.js"></script>
   <link rel="stylesheet" type="text/css" href="{{$asset}}bootstrap-daterangepicker/daterangepicker.css" />


          <div class="card">

              <div class="card-header no-bg b-a-0">
               Periods {!!view('opal.disputes.export-modal')!!}
              </div>
              <div class="card-block" >

                    <table class = 'audittable'>
                        <thead></thead>
                        <tbody>
                            <?php $count = 1; ?>
                            <tr>
                            @foreach($dates as $d)
                            
                              <th><a href="{{route('opal.disputes.period',$d->id)}}">{{$d->dispute_label}}</a></th>

                              @if($count >= 4)
                                </tr><tr>
                                <?php $count = 1; ?>
                              @else
                                <?php $count++; ?>
                              @endif
                          
                            @endforeach
                            </tr>

                        </tbody>
                    </table>

              </div>

          </div>

          <script>

            $('input[name="daterange"]').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                }
            },
            function(start, end, label) {
                // alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });

          </script>

@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')

    <script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>



@endsection