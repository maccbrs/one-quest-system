<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'Approver List (Non-Agents)')

@section('content')



         <div class="card">

         <div class="card-block">
            <div>
              <div class="card-header no-bg b-a-0"><h4>Non-Agents Approver List </h4></div>

             </div> 

             <div class="table-responsive">

               <table class="display nowrap" id="table">

                  <thead>
                     <tr>
                        <th>Employee Code</th>
                        <th>Username</th>
                        <th>Employer</th> 
                        <th>Supervisor</th>
                     </tr>
                  </thead> 
                  <tbody>
                        @foreach($approvers as $a)
                        <tr>
                            <td>{{$a->emp->emp_code}}</td>
                            <td>{{$a->emp->email}}</td>
                           <td>{{$a->emp->name}}</td>
                           <td>{{$a->sup->name}}</td>
                        </tr>
                        @endforeach
                  </tbody> 
               </table>

            </div>
         </div>

@endsection 

@section('footer-scripts')
      <script src="{{$asset}}datatables/jquery-1.8.2.min.js"></script>
      <script src="{{$asset}}datatables/jquery-ui.js"></script>

      <script src="{{$asset}}datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
      <script src="{{$asset}}datatables/jquery.dataTables.yadcf.js"></script>

      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>

<script>      
$(document).ready(function() 
    {
        var currentDate = new Date()
        var day = ('0'+(currentDate.getDate())).slice(-2)
        var month = ('0'+(currentDate.getMonth()+1)).slice(-2)
        var year = currentDate.getFullYear()
        var d = year + "-" + month + "-" + day;

        var oTable;
        oTable = $('#table')

        .DataTable({
        lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
        pageLength: 25, 
        scrollX: true,
        autoWidth: false,
        responsive: true,
        scrollY: '600px',
        scrollCollapse: true,
        dom: 'Blfrtip',
        stateSave: true,
        buttons: [
            {
                extend: 'copyHtml5',
                title: "Non-Agents' Approver List" + " " + d,
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            {
                extend: 'excelHtml5',
                title: "Non-Agents' Approver List" + " " + d,
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            'colvis'
            ]
            });
      });
</script>
@endsection