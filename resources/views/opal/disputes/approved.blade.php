<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'Approved Disputes')

@section('content')

    <div class="card">
         <div class="card-block">
              <div class="card-header no-bg b-a-0"><h4>Approved Disputes</h4></div>


            <div id="dfilter" class="hide">
            <div class="card-deck" >
                             <div class="card">
                                <div class="card-header">Date Filed</div>
                                    <div class="card-block">
                                        <div id="external_filter_container_1" class="filter"></div>
                                            </div>
                                                </div>

                             <!-- <div class="card">
                                <div class="card-header">Date Updated</div>
                                    <div class="card-block">
                                        <div id="external_filter_container_2" class="filter"></div>
                                            </div>
                                                </div> -->

                             <div class="card">
                                <div class="card-header">Dispute Date</div>
                                    <div class="card-block">
                                        <div id="external_filter_container_3" class="filter"></div>
                                            </div>
                                                </div>
                        </div>
                        </div>

            <div class="table-responsive">

            <table id="table" class="display nowrap">
              <thead>
                   <tr>
                      <th>#</th>  
                      <th>Entry #</th>
                      <th>Dispute Status</th>
                      <th>Date and Time Entry</th>
                      <th># of Days Before Validating the Dispute</th>
                      <th>Emp Code</th>
                      <th>Employee Name</th>
                      <th>Department</th>
                      <th>Supervisor</th>
                      <th>Period</th> 
                      <th>Date</th>
                      <th>Issue</th>
                      <th>Employee Remarks</th>
                      <th>Superior Status</th>
                      <th>Superior Remarks</th>
                      <th>Date & Time Superior Validation</th>
                      <th>RA Status</th>
                      <th>RA Remarks</th>
                      <th>Date & Time RA Validation</th>
                      <th>HRD Status</th>
                      <th>HRD Remarks</th>
                      <th>Date & Time HR Validation</th>
                   </tr>
                </thead>
                <tbody>
                   @foreach($disputes as $key => $dispute)
                      <tr class="dispute{{$dispute->id}}">
                        <td>{{$key + 1}}</td>
                        <td>{{$dispute->id}}</td>
                        <td>{{!empty($dispute->hr_response) ? $hr_status[$dispute->hr_response] : "n/a"}}</td>
                        <td>{{$dispute->created_at}}</td>
                        <td>{{$dispute->diff_day}}</td>
                        <td>{{!empty($dispute->userObj->emp_code) ? $dispute->userObj->emp_code : ""}}</td>
                        <td>{{!empty($dispute->userObj->name) ? $dispute->userObj->name : ""}}</td>
                        <td>{{$dispute->dept}}</td>
                        <td>{{$dispute['sup_approver']}}</td>
                        <td>{{$dispute->from . '-' . $dispute->to}}</td>
                        <td>{{$dispute->dispute_date}}</td>
                        <td>{{$dispute->issue_detail}}</td>
                        <td>{{$dispute->notes}}</td>
                        <td>{{!empty($dispute->sup_response) ? $hr_status[$dispute->sup_response] : (!empty($dispute->tl_response) ? $hr_status[$dispute->tl_response] : "n/a" ) }}</td>
                        <td>{{!empty($dispute->sup_comment) ? $dispute->sup_comment : ""}}</td>
                        <td>{{(!empty($dispute['tl_details']->date) ? $dispute['tl_details']->date :"not yet validated")}} </td>
                        <td>{{!empty($dispute->ra_response) ? $hr_status[$dispute->ra_response] : "n/a"}}</td>
                        <td>{{!empty($dispute->ra_comment) ? $dispute->ra_comment : ""}}</td>
                        <td>{{(!empty($dispute['ra_details']->date) ? $dispute['ra_details']->date :"not yet validated")}} </td>
                        <td>{{!empty($dispute->hr_response) ? $hr_status[$dispute->hr_response] : "n/a"}}</td>
                        <td>{{!empty($dispute->hr_comment) ? $dispute->hr_comment : ""}}</td>
                        <td>{{(!empty($dispute['hr_details']->date) ? $dispute['hr_details']->date :"not yet validated")}} </td>
                      </tr>
                   @endforeach

                </tbody>
              </table>
            </div>
          </div>

@endsection 

@section('footer-scripts')
      <script src="{{$asset}}datatables/jquery-1.8.2.min.js"></script>
      <script src="{{$asset}}datatables/jquery-ui.js"></script>

      <script src="{{$asset}}datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
      <script src="{{$asset}}datatables/jquery.dataTables.yadcf.js"></script>

      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>

<script>
$(document).ready(function() 
    {
        var currentDate = new Date()
        var day = ('0'+(currentDate.getDate())).slice(-2)
        var month = ('0'+(currentDate.getMonth()+1)).slice(-2)
        var year = currentDate.getFullYear()
        var d = year + "-" + month + "-" + day;

        var oTable;
        oTable = $('#table')

        .DataTable({
        lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
        pageLength: 25, 
        scrollX: true,
        autoWidth: false,
        responsive: true,
        scrollY: '600px',
        scrollCollapse: true,
        dom: 'Blfrtip',
        stateSave: true,
        /*order: [[3, 'desc']],*/
        buttons: [
            {
                text: 'Date Filter',
                action: function ( e, dt, node, config )
                {
                    var x = $('#dfilter');
                    x.slideToggle('slow');
                }
            },
            {
                extend: 'copyHtml5',
                title: 'Rejected Disputes' + ' ' + d + ' ',
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            {
                extend: 'excelHtml5',
                title: 'Rejected Disputes' + ' ' + d + ' ',
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            'colvis'
            ]
            });
        yadcf.init(oTable, [
            {
            column_number : 3,
            date_format: "yyyy-mm-dd",
            filter_container_id: 'external_filter_container_1',
            filter_type: 'range_date'
            }
            ,
            {
            column_number : 10,
            date_format: "yyyy-mm-dd",
            filter_container_id: 'external_filter_container_2',
            filter_type: 'range_date'
            }
            ,
            {
            column_number : 15,
            date_format: "yyyy-mm-dd",
            filter_container_id: 'external_filter_container_3',
            filter_type: 'range_date'
            }
                ]);
      });
</script>
@endsection