<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'Period')

@section('content')

   <div class="card">

         <div class="card-block">
            <div>
              <div class="card-header no-bg b-a-0"><h4>Period</h4></div>

             </div> 

              <table class="table table-bordered customized-table stripe hover" id="table">
                  <thead>
                     <tr>
                        <th>Agent</th>
                        <th>Period</th> 
                        <th>Date</th>
                        <th>Time</th>
                        <th>Issue</th>
                        <th>HR</th>
                        <th>#</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($disputes as $dispute)
                        <tr>
                           <td>{{!empty($dispute->userObj->name) ? $dispute->userObj->name : ""}}</td>
                           <td>{{$dispute->periodObj->dispute_label}}</td>
                           <td>{{$dispute->dispute_date}}</td>
                           <td>{{$dispute->dispute_time}}</td>
                           <td>{{$dispute->issueObj->issue_name}}</td>
                           <td>{{$help->fetchStatus($dispute->hr_response)}}</td>
                           <td>{!!view('opal.disputes.period-modal',compact('dispute','help','dates','issues'))!!}</td>
                        </tr>
                     @endforeach

                  </tbody>
             </table>
                              

         </div>
      </div>
   </div>
   
@endsection 

@section('footer-scripts')
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
@endsection

@section('added-function')
<script>
$(document).ready(function() 
  {
    $('#table').DataTable({
        scrollY: "450px",
        lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
        pageLength: 25,
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            'colvis'
        ]});
  });
</script>
@endsection

@section('added-script')

@endsection