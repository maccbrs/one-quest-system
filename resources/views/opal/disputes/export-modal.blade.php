<button class="btn btn-primary btn-sm pull-right"  data-toggle="modal" data-target=".export-modal" title = "Export Approved Disputes">
  <i class="material-icons " aria-hidden="true">print </i>
</button> 
<div class="modal fade export-modal" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('opal.settings.dispute-date-export')}}" novalidate>

        {{ csrf_field() }}    

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h6 class="modal-title" id="myModalLabel">Export Clearace in Excel </h6>
          </div>

          <input type="hidden" name="id"   >

          <div class="modal-body">

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label class="control-label">Dispute Date</label>
                </div>
                <div class="col-xs-10">
                  <div class="form-group">
                    <div class="input-prepend input-group m-b-1"><span class="add-on input-group-addon"><i class="material-icons">date_range </i></span>
                      <input type="text" name="daterange" class="form-control drp" value="" placeholder="Date range picker">
                    </div>
                </div>
                </div>
              </div> 
             </div>

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label class="control-label">Status</label>
                </div>
                <div class="col-xs-10">
                  <div class="form-group">
                    <select class="form-control " name="status" >
                      <option selected value = "0" >All</option>
                      <option value = "1" >Pending</option>
                      <option value = "2" >Approved</option>
                      <option value = "3" >Rejected</option>
                    </select> 
                </div>
                </div>
              </div> 
             </div>

          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" >Export</button>
              
          </div>
        </form>
      </div>
  </div>
</div> 