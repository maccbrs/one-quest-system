<html>

              <table class="table table-bordered customized-table dispute-table">
              <thead>
                   <tr>
                      <th>Entry #</th>
                      <th>Dispute Status</th>
                      <th>Date and Time Entry</th>
                      <th># of Days Before Validating the Dispute</th>
                      
                      <th>Emp Code</th>
                      <th>Employee Name</th>
                      <th>Department</th>
                      <th>Supervisor</th>
                      <th>Period</th> 
                      <th>Date</th>
                      <th>Time</th>
                      <th>Issue</th>
                      <th>Employee Remarks</th>
                      <th>Superior Status</th>
                      <th>Superior Remarks</th>
                      <th>Date & Time Superior Validation</th>
                      <th>RA Status</th>
                      <th>RA Remarks</th>
                      <th>Date & Time RA Validation</th>
                      <th>HRD Status</th>
                      <th>HRD Remarks</th>
                      <th>Date & Time HR Validation</th>
                   </tr>
                </thead>
                <tbody>
                   @foreach($disputes as $dispute)
                      <tr>
                        <td>{{$dispute->id}}</td>
                        <td>{{!empty($dispute->hr_response) ? $hr_status[$dispute->hr_response] : "n/a"}}</td>
                        <td>{{$dispute->created_at}}</td>
                        <td>{{$dispute->diff_day}}</td>
                        <td>{{!empty($dispute->userObj->emp_code) ? $dispute->userObj->emp_code : ""}}</td>
                        <td>{{!empty($dispute->userObj->name) ? $dispute->userObj->name : ""}}</td>
                        <td>{{$dispute->dept}}</td>
                        <td>{{$dispute['sup_approver']}}</td>
                        <td>{{$dispute->from . '-' . $dispute->to}}</td>
                        <td>{{$dispute->dispute_date}}</td>
                        <td>{{$dispute->dispute_time}}</td>
                        <td>{{$dispute->issue_detail}}</td>
                        <td>{{$dispute->notes}}</td>
                        <td>{{!empty($dispute->sup_response) ? $hr_status[$dispute->sup_response] : (!empty($dispute->tl_response) ? $hr_status[$dispute->tl_response] : "n/a" ) }}</td>
                        <td>{{!empty($dispute->sup_comment) ? $dispute->sup_comment : ""}}</td>
                        <td>{{(!empty($dispute['tl_details']->date) ? $dispute['tl_details']->date :"not yet validated")}} </td>
                        <td>{{!empty($dispute->ra_response) ? $hr_status[$dispute->ra_response] : "n/a"}}</td>
                        <td>{{!empty($dispute->ra_comment) ? $dispute->ra_comment : ""}}</td>
                        <td>{{(!empty($dispute['ra_details']->date) ? $dispute['ra_details']->date :"not yet validated")}} </td>
                        <td>{{!empty($dispute->hr_response) ? $hr_status[$dispute->hr_response] : "n/a"}}</td>
                        <td>{{!empty($dispute->hr_comment) ? $dispute->hr_comment : ""}}</td>
                        <td>{{(!empty($dispute['hr_details']->date) ? $dispute['hr_details']->date :"not yet validated")}} </td>
                      </tr>
                   @endforeach

                </tbody>
              </table>

</html>