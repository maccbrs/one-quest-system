<button class="btn btn-primary btn-sm pull-right"  data-toggle="modal" data-target=".close-modal-{{$details->id}}" title = "Mark Incomplete">
  <i class="material-icons " aria-hidden="true">feedback </i>
</button> 
<div class="modal fade close-modal-{{$details->id}}" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('opal.department.clearance_close',$details->id)}}" novalidate>

        {{ csrf_field() }}    

          <input type="hidden" value = "{{$details->id}}"  name = "user_id" > 
          <input type="hidden" value = "{{$details->gem->id}}"  name = "gem_id" > 
          <input type="hidden" value = "{{$details->gem->is_representative}}"  name = "is_agent" > 

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h6 class="modal-title" id="myModalLabel">Do you want to mark this Incomplete ? </h6>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" >Mark</button>
              
          </div>
        </form>
      </div>
  </div>
</div> 