<html>

<table>  <!-- Horizontal alignment -->
  <tr>
    <td><b>Clearance Status : {{$indicator}}</b></td>
  </tr>

  <tr>
    <th>Name</th>
    <th>Accounting</th>
    <th>Information Technology(NOC)</th>
    <th>Human Resources</th>
    <th>Operations</th>
    <th>Date</th>
  </tr>
  
    @foreach($users as $key => $value)

      <tr>
        <td>{{$value->gem['name']}}</td>
        <td>
          <span class="tag tag-{{($value['accounting'] == 1) ? 'success' : 'warning' }}">
            {{($value['accounting'] == 1) ? "cleared" : " pending " }}
          </span>
        </td>

        <td>
            <span class="tag tag-{{($value['it'] == 1) ? 'success' : 'warning' }}">
          {{($value['it'] == 1) ? "cleared" : " pending " }}
        </span>
        </td>
        <td>
          <span class="tag tag-{{($value['hr'] == 1) ? 'success' : 'warning' }}">
            {{($value['hr'] == 1) ? "cleared" : " pending " }}
        </td>
          </span>
        <td>
            @if(!empty($value['operations2'])) 
            @foreach($value['operations2'] as $op => $val) 
              @if($val == 1) {{$op}} - <span class="tag tag-success">cleared</span> <br> @else {{$op}} - <span class="tag tag-warning">pending</span> <br> @endif @endforeach @endif
        </td>
        <td>{{$value['updated_at']}}</td>
      </tr>

    @endforeach

</table>

</html>