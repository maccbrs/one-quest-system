<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2') 

@section('title', 'index') 

@section('content')

<script type="text/javascript" src="{{$asset}}bootstrap-daterangepicker/jquery.min.js"></script>
<script type="text/javascript" src="{{$asset}}bootstrap-daterangepicker/moment.min.js"></script>

<script type="text/javascript" src="{{$asset}}bootstrap-daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{{$asset}}bootstrap-daterangepicker/daterangepicker.css" />

<div class="card">
  <div class="card-block">
     {!!view('opal.clearance.export-modal')!!}
    <div>
        <h3>List of Users in Clearance</h3>
    </div>

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item ">
            <a class="nav-link active" data-toggle="tab" href="#clearing" role="tab" aria-expanded="false"> On Clearance </a>
        </li>
        <li class="nav-item">
            <a class="nav-link " data-toggle="tab" href="#incomplet" role="tab" aria-expanded="true"> Incomplete </a>
        </li>
        <li class="nav-item">
            <a class="nav-link " data-toggle="tab" href="#completed" role="tab" aria-expanded="true"> Completed </a>
        </li>

    </ul>

    <div class="tab-content">

      <div id="clearing" class="tab-pane fade in active">
        <div class="table-responsive">
          <table class="table m-b-0">
            <thead>
              <tr>
                  <th>Name</th>
                  <th>Admin</th>
                  <th>Information Technology(NOC)</th>
                  <th>Human Resources</th>
                  <th>Operations</th>
                  <th>On Clearance Date</th>
                  <th>Action</th>
              </tr>
            </thead>
            <tbody>
               @if($users) @foreach($users as $u)
                <tr>
                  <td>
                      {{$u->gem['name']}}
                  </td>
                  <td>
                      <span class="tag tag-{{($u['admin'] == 0) ? 'warning' : 'success' }}">
                    {{($u['admin'] != 0) ? "Cleared - " . $u->admin : " pending " }}
                  </span>
                  </td>
                  <td>
                      <span class="tag tag-{{($u['it'] == 0) ? 'warning' : 'success' }}">
                    {{($u['it'] != 0) ? "Cleared - " . $u->it : " pending " }}
                  </span>
                  </td>
                  <td>
                      <span class="tag tag-{{($u['hr'] == 0) ? 'warning' : 'success' }}">
                    {{($u['hr'] != 0) ? "Cleared - " . $u->hr : " pending " }}</td>
                  </span>
                  <td>
                      @if(!empty($u['operations2'])) 
                        @foreach($u['operations2'] as $op => $val) 
                          @if($val != 0) 
                            {{$op}} - <span class="tag tag-success">{{"Cleared - " . $val}}</span> <br> 
                          @else 
                            {{$op}} - <span class="tag tag-warning">pending</span> <br> 
                          @endif
                        @endforeach
                      @endif
                  </td>
                  <td>
                      {{$u->created_at}}
                  </td>
                  <td>{!!view('opal.clearance.close-modal',['details'=>$u ])!!} {!!view('opal.clearance.clear-modal',['details'=>$u ])!!}</td>
                </tr>
                @endforeach 
              @endif
            </tbody>
          </table>
          @include('pagination.default', ['paginator' => $users])
        </div>
      </div>

      <div id="incomplet" class="tab-pane fade">
        <div class="table-responsive">
          <table class="table m-b-0">
            <thead>
              <tr>
                  <th>Name</th>
                  <th>Accounting</th>
                  <th>Information Technology(NOC)</th>
                  <th>Human Resources</th>
                  <th>Operations</th>
              </tr>
            </thead>
              <tbody>
                @if($incomplete_users) 
                  @foreach($incomplete_users as $u)
                    <tr>
                        <td>
                            {{$u->gem['name']}}
                        </td>
                        <td>
                            <span class="tag tag-{{($u['admin'] == 1) ? 'success' : 'warning' }}">
                          {{($u['admin'] == 1) ? "cleared" : " pending " }}
                        </span>
                        </td>
                        <td>
                            <span class="tag tag-{{($u['it'] == 1) ? 'success' : 'warning' }}">
                          {{($u['it'] == 1) ? "cleared" : " pending " }}
                        </span>
                        </td>
                        <td>
                            <span class="tag tag-{{($u['hr'] == 1) ? 'success' : 'warning' }}">
                          {{($u['hr'] == 1) ? "cleared" : " pending " }}</td>
                        </span>
                            <td>
                                @if(!empty($u['operations2'])) 
                                @foreach($u['operations2'] as $op => $val) 
                                  @if($val == 1) {{$op}} - <span class="tag tag-success">cleared</span> <br> @else {{$op}} - <span class="tag tag-warning">pending</span> <br> @endif @endforeach @endif
                            </td>

                    </tr>
                  @endforeach 
                @endif
            </tbody>
          </table>

          @include('pagination.default', ['paginator' => $incomplete_users])
        </div>
      </div>

      <div id="completed" class="tab-pane fade">
        <div class="table-responsive">
          <table class="table m-b-0">
            <thead>
              <tr>
                  <th>Name</th>
                  <th>Accounting</th>
                  <th>Information Technology(NOC)</th>
                  <th>Human Resources</th>
                  <th>Operations</th>
              </tr>
            </thead>
              <tbody>
                @if($cleared_users) @foreach($cleared_users as $u)
                <tr>
                    <td>
                        {{$u->gem['name']}}
                    </td>
                    <td>
                        <span class="tag tag-{{($u['admin'] == 1) ? 'success' : 'warning' }}">
                      {{($u['admin'] == 1) ? "cleared" : " pending " }}
                    </span>
                    </td>
                    <td>
                        <span class="tag tag-{{($u['it'] == 1) ? 'success' : 'warning' }}">
                      {{($u['it'] == 1) ? "cleared" : " pending " }}
                    </span>
                    </td>
                    <td>
                        <span class="tag tag-{{($u['hr'] == 1) ? 'success' : 'warning' }}">
                      {{($u['hr'] == 1) ? "cleared" : " pending " }}</td>
                    </span>
                        <td>
                            @if(!empty($u['operations2'])) @foreach($u['operations2'] as $op => $val) @if($val == 1) {{$op}} - <span class="tag tag-success">cleared</span> <br> @else {{$op}} - <span class="tag tag-warning">pending</span> <br> @endif @endforeach @endif
                        </td>

                </tr>
                @endforeach @endif
            </tbody>
          </table>

          @include('pagination.default', ['paginator' => $cleared_users])
        </div>
      </div>

    </div>
  </div>
</div>


  <script>
    $('input[name="daterange"]').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        },
        function(start, end, label) {
            // alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
  </script>

@endsection @section('header-scripts') @endsection @section('footer-scripts') @endsection