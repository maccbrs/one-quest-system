<button class="btn btn-primary btn-sm pull-right"  data-toggle="modal" data-target=".conn-modal-{{$details->id}}" title = "Mark Clear to Employee">
  <i class="material-icons " aria-hidden="true">mode_edit </i>
</button> 
<div class="modal fade conn-modal-{{$details->id}}" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('opal.department.clearance_update',$details->id)}}" novalidate>

        {{ csrf_field() }}    

          <input type="hidden" value = "{{$details->id}}"  name = "user_id" > 
          <input type="hidden" value = "{{$details->gem->id}}"  name = "gem_id" > 
          <input type="hidden" value = "{{$details->gem->is_representative}}"  name = "is_agent" > 

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h6 class="modal-title" id="myModalLabel">Mark Users Cleared Items </h6>
          </div>

          <div class="modal-body">
            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label class="control-label">Admin</label>
                </div>
                <div class="col-xs-10">
                  <div class="form-group">
                  <select class="form-control" id="sel1" name = "admin" required>
                    <option value = '{{date("Y/m/d h:i:s")}}' '{{($details->admin = 1) ? "selected" : ''  }}'>Clear</option>
                    <option value = '0' '{{($details->admin = 0) ? "selected" : ''  }}'>Pending</option>
                  </select>
                </div>
                </div>
              </div> 
             </div>

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label class="control-label">NOC </label>
                </div>
                <div class="col-xs-10">
                  <div class="form-group">
                  <select class="form-control" name = "it" required>
                    <option value = '{{date("Y/m/d h:i:s")}}' '{{($details->it = 1) ? "selected" : ''  }}' >Clear</option>
                    <option value = '0' '{{($details->it = 0) ? "selected" : ''  }}' selected>Pending</option>
                  </select>
                </div>
                </div>
              </div> 
             </div>

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label class="control-label">Human Resource {{$details->gem->user_type}}</label>
                </div>
                <div class="col-xs-10">
                  <div class="form-group">
                  <select class="form-control" name = "hr" required>
                    <option value = '{{date("Y/m/d h:i:s")}}' '{{($details->hr = 1) ? "selected" : ''  }}'>Clear</option>
                    <option value = '0' '{{($details->hr = 0) ? "selected" : ''  }}' selected>Pending</option>
                  </select>
                </div>
                </div>
              </div> 
             </div>

            @if($details->gem->user_type == 'user')

              <div class = "row">
                <div class="form-group">
                  <div class="col-xs-2">
                    <label class="control-label">Team Leader</label>
                  </div>
                  <div class="col-xs-10">
                    <div class="form-group">
                    <select class="form-control" name = "operations[teamleader]" required>
                      <option disabled selected>Select Status</option>
                      <option value = '{{date("Y/m/d h:i:s")}}' '{{($details->hr = 1) ? "selected" : ''  }}'>Clear</option>
                      <option value = '0' '{{($details->hr = 0) ? "selected" : ''  }}' selected>Pending</option>
                    </select>
                  </div>
                  </div>
                </div> 
               </div>

              <div class = "row">
                <div class="form-group">
                  <div class="col-xs-2">
                    <label class="control-label">Program Supervisor</label>
                  </div>
                  <div class="col-xs-10">
                    <div class="form-group">
                    <select class="form-control" name = "operations[program_supervisor]" required>
                      <option disabled selected>Select Status</option>
                      <option value = '{{date("Y/m/d h:i:s")}}' '{{($details->hr = 1) ? "selected" : ''  }}'>Clear</option>
                      <option value = '0' '{{($details->hr = 0) ? "selected" : ''  }}' selected>Pending</option>
                    </select>
                  </div>
                  </div>
                </div> 
               </div>

            @endif

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label class="control-label">Operations Manager</label>
                </div>
                <div class="col-xs-10">
                  <div class="form-group">
                  <select class="form-control" name = "operations[manager]" required>
                    <option value = '{{date("Y/m/d h:i:s")}}' '{{($details->hr = 1) ? "selected" : ''  }}'>Clear</option>
                    <option value = '0' '{{($details->hr = 0) ? "selected" : ''  }}' selected>Pending</option>
                  </select>
                </div>
                </div>
              </div> 
             </div>

<!--             <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label class="control-label">Operation</label>
                </div>
                <div class="col-xs-10">
                  <div class="form-group">
                    <label class="checkbox-inline"><input type="checkbox" value="1" name="operations[teamleader]">Team Leader</label>
                    <label class="checkbox-inline"><input type="checkbox" value="1" name="operations[program_supervisor]" >Program Supervisor</label>
                    <label class="checkbox-inline"><input type="checkbox" value="1" name="operations[manager]" >Operations Manager</label>
                </div>
                </div>
              </div> 
             </div> -->
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" >Save</button>
              
          </div>
        </form>
      </div>
  </div>
</div> 