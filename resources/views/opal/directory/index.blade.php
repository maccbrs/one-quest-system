<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>{{$title}}</h3>
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>First Name</th> 
                        <th>Middle Initial</th>
                        <th>Last Name</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                    @if($data)
                      @foreach($data as $d)
                        <tr>
                          <td>{{$d['fname']}}
                            @if($d['status'] == 2)
                              <span class="tag tag-pill tag-warning">For Clearance
                              </span>
                            @endif</td>
                          <td>{{$d['mname']}}</td>
                          <td>{{$d['lname']}}</td>
                          <td>{!!view('opal.directory.action-modal',['details'=>$d,'type'=>$type])!!} Clearance</td>
                        </tr>
                      @endforeach
                    @endif
                  </tbody>
               </table>
                @include('pagination.default', ['paginator' => $items])
            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')


@endsection