<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>{{$title}}</h3>
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>Title</th> 
                        <th>#</th>
                     </tr>
                  </thead>
                  <tbody>
                    @foreach($items as $i)
                      <tr>
                        <td>{{$i->title}}</td>
                        <td><a href="{{route('opal.campaigns.get',$i->id)}}" class="btn btn-primary">view</a></td>
                      </tr>
                    @endforeach
                  </tbody>
               </table>
               @include('pagination.default', ['paginator' => $items])
            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')


@endsection