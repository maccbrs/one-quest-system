<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

  <div class="card">
     <div class="card-block">
        <div>
          <div>

            <section class="pull-right">

              {!!view('opal.announcements.announcement-delete-modal',compact('item'))!!}
              {!!view('opal.announcements.announcement-update-modal',compact('item'))!!}
              
            </section>

            <h3>{{$item->title}} </h3>

          </div>
          <p>{{$item->excerpt}}</p>
        </div> 
        <div class="table-responsive">
          <p>{!! ($item->content) !!}</p>
        </div>
     </div>
  </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')


    <script src="{{$asset}}vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="{{$asset}}vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('#texteditor').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>


@endsection