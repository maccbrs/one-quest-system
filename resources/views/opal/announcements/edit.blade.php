<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

  <div class="card">
     <div class="card-block">
        <div>
          <h3>{{$item->title}} 
            <section class="pull-right">
              <a class="btn btn-info" href="{{ URL::previous() }}">back</a> 
              <a class="btn btn-primary" href="{{route('opal.announcements.edit',$item->id)}}">edit</a> 
              <a class="btn btn-warning" href="{{ URL::previous() }}">delete</a> 
            </section>
          </h3>
          <p>{{$item->excerpt}}</p>
        </div> 
        <div class="table-responsive">
          <p>{!! ($item->content) !!}</p>
        </div>
     </div>
  </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')

    <script src="{{$asset}}vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="{{$asset}}vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>


@endsection