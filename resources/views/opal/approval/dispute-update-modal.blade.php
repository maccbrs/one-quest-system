<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".detail-modal-{{$dispute->id}}"><i class="material-icons" aria-hidden="true">folder_open </i></button>

    <div class="modal fade detail-modal-{{$dispute->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                            <div class="card">
                                <div class="card-header no-bg b-a-0">Update</div>
                                <div class="card-block">
                                    <form method="post" action="{{route('gem.approval.dispute-update')}}" novalidate>
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{$dispute->id}}">
                                        <p class="mb-details-modal auto-y">

                                        <span>Issue:</span> {{$dispute->issueObj->issue_name}}<br>
                                        <span>Employee Code:</span> {{$dispute->userObj->emp_code}}<br>
                                        <span>Agent:</span> {{$dispute->userObj->name}}<br>
                                        <span>Dispute Period:</span> {{$dispute->periodObj->dispute_label}}<br>
                                        <span>Dispute Date:</span> {{$dispute->dispute_date}}<br>
                                        <span>Agent's Note:</span> {{$dispute->notes}}<br>
                                        <span>Supervisor's Response:</span> {{$help->fetchStatus($dispute->sup_response)}} <?=($dispute->sup_response > 1? ' by: ('.$help->fetchUser($dispute->sup_comments,$users).' ) date: '.$help->fetchDate($dispute->sup_comments):'') ?><br>
                                        <span>Supervisor's Comment:</span> {{!empty($help->fetchComments($dispute->sup_comments)) ? ($help->fetchComments($dispute->sup_comments)) : 'None'}}<br>
                                        <span>HR Comment:</span> {{!empty($help->fetchComments($dispute->hr_comments)) ? ($help->fetchComments($dispute->hr_comments)) : 'None'}}<br>
                                        <span>Dispute Period:</span> {{$dispute->periodObj->dispute_label}}<br>
                                                                         
                                        </p>
                          
<!-- 
                                        <fieldset class="form-group">
                                            <label for="exampleSelect1"></label>
                                            <p class="mb-details-modal"></p>
                                        </fieldset>    -->

<!--                                         <fieldset class="form-group">
                                            <label for="exampleSelect1">Dispute Time</label>
                                            <p>{{$dispute->dispute_time}}</p>
                                        </fieldset>  -->
<!-- 
                                        <fieldset class="form-group">
                                            <label for="exampleSelect1"></label>
                                            <p class = "auto-y"></p>
                                        </fieldset>    

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Sup Response</label>
                                            <p class="mb-details-modal"></p>
                                        </fieldset>  

                                       <fieldset class="form-group">
                                            <label for="exampleSelect1">Sup Comment</label>
                                            <p class="mb-details-modal"></p>
                                        </fieldset>                                                                                    
                                        <fieldset class="form-group">
                                            <label for="exampleSelect1" >HR Comment</label>
                                            <p class="mb-details-modal"></p>
                                        </fieldset> -->

                                        <fieldset class="form-group">
                                            <label for="exampleTextarea">Note</label>
                                            <textarea class="form-control" rows="3" name="notes"></textarea>
                                        </fieldset>

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Status</label>
                                            <select class="form-control" name="status">
                                                <option value="1">pending</option>
                                                <option value="2">approve</option>
                                                <option value="3">reject</option>
                                            </select>
                                        </fieldset>
                                        </p>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>  
                                </div>
                            </div>

                </div>
            </div>
        </div>
    </div>