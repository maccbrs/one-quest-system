<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>{{$title}}</h3>
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>Name</th> 
                        <th>Email</th>
                        <th>Date of Registry</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                    @if($users)
                      @foreach($users as $d)
                        <tr>
                          <td>
                            {{$d['name']}}

                            @if($d['status'] == 2)
                              <span class="tag tag-pill tag-warning">For Clearance</span>
                            @endif

                          </td>
                          <td>{{$d['email']}}</td>
                          <td>{{$d['created_at']}}</td>
                          <td>{!!view('opal.department.action-modal',['details'=>$d, 'type'=>$type ])!!} Clearance</td>
                        </tr>
                      @endforeach
                    @endif
                  </tbody>
               </table>
                @include('pagination.default', ['paginator' => $users])
            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')


@endsection