<button class="btn btn-primary btn-sm pull-right"  data-toggle="modal" data-target=".conn-modal-{{$details->id}}">
  <i class="material-icons " aria-hidden="true">contact_mail </i>
</button> 
<div class="modal fade conn-modal-{{$details->id}}" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('opal.department.clearance',$details->id)}}" novalidate>
        {{ csrf_field() }}    

          <input type="hidden" value = "{{$details->id}}"  name = "user_id" > 
          <input type="hidden" value = "{{$details->status}}"  name = "status" > 
          <input type="hidden" value = "{{$type}}"  name = "type" > 
       

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h6 class="modal-title" id="myModalLabel">Are you sure you want to Issue a Clearance to {{$details->name}} </h6>
          </div>


          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No </button>
            <button type="submit" class="btn btn-primary" >Yes</button>
              
          </div>
        </form>
      </div>
  </div>
</div> 