<div class="card shadow" style="margin-left: 50px; margin-right: 50px; background-color: #FFFBE8;">
  <div class="card-header">
    <button type="button" id="close" class="close" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h2>Latest Announcement</h2></div>
  <div class="card-block" style="padding-left: 80px; padding-right: 80px;">
    <h4 class="card-title"><u>{{$latest_announcement->title}}</u></h4>
    <p class="card-text"><small class="lead text-uppercase">From: {{$latest_announcement->source}} DEPARTMENT</small></p>
    <p class="card-text"><h6 class="lead text-justify" style="line-height: 200%;">{!! ($latest_announcement->content) !!}</h6></p>
  </div>
</div>