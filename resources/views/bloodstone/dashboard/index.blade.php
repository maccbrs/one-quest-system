@extends('bloodstone.master')

@section('title')
Dashboard
@endsection

@section('content')
<style type="text/css">
th { font-size: 12px; }
td { font-size: 11px; }
</style>



<?php 
$userlists = "";
foreach($userlist as $keys => $value_field)
		{	if($userlists == "")		
			{ $userlists = $value_field['id'] . ":" . $value_field['name']  ;
			}
			else{
				$userlists = $userlists . "," . $value_field['id'] . ":" . $value_field['name']  ;
			}
		}

//var_dump($userlist);
$array_list = array(
			  "header"=> ucfirst("Assigned To ") , 			 
			  "form-action"=> route('bloodstone.forms.update'), 
			  "form-list"=>  array("assigned_to" => "select|user|" .$userlists,
								   "unique_id" => "hidden|ID|unique_id",
								   "remarks" => "textarea|Remarks|remarks"
								  
								   
								  ) 
			  );
$array_list2 = array(
			  "header"=> ucfirst("Return ") , 			 
			  "form-action"=> route('bloodstone.forms.return'), 
			  "form-list"=>  array("hidden" => "hidden|ID|unique_id" .$userlists,
								   "unique_id" => "hidden|ID|unique_id",
								   "remarks" => "textarea|Remarks|remarks"
								)
					);			
			  			  
			  ?>
			  
{!!view('bloodstone.dashboard.assign-asset-modal',['tl_list' => $array_list])!!}
{!!view('edit-modal',['tl_list' => $array_list2])!!}
{!!view('display-record',['tl_list' => $array_list])!!}
<div class="">
  <h2>Inventory List</h2>
  <div class="pull-right"><a href="{{route('bloodstone.forms.main')}}" class="btn btn-primary" role="button">Add</a></div>
  

  
  
  
  
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">All</a></li>
    <li><a data-toggle="tab" href="#menu1">Available</a></li>
    <li><a data-toggle="tab" href="#menu2">Deployed</a></li>

   
  </ul>


  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
    
      <table id="example2" class="table table-bordered nowrap table-striped table-condensed table-hover" cellspacing="0" width="100%">
        <thead>
            <th>QR</th>
            <th>Unique ID</th>
			<th>Actions</th>
            <th>Type</th>
            <th>Brand</th>
            <th>Model</th>
            <th>Serial No.</th>
            <th>Description</th>
            <th>Remarks</th>
			<th>Assigned By</th>
            <th>Assigned To</th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Department</th>
            
        </thead>

        <tbody>
            @foreach($items as $item)
			
			<?php $Logger2 = new AppHelper; $larr_history = array();?>
			<?php $historystring = "";
			$larr_history =  json_decode($Logger2->DisplayRecordHistory($item->unique_id),true); 
			
			$historystring .=  "[";
			foreach($larr_history as $logkey => $logval){
					$historystring .= "['" . $logval["assigned_by"] . "','" .  $logval["remarks"] .  "','" .  $logval["created_at"] . "']";
					if (next($larr_history)==true) $historystring .= ",";
				}$historystring .=  "]";
			 
															
											
			
			
			
			?>
            <tr>
            <td><img src="{{$qrcode.$item->unique_id}}" alt="My QR code"></td>
			<td>{{!empty($item->unique_id)?($item->unique_id):"N/A"}}</td>
            <td>	<button class="btn btn-info btn-sm btn_history"  data-toggle="modal" 
											onclick="loadDoc('{{$item->unique_id}}');myfunctions({{$historystring}});"
											data-recorddata='{{$historystring}}';
											<?php /*data-recorddata="jQuery.parseJSON({{$Logger2->DisplayRecordHistory($item->unique_id)}})" */?>

											data-target=".modalHistory">History</button>
				
					<?php if($item->current_status == "available")
					{
						?>
						<button type="button" class="btn btn-sm btn-primary fa fa-plus"  data-toggle="modal" 
											  onclick="myfunctions();$('.class_unique_id').val('{{$item->unique_id}}');
											  $('.class_remarks').val('{{$value_field['remarks']}}');"
											  data-target=".modalAddUser">Assign</button>
						<?php
					}
					else if($item->current_status == "deployed")
					{
						?>
						<button type="button" class="btn btn-sm btn-warning btn-sm fa fa-plus"  data-toggle="modal" 
											  onclick="myfunctions();$('.class_unique_id').val('{{$item->unique_id}}');
											  $('.class_remarks').val('{{$value_field['remarks']}}');"
											  data-target=".modalEditUser">Return</button>
						
						<?php
					}
			?>
			
			</td>
            <td>{{!empty($item->type->name)?($item->type->name):"N/A"}}</td>
            <td>{{!empty($item->brand->name)?($item->brand->name):"N/A"}}</td>
            <td>{{!empty($item->model->name)?($item->model->name):"N/A"}}</td>
            <td>{{!empty($item->serial_no)?($item->serial_no):"N/A"}}</td>
            <td>{{!empty($item->description)?($item->description):"N/A"}}</td>
            <td>{{!empty($item->remarks)?($item->remarks):"N/A"}}</td>
			<td>{{!empty($item->assignedby->name)?($item->assignedby->name):"N/A"}}</td>
            <td>{{!empty($item->assignedto->name)?($item->assignedto->name):"N/A"}}</td>
            <td>{{!empty($item->createdby->name)?($item->createdby->name):"N/A"}}</td>
            <td>{{!empty($item->updatedby->name)?($item->updatedby->name):"N/A"}}</td>
            <td>{{!empty($item->dept->name)?($item->dept->name):"N/A"}}</td>
    
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    <div id="menu1" class="tab-pane fade">
      <table id="example3" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <th>Unique ID</th>
			<th>Actions</th>
            <th>Type</th>
            <th>Brand</th>
            <th>Model</th>
            <th>Serial No.</th>
            <th>Description</th>
            <th>Remarks</th>
			<th>Assigned By</th>
            <th>Assigned To</th>
            <th>Created At</th>
            <th>Updated By</th>
            <th>Department</th>

        </thead>

        <tbody>
            @foreach($items as $item)
			@if ($item->current_status == "available")
            <tr>
            <td>{{!empty($item->unique_id)?($item->unique_id):"N/A"}}</td>
			<td><button class="btn btn-info btn-sm btn_history"  data-toggle="modal" 
											onclick="loadDoc('{{$item->unique_id}}');myfunctions({{$historystring}});"
											data-recorddata='{{$historystring}}';
											<?php /*data-recorddata="jQuery.parseJSON({{$Logger2->DisplayRecordHistory($item->unique_id)}})" */?>

											data-target=".modalHistory">History</button>
											<button type="button" class="btn btn-sm btn-primary fa fa-plus"  data-toggle="modal" 
											  onclick="myfunctions();$('.class_unique_id').val('{{$item->unique_id}}');
											  $('.class_remarks').val('{{$value_field['remarks']}}');"
											  data-target=".modalAddUser">Assign</button></td>
            <td>{{!empty($item->type->name)?($item->type->name):"N/A"}}</td>
            <td>{{!empty($item->brand->name)?($item->brand->name):"N/A"}}</td>
            <td>{{!empty($item->model->name)?($item->model->name):"N/A"}}</td>
            <td>{{!empty($item->serial_no)?($item->serial_no):"N/A"}}</td>
            <td>{{!empty($item->description)?($item->description):"N/A"}}</td>
            <td>{{!empty($item->remarks)?($item->remarks):"N/A"}}</td>
			<td>{{!empty($item->assignedby->name)?($item->assignedby->name):"N/A"}}</td>
            <td>{{!empty($item->assignedto->name)?($item->assignedto->name):"N/A"}}</td>
            <td>{{!empty($item->createdby->name)?($item->createdby->name):"N/A"}}</td>
            <td>{{!empty($item->updatedby->name)?($item->updatedby->name):"N/A"}}</td>
            <td>{{!empty($item->dept->name)?($item->dept->name):"N/A"}}</td>
            
            </tr>
			@endif
            @endforeach
        </tbody>
    </table>
    </div>
    <div id="menu2" class="tab-pane fade">
     <table id="example" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <th>Unique ID</th>
			<th>Actions</th>
            <th>Type</th>
            <th>Brand</th>
            <th>Model</th>
            <th>Serial No.</th>
            <th>Description</th>
            <th>Remarks</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Department</th>
            <th>Assigned By</th>
            <th>Assigned To</th>
        </thead>

        <tbody>
            @foreach($items as $item)
				@if ($item->current_status == "deployed")
            <tr>
            <td>{{!empty($item->unique_id)?($item->unique_id):"N/A"}}</td>
			<td><button class="btn btn-info btn-sm btn_history"  data-toggle="modal" 
											onclick="loadDoc('{{$item->unique_id}}');myfunctions({{$historystring}});"
											data-recorddata='{{$historystring}}';
											<?php /*data-recorddata="jQuery.parseJSON({{$Logger2->DisplayRecordHistory($item->unique_id)}})" */?>

											data-target=".modalHistory">History</button>
			<button type="button" class="btn btn-sm btn-warning btn-sm fa fa-plus"  data-toggle="modal" 
											  onclick="myfunctions();$('.class_unique_id').val('{{$item->unique_id}}');
											  $('.class_remarks').val('{{$value_field['remarks']}}');"
											  data-target=".modalEditUser">Return</button></td>
            <td>{{!empty($item->type->name)?($item->type->name):"N/A"}}</td>
            <td>{{!empty($item->brand->name)?($item->brand->name):"N/A"}}</td>
            <td>{{!empty($item->model->name)?($item->model->name):"N/A"}}</td>
            <td>{{!empty($item->serial_no)?($item->serial_no):"N/A"}}</td>
            <td>{{!empty($item->description)?($item->description):"N/A"}}</td>
            <td>{{!empty($item->remarks)?($item->remarks):"N/A"}}</td>
            <td>{{!empty($item->created_at)?($item->created_at):"N/A"}}</td>
            <td>{{!empty($item->updated_at)?($item->updated_at):"N/A"}}</td>
            <td>{{!empty($item->createdby->name)?($item->createdby->name):"N/A"}}</td>
            <td>{{!empty($item->updatedby->name)?($item->updatedby->name):"N/A"}}</td>
            <td>{{!empty($item->dept->name)?($item->dept->name):"N/A"}}</td>
            <td>{{!empty($item->assignedby->name)?($item->assignedby->name):"N/A"}}</td>
            <td>{{!empty($item->assignedto->name)?($item->assignedto->name):"N/A"}}</td>
            </tr>
				@endif
            @endforeach
        </tbody>
    </table>
    </div>
	
	
    
  </div>
</div>








@endsection 

@section('footer-scripts')
      <script type="text/javascript">
    var  dataSet = [
    [ "Tiger Nixon", "System Architect",  "2011/04/25" ],
    [ "Garrett Winters", "Accountant","2011/07/25"],
    [ "Ashton Cox", "Junior Technical Author","2009/01/12" ],
    [ "Cedric Kelly", "Senior Javascript Developer", "2012/03/29" ],
    [ "Airi Satou", "Accountant","2008/11/28"]];

function myfunctions(datasetsrecord)
{
	
/*	
	$('#displayrecord').DataTable(
        {
		data: datasetsrecord,
        columns: [
            { title: "Name" },
            { title: "Remarks" },
            { title: "Date" }
           
        ]
        });		
		$('#displayrecord').DataTable().ajax.reload();	*/
}	
	

$(document).ready(function() {
/* 	$.ajax({
  
				type: "GET",
				url: 'http://localhost:8000/users/a"',
				dataType: 'json',
				success: function (obj, textstatus) {
			  
						$('#displayrecord').DataTable({
							data: obj,
							columns: [
								{ data: 'MONTH' },
								{ data: 'TIME' }, //or { data: 'MONTH', title: 'Month' }`
								{ data: 'TEXT' }
							]
						});
					 
				 
				},
				error: function (obj, textstatus) {
					alert(obj.msg);
				}
			}); */
	
	$('#upload-new-data').on('click', function () {
   datatable.draw(); // Redraw the DataTable
});
	
	
    $('#example').DataTable(
        {
        scrollX: true,
        scrollY: '600px',
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'print'
        ]
        });
	$('#example2').DataTable(
        {
        scrollX: true,
        scrollY: '600px',
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'print'
        ]
        });
		$('#example3').DataTable(
        {
        scrollX: true,
        scrollY: '600px',
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'print'
        ]
        });
		/* $('#displayrecord').DataTable( {
				
				"ajax": "http://localhost:8000/users/a"
			} ); */
		
	
		/*
	$('#example3').DataTable(
        {
        scrollX: true,
        scrollY: '600px',
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'print'
        ]
        });		*/
			
		


} );

function loadDoc( unique_id) {
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", "http://localhost:8000/bloodstone/ajax/42?unique_id=" + unique_id, false);
  xhttp.send();
  document.getElementById("ajaxresult").innerHTML = xhttp.responseText;
}
</script>
@endsection

