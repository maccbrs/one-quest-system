



<div class="modal fade modalAddUserModel" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo $tl_list['form-action'] ?>">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title custom_align"><?php echo $tl_list['header'] ?></h4>
                </div>
                <div class="modal-body">
							

							@foreach($tl_list['form-list'] as $tl => $tls)
                              <?php 
							  $fields_param = explode("|", $tls);
							  if ($fields_param[0] ==='text') {
								  ?>
							  <fieldset class="form-group"> 
											<label for="exampleSelect1">{{ucwords(str_replace("_"," ",$fields_param[1]))}}</label> 
											<input type="{{$fields_param[0]}}" class="form-control input-sm class_{{$fields_param[2]}}" class="form-control input-sm" name="{{$fields_param[2]}}">
							  </fieldset>
							  <?php 
							  }
							  else if ($fields_param[0] ==='textarea') {
							  ?>
							 <fieldset class="form-group">
								<label for="exampleSelect1">{{ucwords(str_replace("_"," ",$fields_param[1]))}}</label>
								<textarea class="form-control input-sm" class="form-control input-sm class_{{$fields_param[2]}}" name="{{$fields_param[2]}}" ></textarea>
							 </fieldset>  
							  
							  <?php
							  }
							  else if ($fields_param[0] ==='hidden') {
							  ?>
							
								
								<input type="{{$fields_param[0]}}" class="form-control input-sm class_{{$fields_param[2]}}" name="{{$fields_param[2]}}" >
							
							  
							  <?php
							  }
							  
							  
							   else if ($fields_param[0] ==='select') {
							  
							  $val_list = explode(",", $fields_param[2]);
							  
							  ?>
							 <fieldset class="form-group">
							   <label for="sel1">Select list {{ucwords(str_replace("_"," ",$fields_param[1]))}}: </label>
							  <select class="form-control input-sm" class="form-control input-sm class_{{$fields_param[2]}}" id="sel1" name="{{$tl}}">
								<?php
								foreach($val_list as $keys => $val)
								{	$val_list2 = explode(":",  $val);
									echo "<option value='" . $val_list2[0] . "'>" . $val_list2[1] . "</option>";
								}
								?>
								
							  </select>
								<!--<label for="exampleSelect1">{{ucwords(str_replace("_"," ",$fields_param[1]))}}</label>
								<textarea class="form-control input-sm" name="{{$fields_param[2]}}" ></textarea> -->
							 </fieldset>  
							  
							  <?php
							  }
							  ?>
							  
                            @endforeach					
				
					
                    
                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-sign"></span> Save
                    </button>
                </div>
            </form>
        </div> 
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>