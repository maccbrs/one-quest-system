<?php $asset = URL::asset('/'); ?> 
<?php $array_list_type = array(
              "header"=> "Add " . 'MstType', 
              "form-action"=> route('bloodstone.masterfile.create','MstType'), 
              "form-list"=> /* $larr_outputArray2*/array("name" => "text|Name|name",
                                   "alias" => "text|Alias|alias",
                                   "remarks" => "textarea|Remarks|remarks"
                                  ) 
              );?>
<?php $array_list_brand = array(
              "header"=> "Add " . 'MstBrand', 
              "form-action"=> route('bloodstone.masterfile.create','MstBrand'), 
              "form-list"=> /* $larr_outputArray2*/array("name" => "text|Name|name",
                                   "alias" => "text|Alias|alias",
                                   "remarks" => "textarea|Remarks|remarks"
                                  ) 
              );?>
<?php $array_list_model = array(
              "header"=> "Add " . 'MstModel', 
              "form-action"=> route('bloodstone.masterfile.create','MstModel'), 
              "form-list"=> /* $larr_outputArray2*/array("name" => "text|Name|name",
                                   "alias" => "text|Alias|alias",
                                   "remarks" => "textarea|Remarks|remarks"
                                  ) 
              );?>
<?php $array_list_status = array(
              "header"=> "Add " . 'MstStatus', 
              "form-action"=> route('bloodstone.masterfile.create','MstStatus'), 
              "form-list"=> /* $larr_outputArray2*/array("name" => "text|Name|name",
                                   "alias" => "text|Alias|alias",
                                   "remarks" => "textarea|Remarks|remarks"
                                  ) 
              );?>
<?php $array_list_vendor = array(
              "header"=> "Add " . 'MstVendor', 
              "form-action"=> route('bloodstone.masterfile.create','MstVendor'), 
              "form-list"=> /* $larr_outputArray2*/array("name" => "text|Name|name",
                                   "alias" => "text|Alias|alias",
                                   "remarks" => "textarea|Remarks|remarks"
                                  ) 
              );?>
<?php $array_list_po = array(
              "header"=> "Add " . 'MstPo', 
              "form-action"=> route('bloodstone.masterfile.create','MstPo'), 
              "form-list"=> /* $larr_outputArray2*/array("name" => "text|Name|name",
                                   /*"alias" => "text|Alias|alias",*/
                                   "remarks" => "textarea|Remarks|remarks"
                                  ) 
              );?>
<?php $array_list_or = array(
              "header"=> "Add " . 'MstOr', 
              "form-action"=> route('bloodstone.masterfile.create','MstOr'), 
              "form-list"=> /* $larr_outputArray2*/array("name" => "text|Name|name",
                                   /*"alias" => "text|Alias|alias",*/
                                   "remarks" => "textarea|Remarks|remarks"
                                  ) 
              );?>              
@extends('bloodstone.master')

@section('title')
Main Form
@endsection

@section('header-css')
<style>
.custom
{
    width: 90% !important;
}
.sidenav
{
    width: 10% !important; 
}
.table-condensed
{
  font-size: 10px;
}
.form-control
{
  font-size: 10px !important;
}
/*th
{
    background-color: #f1f1f1;
}*/
/*.list-group-item-heading {
    padding: 0px 0px !important;
}*/
</style>
@endsection

@section('content')
    {!!view('bloodstone.create-modal-type',['tl_list' => $array_list_type])!!}
    {!!view('bloodstone.create-modal-brand',['tl_list' => $array_list_brand])!!}
    {!!view('bloodstone.create-modal-model',['tl_list' => $array_list_model])!!}
    {!!view('bloodstone.create-modal-status',['tl_list' => $array_list_status])!!}
    {!!view('bloodstone.create-modal-vendor',['tl_list' => $array_list_vendor])!!}
    {!!view('bloodstone.create-modal-or',['tl_list' => $array_list_or])!!}
    {!!view('bloodstone.create-modal-po',['tl_list' => $array_list_po])!!}


    <div class="" style="margin-top: 20px;">
        <div class="row form-group">
            <div class="col-xs-12">
                <ul class="nav nav-tabs setup-panel">
                    <li class="active"><a href="#step-1">
                        Item Registration
                        <!-- <p class="list-group-item-text">Item Registration</p> -->
                    </a></li>
                    <!-- <li class="disabled"><a href="#step-2">
                        <h4 class="list-group-item-heading">Step 2</h4>
                        <p class="list-group-item-text">Deployment</p>
                    </a></li> -->
<!--                     <li class="disabled"><a href="#step-3">
                        <h4 class="list-group-item-heading">Step 3</h4>
                        <p class="list-group-item-text">Third step description</p>
                    </a></li>
                    <li class="disabled"><a href="#step-4">
                        <h4 class="list-group-item-heading">Step 4</h4>
                        <p class="list-group-item-text">Second step description</p>
                    </a></li>  -->   
                </ul>
                @if (session('return_msg'))
                <div class="alert alert-danger">
                    {{ session('return_msg') }}
                </div>
                @endif
            </div>
        </div>
    </div>  

<form class="" method="POST">
{{ csrf_field() }}
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
              <!--   <h1>ITEM REGISTRATION</h1> -->

<!-- <form> -->               
                
    <div class="container col-xs-12">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <table class="table table-bordered nowrap table-striped table-condensed table-hover" id="tab_logic">
                <thead class="thead-inverse">
                    <tr >
                        <th class="text-center">
                            #
                        </th>
                        <th class="text-center">
                            Unique ID
                        </th>
                        <th class="text-center">
                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target=".modalAddUserType"><span class="glyphicon glyphicon-edit"></span></button><br>
                            Type
                        </th>
                        <th class="text-center">
                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target=".modalAddUserBrand"><span class="glyphicon glyphicon-edit"></span></button><br>
                            Brand
                        </th>
                        <th class="text-center">
                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target=".modalAddUserModel"><span class="glyphicon glyphicon-edit"></span></button><br>
                            Model
                        </th>
                        <th class="text-center">
                            Serial No. 
                        </th>
                        <th class="text-center">
                            Description
                        </th>
                        <th class="text-center">
                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target=".modalAddUserStatus"><span class="glyphicon glyphicon-edit"></span></button><br>
                            Status
                        </th>
                        <th class="text-center">
                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target=".modalAddUserVendor"><span class="glyphicon glyphicon-edit"></span></button><br>
                            Vendor
                        </th>
                        <th class="text-center">
                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target=".modalAddUserPo"><span class="glyphicon glyphicon-edit"></span></button><br>
                            P.O. #
                        </th>
                        <th class="text-center">
                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target=".modalAddUserOr"><span class="glyphicon glyphicon-edit"></span></button><br>
                            O.R. #
                        </th>
                        <th class="text-center">
                            Remarks
                        </th>
                        <th class="text-center">
                            Add/Delete Row
                        </th>   
                                                
                    </tr>
                </thead>
                <tbody>
                    <tr id='addr0'>
                        <td>
                        1
                        </td>
                        <td>
                        <input type="text" name='unique_id[]' placeholder='Unique Id' class="form-control input-sm " required/>
                        </td>
                        <td>
                        <select type="text" name="type_id[]" class="form-control input-sm">
                            <option value=''>Select</option>
                            @if(is_array($typeitems)||is_object($typeitems))
                            @foreach($typeitems as $k => $v)
                            <option value='{{$k}}'>{{$v}}</option>
                            @endforeach
                            @endif
                        </select>
                        
                        </td>
                        
                        <td>
                        <select type="text" name="brand_id[]" class="form-control input-sm">
                            <option value=''>Select</option>
                            @if(is_array($branditems)||is_object($branditems))
                            @foreach($branditems as $k => $v)
                            <option value='{{$k}}'>{{$v}}</option>
                            @endforeach
                            @endif
                        </select>
                        </td>

                        <td>
                        <select type="text" name="model_id[]" class="form-control input-sm">
                            <option value=''>Select</option>
                            @if(is_array($modelitems)||is_object($modelitems))
                            @foreach($modelitems as $k => $v)
                            <option value='{{$k}}'>{{$v}}</option>
                            @endforeach
                            @endif
                        </select>                        
                        </td>

                        <td>
                        <input type="text" name='serial_no[]' placeholder='Serial #' class="form-control input-sm"/>
                        </td>

                        <td>
                        <input type="text" name='description[]' placeholder='Description' class="form-control input-sm"/>
                        </td>

                        <td>
                        <select type="text" name="status_id[]" class="form-control input-sm">
                            <option value=''>Select</option>
                            @if(is_array($statusitems)||is_object($statusitems))
                            @foreach($statusitems as $k => $v)
                            <option value='{{$k}}'>{{$v}}</option>
                            @endforeach
                            @endif
                        </select>
                        </td>

                        <td>
                        <select type="text" name="vendor_id[]" class="form-control input-sm">
                            <option value=''>Select</option>
                            @if(is_array($vendoritems)||is_object($vendoritems))
                            @foreach($vendoritems as $k => $v)
                            <option value='{{$k}}'>{{$v}}</option>
                            @endforeach
                            @endif
                        </select>
                        </td>

                        <td>
                        <select type="text" name="purchase_id[]" class="form-control input-sm">
                            <option value=''>Select</option>
                            @if(is_array($purchaseitems)||is_object($purchaseitems))
                            @foreach($purchaseitems as $k => $v)
                            <option value='{{$k}}'>{{$v}}</option>
                            @endforeach
                            @endif
                        </select>
                        </td>    

                        <td>
                        <select type="text" name="receipt_id[]" class="form-control input-sm">
                            <option value=''>Select</option>
                            @if(is_array($receiptitems)||is_object($receiptitems))
                            @foreach($receiptitems as $k => $v)
                            <option value='{{$k}}'>{{$v}}</option>
                            @endforeach
                            @endif
                        </select>
                        </td>                                            

                        <td>
                        <input type="text" name='remarks[]' placeholder='Remarks' class="form-control input-sm"/>
                        </td>

                        <td>
                            <div class="row" style="margin-top:4px;">
                                <div class="col-md-12 col-md-offset-1">
                                    <div class="btn-group btn-group-xs">
                                        <a id="add_row" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></a>
                                        <a id="delete_row" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-minus"></span></a>
                                    </div>
                                </div>
                            </div>
                        </td>

                        <td hidden>
                        <input type="hidden" name='active[]' class="form-control input-sm" value="1">
                        </td>
                        <td hidden>
                        <input type="hidden" name='cby[]' class="form-control input-sm" value="{{Auth::user()->id}}">
                        </td>
                        <td hidden>
                        <input type="hidden" name='uby[]' class="form-control input-sm" value="{{Auth::user()->id}}">
                        </td>
                        <td hidden>
                        <input type="hidden" name='cat[]' class="form-control input-sm" value="{{date('Y-m-d H:i:s')}}">
                        </td>
                        <td hidden>
                        <input type="hidden" name='uat[]' class="form-control input-sm" value="{{date('Y-m-d H:i:s')}}">
                        </td>
                    </tr>
                    <tr id='addr1'></tr>
                </tbody>
                </table>
            </div>
        </div>
    </div>
                
<!-- </form> -->
                
                <button type="Submit" class="btn btn-primary btn-md col-md-offset-5">Submit <span class="glyphicon glyphicon-floppy-disk"></span></button>
            </div>
        </div>
    </div>

</form>

<form class="container">

    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12 well text-center">
<!--                 <h1 class="text-center"> STEP 2</h1> -->
                
<!-- <form> -->

    

<!-- </form>   -->
                
                <button id="activate-step-3" class="btn btn-primary btn-md">Activate Step 3</button>
            </div>
        </div>
    </div>

</form>

<form class="container">

    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12 well text-center">
                <h1 class="text-center"> STEP 3</h1>
                
<!--<form></form> --> 
                
                <button id="activate-step-4" class="btn btn-primary btn-md">Activate Step 4</button>
            </div>
        </div>
    </div>

</form>

<form class="container">
    
    <div class="row setup-content" id="step-4">
        <div class="col-xs-12">
            <div class="col-md-12 well text-center">
                <h1 class="text-center"> STEP 4</h1>
                
<!--<form></form> -->                
                
            </div>
        </div>
    </div>

</form>



@endsection 

@section('footer-scripts')

      <script type="text/javascript">
    
// Activate Next Step

$(document).ready(function() {
    
    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');
        
        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });
    
    $('ul.setup-panel li.active a').trigger('click');
    
    // DEMO ONLY //
    $('#activate-step-2').on('click', function(e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        $(this).remove();
    })
    
    $('#activate-step-3').on('click', function(e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-3"]').trigger('click');
        $(this).remove();
    })
    
    $('#activate-step-4').on('click', function(e) {
        $('ul.setup-panel li:eq(3)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-4"]').trigger('click');
        $(this).remove();
    })
    
    $('#activate-step-3').on('click', function(e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-3"]').trigger('click');
        $(this).remove();
    })
});


// Add , Dlelete row dynamically

     $(document).ready(function(){
      var i=1;
     $("#add_row").click(function(){
      $('#addr'+i).html("<td>"+ (i+1) +"</td><td><input  name='unique_id["+i+"]' type='text' placeholder='Unique ID'  class='form-control input-sm input-md' required></td><td><select type='text' name='type_id["+i+"]' class='form-control input-sm'><option value=''>Select</option>@if(is_array($typeitems)||is_object($typeitems))@foreach($typeitems as $k => $v)<option value='{{$k}}'>{{$v}}</option>@endforeach @endif</select></td><td><select type='text' name='brand_id["+i+"]' class='form-control input-sm'><option value=''>Select</option>@if(is_array($branditems)||is_object($branditems))@foreach($branditems as $k => $v)<option value='{{$k}}'>{{$v}}</option>@endforeach @endif</select></td><td><select type='text' name='model_id["+i+"]' class='form-control input-sm'><option value=''>Select</option>@if(is_array($modelitems)||is_object($modelitems))@foreach($modelitems as $k => $v)<option value='{{$k}}'>{{$v}}</option>@endforeach @endif</select></td><td><input name='serial_no["+i+"]' type='text' placeholder='Serial #' class='form-control input-sm input-md'  /> </td><td><input name='description["+i+"]' type='text' placeholder='Description' class='form-control input-sm input-md'  /> </td><td><select type='text' name='status_id["+i+"]' class='form-control input-sm'><option value=''>Select</option>@if(is_array($statusitems)||is_object($statusitems))@foreach($statusitems as $k => $v)<option value='{{$k}}'>{{$v}}</option>@endforeach @endif</select></td><td><select type='text' name='vendor_id["+i+"]' class='form-control input-sm'><option value=''>Select</option>@if(is_array($vendoritems)||is_object($vendoritems))@foreach($vendoritems as $k => $v)<option value='{{$k}}'>{{$v}}</option>@endforeach @endif</select></td><td><select type='text' name='purchase_id["+i+"]' class='form-control input-sm'><option value=''>Select</option>@if(is_array($purchaseitems)||is_object($purchaseitems))@foreach($purchaseitems as $k => $v)<option value='{{$k}}'>{{$v}}</option>@endforeach @endif</select></td><td><select type='text' name='receipt_id["+i+"]' class='form-control input-sm'><option value=''>Select</option>@if(is_array($receiptitems)||is_object($receiptitems))@foreach($receiptitems as $k => $v)<option value='{{$k}}'>{{$v}}</option>@endforeach @endif</select></td><td><input  name='remarks["+i+"]' type='text' placeholder='Remarks'  class='form-control input-sm input-md'></td><td hidden><input  name='active["+i+"]' type='hidden' placeholder='Active'  class='form-control input-sm input-md' value='1'></td><td hidden><input type='hidden' name='cby["+i+"]' class='form-control input-sm' value='{{Auth::user()->id}}''></td><td hidden><input type='hidden' name='uby["+i+"]' class='form-control input-sm' value='{{Auth::user()->id}}''></td><td hidden><input type='hidden' name='uat[]' class='form-control input-sm' value='{{date('Y-m-d H:i:s')}}''></td><td hidden><input type='hidden' name='cat[]' class='form-control input-sm' value='{{date('Y-m-d H:i:s')}}''></td>");

      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
      i++; 
  });
     $("#delete_row").click(function(){
         if(i>1){
         $("#addr"+(i-1)).html('');
         i--;
         }
     });

});

</script>
@endsection

