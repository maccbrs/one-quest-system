
<?php $assets = URL::asset('/');?>
<?php $pic = URL::asset('/uploads/');?>
<?php $Logger = new AppHelper; $larr_myarray = array();?>
<?php $Auth = Auth::user()->id;?>
<?php $larr_myarray =  json_decode($Logger->DisplayLog(Auth::user()->id),true); $count = count($larr_myarray);?>
<?php $GemUser = new \App\Http\Models\gem\User;?>
<?php $avatar =  $GemUser->where('id','=',$Auth)->get()?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @yield('header-css')
  <link rel="stylesheet" href="{{$assets}}inventory/css/bootstrap.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/css/buttons.dataTables.css">
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 667px;}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Inventory</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="{{route('bloodstone.home')}}">Dashboard</a></li>
        <li><a href="{{route('bloodstone.forms.main')}}">Forms</a></li>
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Master Files
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
   

			<li><a href="{{$assets}}bloodstone/masterfile/MstBrand">Brand</a></li>
			<li><a href="{{$assets}}bloodstone/masterfile/MstDepartment">Department</a></li>
			<li><a href="{{$assets}}bloodstone/masterfile/MstModel">Model</a></li>
			<li><a href="{{$assets}}bloodstone/masterfile/MstStatus">Status</a></li>
			<li><a href="{{$assets}}bloodstone/masterfile/MstType">Type</a></li>

		  
        </ul>
      </li>
        <li><a href="#">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
							
				@if(!empty($Logger->DisplayLog(Auth::user()->id)))

			  <li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Logs <span class="label label-danger">{{$count}}</span><span class="caret"></span></a>
				
				
				<ul class="dropdown-menu" style="width:300px;font-size:10px;">
					
					@foreach($larr_myarray as $logkey => $logval)
					
									<li><div style='padding:10px;'>{{$logval['description'] . $logval['created_at']}}</div></li><hr style='margin-top:5px;margin-bottom:5px'/>
                    @endforeach
									<li><div style='padding:10px;'>View All</div></li><hr style='margin-top:5px;margin-bottom:5px'/>
                    @endif
                      
				</ul>
			  </li>
        <li><a href={{route('gem.dashboard.index')}}><span class="glyphicon glyphicon-home"></span> Milestone</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">

      <div class="user-info">
        <div class="admin-image"><img src="{{$pic.'/sm-'.$avatar[0]['avatar']}}" alt=""></div>
          <div class="admin-action-info"> <span>Welcome<br>{{Auth::user()->name}}</span></div>
      </div>

      <div class="col-sm-12" style="background-color:#D3D3D3">
        	<span class="col-sm-12"><b>@yield('title')</b></span>
      </div>

<!--       <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p> -->
    </div>
    <div class="col-sm-10 text-left custom" style="margin-top: 20px;"> 
    	@yield('content')
    </div>
  </div>
</div>

<!-- <footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer> -->

</body>
      <script src="{{$assets}}inventory/js/jquery-3.2.1.js"></script>
      <script src="{{$assets}}inventory/datatables/jquery-ui.js"></script>
      <script src="{{$assets}}inventory/datatables/bootstrap.min.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
      <script src="{{$assets}}inventory/datatables/jquery.dataTables.yadcf.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>

@yield('footer-scripts')

</html>