<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

<div class="card">
   <div class="card-header no-bg b-a-0"> </div>
      <div class="card-block">
        <div class="table-responsive">
          <div class="content-view">
             <div class="fill-container layout-xs b-b">
              <div class="layout-column-xs overflow-hidden">
                <div class="row m-x-0 fill-container">

                  <div class="col-lg-4 p-a-0 messages-list bg-white b-r flexbox-xs layout-column-xs full-height">
                     <div class="message-header">
                        <div class="message-toolbar">
                         
                             <h4>Group List </h4>

                        </div>
                     </div>

                     <div class="flex-xs scroll-y">
                        <ul class="message-list nav nav-tabs" >

                          
                          @foreach($all_groups as $group)

                            <li class="nav-item message-list-item message_item">
                                <a class="nav-link " data-toggle="tab" href="#group{{$group->id}}" role="tab">
                                  <img class="avatar avatar-sm img-circle pull-left m-r-1" onerror="this.src='{{$asset}}milestone/images/unknown.jpg'" src="{{$asset}}milestone/images/{{$group->id}}.jpg">
                                   <div class="message-list-item-header">
                                      <div class="time"></div>
                                      <span class="bold">{{$group->name}}</span>
                                   </div><p class="overflow-hidden"></p>
                                </a>
                            </li>

                          @endforeach


                        </ul>
                     </div>
                  </div>

                  <div class="col-lg-8 p-a-0 messages-list bg-white b-r flexbox-xs layout-column-xs full-height">
                     <div class="flexbox-xs layout-column-xs message-view">
                        <div class="message-header">
                           <div class="message-toolbar">
                              <div class="pull-right">
                                  
                               <div class = "pull-right">
                                  <a data-toggle="modal" data-target="#myModalGroup" class="m-l-1" tooltip-placement="bottom" uib-tooltip="Reply to all recipients">
                                    <button type="button" class="btn btn-primary m-r-xs m-b-xs">Create Group</button>
                                 </a>
                                </div>

                              </div>
                              <a href="javascript:;" class="hidden-lg-up" data-toggle="message">
                                 <i class="material-icons m-r-1">arrow_back</i>
                              </a>
                           </div>
                        </div>

                        <div class="message-body flex-xs scroll-y">
                          <div class="tab-content">


                            @foreach($all_groups as $group)

                              <?php $group_id = 'group'.$group->id ?>

                                <div  class="tab-pane" id="group{{$group->id}}" role="tabpanel">

                                <div class="overflow-hidden">
                                 <h4 class="lead m-t-0">
                                  <img class="avatar  img-rounded" style ="width:50px;" alt="" src="{{$asset}}milestone/images/{{$group->user}}.jpg">
                                  {{$group->name}} </h4>
                                    
                                    <a data-toggle="modal" data-target="#myModal{{$group->id}}" class="m-l-1 pull-right btn btn-outline-info" tooltip-placement="bottom" uib-tooltip="Reply to all recipients">
                                      <i class="material-icons">mail</i> New Message
                                    </a>

                                    <a data-toggle="modal" data-target="#edit{{$group->id}}" class="m-l-1 pull-right btn btn-outline-success" >
                                      <i class="material-icons">edit</i> Edit Group
                                    </a>

                                  <div class="date">created {{$group->created_at}}</div>
                                  <div class="message-sender">

                                   (
                                    @foreach($members_list[$group->id] as $member)
                                      @if(!empty($member))
                                       <span class="tag tag-info">{{$users_list[$member]}}</span>
                                      <?php $members = ""; ?>
                                      @endif
                                    @endforeach
                                    )

                                  </div> 
                                  <hr>
                                </div>

                                  <ul class="message-list nav nav-tabs" >

                                    @if(!empty($messages_list[$group->id]))
                                      @foreach($messages_list[$group->id] as $m)
                                       <?php $message = 'message'.$m->id ?>
                                        @if($m->to == $group->id || $m->user == $group->id)
                                      
                                          @if($userId == $m->user)
                                            <?php $pull = 'pull-left from_message'?>
                                          @else
                                            <?php $pull = 'pull-right to_message'?>
                                          @endif

                                            <div class="col-lg-9 {{$pull}}">
                                              <h5>{{$m->content->content}}</h5>
                                              <div class="date">sent by {{$users_list[$m->user]}} at {{$m->created_at}} </div>
                                            </div>

                                          @endif
                                        @endforeach
                                      @endif
                                    </ul>
                                  </div>

                                  <div class="modal fade" id="myModal{{$group->id}}"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                     <div class="modal-dialog" role="document">
                                        <div class="modal-content">

                                          <form id="validate" class="form-horizontal" role="form" method="POST" action="{{route('garnet.team.new_message')}}">
                  
                                            <input type="hidden" value ="1" name="type" > 
                                            <input type="hidden" value ="1" name="status" > 
                                            <input type="hidden" value ="group" name="page" >
                                            <input type="hidden" value ="{{$userId}}" name="user" >

                                            {{ csrf_field() }}

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Send a Message</h4>
                                            </div>

                                            <div class="modal-body">

                                              <fieldset class="form-group">
                                                <label for="exampleSelect1">To</label>
                                                  <select class="form-control" id="exampleSelect1" name = "to">
                                                    <option disabled>Select Recipient</option>
                                                    <option value = "{{$group->id}}" >{{$group->name}}</option>
                                                  </select>
                                              </fieldset>
                                              
                            
                                              <input type="hidden" class="form-control" placeholder="Enter Subject" pattern=".{3,40}" name="subject" > 
                                             

                                              <fieldset class="form-group">
                                                <label for="exampleInputEmail1">Message</label>
                                                <textarea class="form-control" rows="7" name="content" pattern=".{3,100}" ></textarea>
                                              </fieldset>

                                            </div>

                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                              <button type="submit" class="btn btn-primary">Send</button>
                                            </div>
                                          </form>
                                        </div>
                                     </div>
                                  </div>

                                  <div class="modal fade" id="edit{{$group->id}}"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">

                                        <form id="validate" class="form-horizontal" role="form" method="POST" action="{{route('garnet.team.edit_group')}}">
                
                                          <input type="hidden" value ="{{$userId}}" name="user" >
                                          <input type="hidden" value ="{{$group->id}}" name="id" > 

                                          {{ csrf_field() }}

                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title" id="myModalLabel">Edit Group</h4>
                                          </div>

                                          <div class="modal-body">

                                            <fieldset class="form-group">
                                              <label for="exampleInputEmail1">Group Name</label>
                                              <input type="text" class="form-control" value="{{$group['name']}}" pattern=".{3,40}" name="name" > 
                                            </fieldset>

                                            <fieldset class="form-group">
                                              <label for="exampleInputEmail1">Status</label>
                                               <select class="form-control" name = "status">
                                                  <option disabled>Select Status</option>
                                                  <option value = "1" >Active</option>
                                                  <option value = "0" >Inactive</option>
                                                </select>
                                            </fieldset>

                                            <fieldset class="form-group">
                                              <label for="exampleInputEmail1">Operations</label>
                                                <select multiple="multiple" class="my-select" name="members[]">
                                                  <optgroup label='Available Members'>
                                                    @if(!empty($all_agents ))
                                                      @foreach($all_agents as $agent)
                                                        @if(!empty($agent->user_id))
                                                          <option value='{{$agent->user_id}}'>{{$agent->first_name . " " . $agent->last_name}}</option>
                                                        @endif
                                                      @endforeach
                                                    @endif
                                                  </optgroup>

                                                  <option value='{{$userId}}' selected>Yourself</option>
                                       
                                                </select>
                                            </fieldset>

                                            <fieldset class="form-group">
                                              <label for="exampleInputEmail1">Support and Non-Agent Staff</label>
                                                <select multiple="multiple" class="my-select" name="members2[]">
                                                  <optgroup label='Available Members'>
                                                    @if(!empty($non_agent ))
                                                      @foreach($non_agent as $non)
                                                        
                                                          <option value='{{$non->id}}'>{{$non->name}}</option>
                                                        
                                                      @endforeach
                                                    @endif
                                                  </optgroup>
                                                </select>
                                            </fieldset>

                                            @if($userType == 'Human Resource')

                                              <fieldset class="form-group">
                                                <label for="exampleInputEmail1">Human Resource</label>
                                                  <select multiple="multiple" class="my-select" name="members3[]">
                                                    <optgroup label='Available Members'>
                                                      @if(!empty($human_resource ))
                                                        @foreach($human_resource as $hr)
                                                         
                                                            <option value='{{$hr->id}}'>{{$hr->name}}</option>
                                                          
                                                        @endforeach
                                                      @endif
                                                    </optgroup>
                                                  </select>
                                              </fieldset>

                                            @endif

                                          </div>

                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                          </div>
                                        </form>
                                      </div>
                                   </div>
                                </div>


                            @endforeach
                          
                          </div>
                        </div>
                     </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
   </div>    
</div>

<div class="modal fade" id="myModalGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <form id="validate" class="form-horizontal" role="form" method="POST" action="{{route('garnet.team.new_group')}}">
        
        <input type="hidden" value ="1" name="status" > 
        <input type="hidden" value ="{{$userId}}" name="user" >

        {{ csrf_field() }}

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Start a Group Message</h4>
        </div>

        <div class="modal-body">

          <fieldset class="form-group">
            <label for="exampleInputEmail1">Group Name</label>
            <input type="text" class="form-control" placeholder="Group Name"  name="name" > 
          </fieldset>

          <fieldset class="form-group">
            <label for="exampleInputEmail1">Operations</label>
              <select multiple="multiple" class="my-select" name="members[]">
                <optgroup label='Available Members'>
                  @if(!empty($all_agents ))
                    @foreach($all_agents as $agent)
                      @if(!empty($agent->user_id))
                        <option value='{{$agent->user_id}}'>{{$agent->first_name . " " . $agent->last_name}}</option>
                      @endif
                    @endforeach
                  @endif
                </optgroup>

                <option  value='{{$userId}}' selected>Yourself</option>
     
              </select>
          </fieldset>

          <fieldset class="form-group">
            <label for="exampleInputEmail1">Support and Non-Agent Staff</label>
              <select multiple="multiple" class="my-select" name="members2[]">
                <optgroup label='Available Members'>
                  @if(!empty($non_agent ))
                    @foreach($non_agent as $non)
                      
                        <option value='{{$non->id}}'>{{$non->name}}</option>
                      
                    @endforeach
                  @endif
                </optgroup>
              </select>
          </fieldset>

          @if($userType == 'Human Resource')

            <fieldset class="form-group">
              <label for="exampleInputEmail1">Human Resource</label>
                <select multiple="multiple" class="my-select" name="members3[]">
                  <optgroup label='Available Members'>
                    @if(!empty($human_resource ))
                      @foreach($human_resource as $hr)
                       
                          <option value='{{$hr->id}}'>{{$hr->name}}</option>
                        
                      @endforeach
                    @endif
                  </optgroup>
                </select>
            </fieldset>

          @endif
          
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Create</button>
        </div>
      </form>
    </div>
  </div>
</div>


@endsection 

@section('footer-scripts')

    <script src="{{$asset}}custom/multiselect/js/jquery.multi-select.js"></script>

    <script type="text/javascript">


      $('.my-select').multiSelect({ selectableOptgroup: true })

    </script>

@endsection
