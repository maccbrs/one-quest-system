p<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

<?php 
    $tls = $help->tls();
    $ps = $help->ps(); 
?>

<link rel="stylesheet" href="{{$asset}}custom/css/profile_layout.css">

<canvas></canvas>

<div id="Profile">

	<br><br>

	<div class="innerwrap">
		<section class="section1 clearfix">
			<div>
				<div class="row grid clearfix">
					<div class="col4 first">
						<div class="col4 last pull-right">        
	                        <h4>Edit Profile Picture {!!view('garnet.profile.edit-image-modal')!!}</h4>
						</div>

						<img src="{{Request::instance()->query('UserImageM')}}" class="avatar avatar-lg img-circle" alt="">
						
						<h2>{{$user->name}}</h2>
						<h4>{{ucfirst($user->user_type)}}</h4>
						
					</div>

				</div>
			</div>
		
		</section> <br><br>

		<div class="col-lg-8">

			<div class="card card-block">
                <h4>Personal Info</h4> <br>

				<div class="col-lg-3"><b>Name</b></div>
				<div class="col-lg-9"><p>{{$request['UserFname']}}</div>

				<div class="col-lg-3"><b>Email</b></div>
				<div class="col-lg-9"><p>{{$user->email}}</div>

				<div class="col-lg-3"><b>Department</b></div>
				<div class="col-lg-9"><p>{{$user->user_type}}</div>
                                                                                 
            </div> 

           	<div class="card card-block">
                <h4>Relevant Info</h4> <br>

					@if(!empty($user_data['applicant']))
						
						@foreach($user_data['applicant'] as $ctr1 =>$value1)
							<div class="col-lg-3"><b>{{$ctr1 }}</b></div>
							<div class="col-lg-9"><p>{{$value1}}</p></div>
						@endforeach

					@endif
                                                                                 
            </div> 
            
			<div class="card card-block">
				<h4>Other Info</h4> <br>

				<div class="profileinfo"><br>

					@if(!empty($user_data['information']->header))
						
						@foreach($user_data['information']->header as $ctr =>$value)
							<div class="col-lg-3"><b>{{$user_data['information']->header[$ctr]}}</b></div>
							<div class="col-lg-9"><p>{{$user_data['information']->content[$ctr]}}</p></div>
						@endforeach

					@endif

					<span class = "pull-right add-more">
						{!!view('lilac.user.add-user-info',['id' => $user->id,'user_data' =>$user_data ])!!}
					</span>
				</div>
			</div>


		</div>

		<div class="col-lg-4 section2">
			<div class="col3 center">
				<div class="profileinfo"><br>
					<img src="{{Request::instance()->query('UserImageSm')}}" class="avatar avatar-lg img-circle" alt="">
					<h5>{{!empty($user_data['motto']) ? $user_data['motto'] : ""}}</h5>
					{!!view('lilac.user.motto',['id' => $user->id,'user_data' =>$user_data ])!!}
				</div>
			</div>
		</div>

	</div>

</div>

<script src="{{$asset}}custom/js/circling.js" ></script>

@endsection 

@section('footer-scripts')

@endsection