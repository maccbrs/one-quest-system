@extends('poppy.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">


                <div class="col-md-8 col-md-offset-2">
                  <div class="x_panel">

                    <div class="x_title">
                      <h2><i class="fa fa-align-left"></i> Tickets</h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                      <!-- start accordion -->
                      <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                        @foreach($tickets as $k => $t)
                        <div class="panel">
                          <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$k}}" aria-expanded="true" aria-controls="collapse{{$k}}">
                            <h4 class="panel-title">{{($t->subject?$t->subject->subject:'untitled')}} <small class="pull-right">last update <i class="fa fa-clock-o"></i> {{$t->lastresponse}} | <i class="fa fa-comments-o"></i>{{count($t->threads)}} </small> </h4>
                          </a>
                          <div id="collapse{{$k}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body"> 

                                <div class="col-md-12 mail_list_column">
                                  @if($t->threads)
                                    @foreach($t->threads as $threads)
                                    <div class="mail_list">
                                      <div class="right">
                                        <h3>{{$threads['poster']}} <small>{{$threads['created']}}</small></h3>
                                        <p>{{$threads['body']}}</p>
                                      </div>
                                    </div> 
                                    @endforeach
                                  @endif
                                </div>

                                <button type="button" class="btn btn-primary fa fa-mail-reply pull-right" data-toggle="modal" data-target=".bs-example-modal-lg{{$k}}"></button>
                                <div class="modal fade bs-example-modal-lg{{$k}}" tabindex="-1" role="dialog" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">

                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">{{($t->subject?$t->subject->subject:'untitled')}}</h4>
                                      </div>

                                          <form class="form-horizontal form-label-left" method="post" action="{{route('poppy.ticket.reply',[$t->ticket_id,$t->user_id])}}">
                                            <div class="modal-body">
                                                  <input type="hidden" name="_method" value="POST">
                                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">  

                                                  <div class="form-group">
                                                    <label class="control-label col-md-4" for="first-name">Issue Summary:<span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-8">
                                                      <input type="text" required="required" class="form-control col-md-12" name="title">
                                                    </div>
                                                  </div>
                                                  <div class="clearfix"></div>

                                                  <div class="form-group">
                                                    <label class="control-label col-md-4" for="last-name">Issue Details:<span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-8">
                                                      <textarea class="form-control" name="content"></textarea>
                                                    </div>
                                                  </div>
                                                  <div class="clearfix"></div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="submit" class="btn btn-primary">post</button>
                                            </div>
                                        </form>

                                    </div>
                                  </div>
                                </div>
                            </div>
                          </div>
                        </div>
                        @endforeach
                        @if($tickets)
                        {{$tickets->links()}}
                        @endif
                      </div>

                    </div>
                  </div>
                </div>

                </div>
            </div>
        </div>
    </div>
@endsection 
<?php $asset = URL::asset('/'); ?> 
@section('footer-scripts')


@endsection