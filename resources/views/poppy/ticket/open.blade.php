 <?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

    <div class="content-view" id="mbflexlist">
        <div class="layout-xs contacts-container">
            <div class="flexbox-xs layout-column-xs contacts-list b-r" style="max-width:340px;">

                <div class="flex-xs scroll-y">
                  @foreach($tickets as $ticket)
                    <a href="javascript:;" class="column-equal viewticket {{($ticket->isanswered?'isanswered':'')}}" data-toggle="ticket{{$ticket->ticket_id}}">
                        <div class="col v-align-middle contact-details p-l-1">
                           <span class="bold">{{$ticket->subject['subject']}}</span> 
                           <span class="small">{{$ticket->thread['entries'][0]['poster']}}</span>
                        </div>
                    </a>
                  @endforeach
                </div>
            </div>

            <div class="flexbox-xs layout-column-xs contact">
                <div class="contact-header hidden-lg-up">
                    <div class="contact-toolbar"><a href="javascript:;" data-toggle="contact"><i class="material-icons visible-xs m-r-1">arrow_back</i></a>
                    </div>
                </div>
                @foreach($tickets as $tick)
                <div class="flex-xs scroll-y p-a-3 ticket ticket{{$tick->ticket_id}} ">
                    <?php $entries = $helper->sortByDate($tick->thread['entries']); ?>
                     @foreach($entries as $item)
                        <div class="card card-block">
                            <div class="profile-timeline-header">
                                <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
                                </a>
                                <div class="profile-timeline-user-details"><a href="#" class="bold">{{$item['poster']}}</a>
                                    <br><span class="text-muted small">{{$item['created']}}</span>
                                </div>
                            </div>
                            <div class="profile-timeline-content">
                                 {!!$item['body']!!}
                            </div>
                        </div>
                     @endforeach
                </div>
                @endforeach
            </div>
         
        </div>
    </div>
@endsection 

@section('footer-scripts')
<style type="text/css">
#mbflexlist .contacts-list{
   width: 340px;
   min-width: 340px;
}
.viewticket{
   background-color: #dd8f7c;
   margin-bottom: 2px;
}
.viewticket.isanswered{
   background-color: #e8e8e8;
}
.ticket{
   display: none;
}
</style>
<script type="text/javascript">
$(document).on('click','.viewticket',function(){
   var id = $(this).data('toggle');
   $(this).css('border','2px solid #0275d8').siblings().css('border','none');
   $('.ticket').css('display','none');
   console.log($('.'+id).html());
   $('.'+id).css('display','block');
});
</script>
@endsection