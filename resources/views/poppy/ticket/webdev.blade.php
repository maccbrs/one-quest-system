 <?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

<div class="card">
    <div class="card-block">
        <div class="table-responsive">
            <table class="table table-sm table-bordered customized-table">
                <thead>
                    <tr>
                        <th>Subject</th>
                        <th>last responder</th>
                        <th>date</th>
                        <th>last response</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($tickets as $ticket)
                    <?php $entry = $helper->lastEntry($ticket->thread['entries']); ?>
                    <tr>
                        <th scope="row">{{$ticket->subject['subject']}}</th>
                        <td>{{$entry['poster']}}</td>
                        <td>{{$entry['created']}}</td>
                        <td>{{strip_tags($entry['body'])}}</td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection 

@section('footer-scripts')
<style type="text/css">
#mbflexlist .contacts-list{
   width: 340px;
   min-width: 340px;
}
.viewticket{
   background-color: #dd8f7c;
   margin-bottom: 2px;
}
.viewticket.isanswered{
   background-color: #e8e8e8;
}
.ticket{
   display: none;
}
</style>
<script type="text/javascript">

</script>
@endsection