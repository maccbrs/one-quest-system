@extends('poppy.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <div class="x_panel">

                <div class="x_title">
                  <h2>New <small>ticket</small></h2>

                  <div class="clearfix"></div>
                </div>
                
                <div class="x_content">
                  <br />

                    <form class="form-horizontal form-label-left" method="post" action="{{route('poppy.ticket.store')}}">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  

                    <div class="form-group">
                      <label class="control-label col-md-4" for="first-name">Issue Summary:<span class="required">*</span>
                      </label>
                      <div class="col-md-8">
                        <input type="text" required="required" class="form-control col-md-12" name="title">
                      </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="form-group">
                      <label class="control-label col-md-4" for="last-name">Issue Details:<span class="required">*</span>
                      </label>
                      <div class="col-md-8">
                        <textarea class="form-control" name="content"></textarea>
                      </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </form>

                </div>
              </div>
            </div>
          </div>

    </div>
@endsection 
<?php $asset = URL::asset('/'); ?> 
@section('footer-scripts')


    <style type="text/css">
      
      .cchart{
          padding: 5px;
          background-color: #0F9D58; 
          border-radius: 1px;
          color: #fff;     
      }
      .fst{
          background-color: #ee8823;
      }
      .ptd{
          background-color: #589796;
      }
      #calendar{
          margin-top: 11px;
      }
   
      .flip-container {
          perspective: 1000px;
      }
          /* flip the pane when hovered */
          .flip-container:hover .flipper, .flip-container.hover .flipper {
              transform: rotateY(180deg);
          }

      .flip-container, .front, .back {
          width: 100%;
          height: 170px;
      }

      /* flip speed goes here */
      .flipper {
          transition: 0.6s;
          transform-style: preserve-3d;

          position: relative;
      }

      /* hide back of pane during swap */
      .front, .back {
          backface-visibility: hidden;

          position: absolute;
          top: 0;
          left: 0;
      }

      /* front pane, placed above back */
      .front {
          z-index: 2;
          /* for firefox 31 */
          transform: rotateY(0deg);
      }

      /* back, initially hidden pane */
      .back {
          transform: rotateY(180deg);
      }

    </style>
    <script src="{{$asset}}gentella/js/editor/bootstrap-wysiwyg.js"></script>


@endsection