<?php $asset = URL::asset('/'); ?> 
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Magellan Reporter</title>

        <!-- Bootstrap core CSS -->

        <link href="{{$asset}}gentella/css/bootstrap.min.css" rel="stylesheet">

        <link href="{{$asset}}gentella/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{$asset}}gentella/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="{{$asset}}gentella/css/custom.css" rel="stylesheet">
        <link href="{{$asset}}gentella/css/icheck/flat/green.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{$asset}}timepicker2/jquery.datetimepicker.css"/>
        <link href="{{$asset}}gentella/css/ticket.css" rel="stylesheet">

        <script src="{{$asset}}gentella/js/jquery.min.js"></script>
        <script src="{{$asset}}gentella/js/nicescroll/jquery.nicescroll.min.js"></script> 

        <!-- datepicker -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

        <style type="text/css"> 

            .header-image{
                background: url('{{$asset}}gentella/images/header-report.jpg') no-repeat center;
                text-align: center;
                background-size: cover;
      
            }
            .mb-block{
                border-left: 2px solid gray;
                margin-left: 15px;
                padding: 0 5px;                
            }
        </style>
        
        @yield('header-scripts')

    </head>


    <body class="nav-sm">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view hover-test">
                        <div class="clearfix"></div>

                        <!-- menu prile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                
                            </div>
                            <div class="profile_info">
                                <span>Welcome,</span>
                                <h2>admin</h2>
                            </div>
                        </div>

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu"> 

                                    <li>
                                        <a>
                                            <div class="navbar " style="border: 0;">
                                                <span>Welcome</span> 
                                                
                                                    @if (Auth::guest())

                                                            Welcome Guest
                                                    @else
                                                
                                                        @if (Auth::user()->avatar == '' || Auth::user()->avatar == 'default.jpg')
                                                        
                                                            <?php $txt = "default.jpg"; ?>

                                                        @else 
                                                            <?php $txt = Auth::user()->avatar; ?>  
                                                        @endif 

                                                        <div style = "text-align:center; color:white;">
                                                            <img src="{{$asset}}uploads/avatars/{{ $txt }}" class="img-user">
                                                            <h2>{{Auth::user()->name}}</h2>
                                                        </div>
                                                         
                                                    @endif
                                                </div>
                                            </a>

                                        <ul class="nav child_menu" style="display: none">
                                            <li>
                                                <a href="">My Profile</a>
                                            </li>

                                            <li>
                                                <a href="{{ url('/logout') }}">Logout</a>
                                            </li>
                                        </ul>
                                    </li> 

                                    @if($restrictions->allowed('poppy.ticket.create')) 
                                    <li>
                                        <a><i class="fa fa-file"></i>Ticket<span class="fa fa-chevron-down" ></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li>
                                                <a href="{{route('poppy.ticket.create')}}" class="" >New Ticket</a>
                                            </li>                                        

                                        </ul>
                                    </li>
                                    @endif

                                    <li>
                                        <a href="{{ url('/home') }}"><i class="fa fa-home"></i>Home<span class="fa fa-chevron-down" ></span></a>
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <!-- /sidebar menu -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href=""><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="x_panel">
                                  <div class="flip-container" >
                                    <div class="flipper">
                                          <!-- front flip -->
                                        <div class="front header-image">
                                            <div class = "row">
                                              <div class="col-md-6">
                                               <span class = "text-bg">`
                                                <div class = "header-text-holder">

                                                      <h1>        
                                                        <a href="{{route('lily.index')}}" class=""><img src ="{{$asset}}gentella/images/reportGIF.gif" style ="width:8%"></a> Sentry
                                                    </h1>

                                                </div>

                                                </span>
                                              </div>
                                              <div class="col-md-6 logo-holder">
                                                <img src="{{$asset}}gentella/images/logo_on_black.png" style = "width: 330px;" alt="" >

                                                <div class = "header-text-holder pull-right" style = "text-align:center; margin-top:25px; width:50%;">
                                                 <h3>Ticket</h3>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                          <!-- back flip -->
                                           <div class="back" style ="text-align:center;">
                                          </div>
                                        </div>
                                    </div>
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>

        <script src="{{$asset}}gentella/js/bootstrap.min.js"></script>


        <!-- bootstrap progress js -->
        <script src="{{$asset}}gentella/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="{{$asset}}gentella/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="{{$asset}}gentella/js/icheck/icheck.min.js"></script>

        <script src="{{$asset}}gentella/js/custom.js"></script>

        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="{{$asset}}timepicker2/build/jquery.datetimepicker.full.js"></script>


        <script>
          $(function() {

          });

            $(document).ready(function() {
                $('.child_menu').css("display","none");
                $('.nav.side-menu > li').removeClass("active");
            });

             $('.mb-btn-disabled').on('click',function(){
                return false;
            });   
        </script>

        @yield('footer-scripts')

    </body>

</html>