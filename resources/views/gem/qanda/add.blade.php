<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')
                        <div class="col-lg-4">
                            <div class="card card-block">
                                <div class="profile-timeline-header">
                                    <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
                                    </a>
                                    <div class="profile-timeline-user-details"><a href="{{route('gem.qanda.dept','hrd')}}" class="bold">HRD</a>
                                        <br><span class="text-muted small">Today 09:24 PM - 23.02.2016</span>
                                    </div>
                                </div>
                                <div class="profile-timeline-content">

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card card-block">
                                <div class="profile-timeline-header">
                                    <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
                                    </a>
                                    <div class="profile-timeline-user-details"><a href="{{route('gem.qanda.dept','noc')}}" class="bold">NOC</a>
                                        <br><span class="text-muted small">Today 09:24 PM - 23.02.2016</span>
                                    </div>
                                </div>
                                <div class="profile-timeline-content">

                                </div>
                            </div>
                        </div>
                                                                    

@endsection 

@section('footer-scripts')

@endsection