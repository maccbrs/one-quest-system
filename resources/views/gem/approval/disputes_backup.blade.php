<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#one" role="tab" aria-expanded="true">Agent Disputes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab" href="#two" role="tab" aria-expanded="true">Non-Agent Disputes</a>
                </li>
<!--                 <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#two" role="tab" aria-expanded="false">Recently Approved / Rejected</a>
                </li> -->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="one" role="tabpanel" aria-expanded="true">
                  <div class="card">
                     <div class="card-header no-bg b-a-0">Agent Disputes</div>
                     <div class="card-block">
                        <div class="table-responsive">

                             <table class="table table-bordered customized-table">
                                 <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Action</th>
                                      <th>Agent</th>
                                      <th>Period</th> 
                                      <th>Date</th>
                                      <th>Issue</th>
                                      <th>TL</th>
                                       @if($type == 'hr') <th>RA</th> @endif
                                       
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if($disputes)
                                       @foreach($disputes as $key => $dispute)
                                          <tr>
                                            <td>{{$key + 1}}</td>
                                            <td>{!!view('gem.approval.dispute-update-modal',compact('dispute','type','help','users'))!!}</td>
                                            <td>{{$dispute->userObj->name}}</td>
                                            <td>{{$dispute->period->dispute_label}}</td>
                                            <td>{{$dispute->dispute_date}}</td>
                                            <td>{{$dispute->issueObj->issue_name}}</td>
                                            <td>{{$help->fetchStatus($dispute->tl_response)}}</td>
                                             @if($type == 'hr') <td>{{$help->fetchStatus($dispute->ra_response)}}</td> @endif
                               
                                          </tr>
                                       @endforeach
                                    @endif
                                 </tbody>
                            </table>      
                        </div>
                     </div>
                  </div>

                </div>
                <div class="tab-pane" id="two" role="tabpanel" aria-expanded="false">
                  <div class="card">
                     <div class="card-header no-bg b-a-0">Non-Agent Disputes</div>
                     <div class="card-block">
                        <div class="table-responsive">            
                          <table class="table table-bordered customized-table">
                                 <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Action</th>
                                      <th>Date Filed</th>
                                      <th>Emp</th>
                                      <th>Period</th> 
                                      <th>Date</th>
                                      <th>Issue</th>
                                      <th>Sup Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if($disputes2)
                                       @foreach($disputes2 as $key2 => $dispute2)
                                       <?php $dispute =  $dispute2; ?>
                                          <tr>
                                            <td>{{$key2 + 1}}</td>
                                            <td>{!!view('gem.approval.dispute2-update-modal',compact('dispute','help'))!!}</td>
                                            <td>{{$dispute2->created_at}}</td>
                                            <td>{{$dispute2->userObj->name}}</td>
                                            <td>{{$dispute2->period->dispute_label}}</td>
                                            <td>{{$dispute2->dispute_date}}</td>
                                            <td>{{$dispute2->issueObj->issue_name}}</td>
                                            <td>{{$help->fetchStatus($dispute2->sup_response)}}</td>
                                          </tr>
                                       @endforeach
                                    @endif
                                 </tbody>
                            </table>
                        </div>
                     </div>
                  </div>
                </div>
            </div>
        </div>
    </div>




@endsection 

@section('footer-scripts')

@endsection