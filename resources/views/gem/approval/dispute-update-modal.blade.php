<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".detail-modal-{{$dispute->id}}"><i class="material-icons" aria-hidden="true">folder_open </i></button>

    <div class="modal fade detail-modal-{{$dispute->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                    <div class="card">
                        <div class="card-header no-bg b-a-0">Update</div>
                        <div class="card-block">
                            <form method="post" action="{{route('gem.approval.dispute-update')}}" novalidate>
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$dispute->id}}">

                                 <p class="mb-details-modal auto-y">

                                        <span>Issue:</span> {{$dispute->issueObj->issue_name}}<br>
                                        <span>Employee Code:</span> {{$dispute->userObj->emp_code}}<br>
                                        <span>Agent:</span> {{$dispute->userObj->name}}<br>
                                        <span>Dispute Period:</span> {{$dispute->issueObj->dispute_label}}<br>
                                        <span>Dispute Date:</span> {{$dispute->dispute_date}}<br>
                                        <span>Agent's Note:</span> {{$dispute->notes}}<br>
                                        @if($type == 'ra' || $type == 'hr')
                                        <span>Team Leader's Response:</span> {{$help->fetchStatus($dispute->tl_response)}} <?=($dispute->tl_response > 1? ' by: ('.$help->fetchUser($dispute->tl_comments,$users).' ) date: '.$help->fetchDate($dispute->tl_comments):'') ?><br>
                                        <span>Team Leader's Comment:</span> {{$help->fetchComments($dispute->tl_comments)}}<br>
                                        @endif
                                        <span>Report Analyst's Response:</span> {{$help->fetchStatus($dispute->ra_response)}} <?=($dispute->ra_response > 1? ' by: ('.$help->fetchUser($dispute->ra_comments,$users).' ) date: '.$help->fetchDate($dispute->ra_comments):'') ?>}}<br>
                                        <span>Report Analyst's Comment:</span> {{$help->fetchComments($dispute->ra_comments)}}<br>
                                        @if($type == 'hr')
                                        <span>HR Response:</span> {{$help->fetchStatus($dispute->hr_response)}} <?=($dispute->hr_response > 1? ' by: ('.$help->fetchUser($dispute->hr_comments,$users).' ) date: '.$help->fetchDate($dispute->hr_comments):'') ?><br>
                                        <span>HR Comment:</span> {{$help->fetchComments($dispute->hr_comments)}}<br>
                                        @endif
                                        <span>Dispute Date:</span> {{$dispute->dispute_date}}<br>
                                        <span>Agent's Note:</span> {{$dispute->notes}}<br>

                                         

                                        </p>

<!--                                 <fieldset class="form-group">
                                    <label for="exampleSelect1">Employee Code</label>
                                    <p>{{$dispute->userObj->emp_code}}</p>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Agent</label>
                                    <p>{{$dispute->userObj->name}}</p>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Issue</label>
                                    <p>{{$dispute->issueObj->issue_name}}</p>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Dispute Period</label>
                                    <p>{{$dispute->period->dispute_label}}</p>
                                </fieldset>                                     

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Dispute Date</label>
                                    <p>{{$dispute->dispute_date}}</p>
                                </fieldset>   

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Agent Comment</label>
                                    <p class = "auto-y">{{$dispute->notes}}</p>
                                </fieldset>   

                                @if($type == 'ra' || $type == 'hr')
                                    <fieldset class="form-group">
                                        <label for="exampleSelect1">TL Response</label>
                                        <p>{{$help->fetchStatus($dispute->tl_response)}} <?=($dispute->tl_response > 1? ' by: ('.$help->fetchUser($dispute->tl_comments,$users).' ) date: '.$help->fetchDate($dispute->tl_comments):'') ?></p>
                                    </fieldset>  
                                    <fieldset class="form-group">
                                        <label for="exampleSelect1">TL Comment</label>
                                        <p class = "auto-y">{{$help->fetchComments($dispute->tl_comments)}}</p>
                                    </fieldset>                                                                                    
                                @endif                                            
                            
                                <fieldset class="form-group">
                                    <label for="exampleSelect1">TL Response</label>
                                    <p>{{$help->fetchStatus($dispute->tl_response)}} <?=($dispute->ra_response > 1? ' by: ('.$help->fetchUser($dispute->ra_comments,$users).' ) date: '.$help->fetchDate($dispute->ra_comments):'') ?></p>
                                </fieldset>  
                                <fieldset class="form-group">
                                    <label for="exampleSelect1">TL Comment</label>
                                    <p class = "auto-y">{{$help->fetchComments($dispute->tl_comments)}}</p>
                                </fieldset>          

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">RA Response</label>
                                    <p>{{$help->fetchStatus($dispute->ra_response)}} <?=($dispute->ra_response > 1? ' by: ('.$help->fetchUser($dispute->ra_comments,$users).' ) date: '.$help->fetchDate($dispute->ra_comments):'') ?></p>
                                </fieldset>  
                                <fieldset class="form-group">
                                    <label for="exampleSelect1">RA Comment</label>
                                    <p class = "auto-y">{{$help->fetchComments($dispute->ra_comments)}}</p>
                                </fieldset>                                                                            
                             
                               @if($type == 'hr')
                                    <fieldset class="form-group">
                                        <label for="exampleSelect1">HR Response</label>
                                        <p>{{$help->fetchStatus($dispute->hr_response)}} <?=($dispute->hr_response > 1? ' by: ('.$help->fetchUser($dispute->hr_comments,$users).' ) date: '.$help->fetchDate($dispute->hr_comments):'') ?></p>
                                    </fieldset>  
                                    <fieldset class="form-group">
                                        <label for="exampleSelect1">HR Comment</label>
                                        <p class = "auto-y">{{$help->fetchComments($dispute->hr_comments)}}</p>
                                    </fieldset>                                                                                    
                                @endif -->

                                <fieldset class="form-group">
                                    <label for="exampleTextarea">Add/Edit Note</label>
                                    <textarea class="form-control" rows="3" name="notes"></textarea>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="1">pending</option>
                                        <option value="2">approve</option>
                                        <option value="3">reject</option>
                                    </select>
                                </fieldset>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>  
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>