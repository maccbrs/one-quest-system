<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#one" role="tab" aria-expanded="true">Filed Disputes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#two" role="tab" aria-expanded="false">Recently Updated</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="one" role="tabpanel" aria-expanded="true">
                  <div class="card">
                     <div class="card-header no-bg b-a-0">Non-Agent Disputes</div>
                     
                     <div class="card-block">

                     <div id="dfilter" class="hide">
                        <div class="card-deck">
                             <div class="card">
                                <div class="card-header">Date Filed</div>
                                    <div class="card-block">
                                        <div id="external_filter_container_1" class="filter"></div>
                                            </div>
                                                </div>

                             <div class="card">
                                <div class="card-header">Date Updated</div>
                                    <div class="card-block">
                                        <div id="external_filter_container_2" class="filter"></div>
                                            </div>
                                                </div>

                             <div class="card">
                                <div class="card-header">Dispute Date</div>
                                    <div class="card-block">
                                        <div id="external_filter_container_3" class="filter"></div>
                                            </div>
                                                </div>
                        </div>
                    </div>

                        <div class="table-responsive">
                          <table cellpadding="0" cellspacing="0" border="0" class="display nowrap" id="table">
                                 <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Action</th>
                                      <th>Date Filed</th>
                                      <th>Date Updated</th>
                                      <th>Emp</th>
                                      <th>Period</th> 
                                      <th>Date</th>
                                      <th>Issue</th>
                                      <th>Sup Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if($disputes)
                                       @foreach($disputes as $key => $dispute)
                                          <tr>
                                            <td>{{$key + 1}}</td>
                                            <td>{!!view('gem.approval.dispute2-update-modal',compact('dispute','help'))!!}</td>
                                            <td>{{$dispute->created_at}}</td>
                                            <td>{{$dispute->updated_at}}</td>
                                            <td>{{$dispute->userObj->name}}</td>
                                            <td>{{$dispute->period->dispute_label}}</td>
                                            <td>{{$dispute->dispute_date}}</td>
                                            <td>{{$dispute->issueObj->issue_name}}</td>
                                            <td>{{$help->fetchStatus($dispute->sup_response)}}</td>
                                          </tr>
                                       @endforeach
                                    @endif
                                 </tbody>
                            </table>
                                             

                        </div>
                     </div>
                  </div>

                </div>
                <div class="tab-pane" id="two" role="tabpanel" aria-expanded="false">
                  <div class="card">
                     <div class="card-header no-bg b-a-0">Non-Agent Disputes</div>
                     <div class="card-block">

                        <div id="dfilter2" class="hide">
                        <div class="card-deck">
                             <div class="card">
                                <div class="card-header">Date Filed</div>
                                    <div class="card-block">
                                        <div id="external_filter_container_4" class="filter"></div>
                                            </div>
                                                </div>

                             <div class="card">
                                <div class="card-header">Date Updated</div>
                                    <div class="card-block">
                                        <div id="external_filter_container_5" class="filter"></div>
                                            </div>
                                                </div>

                             <div class="card">
                                <div class="card-header">Dispute Date</div>
                                    <div class="card-block">
                                        <div id="external_filter_container_6" class="filter"></div>
                                            </div>
                                                </div>
                                                </div>
                        </div>

                        <div class="table-responsive">

                             <table cellpadding="0" cellspacing="0" border="0" class="display nowrap" id="table2">
                                 <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Action</th>
                                      <th>Date Filed</th>
                                      <th>Date Updated</th>
                                      <th>Emp</th>
                                      <th>Period</th> 
                                      <th>Date</th>
                                      <th>Issue</th>
                                      <th>Sup Status</th>
                                      
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if($disputes2)
                                       @foreach($disputes2 as $key2 => $dispute2)
                                          <?php $dispute = $dispute2; ?>
                                          <tr>
                                            <td>{{$key2 + 1}}</td>
                                            <td>{!!view('gem.approval.dispute2-update-modal',compact('dispute','help'))!!}</td>
                                            <td>{{$dispute2->created_at}}</td>
                                            <td>{{$dispute2->updated_at}}</td>
                                            <td>{{$dispute2->userObj->name}}</td>
                                            <td>{{$dispute2->period->dispute_label}}</td>
                                            <td>{{$dispute2->dispute_date}}</td>
                                            <td>{{$dispute2->issueObj->issue_name}}</td>
                                            <td>{{$help->fetchStatus($dispute2->sup_response)}}</td>
                                            
                                          </tr>
                                       @endforeach
                                    @endif
                                 </tbody>
                            </table>
                                             

                        </div>
                     </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection 

@section('footer-scripts')
      <script src="{{$asset}}datatables/jquery-1.8.2.min.js"></script>
      <script src="{{$asset}}datatables/jquery-ui.js"></script>

      <script src="{{$asset}}datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
      <script src="{{$asset}}datatables/jquery.dataTables.yadcf.js"></script>

      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>

      <script>
$(document).ready(function() 
    {
        var currentDate = new Date()
        var day = ('0'+(currentDate.getDate())).slice(-2)
        var month = ('0'+(currentDate.getMonth()+1)).slice(-2)
        var year = currentDate.getFullYear()
        var d = year + "-" + month + "-" + day;

        var oTable;
        oTable = $('#table')

        .DataTable({
        lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
        pageLength: 25, 
        responsive: true,
        autoWidth: false,
        scrollX: true,
        scrollY: '600px',
        scrollCollapse: true,
        dom: 'Blfrtip',
        stateSave: true,
        order: [[4, 'desc']],
        buttons: [
            {
                text: 'Date Filter',
                action: function ( e, dt, node, config )
                {
                    var x = $('#dfilter');
                    x.slideToggle('slow');
                }
            },
            {
                extend: 'copyHtml5',
                title: 'For Approval (Agents Filed Disputes)' + ' ' + d + ' ',
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            {
                extend: 'excelHtml5',
                title: 'For Approval (Agents Filed Disputes)' + ' ' + d + ' ',
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            'colvis'
            ]
            });

        yadcf.init(oTable, [
            {
            column_number : 3,
            date_format: "yyyy-mm-dd",
            filter_container_id: 'external_filter_container_1',
            filter_type: 'range_date'
            }
            ,
            {
            column_number : 4,
            date_format: "yyyy-mm-dd",
            filter_container_id: 'external_filter_container_2',
            filter_type: 'range_date'
            }
            ,
            {
            column_number : 8,
            date_format: "yyyy-mm-dd",
            filter_container_id: 'external_filter_container_3',
            filter_type: 'range_date'
            }
                ]);
        

        $('a[href="#two"]').one('click',function(){    
        setTimeout(function(){    
        var pTable;
        pTable = $('#table2')

        .DataTable({
        lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
        pageLength: 25,
        scrollX: true, 
        scrollY: '600px',
        responsive: true,
        autoWidth: false,
        scrollCollapse: true,
        dom: 'Blfrtip',
        stateSave: true,
        order: [[4, 'desc']],
        buttons: [
            {
                text: 'Date Filter',
                action: function ( e, dt, node, config )
                {
                    var x = $('#dfilter2');
                    x.slideToggle('slow');
                }
            },
            {
                extend: 'copyHtml5',
                title: 'For Approval (Agents Filed Disputes)' + " (" + d + ")",
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            {
                extend: 'excelHtml5',
                title: 'For Approval (Agents Filed Disputes)' + " (" + d + ")",
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            'colvis'
            ]
            });

        yadcf.init(pTable, [
            {
            column_number : 3,
            date_format: "yyyy-mm-dd",
            filter_container_id: 'external_filter_container_4',
            filter_type: 'range_date'
            }
            ,
            {
            column_number : 4,
            date_format: "yyyy-mm-dd",
            filter_container_id: 'external_filter_container_5',
            filter_type: 'range_date'
            }
            ,
            {
            column_number : 8,
            date_format: "yyyy-mm-dd",
            filter_container_id: 'external_filter_container_6',
            filter_type: 'range_date'
            }
                ]);

    },0);
    });


            });

</script>
@endsection