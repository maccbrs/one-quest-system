<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".detail-posted-{{$dispute->id}}"><i class="material-icons" aria-hidden="true">visibility </i></button>

    <div class="modal fade detail-posted-{{$dispute->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" >
                    <h3>Mark this as Posted ? </h3>
                </div>

                <form method="post" action="{{route('gem.approval.dispute-posted')}}" novalidate>
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$dispute->id}}">
                    <input type="hidden" name="status" value="1">
                    <div class="modal-footer" >        
                        <button type="submit" class="btn btn-primary">Yes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </form>  
         
            </div>
        </div>
    </div>