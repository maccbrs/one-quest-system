<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".detail-modal-{{$dispute->id}}"><i class="material-icons" aria-hidden="true">folder_open </i></button>

    <div class="modal fade detail-modal-{{$dispute->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                    <div class="card">
                        <div class="card-header no-bg b-a-0">Update</div>
                        <div class="card-block">
                            <form method="post" action="{{route('gem.approval.dispute-update2')}}" novalidate>
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$dispute->id}}">
                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Agent</label>
                                    <p>{{$dispute->userObj->name}}</p>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Issue</label>
                                    <p>{{$dispute->issueObj->issue_name}}</p>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Dispute Period</label>
                                    <p>{{$dispute->period->dispute_label}}</p>
                                </fieldset>                                     

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Dispute Date</label>
                                    <p>{{$dispute->dispute_date}}</p>
                                </fieldset>   

                                <fieldset class="form-group">
                                    <label for="exampleTextarea">Employee Notes</label>
                                    <p class = "auto-y">{{$dispute->notes}}</p>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleTextarea">Note</label>
                                    <textarea class="form-control" rows="3" name="notes"></textarea>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="1">pending</option>
                                        <option value="2">approve</option>
                                        <option value="3">reject</option>
                                    </select>
                                </fieldset>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>  
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>