<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')
<style>
.shadow{
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}

/*div.shadow:hover {
    -moz-box-shadow: 0 0 5px rgba(0,0,0,0.5);
    -webkit-box-shadow: 0 0 5px rgba(0,0,0,0.5);
    box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }*/
</style>
<div class="row">
<div class="card-deck">

    <!-- <div class="col-lg-4"> -->
        <div class="card shadow card shadow profile-bio">
<a class="background"></a>
                <a href="javascript:;" class="avatar">
                    <img src="{{Request::instance()->query('UserImageSm')}}" alt=""> 
                </a>
            <div class="card-block">
                 <h7 class="card-title"><a href="#" class="bold">USER DETAILS</a></h7>
                    <br><span class="text-muted small"></span>
                    <div class="card-footer">
                    <p class="card-text">
                    <strong>
                    <table  class="table table-bordered" style="" >
                    <tbody>
                    <tr>
                    <td width="">Email </td>
                    <td width=""><span>{{Auth::user()->email }}</td></tr>
                    <tr>
                    <td>Emp No.</td><td>{{Auth::user()->emp_code }}</td></tr>
                    <?php $ApproverFetcher = new AppHelper; $larr_myarray = array();?>
                    <td>Dept/Campaign</td><td></td></tr>
                    <td>Dispute Approver 1</td><td><?php
                            if (isset($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)) 
                                echo $ApproverFetcher->fetchuser($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)->name; else echo "unset"; ?></td></tr>
                    <td>Dispute Approver 2</td><td><?php
                            if (isset($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2))
                                echo $ApproverFetcher->fetchuser($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level3)->name; else echo "unset";?></td></tr>
                    
                    </tbody>
                    </table>
                    </strong>
                    </p>
                    </p>
                    </div>
            </div>
        </div>

<!--         <div class="card shadow profile-bio col-md-4">
            <a class="background"></a>
                <a href="javascript:;" class="avatar">
                    <img src="{{Request::instance()->query('UserImageSm')}}" alt=""> 
                </a>
            <div class="card-block">
                 <h7 class="card-title"><a href="#" class="bold">USER DETAILS</a></h7>
                    <br><span class="text-muted small"></span>
                    <div class="card-footer">
                    <p class="card-text">
                    <strong>
                    <table  class="table table-bordered" style="" >
                    <tbody>
                    <tr>
                    <td width="">Email </td>
                    <td width=""><span>{{Auth::user()->email }}</td></tr>
                    <tr>
                    <td>Emp No.</td><td>{{Auth::user()->emp_code }}</td></tr>
                    <?php $ApproverFetcher = new AppHelper; $larr_myarray = array();?>
                    <td>Dept/Campaign</td><td></td></tr>
                    <td>Dispute Approver 1</td><td><?php
                            if (isset($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)) 
                                echo $ApproverFetcher->fetchuser($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)->name; else echo "unset"; ?></td></tr>
                    <td>Dispute Approver 2</td><td><?php
                            if (isset($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2))
                                echo $ApproverFetcher->fetchuser($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)->name; else echo "unset";?></td></tr>
                    
                    </tbody>
                    </table>
                    </strong>
                    </p>
                    </div>
            </div>
        </div> -->

<!-- 			<div class="card profile-bio col-md-4" >
				<a class="background"></a>
				<a href="javascript:;" class="avatar">
					
					<img src="{{Request::instance()->query('UserImageSm')}}" alt=""> 
				</a>

				<div class="user-details col-md-12" style="padding:10px;height:485px;">
					
				
					
					<table  class="table table-bordered" style="" >
					<tbody>
					<tr>
					<td width="">Email </td>
					<td width=""><span>{{Auth::user()->email }}</td></tr>
					<tr>
					<td>Emp No.</td><td>{{Auth::user()->emp_code }}</td></tr>
					
					
					<?php $ApproverFetcher = new AppHelper; $larr_myarray = array();?>
					
					
					<td>Dept/Campaign</td><td></td></tr>
					<td>Dispute Approver 1</td><td><?php
							if (isset($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)) 
								echo $ApproverFetcher->fetchuser($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)->name; else echo "unset"; ?></td></tr>
					<td>Dispute Approver 2</td><td><?php
							if (isset($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2))
								echo $ApproverFetcher->fetchuser($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)->name; else echo "unset";?></td></tr>
					
					</tbody>
                </table>


				</div>

				<div class="user-stats">
					<ul>
						<li>
							<a href="javascript:;"><span class="small text-uppercase block text-muted"><strong>Posts</strong></span>
							<h5 class="m-b-0"><strong></strong></h5>
							</a>
						</li>

						<li><a href="javascript:;">
							<span class="small text-uppercase block text-muted"><strong>Following</strong></span>
							<h5 class="m-b-0"><strong></strong></h5>
							</a>
						</li>

						<li>
							<a href="javascript:;"><span class="small text-uppercase block text-muted"><strong>Followers</strong></span>
							<h5 class="m-b-0"><strong></strong></h5></a>
						</li>
					</ul>
				</div>
			</div>
 -->


<!--         <div class="card shadow">
            <div class="card-block">
                 <h7 class="card-title"><a href="{{route('gem.announcements.lists',['source' => 'hrd','status' => 'all'])}}" class="bold">HRD - announcements</a></h7>
                    <br><span class="text-muted small">Updated {{Carbon\Carbon::parse($created_hrd->created_at)->diffForHumans()}} <br>{{Carbon\Carbon::parse($created_hrd->created_at)->format('F d, Y')}}</span>
                    <div class="card-footer">
                    <p class="card-text">
                    <small><strong>
                      <a href="{{route('gem.announcements.lists',['source' => 'hrd','status' => 'unread'])}}" class="m-r-xs"><span>Unread ({{$help->print_status($statuses,'hrd','unread')}}) </span></a><br><a href="{{route('gem.announcements.lists',['source' => 'hrd','status' => 'read'])}}" class="m-r-xs"><span>Opened ({{$help->print_status($statuses,'hrd','read')}})</span></a><br><a href="{{route('gem.announcements.lists',['source' => 'hrd','status' => 'all'])}}" class="m-r-xs"><span>All ({{$help->print_status($statuses,'hrd','read')+$help->print_status($statuses,'hrd','unread')}})</span></a>
                    </strong></small>
                    </p>
                    </div>
            </div>
        </div> -->


			<div class="card shadow" style="background-color: #FFFFED; width: 60%;" >
			  <div class="card-header">
				<button type="button" id="close" class="close" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h2>Latest Announcement</h2></div>
			  <div class="card-block" style="overflow:auto; height:600px;">
				<h4 class="card-title"><u>{{$latest_announcement->title}}</u></h4>
				<p class="card-text"><small class="lead text-uppercase">From: {{$latest_announcement->source}} DEPARTMENT</small></p>
				<p class="card-text"><h6 class="lead text-justify" style="line-height: 200%;">{!! ($latest_announcement->content) !!}</h6></p>
			  </div>
			</div>

</div>
</div>
			
<div class="row">
<div class="card-deck">

    <!-- <div class="col-lg-4"> -->
        <div class="card shadow">
            <div class="card-block">
                 <h7 class="card-title"><a href="{{route('gem.announcements.lists',['source' => 'hrd','status' => 'all'])}}" class="bold">HRD - announcements</a></h7>
                    <br><span class="text-muted small">Updated {{Carbon\Carbon::parse($created_hrd->created_at)->diffForHumans()}} <br>{{Carbon\Carbon::parse($created_hrd->created_at)->format('F d, Y')}}</span>
                    <div class="card-footer">
                    <p class="card-text">
                    <small><strong>
                      <a href="{{route('gem.announcements.lists',['source' => 'hrd','status' => 'unread'])}}" class="m-r-xs"><span>Unread ({{$help->print_status($statuses,'hrd','unread')}}) </span></a><br><a href="{{route('gem.announcements.lists',['source' => 'hrd','status' => 'read'])}}" class="m-r-xs"><span>Opened ({{$help->print_status($statuses,'hrd','read')}})</span></a><br><a href="{{route('gem.announcements.lists',['source' => 'hrd','status' => 'all'])}}" class="m-r-xs"><span>All ({{$help->print_status($statuses,'hrd','read')+$help->print_status($statuses,'hrd','unread')}})</span></a>
                    </strong></small>
                    </p>
                    </div>
            </div>
        </div>
    <!-- </div> -->


    <!-- <div class="col-lg-4"> -->
        <div class="card shadow">
            <div class="card-block">
                 <h7 class="card-title"><a href="{{route('gem.announcements.lists',['source' => 'noc','status' => 'all'])}}" class="bold">NOC - announcements</a></h7>
                    <br><span class="text-muted small">Updated {{Carbon\Carbon::parse($created_noc->created_at)->diffForHumans()}} <br>{{Carbon\Carbon::parse($created_noc->created_at)->format('F d, Y')}}</span>
                    <div class="card-footer">
                    <p class="card-text">
                    <small><strong>
                      <a href="{{route('gem.announcements.lists',['source' => 'noc','status' => 'unread'])}}" class="m-r-xs"><span>Unread ({{$help->print_status($statuses,'noc','unread')}}) </span></a><br><a href="{{route('gem.announcements.lists',['source' => 'noc','status' => 'read'])}}" class="m-r-xs"><span>Opened ({{$help->print_status($statuses,'noc','read')}})</span></a><br><a href="{{route('gem.announcements.lists',['source' => 'noc','status' => 'all'])}}" class="m-r-xs"><span>All ({{$help->print_status($statuses,'noc','read')+$help->print_status($statuses,'noc','unread')}})</span></a>
                    </strong></small>
                    </p>
                    </div>
            </div>
        </div>
   <!--  </div>  -->


    <!-- <div class="col-lg-4"> -->
        <div class="card shadow">
            <div class="card-block">
                 <h7 class="card-title"><a href="{{route('gem.announcements.lists',['source' => 'qa','status' => 'all'])}}" class="bold">QA - announcements</a></h7>
                    <br><span class="text-muted small">Updated {{Carbon\Carbon::parse($created_qa->created_at)->diffForHumans()}} <br>{{Carbon\Carbon::parse($created_qa->created_at)->format('F d, Y')}}</span>
                    <div class="card-footer">
                    <p class="card-text">
                    <small><strong>
                      <a href="{{route('gem.announcements.lists',['source' => 'qa','status' => 'unread'])}}" class="m-r-xs"><span>Unread ({{$help->print_status($statuses,'qa','unread')}}) </span></a><br><a href="{{route('gem.announcements.lists',['source' => 'qa','status' => 'read'])}}" class="m-r-xs"><span>Opened ({{$help->print_status($statuses,'qa','read')}})</span></a><br><a href="{{route('gem.announcements.lists',['source' => 'qa','status' => 'all'])}}" class="m-r-xs"><span>All ({{$help->print_status($statuses,'omni','read')+$help->print_status($statuses,'omni','unread')}})</span></a>
                    </strong></small>
                    </p>
                    </div>
            </div>
        </div>
    <!-- </div> --> 

    <!-- <div class="col-lg-4"> -->
        <div class="card shadow">
            <div class="card-block">
                 <h7 class="card-title"><a href="{{route('gem.messages.lists',['source' => 'messages','status' => 'all'])}}" class="bold">Messages</a></h7>
                    <br>
                    @if($type != "hr")
                    <span class="text-muted small">Recieved {{!empty($created_message_user->created_at) ? (Carbon\Carbon::parse($created_message_user->created_at)->diffForHumans()) : "0 message(s)"}}<br>{{!empty($created_message_user->created_at) ? Carbon\Carbon::parse($created_message_user->created_at)->format('F d, Y')  : "N/A"}}</span>
                    @endif
                    @if($type == "hr")
                    <span class="text-muted small">Recieved {{!empty($created_messagehr->created_at) ? (Carbon\Carbon::parse($created_messagehr->created_at)->diffForHumans()) : " 0 message(s)"}}<br>{{!empty($created_messagehr->created_at) ? Carbon\Carbon::parse($created_messagehr->created_at)->format('F d, Y')  : "N/A"}}</span>
                    @endif
                    <div class="card-footer">
                    <p class="card-text">
                    <small><strong>
                      <a href="{{route('gem.messages.lists',['source' => 'messages','status' => 'unread'])}}" class="m-r-xs"><span>Unread ({{$help->print_status_message($statuses_message,'messages','unread')}}) </span></a><br><a href="{{route('gem.messages.lists',['source' => 'messages','status' => 'read'])}}" class="m-r-xs"><span>Opened ({{$help->print_status_message($statuses_message,'messages','read')}})</span></a><br><a href="{{route('gem.messages.lists',['source' => 'messages','status' => 'all'])}}" class="m-r-xs"><span>All ({{$help->print_status_message($statuses_message,'messages','unread')+$help->print_status_message($statuses_message,'messages','read')}})</span></a>
                    </strong></small>
                    </p>
                    </div>
            </div>
        </div>
    <!-- </div> --> 

    <!-- <div class="col-lg-4"> -->
        <div class="card shadow">
            <div class="card-block">
                 <h7 class="card-title"><b>Dispute Approval</b></h7>
                    <br><span class="text-muted small">Last Recieved {{!empty($disputes2->created_at) ? (Carbon\Carbon::parse($disputes2->created_at)->diffForHumans()) : "N/A"}}
                    <br>{{!empty($disputes2->created_at) ? Carbon\Carbon::parse($disputes2->created_at)->format('F d, Y')  : "N/A"}}</span>
                    <div class="card-footer">
                    <p class="card-text">
                    <small><strong>
                    <a href="{{route('gem.approval.disputes',['source' => 'messages','status' => 'unread'])}}" class="m-r-xs">
                    <span class="blink_me">Agents - Waiting ({{$disputes}})</span></a><br>
                    @if($type != "hr" && $type != "tl")
                    <a href="{{route('gem.approval.disputes2',['source' => 'messages','status' => 'unread'])}}" class="m-r-xs" id="user">
                    <span class="blink_me2">Non-Agents - Waiting ({{$disputesuser}})</span></a>
                    @endif
                    @if($type == "hr")
                    <a href="{{route('opal.approval.nonagent',['source' => 'messages','status' => 'unread'])}}" class="m-r-xs" id="user">
                    <span class="blink_me3">Non-Agents - Waiting ({{$disputesnonagent}})</span></a>
                    @endif
                    </strong></small>
                    </p>
                    </div>
            </div>
        </div>
    <!-- </div> --> 
                        
</div>
</div>
 


<script>
window.onload = function(){
    document.getElementById('close').onclick = function(){
        this.parentNode.parentNode.parentNode
        .removeChild(this.parentNode.parentNode);
        return false;
    };
};
</script>

<script>
function blinker() {
var x = {{$disputes}};
var y = {{$disputesuser}};
var z = {{$disputesnonagent}};

    if(x != '0')
    {
        $('.blink_me').fadeOut(500).css('color', 'red', 'font-weight', 'bold');
        $('.blink_me').fadeIn(500).css('color', 'red', 'font-weight', 'bold');
    }

    if(y != '0')
    {
        $('.blink_me2').fadeOut(500).css('color', 'red', 'font-weight', 'bold');
        $('.blink_me2').fadeIn(500).css('color', 'red', 'font-weight', 'bold');
    }

    if(z != '0')
    {
        $('.blink_me3').fadeOut(500).css('color', 'red', 'font-weight', 'bold');
        $('.blink_me3').fadeIn(500).css('color', 'red', 'font-weight', 'bold');
    }
}

setInterval(blinker, 1500);
</script>
@endsection 

@section('footer-scripts')

@endsection

