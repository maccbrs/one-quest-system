<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

    <div class="row">

        <div class="col-sm-12">
            <div class="card card-block">
                <div class="profile-timeline-header">
                    <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
                    </a>
                    <div class="profile-timeline-user-details"><h4>{{$item->title}}</h4>
                        <br><span class="text-muted small">{{$item->created_at}}</span>
                    </div>
                </div>
                <div class="profile-timeline-content">
                    <p>{!! ($item->content) !!}</p>
                </div>
            </div>
        </div>

    </div>

@endsection 

@section('footer-scripts')

@endsection