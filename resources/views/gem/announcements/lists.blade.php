<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

                    @foreach($announcements as $k => $v)

                    <div class="row">
  <div class="col-sm-12">
    <div class="card card-block">
      <div class="profile-timeline-header">
        <div class="profile-timeline-user-details"><h3>{{$v->title}}</h3>
        <p class="card-text">{{Carbon\Carbon::parse($v->created_at)->diffForHumans()}}</p>
        <a href="{{route('gem.announcements.list',$v->id)}}" class="btn btn-primary">Open</a>
      </div>
    </div>
  </div>


                       
                    @endforeach


                     

@endsection 

@section('footer-scripts')

@endsection