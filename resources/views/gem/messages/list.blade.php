<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

<div class="card">
    <div class="card-header no-bg b-a-0">Talk with the Magellan Colleagues</div>
      <div class="card-block">
        <div class="table-responsive">
          <div class="content-view">
             <div class="fill-container layout-xs b-b">
              <div class="layout-column-xs overflow-hidden">
                <div class="row m-x-0 fill-container">

                  <div class="col-lg-12 p-a-0 messages-list bg-white b-r flexbox-xs layout-column-xs full-height">
                    <div class="flexbox-xs layout-column-xs message-view">

                      <div class="message-body flex-xs scroll-y">
                            
                       

                            <div class="col-lg-8" >
                                <h4 class="lead m-t-0">

                                <img class="avatar  img-rounded" style="width:5%;" src="{{$asset}}milestone/images/{{$user_involved->user_id}}.jpg">
                                {{ucfirst($user_involved->name)}} 

                              </h4>
                            </div>
                        
                            <div class="col-lg-4" >
                                <a data-toggle="modal" data-target="#create_message" class="m-l-1 pull-right btn btn-outline-info pull-right" tooltip-placement="bottom" uib-tooltip="Reply to all recipients">
                                    <i class="material-icons">create</i>New Message
                                </a>
                            </div>  
                            
                        <div class="col-lg-12"><hr></div>

                        <ul class="message-list nav nav-tabs" >
                        
                          @foreach($messages_list as $m)
                           <?php $message = 'message'.$m->id ?>
                              @if($userId == $m->user)
                                <?php $pull = 'pull-left from_message'?>
                              @else
                                <?php $pull = 'pull-right to_message'?>
                              @endif
                                <div class="col-lg-9 {{$pull}}">
                                  <h5>{!! nl2br($m->content->content) !!}</h5>
                                  <div class="date">sent by {{$users_list[$m->user]}} at {{$m->created_at}} </div>
                                </div>
                            @endforeach
                     
                        </ul>
                      </div>
                     </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>    
  </div>

  <?php if(!empty($user_involved)){ ?>

  <div class="modal fade" id="create_message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <form id="validate" class="form-horizontal" role="form" method="POST" action="{{route('amber.team.new_message')}}">
            
            <input type="hidden" value ="1" name="type" > 
            <input type="hidden" value ="1" name="status" > 
            <input type="hidden" value ="agents" name="page" >
            <input type="hidden" value ="{{$userId}}" name="user" >

            {{ csrf_field() }}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Send a Message</h4>
            </div>

            <div class="modal-body">

              <fieldset class="form-group">
                <label for="exampleSelect1">To</label>
                  <select class="form-control" id="exampleSelect1" name = "to">
                     <option disabled>Select Recipient</option>
                      <option value ='{{$user_involved->id}}'>{{ucfirst($user_involved->name)}}</option>
                  </select>
              </fieldset>
              
              <input type="hidden" class="form-control" placeholder="Enter Subject" pattern=".{3,40}" name="subject" > 
          
              <fieldset class="form-group">
                <label for="exampleInputEmail1">Message</label>
                <textarea class="form-control" rows="7" name="content" pattern=".{3,100}" ></textarea>
              </fieldset>

            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Send</button>
            </div>
          </form>
        </div>
      </div>
    </div>

 <?php } ?>

@endsection 

@section('footer-scripts')

@endsection