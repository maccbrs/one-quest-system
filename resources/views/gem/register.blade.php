<?php $asset = URL::asset('/'); ?> 
@extends('gem.master')

@section('title', 'login')

@section('content')


      <div class="row">
         <div class="col-lg-12">
            <div class="card">
               <div class="card-header no-bg b-a-0">Default Bootstrap elements</div>
               <div class="card-block">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

	                     <fieldset class="form-group">
	                     	<label for="exampleInputEmail1">Name</label>
	                     	<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
	                        @if ($errors->has('name'))
	                                <small class="text-muted">{{ $errors->first('name') }}</small>
	                        @endif                     	
	                     </fieldset>
						
						  <fieldset class="form-group">
	                     	<label for="emp_code">Employee Code</label>
	                     	<input id="emp_code" type="text" class="form-control" name="emp_code" value="{{ old('emp_code') }}">
	                        @if ($errors->has('name'))
	                                <small class="text-muted">{{ $errors->first('emp_code') }}</small>
	                        @endif                     	
	                     </fieldset> 
						


	                     <fieldset class="form-group">
	                     	<label for="">Email</label>
	                     	<input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
	                        @if ($errors->has('email'))
	                                <small class="text-muted">{{ $errors->first('email') }}</small>
	                        @endif                     	
	                     </fieldset>

	                     <fieldset class="form-group">
	                     	<label for="">Password</label>
	                     	<input id="password" type="text" class="form-control" name="password" value="{{ old('password') }}">
	                        @if ($errors->has('password'))
	                                <small class="text-muted">{{ $errors->first('password') }}</small>
	                        @endif                     	
	                     </fieldset>


	                     <fieldset class="form-group">
	                     	<label for="">Confirm Password</label>
	                     	<input id="password_confirmation" type="text" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}">
	                        @if ($errors->has('password_confirmation'))
	                                <small class="text-muted">{{ $errors->first('password_confirmation') }}</small>
	                        @endif                     	
	                     </fieldset>

                        <fieldset class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                        </fieldset>	                     

                    </form>
               </div>
            </div>
         </div>
      </div>


@endsection 

@section('footer-scripts')

@endsection