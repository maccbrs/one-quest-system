<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

   <div class="card">
      <div class="card-header no-bg b-a-0">

         <h5>Did Lists {!!view('gem.did.validate-modal',compact('id'))!!}</h5>
      </div>
      <div class="card-block">
         <div class="table-responsive">

              <table class="table table-bordered customized-table">
                  <thead>
                     <tr>
                        <th>DID</th>
                        <th>Campaign</th>
                        <th>Test Call Time</th>
                        <th>Status</th> 
                        <th>Notes</th>
                        <th>#</th>
                     </tr>
                  </thead>
                  <tbody>

                     @foreach($items as $item) 
                        <tr>
                           <td>
                              <label class="custom-control custom-checkbox mbcheck">
                                 <input type="checkbox" class="custom-control-input" data-mbcheck="{{$item->id}}" name="{{$item->id}}" value="{{$item->id}}" {{($item->checked?'checked':'')}} > 
                                 <span class="custom-control-indicator"></span> 
                                 <span class="custom-control-description"><a href="sip:{{$item->obj_did->phone}}" target="_blank">{{$item->obj_did->phone}}</a></span>
                              </label>
                           </td>
                           <td>{{$item->obj_did->campaign}}</td>
                           <td>{{$item->date}}</td>
                           <td>{{$item->status}}</td>
                           <td>{{$item->notes}}</td>
                           <td>
                              {!!view('gem.did.notes-modal',['id'=>$item->id])!!}
                           </td>
                        </tr>
                     @endforeach

                  </tbody>
             </table>
             

    
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

<script>
   var base = "{{URL::to('/')}}";
 !function(a) { 
     a.mb_lib = {
         mb_ajax: function(b) {
             var c = {
                 data: "null",
                 url: "null",
                 type: "post",
                 attachto: "null",
                 noty:"null",
                 msg:"success",
                 overlay:"null"
             };
             if (b) a.extend(c, b);

             if(c['overlay'] != "null"){
               a('.overlay').css('display','block');
             }

             a.ajax({
                 url: c["url"],
                 data: c["data"],
                 type: c["type"],
                 success: function(b) {
                  console.log(b);
                   a(c["attachto"]).html(b);

                   if(c["noty"] != "null"){
                     noty({
                         theme: 'app-noty',
                         text: c["msg"],
                         type: 'success',
                         timeout: 3000,
                         layout: 'top',
                         closeWith: ['button', 'click'],
                         animation: {
                             open: 'animated fadeInDown',
                             close: 'animated fadeOutUp'
                         }
                     });
                   }
                   if(c['overlay'] != "null"){
                     a('.overlay').css('display','none');
                   }                            
                 },
                 error: function(a) {
                   if(c['overlay'] != "null"){
                     a('.overlay').css('display','none');
                   }  
                 }
             });
         },
         mbtest: function(a){
           console.log(a);
         }
     };  

      $(".mbcheck input").on("click", function() {
          $this = $(this);
          var a = {
              data: {'id':$this.data('mbcheck'),'_token':'{{csrf_token()}}'},
              url: base + "/gem/dids/mbcheckupdate",
              attachto: ".jy-form"
          };
          $.mb_lib.mb_ajax(a);
      });  

 }(jQuery);








</script>

@endsection