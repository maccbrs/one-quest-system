<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

   <div class="card">
      <div class="card-header no-bg b-a-0">

         <h5>Test Calls {!!view('gem.did.addtestcalls-modal')!!}</h5>
      </div>
      <div class="card-block">
         <div class="table-responsive">

              <table class="table table-bordered customized-table">
                  <thead>
                     <tr>
                        <th>Date</th> 
                        <th>Count</th>
                        <th>Passed</th>
                        <th>Failed</th>
                        <th>#</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($items as $item)
                        <tr>
                           <td>{{$item->created_at}}</td>
                           <td>{{$item->total}}</td>
                           <td>{{$item->passed}}</td>
                           <td>{{$item->failed}}</td>
                           <td><a class="btn btn-primary" href="{{route('gem.did.testcall',$item->id)}}">view</a></td>
                        </tr>
                     @endforeach
                  </tbody> 
             </table>
             

    
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

<script>






</script>

@endsection