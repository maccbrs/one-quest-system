<button class="btn btn-primary btn-sm pull-right"  data-toggle="modal" data-target=".add-did">
  <i class="material-icons " aria-hidden="true">record_voice_over</i>
</button> 
<div class="modal fade add-did" id="editmodal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">


        <div class="modal-body">
          <p>Conduct new test call?</p>
          <form class="form-horizontal" role="form" method="POST" action="{{route('gem.did.createtc')}}">
            {{ csrf_field() }}
          <div class = "row">
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
          </div>

        </form>
      </div>
  </div>
</div> 