<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

   <div class="card">
      <div class="card-header no-bg b-a-0">

         <h5>DIDs List {!!view('gem.did.add-modal')!!}</h5>
      </div>
      <div class="card-block">
         <div class="table-responsive">

              <table class="table table-bordered customized-table">
                  <thead>
                     <tr>
                        <th>Number</th> 
                        <th>Campaign</th>
                        <th>Last test call</th>
                        <th>Last Status</th>
                        <th>#</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if($items->count())
                        @foreach($items as $item)
                           <tr>
                              <td>{{$item->phone}}</td>
                              <td>{{$item->campaign}}</td>
                              <td>x</td>
                              <td>x</td>
                              <td>view</td>
                           </tr>
                        @endforeach
                     @endif
                  </tbody>
             </table>
             

    
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

<script>






</script>

@endsection