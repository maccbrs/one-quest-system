<button class="btn btn-primary btn-sm pull-right"  data-toggle="modal" data-target=".notes{{$id}}">
  <i class="material-icons " aria-hidden="true">textsms </i>
</button> 
<div class="modal fade notes{{$id}}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <p>Notes</p>
        </div>

        <div class="modal-body">
          <form class="form-horizontal" role="form" method="POST" action="{{route('gem.did.notes',$id)}}">
            {{ csrf_field() }}
          
            <div class = "row">
              <fieldset class="form-group">
                  <textarea class="form-control" name="notes" rows="3"></textarea>
              </fieldset>
            </div>
            

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

        </form>
      </div>
  </div>
</div> 