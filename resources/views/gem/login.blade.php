<?php $asset = URL::asset('/'); ?> 
<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type: application/json');
?>

@extends('landing.master')



@section('content')

<!-- <form class="form-signin">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Username</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form> -->
<script type="text/javascript">
if (location.href.indexOf('reload')==-1)
{
   location.href=location.href+'?reload';
}
</script>


<style type="text/css">
input[type="text"], textarea {

  background-color : #FCF3CF !important; 

}

input[type="password"], textarea{

  background-color : #FCF3CF !important; 

}

div.card {
    box-shadow: 0 16px 32px 0 rgba(0, 0, 0, 0.5), 0 24px 80px 0 rgba(0, 0, 0, 0.19);
}
</style>

            <div class="col-md-12 col-sm-12 header-text" >
              
              <form id="validate" class="form-signin" role="form" method="POST" action="{{ url('/login') }}" >
                  {{ csrf_field() }}
                  <div class="card">
                  <div class="card-header">          
                <h4 style="color: #003399"><b>One Quest Site <span style="color: #659D32">(OQS)</span></b></h4>
              </div>
              <div class="card-block" style="padding-left:5%; padding-right: 5%;">
                <div id="login-form">
				 <?php /* <p>Employee #: <input type="text" name="username" value="{{ old('username') }}"></p>*/?>
                   @if ($errors->has('email'))
                   <br>
                  <div class="alert alert-danger">
                    <strong>Error!</strong> {{ $errors->first('email') }}
                  </div>
                  @endif
                  <p class="card-text"><b>Employee ID: </b><input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Employee ID" required autofocus></p>   
                  <p class="card-text"><b>Password: </b><input type="password" placeholder="Password"  class="form-control" name="password"></p>
                  <p style="text-align: center;"><button class="btn btn-primary" style="width: 100%;">Login</button></p>
                </div>
              </div>
              </div>
              <p style="font-size: 8px; margin-top: 30px; color: white;"><b>2018© || Otsuka (Philippines) Pharmaceutical Inc. || WELCOME TO OQS SYSTEM</b></p>
              </form>



            
			<!--
			<div class="col-md-12 col-sm-12" style="text-align:center;color:#ffffff" >
              <form id="validate" class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                  {{ csrf_field() }}            
                <h2 style="color:#ffffff;">Login By a Email</span></h1>
                <div id="login-form">
				  <p>Email: <input type="text" name="email" value="{{ old('email') }}"></p>
                  <?php /*<p>Email: <input type="email" name="email" value="{{ old('email') }}"></p> */?>
                  @if ($errors->has('email'))
                  <p class="form-control-label">{{ $errors->first('email') }}</p>
                  @endif   
                  <p>Password: <input type="password" name="password"></p>
                  <p class="buton"><button>Login</button></p>
                </div>
              </form>
            </div>
			-->
			

     

@endsection 

@section('footer-scripts') 


@endsection