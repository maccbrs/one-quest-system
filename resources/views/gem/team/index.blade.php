<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

  <div class="card">
     <div class="card-header no-bg b-a-0">Basic table</div>
      	<div class="card-block">
        	<div class="table-responsive">
         	<div class="content-view">
         		<div class="fill-container layout-xs b-b">
         			<div class="layout-column-xs overflow-hidden">
         				<div class="row m-x-0 fill-container">

         					<div class="col-lg-4 p-a-0 messages-list bg-white b-r flexbox-xs layout-column-xs full-height">
         						<div class="message-header">
         							<div class="message-toolbar">
         								<form class="form-inline">
         									<input class="form-control" placeholder="Search messages" type="text">
         								</form>
         							</div>
         						</div>

         						<div class="flex-xs scroll-y">
         							<ul class="message-list">
         								<li class="message-list-item">
         									<a href="javascript:;" class="b-b" data-toggle="message">
         										<img class="avatar avatar-sm img-circle pull-left m-r-1" alt="" src="images/face5.jpg">
         										<div class="message-list-item-header">
         											<div class="time">24 March 2015</div>
         											<span class="bold">Catherine Moreno</span>
         										</div><p class="overflow-hidden">Check out this weeks most popular website designs in the Milkyway! …</p>
         									</a>
         								</li>
         								<li class="message-list-item">
         									<a href="javascript:;" class="b-b" data-toggle="message">
         										<img class="avatar avatar-sm img-circle pull-left m-r-1" alt="" src="images/face4.jpg">
         										<div class="message-list-item-header">
         											<div class="time">27 March 2015</div>
         											<span class="bold">Denise Peterson</span>
         										</div><p class="overflow-hidden">eBook: The complete guide to creating Angularjs single page applications is here …</p>
         									</a>
         								</li> 
         								<li class="message-list-item">
         									<a href="javascript:;" class="b-b" data-toggle="message">
         										<img class="avatar avatar-sm img-circle pull-left m-r-1" alt="" src="images/face3.jpg">
         										<div class="message-list-item-header">
         											<div class="time">28 March 2015</div>
         											<span class="bold">Charles Wilson</span>
         										</div><p class="overflow-hidden">Cum sociis natoque penatibus et magnis dis parturient montes, ridiculus mus …</p>
         									</a>
         								</li> 
         							</ul>
         						</div>
         					</div>

         					<div class="col-lg-8 p-a-0 messages-list bg-white b-r flexbox-xs layout-column-xs full-height">
         						<div class="flexbox-xs layout-column-xs message-view">
			         				<div class="message-header">
			         					<div class="message-toolbar">
			         						<div class="pull-right">
			         							<a href="javascript:;" class="m-l-1" tooltip-placement="bottom" uib-tooltip="Reply to sender">
			         								<i class="material-icons">reply </i>
			         							</a>

			         							<a href="javascript:;" class="m-l-1" tooltip-placement="bottom" uib-tooltip="Reply to all recipients">
			         								<i class="material-icons">reply_all </i>
			         							</a>
			         							<a href="javascript:;" class="m-l-1" tooltip-placement="bottom" uib-tooltip="Forward message">
			         								<i class="material-icons">forward </i>
			         							</a>
			         							<a href="javascript:;" class="m-l-1" tooltip-placement="left" uib-tooltip="Flag message">
			         								<i class="material-icons">flag</i>
			         							</a>
			         						</div>
			         						<a href="javascript:;" class="hidden-lg-up" data-toggle="message">
			         							<i class="material-icons m-r-1">arrow_back</i>
			         						</a>
			         					</div>
			         				</div>

			         				<div class="message-body flex-xs scroll-y">
			         					<div class="p-a-1">
			         						<div class="pull-left m-r-1">
			         							<img class="avatar avatar-md img-rounded" alt="" src="images/face3.jpg">
			         						</div><div class="overflow-hidden">
			         						<div class="date">28 March 2015 14:43:00</div>
			         						<h4 class="lead m-t-0">Sparrow product support</h4>
			         						<div class="message-sender">
			         							<p><b>Charles Wilson </b>to Jeff &amp; Suzzane</p>
			         						</div>
			         					</div>
			         				</div>
				         				<div class="p-a-1"><p>Nulla vitae elit libero, a pharetra augue. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Cras mattis consectetur purus sit amet fermentum. Curabitur blandit tempus porttitor. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p><p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p><blockquote><i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i></blockquote><p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p><p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p><small>Obi-Wan Kenobi, Jedi Knight</small><p>Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>
				         				</div>
				         				<div class="m-a-1">
				         					<div class="bg-default rounded p-a-1">
				         						<a href="javascript:;" class="btn btn-primary btn-sm btn-icon pull-right">
				         							<i class="material-icons">archive </i>
				         							<span>Save files</span>
				         						</a>
				         						<p>
				         							<a href="javascript:;" class="btn btn-sm btn-secondary btn-icon">
				         								<i class="fa fa-file-audio-o"></i> 
				         								<span>Music_samples.mp3 2Mb</span>
				         							</a>
				         						</p>
				         						<p>
				         							<a href="javascript:;" class="btn btn-sm btn-secondary btn-icon">
				         								<i class="fa fa-file-video-o"></i> 
				         								<span>funny_cats.mp4 34Mb</span>
				         							</a>
				         						</p>
				         					</div>
				         				</div>
			         				</div>
			         			</div>
         					</div>
 						</div>
 					</div>
 				</div>
 			</div>
        </div>
    </div>    
  </div>
 


@endsection 

@section('footer-scripts')

@endsection