<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

   <div class="card">
      <div class="card-header no-bg b-a-0">
         <span class = "pull-right">{!!view('gem.garnet.add-supervisors-modal',compact('users'))!!}</span>
         <h5>Supervisors List</h5>
      </div>
      <div class="card-block">
         <div class="table-responsive">

              <table class="table table-bordered customized-table">
                  <thead>
                     <tr>
                        <th>First Name</th> 
                        <th>Middle Name</th>
                        <th>Last Name</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($supervisors as $ps)
                        <tr>
                           <td>{{$ps->first_name}}</td>
                           <td>{{$ps->middle_name}}</td>
                           <td>{{$ps->last_name}}</td>
                        </tr>
                     @endforeach
                  </tbody> 
             </table>
             
     
    
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

<script>



</script>

@endsection