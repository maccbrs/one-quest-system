<button type="button" class="btn btn-primary fa fa-edit pull-right" data-toggle="modal" data-target=".edit-agent-modal{{$agent->id}}"></button>
  <div class="modal fade bd-example-modal-lg edit-agent-modal{{$agent->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <form class="form-horizontal form-label-left" method="post" action="{{route('gem.garnet.edit_agent',$agent->id)}}" novalidate>

           {{ csrf_field() }}
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Agent</h4>
                </div>

                <div class="modal-body">
                    <fieldset class="form-group">
                        <label for="exampleSelect1">First Name</label>
                        <input type="text" class="form-control" name="first_name" value="{{$agent->first_name}}" >
                    </fieldset>                    
               
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Middle Name</label>
                        <input type="text" class="form-control" name="middle_name" value="{{$agent->middle_name}}" >
                    </fieldset>                    
               
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Last Name</label>
                        <input type="text" class="form-control" name="last_name"  value="{{$agent->last_name}}">
                    </fieldset>      

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Team Lead</label>
                          <select class="form-control" name="tl_id">
                            @foreach($teamleads as $tl => $tls)
                              <option value="{{$tls['id']}}" {{($tls['id'] == $agent->tl_id?'selected':'')}}  >{{$tls['first_name'] . " " . $tls['last_name']}}</option>
                            @endforeach
                        </select>
                    </fieldset>   

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Supervisor</label>
                          <select class="form-control" name="ps_id">
                            <option disabled selected>Select Program Supervisor</option>
                            @foreach($supervisors as $ps => $pls)
                              <option value="{{$pls['id']}}" {{($pls['id'] == $agent->ps_id?'selected':'')}}  >{{$pls['first_name'] . " " . $pls['last_name']}}</option>
                            @endforeach
                        </select>
                    </fieldset>   

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Gem User Accnt</label>
                          <select class="form-control" name="user_id">
                            <option disabled selected>Select account</option>
                            @foreach($users as $u)
                              <option value="{{$u->id}}" {{($u->id == $agent->user_id?'selected':'')}}  >{{$u->name}}</option>
                            @endforeach
                        </select>
                    </fieldset> 

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>

            </div>

        </form>
      </div>
  </div>