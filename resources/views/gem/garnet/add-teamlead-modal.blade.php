<button type="button" class="btn btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".add-teamlead-modal"></button>
  <div class="modal fade bd-example-modal-lg add-teamlead-modal" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <form class="form-horizontal form-label-left" method="post" action="{{route('gem.garnet.add_teamlead')}}" novalidate>

           {{ csrf_field() }}
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Team Leader</h4>
                </div>

                <div class="modal-body">
                    <fieldset class="form-group">
                        <label for="exampleSelect1">First Name</label>
                        <input type="text" class="form-control" name="first_name" >
                    </fieldset>                    
               
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Middle Name</label>
                        <input type="text" class="form-control" name="middle_name" >
                    </fieldset>                    
               
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Last Name</label>
                        <input type="text" class="form-control" name="last_name" >
                    </fieldset>      

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Gem User Accnt</label>
                          <select class="form-control" name="user_id">
                            <option disabled selected>Select account</option>
                            @foreach($users as $u)
                              <option value="{{$u->id}}"  >{{$u->name}}</option>
                            @endforeach
                        </select>
                    </fieldset>  

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Supervisor</label>
                          <select class="form-control" name="ps_id">
                            <option disabled selected>Select Program Supervisor</option>
                            @foreach($supervisors as $sup)
                              <option value="{{$sup->id}}" >{{$sup->first_name}} {{$sup->last_name}}</option>
                            @endforeach
                        </select>
                    </fieldset>                     

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>

            </div>

        </form>
      </div>
  </div>