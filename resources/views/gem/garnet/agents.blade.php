<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

   <div class="card">
      <div class="card-header no-bg b-a-0">
         <span class = "pull-right">{!!view('gem.garnet.add-agent-modal',compact('supervisors','teamleads','users'))!!}</span>
         <h5>Agents List</h5> 
      </div>
      <div class="card-block">
         <div class="table-responsive">

              <table class="table table-bordered customized-table">
                  <thead>
                     <tr>
                        <th>First Name</th> 
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Tl</th>
                        <th>Ps</th>
                        <th>Gem Accnt</th>
                        <th>#</th>
                     </tr>
                  </thead>
                  <tbody>

                     @foreach($agents as $agent)
                        <tr>
                           <td>{{$agent->first_name}} </td>
                           <td>{{$agent->middle_name}} </td>
                           <td>{{$agent->last_name}} </td>
                           <td>{{($agent->teamlead?$agent->teamlead->first_name:'')}}</td>
                           <td>{{($agent->supervisor?$agent->supervisor->first_name:'')}}</td>
                            <td>{{($agent->gem?$agent->gem->name:'')}}</td>
                            <td>{!!view('gem.garnet.edit-agent-modal',compact('supervisors','teamleads','users','agent'))!!}</td>
                        </tr>
                     @endforeach

                  </tbody> 
             </table>
             
     
         {!!$agents->links()!!}
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

<script>



</script>

@endsection