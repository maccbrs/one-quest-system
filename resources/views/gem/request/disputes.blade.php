<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'dashboard')

@section('header-scripts')
    <style type="text/css">
    .mb-details-modal{
        color: green;
    }
    .mb-details-modal span{
        color: #000;
        text-decoration: underline;
        font-weight: bold;
    }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#one" role="tab" aria-expanded="true">Filed Disputes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#two" role="tab" aria-expanded="false">Approved</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#three" role="tab" aria-expanded="false">Rejected</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="one" role="tabpanel" aria-expanded="true">
                    <div class="card">
                      <div class="card-block"  >
                          <div class="table-responsive" >
                            <button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".createNewDispute">Create New</button>
                                <table id="table" class="display nowrap">
                                    <thead>
                                        <tr>
                                            <th>Date Created</th>
                                            <th>Dispute Period</th>
                                            <th>Dispute Date</th>
                                            <th>Issue</th>
                                            <th>Supp status</th>
                                            <th>HR Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    @if($items->count())
                                        @foreach($items as $item)
                                            @if($item->hr_response != 2 && !$help->rejected($item->hr_response,$item->sup_response))
                                            <tr>
                                                <td>{{$item->created_at}}</td>
                                                <td>{{$dates[$item->dispute_period]}}</td>
                                                <td>{{$item->dispute_date}}</td>
                                                <td>{{$issues[$item->issue]}}</td>
                                                <td>{{$help->fetchStatus($item->sup_response)}}</td>
                                                <td>{{$help->fetchStatus($item->hr_response)}}</td>
                                                <td>
                                                   {!!view('gem.request.detail-modal',compact('gemissue','issues','item','help','dates'))!!}
                                                   {!!view('gem.request.delete-modal',compact('item','help','dates','issues'))!!}
                                                </td>
                                            </tr> 
                                            @endif                                       
                                        @endforeach 

                                    @endif

                                    </tbody>
                                </table> 
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab-pane" id="two" role="tabpanel" aria-expanded="false">
                    <div class="card">
                      <div class="card-block"  >
                          <div class="table-responsive" >
                            
                                <table id="table2" class="display nowrap">
                                    <thead>
                                        <tr>
                                            <th>Date Created</th>
                                            <th>Dispute Period</th>
                                            <th>Dispute Date</th>
                                            <th>Issue</th>
                                            <th>Supp status</th>
                                            <th>HR Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    @if($items->count())
                                        @foreach($items as $item)
                                            @if($item->hr_response == 2)
                                            <tr>
                                                <td>{{$item->created_at}}</td>
                                                <td>{{$dates[$item->dispute_period]}}</td>
                                                <td>{{$item->dispute_date}}</td>
                                                <td>{{$issues[$item->issue]}}</td>
                                                <td>{{$help->fetchStatus($item->sup_response)}}</td>
                                                <td>{{$help->fetchStatus($item->hr_response)}}</td>
                                                <td>
                                                   {!!view('gem.request.detail-modal',compact('item','help','dates','issues','gemissue'))!!}
                                                </td>
                                            </tr> 
                                            @endif                                       
                                        @endforeach 

                                    @endif
                                    </tbody>
                                </table>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab-pane" id="three" role="tabpanel" aria-expanded="false">
                    <div class="card">
                      <div class="card-block"  >
                          <div class="table-responsive" >
                            
                                <table id="table3" class="display nowrap">
                                    <thead>
                                        <tr>
                                            <th>Date Created</th>
                                            <th>Dispute Period</th>
                                            <th>Dispute Date</th>
                                            <th>Dispute Time</th>
                                            <th>Issue</th>
                                            <th>Supp status</th>
                                            <th>HR Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    @if($items->count())
                                        @foreach($items as $item)
                                            @if($help->rejected($item->hr_response,$item->sup_response))
                                            <tr>
                                                <td>{{$item->created_at}}</td>
                                                <td>{{$dates[$item->dispute_period]}}</td>
                                                <td>{{$item->dispute_date}}</td>
                                                <td>{{$item->dispute_time}}</td>
                                                <td>{{$issues[$item->issue]}}</td>
                                                <td>{{$help->fetchStatus($item->sup_response)}}</td>
                                                <td>{{$help->fetchStatus($item->hr_response)}}</td>
                                                <td>
                                                   {!!view('gem.request.detail-modal',compact('item','help','dates','issues','gemissue'))!!} 
                                                </td>
                                            </tr> 
                                            @endif                                       
                                        @endforeach 

                                    @endif
                                    </tbody>
                                </table>
                          </div>
                      </div>
                  </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade createNewDispute" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                    <div class="card">
                        <div class="card-header no-bg b-a-0">Create New Dispute</div>
                        <div class="card-block">
                            <form method="post" action="{{route('gem.request.dispute-create')}}" novalidate>

                                {{ csrf_field() }}

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Dispute Period</label>
                                    <select class="form-control" name="dispute_period">
                                        <option  disabled>Select Dispute Period</option>
                                        @if($dates)
                                            @foreach($dates as $k => $d)
                                                <option value="{{$k}}">{{$d}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label >Dispute Date</label>
                                    <input name = "dispute_date" class="form-control m-b-1 datepicker" data-provide="datepicker"  > 
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Issue</label>
                                    <select class="form-control" name="issue">
                                        <option  disabled>Select Issue</option>
                                        @if($issues)
                                            @foreach($gemissue as $k1 => $d1)
                                                <option value="{{$k1}}">{{$d1}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleTextarea">Notes</label>
                                    <textarea class="form-control" rows="3" name="notes"></textarea>
                                </fieldset> 

                                <button type="submit" class="btn btn-primary">Submit</button>

                            </form>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>


 
@endsection

@section('footer-scripts')
      <script src="{{$asset}}datatables/jquery-1.8.2.min.js"></script>
      <script src="{{$asset}}datatables/jquery-ui.js"></script>

      <script src="{{$asset}}datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
      <script src="{{$asset}}datatables/jquery.dataTables.yadcf.js"></script>

      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>

    <script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{{$asset}}milestone/vendor/clockpicker/dist/bootstrap-clockpicker.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

    <script type="text/javascript">

        $('.datepicker').datepicker({
             dateFormat: 'YYYY/MM/DD'
    
        });

        $('.timepicker').timepicker();

    $( ".daterangeinput" ).change(function() {
        daterangevalue = $( ".daterangeinput" ).val();
        $( ".disputelabelinput" ).val(daterangevalue);
    });



    </script>

    <script>
$(document).ready(function() 
    {
        var currentDate = new Date()
        var day = ('0'+(currentDate.getDate())).slice(-2)
        var month = ('0'+(currentDate.getMonth()+1)).slice(-2)
        var year = currentDate.getFullYear()
        var d = year + "-" + month + "-" + day;

        var oTable;
        oTable = $('#table')

        .DataTable({
        lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
        pageLength: 25, 
        responsive: true,
        autoWidth: false,
        scrollX: true,
        scrollY: '600px',
        scrollCollapse: true
        
            });
        

        $('a[href="#two"]').one('click',function(){    
        setTimeout(function(){    
        var pTable;
        pTable = $('#table2')

        .DataTable({
        lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
        pageLength: 25, 
        responsive: true,
        autoWidth: false,
        scrollX: true,
        scrollY: '600px',
        scrollCollapse: true
        
            });

        yadcf.init(pTable, [

                ]);

    },0);
    });

        $('a[href="#three"]').one('click',function(){    
        setTimeout(function(){    
        var qTable;
        qTable = $('#table3')

        .DataTable({
        lengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
        pageLength: 25, 
        responsive: true,
        autoWidth: false,
        scrollX: true,
        scrollY: '600px',
        scrollCollapse: true
        
            });

        yadcf.init(pTable, [

                ]);

    },0);
    });


            });

</script>
@endsection