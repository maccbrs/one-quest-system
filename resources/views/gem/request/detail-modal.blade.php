<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".detail-modal-{{$item->id}}" title="View Details"><i class="material-icons" aria-hidden="true">folder_open </i></button>

    <div class="modal fade detail-modal-{{$item->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form method="post" action="{{route('gem.request.dispute-update')}}">
                {{ csrf_field() }}   
                <input type="hidden" value = "{{$item->id}}"  name = "id" > 
                <input type="hidden" value = "{{$item->sup_response}}"  name = "sup_response" > 
                <input type="hidden" value = "{{$item->hr}}"  name = "hr" > 

                <div class="modal-body" >
                        <div class="card">
                            <div class="card-block">
                            <h5>Dispute Details</h5>
                                    <p class="mb-details-modal">
                                    <span>Date Created:</span> {{$item->created_at}} <br>
                                    <span>Dispute Period:</span> {{$dates[$item->dispute_period]}} <br>
                                    <span>Dispute Date:</span> {{$item->dispute_date}} <br>
                                    <span>Dispute Time:</span> {{$item->dispute_time}} <br>
                                    <span>Dispute Issue:</span> {{$issues[$item->issue]}} <br>
                                    <span>Notes:</span> {{$item->notes}} <br>
                                    <span>Supervisor Approval:</span> {{$help->fetchStatus($item->sup_response)}} <br>
                                    <span>Supervisor Notes:</span> {{!empty($help->fetchComments($item->sup_comments))?$help->fetchComments($item->sup_comments) : 'N/A'}} <br>
                                    <span>HR Approval:</span> {{$help->fetchStatus($item->hr_response)}} <br>
                                    <span>HR Notes:</span> {{!empty($help->fetchComments($item->hr_comments))?$help->fetchComments($item->hr_comments) : 'N/A'}} <br>
                                </p>
                                @if($item->sup_response == 1)
                                <br>
                                <h5>Edit Dispute</h5>
                                <fieldset class="form-group">
                                  <label for="exampleSelect1">Dispute Period</label>
                                  <select name="dispute_period" class="form-control" required>
                                  <option value="">Select Dispute Period</option>
                                  @foreach($dates as $k => $d)
                                  <option value="{{$k}}">{{$d}}</option>
                                  @endforeach  
                                  </select>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label >Dispute Date</label>
                                    <input name="dispute_date" class="form-control m-b-1 datepicker" data-provide="datepicker"  required>
                                </fieldset>

                                <fieldset class="form-group">
                                  <label for="exampleSelect1">Issue</label>
                                  <select name="issue_name" class="form-control" required>
                                  <option value="">Select Issue</option>
                                  @foreach($gemissue as $k1 => $d1)
                                  <option value="{{$k1}}">{{$d1}}</option>
                                  @endforeach
                                  </select>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Your Notes</label>
                                    <textarea class="form-control" name="notes" >{{$item->notes}}</textarea>
                                </fieldset>                                        
                                @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        @if($item->sup_response == 1)
                            <button type="submit" class="btn btn-primary">Save</button>
                        @endif
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
