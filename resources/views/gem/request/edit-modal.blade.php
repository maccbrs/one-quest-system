<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".edit-modal-{{$item->id}}" title="View Details"><i class="material-icons" aria-hidden="true">folder_open </i></button>

    <div class="modal fade createNewDispute" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                    <div class="card">
                        <div class="card-header no-bg b-a-0">Edit Dispute</div>
                        <div class="card-block">
                            <form method="post" action="{{route('gem.request.dispute-create')}}" novalidate>

                                {{ csrf_field() }}

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Dispute Period</label>
                                    <select class="form-control" name="dispute_period">
                                        <option  disabled>Select Dispute Period</option>
                                        @if($dates)
                                            @foreach($dates as $k => $d)
                                                <option value="{{$k}}">{{$d}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label >Dispute Date</label>
                                    <input name = "dispute_date" class="form-control m-b-1 datepicker" data-provide="datepicker"  > 
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Issue</label>
                                    <select class="form-control" name="issue">
                                        <option  disabled>Select Issue</option>
                                        @if($issues)
                                            @foreach($gemissue as $k1 => $d1)
                                                <option value="{{$k1}}">{{$d1}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleTextarea">Notes</label>
                                    <textarea class="form-control" rows="3" name="notes"></textarea>
                                </fieldset> 

                                <button type="submit" class="btn btn-primary">Submit</button>

                            </form>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>