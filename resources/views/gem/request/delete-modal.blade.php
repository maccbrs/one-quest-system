<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".delete-modal-{{$item->id}}" title="Delete"><i class="material-icons" aria-hidden="true">delete</i></button>

    <div class="modal fade delete-modal-{{$item->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form method="post" action="{{route('gem.request.dispute-delete')}}" novalidate>
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-body" >
                        <div class="card">
                            <input type = "hidden" name = "id" value = "{{$item->id}}" > 
                            <div class="card-header no-bg b-a-0">Dispute Detail</div>
                            <div class="card-block">

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Date Created</label>
                                    <p>{{$item->created_at}}</p>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Dispute Period</label>
                                    <p>{{$dates[$item->dispute_period]}}</p>
                                </fieldset>                                     

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Dispute Date</label>
                                    <p>{{$item->dispute_date}}</p>
                                </fieldset>   

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Dispute Time</label>
                                    <p>{{$item->dispute_time}}</p>
                                </fieldset>  

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Issue</label>
                                    <p>{{$issues[$item->issue]}}</p>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Notes</label>
                                    <p>{{$item->notes}}</p>
                                </fieldset>

                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-warning">Delete</button>
                    </div>
                </div>
            </form>
        </div>
    </div>