<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')


<div class="col-lg-6">
        <div class="card">
            <div class="card-header no-bg b-a-0">{{$user->ops_function}}</div>
            <div class="card-block">
            	@if($items)
                <p></p>
                <form action="{{route('gem.users.connect')}}" method="post" >
                	{{ csrf_field() }}
                	<input type="hidden" name="user_id" value="{{$user->id}}">
                	<input type="hidden" name="ops_function" value="{{$user->ops_function}}">
					<fieldset class="form-group">
					    <label for="exampleSelect1">Connect</label>
					    <select class="form-control" name="id">
					    	@foreach($items as $item)
					        	<option {{($item->user_id == $user->id?'selected':'')}} value={{$item->id}}>{{$item->first_name}} {{$item->last_name}}</option>
					        @endforeach
					    </select>
					</fieldset>

                    @if($user->ops_function == 'agent')
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Team Lead</label>
                        <select class="form-control" name="tl_id">
                            <option value="">Select</option>
                            @foreach($tls as $tl)
                                <option {{$user->tl_id}} {{($agent?($tl->id == $agent->tl_id?'selected':''):'')}} value={{$tl->id}}> {{$tl->first_name}} {{$tl->last_name}}</option>
                            @endforeach
                        </select>
                    </fieldset> 

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Project Supervisor</label>
                        <select class="form-control" name="ps_id">
                            <option value="">Select</option>
                            @foreach($pss as $ps)
                                <option {{($agent?($ps->id == $agent->ps_id?'selected':''):'')}} value={{$ps->id}}>{{$ps->first_name}} {{$ps->last_name}}</option>
                            @endforeach
                        </select>
                    </fieldset>   

                    @endif

                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a class="btn btn-sm btn-primary" href="{{ route('gem.users.index') }}">Back</a>
                </form>
                @else
                	<p>Operation function for {{$user->name}} not set.</p>
                @endif
            </div>
        </div>

</div>


@endsection 

@section('footer-scripts')

@endsection