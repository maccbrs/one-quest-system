<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

   <div class="card">
      <div class="card-header no-bg b-a-0">
         <span class="pull-right"><form action="{{route('gem.users.search')}}" method="post">{{ csrf_field() }}<input type="text" class="form-control" name="keyword"  placeholder="Search"></form></span>
         <span class = "pull-right">{!!view('lilac.user.create-user-modal',['access' => $accesslist])!!}</span>
         <h5>Users List</h5>
      </div>
      <div class="card-block">
         <div class="table-responsive">

              <table class="table table-bordered customized-table">
                  <thead>
                     <tr>
                        <th>Name</th> 
                        <th>Email</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>

                     @foreach($users as $u)
                  
                     <tr>
                        <td>{{$u->name}}</td>
                        <td>{{$u->email}}</td>
                        <td>

                           {!!view('lilac.user.edit-user-modal',['id' => $u->id,'access' => $accesslist])!!}
                           {!!view('gem.users.settings-modal',['user' => $u])!!}
                           <a class="btn btn-sm btn-primary" href="{{route('gem.users.connector',$u->id)}}"><i class="material-icons" aria-hidden="true">usb</i></a>
                           {!!view('lilac.user.note-modal',['id' => $u->id,'access' => $accesslist,'userId' => $userId,'notedata' => $notedata,'user_type' => $user_type])!!}
                           {!!view('lilac.user.password-modal',['details' => $u])!!}
                           
                        </td>
                     </tr>
                     @endforeach

                  </tbody> 
             </table>
             
            @include('pagination.default', ['paginator' =>$users])
    
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

<script>


   $(document).on("click", ".add-content", function() {
      
      noc_element =  $(this).parent().parent().html();
      $( ".appendhere" ).append(noc_element);

   });

   $(document).on("click", ".remove-content", function() {

     $(this).parent().remove();
       
   });




</script>

@endsection