<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')
   
<div class="col-lg-12">
   <div class="card">
      <div class="card-header no-bg b-a-0">Users List <span class = "pull-right">{!!view('gem.users.agent-create-modal',['tl_list' => $tl_list,'ps_list' => $ps_list])!!}</span></div>
         <div class="card-block">

            <ul class="nav nav-tabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link " data-toggle="tab" href="#one" role="tab" aria-expanded="true">Agents</a>
               </li>
            </ul>

            <div class="tab-content ">
               <div class="tab-pane active" id="one" role="tabpanel" >
                  <table class="table table-bordered customized-table">
                     <thead>
                        <tr>
                           <th>Name</th> 
                           <th>Pogram Supervisor</th>
                           <th>Team Leader</th>
                           <th>Created at</th>
                           <th>Updated</th>
                        </tr>
                     </thead>
                     <tbody>

                        @foreach($agents as $value)

                           <tr>
                              <td>{{!empty($value->first_name) ? $value->first_name . " " . $value->last_name : ""}}</td>
                              <td>{{!empty($value->teamlead->first_name) ? $value->teamlead->first_name . " " . $value->teamlead->last_name  : ""}}</td>
                              <td>{{!empty($value->supervisor->first_name) ? $value->supervisor->first_name . " " . $value->supervisor->last_name  : ""}}</td>
                              <td>{{!empty($value->createdAt) ? $value->createdAt : ""}}</td>
                              <td>{!!view('gem.users.agent-edit-modal',['tl_list' => $tl_list,'ps_list' => $ps_list ,'details' => $value])!!}</td>
                          </tr>

                        @endforeach

                     </tbody>
                  </table>

                     @include('pagination.default', ['paginator' =>$agents])
               </div>
            </div>
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

<script>


</script>

@endsection