<button class="btn btn-primary btn-sm pull-right"  data-toggle="modal" data-target=".conn-modal-{{$details->trainee->id}}">
  <i class="material-icons " aria-hidden="true">contact_mail </i>
</button> 
<div class="modal fade conn-modal-{{$details->trainee->id}}" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Register a Pending User</h4>
        </div>

        <div class="modal-body">
          <form class="form-horizontal" role="form" method="POST" action="{{route('gem.users.register',$details->trainee->id)}}">
            {{ csrf_field() }}

            <input type="hidden" value = "1"  name = "user_level" > 
            <input type="hidden" value = "[]"  name = "options" > 
            <input type="hidden" value = "user"  name = "user_type" > 
            <input type="hidden" value = "1"  name = "status" >
            <input type="hidden" value = "1"  name = "active" >  
            <input type="hidden" value = "0"  name = "auditor" >
            <input type="hidden" value = "1"  name = "is_representative" >
            <input type="hidden" value = "{{$details->trainee->id}}"  name = "id" >
            <input type="hidden" value = "{{$dept[$details->trainee->position_id]}}"  name = "dept" >
          
          <div class = "row">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Name </label>
                  <div class="col-md-6">
                      <input id="name" type="text" value = '{{$details->trainee->first_name . " " . $details->trainee->middle_name . " " .  $details->trainee->last_name}}' class="form-control" name="name" >

                      @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>
            </div>
          
            <div class = "row">
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                  <div class="col-md-6">
                      <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>
            </div>

            <div class = "row">
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="col-md-4 control-label">Password</label>

                  <div class="col-md-6">
                      <input id="password" type="password" class="form-control" name="password">

                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>
            </div>

            <div class = "row">
              <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                  <div class="col-md-6">
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                      @if ($errors->has('password_confirmation'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password_confirmation') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>
            </div>
            </div>


            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
          </div>

        </form>
      </div>
  </div>
</div> 