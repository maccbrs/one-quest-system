<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')
   
<div class="col-lg-12">
   <div class="card">
      <div class="card-header no-bg b-a-0">Users List</div>
         <div class="card-block">

            <ul class="nav nav-tabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link " data-toggle="tab" href="#one" role="tab" aria-expanded="true">Agents</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#two" role="tab" aria-expanded="false">Non-Agents</a>
               </li>
            </ul>

            <div class="tab-content ">
               <div class="tab-pane active" id="one" role="tabpanel" >
                  <table class="table table-bordered customized-table">
                     <thead>
                        <tr>
                           <th>Name</th> 
                           <th>Position</th>
                           <th>Wave</th>
                           <th>Campaign</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>

                        @foreach($pending as $value)
                     
                        <tr>
                           <td>{{$value->trainee->first_name . " " . $value->trainee->middle_name . " " .  $value->trainee->last_name}}</td>
                           <td>{{!empty($value->trainee->position_id) ? $positions[$value->trainee->position_id] : ""}}</td>
                           <td>{{!empty($value->wave->name) ? $value->wave->name : ""}}</td>
                           <td>{{!empty($value->wave->name) ? $campaign[$value->wave->camp_id] : ""}}</td>
                           <td>{!!view('gem.users.register-modal',['details'=>$value,'dept'=>$dept])!!}</td>
                       </tr>
                        @endforeach

                     </tbody>
                  </table>
               </div>
               <div class="tab-pane" id="two" role="tabpanel" >
                  <table class="table table-bordered customized-table">
                     <thead>
                        <tr>
                           <th>Name</th> 
                           <th>Position</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>

                        @foreach($pending_non_agent as $value2)
                     
                        <tr>
                           <td>{{$value2->trainee->first_name . " " . $value2->trainee->middle_name . " " .  $value2->trainee->last_name}}</td>
                           <td>{{!empty($value2->trainee->position_id) ? $positions[$value2->trainee->position_id] : ""}}</td>
                           <td>{!!view('gem.users.register-modal',['details'=>$value2,'dept'=>$dept])!!}</td>
                          
                       </tr>
                        @endforeach

                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

<script>


</script>

@endsection