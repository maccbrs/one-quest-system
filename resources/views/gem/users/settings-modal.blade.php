
<button class="btn btn-primary btn-sm fa fa-cog"   data-toggle="modal" data-target=".settingsxx-modal-{{$user->id}}">
 
</button> 

<div class="modal fade settingsxx-modal-{{$user->id}}"  tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog">

      <div class="modal-content">


        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" >Settings</h4>
        </div>


        <div class="modal-body">
          <form class="form-horizontal" role="form" method="POST" action="{{route('gem.users.settings',$user->id)}}">
           {{ csrf_field() }} 
        <div class = "row">
          <div class="">
              <label for="name" class="col-md-4 control-label">User Type</label>
                <div class="col-md-6">

                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input name="user_type" type="radio" {{($user->user_type=='user'?"checked":'')}}  class="custom-control-input" value="user"> <span class="custom-control-indicator"></span> <span class="custom-control-description">User</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="user_type" type="radio" {{($user->user_type=='operation'?"checked":'')}} class="custom-control-input" value="operation"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Operation</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="user_type" type="radio" {{($user->user_type=='administrator'?"checked":'')}} class="custom-control-input" value="administrator"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Administrator</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="user_type" type="radio" {{($user->user_type=='hr'?"checked":'')}} class="custom-control-input" value="hr"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Human Resources</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="user_type" type="radio" {{($user->user_type=='trainer'?"checked":'')}} class="custom-control-input" value="trainer"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Trainer</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="user_type" type="radio" {{($user->user_type=='reportanalyst'?"checked":'')}} class="custom-control-input" value="reportanalyst"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Report Analyst</span>
                        </label>                                                                                                         
                    </div>


                </div>
            </div>
          </div>


          <p style="color:#333">(for operation dept only)</p>
          <div class = "row">
            <div class="">
                <label for="name" class="col-md-4 control-label">Operation's function</label>
                  <div class="col-md-6">

                      <div class="custom-controls-stacked">
                          <label class="custom-control custom-radio">
                              <input name="ops_function" type="radio" {{($user->ops_function=='agent'?"checked":'')}} class="custom-control-input" value="agent"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Agent</span>
                          </label>
                          <label class="custom-control custom-radio">
                              <input name="ops_function" type="radio" {{($user->ops_function=='teamlead'?"checked":'')}} class="custom-control-input" value="teamlead"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Team Leader</span>
                          </label>
                          <label class="custom-control custom-radio">
                              <input name="ops_function" type="radio" {{($user->ops_function=='supervisor'?"checked":'')}} class="custom-control-input" value="supervisor"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Supervisor</span>
                          </label>
                          <label class="custom-control custom-radio">
                              <input name="ops_function" type="radio" {{($user->ops_function=='others'?"checked":'')}} class="custom-control-input" value="others"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Others</span>
                          </label>                                                     
                      </div>


                  </div>
              </div>
            </div>
          



            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
          </div>

        </form>
        </div>

      </div>


  </div>
</div> 