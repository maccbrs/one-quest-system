<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

	<div class="row">
		<div class="col-lg-12">
            <div class="card">
               <div class="card-header no-bg b-a-0">
               </div>

               	<div class="card-block">
                  
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item ">
							<a class="nav-link active" data-toggle="tab" href="#one" role="tab" aria-expanded="false">Active Waves</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " data-toggle="tab" href="#two" role="tab" aria-expanded="true">Inactive Waves</a>
						</li>
					</ul>

					<div class="tab-content ">
						<div class="tab-pane active" id="one" role="tabpanel" >

							<h3>Wave List</h3>

							<table class="table">
							    <thead class="thead-inverse">
							        <tr>
							            <th>Name</th>
							            <th>Trainer</th>
							            <th>Campaign</th>
							            <th>Trainees</th>
							            <th>Status</th>
							            <th>Created at</th>
							            <th>Action</th>
							        </tr>
							    </thead>
							    <tbody>

							    	@foreach($waves_list as $value)
							    		 <tr>
								            <td>{{$value->name}}</td>
								            <td>
								            	<ul>
								            	@foreach($value->trainer as $value2)

								            		<li>{{$value2->name}}</li>

								            	@endforeach
								            	</ul>
								            </td>
								            <td>{{$value->camp->title}}</td>
								            <td>
								            	<ul>
												@foreach($value->stage as $value3)

								            		<li>{{$app[$value3->trainee_id]}}</li> 

								            	@endforeach
								            	</ul>
								            </td>
								            <td>{{($value->status == 1 ) ? 'Active' : 'Inactive'}}</td>
								            <td>{{$value->created_at}}</td>
								            <th>Action</th>
								        </tr>
								      
								    @endforeach
							       
							    </tbody>
							</table>
						</div>
						<div class="tab-pane" id="two" role="tabpanel" >
							<table class="table">

								<h3>Trainees</h3>

							    <thead class="thead-inverse">
							        <tr>
							            <th>Name</th>
							            <th>Trainer</th>
							            <th>Campaign</th>
							            <th>Trainees</th>
							            <th>Created at</th>
							        </tr>
							    </thead>
							    <tbody>

							    	@foreach($waves_list as $value)
								        <tr>
								          
								        </tr>
								    @endforeach
							       
							    </tbody>
							</table>
						</div>
					</div>
               </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">

</script>

@endsection 

@section('footer-scripts')

@endsection