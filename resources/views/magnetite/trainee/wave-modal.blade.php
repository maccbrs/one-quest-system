<button class="btn btn-primary btn-sm pull-right" {{$get_access}} data-toggle="modal" data-target=".conn-modal">
   Create a Wave
</button> 
<div class="modal fade conn-modal" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('magnetite.wave.add')}}" novalidate>
        {{ csrf_field() }}    

          <input type="hidden" value = ""  name = "trainee_id" > 

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Create a Wave</h4>
          </div>
          <div class="modal-body">

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label for="recipient-name" class="control-label">Wave Name</label>
                </div>
                <div class="col-xs-10">
                  <input type="text" class="form-control " name = "name" >
                </div>
              </div> 
             </div>

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label for="recipient-name" class="control-label">Campaign</label>
                </div>
                <div class="col-xs-10">
                  <select class="form-control " name="camp_id" >
                    <option selected disabled>Select Status</option>
                      @foreach($campaign as $key => $value)
                      <option value = '{{$key}}'>{{$value}}</option>
                    @endforeach
                  </select> 
                </div>
              </div> 
             </div>

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label for="recipient-name" class="control-label">Trainer</label>
                </div>
                <div class="col-xs-10">
                  <select class="form-control " name="trainer_id" >
                    <option selected disabled>Select Trainer</option>
                      @foreach($trainer as $key => $value)

                        <option value = '{{$key}}'>{{$value}}</option>

                      @endforeach
                  </select> 
                </div>
              </div> 
             </div>

            </div>  
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
  </div>
</div> 