<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

	<div class="row">
		<div class="col-lg-12">
            <div class="card">
               <div class="card-header no-bg b-a-0">Add Information Fields for Trainees
               		{!!view('magnetite.trainee.wave-modal',['trainer'=>$trainer,'campaign'=>$campaign,'get_access'=>$get_access])!!}
               </div>

               <div class="card-block">
                  
               		
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item ">
							<a class="nav-link active" data-toggle="tab" href="#one" role="tab" aria-expanded="false">Assign Wave to Applicant</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " data-toggle="tab" href="#two" role="tab" aria-expanded="true">List of Official Trainees</a>
						</li>
					</ul>

					<div class="tab-content ">
						<div class="tab-pane active" id="one" role="tabpanel" >

							<h3>Assign Wave For Applicant</h3>

							<table class="table">
							    <thead class="thead-inverse">
							        <tr>
							            <th>Name</th>
							            <th>Wave</th>
							            <th>Campaign</th>
							            <th>Contact Number</th>
							            <th>Email</th>
							            <th>Address</th>
							            <th>Status</th>
							            <th>Assign Wave to Applicant</th>
							        </tr>
							    </thead>
							    <tbody>
							    	@foreach($trainee as $value)
							    		@if(empty($value->wave->id))
									        <tr>
									            <td>{{$value->trainee->first_name . " " . $value->trainee->middle_name . " " .  $value->trainee->last_name}}</td>
								            	<td>"No Wave Assigned"</td>
								            	<td>"No Wave Assigned"</td>
									            <td>{{$value->trainee->contact_number}}</td>
									            <td>{{$value->trainee->email}}</td>
									            <td>{{$value->trainee->address}}</td>
									            <td>{{$value->stage}}</td>
									        	<td>{!!view('magnetite.trainee.assignment-modal',['details'=>$value,'waves'=>$waves,'get_access'=>$get_access])!!}</td>
									        </tr>
										@endif
								    @endforeach
							    </tbody>
							</table>
						</div>
						<div class="tab-pane" id="two" role="tabpanel" >
							<table class="table">

								<h3>Trainees</h3>

							    <thead class="thead-inverse">
							        <tr>
							           <th>Name</th>
							            <th>Wave</th>
							            <th>Campaign</th>
							            <th>Contact Number</th>
							            <th>Email</th>
							            <th>Address</th>
							            <th>Status</th>
							            <th>Change Wave to Trainee</th>
							        </tr>
							    </thead>
							    <tbody>

							    	@foreach($trainee as $value2)
							    		@if(!empty($value2->wave->id))
									        <tr>
									            <td>{{$value2->trainee->first_name . " " . $value2->trainee->middle_name . " " .  $value2->trainee->last_name}}</td>
									            <td>{{(!empty($value2->wave->name)) ? $value2->wave->name : "No Wave Assigned" }}</td>
									            <td>{{(!empty($value2->wave->camp_id)) ? $campaign[$value2->wave->camp_id] : "No Wave Assigned" }}</td>
									            <td>{{$value2->trainee->contact_number}}</td>
									            <td>{{$value2->trainee->email}}</td>
									            <td>{{$value2->trainee->address}}</td>
									            <td>{{ucfirst($value2->stage)}}</td>
						        				<td>{!!view('magnetite.trainee.update-wave-modal',['details'=>$value2,'waves'=>$waves,'get_access'=>$get_access])!!}</td>
									        </tr>
									    @endif
								    @endforeach
							       
							    </tbody>
							</table>
						</div>
					</div>
               </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">


</script>

@endsection 




@section('footer-scripts')

@endsection