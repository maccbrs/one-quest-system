<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

	<div class="row">
		<div class="col-lg-12">
            <div class="card">
               <div class="card-header no-bg b-a-0"><h3>Trainee Monitoring</h3></div>
               <div class="card-block">
                  
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item ">
							<a class="nav-link active" data-toggle="tab" href="#one" role="tab" aria-expanded="false">Foundation Skill Training</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " data-toggle="tab" href="#two" role="tab" aria-expanded="true">Product Skill Training</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " data-toggle="tab" href="#three" role="tab" aria-expanded="true">Incubation</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " data-toggle="tab" href="#four" role="tab" aria-expanded="true">Failed</a>
						</li>
					</ul>

					<div class="tab-content ">
						<div class="tab-pane active" id="one" role="tabpanel" >

							<h3>Foundation Skill Training </h3>

							<table class="table">
							    <thead class="thead-inverse">
							        <tr>
							            <th>Name</th>
							            <th>Wave</th>
							            <th>Campaign</th>
							            <th>Contact Number</th>
							            <th>Email</th>
							            <th>Address</th>
							            <th>Stage</th>
							            <th>Action</th>
							        </tr>
							    </thead>
							    <tbody>
							    	@foreach($trainee as $value)
							    		
							    		@if($value->stage == 'FST' && !empty($value->wave->name) &&  ($value->wave->trainer_id == $access['id'] || $get_access == 'abled' ))
									        <tr>
									            <td>{{$value->trainee->first_name . " " . $value->trainee->middle_name . " " .  $value->trainee->last_name}}</td>
								            	<td>{{!empty($value->wave->name) ? $value->wave->name : ""}}</td>
								            	<td>{{!empty($value->wave->name) ? $campaign[$value->wave->camp_id] : ""}}</td>
									            <td>{{$value->trainee->contact_number}}</td>
									            <td>{{$value->trainee->email}}</td>
									            <td>{{$value->trainee->address}}</td>
									            <td>{{$value->stage}}</td>
									            <td>{!!view('magnetite.monitoring.update-modal',['details'=>$value->trainee,'stage'=>'PST','get_access'=>$get_access])!!}</td>
									        </tr>
										@endif
										
								    @endforeach
							    </tbody>
							</table>
						</div>
						<div class="tab-pane" id="two" role="tabpanel" >
							<table class="table">

								<h3>Product Skill Training</h3>

							    <thead class="thead-inverse">
							        <tr>
							           <th>Name</th>
							            <th>Wave</th>
							            <th>Campaign</th>
							            <th>Contact Number</th>
							            <th>Email</th>
							            <th>Address</th>
							            <th>Stage</th>
							            <th>Action</th>
							        </tr>
							    </thead>
							    <tbody>

							    	@foreach($trainee as $value2)
							    		@if($value2->stage == 'PST' &&  ($value2->wave->trainer_id == $access['id'] || $get_access == 'abled' ))
									        <tr>
									            <td>{{$value2->trainee->first_name . " " . $value2->trainee->middle_name . " " .  $value2->trainee->last_name}}</td>
								            	<td>{{$value2->wave->name}}</td>
								            	<td>{{$campaign[$value2->wave->camp_id]}}</td>
									            <td>{{$value2->trainee->contact_number}}</td>
									            <td>{{$value2->trainee->email}}</td>
									            <td>{{$value2->trainee->address}}</td>
									            <td>{{$value2->stage}}</td>
									            <td>{!!view('magnetite.monitoring.update-modal',['details'=>$value2->trainee,'stage'=>'Incubation','get_access'=>$get_access])!!}</td>
									        </tr>
									   	@endif
								    @endforeach
							       
							    </tbody>
							</table>
						</div>

						<div class="tab-pane" id="three" role="tabpanel" >
							<table class="table">

								<h3>Incubation</h3>

							    <thead class="thead-inverse">
							        <tr>
							           <th>Name</th>
							            <th>Wave</th>
							            <th>Campaign</th>
							            <th>Contact Number</th>
							            <th>Email</th>
							            <th>Address</th>
							            <th>Stage</th>
							            <th>Action</th>
							        </tr>
							    </thead>
							    <tbody>

							    	@foreach($trainee as $value3)
							    		@if($value3->stage == 'Incubation' &&  ($value3->wave->trainer_id == $access['id'] || $get_access == 'abled' ))
									        <tr>
									            <td>{{$value3->trainee->first_name . " " . $value3->trainee->middle_name . " " .  $value3->trainee->last_name}}</td>
								            	<td>{{$value3->wave->name}}</td>
								            	<td>{{$campaign[$value3->wave->camp_id]}}</td>
									            <td>{{$value3->trainee->contact_number}}</td>
									            <td>{{$value3->trainee->email}}</td>
									            <td>{{$value3->trainee->address}}</td>
									            <td>{{$value3->stage}}</td>
									            <td>{!!view('magnetite.monitoring.update-modal',['details'=>$value3->trainee,'stage'=>'Passed','get_access'=>$get_access])!!}</td>
									        </tr>
									    @endif
								    @endforeach
							       
							    </tbody>
							</table>
						</div>

						<div class="tab-pane" id="four" role="tabpanel" >
							<table class="table">

								<h3>Failed</h3>

							    <thead class="thead-inverse">
							        <tr>
							           <th>Name</th>
							            <th>Wave</th>
							            <th>Campaign</th>
							            <th>Contact Number</th>
							            <th>Email</th>
							            <th>Address</th>
							            <th>Stage</th>
							        </tr>
							    </thead>
							    <tbody>

							    	@foreach($trainee as $value4)
							    		@if($value4->stage == 'failed' &&  ($value4->wave->trainer_id == $access['id'] || $get_access == 'abled' ))
									        <tr>
									            <td>{{$value4->trainee->first_name . " " . $value4->trainee->middle_name . " " .  $value4->trainee->last_name}}</td>
								            	<td>{{$value4->wave->name}}</td>
								            	<td>{{$campaign[$value4->wave->camp_id]}}</td>
									            <td>{{$value4->trainee->contact_number}}</td>
									            <td>{{$value4->trainee->email}}</td>
									            <td>{{$value4->trainee->address}}</td>
									            <td>{{ucfirst($value4->stage)}}</td>
									  		</tr>
									    @endif
								    @endforeach
							       
							    </tbody>
							</table>
						</div>

					</div>
               </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">


</script>

@endsection 




@section('footer-scripts')

@endsection