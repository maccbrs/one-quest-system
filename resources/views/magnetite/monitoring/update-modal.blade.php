<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".conn-modal-{{$details->id}}">
  <i class="material-icons " aria-hidden="true">contact_mail </i>
</button> 
<div class="modal fade conn-modal-{{$details->id}}" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('magnetite.monitoring.update_stage',$details->id)}}" novalidate>
        {{ csrf_field() }}    

          <input type="hidden" value = "{{$details->id}}"  name = "trainee_id" > 
          <input type="hidden" value = "FST"  name = "stage" > 

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Trainer's Evaluation </h4>
          </div>
          <div class="modal-body">

          <div class = "row">
            <div class="form-group">
              <div class="col-xs-2">
                <label for="recipient-name" class="control-label">Evaluation</label>
              </div>
              <div class="col-xs-10">
                <select class="form-control " name="stage" >
                  <option selected value ='{{$stage}}'>Passed</option>
                  <option value ='failed'>Failed</option>
                </select> 
              </div>
            </div> 
           </div>
          </div>  

          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
  </div>
</div> 