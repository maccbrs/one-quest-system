  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.full.min.js"></script>
			  <script> (function() {
				'use strict';

				angular
					.module('xlsx-model', [])
					.directive(
						'xlsxModel', [
							'$parse',
							function($parse) {
								return {
									restrict: 'A',
									link: function(scope, el, attrs) {
										var model = $parse(attrs.xlsxModel);
										var modelSetter = model.assign;
										var readOpts = {
											type: 'binary'
										};

										el.bind('change',
											function() {
												if (attrs.multiple) {
													var master = {};
													var len = el[0].files.length - 1;
													angular
														.forEach(
															el[0].files,
															function(
																f) {
																var reader = new FileReader();
																reader.onload = function(
																	file) {
																	var wb = XLSX
																		.read(
																			file.target.result,
																			readOpts);
																	master[f.name] = {};
																	angular
																		.forEach(
																			wb.SheetNames,
																			function(
																				name) {
																				master[f.name][name] = XLSX.utils
																					.sheet_to_json(wb.Sheets[name]);
																			});
																	if (f === el[0].files[len]) {
																		modelSetter(
																			scope,
																			master);
																		scope
																			.$apply();
																	}
																}
																reader
																	.readAsBinaryString(f);
															});
												} else {
													var reader = new FileReader();
													reader.onload = function(
														file) {
														var wb = XLSX
															.read(
																file.target.result,
																readOpts);
														var data = {};
														angular
															.forEach(
																wb.SheetNames,
																function(
																	name) {
																	data[name] = XLSX.utils
																		.sheet_to_json(wb.Sheets[name]);
																});
														modelSetter(
															scope,
															data);
														scope
															.$apply();
													}
													reader
														.readAsBinaryString(el[0].files[0]);
												}
											});
									}
								};
							}
						]);
			})();

			var app = angular.module('xlsxApp', ['xlsx-model'], );
			app.controller('xlsxCtrl', ['$scope', function($scope) {
			  // Nothing to do here :)

			}]);
			
			
			
			

</script>



<div class="modal fade modalUpload" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo $tl_list['form-action'] ?>">
			  
			  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title custom_align">Upload </h4>
                </div>
                <div class="modal-body">
							

			
								
					<body ng-app="xlsxApp" ng-controller="xlsxCtrl">
					 
						<h2>Select or drop XLSX files:</h2>
						<input type="file" class="form-control" xlsx-model="excel" >
					   
						
						<textarea class="col-md-12" style="height:500px;font-size:10px" name="batchdata"> {{excel | json}}</textarea>
						
					  
					</body>
					  

					
                    
                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-sign"></span> Create
                    </button>
                </div>
            </form>
        </div> 
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>