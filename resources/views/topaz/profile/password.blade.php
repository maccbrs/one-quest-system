<script>
function validateForm() {
    var x = document.forms["password"]["new_password1"].value;
    var y = document.forms["password"]["new_password2"].value;
    var div = document.getElementById("wcpass");
    if (x != y) {
        div.style.display = "block";
        return false;
    }
}

function hide() {
    var div = document.getElementById("wcpass");
      div.style.display = "none";
}

</script>

<button class="btn btn-default btn-sm" data-toggle="modal" data-target=".password-modal">
  <i class="material-icons" aria-hidden="true">edit </i>
</button></h4>
<div class="modal fade password-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form name="password" method="post" class="form-horizontal" action="{{route('topaz.profile.password')}}"" enctype="multipart/form-data" onsubmit="return validateForm()">
        {{ csrf_field() }}   

        <input type="hidden" name="dept_to" value="">
        <input type="hidden" name="type" value="1">                 

        <div class="modal-body">
          <div class = "row">
            <div class="form-group">
              <div class="col-xs-4">
                <label  >Current Password</label>
              </div>
              <div class="col-xs-8">
                <input type="password" name="current_password" value="" class="form-control " required="">   
              </div>
            </div> 
           </div>

          <div class = "row">
            <div class="form-group">
              <div class="col-xs-4">
                <label  >New Password</label>
              </div>
              <div class="col-xs-8">
                <input type="password" name="new_password1" id="new_password1" value="" class="form-control" required="" onclick="return hide()">
              </div>
            </div> 
           </div>

          <div class = "row">
            <div class="form-group">
              <div class="col-xs-4">
                <label  >Repeat Password</label>
              </div>
              <div class="col-xs-8">
                <input type="password" name="new_password2" id="new_password2" value="" class="form-control" required="" onclick="return hide()">
                    <span class="add-on"><i class="icon-eye-open"></i></span>
                  <div id="wcpass" class="alert alert-danger" style="display: none; margin-top: 10px;">
                    <strong>Password and Confirm Password does not match.</strong>
                  </div>
              </div>
              
            </div> 
           </div>

          </div>       

          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <input type="submit" id="btnSubmit" class="btn btn-primary updatebutton" value="Submit" onclick="return Validate()">
          </div>

        </form>
      </div>
  </div>
</div> 