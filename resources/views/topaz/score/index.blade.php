<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')
@section('header-scripts')

@endsection
@section('content')
		<?php $drange = $helper->getStartAndEndDate($helper->current_week()); ?>
        <div class="col-lg-12">
			<div class="card">
			    <div class="card-header no-bg b-a-0">Week No {{$helper->current_week()}} {{'From '.$drange["week_start"].' to '.$drange['week_end']}}</div>
			    <div class="card-block">
			        <div class="table-responsive">

							<table class="table">
							    <thead class="thead-default">
							        <tr>
							            <th>Name</th>
							            <th>Ticket Closed</th>
							            <th>Score</th>
							        </tr>
							    </thead>
							    <tbody>
							    	@foreach($users as $u)
							        <tr>
							            <th scope="row">{{$u->user->name}}</th>
							            <td>{{$u->count}}</td>
							            <td>{{$u->points}}</td>
							        </tr>
							        @endforeach
							    </tbody>
							</table>

			        </div>
			    </div>
			</div>
        </div>
                                                                    
@endsection 

@section('footer-scripts')

@endsection