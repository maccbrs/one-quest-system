<?php $asset = URL::asset('/'); ?> 
<?php $usertype = Auth::user()->user_type; 
$request = Request::instance();
//echo '<pre>';
//print_r($request->query('campaigns')); die;
?>
<!doctype html>
<html lang="en">
   <!-- Mirrored from milestone.nyasha.me/latest/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Aug 2016 23:34:54 GMT -->
   <head>

      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <title>@yield('title')</title>
      <link rel="stylesheet" href="{{$asset}}milestone/vendor/bower-jvectormap/jquery-jvectormap-1.2.2.css">
      <link rel="stylesheet" href="{{$asset}}custom/multiselect/css/multi-select.css">
      <link rel="stylesheet" href="{{$asset}}milestone/styles/app.min.css">
      <link rel="stylesheet" href="{{$asset}}sweetalert/dist/sweetalert.css">
      <link rel="stylesheet" href="{{$asset}}milestone/vendor/blueimp-file-upload/css/jquery.fileupload-ui.css">
      <link rel="stylesheet" href="{{$asset}}milestone/vendor/blueimp-file-upload/css/jquery.fileupload.css">
      <link rel="stylesheet" href="{{$asset}}jquery-ui/jquery-ui.css">
      <link rel="stylesheet" href="{{$asset}}bootstrap-multiselect/dist/css/bootstrap-multiselect.css">
      <link rel="stylesheet" href="{{$asset}}bootstrap-timepicker/css/bootstrap-timepicker.min.css">

      <script  src="{{$asset}}milestone/scripts/jquery-1.12.0.min.js" ></script>
      <script src="{{$asset}}sweetalert/dist/sweetalert.min.js"></script>

      @yield('header-scripts')

   <style type="text/css">

   </style> 


   </head>
   <body class="skin-4">

      <div class="app expanding">
         <div class="off-canvas-overlay" data-toggle="sidebar"></div>
         <div class="sidebar-panel">
            <div class="brand">  <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen hidden-lg-up"><i class="material-icons">menu</i> </a>   <a class="brand-logo"><img class="expanding-hidden" src="{{$asset}}milestone/images/logo.png" alt=""> </a> </div>
            <div class="nav-profile dropdown">
               <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                  <div class="user-image">
                    <img data-url="{{$request->query('UserImageSm')}}" src="{{$request->query('UserImageSm')}}" class="avatar img-circle" alt="user" title="user"></div> 
                  <div class="user-info expanding-hidden">{{Auth::user()->name}}
                    <small class="bold">{{($usertype != 'operation') ? ($usertype) : ((Auth::user()->user_level == 1) ? 'Team Leader' : 'Manager')}}</small></div>
               </a>
               <div class="dropdown-menu">
                  <a class="dropdown-item" href="{{route('garnet.profile.index')}}">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Logout</a>
               </div>
            </div>
            <nav>
               <p class="nav-title">NAVIGATION</p> 
               <ul class="nav">





                  @if($key->access('sunstone'))
                    <li>
                       <a href="javascript:;">
                          <span class="menu-caret"><i class="material-icons">arrow_drop_down</i> </span>
                          <i class="material-icons text-success">record_voice_over</i> <span class="badge bg-primary pull-right">08</span>
                           <span>Recruitment Tool</span>
                       </a>
                       <ul class="sub-menu">
                          <li>
                             <a href="javascript:;"><span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span> <span>Applicants</span></a>
                             <ul class="sub-menu">
                                <li><a href="{{route('sunstone.applicant.pool')}}"><span>Pool</span></a></li>
                                <li><a href="{{route('sunstone.applicant.active')}}"><span>Active</span></a></li>
                                <li><a href="{{route('sunstone.applicant.pool')}}"><span>For Training</span></a></li>
                             </ul>
                          </li>

                          <li><a href="{{route('sunstone.campaign.index')}}"><span>Campaign List</span></a></li>
                          <li><a href="{{route('sunstone.position.index')}}"><span>Position List</span></a></li>
                          <li><a href="{{route('sunstone.applicant.index')}}"><span>Applicant Form</span></a></li>

                          <li>
                             <a href="javascript:;"><span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span> <span>Assessments</span></a>
                             <ul class="sub-menu">
                                <li><a href="{{route('sunstone.assessment.index')}}"><span>Manage</span></a></li>
                                <li><a href="{{route('sunstone.assessment.results')}}"><span>Results</span></a></li>
                             </ul>
                          </li>
                       </ul>
                    </li>
                  @endif 

                  @if($key->access('magnetite'))
                    <li>
                      <a href="javascript:;">
                        <span class="menu-caret"><i class="material-icons">arrow_drop_down</i> </span>
                        <i class="material-icons text-success">school</i> <span class="badge bg-primary pull-right">08</span>
                           <span>Trainer's Tool</span>
                      </a>
                      
                      <ul class="sub-menu">
                        <li><a href="{{route('magnetite.trainee.index')}}"><span>Trainees List</span></a></li>
                        <li><a href="{{route('magnetite.monitoring.index')}}"><span>Trainee Monitoring</span></a></li>
                          
                          <li><a href="{{route('magnetite.wave.index')}}"><span>Wave List</span></a></li>
                      </ul>
                    </li>
                  @endif 


                  @if($key->access('opal'))  

                  <li>
                     <a href="javascript:;">
                        <span class="menu-caret"><i class="material-icons">arrow_drop_down</i> </span>
                        <i class="material-icons " style="color:#63E9FC">people</i> <span class="badge bg-primary pull-right">01</span>
                         <span>HR Tools</span>
                     </a>
                     <ul class="sub-menu">
                        <li>
                           <a href="javascript:;"><span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span> <span>Connector</span></a>
                           <ul class="sub-menu">
                              <li><a href="{{route('opal.connector.refs')}}"><span>Refs</span></a></li>
                           </ul>
                        </li> 
                        <li>
                           <a href="javascript:;"><span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span> <span>Settings</span></a>
                           <ul class="sub-menu">
                              <li><a href="{{route('opal.settings.dispute-date')}}"><span>Dispute Period</span></a></li>
                              <li><a href="{{route('opal.settings.dispute-issue')}}"><span>Dispute Issues</span></a></li>
                           </ul>
                        </li>  
                        <li>
                           <a href="javascript:;"><span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span> <span>Disputes</span></a>
                           <ul class="sub-menu">
                              <li><a href="{{route('opal.disputes.index')}}"><span>approved</span></a></li>
                              <li><a href="{{route('gem.approval.disputes')}}"><span>for approval(agents)</span></a></li>
                              <li><a href="{{route('opal.approval.nonagent')}}"><span>for approval (non agents)</span></a></li>
                           </ul>
                        </li>  
                        <li>
                           <a href="javascript:;"><span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span> <span>Personnel Directory</span></a>
                           <ul class="sub-menu">
                                <li><a href="{{route('opal.directory.department','executives')}}"><span>Executives</span></a></li>
                                <li><a href="{{route('opal.directory.department','bd')}}"><span>BD</span></a></li>
                                <li><a href="{{route('opal.directory.department','hr')}}"><span>HR</span></a></li>
                                <li><a href="{{route('opal.directory.department','admin')}}"><span>Admin</span></a></li>
                                <li><a href="{{route('opal.directory.department','accounting')}}"><span>Acctng</span></a></li>
                                <li>
                                    <a href="javascript:;"><span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span> <span>Operation</span></a>
                                    <ul class="sub-menu">
                                       <li><a href="{{route('opal.directory.operation','campaigns')}}"><span>Campaigns</span></a></li>
                                       <li><a href="{{route('opal.directory.operation','teamleads')}}"><span>Team Leaders</span></a></li>
                                       <li><a href="{{route('opal.directory.operation','agents')}}"><span>Representatives</span></a></li>
                                    </ul>
                                 </li> 
                                <li><a href="{{route('opal.directory.department','quality')}}"><span>Quality Assurance</span></a></li>
                                <li><a href="{{route('opal.directory.department','reportanalyst')}}"><span>Report Analyst</span></a></li>
                                <li><a href="{{route('opal.directory.department','recruitment')}}"><span>Recruitment</span></a></li>
                                <li><a href="{{route('opal.directory.department','trainer')}}"><span>Training Dept</span></a></li>
                                <li><a href="{{route('opal.directory.department','noc')}}"><span>NOC</span></a></li>
                           </ul>
                        </li> 
                        <li>
                           <a href="{{route('opal.announcements.index')}}"><span>Announcements</span></a>
                        </li>                        
                        <li>
                           <a href="{{route('opal.campaigns.index')}}"><span>Campaigns</span></a>
                        </li>   
                        <li>
                           <a href="{{route('opal.clearance.index')}}"><span>Clearance</span></a>
                        </li>                                                                                                  
                     </ul> 
                  </li>
                 @endif

                      
                  <li>
                     <a href="javascript:;">
                        <span class="menu-caret"><i class="material-icons">arrow_drop_down</i> </span>
                        <i class="material-icons " style="color:#98fb98">face</i> <span class="badge bg-primary pull-right">01</span>
                         <span>YOU</span>
                     </a>
                     <ul class="sub-menu">
                        <li>
                           <a href="javascript:;"><span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span> <span>Profile Settings</span></a>
                           <ul class="sub-menu">
                              <li><a href=""><span>Profile Picture</span></a></li>
                           </ul>
                        </li>                      
                        <li>
                           <a href="javascript:;"><span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span> <span>for approval</span></a>
                           <ul class="sub-menu">
                              <li><a href="{{route('gem.approval.disputes')}}"><span> Disputes(agents) </span></a></li>
                              <li><a href="{{route('gem.approval.disputes2')}}"><span> Disputes </span></a></li>
                           </ul>
                        </li>
                        <li>
                           <a href="javascript:;"><span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span> <span>Application</span></a>
                           <ul class="sub-menu">
                              <li><a href="{{route('gem.request.disputes')}}"><span>Disputes</span></a></li>
                           </ul>
                        </li> 

                      
                                                                     
                     </ul> 
                  </li> 
                  <li>

                     <a href="javascript:;">
                        <span class="menu-caret"><i class="material-icons">arrow_drop_down</i> </span>
                        <i class="material-icons " style="color:#63E9FC">contact_phone</i> <span class="badge bg-primary pull-right">01</span>
                         <span>Magellan Team</span>
                     </a>

                     <ul class="sub-menu">
                        <li><a ><span>Notes and Credentials</span></a>
                                       
                         <ul class="sub-menu">
                            <li><a href="{{route('garnet.team.notes')}}"><span>Give Notes/Creds</span></a></li>
                            <li><a href="{{route('garnet.notes.noc','Administrator')}}"><span>NOC</span></a></li>
                            <li><a href="{{route('garnet.notes.hr','Human Resource')}}"><span>Human Resource</span></a></li>
                            <li><a href="{{route('garnet.notes.manager','Manager')}}"><span>Manager</span></a></li> 
                            <li><a href="{{route('garnet.notes.trainer','Trainer')}}"><span></span>Training</a></li> 
                            <li><a href="{{route('garnet.notes.teamleader','Team Leader')}}"><span>Team Leader</span></a></li> 
                            <li><a href="{{route('garnet.notes.qa','Quality Assurance')}}"><span>Quality Assurance</span></a></li>             
                         </ul> 
                  
                        </li>

                        <li><a ><span>Messages</span></a>
                          
                         <ul class="sub-menu">
                            <li><a href="{{route('garnet.team.agents')}}"><span>Agents</span></a></li> 
                            <li><a href="{{route('garnet.team.noc','Administrator')}}"><span>NOC</span></a></li> 
                            <li><a href="{{route('garnet.team.hr','Human Resource')}}"><span>Human Resource</span></a></li> 
                            <li><a href="{{route('garnet.team.training','Trainer')}}"><span>Training</span></a></li> 
                            <li><a href="{{route('garnet.team.qa','Quality Assurance')}}"><span>Quality Assurance</span></a></li>
                            <li><a href="{{route('garnet.team.operations','Operations')}}"><span>Operations</span></a></li>  
                            <li><a href="{{route('garnet.team.group')}}"><span>Group</span></a></li>             
                         </ul> 
                        </li>             
                     </ul> 
                  </li> 
               </ul>
            </nav>
         </div>
         <div class="main-panel">

            <nav class="header navbar">
                  @yield('top-navigation')
            </nav>

            <div class="main-content">
               <div class="content-view">
                  @include('sweet::alert')
                  @yield('content')
               </div>
            </div>
 
         </div> 


         <div class="modal fade sidebar-modal chat-panel" tabindex="-1" role="dialog" aria-labelledby="chat-panel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="chat-header">
                     <a class="pull-right" href="javascript:;" data-dismiss="modal"><i class="material-icons">close</i></a>
                     <div class="chat-header-title">People</div>
                  </div>
                  <div class="modal-body flex scroll-y">
                     <div class="chat-group">
                        <div class="chat-group-header">Favourites</div>
                        <a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-online"></span> <span>Catherine Moreno</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-online"></span> <span>Denise Peterson</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-away"></span> <span>Charles Wilson</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-away"></span> <span>Melissa Welch</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-no-disturb"></span> <span>Vincent Peterson</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Pamela Wood</span></a>
                     </div>
                     <div class="chat-group">
                        <div class="chat-group-header">Online</div>
                        <a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-online"></span> <span>Tammy Carpenter</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-away"></span> <span>Emma Sullivan</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-no-disturb"></span> <span>Andrea Brewer</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-online"></span> <span>Betty Simmons</span></a>
                     </div>
                     <div class="chat-group">
                        <div class="chat-group-header">Other</div>
                        <a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Denise Peterson</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Jose Rivera</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Diana Robertson</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>William Fields</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Emily Stanley</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Jack Hunt</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Sharon Rice</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Mary Holland</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Diane Hughes</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Steven Smith</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Emily Henderson</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Wayne Kelly</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Jane Garcia</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Jose Jimenez</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Rachel Burton</span> </a><a href="javascript:;" data-toggle="modal" data-target=".chat-message"><span class="status-offline"></span> <span>Samantha Ruiz</span></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="modal fade chat-message" tabindex="-1" role="dialog" aria-labelledby="chat-message" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="chat-header">
                     <div class="pull-right dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">more_vert</i></a>
                        <div class="dropdown-menu" role="menu"><a class="dropdown-item" href="javascript:;">Profile</a> <a class="dropdown-item" href="javascript:;">Clear messages</a> <a class="dropdown-item" href="javascript:;">Delete conversation</a> <a class="dropdown-item" href="javascript:;" data-dismiss="modal">Close chat</a></div>
                     </div>
                     <div class="chat-conversation-title">
                        <img src="{{$asset}}milestone/images/face1.jpg" class="avatar avatar-xs img-circle m-r-1 pull-left" alt="">
                        <div class="overflow-hidden"><span><strong>Charles Wilson</strong></span> <small>Last seen today at 03:11</small></div>
                     </div>
                  </div>
                  <div class="modal-body flex scroll-y">
                     <p class="text-xs-center text-muted small text-uppercase bold m-b-1">Yesterday</p>
                     <div class="chat-conversation-user them">
                        <div class="chat-conversation-message">
                           <p>Hey.</p>
                        </div>
                     </div>
                     <div class="chat-conversation-user them">
                        <div class="chat-conversation-message">
                           <p>How are the wife and kids, Taylor?</p>
                        </div>
                     </div>
                     <div class="chat-conversation-user me">
                        <div class="chat-conversation-message">
                           <p>Pretty good, Samuel.</p>
                        </div>
                     </div>
                     <p class="text-xs-center text-muted small text-uppercase bold m-b-1">Today</p>
                     <div class="chat-conversation-user them">
                        <div class="chat-conversation-message">
                           <p>Curabitur blandit tempus porttitor.</p>
                        </div>
                     </div>
                     <div class="chat-conversation-user me">
                        <div class="chat-conversation-message">
                           <p>Goodnight!</p>
                        </div>
                     </div>
                     <div class="chat-conversation-user them">
                        <div class="chat-conversation-message">
                           <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
                        </div>
                     </div>
                  </div>
                  <div class="chat-conversation-footer">
                     <button class="chat-left"><i class="material-icons">face</i></button>
                     <div class="chat-input" contenteditable=""></div>
                     <button class="chat-right"><i class="material-icons">photo</i></button>
                  </div>
               </div>
            </div>
         </div>


      </div>
      <script type="text/javascript"></script>
      <script src="{{$asset}}milestone/vendor/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>
      <script src="{{$asset}}milestone/scripts/helpers/noty-defaults.js"></script>    
      <script src="{{$asset}}milestone/vendor/blueimp-file-upload/js/jquery.fileupload.js"></script>
      <script src="{{$asset}}milestone/scripts/app.min.js"></script>
      <script src="{{$asset}}socketio/socket.io.js"></script>
      <script src="{{$asset}}jquery-ui/jquery-ui.js"></script>
      <script  src="{{$asset}}bootstrap-multiselect/dist/js/bootstrap-multiselect.js" ></script>
      <script  src="{{$asset}}bootstrap-timepicker/js/bootstrap-timepicker.min.js" ></script>
      @yield('footer-scripts')
   </body>
   
</html> 