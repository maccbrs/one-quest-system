<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".close-ticket">
<i class="material-icons" aria-hidden="true">sync_disabled</i>
</button>

    <div class="modal fade close-ticket" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                    <div class="card">
                        <div class="card-block">

                            <form action="{{route('topaz.you.close',$id)}}" method="post">
                                {{ csrf_field() }}
                                <h3>Close ticket and rate the support</h3>
                                <p>Please help us improve our service, give us your most honest feedback.</p>
                                <div class="form-group row">
                                    <label class="col-sm-2 form-control-label">Give credit to:</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select" name="user_id">
                                            <option value="">None</option>
                                            @foreach($responders as $k => $responder)
                                                <option value="{{$k}}">{{$responder}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group row">
                                    <label class="col-sm-2 form-control-label">Feedback:</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select" name="point">
                                            <option value="60">Worse</option>
                                            <option value="70">Poor</option>
                                            <option value="75">Fair</option>
                                            <option value="90">Good</option>
                                            <option value="100">excellent</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group row">
                                    <label class="col-sm-2 form-control-label">Note:</label>
                                    <div class="col-sm-10">
                                        <input name="notes">
                                    </div>
                                </div>                                                                 
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>