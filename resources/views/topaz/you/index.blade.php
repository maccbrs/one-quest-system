<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')
@section('header-scripts')
<style type="text/css">
.color-lightgreen{
  /*  background-color: #F0FFF0;*/
}
.color-lightred{
   /* background-color: #ffe5e5;*/
    font-weight: bold;
/*    text-decoration: line-through;
    color: red;*/
}
.tr-head{
    background-color: #ebebeb;
    color: #777;
}
.tr-closing{
    background-color: #f2576e;
}
.tr-cc{
    color: #9e4638;
}
.tr-closing-cc{
    background-color: #f2d7db;
}

</style>
@endsection
@section('content')
                        <div class="col-lg-12">
<div class="card">
    <div class="card-header no-bg b-a-0">Tickets</div>
    <div class="card-block">
        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="tr-head">
                        <th>Date</th>
                        <th>Title</th>
                        <th>Reply</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Status</th> 
                    </tr>
                </thead>
                <tbody>

                    @foreach($items as $item) 
                    <tr class="<?php if($item->reply->count()): $reply = $help->last_reply($item->reply); echo ($reply['user_id'] == Auth::user()->id?'color-lightgreen':'color-lightred'); endif; ?> {{($item->status == 'closing'?'tr-closing':'')}}">
                        <td>{{$help->time_ago($item->created_at)}}</td>
                        <td><a class="text-primary" href="{{route('topaz.you.show',$item->id)}}">{{$item->title}}</a></td>
                        <td>{{$item->reply->count()}}</td>
                        <td>@if($item->reply->count()) <?php $reply = $help->last_reply($item->reply); echo $help->time_ago($reply['created_at']); ?> @endif</td>
                        <td>@if($item->reply->count()) <?php $reply = $help->last_reply($item->reply); echo $reply['user']['name']; ?> @endif</td>
                        <td>{{($item->status == 'closing'?'for closing '.$help->time_ago($item->updated_at):'')}}</td>
                    </tr>
                    @endforeach

                    @foreach($cc as $c)
                        <tr class="tr-cc {{($c->ticket->status == 'closing'?'tr-closing-cc':'')}}">
                            <td>{{$help->time_ago($c->ticket->created_at)}}</td>
                            <td><a class="text-primary" href="{{route('topaz.you.show_cc',$c->ticket->id)}}">{{$c->ticket->title}}</a></td>
                            <td>{{$c->ticket->reply->count()}}</td>
                            <td>@if($c->ticket->reply->count()) <?php $reply = $help->last_reply($c->ticket->reply); echo $help->time_ago($reply['created_at']); ?> @endif</td>
                            <td>@if($c->ticket->reply->count()) <?php $reply = $help->last_reply($c->ticket->reply); echo $reply['user']['name']; ?> @endif</td>
                            <td>{{($c->ticket->status == 'closing'?'for closing '.$help->time_ago($c->ticket->updated_at):'')}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
                        </div>
                                                                    

@endsection 

@section('footer-scripts')

@endsection