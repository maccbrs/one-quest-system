<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('header-scripts')
    <link rel="stylesheet" href="{{$asset}}milestone/vendor/summernote/dist/summernote.css">
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="card card-block">
            <span class="pull-right" style="margin-left:5px;">{!!view('topaz.you.add-cc',['id'=>$item->id,'users' => $users,'admins' => $admins,'cc' => $cc])!!}</span>
            <span class="pull-right">{!!view('topaz.you.close-ticket',['id'=>$item->id,'responders' => $responders])!!}</span>
            <div class="profile-timeline-header">
                <a href="#" class="profile-timeline-user"><img src="{{asset('uploads/sm-'.$item->user->avatar)}}" alt="" class="img-rounded">
                </a>
                <div class="profile-timeline-user-details">
                    <a href="#" class="bold">
                    [{{$item->user->name}}] <span class="ticket-title">{{$item->title}}</span>
                </a>
                    <br><span class="text-muted small">{{$item->created_at}}</span>
                </div>
            </div> 
            <div class="profile-timeline-content">
                {!!$item->content->content!!}
            </div>

            @foreach($item->reply as $reply) 
                <div class="profile-timeline-header">
                    <a href="#" class="profile-timeline-user"><img src="{{asset('uploads/sm-'.$reply->user->avatar)}}" alt="" class="img-rounded">
                    </a>
                    <div class="profile-timeline-user-details">
                        <a href="#" class="bold">
                        [{{$reply->user->name}}]
                    </a>
                        <br><span class="text-muted small">{{$reply->created_at}}</span>
                    </div>
                </div>
                <div class="profile-timeline-content">
                    {!!$reply->content->content!!}
                </div>                                
            @endforeach

            @if($item->status == 'open')
            <div>
                <div class="summernote"></div> 
                <button class="btn btn-primary" onclick="save()" type="button">Submit</button>
            </div> 
            
            <form id="form-submit" action="{{route('topaz.you.reply')}}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="content" id="input-content-reply">
                <input type="hidden" name="ticket_id" value="{{$item->id}}">
            </form> 
            @elseif($item->status == 'closing') 
            <p>This ticket is for closing, hit this button if want to reopen it --> {!!view('topaz.you.reopen',['id' => $item->id])!!}</p> 
            @else
            <p>Ticket is closed</p>
            @endif                                                            
        </div>
    </div> 
                                                                    

@endsection 

@section('footer-scripts')

    
        <script src="{{$asset}}milestone/vendor/summernote/dist/summernote.js"></script>
        <script type="text/javascript">

        var save = function() {
          $('#input-content-reply').val($('.summernote').summernote('code'));
          $("#form-submit").submit();
          $('.summernote').summernote('destroy');
        };

        $(document).ready(function(){ 
            setTimeout(function(){ $('.summernote').summernote({
               height: 200,
               width:300,
               popover: {
                 image: [],
                 link: [],
                 air: []
               }
             }); }, 500);
        });
   

!function(a) {
    a.mb_lib = {
        mb_ajax: function(b) {
            var c = {
                data: "null",
                url: "null",
                type: "post",
                attachto: "null"
            };
            if (b) a.extend(c, b);
            a.ajax({
                url: c["url"],
                data: c["data"],
                type: c["type"],
                success: function(b) {
                    a(c["attachto"]).html(b);
                    console.log(b);
                },
                error: function(a) {
                    console.log(a);
                }
            });
        }
    }; 

    $(".addCcInput").on('click',function(){
        var a = {
            data: { 
                "_token": "{{ csrf_token() }}",
                "status":$(this).children("input").is(":checked"),
                "user_id":$(this).children("input").val(),
                "ticket_id": "{{$item->id}}"
            },
            url: "{{route('topaz.you.add_cc')}}",
            attachto: ".jy-form"
        }; 
        $.mb_lib.mb_ajax(a);          
    });

        
}(jQuery);


</script>
@endsection