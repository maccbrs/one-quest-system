<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('header-scripts')
<link rel="stylesheet" href="{{$asset}}milestone/vendor/summernote/dist/summernote.css">
@endsection

@section('content')
    @if($items->count())
                        <div class="col-lg-12" style="margin-top: 50px">
                            <div class="card card-block">
                                <div class="profile-timeline-header">
                                    <form id="form-submit" action="{{route('topaz.you.store')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="content" id="input-content">
                                        <label>Title</label>
                                        <input type="text" name="title">
                                        <label style="margin-left:50px;">Campaign/Departmen:</label>
                                        <select class="custom-select" name="campaign_id">
                                            @foreach($items as $item)
                                                <option value="{{$item->campaign_id}}">{{$item->campaign->name}}</option>
                                            @endforeach
                                        </select>                                        
                                    </form>
                                </div>
                                <div>
                                    <div class="summernote"></div> 
                                    <button class="btn btn-primary" onclick="save()" type="button">Submit</button>
                                </div>
                            </div>

                        </div>
 

    @else
        <div class="col-lg-12" style="margin-top: 50px">
            <div class="card card-block">
                No Dept Assigned to you
            </div>
        </div>    
    @endif
                                                                
@endsection 

@section('footer-scripts')

<script src="{{$asset}}milestone/vendor/summernote/dist/summernote.js"></script>
<script type="text/javascript">

// var edit = function() {
//   $('.summernote').summernote({focus: true});
// };



var save = function() {

  $('#input-content').val($('.summernote').summernote('code'));
  $("#form-submit").submit();
  $('.summernote').summernote('destroy');
};


$(document).ready(function(){ 
    setTimeout(function(){ $('.summernote').summernote(); }, 1000);
});
</script>
@endsection