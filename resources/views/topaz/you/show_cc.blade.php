<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('header-scripts')
    <link rel="stylesheet" href="{{$asset}}milestone/vendor/summernote/dist/summernote.css">
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="card card-block">
            <div class="profile-timeline-header">
                <a href="#" class="profile-timeline-user"><img src="{{asset('uploads/sm-'.$item->user->avatar)}}" alt="" class="img-rounded">
                </a>
                <div class="profile-timeline-user-details">
                    <a href="#" class="bold">
                    [{{$item->user->name}}] <span class="ticket-title">{{$item->title}}</span>
                </a>
                    <br><span class="text-muted small">{{$item->created_at}}</span>
                </div>
            </div> 
            <div class="profile-timeline-content">
                {!!$item->ticket->content->content!!}
            </div>

            @foreach($item->ticket->reply as $reply) 
                <div class="profile-timeline-header">
                    <a href="#" class="profile-timeline-user"><img src="{{asset('uploads/sm-'.$reply->user->avatar)}}" alt="" class="img-rounded">
                    </a>
                    <div class="profile-timeline-user-details">
                        <a href="#" class="bold">
                        [{{$reply->user->name}}]
                    </a>
                        <br><span class="text-muted small">{{$reply->created_at}}</span>
                    </div>
                </div>
                <div class="profile-timeline-content">
                    {!!$reply->content->content!!}
                </div>                                
            @endforeach

                                      
        </div>
    </div> 
                                                                    

@endsection 

@section('footer-scripts')

@endsection