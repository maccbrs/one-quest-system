<button class="btn btn-primary btn-sm " data-toggle="modal" data-target=".reopen-ticket">
<i class="material-icons" aria-hidden="true">loop</i>
</button>

    <div class="modal fade reopen-ticket" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                    <div class="card">
                        <div class="card-block">

                            <form action="{{route('topaz.you.reopen',$id)}}" method="post">
                                {{ csrf_field() }}
                                <h3>Are you sure you want to reopen the ticket?</h3>                                                             
                                <button type="submit" class="btn btn-primary">Proceed</button>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>