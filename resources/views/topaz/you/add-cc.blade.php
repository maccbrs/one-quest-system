<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".add-cc">
<i class="material-icons" aria-hidden="true">people</i>
</button>

    <div class="modal fade add-cc" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                    <div class="card">
                        <div class="card-block">
                            <h3>Carbon Copy</h3>
                            <p>Add users who can view this ticket</p>
                            <div class="form-group row">
                                <label class="col-sm-2 form-control-label">Users</label>
                                <div class="col-sm-10">
                                    @foreach($users as $u)

                                        @if(!in_array($u->user_id,$admins) && Auth::user()->id != $u->user_id)
                                            @if(!empty($u->user->name))
                                                <label class="checkbox-inline addCcInput">
                                                    <input type="checkbox" {{(in_array($u->user_id,$cc)?'checked':'')}} value="{{$u->user_id}}"> {{!empty($u->user->name) ? $u->user->name : "" }}
                                                </label> <br>
                                            @endif 
                                        @endif

                                    @endforeach
                              
                                </div>
                            </div>                                                              
                        </div> 
                    </div>

                </div>
            </div>
        </div>
    </div>