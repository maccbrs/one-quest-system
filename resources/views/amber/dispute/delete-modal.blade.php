<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".conn-delete-modal-{{$details->id}}">
  <i class="material-icons " aria-hidden="true">delete </i>
</button> 
<div class="modal fade conn-delete-modal-{{$details->id}}" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
       <form method="post" action="{{route('amber.dispute.delete')}}" novalidate>
        {{ csrf_field() }}    

          <input type="hidden" value = "{{$details->id}}"  name = "id" > 
          <input type="hidden" value = "{{$details->tl_response}}"  name = "tl_response" > 

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Delete this Dispute File? </h4>
          </div>


          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              @if($details->tl_response == 1)
                  <button type="submit" class="btn btn-primary">Yes Im Sure</button>
              @endif
          </div>
        </form>
      </div>
  </div>
</div> 