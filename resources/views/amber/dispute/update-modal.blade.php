<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".conn-modal-{{$details->id}}">
  <i class="material-icons " aria-hidden="true">contact_mail </i>
</button> 
<div class="modal fade conn-modal-{{$details->id}}" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <form method="post" action="{{route('amber.dispute.update')}}">
        {{ csrf_field() }}    

          <input type="hidden" value = "{{$details->id}}"  name = "id" > 
          <input type="hidden" value = "{{$details->tl_response}}"  name = "tl_response" >
          <input type="hidden" value = "{{$details->ra_response}}"  name = "ra_response" > 
          <input type="hidden" value = "{{$details->hr_response}}"  name = "hr_response" > 

          <div class="modal-body">
            <div class="card">
              <div class="card-block">
              <h5>Dispute Details</h5>
                <p class="mb-details-modal">

          <span>Dispute Period:</span> {{$details->period->dispute_label}}<br>
          <span>Dispute Date:</span> {{$details->dispute_date}}<br>
          <span>Dispte Issue:</span> {{$details->issueObj->issue_name}}<br>

          @if($details->tl_responses != 1)
          <span>Supervisor Status:</span> {{$help->response($details->tl_response)}}<br>
          <span>Supervisor Comment:</span> {{!empty($help->fetchComments($details->tl_comments))?$help->fetchComments($details->tl_comments) : 'N/A'}}<br>
          @endif

          @if($details->ra_response != 0)
          <span>Reports Analyst Status:</span> {{$help->response($details->ra_response)}}<br>
          <span>Reports Analyst Comment: </span> {{!empty($help->fetchComments($details->ra_comments))?$help->fetchComments($details->ra_comments) : 'N/A'}}<br>
          @endif


          @if($details->hr_response != 0)
          <span>HR Status:</span> {{$help->response($details->hr_response)}}<br>
          <span>HR Comment:</span> {{!empty($help->fetchComments($details->hr_comments))?$help->fetchComments($details->hr_comments) : 'N/A'}}<br>
          @endif
          </p>
          @if($details->tl_response == 1)
          <br>
          <h5>Edit Dispute</h5>

          <fieldset class="form-group">
          <label for="exampleSelect1">Dispute Period</label>
          <select name="dispute_period" id="txtFirst" class="form-control" required>
          <option value="">Select Dispute Period</option>
          @foreach($datepluck as $k => $d)
          <option value="{{$k}}">{{$d}}</option>
          @endforeach
          </select>
          </fieldset>

          <fieldset class="form-group">
          <label >Dispute Date</label>
          <input name="dispute_date" class="form-control m-b-1 datepicker" id="txtThird" data-provide="datepicker"  required> 
          </fieldset>

          <fieldset class="form-group">
          <label for="exampleSelect1">Issue</label>
          <select name="issue_name" id="txtSecond" class="form-control" required>
          <option value="">Select Issue</option>
          @foreach($issuespluck as $k1 => $d1)
          <option value="{{$k1}}">{{$d1}}</option>
          @endforeach
          </select>
          </fieldset>

          <fieldset class="form-group">
          <label for="exampleSelect1">Your Notes</label>
          <textarea class="form-control" name="notes" >{{$details->notes}}</textarea>
          </fieldset>

          </div>
          @endif
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              @if($details->tl_response == 1)
                  <button type="submit" class="btn btn-primary">Save</button>
              @endif
          </div>
        </form>
      </div>
      </div>
      </form>
      </div>
      </div>
      </div>


