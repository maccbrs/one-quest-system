<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".detail-modal-{{$item->id}}"><i class="material-icons" aria-hidden="true">folder_open </i></button>

    <div class="modal fade detail-modal-{{$item->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                            <div class="card">
                                <div class="card-header no-bg b-a-0">Create New Dispute</div>
                                <div class="card-block">

                                        <div  class="form-group">
                                            <label for="exampleSelect1">Date Created</label>
                                            <p>{{$item->created_at}}</p>
                                        </fieldset>

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Dispute Period</label>
                                            <p>{{$disputepluck[$item->dispute_period]}}</p>
                                        </fieldset>                                     

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Dispute Date</label>
                                            <p>{{$item->dispute_date}}</p>
                                        </fieldset>   

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Dispute Time</label>
                                            <p>{{$item->dispute_time}}</p>
                                        </fieldset>  

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Issue</label>
                                            <p>{{$issuepluck[$item->issue]}}</p>
                                        </fieldset>

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Notes</label>
                                            <p>{{$item->notes}}</p>
                                        </fieldset>

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Supervisor approval</label>
                                            <p>{{($item['tl_response'] == '0') ? 'Pending' : (($item['tl_response'] == '1') ? 'Approved' : 'Rejected' ) }}</p>
                                        </fieldset>  

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">Supervisor Notes</label>
                                            <p>{{$item['tl_comments']}}</p>
                                        </fieldset>  

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">RA approval</label>
                                            <p>{{($item['ra_response'] == '0') ? 'Pending' : (($item['ra_response'] == '1') ? 'Approved' : 'Rejected' ) }}</p>
                                        </fieldset>  

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">RA Notes</label>
                                            <p>{{$item['ra_comments']}}</p>
                                        </fieldset> 

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">HR approval</label>
                                            <p>{{($item['hr_response'] == '0') ? 'Pending' : (($item['hr_response'] == '1') ? 'Approved' : 'Rejected' ) }}</p>
                                        </fieldset>  

                                        <fieldset class="form-group">
                                            <label for="exampleSelect1">HR Notes</label>
                                            <p>{{$item['hr_comments']}}</p>
                                        </fieldset>                                           

                                </div>
                            </div>

                </div>
            </div>
        </div>
    </div>