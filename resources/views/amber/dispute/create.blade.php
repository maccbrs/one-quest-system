<?php $asset = URL::asset('/'); ?> 
@extends('amber.master')

@section('title', 'dashboard')

@section('content')

 <div class="row">
    <div class="col-md-4">

            <div class="card">
                <div class="card-header no-bg b-a-0">Default Bootstrap elements</div>
                <div class="card-block">
                    <p>Bootstrap provides several form control styles, layout options, and custom components for creating a wide variety of forms.</p>
                    <form method="post" action="{{route('amber.dispute.create')}}" novalidate>
                        {{ csrf_field() }}
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"> <small class="text-muted">We'll never share your email with anyone else.</small>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleSelect1">Example select</label>
                            <select class="form-control" id="exampleSelect1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleSelect2">Example multiple select</label>
                            <select multiple="multiple" class="form-control" id="exampleSelect2">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleTextarea">Example textarea</label>
                            <textarea class="form-control" id="exampleTextarea" rows="3">
                            </textarea>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" class="form-control-file" id="exampleInputFile"> <small class="text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
                        </fieldset>
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="checked"> Option one is this and that&mdash;be sure to include why it's great</label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"> Option two can be something else and selecting it will deselect option one</label>
                        </div>
                        <div class="radio disabled">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled="disabled"> Option three is disabled</label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Check me out</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>

    </div>
</div>



@endsection 

@section('footer-scripts')

@endsection