<?php $asset = URL::asset('/'); ?> 

@extends('amber.master')

@section('title', 'dashboard')

@section('header-scripts')
    <style type="text/css">
    .mb-details-modal{
        color: green;
    }
    .mb-details-modal span{
        color: #000;
        text-decoration: underline;
        font-weight: bold;
    }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#one" role="tab" aria-expanded="true">Filed Disputes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#two" role="tab" aria-expanded="false">Approved</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#three" role="tab" aria-expanded="false">Rejected</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="one" role="tabpanel" aria-expanded="true">
                    <div class="card">
                      <div class="card-block"  >
                          <div class="table-responsive" >
                            <button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".createNewDispute">Create New</button>
                                <table class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th>Date Created</th>
                                            <th>Dispute Period</th>
                                            <th>Dispute Date</th>
                                            <th>Issue</th>
                                            <th>Supp status</th>
                                            <th>RA Status</th>
                                            <th>HR Status</th>
                                            <th>#</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        @if($items)
                                            @foreach($items as $item)
                                                @if($item->hr_response != 2 && !$help->rejected($item->tl_response,$item->ra_response,$item->hr_response))
                                                <tr>
                                                    <td>{{$item->created_at}}</td>
                                                    <td>{{$item->period->dispute_label}}</td>
                                                    <td>{{$item->dispute_date}}</td>
                                                    <td>{{$item->issueObj->issue_name}}</td>
                                                    <td>{{$help->response($item->tl_response)}}</td>
                                                    <td>{{$help->response($item->ra_response)}}</td>
                                                    <td>{{$help->response($item->hr_response)}}</td>
                                                    <td>
                                                        {!!view('amber.dispute.delete-modal',['details'=>$item, 'help'=>$help ])!!}
                                                        {!!view('amber.dispute.update-modal',['details'=>$item, 'help'=>$help, 'datepluck'=>$date_pluck, 'issuespluck'=>$issues_pluck])!!}
                        
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab-pane" id="two" role="tabpanel" aria-expanded="false">
                    <div class="card">
                      <div class="card-block"  >
                          <div class="table-responsive" >
                                <table class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th>Date Created</th>
                                            <th>Dispute Period</th>
                                            <th>Dispute Date</th>
                                            <th>Issue</th>
                                            <th>Supp status</th>
                                            <th>RA Status</th>
                                            <th>HR Status</th>
                                            <th>#</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        @if($items)
                                            @foreach($items as $item)
                                                @if($item->hr_response == 2)
                                                <tr>
                                                    <td>{{$item->created_at}}</td>
                                                    <td>{{$item->period->dispute_label}}</td>
                                                    <td>{{$item->dispute_date}}</td>
                                                    <td>{{$item->issueObj->issue_name}}</td>
                                                    <td>{{$help->response($item->tl_response)}}</td>
                                                    <td>{{$help->response($item->ra_response)}}</td>
                                                    <td>{{$help->response($item->hr_response)}}</td>
                                                    <td>{!!view('amber.dispute.update-modal',['details'=>$item, 'help'=>$help, 'datepluck'=>$date_pluck, 'issuespluck'=>$issues_pluck])!!}</td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab-pane" id="three" role="tabpanel" aria-expanded="false">
                    <div class="card">
                      <div class="card-block"  >
                          <div class="table-responsive" >

                                <table class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th>Date Created</th>
                                            <th>Dispute Period</th>
                                            <th>Dispute Date</th>
                                            <th>Issue</th>
                                            <th>Supp status</th>
                                            <th>RA Status</th>
                                            <th>HR Status</th>
                                            <th>#</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        @if($items)
                                            @foreach($items as $item)
                                                @if($help->rejected($item->tl_response,$item->ra_response,$item->hr_response))
                                                <tr>
                                                    <td>{{$item->created_at}}</td>
                                                    <td>{{$item->period->dispute_label}}</td>
                                                    <td>{{$item->dispute_date}}</td>
                                                    <td>{{$item->issueObj->issue_name}}</td>
                                                    <td>{{$help->response($item->tl_response)}}</td>
                                                    <td>{{$help->response($item->ra_response)}}</td>
                                                    <td>{{$help->response($item->hr_response)}}</td>
                                                    <td>{!!view('amber.dispute.update-modal',['details'=>$item, 'help'=>$help, 'datepluck'=>$date_pluck, 'issuespluck'=>$issues_pluck])!!}
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                          </div>
                      </div>
                  </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade createNewDispute" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                    <div class="card">
                        <div class="card-header no-bg b-a-0">Create New Dispute</div>
                        <div class="card-block">
                            <form method="post" action="{{route('amber.dispute.create')}}" novalidate>

                                {{ csrf_field() }}

                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Dispute Period</label>
                                    <select class="form-control" name="dispute_period">
                                        <option  disabled>Select Dispute Period</option>
                                        @foreach($date_pluck as $k => $d)
                                                <option value="{{$k}}">{{$d}}</option>
                                            @endforeach
                                    </select>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label >Dispute Date</label>
                                    <input name = "dispute_date" class="form-control m-b-1 datepicker" data-provide="datepicker"  > 
                
                                </fieldset>


                                <fieldset class="form-group">
                                    <label for="exampleSelect1">Issue</label>
                                    <select class="form-control" name="issue">
                                        <option  disabled>Select Issue</option>
                                        @foreach($issues_pluck as $k1 => $d1)
                                            <option value="{{$k1}}">{{$d1}}</option>
                                        @endforeach
                                    </select>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="exampleTextarea">Notes</label>
                                    <textarea class="form-control" rows="3" name="notes"></textarea>
                                </fieldset> 

                                <button type="submit" class="btn btn-primary">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


 
@endsection

@section('footer-scripts')
    <script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{{$asset}}milestone/vendor/clockpicker/dist/bootstrap-clockpicker.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

    <script type="text/javascript">

        $('.datepicker').datepicker({
             dateFormat: 'YYYY/MM/DD'
    
        });

        $('.timepicker').timepicker();

    $( ".daterangeinput" ).change(function() {
        daterangevalue = $( ".daterangeinput" ).val();
        $( ".disputelabelinput" ).val(daterangevalue);
    });



    </script>
@endsection