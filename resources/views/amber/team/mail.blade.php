<?php $asset = URL::asset('/'); ?> 

@extends('amber.master')

@section('title', 'dashboard')

@section('content')

@if(!empty($magellan_users))

	<div class="card">
	   <div class="card-header no-bg b-a-0"> </div>
	      <div class="card-block">
	        <div class="table-responsive">
	          <div class="content-view">
	             <div class="fill-container layout-xs b-b">
	              <div class="layout-column-xs overflow-hidden">
	                <div class="row m-x-0 fill-container">

	                  <div class="col-lg-4 p-a-0 messages-list bg-white b-r flexbox-xs layout-column-xs full-height">
	                     <div class="message-header">
	                        <div class="message-toolbar">
	                         
	                              <h4>{{ucfirst($dept)}} Personnel </h4>
	                          
	                        </div>
	                     </div>

	                     <div class="flex-xs scroll-y">
	                        <ul class="message-list nav nav-tabs" >

	                        	@foreach($magellan_users as $user)

                        			<?php $user_id = 'message'.$user->id ?>
	                        		<li class="nav-item message-list-item message_item">
		                                <a class="nav-link " data-toggle="tab" href="#{{$user_id}}" role="tab">
		                                  <img class="avatar avatar-sm img-circle pull-left m-r-1" onerror="this.src='{{$asset}}milestone/images/unknown.jpg'" src="{{$asset}}milestone/images/{{$user->id}}.jpg">
		                                   <div class="message-list-item-header">
		                                      <div class="time"></div>
		                                      <span class="bold">{{$user->name }}</span>
		                                   </div><p class="overflow-hidden"></p>
		                                </a>
		                            </li>

	                        	@endforeach


	                          </ul>
	                     </div>
	                  </div>

	                  <div class="col-lg-8 p-a-0 messages-list bg-white b-r flexbox-xs layout-column-xs full-height">
	                     <div class="flexbox-xs layout-column-xs message-view">
	                        <div class="message-header">
	                           <div class="message-toolbar">
	                              <div class="pull-right">
	                                
	                              </div>
	                              <a href="javascript:;" class="hidden-lg-up" data-toggle="message">
	                                 <i class="material-icons m-r-1">arrow_back</i>
	                              </a>
	                           </div>
	                        </div>

	                        <div class="message-body flex-xs scroll-y">
	                          <div class="tab-content">

	                          	@foreach($magellan_users as $user2)

   									<?php $user_id2 = 'message'.$user2->id ?>

	                        		<div class="tab-pane" id="{{$user_id2}}" role="tabpanel">

	                                	<a data-toggle="modal" data-target="#myModal{{$user2->id}}" class="m-l-1 pull-right btn btn-outline-info" tooltip-placement="bottom" uib-tooltip="Reply to all recipients">
		                                    <i class="material-icons">create</i>New Message
		                                </a>

			                            <div class="overflow-hidden">
		                                 <h4 class="lead m-t-0">
		                                  <img class="avatar  img-rounded" alt="" src="{{$asset}}milestone/images/{{$user_id2}}.jpg">
		                                  {{$user2->name}} </h4>
		                                </div>

									<hr>

	                        		<ul class="message-list nav nav-tabs" >
		                                @if(!empty($messages_list[$user2->id]))
		                                  @foreach($messages_list[$user2->id] as $m)
		                                   <?php $message = 'message'.$m->id ?>
		                                      @if($userId == $m->user)
		                                        <?php $pull = 'pull-left from_message'?>
		                                      @else
		                                        <?php $pull = 'pull-right to_message'?>
		                                      @endif
		                                        <div class="col-lg-9 {{$pull}}">
		                                          <h5>{{$m->content->content}}</h5>
		                                          <div class="date">sent by {{$users_list[$m->user]}} at {{$m->created_at}} </div>
		                                        </div>
		                                    @endforeach
	                                 	@endif
	                                </ul>
                               
                                    </div>

                                    <div class="modal fade" id="myModal{{$user2->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									   <div class="modal-dialog" role="document">
									      <div class="modal-content">

									        <form id="validate" class="form-horizontal" role="form" method="POST" action="{{route('amber.team.new_message')}}">
									          
									          <input type="hidden" value ="1" name="type" > 
									          <input type="hidden" value ="1" name="status" > 
									          <input type="hidden" value ="agents" name="page" >
									          <input type="hidden" value ="{{$userId}}" name="user" >

									          {{ csrf_field() }}

									          <div class="modal-header">
									              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									              <h4 class="modal-title" id="myModalLabel">Send a Message</h4>
									          </div>

									          <div class="modal-body">

									            <fieldset class="form-group">
									              <label for="exampleSelect1">To</label>
									                <select class="form-control" id="exampleSelect1" name = "to">

									                  <option disabled>Select Recipient</option>
									                  <option value = "{{$user2->id}}" >{{$user2->name}}</option>
									                 
									                </select>
									            </fieldset>
									            
									            <input type="hidden" class="form-control" placeholder="Enter Subject" pattern=".{3,40}" name="subject" > 
									      
									            <fieldset class="form-group">
									              <label for="exampleInputEmail1">Message</label>
									              <textarea class="form-control" rows="7" name="content" pattern=".{3,100}" ></textarea>
									            </fieldset>

									          </div>

									          <div class="modal-footer">
									            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
									            <button type="submit" class="btn btn-primary">Send</button>
									          </div>
									        </form>
									      </div>
									   </div>
									</div>


	                        	@endforeach

	                          </div>
	                        </div>
	                     </div>
	                  </div>
	                </div>
	              </div>
	          </div>
	        </div>
	      </div>
	   </div>    
	</div>

@endif

	<script>

	// 	$( ".nav-link" ).on( "click", function() {
	// 	   	to_id = $(this).children(".id_holder").val();
	// 	   	user_id = $(this).children(".user_id").val();

	// 	   	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

 //    		$.ajax({
	//           url: "/amber/team/test",
	//           type: "POST",
	//           data: {
	//               _token: CSRF_TOKEN,
	//               to: to_id,
	//               user: user_id,
	//           },
	//           success:function(data) {
	//             alert('fail');
	//           },
	 
	//       });


	// 	});

	 </script>

@endsection 


@section('footer-scripts')

@endsection