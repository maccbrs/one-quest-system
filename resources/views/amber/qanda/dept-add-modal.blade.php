<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target=".add-modal">
  <i class="material-icons" aria-hidden="true">add </i>
</button>
<div class="modal fade add-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('amber.qanda.create')}}" novalidate>
        {{ csrf_field() }}   
        <input type="hidden" name="dept_to" value="{{$id}}">
        <input type="hidden" name="type" value="1">                      

          <div class="modal-body">
            <fieldset class="form-group">
                <label for="exampleSelect1">Post To {{$id}}</label>
            </fieldset>              
            <fieldset class="form-group">
                <label for="content">Query</label>
                <textarea class="form-control" rows="3" name="inquiry"></textarea>
            </fieldset>                                  
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Post</button>
          </div>
        </form>
      </div>
  </div>
</div> 