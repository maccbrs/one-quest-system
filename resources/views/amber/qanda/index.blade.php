<?php $asset = URL::asset('/'); ?> 
@extends('amber.master')

@section('title', 'dashboard')

@section('content')

            <div class="card card-block">
                {!!view('amber.qanda.dept-add-modal',compact('id'))!!}

                @if($items->count())
                    @foreach($items as $item)
                        <div class="profile-timeline-header">
                            <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
                            </a>
                            <div class="profile-timeline-user-details"><a href="#" class="bold">{{$item->user->name}}</a>
                                <br><span class="text-muted small">{{$item->created_at}}</span>
                            </div>
                        </div>
                        <div class="profile-timeline-content">
                            <p>{{$item->inquiry}}</p>
                        </div>
                    @endforeach
                @endif

            </div>  

@endsection 

@section('footer-scripts')

@endsection