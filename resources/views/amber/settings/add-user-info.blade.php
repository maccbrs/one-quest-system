
<a href="{{route('poppy.ticket.open')}}" data-toggle="modal" data-target=".modalAddInfo{{$id}}"><I>Add more details</I> <i class="fa fa-plus"></i></a>
  <div class="modal fade bd-example-modal-lg modalAddInfo{{$id}}" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <form class="form-horizontal form-label-left" method="post" enctype ='multipart/form-data' action="{{route('amber.settings.add_info')}}" novalidate>

           {{ csrf_field() }}

            <input type="hidden"  name="user_id" value = '{{$id}}'  > 

            <div class="modal-content  ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add More Interesting Info About Youself</h4>
                </div>

                <div class="modal-body">

                    @if(!empty($user_data['information']->header))
                    
                        @foreach($user_data['information']->header as $ctr =>$value)
                     
                            <input type="hidden"  name="header[]" value = '{{$user_data['information']->header[$ctr]}}'  >
                            <input type="hidden" class="form-control" placeholder="Gender" name="content[]" readonly value = "{{$user_data['information']->content[$ctr]}}"> 
                          
                        @endforeach

                    @endif

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Your Info </label> <br>
                        <div class="col-lg-6">
                            <label for="exampleSelect1">Information Header</label>
                            <input type="text" class="form-control"  name="header[]"  > 
                        </div>
                        <div class="col-lg-6">
                            <label for="exampleSelect1">Content</label>
                            <input type="text" class="form-control"  name="content[]"  > 
                        </div>
                    </fieldset>  

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </form>
      </div>
  </div>
