<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

    @foreach($announcements as $k => $v)

        <div class="col-lg-12">
            <div class="card card-block">
                <div class="profile-timeline-header">
                    <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
                    </a>
                    <div class="profile-timeline-user-details"><a href="#" class="bold">{{$v->title}}</a>
                        <br><span class="text-muted small">{{$v->created_at}}</span>
                    </div>
                </div>
                <div class="profile-timeline-content">
                    <p>{{$v->excerpt}}...</p>
                    <a href="{{route('amber.announcements.list',$v->id)}}" class="btn btn-primary">view</a>
                </div>
            </div>
        </div>
    @endforeach
 
@endsection 

@section('footer-scripts')

@endsection