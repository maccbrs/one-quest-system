<?php $asset = URL::asset('/'); ?> 
@extends('amber.master')

@section('title', 'dashboard')

@section('top-navigation')

	<nav class="header navbar">
	  <div class="header-inner">
	    <div class="navbar-item navbar-spacer-right brand hidden-lg-up">

	      <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen">
	        <i class="material-icons">menu</i>
	      </a>

	      <a class="brand-logo hidden-xs-down">
	        <img src="images/logo_white.png" alt="logo">
	      </a> 
	    </div>

	    <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#">
	      <span>
	      	<img src="{{$asset}}milestone/images/website_logo.png" alt="" class="img-rounded" style ="width: 2%;"> Magellan-Link </span>
	    </a>

	  </div>
	</nav>

@endsection

@section('content')

<style>
.announcement-format{
	margin-left: 50px;
	margin-right: 50px;
}
</style>

<div class="content-view">
	<div class="row">
		<div class="col-lg-4">
			<div class="card profile-bio">
				<a class="background"></a>
				<a href="javascript:;" class="avatar">
					<img src="{{Request::instance()->query('UserImageSm')}}" alt="">
					<!--<img src="{{Request::instance()->query('UserImageM')}}" alt=""> -->
				</a>

				<div class="user-details col-md-12" style="padding:10px;height:485px;">
					
				
					
					<table  class="table table-bordered" style="" >
					<tbody>
					<tr>
					<td width="">Email </td>
					<td width=""><span>{{Auth::user()->email }}</td></tr>
					<tr>
					<td>Emp No.</td><td>{{Auth::user()->emp_code }}</td></tr>
					
					
					<?php $ApproverFetcher = new AppHelper; $larr_myarray = array();?>
					
					
					<td>Dept/Campaign</td><td></td></tr>
					<td>Dispute Approver 1</td><td><?php
							if (isset($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)) 
								echo $ApproverFetcher->fetchuser($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)->name; else echo "unset"; ?></td></tr>
					<td>Dispute Approver 1</td><td><?php
							if (isset($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2))
								echo $ApproverFetcher->fetchuser($ApproverFetcher->fetchTableByCondition(Auth::user()->id,"amethyst" . chr(92) . "GemApprover" )[0]->level2)->name; else echo "unset";?></td></tr>
					
					</tbody></table>


				</div>

				<div class="user-stats">
					<ul>
						<li>
							<a href="javascript:;"><span class="small text-uppercase block text-muted"><strong>Posts</strong></span>
							<h5 class="m-b-0"><strong></strong></h5>
							</a>
						</li>

						<li><a href="javascript:;">
							<span class="small text-uppercase block text-muted"><strong>Following</strong></span>
							<h5 class="m-b-0"><strong></strong></h5>
							</a>
						</li>

						<li>
							<a href="javascript:;"><span class="small text-uppercase block text-muted"><strong>Followers</strong></span>
							<h5 class="m-b-0"><strong></strong></h5></a>
						</li>
					</ul>
				</div>
			</div>

			<div class="profile-sidebar">

				<div class="card">
					<div class="card-block">
				        <div class="profile-timeline-header">
	                        <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
	                        </a>
	                        <div class="profile-timeline-user-details"><a href="{{route('amber.messages.lists',['source' => 'messages','status' => 'all'])}}" class="bold">Messages</a>
	                            <br><span class="text-muted small">Recieved {{!empty($created_message->created_at) ? (Carbon\Carbon::parse($created_message->created_at)->diffForHumans()) : 'No Message'}}</span>
	                        </div>
	                    </div>
	                    <div class="profile-timeline-content">
	                       <a href="{{route('amber.messages.lists',['source' => 'messages','status' => 'unread'])}}" class="m-r-xs">
	                       	<span>Unread({{$help->print_status_message($statuses_message,'messages','unread')}}) </span></a>
	                       	<a href="{{route('amber.messages.lists',['source' => 'messages','status' => 'read'])}}" class="m-r-xs">
	                       		<span>Opened({{$help->print_status_message($statuses_message,'messages','read')}})</span>
	                       	</a>
	                       	<a href="{{route('amber.messages.lists',['source' => 'messages','status' => 'all'])}}" class="m-r-xs">
	                       		<span>All({{$help->print_status_message($statuses_message,'messages','unread')+$help->print_status_message($statuses_message,'messages','read')}})</span>
	                       	</a>
	                    </div>
					</div>
				</div>
				
				<div class="card">
					<div class="card-block">
				        <div class="profile-timeline-header">
	                        <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
	                        </a>
	                        <div class="profile-timeline-user-details"><a href="{{route('amber.announcements.lists',['source' => 'hrd','status' => 'all'])}}" class="bold">HRD - announcements</a>
	                            <br><span class="text-muted small">Updated {{Carbon\Carbon::parse($created_hrd->created_at)->diffForHumans()}} - {{Carbon\Carbon::parse($created_hrd->created_at)->format('F d, Y')}}</span>
	                        </div>
	                    </div>
	                    <div class="profile-timeline-content">
	                    	<a href="{{route('amber.announcements.lists',['source' => 'hrd','status' => 'unread'])}}" class="m-r-xs"><span>Unread({{$help->print_status($statuses,'hrd','unread')}}) </span></a><a href="{{route('amber.announcements.lists',['source' => 'hrd','status' => 'read'])}}" class="m-r-xs"><span>Opened({{$help->print_status($statuses,'hrd','read')}})</span></a><a href="{{route('amber.announcements.lists',['source' => 'hrd','status' => 'all'])}}" class="m-r-xs"><span>All({{$help->print_status($statuses,'hrd','unread')+$help->print_status($statuses,'hrd','read')}})</span></a>
	                    </div>
					</div>
				</div>

				<div class="card">
					<div class="card-block">
				        <div class="profile-timeline-header">
	                        <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
	                        </a>
	                        <div class="profile-timeline-user-details"><a href="{{route('amber.announcements.lists',['source' => 'noc','status' => 'all'])}}" class="bold">NOC - announcements </a>
	                            <br><span class="text-muted small">Updated {{Carbon\Carbon::parse($created_noc->created_at)->diffForHumans()}} - {{Carbon\Carbon::parse($created_noc->created_at)->format('F d, Y')}}</span>
	                        </div>
	                    </div>
	                    <div class="profile-timeline-content">
	                    	<a href="{{route('amber.announcements.lists',['source' => 'noc','status' => 'unread'])}}" class="m-r-xs"><span>Unread({{$help->print_status($statuses,'noc','unread')}}) </span></a><a href="{{route('amber.announcements.lists',['source' => 'noc','status' => 'read'])}}" class="m-r-xs"><span>Opened({{$help->print_status($statuses,'noc','read')}})</span></a><a href="{{route('amber.announcements.lists',['source' => 'noc','status' => 'all'])}}" class="m-r-xs"><span>All({{$help->print_status($statuses,'noc','unread')+$help->print_status($statuses,'noc','read')}})</span></a>              
	                    </div>
					</div>
				</div>


				<div class="card">
					<div class="card-block">
				        <div class="profile-timeline-header">
	                        <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
	                        </a>
	                        <div class="profile-timeline-user-details"><a href="{{route('amber.announcements.lists',['source' => 'qa','status' => 'all'])}}" class="bold">Team Q&A  </a>
	                            <br><span class="text-muted small">Updated {{Carbon\Carbon::parse($created_qa->created_at)->diffForHumans()}} - {{Carbon\Carbon::parse($created_qa->created_at)->format('F d, Y')}}</span>
	                        </div>
	                    </div>
	                    <div class="profile-timeline-content">
	                    	<a href="{{route('amber.announcements.lists',['source' => 'qa','status' => 'unread'])}}" class="m-r-xs"><span>Unread({{$help->print_status($statuses,'qa','unread')}}) </span></a><a href="{{route('amber.announcements.lists',['source' => 'qa','status' => 'read'])}}" class="m-r-xs"><span>Opened({{$help->print_status($statuses,'qa','read')}})</span></a><a href="{{route('amber.announcements.lists',['source' => 'qa','status' => 'all'])}}" class="m-r-xs"><span>All({{$help->print_status($statuses,'qa','read')+$help->print_status($statuses,'qa','unread')}})</span></a>   
	                    </div>
					</div>
				</div>

			</div>
		</div>
		
		<div class="col-lg-8">
			<div class="card card-block">
				<form id="validate" class="form-horizontal" role="form" method="POST" action="{{route('amber.newsfeed.new_post')}}">

					{{ csrf_field() }}

					<input type="hidden" value ="" name="attachment" >
					<input type="hidden" value ="1" name="status" >
					<input type="hidden" value ="post" name="type" >
					<input type="hidden" value ="" name="tags" >
					<input type="hidden" value ="" name="reactions" >
					<input type="hidden" value ="{{$user_id}}"  name="created_for" >
					<input type="hidden" value ="{{$user_id}}" name="created_by" >


					<div class="panel-body">
	                	<textarea id="editor" class="form-control"  name="caption"></textarea>
	            	</div>
	            	
	            	<button type="submit" class="btn btn-primary pull-right">Post</button>
	            </form>
	        </div>
		</div>
	</div>
</div>

<!-- announcement modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Latest Announcement</h4>
      </div>
      <div class="modal-body" style="background-color: #FFFBE8;">
        <h4 class="card-title announcement-format"><u>{{$latest_announcement->title}}</u><br></h4>
        <small class="lead text-uppercase announcement-format">From: {{$latest_announcement->source}} DEPARTMENT</small></p>
        <p class="card-text"><h6 class="lead text-justify announcement-format" style="line-height: 200%;">{!! ($latest_announcement->content) !!}</h6></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
    window.onload = function() {
    
    $('#myModal').modal('show');
}
</script>
@endsection 

@section('footer-scripts')

@endsection