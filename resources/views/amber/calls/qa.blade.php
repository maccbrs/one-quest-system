<?php $asset = URL::asset('/'); ?> 
@extends('amber.master')

@section('title', 'dashboard')

@section('content')

      <div class="card">

         <div class="card-block">
            <div class="table-responsive">
                 <div class="row">
                    <div class="col-md-12">
                          <div class="card">
                              <div class="card-block"  >
                                  <div class="table-responsive" id="auditList">
                                    <table class="table table-bordered ">
                                        <thead>
                                            <tr>
                                                <th>Agent</th>
                                                <th>Call Date</th>
                                                <th>Campaign Id</th>
                                                <th>Call Length</th>
                                                <th>Dispo</th>
                                                <th>Listen</th>
                                                <th>Score</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @if($items->count())
                                            @foreach($items as $item)
                                            <tr >
                                                <td>{{$item->agent}}</td>
                                                <td>{{$item->call_date}}</td>
                                                <td>{{$item->campaign_id}}</td>
                                                <td>{{$item->length_in_min}}</td>
                                                <td>{{$item->status}}</td>
                                                <td>
                                                <audio controls="controls">  
                                                    <source src="{{$item->location}}" />  
                                                </audio>
                                                </td> 
                                                <td>{{$item->score}}</td>
                                            </tr> 
                                            @endforeach
                                          @endif                         
                                        </tbody>
                                    </table>

                                    @include('pagination.default', ['paginator' => $items])
                                  </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
         </div>
      </div>


@endsection 

@section('footer-scripts')

@endsection