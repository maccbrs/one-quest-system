<?php $asset = URL::asset('/'); ?> 
@extends('amber.master')

@section('title', 'index')

@section('content')
                              <div class="card">
         <div class="card-block">
              <div class="card-header no-bg b-a-0"><h4>Messages (Read)</h4></div> 

             <div class="table-responsive">

                          <table class="table table-inbox table-hover">
                            <tbody>
                            @foreach($messages as $k => $v)
                              <tr class="unread">     
                                  <td class="inbox-small-cells">@if($v->page != 'group')
                        <a href="{{route('amber.messages.list',$v->id)}}" class="btn btn-primary">view</a>
                    @else
                        <a href="{{route('amber.messages.grouplist',$v->id)}}" class="btn btn-primary">view</a>
                    @endif</td>
                                  <td class="view-message  dont-show">{{$v->subject}}</td>
                                  <td class="view-message ">{{json_encode($v->excerpt)}}...</td>
                                  <td class="view-message  text-right">{{$v->created_at}}</td>

                              </tr>
                              @endforeach
                              </tbody>
                              </table>

                              </div>
                              </div>
                              </div>

<!--     @foreach($messages as $k => $v)

        <div class="col-lg-12">
            <div class="card card-block">
                <div class="profile-timeline-header">
                    <a href="#" class="profile-timeline-user"><img src="images/avatar.jpg" alt="" class="img-rounded">
                    </a>
                    <div class="profile-timeline-user-details"><a href="#" class="bold">{{$v->subject}}</a>
                        <br><span class="text-muted small">{{$v->created_at}}</span>
                    </div>
                </div>
                <div class="profile-timeline-content">
                    <p>{{json_encode($v->excerpt)}}...</p>
                     @if($v->page != 'group')
                        <a href="{{route('amber.messages.list',$v->id)}}" class="btn btn-primary">view</a>
                    @else
                        <a href="{{route('amber.messages.grouplist',$v->id)}}" class="btn btn-primary">view</a>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
 <script>
function addRowHandlers() {
    var table = document.getElementById("tableId");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = 
            function(row) 
            {
                return function() { 
                                        var cell = row.getElementsByTagName("td")[0];
                                        var id = cell.innerHTML;
                                        
                                 };
            };

        currentRow.onclick = createClickHandler(currentRow);
    }
}
</script> -->

@endsection 

@section('footer-scripts')

@endsection