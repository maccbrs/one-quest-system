<?php $asset = URL::asset('/'); ?> 
@extends('amber.master')

@section('title', 'You')

@section('content')

<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-block">
            <div class="table-responsive">

               @if($dept == 'Administrator')
                  <?php $head = 'NOC'; ?>
               @endif

               @if($dept == 'Quality Assurance')
                  <?php $head = 'Quality Assurance';?>
               @endif

               @if($dept == 'Team Leader')
                  <?php $head = 'Team Leader'; ?>
               @endif

                @if($dept == 'Human Resource')
                  <?php $head = 'Human Resource'; ?>
               @endif

              @if($dept == 'Manager')
                  <?php $head = 'Manager'; ?>
               @endif

               @if($dept == 'Trainer')
                  <?php $head = 'Training'; ?>
               @endif


               <h2> {{ucfirst($head)}} Notes / Credentials</h2> <br><br>
               <div class="main-content">
                  <div class="content-view">
                     <div class="row">

                        @foreach( $note_list as $ctr =>$value)

                           <?php $counter = 0; ?>

                           @foreach( $value->type->type as $ctr2 =>$value2)

                              @if($value->status->status[$ctr2] == $dept)
                   
                                 <?php

                                    if($value->type->type[$ctr2] == 1){

                                       $type_hoder = "info";
                                    }

                                    if($value->type->type[$ctr2] == 2){

                                       $type_hoder = "primary";
                                    }

                                    if($value->type->type[$ctr2] == 3){

                                       $type_hoder = "success";
                                    }

                                    if($value->type->type[$ctr2] == 4){

                                       $type_hoder = "warning";
                                    }

                                    if($value->type->type[$ctr2] == 5){

                                       $type_hoder = "danger";
                                    }
                    
                                 ?>
                               
                                 <div class="col-lg-4 col-md-6">

                                    <div class="card card-{{$type_hoder}} card-inverse">

                                       <div class="card-header">{{ucfirst($value->header->subject[$counter])}}
                                          <div class="card-controls">
                                          </div>
                                       </div>

                                       <div class="card-block">

                                          <p class="card-text">{{ucfirst($value->content->content[$counter])}}</p>

                                       </div>

                                    </div>
                                 
                                 </div>

                                 <?php  $counter++  ?>

                              @endif

                           @endforeach

                         @endforeach

                     </div>

                  </div> 
               </div>

            </div>
         </div>
      </div>
   </div>
</div>



@endsection 

@section('footer-scripts')

@endsection