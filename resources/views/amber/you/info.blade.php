<?php $asset = URL::asset('/'); ?> 
@extends('amber.master')

@section('title', 'You')

@section('content')

<div class="row">
<div class="col-md-5">
      <div class="card">

         <div class="card-block">
            <div class="table-responsive">
               @if($self)
               <div class="column-equal m-b-2">
                  <div class="col p-l-2 text-xs-right" >
                     <span class="text-muted">First Name</span></div><div class="col p-l-2">
                     {{$self->first_name}}
                  </div>
                                   
               </div>

               <div class="column-equal m-b-2">
                  <div class="col p-l-2 text-xs-right" >
                     <span class="text-muted">Middle Name</span></div><div class="col p-l-2">
                     {{$self->middle_name}}
                  </div>   
               </div>

               <div class="column-equal m-b-2">
                  <div class="col p-l-2 text-xs-right" >
                     <span class="text-muted">Last Name</span></div><div class="col p-l-2">
                     {{$self->last_name}}
                  </div>
               </div>
               @endif
            </div>
         </div>
      </div>

</div>

</div>


@endsection 

@section('footer-scripts')

@endsection