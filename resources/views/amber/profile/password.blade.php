<button class="btn btn-default btn-sm" data-toggle="modal" data-target=".password-modal">
  <i class="material-icons" aria-hidden="true">edit </i>
</button></h4>
<div class="modal fade password-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('amber.profile.password')}}" enctype="multipart/form-data" novalidate>
        {{ csrf_field() }}   

        <input type="hidden" name="dept_to" value="">
        <input type="hidden" name="type" value="1">                 

        <div class="modal-body">
          <div class = "row">
            <div class="form-group">
              <div class="col-xs-4">
                <label  >Current Password</label>
              </div>
              <div class="col-xs-8">
                <input type="text" name="current_password" value="" class="form-control ">   
              </div>
            </div> 
           </div>

          <div class = "row">
            <div class="form-group">
              <div class="col-xs-4">
                <label  >New Password</label>
              </div>
              <div class="col-xs-8">
                <input type="text" name="new_password1" value="" class="form-control newpass1">   
              </div>
            </div> 
           </div>

          <div class = "row">
            <div class="form-group">
              <div class="col-xs-4">
                <label  >Repeat Password</label>
              </div>
              <div class="col-xs-8">
                <input type="text" name="new_password2" value="" class="form-control newpass2">   
              </div>
            </div> 
           </div>

          </div>       

          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary updatebutton">Update</button>
          </div>

        </form>
      </div>
  </div>
</div> 