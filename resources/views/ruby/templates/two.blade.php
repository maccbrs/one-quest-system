<?php $asset = URL::asset('/'); ?>

@extends('gem.master2')


@section('title', 'QA tools')
 @section('top-navigation')

@endsection @section('content')

<h2>Web Audio Api Equalizer</h2>

<audio id="player" controls crossorigin>
  <source src="http://api.audiotool.com/track/volution/play.mp3" type="audio/mpeg">
  <source src="http://api.audiotool.com/track/volution/play.ogg" type="audio/ogg">
</audio>



<script>


var context = new AudioContext();
var mediaElement = document.getElementById('player');
var sourceNode = context.createMediaElementSource(mediaElement);

var gainDb = -40.0;
var bandSplit = [360,3600];

var hBand = context.createBiquadFilter();
hBand.type = "lowshelf";
hBand.frequency.value = bandSplit[0];
hBand.gain.value = gainDb;

var hInvert = context.createGain();
hInvert.gain.value = -1.0;

var mBand = context.createGain();

var lBand = context.createBiquadFilter();
lBand.type = "highshelf";
lBand.frequency.value = bandSplit[1];
lBand.gain.value = gainDb;

var lInvert = context.createGain();
lInvert.gain.value = -1.0;

sourceNode.connect(lBand);
sourceNode.connect(mBand);
sourceNode.connect(hBand);

hBand.connect(hInvert);
lBand.connect(lInvert);

hInvert.connect(mBand);
lInvert.connect(mBand);

var lGain = context.createGain();
var mGain = context.createGain();
var hGain = context.createGain();

lBand.connect(lGain);
mBand.connect(mGain);
hBand.connect(hGain);

var sum = context.createGain();
lGain.connect(sum);
mGain.connect(sum);
hGain.connect(sum);
sum.connect(context.destination);


</script>


@endsection @section('header-scripts') 

@endsection @section('footer-scripts') 

@endsection