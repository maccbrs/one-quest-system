<?php $asset = URL::asset('/'); ?>

@extends('gem.master2')

<style>

  .single{

    width:33%;
    float: left;
    margin-right: 3px;

  }

</style>

<link rel="stylesheet" href="{{$asset}}rubyjs/soundmanager/css/index-rollup.css"> 
@section('title', 'QA tools')
 @section('top-navigation')
<div class="header-inner">
    <div class="navbar-item navbar-spacer-right brand hidden-lg-up"> <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen"><i class="material-icons">menu</i> </a>
        <a class="brand-logo hidden-xs-down"><img src="{{$asset}}milestone/images/logo_white.png" alt="logo"> </a>
    </div>

    <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#"><span>Settings</span></a>

    <div class="navbar-item nav navbar-nav">

        <div class="nav-item nav-link">

        </div>

        <div class="nav-item nav-link dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><span>English</span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="javascript:;">English</a> <a class="dropdown-item" href="javascript:;">Russian</a></div>
        </div>

        <div class="nav-item nav-link dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">notifications</i> <span class="tag tag-danger">4</span></a>
            <div class="dropdown-menu dropdown-menu-right notifications">
                <div class="dropdown-item">
                    <div class="notifications-wrapper">
                        <ul class="notifications-list">
                            <li>
                                <a href="javascript:;">
                                    <div class="notification-icon">
                                        <div class="circle-icon bg-success text-white"><i class="material-icons">arrow_upward</i></div>
                                    </div>
                                    <div class="notification-message"><b>Sean</b> launched a new application <span class="time">2 seconds ago</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <div class="notification-icon">
                                        <div class="circle-icon bg-danger text-white"><i class="material-icons">check</i></div>
                                    </div>
                                    <div class="notification-message"><b>Removed calendar</b> from app list <span class="time">4 hours ago</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="notification-icon"><span class="circle-icon bg-info text-white">J</span></span>
                                    <div class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list <span class="time">9 days ago</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="notification-icon"><span class="circle-icon bg-primary text-white">C</span></span>
                                    <div class="notification-message"><b>Conan Johns</b> created a new list <span class="time">9 days ago</span></div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="notification-footer">Notifications</div>
                </div>
            </div>
        </div>
        <a href="javascript:;" class="nav-item nav-link nav-link-icon" data-toggle="modal" data-target=".chat-panel" data-backdrop="false"><i class="material-icons">chat_bubble</i></a>
    </div>
</div>
@endsection @section('content')

<h1>Audio Page</h1>

<br>

<div class="row">
 
    <div class="card card-block multiple">
     <div class="col-md-4"><h2>Campaign Name</h2></div>
     <p class="card-text"> October 3, 2016 15:21 - October 4, 2016 15:21 ) </p>
    <div class="col-md-8">
      <div class="sm2-bar-ui" style="min-width:10em">
          <div class="bd sm2-main-controls">

              <div class="sm2-inline-texture"></div>
              <div class="sm2-inline-gradient"></div>

              <div class="sm2-inline-element sm2-button-element">
                  <div class="sm2-button-bd">
                      <a href="#play" class="sm2-inline-button play-pause">Play / pause</a>
                  </div>
              </div>

              <div class="sm2-inline-element sm2-inline-status">

                  <div class="sm2-playlist">
                      <div class="sm2-playlist-target">
                          <!-- playlist <ul> + <li> markup will be injected here -->
                          <!-- if you want default / non-JS content, you can put that here. -->
                          <noscript><p>JavaScript is required.</p></noscript>
                      </div>
                  </div>

                  <div class="sm2-progress">
                      <div class="sm2-row">
                          <div class="sm2-inline-time">0:00</div>
                          <div class="sm2-progress-bd">
                              <div class="sm2-progress-track">
                                  <div class="sm2-progress-bar"></div>
                                  <div class="sm2-progress-ball">
                                      <div class="icon-overlay"></div>
                                  </div>
                              </div>
                          </div>
                          <div class="sm2-inline-duration">0:00</div>
                      </div>
                  </div>

              </div>

              <div class="sm2-inline-element sm2-button-element sm2-volume">
                  <div class="sm2-button-bd">
                      <span class="sm2-inline-button sm2-volume-control volume-shade"></span>
                      <a href="#volume" class="sm2-inline-button sm2-volume-control">volume</a>
                  </div>
              </div>

              <div class="sm2-inline-element sm2-button-element sm2-menu">
                  <div class="sm2-button-bd">
                      <a href="#menu" class="sm2-inline-button menu">menu</a>
                  </div>
              </div>
          </div>
          <div class="bd sm2-playlist-drawer sm2-element">

              <div class="sm2-inline-texture">
                  <div class="sm2-box-shadow"></div>
              </div>

              <!-- playlist content is mirrored here -->

              <div class="sm2-playlist-wrapper">
                  <ul class="sm2-playlist-bd">
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160611-224734_9124018985-all.mp3" class="exclude button-exclude inline-exclude">SonReal - LA (Prod. Chin Injetti)<span class="label">Explicit</span></a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160612-020027_KABAYAN_2037470999-all.mp3" class="exclude button-exclude inline-exclude"><b>SonReal</b> - Let Me <span class="label">Explicit</span></a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160611-174516_9402328124-all.mp3" class="exclude button-exclude inline-exclude"><b>SonReal</b> - People Asking <span class="label">Explicit</span></a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160612-090033_KABAYAN_02036975736-all.mp3" class="exclude button-exclude inline-exclude"><b>SonReal</b> - Already There Remix ft. Rich Kidd, Saukrates <span class="label">Explicit</span></a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160611-180557_6197514088-all.mp3" class="exclude button-exclude inline-exclude"><b>The Fugitives</b> - Graffiti Sex</a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160612-090619_KABAYAN_7441907227-all.mp3" class="exclude button-exclude inline-exclude"><b>Adrian Glynn</b> - Seven Or Eight Days</a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160611-182512_8664344101-all.mp3" class="exclude button-exclude inline-exclude"><b>SonReal</b> - I Tried</a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160611-183205_8052940997-all.mp3" class="exclude button-exclude inline-exclude">32" Gong Sounds (rubber + standard mallets)</a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160611-185601_8664344101-all.mp3" class="exclude button-exclude inline-exclude">Armstrong Beat</a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160612-020027_KABAYAN_2037470999-all.mp3" class="exclude button-exclude inline-exclude">Untitled Groove</a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160611-190239_7082507596-all.mp3" class="exclude button-exclude inline-exclude">Birds In Kaua'i (AAC)</a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160611-191926_3609366406-all.mp3" class="exclude button-exclude inline-exclude">Po'ipu Beach Waves (OGG)</a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160611-193004_8052940997-all.mp3" class="exclude button-exclude inline-exclude">A corked beer bottle (WAV)</a></li>
                      <li><a href="http://192.168.200.100/archive/2016-06-12/20160611-193829_8664344101-all.mp3" class="norewrite exclude button-exclude inline-exclude">Rain</a></li>
                  </ul>
              </div>

              <div class="sm2-extra-controls">

                  <div class="bd">

                      <div class="sm2-inline-element sm2-button-element">
                          <a href="#prev" title="Previous" class="sm2-inline-button previous">&lt; previous</a>
                      </div>

                      <div class="sm2-inline-element sm2-button-element">
                          <a href="#next" title="Next" class="sm2-inline-button next">&gt; next</a>
                      </div>

                      <div class="sm2-inline-element sm2-button-element disabled">
                          <a href="#repeat" title="Repeat playlist" class="sm2-inline-button repeat">&infin; repeat</a>
                      </div>

                      <div class="sm2-inline-element sm2-button-element disabled">
                          <a href="#shuffle" title="Shuffle" class="sm2-inline-button shuffle">shuffle</a>
                      </div>

                  </div>

              </div>
          </div>
      </div> 
    </div>
    <br>
  </div>
</div>


  <div class="card card-block single" >
    


        <div id="special-demo-right">
          <a  href="http://192.168.200.100/archive/2016-06-12/20160611-193829_8664344101-all.mp3" title="Play &quot;Change&quot;" class="sm2_button exclude inline-exclude norewrite"></a>
          &nbsp;&nbsp;  Peter
        </div>


 
  <div class="card card-block single">
    <h4 class="card-title">Peter Santos</h4>
    <div class="ui360">
        <a href="http://192.168.200.100/archive/2016-06-12/20160611-193829_8664344101-all.mp3" class="norewrite exclude button-exclude inline-exclude">&nbsp;</a> 
       <p class="card-text"> Centennial Mortgage ( Call Date : 12:26 ) </p>
    </div>  
  </div>

    <div class="card card-block single">
      <h4 class="card-title">Anna Macalindol</h4>
      <div class="ui360">
          <a href="http://192.168.200.100/archive/2016-06-12/20160611-193829_8664344101-all.mp3" class="norewrite exclude button-exclude inline-exclude">&nbsp;</a> 
         <p class="card-text"> Centennial Mortgage ( Call Date : 10:21 ) </p>
      </div>  
    </div>   

    <div class="card card-block single" >
    <h4 class="card-title">Juan De la Cruz</h4>
    <div class="ui360">
        <a href="http://192.168.200.100/archive/2016-06-12/20160611-193829_8664344101-all.mp3" class="norewrite exclude button-exclude inline-exclude">&nbsp;</a> 
       <p class="card-text"> Prescripto ( Call Date : 15:21 ) </p>
    </div>
  </div>
 
  <div class="card card-block single">
    <h4 class="card-title">Peter Santos</h4>
    <div class="ui360">
        <a href="http://192.168.200.100/archive/2016-06-12/20160611-193829_8664344101-all.mp3" class="norewrite exclude button-exclude inline-exclude">&nbsp;</a> 
       <p class="card-text"> Centennial Mortgage ( Call Date : 12:26 ) </p>
    </div>  
  </div>

    <div class="card card-block single">
      <h4 class="card-title">Anna Macalindol</h4>
      <div class="ui360">
          <a href="http://192.168.200.100/archive/2016-06-12/20160611-193829_8664344101-all.mp3" class="norewrite exclude button-exclude inline-exclude">&nbsp;</a> 
         <p class="card-text"> Centennial Mortgage ( Call Date : 10:21 ) </p>
      </div>  
    </div>

    <div class="card card-block single" >
    <h4 class="card-title">Juan De la Cruz</h4>
    <div class="ui360">
        <a href="http://192.168.200.100/archive/2016-06-12/20160611-193829_8664344101-all.mp3" class="norewrite exclude button-exclude inline-exclude">&nbsp;</a> 
       <p class="card-text"> Prescripto ( Call Date : 15:21 ) </p>
    </div>
  </div>
 
  <div class="card card-block single">
    <h4 class="card-title">Peter Santos</h4>
    <div class="ui360">
        <a href="http://192.168.200.100/archive/2016-06-12/20160611-193829_8664344101-all.mp3" class="norewrite exclude button-exclude inline-exclude">&nbsp;</a> 
       <p class="card-text"> Centennial Mortgage ( Call Date : 12:26 ) </p>
    </div>  
  </div>

    <div class="card card-block single">
      <h4 class="card-title">Anna Macalindol</h4>
      <div class="ui360">
          <a href="http://192.168.200.100/archive/2016-06-12/20160611-193829_8664344101-all.mp3" class="norewrite exclude button-exclude inline-exclude">&nbsp;</a> 
         <p class="card-text"> Centennial Mortgage ( Call Date : 10:21 ) </p>
      </div>  
    </div>
  </div>
  
<script src="{{$asset}}rubyjs/soundmanager/js/soundmanager2-jsmin.js"></script>
<script src="{{$asset}}rubyjs/soundmanager/js/soundmanager2-nodebug-jsmin.js"></script>
<script src="{{$asset}}rubyjs/soundmanager/js/soundmanager2-nodebug.js"></script>
<script src="{{$asset}}rubyjs/soundmanager/js/soundmanager2.js"></script>
<script src="{{$asset}}rubyjs/soundmanager/js/index-rollup.js"></script>
<script src="{{$asset}}rubyjs/soundmanager/js/bar-ui.js"></script>

@endsection @section('header-scripts') 

@endsection @section('footer-scripts') 

@endsection