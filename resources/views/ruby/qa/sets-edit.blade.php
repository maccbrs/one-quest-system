<?php $asset = URL::asset('/'); ?> 

<style>

  .bound-group{
    margin: 10px;
  }

  .radio{
    text-align: center;
  }

</style>

@extends('gem.master2')

@section('title', 'QA tools') 

@section('top-navigation')
   <div class="header-inner">
      <div class="navbar-item navbar-spacer-right brand hidden-lg-up">  <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen"><i class="material-icons">menu</i> </a>   <a class="brand-logo hidden-xs-down"><img src="{{$asset}}milestone/images/logo_white.png" alt="logo"> </a> </div>
      
      <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#"><span>Settings Accnt</span></a>
     
      <div class="navbar-item nav navbar-nav">

         <div class="nav-item nav-link">

         </div>

         <div class="nav-item nav-link dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><span>English</span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="javascript:;">English</a> <a class="dropdown-item" href="javascript:;">Russian</a></div>
         </div>

         <div class="nav-item nav-link dropdown"> 
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">notifications</i> <span class="tag tag-danger">4</span></a>
            <div class="dropdown-menu dropdown-menu-right notifications">
               <div class="dropdown-item">
                  <div class="notifications-wrapper">
                     <ul class="notifications-list">
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-success text-white"><i class="material-icons">arrow_upward</i></div>
                              </div>
                              <div class="notification-message"><b>Sean</b> launched a new application <span class="time">2 seconds ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-danger text-white"><i class="material-icons">check</i></div>
                              </div>
                              <div class="notification-message"><b>Removed calendar</b> from app list <span class="time">4 hours ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-info text-white">J</span></span>
                              <div class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-primary text-white">C</span></span>
                              <div class="notification-message"><b>Conan Johns</b> created a new list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                     </ul>
                  </div>
                  <div class="notification-footer">Notifications</div>
               </div>
            </div>
         </div>
         <a href="javascript:;" class="nav-item nav-link nav-link-icon" data-toggle="modal" data-target=".chat-panel" data-backdrop="false"><i class="material-icons">chat_bubble</i></a>
      </div>                  
   </div>
@endsection

@section('content')

 <div class="row">
    <div class="col-md-12">
      <div class="card">
          <div class="card-block">
            <h5>Ingroups &nbsp;&nbsp; <button type="button" class="btn btn-primary m-r-xs m-b-xs editIngroups">Edit</button>  </h5>
            <p id="ingroup-value-x">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              @foreach($ingroup as $key => $value)
                <span class="tag tag-pill tag-info">
                  {{$value}}
                </span>
              @endforeach
              </p>
        </div>
      </div>
    </div> 
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="card">
          <div class="card-block">
            <h5>Bound</h5>
              <?php $bounds = $help->bounds($row->bound); ?>
            <div class="radio">
              @foreach($bounds as $k => $v)
                <label class = "bound-group"><input type="radio" name="bound" class="formdata-bound" value="{{$k}}" {{($v?'checked="checked"':'')}}>&nbsp;&nbsp;{{ucfirst($k)}}</label>
              @endforeach
            </div>
        </div>
      </div>
    </div> 

    <div class="col-md-6">
        <div class="card">
          <div class="card-block">
            <h5>Frequency</h5>
              <div class="radio">
                  <label class ="bound-group"><input type="radio" name="frequency" class="formdata-frequency" value="monthly" {{($row->frequency == 'monthly'?'checked="checked"':'')}} >Monthly</label>
                  <label class ="bound-group"><input type="radio" name="frequency" class="formdata-frequency" value="weekly" {{($row->frequency == 'weekly'?'checked="checked"':'')}} >Weekly</label>
                  <label class ="bound-group"><input type="radio" name="frequency" class="formdata-frequency" value="daily" {{($row->frequency == 'daily'?'checked="checked"':'')}} >Daily</label>
              </div>
          </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card">
          <div class="card-block">
              <h5>Agents &nbsp;&nbsp; <button type="button" class="btn btn-primary m-r-xs m-b-xs editAgents">Edit</button></h5>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <p id="agents-value-x">
              @foreach($agent as $key2 => $value2)
                <span class="tag tag-pill tag-info">
                  {{$value2}}
                </span>
              @endforeach
              </p>
        
        </div>
      </div>
    </div> 
  </div>

  <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-block">
            <h5>Dispositions &nbsp;&nbsp;<button type="button" class="btn btn-primary m-r-xs m-b-xs editDispositions">Edit</button></h5>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <p id="dispo-value-x">
               @foreach($dispo as $key4 => $value4)
                <span class="tag tag-pill tag-info">
                  {{$value4}}
                </span>
               @endforeach
               </p>
               
          </div>
        </div>
      </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-block">
          <h5>Call Details</h5>
            <div class="row">
              <div class="col-md-6">
                <label>Call Counts</label>
                <input class="form-control" id="exampleInputEmail1"  type="text" name="call_count" value="{{$row->call_count}}">
              </div>
              <div class="col-md-6">
                <label>Time to Generate</label>
                <input class="form-control" type="time" name="generate_time" value="{{$row->time_generate}}"> 
              </div>    
            </div> 
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="card">
        <div class="card-block">
          <h5>Call Duration(mins)</h5>
            <div class="row">
              <div class="col-md-6">
                <label>min</label>
                <input class="form-control" type="text" name="call_min" value="{{$row->duration_min}}">
              </div>
              <div class="col-md-6">
                <label>max</label>
                <input class="form-control" type="text" name="call_max" value="{{$row->duration_max}}"> 
              </div>    
            </div> 
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card">
            <div class="card-block">
                <h5>Auditor / QA Staff &nbsp;&nbsp; <button type="button" class="btn btn-primary m-r-xs m-b-xs editAuditor">Edit</button></h5>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <p id="auditor-value-x">
                @foreach($auditor as $key3 => $value3)
                <span class="tag tag-pill tag-info">
                  {{$help->auditorFormat($value3)}}
                </span>
                @endforeach
               </p>
          
          </div>
        </div>
      </div> 
    </div>

<div class="modal fade editIngroupsModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" id="editIngroupsModal-container">
               
               <div class="card-block">
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Select bound</label>
                       <div class="col-xs-10">
                          <fieldset class="form-group">
                              <select class="form-control selectbound">
                                  <option value="">select</option>
                                  <option value="inbound">inbound</option>
                                  <option value="outbound">outbound</option>
                              </select>
                          </fieldset>
                       </div>
                   </div>

                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Select Ingroups</label> 
                       <div class="col-xs-10" id="template-x">
                           
                       </div>
                   </div>   

               </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary selectInbound-formdata-submit">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade editAgentsModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" id="editAgentsModal-container">
               
               <div class="card-block">

                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Select Agents</label>
                       <div class="col-xs-10" id="template2-x">
                           
                       </div>
                   </div>   

               </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary agents-formdata-submit">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade editAuditorModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" id="editAuditorModal-container">
               
               <div class="card-block">

                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Select Auditors</label>
                       <div class="col-xs-10" id="template3-x">
                           
                       </div>
                   </div>   

               </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary auditor-formdata-submit">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- editDispositions -->
<div class="modal fade editDispositionsModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" id="editDispositionsModal-container">
               
               <div class="card-block">
                   <div class="form-group row">
                        <label class="col-xs-2 col-form-label">Select Campaign</label>
                        <select class="form-control" id="template-select-campaign-x">
                            <option>select</option>
                        </select>
                   </div>  
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Select Dispositions</label>
                       <div class="col-xs-10" id="template4-x">
                           
                       </div>
                   </div>   

               </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary dispo-formdata-submit">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')
<script src="{{$asset}}custom/js/ejs.js"></script>
<script type="text/javascript">
var socket = io.connect('http://{{config("app.ruby")}}',{ query: "user=1" });
var base = "{{url('ruby/')}}";
var accntId = "{{$id}}";
</script>
<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script> 
<script src="{{$asset}}socketio/ruby/ruby-settings-set-edit.js"></script> 


<script id="template-y" type="text/template">
   <%for(var i in rows){%>
      <label class="checkbox-inline"><input type="checkbox" class="formdata-ingroups" value="<%=rows[i]%>"><%=rows[i]%></label>
    <%}%>
</script>

<script id="template2-y" type="text/template">
   <%for(var i in rows){%>
      <label class="checkbox-inline"><input type="checkbox" class="formdata-agents" value="<%=rows[i].user%>"><%=rows[i].full_name%></label>
    <%}%>
</script>
<script id="template3-y" type="text/template">
   <%for(var i in rows){%>
      <label class="checkbox-inline"><input type="radio" class="formdata-auditor" name="auditor" value="<%=rows[i].id%>_<%=rows[i].name%>"><%=rows[i].name%></label>
    <%}%>
</script>
<script id="template4-y" type="text/template">
   <%for(var i in rows){%>
      <label class="checkbox-inline"><input type="checkbox" class="formdata-dispo" value="<%=rows[i].status%>"><%=rows[i].status%></label>
    <%}%>
</script>
<script id="template-select-campaign-y" type="text/template">
   <%for(var i in rows){%>
      <option value="<%=rows[i].campaign_id%>"><%=rows[i].campaign_id%></option>
    <%}%>
</script>
@endsection
