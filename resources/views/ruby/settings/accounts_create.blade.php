<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'QA tools') 


@section('top-navigation')

   <div class="header-inner">
      <div class="navbar-item navbar-spacer-right brand hidden-lg-up">  <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen"><i class="material-icons">menu</i> </a>   <a class="brand-logo hidden-xs-down"><img src="{{$asset}}milestone/images/logo_white.png" alt="logo"> </a> </div>
      
      <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#"><span>Dashboard</span></a>
     
      <div class="navbar-item nav navbar-nav">

         <div class="nav-item nav-link">

         </div>

         <div class="nav-item nav-link dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><span>English</span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="javascript:;">English</a> <a class="dropdown-item" href="javascript:;">Russian</a></div>
         </div>

         <div class="nav-item nav-link dropdown"> 
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">notifications</i> <span class="tag tag-danger">4</span></a>
            <div class="dropdown-menu dropdown-menu-right notifications">
               <div class="dropdown-item">
                  <div class="notifications-wrapper">
                     <ul class="notifications-list">
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-success text-white"><i class="material-icons">arrow_upward</i></div>
                              </div>
                              <div class="notification-message"><b>Sean</b> launched a new application <span class="time">2 seconds ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-danger text-white"><i class="material-icons">check</i></div>
                              </div>
                              <div class="notification-message"><b>Removed calendar</b> from app list <span class="time">4 hours ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-info text-white">J</span></span>
                              <div class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-primary text-white">C</span></span>
                              <div class="notification-message"><b>Conan Johns</b> created a new list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                     </ul>
                  </div>
                  <div class="notification-footer">Notifications</div>
               </div>
            </div>
         </div>
         <a href="javascript:;" class="nav-item nav-link nav-link-icon" data-toggle="modal" data-target=".chat-panel" data-backdrop="false"><i class="material-icons">chat_bubble</i></a>
      </div>                  
   </div>
@endsection

@section('content')


 <div class="row">
    <div class="col-md-12">
          <div class="card">
              <div class="card-block"  >
                  <div class="table-responsive" >

                      <form method="post" action="{{route('ruby.settings.accounts_post')}}" novalidate>
                        {{ csrf_field() }}
                        <div class="row">

                          <div class="col-md-12">
                                <fieldset class="form-group">
                                    <label for="forTitle">Title</label>
                                    <input type="text" class="form-control" name="title" id="forTitle">
                                    <p>Do not mixed up Inbound and Outbound.</p> 
                                    <h6 class="m-t-1">Select Bound</h6>
                                    <select class="custom-select" name="bound">
                                        <option value="inbound">Inbound</option>
                                        <option value="outbound">Outbound</option>
                                    </select>                                                                     
                                </fieldset>                              
                          </div>

                          <div class="col-md-6">
                                  <h3>Inbound</h3>
                                  @foreach($closer as $c)
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="inbounds[]" value="{{$c->campaign_id}}">{{$c->campaign_id}}
                                    </label>
                                  @endforeach   
                          </div>
                          <div class="col-md-6">
                                  <h3>Outbound</h3>
                                  @foreach($recordings as $r)
                                    @if($r->campaign_id)
                                      <label class="checkbox-inline">
                                          <input type="checkbox" name="outbounds[]" value="{{$r->campaign_id}}">{{$r->campaign_id}}
                                      </label>
                                    @endif
                                  @endforeach   
                          </div>

                          <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Go</button>
                          </div>
                        </div>
                      </form>

                  </div>
              </div>
          </div>
    </div>
</div>

@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')
@endsection
