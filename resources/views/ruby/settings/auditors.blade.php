<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'QA tools') 


@section('top-navigation')
   <div class="header-inner">
      <div class="navbar-item navbar-spacer-right brand hidden-lg-up">  <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen"><i class="material-icons">menu</i> </a>   <a class="brand-logo hidden-xs-down"><img src="{{$asset}}milestone/images/logo_white.png" alt="logo"> </a> </div>
      
      <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#"><span>Settings Accnt</span></a>
     
      <div class="navbar-item nav navbar-nav">

         <div class="nav-item nav-link">
          <button type="button" class="btn btn-primary btn-sm addNewAuditor">Add New Auditor</button>
         </div>

         <div class="nav-item nav-link dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><span>English</span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="javascript:;">English</a> <a class="dropdown-item" href="javascript:;">Russian</a></div>
         </div>

         <div class="nav-item nav-link dropdown"> 
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">notifications</i> <span class="tag tag-danger">4</span></a>
            <div class="dropdown-menu dropdown-menu-right notifications">
               <div class="dropdown-item">
                  <div class="notifications-wrapper">
                     <ul class="notifications-list">
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-success text-white"><i class="material-icons">arrow_upward</i></div>
                              </div>
                              <div class="notification-message"><b>Sean</b> launched a new application <span class="time">2 seconds ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-danger text-white"><i class="material-icons">check</i></div>
                              </div>
                              <div class="notification-message"><b>Removed calendar</b> from app list <span class="time">4 hours ago</span></div>
                           </a>
                        </li> 
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-info text-white">J</span></span>
                              <div class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-primary text-white">C</span></span>
                              <div class="notification-message"><b>Conan Johns</b> created a new list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                     </ul>
                  </div>
                  <div class="notification-footer">Notifications</div>
               </div>
            </div>
         </div>
         <a href="javascript:;" class="nav-item nav-link nav-link-icon" data-toggle="modal" data-target=".chat-panel" data-backdrop="false"><i class="material-icons">chat_bubble</i></a>
      </div>                  
   </div>
@endsection

@section('content')

 <div class="row">
    <div class="col-md-12">

          <div class="card">
              <div class="card-header no-bg b-a-0">Sets</div>
              <div class="card-block">
                  <div class="table-responsive" id="auditor-list">

                  </div>
              </div>
          </div>
        </div>
    </div>


<div class="modal fade addNewAuditorModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" id="addNewAuditorModal-container">
               
               <div class="card-block">

                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Select User</label>
                       <div class="col-xs-10" id="template-x">

                       </div> 
                   </div>
 

               </div>

            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary formdata-submit">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')
<script src="{{$asset}}custom/js/ejs.js"></script>
<script type="text/javascript">
var socket = io.connect('http://{{config("app.ruby")}}',{ query: "user=1" });
var base = "{{url('ruby/')}}";
var baseUrl = "{{url('/')}}";
</script>
<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>
<script src="{{$asset}}rubyjs/settings/auditors.js"></script> 



<script id="template-y" type="text/template">
   <%for(var i in rows){%>
      <label class="checkbox-inline"><input type="radio" class="formdata-users" name="user" value="<%=rows[i].id%>"><%=rows[i].name%></label>
    <%}%>
</script>

@endsection