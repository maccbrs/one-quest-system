<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'QA tools') 


@section('top-navigation')
   <div class="header-inner">
      <div class="navbar-item navbar-spacer-right brand hidden-lg-up">  <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen"><i class="material-icons">menu</i> </a>   <a class="brand-logo hidden-xs-down"><img src="{{$asset}}milestone/images/logo_white.png" alt="logo"> </a> </div>
      
      <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#"><span>Search</span></a>
     
      <div class="navbar-item nav navbar-nav">

         <div class="nav-item nav-link">

         </div>

         <div class="nav-item nav-link dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><span>English</span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="javascript:;">English</a> <a class="dropdown-item" href="javascript:;">Russian</a></div>
         </div>

         <div class="nav-item nav-link dropdown"> 
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">notifications</i> <span class="tag tag-danger">4</span></a>
            <div class="dropdown-menu dropdown-menu-right notifications">
               <div class="dropdown-item">
                  <div class="notifications-wrapper">
                     <ul class="notifications-list">
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-success text-white"><i class="material-icons">arrow_upward</i></div>
                              </div>
                              <div class="notification-message"><b>Sean</b> launched a new application <span class="time">2 seconds ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-danger text-white"><i class="material-icons">check</i></div>
                              </div>
                              <div class="notification-message"><b>Removed calendar</b> from app list <span class="time">4 hours ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-info text-white">J</span></span>
                              <div class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-primary text-white">C</span></span>
                              <div class="notification-message"><b>Conan Johns</b> created a new list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                     </ul>
                  </div>
                  <div class="notification-footer">Notifications</div>
               </div>
            </div>
         </div>
         <a href="javascript:;" class="nav-item nav-link nav-link-icon" data-toggle="modal" data-target=".chat-panel" data-backdrop="false"><i class="material-icons">chat_bubble</i></a>
      </div>                  
   </div>
@endsection

@section('content')


  <script type="text/javascript" src="{{$asset}}bootstrap-daterangepicker/jquery.min.js"></script>
  <script type="text/javascript" src="{{$asset}}bootstrap-daterangepicker/moment.min.js"></script>

  <script type="text/javascript" src="{{$asset}}bootstrap-daterangepicker/daterangepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="{{$asset}}bootstrap-daterangepicker/daterangepicker.css" />

<div class="row">
    <div class="col-md-12">

          <div class="card">

              <div class="card-header no-bg b-a-0">
                <div class="row">
                  <form method="post" action="{{route('ruby.audit.search')}}" novalidate>
                      {{ csrf_field() }} 

                      <div class="col-md-4">
                        <fieldset class="form-group">
                          <input type="text" class="form-control" name="keyword" placeholder="Enter keyword"> <small class="text-muted">keyword</small>
                        </fieldset>
                      </div>

                      <div class="col-md-4">
                        <fieldset class="form-group">
                          <div class="input-prepend input-group m-b-1"><span class="add-on input-group-addon"><i class="material-icons">date_range </i></span>
                            <input type="text" name="daterange" class="form-control drp" value="" placeholder="Date range picker">
                          </div>
                        </fieldset>
                      </div>

                      <div class="col-md-3">
                          <fieldset class="form-group">
                              <select class="form-control" name="bound">
                                  <option value="inbound">Inbound</option>
                                  <option value="outbound">Outbound</option>
                              </select>
                              <small class="text-muted">Bound</small>
                          </fieldset>
                      </div> 
                      <div class="col-md-1">
                            <button type="submit" class="btn btn-primary">Search</button>
                      </div> 
                  </form>                
                </div>
              </div>
              <div class="card-block" >

                @if(isset($result))
                  <div class="table-responsive" id="auditList">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th>Agent</th>
                                <th>Call Date</th>
                                <th>Campaign Id</th>
                                <th>Call Length</th>
                                <th>Dispo</th>
                                <th>Listen</th>
                                <th>phone</th>
                                <th>locked</th>
                                <th>score</th>
                                <th>Auditor</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                          @if($result->count())
                            @foreach($result as $c)
                            <tr >
                                <td>{{$c->agent}}</td>
                                <td>{{$c->call_date}}</td>
                                <td>{{$c->campaign_id}}</td>
                                <td>{{$c->length_in_min}}</td>
                                <td>{{$c->status}}</td>
                                <td >

                                <audio id="player{{$c->id}}" src="{{$c->location}}" preload='false'></audio>
                                <div class="btn-group m-r-xs m-b-xs">
                                  <?php $playid = '"player'.$c->id.'"'; ?>
                                  <button class="btn btn-danger btn-sm m-r-xs" onclick="document.getElementById({{$playid}}).play()">Play</button>
                                  <button class="btn btn-danger btn-sm m-r-xs" onclick="document.getElementById({{$playid}}).pause()">Pause</button>
                                </div>                                
                                </td> 
                                <td>{{$c->phone}} </td>
                                <td>{{($c->locked == 0) ? 'no' : 'yes'}} </td>
                                <td>{{$c->score}} </td>
                                <td>{{$help->evaluator($c->evaluator)}}</td>
                                <td class="tbllink" data-link="{{route('ruby.audit.index',$c->id)}}?bound=inbound&account=1" > <!-- //{base+"/audit/"+list.id+"?bound="+props.data.bound+"&account="+props.data.account_id} -->
                                  <a href='#' class="editable editable-click">Audit</a>
                                </td>
                            </tr> 
                            @endforeach
                          @endif                         
                        </tbody>
                    </table>

                     @include('pagination.default', ['paginator' => $result])
                  </div>           
                @endif
              </div>
            </div>
          </div>
      </div>

  <script>

    $('input[name="daterange"]').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        }
    },
    function(start, end, label) {
        // alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });

  </script>

@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>



@endsection