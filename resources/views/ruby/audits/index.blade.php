<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'QA tools') 


@section('top-navigation')

   <div class="header-inner">
      <div class="navbar-item navbar-spacer-right brand hidden-lg-up">  <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen"><i class="material-icons">menu</i> </a>   <a class="brand-logo hidden-xs-down"><img src="{{$asset}}milestone/images/logo_white.png" alt="logo"> </a> </div>
      
      <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#"><span>Dashboard</span></a>
     
      <div class="navbar-item nav navbar-nav">

         <div class="nav-item nav-link">

         </div>

         <div class="nav-item nav-link dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><span>English</span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="javascript:;">English</a> <a class="dropdown-item" href="javascript:;">Russian</a></div>
         </div>

         <div class="nav-item nav-link dropdown"> 
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">notifications</i> <span class="tag tag-danger">4</span></a>
            <div class="dropdown-menu dropdown-menu-right notifications">
               <div class="dropdown-item">
                  <div class="notifications-wrapper">
                     <ul class="notifications-list">
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-success text-white"><i class="material-icons">arrow_upward</i></div>
                              </div>
                              <div class="notification-message"><b>Sean</b> launched a new application <span class="time">2 seconds ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-danger text-white"><i class="material-icons">check</i></div>
                              </div>
                              <div class="notification-message"><b>Removed calendar</b> from app list <span class="time">4 hours ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-info text-white">J</span></span>
                              <div class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-primary text-white">C</span></span>
                              <div class="notification-message"><b>Conan Johns</b> created a new list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                     </ul>
                  </div>
                  <div class="notification-footer">Notifications</div>
               </div>
            </div>
         </div>
         <a href="javascript:;" class="nav-item nav-link nav-link-icon" data-toggle="modal" data-target=".chat-panel" data-backdrop="false"><i class="material-icons">chat_bubble</i></a>
      </div>                  
   </div>
@endsection

@section('content')

 <div class="row">
    <div class="col-md-12">
          <div class="card">
              <div class="card-header no-bg b-a-0">Set Detail</div>  
              <div class="card-block" id="setInfo" >
                  <div id="row"> 
                    <table class = 'audittable'>
                      <thead></thead>
                      <tbody>
                      <tr>
                        <th> Title : </th>
                        <td>{{$audit->set->title}}</td>
                        <th> Call duration (min):</th>
                        <td>{{$audit->set->duration_min}}</td>
                        <th> Time generate: </th>
                        <td>{{$audit->set->time_generate}}</td>
                      </tr>

                      <tr>
                        <th> Bound : </th>
                        <td>{{$audit->set->bound}}</td>
                        <th> Call duration (max): </th>
                        <td>{{$audit->set->duration_max}}</td>
                        <th>Next generate:  </th>
                        <td>{{$audit->set->next_generate}}</td>
                      </tr>

                      <tr>
                        <th> Call count: </th>
                        <td>{{$audit->set->call_count}}</td>
                        <td></td>
                        <td></td>
                        <th> Frequency:</th>
                        <td>{{$audit->set->frequency}}</td>
                      </tr>
                      </tbody>
                    </table>
                    <br/>

                    <table class = 'audittable'>
                      <thead></thead>
                      <tbody>
                      <tr>
                        <th> Ingroups: </th>
                        <td>{{$audit->set->ingroups}}</td>
                      </tr>

                      <tr>
                        <th> Agents: </th>
                        <td>{{$audit->set->agents}}</td>
                      </tr>

                      <tr>
                        <th> Dispositions: </th>
                        <td>{{$audit->set->disposition}}</td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
          </div>
    </div>
</div>

 <div class="row">
    <div class="col-md-12">
          <div class="card">
              <div class="card-block"  >
                  <div class="table-responsive" id="auditList">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th>Agent</th>
                                <th>Call Date</th>
                                <th>Campaign Id</th>
                                <th>Call Length</th>
                                <th>Dispo</th>
                                <th>Listen</th>
                                <th>done</th>
                                <th>locked</th>
                                <th>score</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                          @if($audit->closer->count())
                            @foreach($audit->closer as $closer)
                            <tr >
                                <td>{{$closer->agent}}</td>
                                <td>{{$closer->call_date}}</td>
                                <td>{{$closer->campaign_id}}</td>
                                <td>{{$closer->length_in_min}}</td>
                                <td>{{$closer->status}}</td>
                                <td>
                                <audio controls="controls">  
                                    <source src="{{$closer->location}}" />  
                                </audio>
                                </td> 
                                <td>{{($closer->is_complete == '') ? 'no' : 'yes'}} </td>
                                <td>{{($closer->locked == 0) ? 'no' : 'yes'}} </td>
                                <td>{{$closer->score}} </td>
                                <td class="tbllink" data-link="{{route('ruby.audit.index',$closer->id)}}?bound={{$audit->set->bound}}&account={{$audit->set->account_id}}" > <!-- //{base+"/audit/"+list.id+"?bound="+props.data.bound+"&account="+props.data.account_id} -->
                                  <a href='#' class="editable editable-click">Audit</a>
                                </td>
                            </tr> 
                            @endforeach
                          @endif
                          @if($audit->recordings->count())
                          <tr >
                              <td>{list.agent}</td>
                              <td>{timeFormat(list.call_date)}</td>
                              <td>{list.campaign_id}</td>
                              <td>{list.length_in_min}</td>
                              <td>{list.status}</td>
                              <td>
                                <audio id="player" controls crossorigin ><source src={list.location} type="audio/mp3" /></audio>
                              </td> 
                              <td class="tbllink" data-link={base+"/audit/"+list.id+"?bound="+props.data.bound+"&account="+props.data.account_id}>
                                <a href='#' class="editable editable-click">Audit</a>
                              </td>
                          </tr> 
                          @endif                          
                        </tbody>
                    </table>


                  </div>
              </div>
          </div>
    </div>
</div>

@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

<script src="{{$asset}}custom/js/ejs.js"></script>
<script type="text/javascript">
var socket = io.connect('http://{{config("app.ruby")}}',{ query: "user=1" });
var base = "{{url('ruby/')}}";
var UserId = {{Auth::user()->id}};

</script>
<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>



@endsection

