<script type="text/javascript">
function textareaReplaceLineBreaks(textareaID) {
    (function($) {        
        if (!textareaID) var textareaID = "#content";
        var textarea = $(textareaID);
        
        textarea.val(
            $(textarea).val().replace(new RegExp('\r?\n','g'), '<br />')
        );
        
    })(jQuery);        
}
</script>
<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target=".add-modal">
  <i class="material-icons" aria-hidden="true">add </i>
</button>
<div class="modal fade add-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('ruby.announcements.create')}}" novalidate>
        {{ csrf_field() }}   
        <input type="hidden" name="source" value="qa">                     

          <div class="modal-body">
            <fieldset class="form-group">
                <label for="exampleSelect1">To</label>
                <select class="form-control" name='destination'>
                    <option value="all">all</option>
                    <option value="hrd">hrd</option>
                    <option value="noc">noc</option>
                    <option value="bd">bd</option>
                </select>
            </fieldset>              
            <fieldset class="form-group @if($errors->has('title')) has-danger @endif ">
               <label for="username">title</label>
               <input type="title" class="form-control" name="title" value="{{ old('title') }}"> 
                @if ($errors->has('title'))
                    <label class="form-control-label">{{ $errors->first('title') }}</label>
                @endif                      
            </fieldset>  
            <fieldset class="form-group @if($errors->has('excerpt')) has-danger @endif ">
               <label for="username">excerpt</label>
               <input type="excerpt" class="form-control" name="excerpt" value="{{ old('excerpt') }}"> 
                @if ($errors->has('excerpt'))
                    <label class="form-control-label">{{ $errors->first('excerpt') }}</label>
                @endif                      
            </fieldset>   
            <fieldset class="form-group">
                <label for="content">Content</label>
                <textarea class="form-control" id="content" rows="3" name="content"></textarea>
            </fieldset>                                  
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" onclick="textareaReplaceLineBreaks('#content');">Add</button>
          </div>
        </form>
      </div>
  </div>
</div> 