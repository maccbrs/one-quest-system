<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>QA Announcements</h3>
              {!!view('ruby.announcements.add-modal')!!}
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>Title</th> 
                        <th>Excerpt</th>
                        <th>To</th>
                        <th>#</th>
                     </tr>
                  </thead> 
                  <tbody>
                     @if($items->count())
                        @foreach($items as $i)
                        <tr>
                           <td>{{$i->title}}</td>
                           <td>{{$i->excerpt}}</td>
                           <td>{{$i->destination}}</td>
                           <td>
                              <a class="btn btn-info" href="{{route('ruby.announcements.get',$i->id)}}">view</a>
                              <a class="btn btn-info" href="{{route('ruby.announcements.seen',$i->id)}}">({{$seen[$i->id]}})Seen</a>
                           </td>
                        </tr>
                        @endforeach
                     @endif
                  </tbody>
               </table>

            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')

   <script src="{{$asset}}vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
   <script src="{{$asset}}vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
   <script>
      $('#texteditor').ckeditor();
      // $('.textarea').ckeditor(); // if class is prefered.
   </script>

@endsection