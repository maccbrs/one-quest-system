<script type="text/javascript">

function textareaReplaceLineBreaks(textareaID) {
    (function($) {        
        if (!textareaID) var textareaID = "#content";
        var textarea = $(textareaID);
        
        textarea.val(
            $(textarea).val().replace(new RegExp('\r?\n','g'), '<br />')
        );
        
    })(jQuery);        
}
</script>

<button class="btn btn-success pull-right" data-toggle="modal" data-target=".add-modal-{{$item->id}}" >
  Edit 
</button>&nbsp;

<div class="modal fade add-modal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('gem.announcements.announcement-update')}}" novalidate>
        {{ csrf_field() }}   

        <input type="hidden" class="form-control" name="id" value="{{ $item->id }}">       

          <div class="modal-body">

            <fieldset class="form-group">
                <label for="exampleSelect1">To</label>
                <select class="form-control" name='destination'>
                    <option value="all" >all</option>
                    <option value="hrd" >hrd</option>
                    <option value="noc" >noc</option>
                    <option value="bd" >bd</option>
                </select>
            </fieldset>     

            <fieldset class="form-group @if($errors->has('title')) has-danger @endif ">
               <label for="username">title</label>
                  <input type="text" class="form-control" name="title" value="{{ $item->title }}"> 
            </fieldset>  

            <fieldset class="form-group @if($errors->has('excerpt')) has-danger @endif ">
               <label for="username">excerpt</label>
                <input type="text" class="form-control" name="excerpt" value="{{ $item->excerpt }}"> 
            </fieldset>   

            <fieldset class="form-group">
              <label for="username">Content</label>
              <div class="panel-body">
                <textarea id="texteditor" class="form-control" rows="20" height="100px" name="content"><?= (isset( $item->content)? $item->content:'') ?></textarea>
              </div>
            </fieldset>   

          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" >Edit</button>
          </div>
        </form>
      </div>
  </div>
</div> 