<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'QA tools') 


@section('top-navigation')
   <div class="header-inner">
      <div class="navbar-item navbar-spacer-right brand hidden-lg-up">  <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen"><i class="material-icons">menu</i> </a>   <a class="brand-logo hidden-xs-down"><img src="{{$asset}}milestone/images/logo_white.png" alt="logo"> </a> </div>
      
      <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#"><span>Campaigns</span></a>
     
      <div class="navbar-item nav navbar-nav">

         <div class="nav-item nav-link">

         </div>

         <div class="nav-item nav-link dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><span>English</span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="javascript:;">English</a> <a class="dropdown-item" href="javascript:;">Russian</a></div>
         </div>

         <div class="nav-item nav-link dropdown"> 
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">notifications</i> <span class="tag tag-danger">4</span></a>
            <div class="dropdown-menu dropdown-menu-right notifications">
               <div class="dropdown-item">
                  <div class="notifications-wrapper">
                     <ul class="notifications-list">
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-success text-white"><i class="material-icons">arrow_upward</i></div>
                              </div>
                              <div class="notification-message"><b>Sean</b> launched a new application <span class="time">2 seconds ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-danger text-white"><i class="material-icons">check</i></div>
                              </div>
                              <div class="notification-message"><b>Removed calendar</b> from app list <span class="time">4 hours ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-info text-white">J</span></span>
                              <div class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-primary text-white">C</span></span>
                              <div class="notification-message"><b>Conan Johns</b> created a new list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                     </ul>
                  </div>
                  <div class="notification-footer">Notifications</div>
               </div>
            </div>
         </div>
         <a href="javascript:;" class="nav-item nav-link nav-link-icon" data-toggle="modal" data-target=".chat-panel" data-backdrop="false"><i class="material-icons">chat_bubble</i></a>
      </div>                  
   </div>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">

          <div class="card">

              <div class="card-header no-bg b-a-0">
                Accounts
              </div>
              <div class="card-block" >

                  <div class="table-responsive" id="auditList">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th>Agent</th>
                                <th>Call Date</th>
                                <th>Campaign Id</th>
                                <th>Call Length</th>
                                <th>Dispo</th>
                                <th>Listen</th>
                                <th>done</th>
                                <th>locked</th>
                                <th>score</th>
                                <th>Evaluator</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                          @if($results->count())
                            @foreach($results as $r)
                            <tr >
                                <td>{{$r->agent}}</td>
                                <td>{{$r->call_date}}</td>
                                <td>{{$r->campaign_id}}</td>
                                <td>{{$r->length_in_min}}</td>
                                <td>{{$r->status}}</td>
                                <td>
                                <audio controls="controls">  
                                    <source src="{{$r->location}}" />  
                                </audio>
                                </td> 
                                <td>{{($r->is_complete == '') ? 'no' : 'yes'}} </td>
                                <td>{{($r->locked == 0) ? 'no' : 'yes'}} </td>
                                <td>{{$r->score}} </td>
                                <td>{{$help->evaluator($r->evaluator)}}</td>
                                <td class="tbllink" data-link="{{route('ruby.audit.index',$r->id)}}?bound={{$bound}}&account=1" > <!-- //{base+"/audit/"+list.id+"?bound="+props.data.bound+"&account="+props.data.account_id} -->
                                  <a href='#' class="editable editable-click">Audit</a>
                                </td>
                            </tr> 
                            @endforeach
                          @endif                         
                        </tbody>
                    </table>


                  </div>

              </div>

          </div>
    </div>
 
</div>




@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>



@endsection