<?php $asset = URL::asset('/'); ?> 

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">


    <title>OQS</title>

    <!-- Bootstrap core CSS -->
    <link href="{{$asset}}CSS/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/signin/signin.css" rel="stylesheet">
  </head>

  <body style="background-color: #3498DB" >
    <div class="container" >

      @yield('content')

    </div> <!-- /container -->
  </body>
</html>
