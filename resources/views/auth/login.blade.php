@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                         <fieldset class="form-group">
                            <label for="email">Email</label>
                            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                    <small class="text-muted">{{ $errors->first('email') }}</small>
                            @endif                      
                         </fieldset>

                         <fieldset class="form-group">
                            <label for="email">Email</label>
                            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                    <small class="text-muted">{{ $errors->first('email') }}</small>
                            @endif                      
                         </fieldset>

                        <fieldset class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
