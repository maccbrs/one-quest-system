<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'Inventory')

@section('content')
		<section class="content-header">
            <h1>Inventory</h1>
        </section>
        <!-- Main content -->
        <section class="content">

                    <div class="panel ">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Items</th>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>#</th>
                                    </tr>
                                    </thead>
                                    <tbody>
	                                    <tr>
	                                        <td><a class="label label-sm label-success" href="{{route('gemstone.inventory.costume-pc-index')}}">PCs</a></td>
	                                        <td>xxx</td>
	                                        <td>xxx</td>
	                                        <td>xxx</td>
	                                        <td>xxx</td>
	                                    </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>

        </section>
@endsection 

@section('footer-scripts')

@endsection