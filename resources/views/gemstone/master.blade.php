<?php 
$asset = URL::asset('/'); 
$usertype = Auth::user()->user_type; 
$request = Request::instance();
$AppHelper = new AppHelper;
$userList = $AppHelper->fetchTable('gem' . chr(92) . 'User','emp_code','sortBy');

?> 
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="{{$asset}}gemstone/img/favicon.ico"/>
    
          <link rel="stylesheet" href="{{$asset}}datatables/DataTables-1.10.15/media/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="{{$asset}}datatables/jquery.dataTables.yadcf.css">
      <link rel="stylesheet" href="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{$asset}}milestone/styles/app.min.css">
    
    <link type="text/css" href="{{$asset}}gemstone/css/app.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{$asset}}gemstone/css/custom.css">
    <link rel="stylesheet" href="{{$asset}}gemstone/css/pages/dashboard1.css">
    <link rel="stylesheet" type="text/css" href="{{$asset}}gemstone/vendors/sweetalert2/css/sweetalert2.min.css"/>
    <link rel="stylesheet" href="{{$asset}}sweetalert/dist/sweetalert.css">
          <link rel="stylesheet" href="{{$asset}}jquery-ui/jquery-ui.css">

    <script src="{{$asset}}sweetalert/dist/sweetalert.min.js"></script>
	
    <style type="text/css">
    .head-span{
            font-size: 24px;
            font-weight: bold;
    }
    </style>
    @yield('header-scripts')
</head>

<body>

<header class="header">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="logo">
            <span class="head-span">NOC CENTRAL</span>
        </a>
        <div>
            <a href="javascript:void(0)" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i
                    class="fa fa-fw fa-bars"></i>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i
                            class="fa  fa-fw fa-bell-o black"></i>
                        <span class="label label-danger">3</span>
                    </a>
                    <ul class="dropdown-menu dropdown-notifications table-striped">
                        <li>
                            <a href="#" class="notification striped-col">
                                <img src="{{$request->query('UserImageSm')}}" class="img-circle" alt="User Image">
                                <div class="notif-body"><strong>Anderson</strong> shared post from
                                    <strong>Ipsum</strong>.
                                    <br>
                                    <small>Just Now</small>
                                </div>
                                <span class="label label-success label-mini msg-lable">New</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="notification">
                                <img src="{{$request->query('UserImageSm')}}" class="img-circle" alt="User Image">
                                <div class="notif-body"><strong>Williams</strong> liked <strong>Lorem Ipsum</strong>
                                    typesetting.
                                    <br>
                                    <small>5 minutes ago</small>
                                </div>
                                <span class="label label-success label-mini msg-lable">New</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="notification striped-col">
                                <img src="{{$request->query('UserImageSm')}}" class="img-circle" alt="User Image">
                                <div class="notif-body">
                                    <strong>Robinson</strong> started follwing <strong>Trac Theme</strong>.
                                    <br>
                                    <small>14/10/2014 1:31 pm</small>
                                </div>
                                <span class="label label-success label-mini msg-lable">New</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="notification">
                                <img src="{{$request->query('UserImageSm')}}" class="img-circle" alt="User Image">
                                <div class="notif-body">
                                    <strong>Franklin</strong> Liked <strong>Trending Designs</strong> Post.
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-footer"><a href="javascript:void(0)">View All messages</a></li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown-->
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0)" class="dropdown-toggle padding-user" data-toggle="dropdown">
                        <img src="{{$request->query('UserImageSm')}}" class="img-circle img-responsive pull-left" alt="User Image">
                        <div class="riot">
                            <div>
                                <i class="caret"></i>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User name-->
                        <li class="user-name text-center">
                            <span>{{Auth::user()->name}}</span>
                        </li>
                        <!-- Menu Body -->
                        <li class="p-t-3">
                            <a href="{{route('gem.dashboard.index')}}">
                                Milestone<i class="fa fa-fw fa-sign-out pull-right"></i>
                            </a> 
                        </li>
                        <li>
                          <a href="{{ url('/logout') }}">
                            Logout <i class="fa fa-fw fa-sign-out pull-right"></i>
                          </a>                           
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-aside">
        <!-- sidebar: style can be found in sidebar-->
        <section class="sidebar">
            <div id="menu" role="navigation">
                <div class="nav_profile">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="javascript:void(0)">
                            <img src="{{$request->query('UserImageSm')}}" class="img-circle" alt="User Image">
                        </a>
                        <div class="content-profile">
                            <h4 class="media-heading">
                                {{Auth::user()->name}}
                            </h4>
                            <p>{{($usertype != 'operation') ? ($usertype) : ((Auth::user()->user_level == 1) ? 'Team Leader' : 'Manager')}}</p>
                        </div>
                    </div>
                </div>
                <ul class="navigation">
                    <li class="active" id="active">
                        <a href="{{route('gemstone.dashboard.index')}}">
                            <i class="menu-icon fa fa-fw fa-home"></i>
                            <span class="mm-text ">Dashboard 1</span>
                        </a>
                    </li>

                    <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-archive"></i>
                            <span>Ticketing</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('gemstone.department.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i>Department
                                </a>
                            </li>                                                      
                        </ul>
                    </li>

                    <li class="menu-dropdown">
                        <a href="#">
                            <i class="menu-icon fa fa-video-camera"></i>
                            <span>Cctv</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('gemstone.cctv.all')}}">
                                    <i class="fa fa-fw fa-list-ol"></i> all
                                </a>
                            </li>
                            <li>
                                <a href="{{route('gemstone.cctv.check')}}">
                                    <i class="fa fa-fw fa-check"></i> Check cctv status
                                </a>
                            </li>
                            <li>
                                <a href="{{route('gemstone.cctv.results')}}">
                                    <i class="fa fa-fw fa-ellipsis-v"></i> Results
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown">
                        <a href="#">
                            <i class="menu-icon fa fa-inr"></i>
                            <span>BioProxy</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('gemstone.bio.all')}}">
                                    <i class="fa fa-fw fa-list-ol"></i> all
                                </a>
                            </li>
                            <li>
                                <a href="{{route('gemstone.bio.check')}}">
                                    <i class="fa fa-fw fa-check"></i> Check BioProxy status
                                </a>
                            </li>
                            <li>
                                <a href="{{route('gemstone.bio.results')}}">
                                    <i class="fa fa-fw fa-ellipsis-v"></i> Results
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-users"></i>
                            <span>Users</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('gemstone.users.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i> Milestone
                                </a>
                            </li>
                            <li>
                                <a href="{{route('gemstone.users.agents')}}">
                                    <i class="fa fa-fw fa-list-ol"></i> Agents
                                </a>
                            </li>    
                            <li>
                                <a href="{{route('gemstone.users.tl')}}">
                                    <i class="fa fa-fw fa-list-ol"></i> Team Leaders
                                </a>
                            </li>   
                            <li>
                                <a href="{{route('gemstone.users.ps')}}">
                                    <i class="fa fa-fw fa-list-ol"></i> Program Supervisors
                                </a>
                            </li>                      
                        </ul>
                    </li>

                    <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-phone"></i>
                            <span>DIDs</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('gemstone.did.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i> did list
                                </a>
                            </li> 
                            <li>
                                <a href="{{route('gemstone.did.testcalls')}}">
                                    <i class="fa fa-fw fa-ellipsis-v"></i> Test Calls
                                </a>
                            </li>                                                      
                        </ul>
                         
                    </li>

<!--                     <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-archive"></i>
                            <span>Inventory</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('gemstone.inventory.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i>indexes
                                </a>
                            </li>                                                      
                        </ul>
                    </li> -->
                    <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-archive"></i>
                            <span>Asset Mngt</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('gemstone.asset.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i>List Assets
                                </a>
                            </li>
                            <li>
                                <a href="{{route('gemstone.license.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i>Licences
                                </a>
                            </li>                                                                                   
                        </ul>
                    </li>
                    <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-comment-o"></i>
                            <span>Announcements</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('gemstone.announcement.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i> View
                                </a>
                            </li>                                     
                        </ul>
                    </li>

                    <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-user"></i>
                            <span>Dispute Approver</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('gemstone.approver.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i> Manage
                                </a>
                            </li>                                     
                        </ul>
                    </li>
					<li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-diamond"></i>
                            <span>System Table</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
							<li>
                                <a href="{{route('gemstone.system.index')}}">
                                    <i class="fa fa-fw fa-briefcase"></i>System Parameter
                                </a>
                            </li>
                            <li>
                                <a href="{{route('gemstone.asset.index')}}">
                                    <i class="fa fa-fw fa-briefcase"></i>Campaign
                                </a>
                            </li>
                            <li>
                                <a href="{{route('gemstone.license.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i>Licences
                                </a>
                            </li>                                                                                   
                        </ul>
                    </li>
					<li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-cog"></i>
                            <span>Tools</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="#">
								
                                    <i class="fa fa-fw fa-lock"></i> <span class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Password Generator</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
								
                                    <i class="fa fa-fw fa-lock"></i> <span class="btn btn-info btn-sm" data-toggle="modal" data-target="#ResetPassword">Reset Password</span>
                                </a>
                            </li>                                                                           
                        </ul>
                    </li>

                </ul>
         
            </div>
            <!-- menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
    <aside class="right-aside">
        @include('sweet::alert')
        @yield('content')

        <!-- Content Header (Page header) -->
        
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<!-- global js -->
<script src="{{$asset}}gemstone/js/app.js" type="text/javascript"></script>

<script type="text/javascript" src="{{$asset}}gemstone/js/pages/alerts.js"></script>
      <script src="{{$asset}}jquery-ui/jquery-ui.js"></script>
@yield('footer-scripts')
<!-- end of page level js -->


 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
		
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Password Generator</h4>
        </div>
        <div class="modal-body">
		  <p><input  id="suffix"  type="textbox" style="display:none;" oninput="myFunction()" />Suffix <input  data-textbox="suffix" class="checkbox" id="suffix-checkbox" type="checkbox"> <input style="display:none;" id="prefix" type="textbox" oninput="myFunction()"/> Prefix  <input  id="prefix-checkbox" class="checkbox" data-textbox="prefix" type="checkbox"></p>
		 
          <p>Full Name:</p>
		  <p><input type="textbox" name="textbox1" oninput="myFunction()" class="form-control" id="demo"></p>
		  <p>Password: </p>
		  <p><input type="text" readonly="true" id="txtHint" class="form-control"></span></p>

			<!-- <button onclick="myFunction()" >Get Password</button>-->
		  
		  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div> <!-- end of modal -->
  
  
   <!-- Modal -->
  <div class="modal fade" id="ResetPassword" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
		
		 	
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reset Password  <div id="return_msg"></div></h4>
        </div>
        <div class="modal-body">
		  
		 <div id="return_msg"></div>
          <p>Employee Number -- FullName</p>
		  <p>
			<select name="user_id" id="user_id" class="col-md-12 form-control">
				<?php foreach($userList as $key => $val)
						echo "<option value='" . $val->id ."'>" . $val->emp_code . ' - ' . $val->name . "</option>";
				
				?>
			
				
			</select>
			</p>
		  <p>Password: </p>
		  <p><input type="text" id="newpass"  class="form-control"></span></p>
		  <input type="button" value="Reset Password" onclick="resetpass();"/>
		  <!--<input type="button" value="Reset Password" onclick="loadDoc($('#user_id').val(),$('#newpass').val())"/>
			<!-- <button onclick="myFunction()" >Get Password</button>-->
		  
		  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div> <!-- end of modal -->
  
  
  
  <script>
  	$( document ).ready(function() {
  
				$( ".checkbox" ).on( "change", function() {
				if ($(this).prop("checked")==true){
				$("#" + $(this).data("textbox")).css("display", "inline");//	$("#" + $(this).data("textbox")).show();
				myFunction();
				}
			
				else {
				//	$("#suffix").hide;
				$("#" + $(this).data("textbox")).css("display", "none");myFunction();//alert('no');//	$("#" + $(this).data("textbox")).hide();
					}	
				});
		  
		});
  
  
  
  
  
  
function myFunction() {
	
	var suff = "";
	var pref = "";
	
	
	if ($("#suffix-checkbox").prop("checked")==true){
		suff = $("#suffix").val();
	}
	else {suff = "";}
	if ($("#prefix-checkbox").prop("checked")==true){
		pref = $("#prefix").val();
	}
	else {pref = "";}
	
	
	
	
	
    var str = document.getElementById("demo").value; 
    var res = str
  .toLowerCase().replace(/ /g,"").replace(/,/g,".").replace(/a/g,"@")
   .replace(/b/g,"13")
   .replace(/c/g,"C")
   .replace(/d/g,"c1")
   .replace(/e/g,"3")
   .replace(/f/g,"F")
   .replace(/g/g,"g")
   .replace(/h/g,"h")
   .replace(/i/g,"!")
   .replace(/j/g,"J")
   .replace(/k/g,"1<")
   .replace(/l/g,"L")
   .replace(/m/g,"M")
   .replace(/n/g,"n")
   .replace(/o/g,"0")
   .replace(/p/g,"p")
   .replace(/q/g,"Q")
   .replace(/r/g,"R")
   .replace(/s/g,"$")
   .replace(/t/g,"t")
   .replace(/u/g,"U")
   .replace(/v/g,"\/")
   .replace(/w/g,"W")
   .replace(/x/g,"#")
   .replace(/y/g,"Y")
   .replace(/z/g,"%");

    $('#txtHint').val(suff + res + pref);
}
function resetpass( ) {
	
	
	var r = confirm("Confirmation on Resetting Password ");
	if (r == true) {
        loadXMLDoc();
    } 
	

	
}


function loadXMLDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("return_msg").innerHTML = "<span style='background-color:#f2f2f2;'>" + this.responseText + "</span>" ;
    }
  };
  xhttp.open("GET", "<?php echo $asset;?>bloodstone/ajaxresetpass?id=" + document.getElementById("user_id").value +"&password=" + document.getElementById("newpass").value, true);
  xhttp.send();
  //document.getElementById("return_msg").innerHTML = xhttp.responseText;
}


/*

function loadDoc() {  
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", "localhost:8000/bloodstone/ajaxresetpass?id=568&password=11111", true);
  //xhttp.open("GET", <?php // echo $asset;?>"localhost:8000/bloodstone/ajaxresetpass?id=" + unique_id + "&password=" + password, false);
  xhttp.send();
  //document.getElementById("return_msg").innerHTML = xhttp.responseText;
}
*/

</script>
</body>


<!-- Mirrored from demo.lorvent.com/clean/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 13 Apr 2017 09:54:33 GMT -->
</html>
