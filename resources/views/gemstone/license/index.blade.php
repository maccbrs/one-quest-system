<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'Asset - list all')
@section('header-scripts')
<style type="text/css">
#gemstone-asset-index > label{
    padding-right: 5px;
}
</style>
@endsection
@section('content')
		<section class="content-header">
            <h1>License</h1>
        </section>
        <!-- Main content -->
        <section class="content">

                    <div class="panel ">
                        <div class="panel-body">
                            <div class="table-responsive" id="gemstone-asset-index">
                                <label>Select Group By:</label>
                                 <a class="btn btn-sm btn-primary pull-right" href="{{route('gemstone.license.add')}}">Add</a>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Company</th>
                                        <th>License</th>
                                        <th>Manufacturer</th>
                                        <th>Product Key</th>
                                        <th>Purchase Date</th>
                                        <th>Seats</th>
                                        <th>Remaining Seats</th>
                                        <th>Date Entry</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($items as $item)
                                        <tr>
                                            <td><a href="{{route('gemstone.license.group',['field' => 'company','value' => $item->company])}}">{{$item->company}}</a></td> 
                                            <td><a href="{{route('gemstone.license.group',['field' => 'license','value' => $item->license])}}">{{$item->license}}</a></td>
                                            <td><a href="{{route('gemstone.license.group',['field' => 'manufacturer','value' => $item->manufacturer])}}">{{$item->manufacturer}}</a></td>
                                            <td><a href="{{route('gemstone.license.view',$item->id)}}">{{$item->product_key}}</a></td>
                                            <td><a href="{{route('gemstone.license.group',['field' => 'purchase_date','value' => $item->purchase_date])}}">{{$item->purchase_date}}</a></td>
                                            <td>{{$item->seats}}</td>
                                            <td>{{$item->remaining_seats}}</td>
                                            <td>{{$item->created_at}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>

        </section>
@endsection 

@section('footer-scripts')

@endsection