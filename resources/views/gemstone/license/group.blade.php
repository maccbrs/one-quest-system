<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'License - list all')

@section('content')
		<section class="content-header">
            <h1>Group By: {{$field}} = {{$value}}</h1>
        </section>
        <!-- Main content -->
        <section class="content">

                    <div class="panel ">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <a class="btn btn-sm btn-primary pull-right" href="{{route('gemstone.asset.add')}}">Add</a>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Company</th>
                                        <th>License</th>
                                        <th>Manufacturer</th>
                                        <th>Product Key</th>
                                        <th>Purchase Date</th>
                                        <th>Seats</th>
                                        <th>Remaining Seats</th>
                                        <th>Date Entry</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($items as $item)
                                        <tr>
                                            <td>{{$item->company}}</td> 
                                            <td>{{$item->license}}</td>
                                            <td><a href="{{route('gemstone.asset.view',$item->id)}}">{{$item->manufacturer}}</a></td>
                                            <td><a href="{{route('gemstone.asset.group',['field' => 'model','value' => $item->product_key])}}">{{$item->product_key}}</a></td>
                                            <td><a href="{{route('gemstone.asset.group',['field' => 'purchase_date','value' => $item->purchase_date])}}">{{$item->purchase_date}}</a></td>
                                            <td><a href="{{route('gemstone.asset.group',['field' => 'supplier','value' => $item->seats])}}">{{$item->seats}}</a></td>
                                            <td><a href="{{route('gemstone.asset.group',['field' => 'supplier','value' => $item->remaining_seats])}}">{{$item->remaining_seats}}</a></td>
                                            <td><a href="{{route('gemstone.asset.group',['field' => 'created_at','value' => $item->created_at])}}">{{$item->created_at}}</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                                {{$items->links()}}
                            </div>
                        </div>
                    </div>

        </section>
@endsection 

@section('footer-scripts')

@endsection