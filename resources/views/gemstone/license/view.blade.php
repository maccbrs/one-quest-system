<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'Item')

@section('content')
		<section class="content-header">
            <h1>{{$item->product_key}}</h1>
        </section>
        <!-- Main content -->
        <section class="content">

                <div class="col-sm-8 col-sm-offset-2">
                    <div class="panel ">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="ti-menu"></i> License Detail
                            </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw ti-angle-up clickable"></i>
                                    <i class="fa fa-fw ti-close removepanel clickable"></i>
                                </span> 
                        </div>
                        <div class="panel-body">
                            <div class="box-body">
                                <dl class="dl-horizontal">

                                    <dt>
                                        Product Key
                                    </dt>
                                    <dd>
                                        {{$item->product_key}}
                                    </dd>

                                    <!-- *** -->
                                    <dt>Company</dt>
                                    <dd>
                                        {{$item->company}}
                                    </dd> 

                                    <dt>License</dt> 
                                    <dd>
                                        {{$item->license}}
                                    </dd>
                                    <dt>
                                        Manufacturer
                                    </dt>
                                    <dd>
                                        {{$item->manufacturer}}
                                    </dd>
                                    <dt>Seats</dt>
                                    <dd>
                                        {{$item->seats}}
                                    </dd>
                                    <dt>
                                        Remaining Seats
                                    </dt>
                                    <dd>
                                        {{$item->remaining_seats}}
                                    </dd>
                                    <dt>Purchase Date</dt>
                                    <dd>
                                        {{$item->purchase_date}}
                                    </dd>
                                    <!-- *** -->
                                    <dt>Purchase Cost</dt>
                                    <dd>
                                        {{$item->purchase_cost}}
                                    </dd>
                                  
                                    <!-- *** -->
                                    <dt>Date Entry</dt>
                                    <dd>
                                        {{$item->created_at}}
                                    </dd> 
                                                                                                                                                                                                                                
                                </dl>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

        </section>
@endsection 

@section('footer-scripts')

@endsection