<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')




<?php


//header  		=== Modal Header
//form-action 	=== Form action
//form-list 	=== input list
	/* form list === separator |
			1 input type  possible value "text , radio , checkbox , select , textarea"
			2 Label
			3 name
      Sample
	  
	  $array_list = array(
			  "header"=>"Parameter", 
			  "form-action"=> route('gemstone.users.agent_register'), 
			  "form-list"=>  array("parameter-key" => "text|Parameter Key|parameter-key",
								   "parameter-value" => "text|Parameter Value|parameter-value"
								  )
			  );
	  

	*/

$array_list = array(
			  "header"=>"Parameter", 
			  "form-action"=> route('gemstone.system.create'), 
			  "form-list"=>  array("parameter-key" => "text|Parameter Key|parameter_key",
								   "parameter-value" => "text|Parameter Value|parameter_value",
								   "remarks" => "textarea|Remarks|remarks"
								  )
			  );
$form_action = route('gemstone.system.edit');
$array_list2 = array(
			  "header"=>"Parameter", 
			  "form-action"=> route('gemstone.system.edit'), 
			  "form-list"=>  array("hidden" => "hidden|Parameter Key|id",
								   "parameter-key" => "text|Parameter Key|parameter_key",
								   "parameter-value" => "text|Parameter Value|parameter_value",
								   "remarks" => "textarea|Remarks|remarks",
								   "action" => "hidden|Parameter Key|action",
								  )
			  );			  
			  

?>


<section class="content-header">
            <h1>System Parameter <span class = "pull-right">
			</span>
			<span class = "pull-right">
			<button type="button" class="btn btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".modalAddUser"></button>
			{!!view('create-modal',['tl_list' => $array_list])!!}{!!view('edit-modal',['tl_list' => $array_list2])!!} {!!view('delete-modal',['tl_list' => $array_list2])!!}</span>
			
			
			</h1>
			
			
			
        </section>
        <!-- Main content -->
        
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">

                            	
                                <table id="table" class="table table-bordered table-hover">
					                     <thead>
					                        <tr>
					                           <th style="width:20%">Parameter Key</th> 
					                           <th style="width:42%">Parameter Value</th>
					                           <th style="width:10%">Remarks</th>
					                           <th style="width:18%">Actions</th>
					                        </tr>
					                     </thead>
					                     <tbody>
											@foreach($MstSystemParameter as $tl => $tls)
											  <tr>
											  <td>{{$tls['parameter_key']}}</td>
											  <td>{{$tls['parameter_value']}}</td>
											  <td>{{$tls['remarks']}}</td>
											  <td><button type="button" data-toggle="modal" 
											  onclick="$('.class_parameter_key').val('{{$tls['parameter_key']}}');
											  $('.class_parameter_value').val('{{$tls['parameter_value']}}');
											  $('.class_id').val('{{$tls['id']}}');
											  $('.class_remarks').val('{{$tls['remarks']}}');
											  $('.class_action').val('edit')" 
											  
											  
											  data-target=".modalEditUser"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>edit</button>  | 
											  
											  <button type="button" data-toggle="modal" 
											  onclick="$('.class_parameter_key').val('{{$tls['parameter_key']}}');
											  $('.class_parameter_value').val('{{$tls['parameter_value']}}');
											  $('.class_id').val('{{$tls['id']}}');
											  $('.class_remarks').val('{{$tls['remarks']}}');
											  $('#desc').html('{{$tls['parameter_key']}}');
											  $('.class_action').val('delete')" 
											  
											  
											  data-target=".modalDelete"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>delete</button></a></td>
											  </tr>
											@endforeach

					                     </tbody>
                                </table>
                               

                                   
                               
                            </div>
                        </div>
                    </div>

            </div>


        </section>

@endsection 

@section('footer-scripts')

@endsection