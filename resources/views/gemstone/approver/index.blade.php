<?php $asset = URL::asset('/'); ?> 

@extends('gemstone.master')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>Disputes Approver </h3>
              {!!view('gemstone.approver.add-modal',compact('items','help'))!!}
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>Employer</th> 
                        <th>Supervisor</th>
                     </tr>
                  </thead> 
                  <tbody>
                     @if($approvers->count())
                        @foreach($approvers as $a)
                        <tr>
                           <td>{{($a->emp?$a->emp->name:'')}}</td>
                           <td>{{($a->sup?$a->sup->name:'')}}</td>
                        </tr>
                        @endforeach
                     @endif
                  </tbody> 
               </table>

            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection