<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')
        <section class="content-header">
            <h1>Cctv Monitoring </h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">

                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Ip address</th>
                                            <th>Status</th>
                                            <th>Date checked</th>
                                            <th>By:</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($items as $item)
                                        <tr>
                                            <td>{{$item->cctv->name}}</td>
                                            <td>{{$item->cctv->ip}}</td>
                                            <td>{!!($item->status?'<span class="label label-sm label-success">online</span>':'<span class="label label-sm label-danger">offline</span>')!!}</td>
                                            <td>{{$carbon->createFromFormat('Y-m-d H:i:s',$item->created_at)->toDayDateTimeString()}}</td>
                                            <td>
                                                {{$item->checklist->user->name}}
                                            </td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                                {{$items->links()}}
                            </div>
                        </div>
                    </div>

            </div>




        </section>



@endsection 

@section('footer-scripts')

@endsection