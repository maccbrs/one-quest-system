
<button type="button" class="btn btn-primary fa fa-edit pull-right" data-toggle="modal" data-target=".conn-modal-{{$details->id}}"></button>
<div class="modal fade conn-modal-{{$details->id}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="{{route('gemstone.users.agent_edit')}}">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" name="id" value = "{{$details->id}}" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title custom_align">Edit agent</h4>
                </div>
                <div class="modal-body">
                    <fieldset class="form-group">
                        <label for="exampleSelect1">First Name</label>
                        <input type="text" class="form-control" name="first_name" value = "{{$details->first_name}}" >
                    </fieldset>                    
               
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Middle Name</label>
                        <input type="text" class="form-control" name="middle_name" value = "{{$details->middle_name}}">
                    </fieldset>                    
               
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Last Name</label>
                        <input type="text" class="form-control" name="last_name" value = "{{$details->last_name}}">
                    </fieldset>      
                    
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Team Leader</label> 
                          <select class="form-control" name="tl_id">
                            <option  selected value = "{{$details->tl_id}}">Select Team Leader</option>
                            @foreach($tl_list as $tl => $tls)
                              <option value="{{$tls['id']}}" {{($details->teamlead->id == $tls['id']?'selected':'')}}  >{{$tls['first_name'] . " " . $tls['last_name']}}</option>
                            @endforeach
                        </select>
                    </fieldset>   

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Program Supervisor</label>
                          <select class="form-control" name="ps_id">
                            <option  selected value = "{{$details->ps_id}}">Select Program Supervisor</option>
                            @foreach($ps_list as $ps => $pls)
                              <option value="{{$pls['id']}}" {{($details->supervisor->id == $pls['id']?'selected':'')}} >{{$pls['first_name'] . " " . $pls['last_name']}}</option>
                            @endforeach
                        </select>
                    </fieldset>  

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Gem Account</label>
                          <select class="form-control" name="user_id">
                            <option disabled selected>Select Account </option>
                            @foreach($agent_pluck as $gem_id => $gem_name)
                                <option value="{{$gem_id}}" {{($details->user_id == $gem_id?'selected':'')}} >{{$gem_name}} </option>
                            @endforeach
                        </select>
                    </fieldset>  
 

                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-sign"></span> Submit
                    </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>