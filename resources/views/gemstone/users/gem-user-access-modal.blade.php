<button type="button" class="btn btn-primary fa fa-group" data-toggle="modal" data-target=".modalEditUser{{$id}}"></button>
<div class="modal fade modalEditUser{{$id}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="{{route('lilac.user.access_edit',$id)}}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title custom_align">Edit User Access</h4>
                </div>
                <div class="modal-body">
                    <div class = "access-group">
                        @foreach($access as $a => $value)
                       
                            {!!view('lilac.forms.checkbox',['label' => $value,'name' => $value ,'value' => $value])!!}

                        @endforeach
                    </div>

                    <input type="hidden" name ="id" value ="{{$id}}"></input>
                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-sign"></span> Submit
                    </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>