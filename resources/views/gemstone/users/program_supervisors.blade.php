<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')

   <div class="card">

      <div class="card-header no-bg b-a-0">
        <h5>Teamleads List</h5>
      </div>

      <div class="card-block">
        <div class="table-responsive">
          <span class = "pull-right">{!!view('gemstone.users.add-supervisors-modal',compact('users'))!!}</span>
              <table class="table table-bordered customized-table">
                  <thead>
                     <tr>
                        <th>First Name</th> 
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Gem Account</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($supervisors as $ps)
                        <tr>
                           <td>{{$ps->first_name}}</td>
                           <td>{{$ps->middle_name}}</td>
                           <td>{{$ps->last_name}}</td>
                           <td>{{$ps->user_id}}</td>
                           <td>{!!view('gemstone.users.edit-ps-modal',compact('users','ps'))!!}</td>
                        </tr>
                     @endforeach
                  </tbody> 
             </table>
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

<script>

</script>

@endsection