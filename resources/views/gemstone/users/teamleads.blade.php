<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')

   <div class="card">
      <div class="card-header no-bg b-a-0">
         
        <h5>Teamleads List</h5>
        
      </div>
      <div class="card-block">
         <div class="table-responsive">
         	<span class = "pull-right">{!!view('gemstone.users.add-teamlead-modal',compact('users','supervisors'))!!}</span>
            <table class="table table-bordered customized-table">
                <thead>
                    <tr>
                        <th>First Name</th> 
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Supervisor</th>
                        <th>Gem Account</th>
                        <th>#</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($teamleads as $tl)
                        <tr>
                           <td>{{$tl->first_name}}</td>
                           <td>{{$tl->middle_name}}</td>
                           <td>{{$tl->last_name}}</td>
                           <td>{{($tl->supervisor?$tl->supervisor->first_name:'')}}</td>
                           <td>{{($tl->gem?$tl->gem->name:'')}}</td>
                           <td>{!!view('gemstone.users.edit-teamlead-modal',compact('supervisors','users','tl'))!!}</td>
                        </tr>
                     @endforeach
                  </tbody>  
             </table>
             
     
    
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

<script>

</script>

@endsection