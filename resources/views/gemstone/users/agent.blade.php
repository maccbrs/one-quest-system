 <?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')
        <section class="content-header">
            <h1>Agent Users <span class = "pull-right">{!!view('gemstone.users.agent-create-modal',['tl_list' => $tl_list,'ps_list' => $ps_list])!!}</span></h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">

                            	
                                <table id="table" class="table table-bordered table-hover">
					                     <thead>
					                        <tr>
					                           <th>Name</th> 
					                           <th>Team Leader</th>
					                           <th>Pogram Supervisor</th>
					                           <th>Created at</th>
					                           <th>Updated</th>
					                        </tr>
					                     </thead>
					                     <tbody>

					                        @foreach($agents as $value)

					                           <tr>
					                              <td>{{!empty($value->first_name) ? $value->first_name . " " . $value->last_name : ""}}</td>
					                              <td>{{!empty($value->teamlead->first_name) ? $value->teamlead->first_name . " " . $value->teamlead->last_name  : ""}}</td>
					                              <td>{{!empty($value->supervisor->first_name) ? $value->supervisor->first_name . " " . $value->supervisor->last_name  : ""}}</td>
					                              <td>{{!empty($value->createdAt) ? $value->createdAt : ""}}</td>
                                                   <td><button type="button" class="btn btn-primary  fa fa-edit pull-right edit-approver"  
                                                               data-toggle="modal" 
                                                               data-target="#myModal" 
                                                               data-infoid="{{$value->id}}"
                                                               data-infofirstname="{{$value->first_name}}" 
                                                               data-infomiddlename="{{$value->middle_name}}" 
                                                               data-infolastname="{{$value->last_name}}" 
                                                               data-infotl_id="{{$value->teamlead->id}}" 
                                                               data-infops_id="{{$value->supervisor->id}}" 
                                                               data-infouser_id="{{$value->user_id}}" 
                                                               ></button>
                                                   </td>
					                             <?php /* <td>{!!view('gemstone.users.agent-edit-modal',['tl_list' => $tl_list,'ps_list' => $ps_list ,'details' => $value,'agent_pluck' => $agent_pluck])!!}</td>
                                                  */ ?>
					                          </tr>

					                        @endforeach

					                     </tbody>
                                </table>
                               

                                      <!-- Modal -->
                                      <div class="modal fade" id="myModal" role="dialog">
                                        <div class="modal-dialog">
                                             <form method="POST" action="{{route('gemstone.users.agent_edit')}}">
                                                {{ csrf_field() }}
                                                <input type="hidden" class="form-control" name="id" id="id" value = "" >

                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Edit Agent Approver</h4>
                                            </div>
                                            <div class="modal-body">
                                             
                                                
                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">First Name</label>
                                                    <input type="text" class="form-control" name="first_name" id="inputfield_firstname"value = "" >
                                                </fieldset>                    

                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">Middle Name</label>
                                                    <input type="text" class="form-control" name="middle_name" id="inputfield_middlename" value = "">
                                                </fieldset>                    

                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">Last Name</label>
                                                    <input type="text" class="form-control" name="last_name" id="inputfield_lastname" value = "">
                                                </fieldset>   
                                                
                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">Team Leader</label> 
                                                      <select class="form-control" name="tl_id" id="tl_id">
                                                        <option disabled selected  = "">Select Team Leader</option>
                                                        @foreach($tl_list as $tl => $tls)
                                                          <option value="{{$tls['id']}}"   >{{$tls['first_name'] . " " . $tls['last_name']}}</option>
                                                        @endforeach
                                                    </select>
                                                </fieldset>   
                                               
                                                
                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">Program Supervisor</label>
                                                      <select class="form-control" name="ps_id" id="ps_id">
                                                        
                                                        <option disabled selected = "">Select Program Supervisor</option>
                                                         @foreach($ps_list as $ps => $pls)
                                                          <option value="{{$pls['id']}}"  >{{$pls['first_name'] . " " . $pls['last_name']}}</option>
                                                        @endforeach 
                                                    </select>
                                                </fieldset>  

                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">Gem Account</label>
                                                      <select class="form-control" name="user_id" id="user_id">
                                                         
                                                        <option disabled selected>Select Account </option>
                                                        @foreach($agent_pluck as $gem_id => $gem_name)
                                                            <option value="{{$gem_id}}"  >{{$gem_name}} </option>
                                                        @endforeach
                                                         
                                                    </select>
                                                </fieldset>  
                                                
                                                
                                                                                            
                                                
                                            </div>
                                            <div class="modal-footer">
                                              <button type="submit" class="btn btn-success">
                                                        <span class="glyphicon glyphicon-ok-sign"></span> Submit
                                                    </button>
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                          </div>
                                        </form>
                                        </div>
                                      </div><!-- Modal -->
                               
                            </div>
                        </div>
                    </div>

            </div>


        </section>



@endsection 

@section('footer-scripts')
     
      <script src="{{$asset}}datatables/jquery-ui.js"></script>

      <script src="{{$asset}}datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
     <script src="{{$asset}}datatables/jquery.dataTables.yadcf.js"></script> 

      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$asset}}datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>  

<script>      
$(document).ready(function() 
    {
        var currentDate = new Date()
        var day = ('0'+(currentDate.getDate())).slice(-2)
        var month = ('0'+(currentDate.getMonth()+1)).slice(-2)
        var year = currentDate.getFullYear()
        var d = year + "-" + month + "-" + day;

        var oTable;
        oTable = $('#table')

        .DataTable({
        lengthMenu: [[10, 25, 50, 75, 100, -1], [10,25, 50, 75, 100, "All"]],
        pageLength: 215, 
        scrollX: true,
        autoWidth: false,
        responsive: true,
        
        scrollCollapse: true,
        dom: 'Blfrtip',
        stateSave: true,
        buttons: [
            {
                extend: 'copyHtml5',
                title: "Agents' Approver List" + " " + d,
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            {
                extend: 'excelHtml5',
                title: "Agents' Approver List" + " " + d,
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            'colvis'
            ]
            });
    
        $(".edit-approver").click(function() {
            $('#id').val($(this).data("infoid"));
            $('#inputfield_firstname').val($(this).data("infofirstname"));
            $('#inputfield_middlename').val($(this).data("infomiddlename"));
            $('#inputfield_lastname').val($(this).data("infolastname"));
            $('#tl_id').val($(this).data("infotl_id"));
            $('#ps_id').val($(this).data("infops_id"));
            $('#user_id').val($(this).data("infouser_id"));
            
            
          
        });
    
    
    
      }); // $(document).ready(function()  {
    

</script>

@endsection