
<button type="button" class="btn btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".modalAddUser"></button>
<div class="modal fade modalAddUser" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="{{route('gemstone.users.agent_register')}}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title custom_align">Add new agent</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label >First Name</label>
                        <input type="text" class="form-control" name="first_name" >
                    </div>
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Middle Name</label>
                        <input type="text" class="form-control" name="middle_name" >
                    </fieldset>                    
               
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Last Name</label>
                        <input type="text" class="form-control" name="last_name" >
                    </fieldset>      

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Last Name</label>
                          <select class="form-control" name="tl_id">
                            <option disabled selected>Select Team Leader</option>
                            @foreach($tl_list as $tl => $tls)
                              <option value="{{$tls['id']}}"  >{{$tls['first_name'] . " " . $tls['last_name']}}</option>
                            @endforeach
                        </select>
                    </fieldset>   

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Last Name</label>
                          <select class="form-control" name="ps_id">
                            <option disabled selected>Select Program Supervisor</option>
                            @foreach($ps_list as $ps => $pls)
                              <option value="{{$pls['id']}}"  >{{$pls['first_name'] . " " . $pls['last_name']}}</option>
                            @endforeach
                        </select>
                    </fieldset>  
                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-sign"></span> Submit
                    </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>