<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')
        <section class="content-header">
            <h1>Milestone Users</h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">


                                <ul class="nav nav-tabs" role="tablist">
                                   <li class="nav-item">
                                      <a class="nav-link " data-toggle="tab" href="#one" role="tab" aria-expanded="true">Agents</a>
                                   </li>
                                   <li class="nav-item">
                                      <a class="nav-link" data-toggle="tab" href="#two" role="tab" aria-expanded="false">Non-Agents</a>
                                   </li>
                                </ul>

                                <div class="tab-content ">
                                   <div class="tab-pane active table-responsive" id="one" role="tabpanel" >
                                      <table class="table table-bordered table-hover">
                                         <thead>
                                            <tr>
                                               <th>Name</th> 
                                               <th>Position</th>
                                               <th>Wave</th>
                                               <th>Campaign</th>
                                               <th>Action</th>
                                            </tr>
                                         </thead>
                                         <tbody>

                                            @foreach($pending as $value)
                                         
                                            <tr>
                                               <td>{{$value->trainee->first_name . " " . $value->trainee->middle_name . " " .  $value->trainee->last_name}}</td>
                                               <td>{{!empty($value->trainee->position_id) ? $positions[$value->trainee->position_id] : ""}}</td>
                                               <td>{{!empty($value->wave->name) ? $value->wave->name : ""}}</td>
                                               <td>{{!empty($value->wave->name) ? $campaign[$value->wave->camp_id] : ""}}</td>
                                               <td>{!!view('gem.users.register-modal',['details'=>$value,'dept'=>$dept])!!}</td>
                                           </tr>
                                            @endforeach

                                         </tbody>
                                      </table>
                                   </div>
                                   <div class="tab-pane table-responsive" id="two" role="tabpanel" >
                                      <table class="table table-bordered table-hover">
                                         <thead>
                                            <tr>
                                               <th>Name</th> 
                                               <th>Position</th>
                                               <th>Action</th>
                                            </tr>
                                         </thead>
                                         <tbody>

                                            @foreach($pending_non_agent as $value2)
                                         
                                            <tr>
                                               <td>{{$value2->trainee->first_name . " " . $value2->trainee->middle_name . " " .  $value2->trainee->last_name}}</td>
                                               <td>{{!empty($value2->trainee->position_id) ? $positions[$value2->trainee->position_id] : ""}}</td>
                                               <td>{!!view('gem.users.register-modal',['details'=>$value2,'dept'=>$dept])!!}</td>
                                              
                                           </tr>
                                            @endforeach

                                         </tbody>
                                      </table>
                                   </div>
                                </div>



                        </div>
                    </div>

            </div>




        </section>



@endsection 

@section('footer-scripts')

@endsection