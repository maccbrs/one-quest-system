<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')
        <section class="content-header">
            <h1>Milestone Users</h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">

                                <span class="pull-right"><form action="{{route('gemstone.users.search')}}" method="post">{{ csrf_field() }}<input type="text" class="form-control" name="keyword"  placeholder="Search"></form></span>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Name</th> 
                                            <th>Email</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($users as $u)
                                      
                                         <tr>
                                            <td>{{$u->name}}</td>
                                            <td>{{$u->email}}</td>
                                            <td>

                                               {!!view('gemstone.users.gem-user-access-modal',['id' => $u->id,'access' => $accesslist])!!}
                                               {!!view('gem.users.settings-modal',['user' => $u])!!}
                                               <a class="btn btn-sm btn-primary fa fa-usb" href="{{route('gem.users.connector',$u->id)}}"></a>
                                               {!!view('lilac.user.note-modal',['id' => $u->id,'access' => $accesslist,'userId' => $userId,'notedata' => $notedata,'user_type' => $user_type])!!}
                                               {!!view('lilac.user.password-modal',['details' => $u])!!}
                                               {!!view('lilac.user.emp-code-modal',['details' => $u])!!}
                                               
                                            </td>
                                         </tr>
                                         @endforeach

                                    </tbody>
                                </table>
                                {{$users->links()}}
                            </div>
                        </div>
                    </div>

            </div>




        </section>



@endsection 

@section('footer-scripts')

@endsection