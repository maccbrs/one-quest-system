<button type="button" class="btn btn-primary fa fa-edit pull-right" data-toggle="modal" data-target=".edit-teamlead-modal{{$ps->id}}"></button>
<div class="modal fade edit-teamlead-modal{{$ps->id}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="{{route('gemstone.users.update-ps',$ps->id)}}">
             {{ csrf_field() }}
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Program Supervisor</h4>
                </div>

                <div class="modal-body">
                    <fieldset class="form-group">
                        <label for="exampleSelect1">First Name</label>
                        <input type="text" class="form-control" name="first_name" value="{{$ps->first_name}}" >
                    </fieldset>                    
               
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Middle Name</label>
                        <input type="text" class="form-control" name="middle_name" value="{{$ps->middle_name}}" >
                    </fieldset>                    
               
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Last Name</label>
                        <input type="text" class="form-control" name="last_name"  value="{{$ps->last_name}}">
                    </fieldset>      

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Gem Account</label>
                          <select class="form-control" name="user_id">
                             <option disabled selected>Select Gem Account</option>
                            @foreach($users as $user)
                              <option value="{{$user->id}}" {{($ps->user_id == $user->id?'selected':'')}}>{{$user->name}}</option>
                            @endforeach
                        </select>
                    </fieldset> 

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>

            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

