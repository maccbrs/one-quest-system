<?php $asset = URL::asset('/'); ?> 

@extends('gemstone.master')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>NOC Announcements</h3>
              {!!view('amethyst.announcements.add-modal')!!}
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>Title</th> 
                        <th>Excerpt</th>
                        <th>To</th>
                        <th>#</th>
                     </tr>
                  </thead> 
                  <tbody>
                     @if($items->count())
                        @foreach($items as $i)
                        <tr>
                           <td>{{$i->title}}</td>
                           <td>{{$i->excerpt}}</td>
                           <td>{{$i->destination}}</td>
                           <td>
                              <a class="btn btn-info" href="{{route('gemstone.announcements.get',$i->id)}}">view</a>
                              <a class="btn btn-info" href="{{route('gemstone.announcements.seen',$i->id)}}">({{$seen[$i->id]}})Seen</a>
                           </td>
                        </tr>
                        @endforeach
                     @endif
                  </tbody>
               </table>

            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')


@endsection