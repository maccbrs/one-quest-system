<?php $asset = URL::asset('/'); ?> 

@extends('gemstone.master')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>{{$item->title}}
              </h3>
              <p><a class="btn btn-info" href="{{ URL::previous() }}">back</a></p>
              <p>{{$item->excerpt}}</p>
            </div> 
            <div class="table-responsive">
              <p>{{$item->content}}</p>
            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')


@endsection