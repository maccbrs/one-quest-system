<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'Asset - list all')

@section('content')
		<section class="content-header">
            <h1>Group By: {{$field}} = {{$value}}</h1>
        </section>
        <!-- Main content -->
        <section class="content">

                    <div class="panel ">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <label>Select Group By:</label>
                                {{$Help->print_options2('model',$options,'Model')}}
                                {{$Help->print_options2('status',$options,'Status')}}
                                {{$Help->print_options2('supplier',$options,'Supplier')}}
                                {{$Help->print_options2('default_location',$options,'Default Location')}}
                                {{$Help->print_options2('asset_type',$options,'Asset Type')}}
                                {{$Help->print_options2('workstation',$options,'Workstation')}}                                
                                <a class="btn btn-sm btn-primary pull-right" href="{{route('gemstone.asset.add')}}">Add</a>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Asset Tag</th>
                                        <th>Serial</th>
                                        <th>Name</th>
                                        <th>Model</th>
                                        <th>Purchase Date</th>
                                        <th>Supplier</th>
                                        <th>Date Entry</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($items as $item)
	                                    <tr>
	                                        <td>{{$item->asset_tag}}</td>
                                            <td>{{$item->serial}}</td>
	                                        <td><a href="{{route('gemstone.asset.view',$item->id)}}">{{$item->asset_name}}</a></td>
	                                        <td><a href="{{route('gemstone.asset.group',['field' => 'model','value' => $item->model])}}">{{$item->model}}</a></td>
	                                        <td><a href="{{route('gemstone.asset.group',['field' => 'purchase_date','value' => $item->purchase_date])}}">{{$item->purchase_date}}</a></td>
	                                        <td><a href="{{route('gemstone.asset.group',['field' => 'supplier','value' => $item->supplier])}}">{{$item->supplier}}</a></td>
                                            <td><a href="{{route('gemstone.asset.group',['field' => 'created_at','value' => $item->created_at])}}">{{$item->created_at}}</a></td>
	                                    </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                                {{$items->links()}}
                            </div>
                        </div>
                    </div>

        </section>
@endsection 

@section('footer-scripts')

@endsection