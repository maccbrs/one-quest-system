<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'Asset - Add New')

@section('header-scripts')
    <link href="{{$asset}}gemstone/vendors/airdatepicker/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
		<section class="content-header">
            <h1>New Assets</h1>
        </section>
        <!-- Main content -->
        <section class="content">

                    <div class="panel ">
                        <div class="panel-body">
                            <form method="post" action="{{route('gemstone.asset.create')}}" class="form-horizontal p-10">
                                {{ csrf_field() }}
                                <div class="form-group p10">
                                    <label for="asset_tag" class="col-sm-2 control-label">Asset Tag
                                    </label>
                                    <div class="col-sm-8">
                                        <input id="asset_tag" name="asset_tag" type="text" class="form-control required"/>
                                    </div>
                                    <div class="col-sm-2">
                                    </div>   
                                </div> 

                                <div class="form-group">
                                    <label for="model" class="col-sm-2 control-label">Details
                                    </label>
                                    <div class="col-sm-8">
                                        <input  name="details" type="text" class="form-control required"/>
                                    </div>
                                    <div class="col-sm-2">
                                                                                    
                                    </div>                                   
                                </div>
                                
                                <div class="form-group">
                                    <label for="serial" class="col-sm-2 control-label">Serial
                                    </label>
                                    <div class="col-sm-8">
                                        <input name="serial" type="text" class="form-control required"/>
                                    </div>
                                    <div class="col-sm-2">
                                    </div>                                   
                                </div> 

                                <div class="form-group">
                                    <label for="asset_name" class="col-sm-2 control-label">Asset Name
                                    </label>
                                    <div class="col-sm-8">
                                        <input name="asset_name" type="text" class="form-control required"/>
                                    </div>
                                    <div class="col-sm-2">
                                    </div>                                   
                                </div> 

                                <div class="form-group">
                                    <label for="purchase_date" class="col-sm-2 control-label">Purchase Date
                                    </label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-fw fa-calendar"></i>
                                            </div>
                                            <input type="text" name="purchase_date" class="form-control pull-right" data-language='en' id="my-element" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                    </div>                                   
                                </div> 

                                <div class="form-group">
                                    <label for="order_no" class="col-sm-2 control-label">Order Number
                                    </label>
                                    <div class="col-sm-8">
                                        <input name="order_no" type="text" class="form-control required"/>
                                    </div>
                                    <div class="col-sm-2">
                                    </div>                                   
                                </div>

                                <div class="form-group">
                                    <label for="purchase_cost" class="col-sm-2 control-label">Purchase Cost
                                    </label>
                                    <div class="col-sm-8">
                                        <input name="purchase_cost" type="text" class="form-control required"/>
                                    </div>
                                    <div class="col-sm-2">
                                    </div>                                   
                                </div>                                

                                <div class="form-group">
                                    <label for="warranty" class="col-sm-2 control-label">Warranty
                                    </label>
                                    <div class="col-sm-8">
                                        <input name="warranty" type="text" class="form-control required"/>
                                    </div>
                                    <div class="col-sm-2">
                                    </div>                                   
                                </div>

                                <div class="form-group">
                                    <label for="notes" class="col-sm-2 control-label">Notes
                                    </label>
                                    <div class="col-sm-8">
                                        <input name="notes" type="text" class="form-control required"/>
                                    </div>
                                    <div class="col-sm-2">
                                    </div>                                   
                                </div>
                                @foreach($options as $opt)
                                <div class="form-group">
                                    <label for="model" class="col-sm-2 control-label">{{$Help->proper_title($opt->type)}}
                                    </label>
                                    <div class="col-sm-8">
                                        {{$Help->print_options($opt->type,$options)}}
                                    </div>
                                    <div class="col-sm-2">
                                            <span class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add-{{$opt->type}}" data-placement="top"><span class="fa fa-fw fa-plus"></span></span>                                        
                                    </div>                                   
                                </div>
                                @endforeach

                                <div class="form-group p-10 form-actions">
                                    <div class="col-md-offset-4 col-md-8">
                                        <button type="submit" class="btn btn-primary">Submit
                                        </button>
                                    </div>
                                </div>

                            </form>                            
                        </div>
                    </div>

        </section>

        @foreach($options as $opt)
            <div class="modal fade" id="add-{{$opt->type}}" tabindex="-1" role="dialog" aria-labelledby="add-{{$opt->type}}" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post" action="{{route('gemstone.asset.create-options',$opt->type)}}" >
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title custom_align" >Add {{$Help->proper_title($opt->type)}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control" name="{{$opt->type}}" type="text" placeholder="{{$Help->proper_title($opt->type)}}">
                                </div>
                            </div>
                            <div class="modal-footer ">
                                <button type="submit" class="btn btn-primary" >
                                    <span class="glyphicon glyphicon-ok-sign"></span> Save
                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        @endforeach

@endsection 

@section('footer-scripts')
    <script src="{{$asset}}gemstone/vendors/airdatepicker/js/datepicker.min.js" type="text/javascript"></script>
    <script src="{{$asset}}gemstone/vendors/airdatepicker/js/datepicker.en.js" type="text/javascript"></script>
    <script type="text/javascript">
     $('#my-element').datepicker({ dateFormat: 'yy-m-d' });
    </script>
@endsection