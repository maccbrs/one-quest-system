<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'Item')

@section('content')
		<section class="content-header">
            <h1>{{$item->asset_name}}</h1>
        </section>
        <!-- Main content -->
        <section class="content">

                <div class="col-sm-8 col-sm-offset-2">
                    <div class="panel ">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="ti-menu"></i> Asset Detail
                            </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw ti-angle-up clickable"></i>
                                    <i class="fa fa-fw ti-close removepanel clickable"></i>
                                </span>
                        </div>
                        <div class="panel-body">
                            <div class="box-body"> 
                                <dl class="dl-horizontal">

                                    <dt>
                                        Asset Name
                                    </dt>
                                    <dd>
                                        {{$item->asset_name}}
                                    </dd>

                                    <!-- *** -->
                                    <dt>Details</dt>
                                    <dd>
                                        {{$item->details}}
                                    </dd> 

                                    <dt>Asset Tag</dt> 
                                    <dd>
                                        {{$item->asset_tag}}
                                    </dd>
                                    <dt>
                                        Model
                                    </dt>
                                    <dd>
                                        {{$item->model}}
                                    </dd>
                                    <dt>Status</dt>
                                    <dd>
                                        {{$item->status}}
                                    </dd>
                                    <dt>
                                        Serial
                                    </dt>
                                    <dd>
                                        {{$item->serial}}
                                    </dd>
                                    <dt>Order No</dt>
                                    <dd>
                                        {{$item->order_no}}
                                    </dd>
                                    <!-- *** -->
                                    <dt>Purchase Date</dt>
                                    <dd>
                                        {{$item->purchase_date}}
                                    </dd>
                                    <!-- *** -->
                                    <dt>Purchase Cost</dt>
                                    <dd>
                                        {{$item->purchase_cost}}
                                    </dd>
                                    <!-- *** -->
                                    <dt>Supplier</dt>
                                    <dd>
                                        {{$item->supplier}}
                                    </dd>                                    
                                    <!-- *** -->
                                    <dt>Warranty</dt>
                                    <dd>
                                        {{$item->warranty}}
                                    </dd>


                                    <!-- *** -->
                                    <dt>Notes</dt>
                                    <dd>
                                        {{$item->notes}}
                                    </dd>
                                    <!-- *** -->
                                    <dt>Default Location</dt>
                                    <dd>
                                        {{$item->default_location}}
                                    </dd> 
                                    <!-- *** -->
                                    <dt>Workstation</dt>
                                    <dd>
                                        {{$item->workstation}}
                                    </dd> 
                                    <dt>Memory</dt>
                                    <dd>
                                        {{$item->memory}}
                                    </dd>  
                                    <dt>Processor</dt>
                                    <dd>
                                        {{$item->processor}}
                                    </dd>                                                                                                               
                                    <!-- *** -->
                                    <dt>License</dt>
                                    <dd>
                                      @if(!$item->ObjLicense)
                                      <span class="btn btn-primary btn-xs" data-toggle="modal" data-target="#assignLicense" data-placement="top">assign</span>
                                      @else
                                        {{$item->ObjLicense->license}} <span class="btn btn-primary btn-xs" data-toggle="modal" data-target="#reAssignLicense" data-placement="top">re assign</span>
                                      @endif
                                    </dd>                                                                                                                                                                                                                                 
                                </dl>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

        </section>


            @if(!$item->ObjLicense)
            <div class="modal fade" id="assignLicense" tabindex="-1" role="dialog" aria-labelledby="assignLicense" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post" action="{{route('gemstone.license.assign',$item->id)}}" >
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title custom_align" >Assign License</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <select name="license">
                                        @foreach($licenses as $license)
                                        <option value="{{$license->id.'_'.$license->license.' - '.$license->product_key}}">{{$license->license.' - '.$license->product_key}}</option>
                                        @endforeach 
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer ">
                                <button type="submit" class="btn btn-primary" >
                                    <span class="glyphicon glyphicon-ok-sign"></span> Save
                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            @else
            <div class="modal fade" id="reAssignLicense" tabindex="-1" role="dialog" aria-labelledby="reAssignLicense" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post" action="{{route('gemstone.license.reassign',$item->ObjLicense->id)}}" >
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title custom_align" >Re Assign License</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <select name="license">
                                        @foreach($licenses as $license)
                                        <option value="{{$license->id.'_'.$license->license.' - '.$license->product_key}}">{{$license->license.' - '.$license->product_key}}</option>
                                        @endforeach 
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer ">
                                <button type="submit" class="btn btn-primary" >
                                    <span class="glyphicon glyphicon-ok-sign"></span> Save
                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            @endif           

@endsection 

@section('footer-scripts')

@endsection