<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')
@section('header-scripts')
<style type="text/css">
.color-lightgreen{
  /*  background-color: #F0FFF0;*/
}
.color-lightred{
   /* background-color: #ffe5e5;*/
    font-weight: bold;
}
.tr-head{
    background-color: #ebebeb;
    color: #777;
}

.tr-linethrough{
    text-decoration: line-through;
}
.assignedUser{
        padding: 2px;
    padding-left: 10px;
    padding-right: 30px;
margin-top: -5px;
    margin-bottom: -2px;    
}

</style>
@endsection
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header no-bg b-a-0">Tickets</div>
            <div class="card-block">
                <div class="table-responsive">
                    <table class="table table-sm">
                        <thead>
                            <tr class="tr-head">
                                <th>Date</th>
                                <th>Title</th>
                                <th>Created By</th>
                                <th>Reply</th>
                                <th>Last Updated</th>
                                <th>Last Updated By</th>
                                <th>Assigned</th> 
                            </tr>
                        </thead> 
                        <tbody>

                            @foreach($items as $item)
                            <tr class="<?php if($item->reply): $reply = $help->last_reply($item->reply); echo (in_array($reply['user_id'], $admins)?'color-lightgreen':'color-lightred'); endif; ?> {{($item->status == 'closing'?'tr-linethrough':'')}}">
                                <td>{{$item->created_at->setTimezone('Asia/Manila')}}</td>
                                <td><a class="text-primary" href="{{route('gemstone.department.ticket',$item->id)}}">{{$item->title}}</a></td>
                                <td>{{($item->user?$item->user->name:'')}}</td>
                                <td>{{$item->reply->count()}}</td>
                                <td>@if($item->reply->count()) <?php $reply = $help->last_reply($item->reply); echo $help->time_ago($reply['created_at']); ?> @endif</td>
                                <td>@if($item->reply->count()) <?php $reply = $help->last_reply($item->reply); echo $reply['user']['name']; ?> @endif</td>
                                <td>
                                    <select class="custom-select assignedUser" data-ticketid="{{$item->id}}">
                                        <option value="0">None</option>
                                        @foreach($users as $u)
                                        <option value="{{$u->user_id}}" {{($item->assigned_id == $u->user_id?'selected':'')}}>{{($u->user?$u->user->name:'')}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div> 
                                                                    

@endsection  

@section('footer-scripts')
<script type="text/javascript">

    !function(a) {
        a.mb_lib = {
            mb_ajax: function(b) {
                var c = {
                    data: "null",
                    url: "null",
                    type: "post",
                    attachto: "null"
                };
                if (b) a.extend(c, b);
                a.ajax({
                    url: c["url"],
                    data: c["data"],
                    type: c["type"],
                    success: function(b) {
                        a(c["attachto"]).html(b);
                        console.log(b);
                    },
                    error: function(a) {
                        console.log(a);
                    }
                });
            }
        }; 

        $(".assignedUser").on('change',function(){
            console.log();
            var a = {
                data: { 
                    "_token": "{{ csrf_token() }}",
                    "ticket_id":$(this).data('ticketid'),
                    "user_id":$(this).val()
                },
                url: "{{route('gemstone.department.assign')}}",
                attachto: ".jy-form"
            }; 
             $.mb_lib.mb_ajax(a);          
        });

            
    }(jQuery);


</script>
@endsection