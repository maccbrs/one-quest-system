<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".add-modal">
<i class="material-icons" aria-hidden="true">add</i>
</button>

    <div class="modal fade add-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                    <div class="card">
                        <div class="card-header no-bg b-a-0">Add Campaign</div>
                        <div class="card-block">

                            <form id="form-submit" action="{{route('gemstone.department.store')}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 form-control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                </div> 
                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

