<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('header-scripts')
    <link rel="stylesheet" href="{{$asset}}milestone/vendor/summernote/dist/summernote.css">
@endsection

@section('content')
                        <div class="col-lg-12">
                            <div class="card card-block">

                                 <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default btn-sm {{($item->status == 'open'?'active':'')}} foreclosed">
                                        <input type="radio" name="status" autocomplete="off" value="open">open</label>
                                    <label class="btn btn-default btn-sm {{($item->status == 'closing'?'active':'')}} foreclosed">
                                        <input type="radio" name="status" autocomplete="off" value="closing">closing</label>
                                </div>    
                           
                                <div class="profile-timeline-header">
                                    <a href="#" class="profile-timeline-user"><img src="{{asset('uploads/sm-'.$item->user->avatar)}}" alt="" class="img-rounded">
                                    </a>
                                    <div class="profile-timeline-user-details">
                                        <a href="#" class="bold">
                                        [{{$item->user->name}}] <span class="ticket-title">{{$item->title}}</span>
                                    </a>
                                        <br><span class="text-muted small">{{$item->created_at}}</span>
                                    </div>
                                </div>
                                <div class="profile-timeline-content">
                                    {!!$item->content->content!!}
                                </div>

                                @foreach($item->reply as $reply)
                                    <div class="profile-timeline-header">
                                        <a href="#" class="profile-timeline-user"><img src="{{asset('uploads/sm-'.$reply->user->avatar)}}" alt="" class="img-rounded">
                                        </a>
                                        <div class="profile-timeline-user-details">
                                            <a href="#" class="bold">
                                            [{{$reply->user->name}}]
                                        </a>
                                            <br><span class="text-muted small">{{$reply->created_at}}</span>
                                        </div>
                                    </div>
                                    <div class="profile-timeline-content">
                                        {!!$reply->content->content!!}
                                    </div>                                
                                @endforeach
                                <div>
                                    <div class="summernote"></div> 
                                    <button class="btn btn-primary" onclick="save()" type="button">Submit</button>
                                </div> 
                                <form id="form-submit" action="{{route('topaz.you.reply')}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="content" id="input-content-reply">
                                    <input type="hidden" name="ticket_id" value="{{$item->id}}">
                                </form>                                                               
                            </div> 
                        </div>
                                                                    

@endsection 

@section('footer-scripts')
<script src="{{$asset}}milestone/vendor/summernote/dist/summernote.js"></script>
<script type="text/javascript">

var save = function() {
  $('#input-content-reply').val($('.summernote').summernote('code'));
  $("#form-submit").submit();
  $('.summernote').summernote('destroy');
};

$(document).ready(function(){ 
    setTimeout(function(){ $('.summernote').summernote({
       height: 200,
       width:300,
       popover: {
         image: [],
         link: [],
         air: []
       }
     }); }, 500);
});

!function(a) {
    a.mb_lib = {
        mb_ajax: function(b) {
            var c = {
                data: "null",
                url: "null",
                type: "post",
                attachto: "null"
            };
            if (b) a.extend(c, b);
            a.ajax({
                url: c["url"],
                data: c["data"],
                type: c["type"],
                success: function(b) {
                    a(c["attachto"]).html(b);
                },
                error: function(a) {
                    console.log(a);
                }
            });
        }
    }; 

    $(".foreclosed").on('click',function(){
        var a = {
            data: { "_token": "{{ csrf_token() }}","content":$(this).children("input").val(),"id":"{{$item->id}}"},
            url: "{{route('topaz.department.status')}}",
            attachto: ".jy-form"
        }; 
        $.mb_lib.mb_ajax(a);              
    });

        
}(jQuery);



</script>
@endsection