<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".add-user">
<i class="material-icons" aria-hidden="true">add</i>
</button>

    <div class="modal fade add-user" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                    <div class="card">
                        <div class="card-block">

                            <form id="form-submit" action="{{route('gemstone.department.user_store')}}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="campaign_id" value="{{$id}}">
                                <div class="form-group row">
                                    <label class="col-sm-2 form-control-label">Select Users</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select" name="user_id">
                                            <option value="">Select Users</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> 
                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>