<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')
        <section class="content-header">
            <h1>Biometrics and Proximity </h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">

                                <button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#add-bio" data-placement="top"><span class="fa fa-fw fa-plus"></span></button>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Detail</th>
                                            <th>Latest Status</th>
                                            <th>Date checked</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($items as $item)
                                        <tr>
                                            <td>{{$item->id}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->detail}}</td>
                                            <td><span class="label label-sm label-success">ok</span></td>
                                            <td>2017-03-02 06:51:04</td>
                                            <td>
                                                some remarks
                                            </td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                                {{$items->links()}}
                            </div>
                        </div>
                    </div>

            </div>




        </section>

            <div class="modal fade" id="add-bio" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="POST" action="{{route('gemstone.bio.create')}}">
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title custom_align">Add new bio unit</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control " type="text" placeholder="Name" name="name">
                                </div>
                                <div class="form-group">
                                    <input class="form-control " type="text" placeholder="Detail" name="detail">
                                </div>
                            </div>
                            <div class="modal-footer ">
                                <button type="submit" class="btn btn-success">
                                    <span class="glyphicon glyphicon-ok-sign"></span> Submit
                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

@endsection 

@section('footer-scripts')

@endsection