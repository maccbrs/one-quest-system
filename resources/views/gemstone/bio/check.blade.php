<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')
        <section class="content-header">
            <h1>Check BioProx status </h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">


                            <?php $count = 0; ?>
                            @if($checklist)
                                <div class="panel">
                                    <div class="panel-header">
                                        {{$carbon->createFromFormat('Y-m-d H:i:s',$checklist->created_at)->toDayDateTimeString()}}
                                    </div>
                                    <div class="panel-body">
                                            @foreach($checklist->bio_statuses as $item)
                                                <label>{{$item->bio->name}}</label>
                                                <input type="checkbox" data-ccs-id="{{$item->id}}"  name="my-checkbox" data-on-text="passed" data-off-text="failed" data-on-color="info" data-off-color="primary" data-animate {{($item->status?'checked':'')}}>                                
                                                <?php $count++; $count = ($count==5?0:$count); ?>
                                            @endforeach
                                    </div>
                                   <form method="POST" action="{{route('gemstone.bio.submit-checklist')}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{$checklist->id}}">
                                        <button type="submit" class="btn btn-success pull-right">Submit Checklist</button>
                                   </form>                                    
                                </div>                                
                            @else
                               <form method="POST" action="{{route('gemstone.bio.check-status',['status'=>'new'])}}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-info">Create new checklist?</button>
                               </form>                            
                            @endif



            </div>


        </section>



@endsection 

@section('footer-scripts')
<link href="{{$asset}}gemstone/vendors/bootstrap-switch/css/bootstrap-switch.css" rel="stylesheet" type="text/css">
<script src="{{$asset}}gemstone/vendors/bootstrap-switch/js/bootstrap-switch.js" type="text/javascript"></script>
<script type="text/javascript">

!function(a) {
    a.mb_lib = {
        mb_ajax: function(b) {
            var c = {
                data: "null",
                url: "null",
                type: "post",
                attachto: "null"
            };
            if (b) a.extend(c, b);
            a.ajax({
                url: c["url"],
                data: c["data"],
                type: c["type"],
                success: function(b) {
                    a(c["attachto"]).html(b);
                },
                error: function(a) {
                    console.log(a);
                }
            });
        }
    }; 

    $("[name='my-checkbox']").on('switchChange.bootstrapSwitch',function(event, state){
        console.log('hi');
        var $this = $(this);
        var a = {
            data: { "_token": "{{ csrf_token() }}","status":state,"id":$this.data('ccs-id')},
            url: "{{route('gemstone.bio.update-status')}}",
            attachto: ".jy-form"
        }; 
        $.mb_lib.mb_ajax(a);         

    });
    $("[name='my-checkbox']").bootstrapSwitch("size","mini");
        
}(jQuery);





</script>
@endsection