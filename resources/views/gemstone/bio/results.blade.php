<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')
        <section class="content-header">
            <h1>BioProx Monitoring </h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">

                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Passed</th>
                                            <th>Failed</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($items as $item)
                                            <tr>
                                                <td>{{$carbon->createFromFormat('Y-m-d H:i:s',$item->created_at)->toDayDateTimeString()}}</td>
                                                <td> <a href="{{route('gemstone.bio.result',['checklist' => $item->id,'status' => 'offline'])}}" class="label label-sm label-danger">{{$item->offline->count()}}</a></td>
                                                <td><a href="{{route('gemstone.bio.result',['checklist' => $item->id,'status' => 'online'])}}" class="label label-sm label-success">{{$item->online->count()}}</a></td>
                                                <td><a href="{{route('gemstone.bio.result',['checklist' => $item->id,'status' => 'all'])}}" class="label label-sm label-primary">{{$item->offline->count()+$item->online->count()}}</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                               
                            </div>
                        </div>
                    </div>

            </div>




        </section>



@endsection 

@section('footer-scripts')

@endsection