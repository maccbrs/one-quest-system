<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')
        <section class="content-header">
            <h1>DIDs List {!!view('gemstone.did.add-modal')!!}</h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                       <tr>
                                          <th>Number</th> 
                                          <th>Campaign</th>
                                          <th>Last test call</th>
                                          <th>Last Status</th>
                                          <th>#</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       @if($items->count())
                                          @foreach($items as $item)
                                             <tr>
                                                <td><a href="{{route('gemstone.did.dids',$item->id)}}" style="font-size:100%;" class="label label-sm label-primary">{{$item->phone}}</a></td>
                                                <td>{{$item->campaign}}</td>
                                                <td>{{$item->last_test_call->created_at}}</td>
                                                <td><span class="label label-sm label-{{($item->last_test_call->status =='passed'?'success':'danger')}}">{{$item->last_test_call->status}}</span></td>
                                                <td>
                                                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target=".del{{$item->id}}"><i class="fa fa-fw fa-trash-o"></i></button class="btn btn-sm btn-primary">
                                                    <div class="modal fade del{{$item->id}}"  tabindex="-1" role="dialog" aria-hidden="true">
                                                      <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <form class="form-horizontal" role="form" method="POST" action="{{route('gemstone.did.destroy',$item->id)}}">
                                                                {{ csrf_field() }}
                                                                    
                                                                        <h4 class="modal-title" style="text-align:center;">Are you sure you want to delete {{$item->phone}}?</h4>
                                                                                                                                    
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                        <button type="submit" class="btn btn-primary">Proceed</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                      </div>
                                                    </div> 

                                                </td>
                                             </tr>
                                          @endforeach
                                       @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

            </div>

        </section>


@endsection 

@section('footer-scripts')

@endsection