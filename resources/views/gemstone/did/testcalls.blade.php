<?php $asset = URL::asset('/'); ?> 
@extends('gemstone.master')

@section('title', 'index')

@section('content')
        <section class="content-header">
            <h1>Test Calls {!!view('gemstone.did.addtestcalls-modal')!!}</h1>
            <div class="clearfix"></div>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                       <tr>
                                          <th>Date</th> 
                                          <th>Total</th>
                                          <th>Passed</th>
                                          <th>Failed</th> 
                                       </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($items as $item)
                                          <tr>
                                             <td>{{$item->created_at}}</td>
                                             <td><a href="{{route('gemstone.did.testcall',$item->id)}}" class="label label-sm label-primary">{{$item->total}}</a></td>
                                             <td><a href="{{route('gemstone.did.passed',$item->id)}}" class="label label-sm label-success">{{$item->passed}}</a></td>
                                             <td><a href="{{route('gemstone.did.failed',$item->id)}}" class="label label-sm label-danger">{{$item->failed}}</a></td>
                                          </tr>
                                       @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

            </div>

        </section>


@endsection 

@section('footer-scripts')

@endsection