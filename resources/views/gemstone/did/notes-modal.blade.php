<button class="btn btn-primary btn-sm pull-right"  data-toggle="modal" data-target=".notes{{$id}}">
  <i class="fa fa-fw fa-comment"></i>
</button> 
<div class="modal fade notes{{$id}}"  tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Notes</h4>
        </div>

        <div class="modal-body">
          <form class="form-horizontal" role="form" method="POST" action="{{route('gemstone.did.notes',$id)}}">
            {{ csrf_field() }}

          <div class = "row">
             <div class="form-group">
                  <div class="col-md-12">
                      <textarea class="form-control" name="notes" rows="3">{{$notes}}</textarea>
                  </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
          </div>

        </form>
      </div>
  </div>
</div> 