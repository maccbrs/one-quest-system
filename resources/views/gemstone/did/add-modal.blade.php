<button class="btn btn-primary btn-sm pull-right"  data-toggle="modal" data-target=".add-did">
  <i class="fa fa-fw fa-plus"></i>
</button> 
<div class="modal fade add-did" id="editmodal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Add New Did</h4>
        </div>

        <div class="modal-body">
          <form class="form-horizontal" role="form" method="POST" action="{{route('gemstone.did.create')}}">
            {{ csrf_field() }}

            <input type="hidden" value = "1"  name = "user_level" > 

          
          <div class = "row">
             <div class="form-group">
                <label class="col-md-4 control-label">Phone Number </label>
                  <div class="col-md-6">
                      <input type="text" class="form-control" name="phone" >
                  </div>
              </div>
             <div class="form-group">
                <label class="col-md-4 control-label">Campaign</label> 
                  <div class="col-md-6">
                      <input type="text" class="form-control" name="campaign" >
                  </div>
              </div>              
            </div>
          

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
          </div>

        </form>
      </div>
  </div>
</div> 