<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <div>
                <section class="pull-right">
                  {!!view('amethyst.announcements.announcement-delete-modal',compact('item'))!!}
                  {!!view('amethyst.announcements.announcement-update-modal',compact('item'))!!}
                </section>

                <h3>{{$item->title}} </h3>

              </div>
              <p><a class="btn btn-info" href="{{ URL::previous() }}">back</a></p>
              <p>{{$item->excerpt}}</p>
            </div> 
            <div class="table-responsive">
              <p>{!! ($item->content) !!}</p>
            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')

  <script src="{{$asset}}vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  <script src="{{$asset}}vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
  <script>
      $('#texteditor').ckeditor();
      // $('.textarea').ckeditor(); // if class is prefered.
  </script>

@endsection