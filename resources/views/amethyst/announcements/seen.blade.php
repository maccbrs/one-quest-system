<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>Seen by:</h3>
            </div> 
            <div class="table-responsive">
               <ul>
                  @foreach($items as $item)
                  <li>{{$item->userObj->name}} : {{$item->created_at}}</li>
                  @endforeach
               </ul>
            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection

@section('footer-scripts')


@endsection