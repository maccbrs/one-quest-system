
<a class="btn btn-warning"  data-toggle="modal" data-target=".delete-modal-{{$item->id}}" style = "color: white">Delete</a>

<div class="modal fade delete-modal-{{$item->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" >
               
                <div class="card">
                    <div class="card-header no-bg b-a-0">Are you sure you want to delete this announcement?</div>
                    <div class="card-block">
                        <form method="post" action="{{route('gem.announcements.announcement-delete')}}" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$item->id}}">
                             <button type="submit" class="btn btn-warning" >Delete</button>
                        </form>  
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>