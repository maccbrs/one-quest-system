<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target=".attendance-modal{{$details->id}}">
  <i class="material-icons" aria-hidden="true">edit </i>
</button>
<div class="modal fade attendance-modal{{$details->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('amethyst.noc_stat.attendance',$details->id)}}" novalidate>
        {{ csrf_field() }}    

          <input type="hidden" value = "{{$details->id}}"  name = "noc_id" > 

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Attendace Details</h4>
          </div>
          <div class="modal-body">

          <div class = "row">
            <div class="form-group">
              <div class="col-xs-2">
                <label for="recipient-name" class="control-label">Late</label>
              </div>
              <div class="col-xs-10">
                <input type="text" class = "form-control" value = "{{($details->attendance?$details->attendance->late  :'0')}}"  name = "lates" > 
              </div>
            </div> 
           </div>

          <div class = "row">
            <div class="form-group">
              <div class="col-xs-2">
                <label for="recipient-name" class="control-label">Missed Logs</label>
              </div>
              <div class="col-xs-10">
                <input type="text" class = "form-control" value = "{{($details->attendance?$details->attendance->missed_logs  :'0')}}"  name = "missed_logs" > 
              </div>
            </div> 
           </div>

          </div>  

          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
  </div>
</div> 