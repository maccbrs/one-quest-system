<?php $asset = URL::asset('/'); ?> 

@extends('gem.master2')

@section('title', 'index')

@section('content')

      <div class="card">

         <div class="card-block">
            <div>
              <h3>NOC Statistics</h3>
              
            </div> 
            <div class="table-responsive">
               <table class="table m-b-0">
                  <thead>
                     <tr>
                        <th>Name</th> 
                        <th>Position</th>
                        <th># of Late</th>
                        <th># of Misssed Logs</th>
                        <th>Action</th>
                     </tr>
                  </thead> 
                  <tbody>
                    
                     @foreach($noc as $a)
                        <tr>
                           <td>{{($a->last_name?$a->last_name . ", " . $a->first_name  :'')}}</td>
                           <td>{{( $a->type == '1' ) ? 'Support' :( $a->type == '2' ? 'Developer' : 'HelpDesk')}}</td>
        <!--                    <td>{{(($a->type == 1) ? 'Support' : ($a->type == 2) ? 'Developer' : 'Helpdesk')}}</td> -->
                           <td>{{($a->attendance?$a->attendance->late  :'0')}}</td>
                           <td>{{($a->attendance?$a->attendance->missed_logs  :'0')}}</td>
                           <td>{!!view('amethyst.noc_stats.attendance-modal',['details'=>$a])!!}</td>
                        </tr>
                     @endforeach
                 
                  </tbody> 
               </table>

            </div>
         </div>
      </div>


@endsection 

@section('header-scripts')

 
@endsection