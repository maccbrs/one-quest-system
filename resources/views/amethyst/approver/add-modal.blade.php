<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target=".add-modal">
  <i class="material-icons" aria-hidden="true">add </i>
</button>
<div class="modal fade add-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('amethyst.approver.create')}}" novalidate>
        {{ csrf_field() }}                       

          <div class="modal-body">

            <fieldset class="form-group">
                <label for="exampleSelect1">emp</label>
                <select class="form-control" name='level1'>
                    @foreach($items as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </fieldset>              
            <fieldset class="form-group">
                <label for="exampleSelect1">sup</label>
                <select class="form-control" name='level2'>
                    @foreach($items as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </fieldset>                                 
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Add</button>
          </div>
        </form>
      </div>
  </div>
</div> 