

<div class="modal fade modalDelete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="<?php echo $tl_list['form-action'] ?>" id="edit-form">
                {{ csrf_field() }}
				
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title custom_align">Are You Sure want to delete <span id="desc"></span></h4>
                </div>
                <div class="modal-body" style="display:none">
							

							@foreach($tl_list['form-list'] as $tl => $tls)
                              <?php 
							  $fields_param = explode("|", $tls);
							  if ($fields_param[0] ==='text') {
								  ?>
							  <fieldset class="form-group"> 
											<label for="exampleSelect1">{{$fields_param[1]}}</label> 
											<input type="{{$fields_param[0]}}" class="form-control class_{{$fields_param[2]}}" name="{{$fields_param[2]}}" >
							  </fieldset>
							  <?php 
							  }
							  else if ($fields_param[0] ==='hidden') {
							  ?>
							
								
								<input type="{{$fields_param[0]}}" class="form-control class_{{$fields_param[2]}}" name="{{$fields_param[2]}}" >
							
							  
							  <?php
							  }
							  
							  else if ($fields_param[0] ==='textarea') {
							  ?>
							 <fieldset class="form-group">
								<label for="exampleSelect1">{{$fields_param[1]}}</label>
								<textarea class="form-control class_{{$fields_param[2]}}" name="{{$fields_param[2]}}" ></textarea>
							 </fieldset>  
							  
							  <?php
							  }
							  ?>
							  
                            @endforeach					
				
					
                    
                </div>
                <div class="modal-footer ">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-sign"></span> Delete
                    </button>
                </div>
            </form>
        </div> 
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>