               <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target=".bd-example-modal">
                  <i class="material-icons" aria-hidden="true">add </i>
               </button>
               <div class="modal fade bd-example-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                      <div class="modal-content">
                        <form method="post" action="{{route('gem.campaign.store')}}" novalidate>
                        {{ csrf_field() }}                        
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                              </button>
                              <h4 class="modal-title" id="myModalLabel">Add new campaign</h4>
                          </div>
                          <div class="modal-body">
                                  <div class="form-group">
                                      <label >Campaign Name</label>
                                      <input type="input" class="form-control" name="campaign_name">
                                  </div>
                                  <div class="form-group">
                                      <label >Campaign Id</label>
                                      <input type="input" class="form-control" name="campaign_id">
                                  </div>
                                  <button type="submit" class="btn btn-default">Submit</button>
                              
                          </div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                          </div>
                        </form>
                      </div>
                  </div>
               </div>    