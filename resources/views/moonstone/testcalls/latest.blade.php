<?php $asset = URL::asset('/'); ?> 
@extends('moonstone.master')

@section('title', 'Latest Test Calls')

@section('content')

   <div class="card">
      <div class="card-header no-bg b-a-0">

         <h5>Test Call Date: {{$item->created_at->format('l F d Y, H:i:s')}}</h5>
      </div>
      <div class="card-block">
         <div class="table-responsive">

              <table class="table table-sm">
                  <thead>
                     <tr>
                        <th>Number</th> 
                        <th>Last test call</th>
                        <th>Last Status</th>
                        <th>Notes</th>
                     </tr>
                  </thead>
                  <tbody>
                    @if($item->obj_testcalls->count())
                      @foreach($item->obj_testcalls as $i)
                      <tr>
                        <td>{{$i->obj_did->phone}}</td>
                        <td>{{$i->updated_at}}</td>
                        <td>{{$i->status}}</td>
                        <td>{{$i->notes}}</td>
                      </tr>
                      @endforeach
                    @endif
                  </tbody>
             </table>
             

    
         </div>
      </div>
   </div>


@endsection 

@section('footer-scripts')

@endsection