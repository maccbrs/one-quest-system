<?php $asset = URL::asset('/'); ?> 
<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="theme-color" content="#4C7FF0">
      <title>Login</title>
      <link rel="stylesheet" href="{{$asset}}milestone/styles/app.min.css">
      <style type="text/css">

      
      </style>
   </head>
   <body>
         <div class="main-panel">

            <div class="main-content">
               <div class="content-view">
                  @yield('content')
               </div>
            </div>
 
         </div> 
      <script type="text/javascript">

      window.paceOptions = {
         document: true,
         eventLag: true,
         restartOnPushState: true,
         restartOnRequestAfter: true,
         ajax: {
           trackMethods: [ 'POST','GET']
         }
         };



      </script>
      <script src="{{$asset}}milestone/scripts/app.min.js"></script> 
      <script src="{{$asset}}milestone/vendor/jquery-validation/dist/jquery.validate.min.js"></script>  
      <script type="text/javascript">$('#validate').validate();</script> 

      @yield('footer-scripts')
   </body>

</html>