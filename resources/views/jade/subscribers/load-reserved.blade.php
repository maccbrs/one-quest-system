@extends('jade.master')

@section('title', 'Users|Index')

@section('content')

        <div class="block-header">
            <h2>Subscriber: {{$subscriber->client}} <a href="{{route('jade.subscribers.index')}}"class="pull-right"><i class="material-icons">reply</i></a></h2>
            {!!view('jade.errors')!!}
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Reserved</h2>


                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">more_vert</i> </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);"  data-toggle="modal" data-target="#add-minute" >Add Minutes</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <div class="body">

                         <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Minutes</th>
                                    <th>Used</th>
                                    <th>Remaining</th>
                                </tr>
                            </thead>                            
                            <tbody>
                                    <tr>
                                        <td>{{$reserved->minutes}}</td>
                                        <td>xx</td>
                                        <td>xx</td>
                                    </tr>
                            </tbody>
                        </table>
                        <h3>Breakdown</h3>                      
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Minutes</th>
                                    <th>Date Added</th>
                                    <th>Added By:</th>
                                </tr>
                            </thead>                            
                            <tbody>
                                @if($reserved->breakdown)
                                    <?php $arr = json_decode($reserved->breakdown); ?>
                                    @foreach($arr as $res)
                                    <tr>
                                        <td>{{$res->minutes}}</td>
                                        <td>{{$res->date}}</td>
                                        <td>{{$res->by}}</td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add-minute" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <form  action="{{route('jade.subscribers.load-addreserved',$reserved->id)}}" method="post">
                    {{ csrf_field() }}  

                    <div class="modal-body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="minutes" />
                                            <label class="form-label">Minutes</label> 
                                        </div>
                                    </div>
                                </div>

                            </div>
                    </div>
                                                             
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect" >Confirm</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                    </div>
                     </form>
                </div>
            </div>
        </div> 

@endsection 

@section('footer-scripts')

@endsection