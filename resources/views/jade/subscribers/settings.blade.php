@extends('jade.master')

@section('title', 'Acc|Settings')

@section('content')

        <div class="block-header">
            <h2>Subscriber: {{$subscriber->client}}</h2>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Account Settings</h2>

                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">                        
                            <tbody>
                                <tr>
                                    <th>Account Status</th>
                                    <td>
                                        <p>
                                            <span class="mb-input">Default<input type="radio" name="account_status" value="default" {{($subscriber->account_status == 'default'?'checked':'')}}></span>
                                            <span class="mb-input">Open <input type="radio" name="account_status" value="open" {{($subscriber->account_status == 'open'?'checked':'')}}></span> 
                                            <span class="mb-input">Suspended<input type="radio" name="account_status" value="suspended" {{($subscriber->account_status == 'suspended'?'checked':'')}}></span>                                           
                                        </p>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


@endsection 

@section('footer-scripts')
<style type="text/css">
 .mb-input > input[type="radio"] {
    opacity: 100;
    position: relative;
    left: 0;
    margin-left: 5px;
 }  
.mb-input{
    font-weight: bold;
    color: #333;
    margin-right: 20px;    
}
</style>

<script type="text/javascript">
 
 !function(a) {

    var jadeSubscriberAccountTypeUpdate = "{{route('jade.subscribers.settings-account-type-update',$subscriber->id)}}";
    var _token = "{{csrf_token()}}";
    a.mb_lib = {
        mb_ajax: function(b) {
            var c = {
                data: "null",
                url: "null",
                type: "post",
                attachto: "null"
            };
            if (b) a.extend(c, b);
            a.ajax({
                url: c["url"],
                data: c["data"],
                type: c["type"],
                success: function(b) {
                    console.log(b);
                },
                error: function(a) {
                    console.log(a);
                }
            });
        }
    }; 

    $('.mb-input > input[type="radio"]').on('click',function(){
        var val = $(this).val();
        var subscriber = "<?=$subscriber->id?>";
        var a = {
            data: {value: val,subscriber:subscriber,_token:_token},
            url: jadeSubscriberAccountTypeUpdate,
        };
        $.mb_lib.mb_ajax(a);
    });
      
}(jQuery);   

</script>
@endsection