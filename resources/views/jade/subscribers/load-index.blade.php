@extends('jade.master')

@section('title', 'Users|Index')

@section('content')

        <div class="block-header">
            <h2>Subscriber: {{$subscriber->client}} <a href="{{route('jade.subscribers.index')}}"class="pull-right"><i class="material-icons">reply</i></a></h2>
            {!!view('jade.errors')!!}
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Loads</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown"> 
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">more_vert</i> </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('jade.subscribers.load-add',$subscriber->id)}}">Add Load</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Year</th>
                                    <th>Month</th>
                                    <th>Minutes</th>
                                    <th>#</th>
                                </tr>
                            </thead>                            
                            <tbody>
                                @foreach($loads as $load)
                                    <tr>
                                        <td>{{$load->year}}</td>
                                        <td>{{month($load->month)}}</td>
                                        <td>{{$load->minutes}}</td>
                                        <td>
                                            <a href="{{route('jade.subscribers.load-view',$load->id)}}" class="btn btn-default btn-circle waves-effect waves-circle waves-float"> 
                                                <i class="material-icons">pageview</i>
                                            </a>     
                                            <a href="{{route('jade.subscribers.load-view',$load->id)}}" class="btn btn-default btn-circle waves-effect waves-circle waves-float"> 
                                                <i class="material-icons">edit</i>
                                            </a>   
                                             @if(!isPast($load->startdate))
                                            <button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="modal" data-target="#delete-load{{$load->id}}"> 
                                                <i class="material-icons">delete</i>
                                            </button> 
                                            <div class="modal fade" id="delete-load{{$load->id}}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-sm" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-body">Are you sure you want to delete load {{month($load->month)}} {{$load->year}}?</div>
                                                        <form  action="{{route('jade.subscribers.load-delete',$load->id)}}" method="post">
                                                        {{ csrf_field() }}                                                               
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-link waves-effect" >Confirm</button>
                                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                         </form>
                                                    </div>
                                                </div>
                                            </div> 
                                            @endif                                                
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


@endsection 

@section('footer-scripts')

@endsection