<?php $assets = URL::asset('/university'); ?>
@extends('jade.master')

@section('title', 'Subscribers')

@section('content')

        <div class="block-header">
            <h2>Subscribe: {{$subscriber->client}}</h2>
        </div>

        <div class="row clearfix">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="card">

                    <div class="body">
                        <form  action="{{route('jade.subscribers.did-create',$subscriber->id)}}" method="post">
                            {{ csrf_field() }}
                            <h3 class="card-inside-title">Add DID</h3>
                            <div class="row clearfix">

                                <div class="col-sm-12">
                                    <div class="form-group form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="did" />
                                            <label class="form-label">Phone Number</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect pull-right">Add</button>
                        </form>
                    </div>

                </div>
            </div>
        </div> 


@endsection 

@section('footer-scripts')

@endsection