<?php $assets = URL::asset('/university'); ?>
@extends('jade.master')

@section('title', 'Subscribers')

@section('content')

        <div class="block-header">
            <h2>Subscribers</h2>
        </div>

        <div class="row clearfix">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="card">

                    <div class="body">
                        <form  action="{{route('jade.subscribers.create')}}" method="post">
                            {{ csrf_field() }}
                            <h3 class="card-inside-title">Add Subscriber</h3>
                            <div class="row clearfix">

                                <div class="col-sm-12">
                                    <div class="form-group form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="client" />
                                            <label class="form-label">Client Name</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="email" />
                                            <label class="form-label">Email</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="alias" />
                                            <label class="form-label">Alias</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect pull-right">Add</button>
                        </form>
                    </div>

                </div>
            </div>
        </div> 


@endsection 

@section('footer-scripts')

@endsection