@extends('jade.master')

@section('title', 'Users|Index')

@section('content')

        <div class="block-header">
            <h2>Users</h2>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>All Users</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">more_vert</i> </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('jade.subscribers.add')}}">Add Subscriber</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Alias</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>#</th>
                                </tr>
                            </thead>                            
                            <tbody>
                                @foreach($subscribers as $subscriber)
                                    <tr>
                                        <td>
                                            <a href="{{route('jade.dashboard.client',$subscriber->id)}}" class="mb-link">
                                                {{$subscriber->client}}
                                            </a>
                                        </td>
                                        <td>{{$subscriber->alias}}</td>
                                        <td>{{$subscriber->email}}</td>
                                        <td>{{$subscriber->account_status}}
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default waves-effect">Options</button>
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="{{route('jade.subscribers.load-index',$subscriber->id)}}" class=" waves-effect waves-block"><i class="material-icons">credit_card</i>Prepaid Load</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{route('jade.subscribers.load-reserved',$subscriber->id)}}" class=" waves-effect waves-block"><i class="material-icons">credit_card</i>Overage</a>
                                                    </li>                                                    
                                                    <li>
                                                        <a href="{{route('jade.subscribers.did-index',$subscriber->id)}}" class=" waves-effect waves-block"><i class="material-icons">phone</i>DIDs</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{route('jade.subscribers.settings',$subscriber->id)}}" class=" waves-effect waves-block"><i class="material-icons">build</i>Acc Settings</a>
                                                    </li>                                                    
                                                    <li role="separator" class="divider"></li>
                                                    <li>
                                                        <a href="#"  class=" waves-effect waves-block" data-toggle="modal" data-target="#delete-subscriber{{$subscriber->id}}"><i class="material-icons">delete</i>Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="modal fade" id="delete-subscriber{{$subscriber->id}}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-sm" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-body">Are you sure you want to delete {{$subscriber->client}}?</div>
                                                        <form  action="{{route('jade.subscribers.delete',$subscriber->id)}}" method="post">
                                                        {{ csrf_field() }}                                                               
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-link waves-effect" >Confirm</button>
                                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                         </form>
                                                    </div>
                                                </div>
                                            </div>                                              
                                        </td>           
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!!$subscribers->links()!!}
                    </div>
                </div>
            </div>
        </div>


@endsection 

@section('footer-scripts')

@endsection