<?php $assets = URL::asset('/university'); ?>
@extends('jade.master')

@section('title', 'Subscribers')
 

@section('content')

        <div class="block-header">
            <h2>Subscribe: {{$subscriber->client}}</h2>
        </div>

        <div class="row clearfix">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="card">

                    <div class="body">
                        <form  action="{{route('jade.subscribers.load-create',$subscriber->id)}}" method="post">
                            {{ csrf_field() }}
                            <h3 class="card-inside-title">Add Load </h3>
                            {!!view('jade.errors')!!}
                            <div class="row clearfix">

                                <div class="col-sm-12">
                                    <div class="form-group form-float form-group-sm">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="minutes" />
                                            <label class="form-label">Minutes</label>
                                        </div>
                                    </div>                                 
                                </div>
                                <div class="col-sm-6">
                                     <div class="form-group form-float form-group-sm" >
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="month">
                                                <option value="">-- Select Month --</option>
                                                <option value="1">Jan</option>
                                                <option value="2">Feb</option>
                                                <option value="3">Mar</option>
                                                <option value="4">Apr</option>
                                                <option value="5">May</option>
                                                <option value="6">June</option>
                                                <option value="7">July</option>
                                                <option value="8">Aug</option>
                                                <option value="9">Sept</option>
                                                <option value="10">Oct</option>
                                                <option value="11">Nov</option>
                                                <option value="12">Dec</option>
                                            </select> 
                                        </div>
                                    </div>                                
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float form-group-sm">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="year">
                                                <option value="">-- Select Year --</option>
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                                <option value="2019">2019</option>
                                                <option value="2020">2020</option>
                                                <option value="2021">2021</option>
                                            </select> 
                                        </div>
                                    </div>                               
                                </div>                                
                                 
                                  

                            </div>

                            <button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect pull-right">Add</button>
                        </form>
                    </div>

                </div>
            </div>
        </div> 


@endsection 

@section('footer-scripts')
<link href="{{$assets}}/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
@endsection