@extends('jade.master')

@section('title', 'Users|Index')

@section('content')

        <div class="block-header">
            <h2>Subscriber: {{$subscriber->client}}</h2>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>DIDs</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">more_vert</i> </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('jade.subscribers.did-add',$subscriber->id)}}">Add DID</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Phone</th>
                                    <th>Date Added</th>
                                    <th>#</th>
                                </tr>
                            </thead>                            
                            <tbody>
                                @foreach($dids as $did)
                                    <tr>
                                        <td>{{$did->did}}</td>
                                        <td>{{$did->created_at}}</td>
                                        <td>
                                                <button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float" data-toggle="modal" data-target="#delete{{$did->id}}"> 
                                                    <i class="material-icons">delete</i> 
                                                </button>   
                                                <div class="modal fade" id="delete{{$did->id}}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-sm" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body">Are you sure you want to delete {{$did->did}}?</div>
                                                            <form  action="{{route('jade.subscribers.did-delete',$subscriber->id)}}" method="post">
                                                            {{ csrf_field() }}                                                               
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-link waves-effect" >Confirm</button>
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                                                            </div>
                                                             </form>
                                                        </div>
                                                    </div>
                                                </div>                                                                                         
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


@endsection 

@section('footer-scripts')

@endsection