<?php $assets = URL::asset('/university'); ?>
@extends('jade.master')

@section('title', 'Users|Index')
<link href="{{$assets}}/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
@section('content')

        <div class="block-header">
            <h2>Users</h2>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">

                    <div class="body">
                        <form  action="{{route('jade.users.create')}}" method="post">
                            {{ csrf_field() }}
                            <h3 class="card-inside-title">Add User</h3>
                            <div class="row clearfix">

                                <div class="col-sm-12">
                                    <select class="form-control show-tick" name="gem_id">
                                        <option value="">-- Select User --</option> 
                                        @foreach($gemusers as $user)
                                        <option value="{{$user->id}}||{{$user->name}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect pull-right">Add</button>
                        </form>
                    </div>

                </div>
            </div>
        </div> 


@endsection 

@section('footer-scripts')

@endsection