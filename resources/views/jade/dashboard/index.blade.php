@extends('jade.master')

@section('title', 'Home')

@section('content')

        <div class="block-header">
            <h2>Dashboard</h2>
        </div>
        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <ul class="nav nav-pills">
                            <li class="active">
                                <a  href="#1a" data-toggle="tab"><h2>Active Account with load</h2></a>
                            </li>
<!--                             <li>
                                <a  href="#2a" data-toggle="tab"><h2>Active Account with overage</h2></a>
                            </li> -->
                        </ul>   
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">more_vert</i> </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">

                        <div class="tab-content clearfix">
                        <div class="tab-pane active" id="1a">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Client</th>
                                    <th>Alias</th>
                                    <th>Remaining Load</th>
                                    <th>Remaining Overage</th>
                                    <th>Coverage</th> 
                                    <th>Account Status</th>
                                </tr>
                            </thead>                            
                            <tbody>
                                @foreach($loads as $load)
                                <tr>
                                    <td>
                                    <a href="{{route('jade.dashboard.client',$load->subscriber->id)}}" class="mb-link">
                                        {{$load->subscriber->client}}
                                    </a>
                                    </td>
                                    <td>{{$load->subscriber->alias}}</td>
                                    <td>
                                        <a class="mb-link" href="{{route('jade.dashboard.load',$load->id)}}">{{$load->remaining}}</a>
                                    </td>
                                    <td>
                                        <a href="{{route('jade.dashboard.overage',$load->subscriber->id)}}" class="mb-link">{{($load->overage?$load->overage->remaining:0)}}
                                        </a>
                                    </td>
                                    <td>{{$load->startdate}} to {{$load->enddate}}</td>
                                    <td>{{$load->subscriber->account_status}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>



                </div>
                </div>
            </div>
        </div>


@endsection 

@section('footer-scripts')

@endsection