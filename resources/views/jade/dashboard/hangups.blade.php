@extends('jade.master')

@section('title', 'Home')

@section('content')


        <div class="block-header">
            <h2>Hangups</h2>
        </div>
        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Call Hangups</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">more_vert</i> </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>DID</th>
                                    <th>Call Date</th>
                                    <th>Reason</th>
                                </tr>
                            </thead>                            
                            <tbody>
                                @foreach($hangups as $hangup)
                                <tr>
                                    <td>
                                        {{$hangup->did}}
                                    </td>
                                    <td>
                                        {{$hangup->created_at}}
                                    </td>
                                    <td>
                                        {{explode('>',$hangup->message,2)[0]}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


@endsection 

@section('footer-scripts')

@endsection