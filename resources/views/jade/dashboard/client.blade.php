@extends('jade.master')

@section('title', 'Home')

@section('content')

        
        <div class="row clearfix" class="mtop-20px">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Load Detail</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">more_vert</i> </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <tbody>
                                <tr>
                                    <th>Client</th>
                                    <td>{{$subscriber->client}}</td>
                                </tr>
                                <tr>
                                    <th>alias</th>
                                    <td>{{$subscriber->alias}}</td>
                                </tr>                                                                                        
                            </tbody>
                        </table>
                        <h3>DIDs</h3>
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">                      
                            <tbody>
                            @if($subscriber->dids)
                                @foreach($subscriber->dids as $did)
                                <tr>
                                    <td>{{$did->did}}</td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>  
                   
                    </div>
                </div>
            </div>
        </div>


@endsection 

@section('footer-scripts')

@endsection