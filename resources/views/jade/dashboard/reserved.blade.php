@extends('jade.master')

@section('title', 'Home')

@section('content')

        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Load Detail</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="material-icons">more_vert</i> </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <tbody>
                                <tr>
                                    <th>Client</th>
                                    <td>{{$reserved->subscriber->client}}</td>
                                </tr>
                                <tr>
                                    <th>alias</th>
                                    <td>{{$reserved->subscriber->alias}}</td>
                                </tr> 
                                 <tr>
                                    <th>Reserved</th>
                                    <td>{{$reserved->minutes}}</td>
                                </tr>  
                                 <tr>
                                    <th>Reserved Remaining</th>
                                    <td>{{$reserved->remaining}}</td>
                                </tr>                                                                                          
                            </tbody>
                        </table>
                        <h3>Call Details</h3>
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>agent</th>
                                    <th>Call Date</th>
                                    <th>Campaign Id</th>
                                    <th>DID</th>
                                    <th>Durations</th>
                                </tr>
                            </thead>                            
                            <tbody>
                                @foreach($calls as $call)
                                <tr>
                                    <td>{{$call->user}}</td>
                                    <td>{{$call->created_at}}</td>
                                    <td>{{$call->campaign_id}}</td>
                                    <td>{{$call->did}}</td>
                                    <td>{{$call->durations}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>  
                        {!!$calls->links()!!}                      
                    </div>
                </div>
            </div>
        </div>


@endsection 

@section('footer-scripts')

@endsection