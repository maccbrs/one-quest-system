
@extends('rubellite.master')

@section('title', 'dashboard')

@section('top-navigation')


@endsection

@section('content')

<?php 

 $DataSource = new AppHelper;
 $larr_columnfield = $DataSource->fetchTable('rubellite' . chr(92) . 'MstKeyFactors', 'name' , 'SortBy'); 


 $KeyFactorList ="";
$KeyFactorArray = array();		
		if(count($larr_columnfield) > 0){
				 foreach($larr_columnfield as $key => $val)
				{		
						$KeyFactorList .= $val->id . ":" . $val->name . ",";
						$KeyFactorArray[$val->id] =$val->name; // ASSIGN KEY CONVERT KEYFACTOR VALUE TO NAME  assign in Assoc Array
				}
				$KeyFactorList = rtrim($KeyFactorList,',');
			} // if(isset($larr_columnfield))
		else {$KeyFactorList = ":" ;}
 
//var_dump($KeyFactorArray);

 
 
$larr_outputArray2 =array(
						  "modalName" => "CreateNew",
						  "action" => "hidden|Actions|action",
						  "mst_keyfactor_id" => "select|Key Factor|" . $KeyFactorList,
						  );
			
			foreach($MstRecordsColumn as $key => $val)		
			{
				if(($val != 'mst_keyfactor_id') && ($val != 'id')){
				array_push($larr_outputArray2,"'" . $val . "'","text|" . ucfirst($val) . "|" . $val ); 
				}
			}
			
			


$array_list = array(
              
			   "modalName" => "CreateNew",
			  "header"=> "Add "  , 
			  "form-action"=> route('rubellite.masterfile.create',$urllist), 
			  "form-list"=>  $larr_outputArray2);
			  ?>
			  
{!!view('rubellite.masterfile.new-create-modal',['tl_list' => $array_list])!!}
<div class="content-view">
<span class="pull-left" style="font-size:20px;"><?php echo $urllist;?></span>
<button type="button" class="btn btn-sm btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".FormCreateNew">Add</button>
<br/>
<br/>
<hr/>
<div class="clear:both"/></div>
	<div class="row">
		

		<div class="col-lg-12">
			
		
			<div class="card card-block">
				
			<table id="example" class="display table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
					 <?php 			
					foreach($MstRecordsColumn as $key => $val)		
					{
						echo "<th>" .$val . "</th>";
					}
						echo "<th>Action</th>";
					?>
					  
					</tr>
				</thead>
				<tfoot>
					<tr>
					<?php 			
					foreach($MstRecordsColumn as $key2 => $val2)		
					{
						echo "<th>" .$val . "</th>";
					}
						echo "<th>Action</th>";
					?>
						
					</tr>
				</tfoot>
				<tbody>
				   <?php 
					//var_dump($MstRecords);
					foreach($MstRecords as $key => $val)		
					{
						echo "<tr>" ;
							foreach($MstRecordsColumn as $key2 => $val2)		
							{	
								if($val2 == 'mst_keyfactor_id')
								{
								echo "<td>" . $KeyFactorArray[$val->$val2] . "</td>";
								}
								
								/*  if($val2 = 'mst_keyfactor_id')
								{   echo "<td>" ;
									echo $KeyFactorArray[$val->$val2];
									echo "</td>";
								}*/
								else   {
								echo "<td>" .  $val->$val2. "</td>";
								}
							}?>
							
							<td>Edit | Delete</td>
						<?php 
						echo "</tr>" ;
					}
					
					?>
				
			
					
				</tbody>
			</table>
	        </div>
		</div>
	</div>
</div>




@endsection 

@section('footer-scripts')

@endsection