
@extends('rubellite.master')

@section('title', 'dashboard')

@section('top-navigation')



@endsection

@section('content')



<div class="content-view">
	<div class="row">
		

		<div class="col-lg-12">
			<div class="card card-block">
				<form id="validate" class="form-horizontal" role="form" method="POST" action="{{route('amber.newsfeed.new_post')}}">

					{{ csrf_field() }}

					<input type="hidden" value ="" name="attachment" >
					<input type="hidden" value ="1" name="status" >
					<input type="hidden" value ="post" name="type" >
					<input type="hidden" value ="" name="tags" >
					<input type="hidden" value ="" name="reactions" >



					<div class="panel-body">
	                	<textarea id="editor" class="form-control"  name="caption"></textarea>
	            	</div>
	            	
	            	<button type="submit" class="btn btn-primary pull-right">Post</button>
	            </form>
	        </div>
		</div>
	</div>
</div>




@endsection 

@section('footer-scripts')

@endsection