
@extends('rubellite.master')

@section('title', 'dashboard')

@section('top-navigation')



@endsection

@section('content')

<?php 
$larr_outputArray2 =array(
						  "modalName" => "CreateNew",
						  "action" => "hidden|Actions|action");
	
			foreach($MstRecordsColumn as $key => $val)		
			{
				if($val != 'id'){
				array_push($larr_outputArray2,"'" . $val . "'","text|" . ucfirst($val) . "|" . $val ); 
				}
			}
		


$array_list = array(
			  "header"=> "Add "  , 
			  "form-action"=> route('rubellite.masterfile.create',$urllist), 
			  "form-list"=>  $larr_outputArray2);
			  ?>
{!!view('create-modal',['tl_list' => $array_list])!!}
{!!view('edit-modal',['tl_list' => $array_list])!!}
{!!view('delete-modal',['tl_list' => $array_list])!!}
<div class="content-view">
<span class="pull-left" style="font-size:20px;"><?php echo $urllist;?></span>
<button type="button" class="btn btn-sm btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".modalAddUser">Add</button>
<br/>
<br/>
<hr/>
<div class="clear:both"/></div>
	<div class="row">
		

		<div class="col-lg-12">
			
		
			<div class="card card-block">
				
				<table id="example" class="display table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
			 <?php 			
			foreach($MstRecordsColumn as $key => $val)		
			{
				echo "<th>" .$val . "</th>";
			}
				echo "<th>Action</th>";
			?>
              
            </tr>
        </thead>
        <tfoot>
            <tr>
            <?php 			
			foreach($MstRecordsColumn as $key2 => $val2)		
			{
				echo "<th>" .$val . "</th>";
			}
				echo "<th>Action</th>";
			?>
				
            </tr>
        </tfoot>
        <tbody>
           <?php 
			//var_dump($MstRecords);
			foreach($MstRecords as $key => $val)		
			{
				echo "<tr>" ;
					foreach($MstRecordsColumn as $key2 => $val2)		
					{
						echo "<td>" . $val->$val2. "</td>";
					}?>
					
					<td>Edit 
					<button type="button" data-toggle="modal" 
											  onclick="$('.class_id').val('{{$value_field['id']}}');
											  <?php 
													foreach($table_columns as $keys2 => $value_field2)
													{
													
																	                       
												
													echo "$('.class_" .  $value_field2 . "')" . ".val('" . $value_field[$value_field2] . "');" ;
													}
													
											  /*
											
											  $('.class_alias').val('{{$value_field['alias']}}');
											  $('.class_remarks').val('{{$value_field['remarks']}}'); */
											  ?>
											  $('.class_action').val('edit')" 
											  
											  
											  data-target=".modalEditUser"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>edit</button>
					
					| 
					 <button type="button" data-toggle="modal" 
											  onclick="console.log('test');
											  $('.class_action').val('delete')" 
											  
											  
											  data-target=".modalDelete"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>delete</button>
					
					</td>
				<?php 
				echo "</tr>" ;
			}
			
			?>
		
    
            
        </tbody>
    </table>
	        </div>
		</div>
	</div>
</div>




@endsection 

@section('footer-scripts')

@endsection