<?php $asset = URL::asset('/'); $request = Request::instance(); ?> 
@extends('bloodstone.master')

@section('title', ' Master File') 


@section('top-navigation')


@endsection

@section('content')


<style>

 .sidebar-navi a div{
	 background-color:#f2f2f2;
	 
 }
 .sidebar-navi a div span{
	 font-size:20px;
	 
 }
  .sidebar-navi a div:hover{
	 background-color:red;
	 
 }
</style>


<?php 

$mstlisting = $urllist;
$larr_outputArray = array();
$larr_outputArray2 =array("hidden" => "hidden|column_id|id",
								  
								   "action" => "hidden|Actions|action",
								   
								  );


				/* foreach($MstBrand as $keys2 => $value_field)
					{
				
						$larr_outputArray = json_decode($value_field, true);
						
					}	 */
					foreach($table_columns as $larr_key => $larr_val)														
						{												
							if (($larr_val != 'id') && 
								
								($larr_val != 'status') && 
								
								($larr_val != 'created_by') && 
								
								($larr_val != 'updated_by') )
							{array_push($larr_outputArray2,"'" . $larr_val . "'","text|" . ucfirst($larr_val) . "|" . $larr_val ); }
							else if ($larr_val == 'remarks')   
							{
								array_push($larr_outputArray2,"'" . $larr_val . "'","textarea|" . ucfirst($larr_val) . "|" . $larr_val ); 
								
							}
							
					
								
						}

														
														
												

$array_list = array(
			  "header"=> "Add " . ucfirst($urllist) , 
			  "form-action"=> route('bloodstone.masterfile.create',$mstlisting), 
			  "form-list"=>  $larr_outputArray2/*array("name" => "text|Name|name",
								   "remarks" => "textarea|Remarks|remarks",
								   "test" => "textarea|test|remarks"
								   
								  ) */
			  );


$array_list2 = array(
			  "header"=> "Update " . ucfirst($urllist) , 
			  "form-action"=> route('bloodstone.masterfile.edit',$mstlisting), 
			  "form-list"=>  $larr_outputArray2 
			  );

$batch_upload = array(
			  "header"=> ucfirst($urllist) , 
			  "form-action"=> route('bloodstone.masterfile.batchcreate',$mstlisting), 
			  "form-list"=>  $larr_outputArray2
			  );
			 
			  ?>

<div class="">
	
	   <div class="col-md-12">
	
	
        <div class="card">
			
            <div class="card-header no-bg b-a-0">
              <h4>{{ ucfirst($urllist) }}
			  <span class = "pull-right">
			<button type="button" class="btn btn-sm btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".modalAddUser">Add</button>
			<button type="button" class="btn btn-sm btn-info fa fa-upload pull-right" style="margin-right:10px" data-toggle="modal" data-target=".modalUpload">Upload Excel</button>
			{!!view('batch-upload',['tl_list' => $batch_upload])!!}
			{!!view('create-modal',['tl_list' => $array_list])!!}{!!view('edit-modal',['tl_list' => $array_list2])!!} {!!view('delete-modal',['tl_list' => $array_list2])!!}</span>
			  </h4>
			  <hr/>
            </div>
            <div class="card-block" >
			
					@if (session('return_msg'))
						<div class="alert alert-danger">
							{{ session('return_msg') }}
						</div>
					@endif
					

            <div class="row media">
                <div class="">
                   
										   <table id="example" class="display nowrap table-bordered">
					                     <thead>
												@foreach($table_columns as $keys => $value_field)
												
													<th>{{$value_field}}</th>
																	                       
												@endforeach
											   
											   <th>Actions</th>
					                       
					                     </thead>
					                     <tbody>
										
										
										<?php	
											$larr_outputArray = array();
												
												?>
											@foreach($MstBrand as $keys => $value_field)
												<tr>
												@foreach($table_columns as $keys2 => $value_field2)
													<td>{{$value_field[$value_field2]}} </td>
													
																	                       
												@endforeach	
											  
											  
											  
											  <td><button type="button" data-toggle="modal" 
											  onclick="$('.class_id').val('{{$value_field['id']}}');
											  <?php 
													foreach($table_columns as $keys2 => $value_field2)
													{
													
																	                       
												
													echo "$('.class_" .  $value_field2 . "')" . ".val('" . $value_field[$value_field2] . "');" ;
													}
													
											  /*
											
											  $('.class_alias').val('{{$value_field['alias']}}');
											  $('.class_remarks').val('{{$value_field['remarks']}}'); */
											  ?>
											  $('.class_action').val('edit')" 
											  
											  
											  data-target=".modalEditUser"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>edit</button>  | 
											  
											  <button type="button" data-toggle="modal" 
											  onclick="console.log('test');$('.class_name').val('{{$value_field['name']}}');
											  $('.class_id').val('{{$value_field['id']}}');
											  $('.class_remarks').val('{{$value_field['remarks']}}');
											  $('#desc').html('{{$value_field['name']}}');
											  $('.class_action').val('delete')" 
											  
											  
											  data-target=".modalDelete"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>delete</button></a></td>
											
												
											  </tr>
											@endforeach

					                     </tbody>
                                </table>
                    
                </div>
            </div>

            </div>
        </div>
    </div>
	
</div>



@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

  <script type="text/javascript">
    

$(document).ready(function() {
    $('#example').DataTable(
        {
       
       
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'print'
        ]
        });
	
			
} );


function loadDoc( unique_id) {
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", "http://localhost:8000/bloodstone/ajax/42?unique_id=" + unique_id, false);
  xhttp.send();
  document.getElementById("ajaxresult").innerHTML = xhttp.responseText;
}


</script>



@endsection