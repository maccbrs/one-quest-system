
<?php $assets = URL::asset('/');?>
<?php $pic = URL::asset('/uploads/');?>
<?php $Logger = new AppHelper; $larr_myarray = array();?>
<?php $Auth = Auth::user()->id;?>
<?php $larr_myarray =  json_decode($Logger->DisplayLog(Auth::user()->id),true); $count = count($larr_myarray);?>
<?php $GemUser = new \App\Http\Models\gem\User;?>
<?php $avatar =  $GemUser->where('id','=',$Auth)->get()?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @yield('header-css')
  <link rel="stylesheet" href="{{$assets}}inventory/css/bootstrap.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/css/buttons.dataTables.css">
  <link rel="stylesheet" href="{{$assets}}css/rubillite.css">
    
	

  
   <script src="{{$assets}}inventory/js/jquery-3.2.1.js"></script>
      <script src="{{$assets}}inventory/datatables/jquery-ui.js"></script>
      <script src="{{$assets}}inventory/datatables/bootstrap.min.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
      <script src="{{$assets}}inventory/datatables/jquery.dataTables.yadcf.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>
	  <script src="{{$assets}}js/rubellite.js"></script>
	  

	  
  
 
</head>
<body>
<div style="min-height:95vh;"> <!--FULL SCREEN -->
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid" style="position:fixed;z-index:9999;background-color:rgb(4, 52, 75);width:100%;margin:0px;!important;top:0;left:0;">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			  </button>
			  
			 <!-- <a class="navbar-brand" href="#">Calidad</a> -->
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
			  <ul class="nav navbar-nav">
				<li><a href="#">Dashboard</a></li><!--{{route('bloodstone.home')}} -->
				<li><a href="#">Scan </a></li>
				<li><a href="#">Create Template </a></li>
				<li><a href="#">Reports</a></li>
				<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Master Files
				</a>
				<ul class="dropdown-menu">
		   

					<li><a href="{{$assets}}rubellite/masterfile/MstCampaigns">Campaigns</a></li>
					<li><a href="{{$assets}}rubellite/masterfile/MstKeyfactors">Key Factors</a></li>
					<li><a href="{{$assets}}rubellite/masterfile/MstParameters">Parameter</a></li>
					<li><a href="{{$assets}}rubellite/masterfile/MstElements">Elements</a></li>
					<!--<li><a href="{{$assets}}rubellite/masterfile/MstStatus">Weeks</a></li> -->
					<li><a href="{{$assets}}rubellite/masterfile/SystemSetting">System Setting</a></li>

				  
				</ul>
			  </li>
			   
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
									
						@if(!empty($Logger->DisplayLog(Auth::user()->id)))

					  <li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Logs <span class="label label-danger">{{$count}}</span></a>
						
						
						<ul class="dropdown-menu" style="width:300px;font-size:10px;">
							
							@foreach($larr_myarray as $logkey => $logval)
							
											<li><div style='padding:10px;'>{{$logval['description'] . $logval['created_at']}}</div></li><hr style='margin-top:5px;margin-bottom:5px'/>
							@endforeach
											<li><div style='padding:10px;'>View All</div></li><hr style='margin-top:5px;margin-bottom:5px'/>
							@endif
							  
						</ul>
					  </li>
				<li><a href={{route('gem.dashboard.index')}}><span class="glyphicon glyphicon-home"></span> Milestone</a></li>
			  </ul>
			</div>
		  </div>
		</nav>
		<div class="" style="background-color:#fffff11">
			<div class="col-sm-2 sidenav" style="min-height:90vh;position:fixed">

			  <div class="user-info">
			  <img src="{{$pic}}/qa-logo.png"/>
			  <hr/>
			  <div class="row">
				<div class="col-sm-4">
					<img src="{{$pic.'/sm-'.$avatar[0]['avatar']}}" alt="">
				</div>
				<div class="col-sm-8">
					 <span>Welcome<br>{{Auth::user()->name}}</span>
				</div>
			  </div>
			  
			    <table>
					<tr>
					</tr>
				</table>
				
			  
				
				  
			  </div>
				 <hr/>
			  <div class="col-sm-12 sidenav-menu" >
				<div class="row">
					<ul class="list-unstyled">
						<li>List Menu 1s</li>
						<li>List Menu 1</li>
						<li>List Menu 1</li>
						<li>List Menu 1</li>
						<li>List Menu 1</li>
					</ul>
					<span class="col-sm-12"><b>@yield('title')</b></span>
				</div>	
			  </div>


			</div>
			
			<div class="col-sm-10  offset-sm-2 text-left custom" style="float:right;margin-top: 20px;min-height:90vh;"> 
				@yield('content')
				
				
			</div>
			<div style="clear:both"></div>
			
		</div>		

	<footer class=" text-center" style="position:relative;width:100%;bottom:0;left:0;">
	  <p>Footer Text</p>
	</footer>

</div > <!--FULL SCREEN -->



</body>
     

@yield('footer-scripts')

</html>