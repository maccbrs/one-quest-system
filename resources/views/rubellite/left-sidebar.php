
    <div class="col-md-2">
	
	
        <div class="card">
			
            <div class="card-header no-bg b-a-0">
              <h4>Edit Profile Picture {!!view('topaz.profile.update_pic')!!}</h4>
            </div>
            <div class="card-block" >

            <div class="row media">
                <div class="col-md-3 col-sm-4 col-xs-6 media-item">
                    <!--<a class="card card-block text-xs-center" href="javascript:;" title="caption image 1"><img class="img-fluid center-block" src="{{$request->query('UserImageL')}}">
                    </a> -->
                    <div class="text-xs-center">
                        <p class="m-b-0 bold">Profile Picture<span>1</span></p>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
