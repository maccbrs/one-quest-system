<div id="result{{ $applicant_id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Exam Results</h4>
            </div>

            <div class="modal-body">

                @foreach($exam_found as $exam)
                    Assessment Name: {{ $exam['name'] }}<br>
                    Number of questions: {{ $exam['count'] }}<br>
                    Number of correct answers: {{ $exam['correct_answer'] }}<br><br>
                @endforeach

            </div>

        </div>
    </div>
</div>