<div id="assessment{{ $assessment->id }}" class="modal fade add-assessment-modal in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Assessment</h4>
            </div>
            <form role="form" method="post" enctype ='multipart/form-data' action="{{ route('sunstone.assessment.edit') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="assessmentName">Assessment Name</label>
                        <input name="assessment_name" value="{{$assessment->name}}" type="text" class="form-control" placeholder="Enter Assessment Name">
                        <input name="assessment_id" value="{{$assessment->id}}" type="hidden">
                    </div>
                    <div class="form-group">
                        <label for="positionName">Position</label><br>
                        <select id="multi{{ $assessment->id }}" multiple="multiple" class="form-control" name="position_id[]" style="width: 300px">
                            @foreach($positions_all as $key => $position)
                                <option value="{{ $position->id }}">{{ $position->name }}</option>
                            @endforeach
                        </select>
                        <?php $position_ids = []?>
                        @foreach($assessment_positions as $key => $position)
                            <?php array_push($position_ids, $position->id); ?>
                        @endforeach

                    </div>

                    <div class="form-group">
                        <label>Passing Rate (%)</label>
                        <input class="form-control" type="number" name="passing_rate">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#assessment{{ $assessment->id }} select[name='position_id[]']").multiselect({
            buttonWidth: '400px'
        }).val({{ json_encode($position_ids) }});
        $("#assessment{{ $assessment->id }} select[name='position_id[]']").multiselect('refresh');
    });
</script>