<?php $asset = URL::asset('/'); ?>
@extends('layouts.applicant')

@section('content')

        <ul class="nav nav-pills nav-stacked col-sm-1">
            <li class="nav-item"><a class="nav-link active" href="{{ route('sunstone.applicant.application-form') }}">Application Form</a>
            </li>
            <li class="nav-item"><a class="nav-link" href="{{ route('sunstone.assessment.exam-search') }}">Online Exam</a>
            </li>
        </ul>

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1" style = "margin-top: 80PX; text-align: center;">
                    <div class="panel panel-default" style = "padding: 20px;">
                        <img src="{{$asset}}milestone/images/thankyou.png"  >
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10 col-md-offset-1" style = "margin-top: 80PX; text-align: center;">
                    <div class="alert alert-danger">You are done with the Online Exam. Kindly inform Recruitment.</div>
                </div>
            </div>

        </div>

@endsection