<?php $asset = URL::asset('/'); ?>
@extends('layouts.applicant')

@section('content')

    <ul class="nav nav-pills nav-stacked col-sm-1">
        <li class="nav-item"><a class="nav-link active" href="{{ route('sunstone.applicant.application-form') }}">Application Form</a>
        </li>
        <li class="nav-item"><a class="nav-link" href="{{ route('sunstone.assessment.exam-search') }}">Online Exam</a>
        </li>
    </ul>

    <div class="container">

        <div class="row">
            @if(!empty($applicant))
            <div class="col-md-12">

                <div class="jumbotron" style="background: rgba(200, 54, 54, 0)">
                    <h2>
                        Hello {{ $applicant->first_name }} !
                    </h2>

                    <p style="font-weight: 400">
                       This page serves as a confirmation of your identity.
                    </p>
                    <p style="font-weight: 400">
                        If you are not <strong> {{ $applicant->first_name }} {{ $applicant->middle_name }} {{ $applicant->last_name }} </strong> please let Recruitment know. Otherwise, you can start your exam by clicking the button below. Good luck!
                    </p>

                    <p>
                    <form class="form" method="post" enctype ='multipart/form-data' action="{{ route('sunstone.assessment.exam-start') }}" validate>
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-primary btn-large" value="Start Exam">
                        <input type="hidden" name="applicant_id" value="{{ $applicant->id }}">
                    </form>
                    </p>

                </div>
            </div>
            @else
            <div class="col-md-12">
                <div class="jumbotron" style="background: rgba(200, 54, 54, 0)">
                    <h2>
                        Online Exam
                    </h2>

                    <form class="form" method="post" enctype ='multipart/form-data' action="{{ route('sunstone.assessment.exam-search') }}" validate>
                        {{ csrf_field() }}
                        <p style="font-weight: 400">
                            <input type="text" class="form-control" name="applicant_id" placeholder="Please enter your Application ID" required/>
                        </p>

                        <p>
                            <input type="submit" class="btn btn-primary btn-large" value="Search">
                        </p>
                    </form>
                </div>
            </div>
            @endif
        </div>
    </div>


@endsection
