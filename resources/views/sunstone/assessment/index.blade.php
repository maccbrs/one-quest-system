<?php $asset = URL::asset('/'); ?>
@extends('gem.master2')

@section('title', 'index')

@section('content')
    <div class="container-fluid">
        {!! view('sunstone.assessment.add-assessment-modal', compact('positions_all')) !!}
        @if(!empty($assessment_found))
        {!! view('sunstone.assessment.add-question-modal', compact('assessment_found')) !!}
        @endif
        <div class="row">
            <div class="col-md-6">
                <div class="list-group">
                    <div class="list-group-item">
                        <h4>
                            Assessment List
                            <button class="list-group-item-text pull-right btn btn-primary btn-sm" data-toggle="modal" data-target=".add-assessment-modal">
                                <i class="material-icons" aria-hidden="true">add </i>
                            </button>
                        </h4>
                    </div>

                    @if(count($assessments_all) > 0)
                        <div class="list-group-item">
                            <table class="table m-b-0">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Passing Rate</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($assessments_all as $key => $assessment)
                                    <tr>
                                        <td>{{ $assessment->name }}</td>
                                        <td>
                                            @foreach($assessment->positions as $position)
                                            {{ $position->name }},
                                            @endforeach
                                        </td>
                                        <td>
                                            {{ $assessment->passing_rate }}
                                        </td>
                                        <td>
                                            <a href="{{ route('sunstone.assessment.manage_question', $assessment->id) }}"><i class="material-icons" aria-hidden="true">settings </i></a>
                                            <a data-toggle="modal" data-target="#assessment{{ $assessment->id }}" href="#assessment{{ $assessment->id }}"><i class="material-icons" aria-hidden="true">edit </i></a>
                                            <?php $assessment_positions =  $assessment->positions; ?>
                                            {!! view('sunstone.assessment.edit-assessment-modal', compact('assessment', 'assessment_positions', 'positions_all')) !!}
                                            <i class="material-icons" aria-hidden="true">delete </i>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="list-group-item">
                            Nothing yet here. Add an Assessment by clicking <i class="material-icons" aria-hidden="true">add </i>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="list-group">
                    @if(!empty($assessment_found))
                        <?php $assessment_id = $assessment_found->id?>
                    <div class="list-group-item">
                        <h4>
                            Questions - {{ $assessment_found->name  }}
                            <button class="list-group-item-text pull-right btn btn-primary btn-sm" data-toggle="modal" data-target=".add-question-modal">
                                <i class="material-icons" aria-hidden="true">add </i>
                            </button>
                        </h4>
                    </div>
                        @if(count($assessment_found->questions) < 1)
                        <div class="list-group-item">
                            This assessment is empty. Add a question by clicking <i class="material-icons" aria-hidden="true">add </i>
                        </div>
                        @else
                            @foreach($assessment_found->questions as $key => $question)
                                <div class="list-group-item">
                                    @if($question->image != "none")
                                        <img src="{{$question->image}}" />
                                    @endif
                                    @if(strlen($question->body) > 100)
                                            {{ substr($question->body, 0, 100) . "..." }}
                                    @else
                                            {{ $question->body }}
                                    @endif
                                        <a class="pull-right" data-toggle="modal" data-target="#question{{ $question->id }}" href="#question{{ $question->id }}"><i class="material-icons" aria-hidden="true">search </i></a>
                                        {!! view('sunstone.assessment.view-question-modal', compact('question', 'assessment_id')) !!}
                                </div>
                            @endforeach
                        @endif

                    @else
                        <div class="list-group-item">
                            <h4>
                                Action details
                            </h4>
                        </div>
                        <div class="list-group-item">
                            <i class="material-icons" aria-hidden="true">settings </i>
                            click this icon to manage an Assessment's questions
                        </div>
                        <div class="list-group-item">
                            <i class="material-icons" aria-hidden="true">edit </i>
                            click this icon to edit the details of an Assessment
                        </div>
                        <div class="list-group-item">
                            <i class="material-icons" aria-hidden="true">delete </i>
                            clicking this icon would delete an entire Assessment
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer-scripts')

@endsection