<div class="modal fade add-assessment-modal in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Assessment</h4>
            </div>
            <form role="form" class="add-assessment-form" method="post" enctype ='multipart/form-data' action="{{ route('sunstone.assessment.add') }}" validate>
                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="form-group">
                        <label for="assessmentName">Assessment Name</label>
                        <input name="assessment_name" type="text" class="form-control" placeholder="Enter Assessment Name" required>
                    </div>
                    <div class="form-group">
                        <label for="positionName">Position</label><br />
                        <select multiple="multiple" class="form-control" name="position_id[]" style="width: 300px">
                            @foreach($positions_all as $key => $position)
                                <option value="{{ $position->id }}">{{ $position->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Passing Rate (%)</label>
                        <input class="form-control" type="number" name="passing_rate">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var positionsList = [
                @foreach($positions_all as $key => $position)
            {value: '{!! $position->id !!}', label: '{!! $position->name !!}'},
            @endforeach
        ];

        $(".add-assessment-form select[name='position_id[]']").multiselect({
            buttonWidth: '400px'
        });

    });
</script>