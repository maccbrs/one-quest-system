<div class="modal fade add-question-modal in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Question Builder</h4>
            </div>

            <div class="card">

                <div class="card-block">

                    <form id="addQuestionForm" class="form-horizontal form-label-left" method="post" enctype='multipart/form-data' action="{{route('sunstone.assessment.add_question')}}" validate>
                        {{ csrf_field() }}

                        <input type="hidden" name="assessment_id" value="{{ !(empty($assessment_found)) ? $assessment_found->id : ''}}">

                        <div class="card">
                            <div class="card-header">
                                Question
                            </div>
                            <div class="card-block row">
                                <div class="card-title">
                                    <div class="col-lg-5">
                                        <select class="form-control" id="questionType" name="question_type" required>
                                        <option value='' style="display: none">Select Question Type</option>
                                        <option value='text'>Text</option>
                                        <option value='image'>Image</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 pull-right">
                                        <input name="time_limit" id="timepicker" type="text" class="form-control" style="width: 80px">
                                        Time (hh:mm:ss)
                                    </div>
                                </div>

                                <p class="card-text">
                                <div id="questionBlock">
                                    <input name="question" type="hidden" id="questionJson" />
                                </div>
                                </p>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                Choices
                                <button type="button" class="btn btn-sm" id="addChoiceText" style="position: relative; left: 400px">
                                    <i class="fa fa-keyboard-o" aria-hidden="true"></i>
                                </button>
                                <button type="button" class="btn btn-sm" id="addChoiceImage" style="position: relative; left: 400px">
                                    <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="card-block">
                                <p class="card-text">
                                <div class="col-lg-12" id="choicesList">
                                    <input type="hidden" id="choiceCount" value="0">
                                    <input type="hidden" id="choiceJson" name="choices" value="">
                                </div>
                                </p>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary pull-right" id="submit">Submit</button>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $( document ).ready(function() {

        $('#timepicker').timepicker({
            template: false,
            showInputs: false,
            showMeridian: false,
            showSeconds: true,
            explicitMode: true,
            defaultTime: '00:00:00'
        });

        $('#questionType').on('change', function () {
            if($(this).val() == 'text'){
                $('#questionImageBlock').remove();
                $('#questionBlock').append(
                    "<div class='col-lg-12' id='questionTextBlock'>" +
                    "<fieldset class='form-group'>" +
                    "<textarea required rows='5' class='form-control' name='questionText' placeholder='Enter your question'></textarea>" +
                    "</fieldset>" +
                    "</div>"
                );
            }else{
                $('#questionTextBlock').remove();
                $('#questionBlock').append(
                    "<div class='col-md-12' id='questionImageBlock'>" +
                    "<fieldset class='form-group'>" +
                    "<div class='input-group'>" +
                    "<span class='input-group-btn'>" +
                    "<span class='btn btn-default btn-file'>" +
                    "Browse… <input required type='file' id='imgInp'>" +
                    "</span>" +
                    "</span>" +
                    "<input type='text' class='form-control' readonly>" +
                    "</div>" +
                    "<img id='img-upload'/>" +
                    "<input type='hidden' name='questionImg' id='questionImg'/>" +
                    "<input type='text' class='form-control' name='questionImageInstruction' placeholder='Enter Instructions'>" +
                    "</fieldset>" +
                    "</div>"
                );

                $(document).on('change', '.btn-file :file', function() {
                    var input = $(this),
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [label]);
                });

                $('.btn-file :file').on('fileselect', function(event, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }

                });
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#img-upload').attr('src', e.target.result);
                            $('#questionImg').val(e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#imgInp").change(function(){
                    readURL(this);
                });
            }
        });

        $('#addQuestionForm').on('submit', function () {
            var choices = [];
            var question = {};
            var id = 0;


            if($('#questionType').val() == 'text')
                question = {type: 'text', value: $('#questionBlock fieldset').find("textarea").val(), image: 'none'}
            else
                question = {type: 'image', value: $('#questionBlock fieldset').find("input[name='questionImageInstruction']").val(),
                    image: $('#questionBlock fieldset').find("input[name='questionImg']").val()}

            $('#questionJson').val(JSON.stringify(question));

            $('#choicesList fieldset').each(function () {
                choices.push(
                    {id: id, body: $(this).find("input[name*='choice']").val(), is_correct_answer: $(this).find('input:radio').prop('checked')}
                );
                id++;
            });
            $('#choiceJson').val(JSON.stringify(choices));
        });

        $('#addChoiceText').on('click', function () {
            var choice_count = $('#choiceCount').val();
            var choice_selector = "#choiceText-" + choice_count;
            var choice_btn = "#btn-" + choice_count;

            $('#choicesList').
            append(
                "<fieldset class='form-group' id='choiceText-" + choice_count + "'>" +
                "<div class='input-group'>" +
                "<input type='text' required class='form-control' name='choice-text' placeholder='Choice in words' />" +
                "<div class='input-group-addon'>" +
                "<input name='is_correct_answer' type='radio'>" +
                "</div>" +
                "<div class='input-group-addon'>" +
                "<a href='#' id='btn-" + choice_count + "'>" +
                "<i class='fa fa-close' aria-hidden='true'></i>" +
                "</a>" +
                "</div>" +
                "</div>" +
                "</fieldset>"
            );

            $(choice_btn).on('click', function () {
                $(choice_selector).remove();
            });

            choice_count++;
            $('#choiceCount').val(choice_count);
        });

        $('#addChoiceImage').on('click', function () {
            var choice_count = $('#choiceCount').val();
            var choice_selector = "#choiceImage-" + choice_count;
            var choice_btn = "#btn-" + choice_count;

            $('#choicesList').
            append(
                "<fieldset class='form-group' id='choiceImage-" + choice_count + "'>" +
                "<div class='input-group'>" +
                "<span class='input-group-btn'>" +
                "<span class='btn btn-default btn-file'>" +
                "Browse… <input type='file' id='imgInp-" + choice_count + "'>" +
                "</span>" +
                "</span>" +
                "<input required type='text' class='form-control' placeholder='Choice as an image' readonly>" +
                "<div class='input-group-addon'>" +
                "<input name='is_correct_answer' type='radio'>" +
                "</div>" +
                "<div class='input-group-addon'>" +
                "<a href='#' id='btn-" + choice_count + "'>" +
                "<i class='fa fa-close' aria-hidden='true'></i>" +
                "</a>" +
                "</div>" +
                "</div>" +
                "<img id='img-upload-"+ choice_count +"'/>" +
                "<input type='hidden' name='choice-img' id='img-data-"+ choice_count +"'/>" +
                "</fieldset>"
            );
            $(choice_btn).on('click', function () {
                $(choice_selector).remove();
            });

            $(document).on('change', '#imgInp-' + choice_count, function() {
                var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });

            $('#imgInp-' + choice_count).on('fileselect', function(event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }

            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img-upload-' + (choice_count - 1)).attr('src', e.target.result);
                        $('#img-data-' + (choice_count - 1)).val(e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp-" + choice_count).change(function(){
                readURL(this);
            });

            choice_count++;
            $('#choiceCount').val(choice_count);
        });
    });

</script>

<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        width: 100%;
    }
</style>