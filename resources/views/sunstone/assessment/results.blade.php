<?php $asset = URL::asset('/'); ?>
@extends('gem.master2')

@section('title', 'index')

@section('content')

    <div class="card">
        <div class="card-header no-bg b-a-0">Assessment Results
            <div class="search-applicants-block">
                <form class="form-horizontal form-label-right" method="post" enctype ='multipart/form-data' action="{{route('sunstone.assessment.search_applicant')}}" novalidate>
                    {{ csrf_field() }}
                    <span>SEARCH:</span>
                    <input type="text" name="search_key" class="form-control-sm" placeholder="Search key">
                    <select class="form-control-sm" name="search_field">
                        <option value="name">Name</option>
                        <option value="id">Applicant ID</option>
                    </select>
                    <button type="submit" class="btn btn-sm">Submit</button>
                </form>
            </div>
        </div>
        <div class="card-block">

            @if(empty($exams))
                <div class="alert alert-danger">Can't find what you are looking for. Try another search key.</div>

            @else

            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="col-lg-1">Applicant#</th>
                        <th class="col-lg-3">Applicant Name</th>
                        <th class="col-lg-2">Assessment Name</th>
                        <th class="col-lg-2">Score (points / items)</th>
                        <th class="col-lg-2">Rating</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($exams as $key => $exam)
                        <tr>
                            <td class="col-lg-1">{{ $exam->applicant['id'] }}</td>
                            <td class="col-lg-3">
                                {{ $exam->applicant['first_name'] }}
                                {{ $exam->applicant['middle_name'] }}
                                {{ $exam->applicant['last_name'] }}
                            </td>
                            <td class="col-lg-2">{{ $exam->name }}</td>
                            <td class="col-lg-2">
                                {{$results[$exam->id]['points']}} / {{$results[$exam->id]['question_count']}}
                                ( {{ $results[$exam->id]['score'] }}% )
                            </td>
                            <td class="col-lg-2 {{ ($results[$exam->id]['rating'] == 'Failed') ? 'alert-danger' : '' }}">
                                {{ $results[$exam->id]['rating'] }} (>={{ $results[$exam->id]['passing_rate'] }}%)
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
                @include('pagination.default', ['paginator' => $exams])
            @endif
        </div>
    </div>

@endsection

@section('footer-scripts')

@endsection