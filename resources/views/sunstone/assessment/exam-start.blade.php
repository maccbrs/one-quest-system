<?php $asset = URL::asset('/'); ?>
@extends('layouts.applicant')

@section('content')

    <div class="col-md-12">
        <div class="col-md-">
            <h4>Time Limit: <span id="timeLimit"></span></h4>
        <h2>
            {{ $exam->name }}
        </h2>

        <form method="post" enctype ='multipart/form-data' action="{{route('sunstone.assessment.exam-next')}}" novalidate>
            {{ csrf_field() }}
            <div class="list-group">
                <div class="list-group-item">
                    @if($exam_question->image != 'none')
                        <img src="{{ $exam_question->image }}">
                    @endif
                    <p class="list-group-item-text">{{ $exam_question->body }}</p>
                </div>
                @foreach($exam_question->choices as $choice)
                    <div class="list-group-item">
                        <label>
                            <input type="radio" name="answer" value="{{ $choice->id }}">
                            @if(strpos($choice->body, 'data:image') !== false)
                                <img src="{{ $choice->body }}">
                            @else
                                {{ $choice->body }}
                            @endif
                        </label>
                    </div>
                @endforeach
            </div>
            <input type="hidden" name="question_id" value="{{ $exam_question->id }}">
            <input type="hidden" name="exam_id" value="{{ $exam->id }}">
            <input type="hidden" name="applicant_id" value="{{ $applicant_id }}">
            <p>
                <input type="submit" class="btn btn-primary btn-large col-md-2" value="Next question...">
            </p>
        </form>

    </div>

    <script  src="{{$asset}}jquery.countdown/jquery.countdown.min.js" ></script>

    <script>
        $(document).ready(function () {
            function sformat(s) {
                var fm = [
                    Math.floor(s / 60 / 60) % 24, // HOURS
                    Math.floor(s / 60) % 60, // MINUTES
                    s % 60 // SECONDS
                ];
                return $.map(fm, function(v, i) { return ((v < 10) ? '0' : '') + v; }).join(':');
            }

            var hms = '{{ $exam_question->time_limit }}';
            var a = hms.split(':');
            var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);

            if(seconds == 0){
                $("#timeLimit").html("None");
                return;
            }

            var count = seconds;
            var countdown = setInterval(function(){
                $("#timeLimit").html(sformat(count));
                if (count == 0) {
                    clearInterval(countdown);
                    //$('form').trigger('submit');
                    $("input[type='submit']").trigger('click');
                }
                count--;
            }, 1000);
        })
    </script>
@endsection