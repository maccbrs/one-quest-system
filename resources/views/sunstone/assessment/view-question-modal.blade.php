<div id="question{{ $question->id }}" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Manage Question</h4>
            </div>
            <form id="form{{$question->id}}" role="form" class="view-question-form" method="post" enctype ='multipart/form-data' action="{{ route('sunstone.assessment.edit_question') }}" validate>
                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="form-group">
                        @if(strpos($question->image, 'data:image') !== false)
                            <img src="{{ $question->image }}" />
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Question </label>
                        <textarea name="body" class="form-control" required>{{ $question->body }}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Time limit (hh:mm:ss)</label>
                        <input name="time_limit" class="form-control" data-template="false" data-provide="timepicker" data-show-seconds="true" data-show-meridian="false" type="text" value="{{ $question->time_limit }}"/>
                    </div>

                    <div class="form-group">
                        <label>Choices</label>
                        <button type="button" class="btn btn-sm pull-right" id="addChoiceText{{ $question->id }}">
                            <i class="fa fa-keyboard-o" aria-hidden="true"></i>
                        </button>
                        <button type="button" class="btn btn-sm pull-right" id="addChoiceImage">
                            <i class="fa fa-file-image-o" aria-hidden="true"></i>
                        </button>

                        <div class="list-group">
                            <div class="list-group-item" id="questionBlock{{ $question->id }}">
                                <input name="question" type="hidden" id="questionJson{{ $question->id }}" />
                            </div>

                            @foreach($question->choices as $key => $choice)
                            <div class="list-group-item">

                                <div class="radio">
                                    <input name="answer" type="radio" value="{{ $choice->id }}" {{ ($choice->is_correct_answer) ? "checked" : "" }}>
                                    <label>
                                    @if(strpos($choice, 'data:image'))
                                        <img src="{{ $choice->body }}">
                                    @else
                                        {{ $choice->body }}
                                    @endif

                                    </label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="assessment_id" value="{{ $assessment_id }}">
                    <input type="hidden" name="question_id" value="{{ $question->id }}">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a data-toggle="modal" data-target="#{{ $question->id }}"  type="button" class="btn btn-danger btn-icon m-r-xs m-b-xs pull-left"><i class="material-icons">delete </i><span>Delete</span>
                    </a>
                </div>
            </form>

        </div>
    </div>
</div>

<div id="{{ $question->id }}" class="modal modal-sm fade in">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Delete question</h4>
        </div>
        <div class="modal-body">Are you sure you want to delete it?</div>
        <div class="modal-footer">

            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <a class="btn btn-danger" href="{{ route('sunstone.assessment.delete_question', ['question_id' => $question->id, 'assessment_id' => $assessment_id]) }}">DELETE</a>
        </div>
    </div>
</div>
