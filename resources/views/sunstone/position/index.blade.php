<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

<div class="row">
	<div class="col-lg-12">
        <div class="card">
        	<div class="card-header no-bg b-a-0">Position List {!!view('sunstone.position.position-modal')!!}</div>

        		<div class="card-block">

					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#active" role="tab" aria-expanded="true">Active</a>
						</li>
						<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#inactive" role="tab" aria-expanded="false">Inactive</a>
						</li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane active" id="active" role="tabpanel" aria-expanded="true">
							<table class="table">
								<thead class="thead-inverse">
								<tr>
									<th>Position Name</th>
									<th>Alias</th>
									<th>Type</th>
									<th>Created at</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								@foreach($positions as $value)
									@if($value->status == 1)
										<tr>
											<td>{{$value->name}}</td>
											<td>{{$value->alias}}</td>
											<td>{{($value->type == 1) ? 'Agent' :  'Non-Agent'}}</td>
											<td>{{$value->created_at}}</td>
											<td>
												<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#position{{ $value->id }}">
													<i class="material-icons " aria-hidden="true">edit </i>
												</button>
												{!! view('sunstone.position.edit-modal', compact('value')) !!}
											</td>
										</tr>
									@endif
								@endforeach
								</tbody>
							</table>
						</div>
						<div class="tab-pane" id="inactive" role="tabpanel" aria-expanded="true">
							<table class="table">
								<thead class="thead-inverse">
								<tr>
									<th>Position Name</th>
									<th>Alias</th>
									<th>Type</th>
									<th>Created at</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								@foreach($positions as $value)
									@if($value->status == 2)
										<tr>
											<td>{{$value->name}}</td>
											<td>{{$value->alias}}</td>
											<td>{{($value->type == 2) ? 'Agent' :  'Non-Agent'}}</td>
											<td>{{$value->created_at}}</td>
											<td>
												<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#position{{ $value->id }}">
													<i class="material-icons " aria-hidden="true">edit </i>
												</button>
												{!! view('sunstone.position.edit-modal', compact('value')) !!}
											</td>
										</tr>
									@endif
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>

@endsection 


@section('footer-scripts')

@endsection