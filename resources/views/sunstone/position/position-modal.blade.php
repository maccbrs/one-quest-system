<button id="positionModalBtn" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".conn-modal-campaign">
  <i class="material-icons " aria-hidden="true">add </i>
</button>
<div class="modal fade conn-modal-campaign" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form id="addPositionForm" method="post" action="{{route('sunstone.position.add')}}" validate>
        {{ csrf_field() }}    

          <input type="hidden"   name = "status" value = 1  >

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Campaign Information</h4>
          </div>
          <div class="modal-body">

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label for="recipient-name" class="control-label">Position Name</label>
                </div>

                <div class="col-xs-10">
                  <input type="text" class="form-control disputelabelinput2 "  name = "name"  required>
                </div>
              </div> 
             </div> 

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label for="recipient-name" class="control-label">Alias</label>
                </div>

                <div class="col-xs-10">
                  <input type="text" class="form-control disputelabelinput2 "   name = "alias" required>
                </div>
              </div> 
             </div> 
            

            <div class = "row">
              <div class="form-group">
                <div class="col-xs-2">
                  <label for="recipient-name" class="control-label">Type</label>
                </div>

                <div class="col-xs-10">
                   <select class="form-control " name="type" required>
                  <option selected value ='1'>Agent</option>
                  <option value ='2'>Non-Agent</option>
                </select> 
                </div>
              </div> 
             </div> 
          

             </div> 

          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" id="savePositionBtn">Save</button>
          </div>
        </form>
      </div>
  </div>
</div> 