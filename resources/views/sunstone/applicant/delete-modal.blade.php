<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".delete-modal-{{$details->id}}">
  Delete
</button> 
<div class="modal fade delete-modal-{{$details->id}}  modal-field" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">

        <form class="delete" action="{{route('sunstone.applicant.delete',$details->id)}}" name="delete_button" method="POST">

          
          {{ csrf_field() }}  

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Delete this Field?</h4>
          </div>

          <input type="hidden" value = "{{$details->id}}"  name = "id" > 

           <div class="modal-body">
            <div class = "row">
              <div class="col-lg-6">
                <fieldset class="form-group">
                  <label for="exampleInputEmail1">Label</label>
                  <input type="text" class="form-control" name="type" disabled placeholder="Input Field Name" value = '{{$details->label}}'> 
                </fieldset>
              </div>

              <div class="col-lg-6">
                <fieldset class="form-group">
                  <label for="exampleInputEmail1">Value</label>
                  <input type="text" class="form-control" name="type" disabled placeholder="Input Field Name" value = '{{$details->value}}'> 
                </fieldset>
              </div>
            </div>
          </div>
          
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            <button type="submit" class="btn btn-warning" name="delete_button">Delete</button>

          </div>
          
        </form>
  
      </div>
  </div>
</div> 

