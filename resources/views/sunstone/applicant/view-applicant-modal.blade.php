<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".conn-modal-{{$details->id}}">
    <i class="material-icons " aria-hidden="true">mode_edit </i>
</button>

@if(!empty($details->additional))
    @if(empty($details->attachment))
        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".conn-modal-attach-{{$details->id}}">
            <i class="material-icons">attachment</i>
        </button>
    @else
        <button class="btn btn-primary btn-sm">
            <a href="{{ route('sunstone.applicant.download_resume', ['applicant_id' => $details->id]) }}">
                <i class="material-icons">system_update_alt</i>
            </a>
        </button>
    @endif
@endif

<div class="modal fade conn-modal-attach-{{$details->id}}" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Attach Resume</h4>
            </div>

            <form method="post" action="{{route('sunstone.applicant.attach_resume',$details->id)}}" enctype="multipart/form-data"  novalidate>
                {{ csrf_field() }}

                <div class="modal-body">

                    <script>
                        $(function() {
                            $(document).on('change', ':file', function() {
                                var input = $(this),
                                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                                input.trigger('fileselect', [numFiles, label]);
                            });
                            $(document).ready( function() {
                                $(':file').on('fileselect', function(event, numFiles, label) {

                                    var input = $(this).parents('.input-group').find(':text'),
                                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                                    if( input.length ) {
                                        input.val(log);
                                    } else {
                                        if( log ) alert(log);
                                    }
                                });
                            });
                        });
                    </script>

                    <div class="container" style="margin-top: 20px;">
                        <div class="row">

                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="input-group">
                                    <label class="input-group-btn">
                    <span class="btn btn-primary">
                        Browse&hellip; <input name="resume" type="file" style="display: none;" multiple>
                    </span>
                                    </label>
                                    <input type="text" name="resume" class="form-control" style="width: 300px" readonly>
                                    <input type="hidden" value="{{ $details->id }}" name="id" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Post</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade conn-modal-{{$details->id}}" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('sunstone.applicant.update_applicant',$details->id)}}" validate>
        {{ csrf_field() }}    

          <input type="hidden" value = "{{$details->id}}"  name = "id" > 
          <input type="hidden" value = "{{!empty($details->position->type) ? $details->position->type : " "}}"  name = "position_type" > 

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Applicant Information</h4>
          </div>
          <div class="modal-body">

              <div class = "row">
                  <div class="form-group">
                      <div class="col-xs-2">
                          <label for="recipient-name" class="control-label">Last Name</label>
                      </div>

                      <div class="col-xs-10">
                          <input type="text" class="form-control disputelabelinput2 " value = "{{$details->last_name}}"  name = "last_name"  >
                      </div>
                  </div>
              </div>

              <div class = "row">
                  <div class="form-group">
                      <div class="col-xs-2">
                          <label for="recipient-name" class="control-label">First Name</label>
                      </div>

                      <div class="col-xs-10">
                          <input type="text" class="form-control disputelabelinput2 " value = "{{$details->first_name}}"  name = "first_name"  >
                      </div>
                  </div>
              </div>

              <div class = "row">
                  <div class="form-group">
                      <div class="col-xs-2">
                          <label for="recipient-name" class="control-label">Middle Name</label>
                      </div>

                      <div class="col-xs-10">
                          <input type="text" class="form-control disputelabelinput2 " value = "{{$details->middle_name}}"  name = "middle_name"  >
                      </div>
                  </div>
              </div>
              <div class = "row">
                  <div class="form-group">
                      <div class="col-xs-2">
                          <label for="recipient-name" class="control-label">Position</label>
                      </div>

                      <div class="col-xs-10">
                          <select class="form-control" name="position_id" >
                              @foreach($positions as $position)
                                  <option  value ='{{$position->id}}'>{{$position->name}}</option>
                              @endforeach
                          </select>
                      </div>
                  </div>
              </div>

              <div class = "row">
                  <div class="form-group">
                      <div class="col-xs-2">
                          <label for="recipient-name" class="control-label">Status</label>
                      </div>

                      <div class="col-xs-10">
                          <select class="form-control applicant_status" name="status" >
                              <option value = '5'>For-training</option>
                              <option value = '4'>Ready-hire</option>
                              <option value = '3'>Near-hire</option>
                              <option value = '2'>Far-hire</option>
                              <option selected value = '1'>Pending</option>
                          </select>
                      </div>
                  </div>
              </div>

              <div class = "row campaign_select">
                  <div class="form-group">
                      <div class="col-xs-2">
                          <label for="recipient-name" class="control-label">Campaign</label>
                      </div>

                      <div class="col-xs-10">
                          <select class="form-control" name="camp_id" >
                              @foreach($campaigns as $campaign)
                                  <option  value = '{{$campaign->id}}'>{{$campaign->title}}</option>
                              @endforeach
                          </select>
                      </div>
                  </div>
              </div>


              @if(!empty(json_decode($details->additional)))
                  @foreach(json_decode($details->additional) as $key => $value)
                      <div class = "row">
                          <div class="form-group">
                              <div class="col-xs-2">
                                  <label for="recipient-name" class="control-label">{{$key}}</label>
                              </div>
                              <?php $header = 'additional['. $key.']'?>
                              <div class="col-xs-10">
                                  @if(is_array ($value))
                                      @foreach($value as $checkbox)
                                          {{ $checkbox }}
                                      @endforeach
                                  @else
                                  <input type="text" class="form-control disputelabelinput2 " value = "{{$value}}"  name ="{{$header}}"  >
                                  @endif
                              </div>
                          </div>
                      </div>
                  @endforeach
              @endif

            </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
  </div>
</div> 