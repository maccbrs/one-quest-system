<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

	<div class="row">
		<div class="col-lg-6">
            <div class="card">

               <div class="card-header no-bg b-a-0">Add Information Fields for Applicants</div>

               <div class="card-block">
                  <p>Add Fields for Applicant</p>
                  
                  <form class="form-horizontal form-label-left" method="post" enctype ='multipart/form-data' action="{{route('sunstone.applicant.add_info')}}" novalidate>

                  	{{ csrf_field() }}

                  	<input type="hidden" value =1 name="status" > 

                  	<div class="col-lg-6">
	                	<fieldset class="form-group">
	                     	<label for="exampleInputEmail1">Type</label>
	                     	<select class="form-control " id = "input_type" name="type">
	                           <option value = 'text'>Text</option>
	                           <option value = 'select'>Dropdown</option>
	                           <option value = 'check'>Check box</option>
	                           <option value = 'textarea'>Text Area</option>
	                           <option value = 'radio'>Radio</option>
	                        </select> 
	                    </fieldset>
	                </div>

	                <div class="col-lg-3">
	                    <fieldset class="form-group">
	                     	<label for="exampleInputEmail1">Require Status</label>
	                     	<select class="form-control" name="required_status">
	                           <option value = 'required'>Yes</option>
	                           <option value = ' '>No</option>
	                        </select> 
	                    </fieldset>
	                </div>

	                <div class="col-lg-3">
	                    <fieldset class="form-group">
	                     	<label for="exampleInputEmail1">Size</label>
	                     	<select class="form-control" name="size">
	                           <option value = 12>100%</option>
	                           <option value = 9>75%</option>
	                           <option value = 6>50%</option>
	                           <option value = 3>25%</option>
	                        </select> 
	                    </fieldset>
	                </div>

	                <div class="col-lg-6">
	                    <fieldset class="form-group">
	                     	<label for="exampleInputEmail1">Key</label>
	                     	<input type="text" class="form-control" name="key" placeholder="Input Field Name"> 
	                     	<small> No spaces and special characters </small>
	                    </fieldset>
	                </div>

                  	<div class="col-lg-6">
	                    <fieldset class="form-group">
	                     	<label for="exampleInputEmail1">Label</label>
	                     	<input type="text" class="form-control" name="label" placeholder="Input Field Label">
	                     	<small> This will appear as Field Label </small>
	                    </fieldset>
	                </div>

	                <div class ="choices">
		                <div class="col-lg-6">
		                	<fieldset class="form-group">
		                     	<label for="exampleInputEmail1">Option Values</label>
		                     	<textarea  class="form-control" name="option_values" placeholder="Input Text Field Value/Options"> </textarea>
		                     	<small> Input Values seperated by comma (','), no spaces and special characters </small>
		                    </fieldset>
		                </div>

		               	<div class="col-lg-6">
		                	<fieldset class="form-group">
		                     	<label for="exampleInputEmail1">Option Labels</label>
		                     	<textarea  class="form-control" name="label_values" placeholder="Input Text Field Value/Options">  </textarea>
		                     	<small> Input Values seperated by comma (',') </small>
		                    </fieldset>
		                </div>
		            </div>

                     <button type="submit" class="btn btn-primary pull-right">Submit</button>

                  </form>

               </div>
            </div>
		</div>

		<div class="col-lg-6 form-holder">

			<div class="card sample-form">
			
				<div class="card-header no-bg b-a-0">
					<div class="col-lg-9"></div>
	                    <div class="col-lg-1">
	                        <a href="{{route('sunstone.applicant.edit')}}">
	                            <button type="submit" class="btn btn-primary pull-right">Edit</button>
	                        </a>
	                    </div>
	                <div class="col-lg-2">
	                    <a href="{{route('sunstone.applicant.sort')}}">
	                        <button type="submit" class="btn btn-primary pull-right">Sort</button>
	                    </a>
	                </div>

				</div>

               <div class="card-block">


               <div class="col-lg-4">
                    <fieldset class="form-group">
                        <label for="exampleInputEmail1">*Last Name</label>
                        <input type="text" class="form-control" placeholder="Last Name" name ="last_name" disabled> 
                    </fieldset>
                </div>

                <div class="col-lg-4">
                    <fieldset class="form-group">
                        <label for="exampleInputEmail1">*First Name</label>
                        <input type="text" class="form-control"  placeholder="First Name" name ="first_name" disabled > 
                    </fieldset>
                </div>

                <div class="col-lg-4">
                    <fieldset class="form-group">
                        <label for="exampleInputEmail1">*Middle Name</label>
                        <input type="text" class="form-control"  placeholder="Middle Name" name ="middle_name" disabled> 
                    </fieldset>
                </div>

                <div class="col-lg-6">
                    <fieldset class="form-group">
                        <label for="exampleInputEmail1">*Position</label>
                        <select  class="form-control"   name ="position_id" disabled>
                            <option  disabled >Select a Position</option>
                            @foreach($positions as $key => $position)
                                <option  value='{{$key}}'>{{$position}}</option>
                            @endforeach
                        </select>
                    </fieldset>
                </div>

                <div class="col-lg-6">
                    <fieldset class="form-group">
                        <label >*Application Date</label>
                        <input name = "source_date" class="form-control m-b-1 datepicker" data-provide="datepicker" disabled > 
                    </fieldset>
                </div>


	                @foreach($formfields as $field)

	                    <div class="col-lg-{{$field->size}}">
	                    	<fieldset class="form-group">
		                     	<label for="exampleInputEmail1">{{(($field->required_status == 'required' )? '*' : '') . $field->label}}</label>

		                     	<?php
								    switch( $field->type) {

								        case 'text': echo 

								        	'<input type="text" class="form-control"  disabled  disabled '.$field->required_status.'> '; 

								        break;

								        case 'radio':  

								        	$options =  $field->option_values;
								        	$Options_values = explode(',', $options);
								        	$options =  $field->label_values;
								        	$label_values = explode(',', $options);

								        	echo '<br>' ;

											foreach ($Options_values as $key => $value) {
												
												echo ' <input type="radio"  name='.$field->key.'  checked disabled>'. $label_values[$key] .'&nbsp &nbsp' ;

											}

								        break;

								        case 'textarea':

								        	echo '<textarea class="form-control"   form="usrform" disabled '.$field->required_status.'></textarea>'; 

								        break;

								        case 'select':   

								        	$options =  $field->option_values;
								        	$Options_values = explode(',', $options);
								        	$options =  $field->label_values;
								        	$label_values = explode(',', $options);

								        	echo '<br>' ;

								        	echo '<select disabled class="form-control" value='.$field->key.' '.$field->required_status.'>';

												foreach ($Options_values as $key => $value) {
													echo 	'<option value = "text">'.$value.'</option>';
									        		
												}

					                      	echo   '</select>' ; 

					                    break;

								        case 'check':   

								        	$options =  $field->option_values;
								        	$Options_values = explode(',', $options);
								        	$options =  $field->label_values;
								        	$label_values = explode(',', $options);

								        	echo '<br>' ;

								        		foreach ($Options_values as $key => $value) {

								        			echo '<input type="checkbox"  value='.$field->key.'"  disabled>' . $label_values[$key] .'&nbsp &nbsp'; 
								        		}

								        break;
								    }
								?>
		                    </fieldset>
		                </div>
	                @endforeach

            	</div>
        	</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$( document ).ready(function() {

	    $( ".choices" ).hide();
	    
	});

    $( "#input_type" ).change(function() {
    	type_val = $( this ).val();

    	if(type_val == 'text' ||  type_val == 'textarea'){

	  		$( ".choices" ).hide();
	  	}else{

	  		$( ".choices" ).show();
	  	}
	});

</script>

@endsection 

@section('footer-scripts')

@endsection