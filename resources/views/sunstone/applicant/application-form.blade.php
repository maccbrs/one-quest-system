<?php $asset = URL::asset('/'); ?> 
@extends('layouts.applicant')

@section('content')

@if($formfields != null)
    <ul class="nav nav-pills nav-stacked col-sm-1">
        <li class="nav-item"><a class="nav-link active" href="{{ route('sunstone.applicant.application-form') }}">Application Form</a>
        </li>
        <li class="nav-item"><a class="nav-link" href="{{ route('sunstone.assessment.exam-search') }}">Online Exam</a>
        </li>
    </ul>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Application Form</div>

                        <form class="form-horizontal form-label-left" id = 'formID' method="post" enctype ='multipart/form-data' action="{{route('sunstone.applicant.application-save')}}" novalidate>

                        {{ csrf_field() }}

                        <input type="hidden" value =1 name="status" > 

                        <div class="panel-body">
                            <div class="col-lg-12 form-holder">
                                <div class="card sample-form">
                
                                   <div class="card-block">

                                        <div class="col-lg-4">
                                            <fieldset class="form-group">
                                                <label for="exampleInputEmail1">*Last Name</label>
                                                <input type="text" class="form-control" placeholder="Last Name" name ="last_name" required = "true"> 
                                            </fieldset>
                                        </div>

                                        <div class="col-lg-4">
                                            <fieldset class="form-group">
                                                <label for="exampleInputEmail1">*First Name</label>
                                                <input type="text" class="form-control"  placeholder="First Name" name ="first_name" required = "true"> 
                                            </fieldset>
                                        </div>

                                        <div class="col-lg-4">
                                            <fieldset class="form-group">
                                                <label for="exampleInputEmail1">*Middle Name</label>
                                                <input type="text" class="form-control"  placeholder="Middle Name" name ="middle_name" required = "true"> 
                                            </fieldset>
                                        </div>

                                        <div class="col-lg-12">
                                            <fieldset class="form-group">
                                                <label for="exampleInputEmail1">*Position</label>
                                                <select  class="form-control"   name ="position_id" >
                                                    <option  disabled >Select a Position</option>
                                                    @foreach($positions as $key => $position)
                                                        <option  value='{{$key}}'>{{$position}}</option>
                                                    @endforeach
                                                </select>
                                            </fieldset>
                                        </div>


                                        <div class="col-lg-6">
                                            <fieldset class="form-group">
                                                <label >*Application Date</label>
                                                <input name = "source_date" class="form-control m-b-1 datepicker" data-provide="datepicker"  > 
                                            </fieldset>
                                        </div>

                                            @foreach($formfields as $field)

                                                <div class="col-lg-{{$field->size}}">
                                                    <fieldset class="form-group">
                                                        <label for="exampleInputEmail1">{{(($field->required_status == 'required' )? '*' : '') . $field->label}}</label>

                                                        <?php
                                                            switch( $field->type) {

                                                                case 'text': echo 

                                                                    '<input type="text" class="form-control" name = "additional['.$field->key.']"    '.$field->required_status.'> '; 

                                                                break;

                                                                case 'textarea':

                                                                    echo '<textarea class="form-control" name = "additional['.$field->key.']"   form="usrform"  '.$field->required_status.'></textarea>'; 

                                                                break;

                                                                case 'select':   

                                                                    $labels_values = array_combine(explode(',', $field->label_values),
                                                                        explode(',', $field->option_values));

                                                                    echo '<br>' ;

                                                                    echo '<select  class="form-control" name="additional['.$field->key.']" '.$field->required_status.'>';

                                                                        foreach ($labels_values as $label => $value) {
                                                                            echo    '<option value = "'. $value .'">'.$label.'</option>';
                                                                            
                                                                        }

                                                                    echo   '</select>' ; 

                                                                break;

                                                                case 'radio':  

                                                                    $options =  $field->option_values;
                                                                    $Options_values = explode(',', $options);
                                                                    $options =  $field->label_values;
                                                                    $label_values = explode(',', $options);

                                                                    echo '<br>' ;

                                                                    foreach ($Options_values as $key => $value) {
                                                                        
                                                                        echo ' <input type="radio"  name='.$field->key.'  checked >'. $label_values[$key] .'&nbsp &nbsp' ;

                                                                    }

                                                                break;


                                                                case 'check':

                                                                    $labels_values = array_combine(explode(',', $field->label_values),
                                                                        explode(',', $field->option_values));

                                                                    echo '<br>' ;

                                                                        foreach ($labels_values as $label => $value) {

                                                                            echo '&nbsp;&nbsp;<input type="checkbox"  name="additional['.$field->key.'][]"  value="' . $value . '"">' . $label .'&nbsp &nbsp';
                                                                        }


                                                                break;
                                                            }
                                                        ?>
                                                    </fieldset>
                                                </div>
                                            @endforeach


                                            <div class="col-lg-12">

                                              <button type="submit" class="btn btn-primary pull-right">Submit</button>

                                            </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>

        var form = document.getElementById('formID'); 
        form.noValidate = true;
        form.addEventListener('submit', function(event) { 
                if (!event.target.checkValidity()) {
                    event.preventDefault(); 
                    swal("Check one of the required fields!", "Something is missing.") 
                }
            }, false);


    </script>




@else

    <ul class="nav nav-pills nav-stacked col-sm-1">
        <li class="nav-item"><a class="nav-link active" href="{{ route('sunstone.applicant.application-form') }}">Application Form</a>
        </li>
        <li class="nav-item"><a class="nav-link" href="{{ route('sunstone.assessment.exam-search') }}">Online Exam</a>
        </li>
    </ul>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" style = "margin-top: 80PX; text-align: center;">
                <div class="panel panel-default" style = "padding: 20px;">
                     <img src="{{$asset}}milestone/images/thankyou.png"  >
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10 col-md-offset-1" style = "margin-top: 80PX; text-align: center;">
                <div class="alert alert-danger">This is your Application ID: <h3>{{ $applicant_id }}</h3>. Please take note of this number.
                You will need it for your online assessment.</div>
            </div>
        </div>

    </div>

    <script>
/*
        window.setTimeout(function(){

            var referrer = document.referrer;
            window.location.href = referrer;

    }, 4500);*/

    </script>

@endif


    <script src="{{$asset}}milestone/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>



@endsection
