<div class="modal fade" id="jo-modal{{ $applicant->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Job Offer</h4>
            </div>

            <form id="interview{{ $applicant->id }}" method="post" action="{{ route('sunstone.applicant.handle-job-offer') }}" enctype="multipart/form-data" validate>
                {{ csrf_field() }}
                <div class="modal-body">
                    @if(count($applicant->interviews) > 0)
                        <div class="row">
                            <div class="col-lg-12">
                                <fieldset class="form-group">
                                    <label>Interview History</label>
                                    <?php $text="" ; foreach ($applicant->interviews as $interview){ $text = $text."[".$interview->type."@".$interview->created_at."] ". $interview->interviewer_name.": ".$interview->remarks."&#013;&#010;"; } ?>
                                    <textarea rows="8" cols="90" name="interview_history" disabled>{{ $text }}</textarea>
                                </fieldset>
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-10">
                            <fieldset class="form-group">
                                <label>New Interview Remarks</label>
                                <textarea rows="8" cols="90" name="interview_remarks" placeholder="Please leave your interview remarks here..." required></textarea>
                            </fieldset>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-7">
                            <fieldset class="form-group">
                                <label>Interviewer's Name</label>
                                <input type="text" name="interviewer_name" class="form-control" placeholder="Who conducted the interview?" required>
                            </fieldset>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <input type="hidden" name="applicant_id" value="{{ $applicant->id }}">
                        <input type="submit" value="Put on Job Offer queue" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#interview{{ $applicant->id }} select[name='endorsed_campaigns[]']").multiselect({
            buttonWidth: '400px'
        });
        $("#interview{{ $applicant->id }} select[name='endorsed_campaigns[]']").multiselect('refresh');
    });
</script>