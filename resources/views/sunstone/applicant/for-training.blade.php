<?php $asset = URL::asset('/'); ?>
@extends('gem.master2')

@section('title', 'index')

@section('content')

    <div class="card">
        <h4 class="card-header">
            Applicants Endorsed to Training

            <div class="search-applicants-block">
                <form class="form-horizontal form-label-right" method="post" enctype ='multipart/form-data' action="{{route('sunstone.applicant.search')}}" novalidate>
                    {{ csrf_field() }}
                    <span>SEARCH:</span>
                    <input type="text" name="search_key" class="form-control-sm" placeholder="Search key">
                    <select class="form-control-sm" name="search_field">
                        <option value="id">ID</option>
                        <option value="name">Name</option>
                        <option value="position">Position</option>
                    </select>
                    <button type="submit" class="btn btn-sm">Submit</button>
                </form>
            </div>

        </h4>
        <div class="card-block">
            @if($applicants->isEmpty())
                <div class="card-header no-bg b-a-0">No record found</div>
            @else
                <table class="table">
                    <thead class="thead-inverse">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($applicants as $applicant)
                        <tr>
                            @if($applicant->status == 5)
                                <td>{{ $applicant->id }}</td>
                                <td>{{ $applicant->first_name }} {{ $applicant->middel_name }} {{ $applicant->last_name }}</td>
                                <td>{{ $applicant->position->name }}</td>
                                <td class="row">
                                    <div class="col-sm-4">
                                        {!!view('sunstone.applicant.view-applicant-modal',['details'=>$applicant,'positions' => $positions,'campaigns' => $campaigns])!!}
                                    </div>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>


@endsection

@section('footer-scripts')

@endsection