<div class="modal fade" id="modal{{ $applicant->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Interview Remarks</h4>
            </div>

            <form id="interview{{ $applicant->id }}" method="post" action="{{ route('sunstone.applicant.interview-dispo') }}" enctype="multipart/form-data"  validate>
                {{ csrf_field() }}
                <div class="modal-body row">
                    @if(count($applicant->interviews) > 0)
                    <div class="col-lg-12">
                        <fieldset class="form-group">
                            <label>Interview History</label>
                            <?php
                            $text = "";
                            foreach ($applicant->interviews as $interview){
                                $text = "[".$interview->type."@".$interview->created_at."] ".
                                    $interview->interviewer_name.": ".$interview->remarks."&#013;&#010;";
                            }
                            ?>
                            <textarea rows="8" cols="90" name="interview_history" disabled>{{ $text }}</textarea>
                        </fieldset>
                    </div>
                    @endif

                    <div class="col-lg-12">
                        <fieldset class="form-group">
                            <label>New Interview Remarks</label>
                            <textarea rows="8" cols="90" name="interview_remarks" placeholder="Please leave your interview remarks here..." required></textarea>
                        </fieldset>
                    </div>

                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label>Interview Type</label>
                            <select value="{{$applicant->interview_type}}" name="interview_type" class="form-control" required>
                                <option style="display:none">Please select Interview Type</option>
                                <option {{ ($applicant->interview_type == "onsite") ? "selected" : "" }} value="onsite">On-site</option>
                                <option {{ ($applicant->interview_type == "phone") ? "selected" : "" }} value="phone">Over-the-phone</option>
                            </select>
                        </fieldset>
                    </div>

                    <div class="col-lg-7">
                        <fieldset class="form-group">
                            <label>Interviewer's Name</label>
                            <input type="text" name="interviewer_name" class="form-control" placeholder="Who conducted the interview?" required>
                        </fieldset>
                    </div>


                    <div class="col-lg-7">
                        <fieldset class="form-group">
                            <label>Interview Disposition</label>
                            <select name="interview_dispo" class="form-control" required>
                                <option style="display:none">Please select Interview Disposition</option>
                                <option {{ ($applicant->status == 4) ? "selected" : "" }} value="4">Ready-hire</option>
                                <option {{ ($applicant->status == 3) ? "selected" : "" }} value="3">Near-hire</option>
                                <option {{ ($applicant->status == 2) ? "selected" : "" }} value="2">Far-hire</option>
                                <option value="6">Endorse to Department</option>
                            </select>
                        </fieldset>
                    </div>
                    @if($applicant->position->type == 1)
                    <div class="col-lg-7">
                        <fieldset class="form-group">
                            <label>Campaign</label>
                            <select multiple="multiple" class="form-control" name="endorsed_campaigns[]" style="width: 300px">
                                @foreach($campaigns as $campaign)
                                    <option value="{{$campaign->id}}">{{$campaign->title}}</option>
                                @endforeach
                            </select>
                        </fieldset>
                    </div>
                    @endif
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="applicant_id" value="{{ $applicant->id }}">
                    <input type="submit" value="Dispose" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#interview{{ $applicant->id }} select[name='endorsed_campaigns[]']").multiselect({
            buttonWidth: '400px'
        });
        $("#interview{{ $applicant->id }} select[name='endorsed_campaigns[]']").multiselect('refresh');
    });
</script>
