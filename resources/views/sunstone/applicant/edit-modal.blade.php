<button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".conn-modal-{{$details->id}}">
  Edit
</button> 
<div class="modal fade conn-modal-{{$details->id}}  modal-field change_type" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="{{route('sunstone.applicant.update_field',$details->id)}}" name="update_button"  novalidate>
        {{ csrf_field() }}    

          <input type="hidden" value = "{{$details->id}}"  name = "id" > 
         

          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Edit Field</h4>
          </div>

          <div class="modal-body">
              <div class = "row">
                <div class="col-lg-6">
                    <fieldset class="form-group">
                        <label for="exampleInputEmail1">Type</label>
                          <input type="text" class="form-control" name="type" placeholder="Input Field Name" value = "{{$details->type}}" readonly> 
                      </fieldset>
                  </div>

                  <div class="col-lg-3">
                      <fieldset class="form-group">
                        <label for="exampleInputEmail1">Require Status</label>
                        <select class="form-control" name="required_status">
                             <option value = 'required'>Yes</option>
                             <option value = ' '>No</option>
                          </select> 
                      </fieldset>
                  </div>

                  <div class="col-lg-3">
                      <fieldset class="form-group">
                        <label for="exampleInputEmail1">Size</label>
                        <select class="form-control" name="size">
                             <option value = 12>100%</option>
                             <option value = 9>75%</option>
                             <option value = 6>50%</option>
                             <option value = 3>25%</option>
                          </select> 
                      </fieldset>
                  </div>

                  <div class="col-lg-6">
                      <fieldset class="form-group">
                        <label for="exampleInputEmail1">Key</label>
                        <input type="text" class="form-control" name="key" placeholder="Input Field Name" readonly> 
                        <small> You cannot edit </small>
                      </fieldset>
                  </div>

                    <div class="col-lg-6">
                      <fieldset class="form-group">
                        <label for="exampleInputEmail1">Label</label>
                        <input type="text" class="form-control" name="label" placeholder="Input Field Label" value = '{{$details->label}}'>
                        <small> This will appear as Field Label </small>
                      </fieldset>
                  </div>

                @if(!empty($details['option_values']))

                  <div class ="choices">
                    <div class="col-lg-6">
                      <fieldset class="form-group">
                          <label for="exampleInputEmail1">Option Values</label>
                          <textarea  class="form-control" name="option_values" placeholder="Input Text Field Value/Options">{{$details->option_values}} </textarea>
                          <small> Input Values seperated by comma (','), no spaces and special characters </small>
                        </fieldset>
                    </div>

                    <div class="col-lg-6">
                      <fieldset class="form-group">
                          <label for="exampleInputEmail1">Option Labels</label>
                          <textarea  class="form-control" name="label_values" placeholder="Input Text Field Value/Options">{{$details->label_values}}</textarea>
                          <small> Input Values seperated by comma (',') </small>
                        </fieldset>
                    </div>
                </div>
              @endif
              
          </div>
        </div>  

          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" name="update_button" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
  </div>
</div> 

