<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('content')

<div class="content-view">
    <div class="row">
        <div class="col-md-6">
        
            <h6 class="m-b-12">Edit</h6>
          
            {{ csrf_field() }}    

            <ul class="sortable-list connectedSortable mbin" style="min-height:100px;">
                @if(!empty($formfields))
                @foreach($formfields as $key => $v)
                    <li data-key="{{$v->key}}" data-label="{{$v->label}}"> 
                        <code>{{$v->key}}</code>{{$v->label}} 
                        <input type = "hidden" name="test[]" value = "{{$v->id}}"   > 
                        <div class = "pull-right" style = "margin-right: 5px;">
                            <div class="col-xs-7">
                                {!!view('sunstone.applicant.edit-modal',['details'=>$v])!!}  
                            </div>
                            <div class="col-xs-5">
                                {!!view('sunstone.applicant.delete-modal',['details'=>$v])!!}  
                            </div>
                        </div>
                    </li>
                @endforeach
            @else
                <li data-key="null" style="display:hidden;">Empty</li>
            @endif
            </ul>

        </div>

        <div class="col-md-6">

            <div class="card sample-form">
               <div class="card-block">
                   <div class="col-lg-4">
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">*Last Name</label>
                            <input type="text" class="form-control" placeholder="Last Name" name ="last_name" disabled> 
                        </fieldset>
                    </div>

                    <div class="col-lg-4">
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">*First Name</label>
                            <input type="text" class="form-control"  placeholder="First Name" name ="first_name" disabled > 
                        </fieldset>
                    </div>

                    <div class="col-lg-4">
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">*Middle Name</label>
                            <input type="text" class="form-control"  placeholder="Middle Name" name ="middle_name" disabled> 
                        </fieldset>
                    </div>

                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label for="exampleInputEmail1">*Position</label>
                            <select  class="form-control"   name ="position_id" disabled>
                                <option  disabled >Select a Position</option>
                                @foreach($positions as $key => $position)
                                    <option  value='{{$key}}'>{{$position}}</option>
                                @endforeach
                            </select>
                        </fieldset>
                    </div>

                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label >*Application Date</label>
                            <input name = "source_date" class="form-control m-b-1 datepicker" data-provide="datepicker" disabled > 
                        </fieldset>
                    </div>

                    @foreach($formfields as $field)

                        <div class="col-lg-{{$field->size}}">
                            <fieldset class="form-group">
                                <label for="exampleInputEmail1">{{(($field->required_status == 'required' )? '*' : '') . $field->label}}</label>

                                <?php
                                    switch( $field->type) {

                                        case 'text': echo 

                                            '<input type="text" class="form-control"  disabled  disabled '.$field->required_status.'> '; 

                                        break;

                                        case 'radio':  

                                            $options =  $field->option_values;
                                            $Options_values = explode(',', $options);
                                            $options =  $field->label_values;
                                            $label_values = explode(',', $options);

                                            echo '<br>' ;

                                            foreach ($Options_values as $key => $value) {
                                                
                                                echo ' <input type="radio"  name='.$field->key.'  checked disabled>'. $label_values[$key] .'&nbsp &nbsp' ;

                                            }

                                        break;

                                        case 'textarea':

                                            echo '<textarea class="form-control"   form="usrform" disabled '.$field->required_status.'></textarea>'; 

                                        break;

                                        case 'select':   

                                            $options =  $field->option_values;
                                            $Options_values = explode(',', $options);
                                            $options =  $field->label_values;
                                            $label_values = explode(',', $options);

                                            echo '<br>' ;

                                            echo '<select disabled class="form-control" value='.$field->key.' '.$field->required_status.'>';

                                                foreach ($Options_values as $key => $value) {
                                                    echo    '<option value = "text">'.$value.'</option>';
                                                    
                                                }

                                            echo   '</select>' ; 

                                        break;

                                        case 'check':   

                                            $options =  $field->option_values;
                                            $Options_values = explode(',', $options);
                                            $options =  $field->label_values;
                                            $label_values = explode(',', $options);

                                            echo '<br>' ;

                                                foreach ($Options_values as $key => $value) {

                                                    echo '<input type="checkbox"  value='.$field->key.'"  disabled>' . $label_values[$key] .'&nbsp &nbsp'; 
                                                }

                                        break;
                                    }
                                ?>
                            </fieldset>
                        </div>
                    @endforeach

                </div>
            </div>

        </div>
       
       
</div>

              
</div>


@endsection 

@section('footer-scripts')

    <script src="{{$asset}}milestone/vendor/jquery.ui/ui/core.js"></script>
    <script src="{{$asset}}milestone/vendor/jquery.ui/ui/widget.js"></script>
    <script src="{{$asset}}milestone/vendor/jquery.ui/ui/mouse.js"></script>
    <script src="{{$asset}}milestone/vendor/jquery.ui/ui/sortable.js"></script> 

    <script type="text/javascript">

        (function($) {
            'use strict';

            let getdata = (s) => {
                let res = [];
                let items = $(s).find('li').map(function() {
                  var item = { };
                  item.key = $(this).data('key');
                  item.label = $(this).data('label');
                  return item;
                });   
                $.each(items,(key, value) => { 
                    if(value.key){
                     res.push( {key:value.key,label:value.label} ) 
                    }
                }); 
                return res;         
            }

            let process = ()=>{

                let items = {enabled:getdata('.mbin'),disable:getdata('.mbout')};
                

                // $.ajaxSetup({          
                //   headers: {  
                //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                //     }
                // }); 

                // $.ajax({


                //     type:'POST',
                //     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                //     url: '/sunstone/applicant/sort_field',
                //     data: data,
                //     success: function(data){
                //         console.log(data);
                //     },
                //     error: function(error){
                //         console.log(error);
                //     }
                //  },"json");

            };

            $('.sortable-list').sortable({
                placeholder: 'ui-state-highlight',
                connectWith: '.connectedSortable',
                stop: process
            }).disableSelection();

        })(jQuery);





    </script> 


@endsection