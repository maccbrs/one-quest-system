<?php $asset = URL::asset('/'); ?>
@extends('gem.master2')

@section('title', 'index')

@section('content')

    <div class="card">
        <h4 class="card-header">
            Applicants in Pool

            <div class="search-applicants-block">
                <form class="form-horizontal form-label-right" method="post" enctype ='multipart/form-data' action="{{route('sunstone.applicant.search')}}" novalidate>
                    {{ csrf_field() }}
                    <span>SEARCH:</span>
                    <input type="text" name="search_key" class="form-control-sm" placeholder="Search key">
                    <select class="form-control-sm" name="search_field">
                        <option value="id">ID</option>
                        <option value="name">Name</option>
                        <option value="position">Position</option>
                    </select>
                    <button type="submit" class="btn btn-sm">Submit</button>
                </form>
            </div>

        </h4>
        <div class="card-block">
            @if($applicants->isEmpty())
                <div class="card-header no-bg b-a-0">No record found</div>
            @else
            <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#pending" data-toggle="tab">Pending</a>
                </li>
                <li class="nav-item"><a class="nav-link" href="#readyhire" data-toggle="tab">Ready-hire</a>
                </li>
                <li class="nav-item"><a class="nav-link" href="#nearhire" data-toggle="tab">Near-hire</a>
                </li>
                <li class="nav-item"><a class="nav-link" href="#farhire" data-toggle="tab">Far-hire</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="pending" role="tabpanel">
                    <table class="table">
                        <thead class="thead-inverse">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Exam Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($applicants as $applicant)
                            <tr>
                                @if($applicant->status == 1)
                                    <td>{{ $applicant->id }}</td>
                                    <td>{{ $applicant->first_name }} {{ $applicant->middel_name }} {{ $applicant->last_name }}</td>
                                    <td>{{ $applicant->position->name }}</td>
                                    <td>
                                        @if($applicant->exams)
                                            No exam yet
                                        @else
                                            @foreach($applicant->exams as $exam)
                                                {{ $exam->name }}: {{ $exam->is_passed() ? "Passed" : "Failed" }}
                                                ({{ $exam->get_score() }}%)
                                                <br>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="row">
                                        <div class="col-sm-5">
                                 
                                            {!!view('sunstone.applicant.view-applicant-modal',['details'=>$applicant,'positions' => $positions,'campaigns' => $campaigns])!!}
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="{{ route('sunstone.applicant.print-information', $applicant->id) }}" class="btn btn-primary btn-sm">
                                                <i class="material-icons " aria-hidden="true">local_printshop </i>
                                            </a>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal{{ $applicant->id }}">
                                                @if(count($applicant->interviews) < 1)
                                                    Interview <br> Disposition
                                                @else
                                                    Reconsider
                                                @endif
                                            </a>
                                            {!! view('sunstone.applicant.interview-remarks-modal', ['applicant' => $applicant, 'campaigns' => $campaigns]) !!}
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="readyhire" role="tabpanel">
                    <table class="table">
                        <thead class="thead-inverse">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Exam Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($applicants as $applicant)
                            <tr>
                                @if($applicant->status == 4)
                                    <td>{{ $applicant->id }}</td>
                                    <td>{{ $applicant->first_name }} {{ $applicant->middel_name }} {{ $applicant->last_name }}</td>
                                    <td>{{ $applicant->position->name }}</td>
                                    <td>
                                        @if(count($applicant->exams) < 4)
                                            No exam yet
                                        @else
                                            @foreach($applicant->exams as $exam)
                                                {{ $exam->name }}: {{ $exam->is_passed() ? "Passed" : "Failed" }}
                                                ({{ $exam->get_score() }}%)
                                                <br>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="row">
                                        <div class="col-sm-5">
                                            {!!view('sunstone.applicant.view-applicant-modal',['details'=>$applicant,'positions' => $positions,'campaigns' => $campaigns])!!}
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="{{ route('sunstone.applicant.print-information', $applicant->id) }}" class="btn btn-primary btn-sm">
                                                <i class="material-icons " aria-hidden="true">local_printshop </i>
                                            </a>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal{{ $applicant->id }}">
                                                @if(count($applicant->interviews) < 1)
                                                    Interview <br> Disposition
                                                @else
                                                    Reconsider
                                                @endif
                                            </a>
                                            {!! view('sunstone.applicant.interview-remarks-modal', ['applicant' => $applicant, 'campaigns' => $campaigns]) !!}
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="nearhire" role="tabpanel">
                    <table class="table">
                        <thead class="thead-inverse">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Exam Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($applicants as $applicant)
                            <tr>
                                @if($applicant->status == 3)
                                    <td>{{ $applicant->id }}</td>
                                    <td>{{ $applicant->first_name }} {{ $applicant->middel_name }} {{ $applicant->last_name }}</td>
                                    <td>{{ $applicant->position->name }}</td>
                                    <td>
                                        @if(count($applicant->exams) < 4)
                                            No exam yet
                                        @else
                                            @foreach($applicant->exams as $exam)
                                                {{ $exam->name }}: {{ $exam->is_passed() ? "Passed" : "Failed" }}
                                                ({{ $exam->get_score() }}%)
                                                <br>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="row">
                                        <div class="col-sm-5">
                                            {!!view('sunstone.applicant.view-applicant-modal',['details'=>$applicant,'positions' => $positions,'campaigns' => $campaigns])!!}
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="{{ route('sunstone.applicant.print-information', $applicant->id) }}" class="btn btn-primary btn-sm">
                                                <i class="material-icons " aria-hidden="true">local_printshop </i>
                                            </a>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal{{ $applicant->id }}">
                                                @if(count($applicant->interviews) < 1)
                                                    Interview <br> Disposition
                                                @else
                                                    Reconsider
                                                @endif
                                            </a>
                                            {!! view('sunstone.applicant.interview-remarks-modal', ['applicant' => $applicant, 'campaigns' => $campaigns]) !!}
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="farhire" role="tabpanel">
                    <table class="table">
                        <thead class="thead-inverse">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Exam Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($applicants as $applicant)
                            <tr>
                                @if($applicant->status == 2)
                                    <td>{{ $applicant->id }}</td>
                                    <td>{{ $applicant->first_name }} {{ $applicant->middel_name }} {{ $applicant->last_name }}</td>
                                    <td>{{ $applicant->position->name }}</td>
                                    <td>
                                        @if(count($applicant->exams) < 4)
                                            No exam yet
                                        @else
                                            @foreach($applicant->exams as $exam)
                                                {{ $exam->name }}: {{ $exam->is_passed() ? "Passed" : "Failed" }}
                                                ({{ $exam->get_score() }}%)
                                                <br>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="row">
                                        <div class="col-sm-5">
                                            {!!view('sunstone.applicant.view-applicant-modal',['details'=>$applicant,'positions' => $positions,'campaigns' => $campaigns])!!}
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="{{ route('sunstone.applicant.print-information', $applicant->id) }}" class="btn btn-primary btn-sm">
                                                <i class="material-icons " aria-hidden="true">local_printshop </i>
                                            </a>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal{{ $applicant->id }}">
                                                @if(count($applicant->interviews) < 1)
                                                    Interview <br> Disposition
                                                @else
                                                    Reconsider
                                                @endif
                                            </a>
                                            {!! view('sunstone.applicant.interview-remarks-modal', ['applicant' => $applicant, 'campaigns' => $campaigns]) !!}
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @include('pagination.default', ['paginator' => $applicants])
            @endif
        </div>
    </div>


@endsection

@section('footer-scripts')

@endsection