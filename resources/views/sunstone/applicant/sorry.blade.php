<?php $asset = URL::asset('/'); ?> 
@extends('layouts.applicant')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" style = "margin-top: 80PX; text-align: center;">
                <div class="panel panel-default" style = "padding: 20px;">
                    <h2>{{$email}} is not active in this site. </h2>
                    <img style = 'width: 500px;'src="{{$asset}}milestone/images/sorry.jpg"  >
                </div>
            </div>
        </div>
    </div>


@endsection
