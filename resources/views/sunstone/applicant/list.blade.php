<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

<div class="col-lg-12">
	<div class="card">
		<div class="card-header no-bg b-a-0">Quick View to all Applicants
			<div class="search-applicants-block">
				<form class="form-horizontal form-label-right" method="post" enctype ='multipart/form-data' action="{{route('sunstone.applicant.search')}}" novalidate>
					{{ csrf_field() }}
					<span>SEARCH:</span>
					<input type="text" name="search_key" class="form-control-sm" placeholder="Search key">
					<select class="form-control-sm" name="search_field">
						<option value="name">Name</option>
						<option value="position">Position</option>
					</select>
					<button type="submit" class="btn btn-sm">Submit</button>
				</form>
			</div>
		</div>

			<div class="card-block">
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link " href="{{route('sunstone.applicant.list', ['status' => 'pending'])}}" role="tab" aria-expanded="true">Pending</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="{{route('sunstone.applicant.list', ['status' => 'far-hire'])}}" role="tab" aria-expanded="true">Far-hire</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="{{route('sunstone.applicant.list', ['status' => 'near-hire'])}}" role="tab" aria-expanded="true">Near-hire</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="{{route('sunstone.applicant.list', ['status' => 'ready-hire'])}}" role="tab" aria-expanded="true">Ready-hire</a>
					</li>
					<li class="nav-item ">
						<a class="nav-link " href="{{route('sunstone.applicant.list', ['status' => 'for-training'])}}" role="tab" aria-expanded="true">For-training</a>
					</li>
					<li class="nav-item ">
						<a class="nav-link " href="{{route('sunstone.applicant.list', ['status' => 'all'])}}" role="tab" aria-expanded="true">All</a>
					</li>
				</ul>

				<div class="tab-content ">
					<div class="tab-pane active" id="one" role="tabpanel" >
						@if($applicant->isEmpty())
							<div class="card-header no-bg b-a-0">No record found</div>
						@else
						<table class="table">
						    <thead class="thead-inverse">
						        <tr>
						            <th>Name</th>
						            <th>Position</th>
						            <th>Status</th>
						            <th>Action</th>
						        </tr>
						    </thead>
						    <tbody>

						    	@foreach($applicant as $value)
								        <tr>
								            <td>{{$value->first_name . " " . $value->middle_name . " " .  $value->last_name}}</td>
								            <td>{{!empty($value->position->name) ? $value->position->name : " " }}</td>
								            <td>{{$value->status_name}}</td>
											@if($value->status == 5)
												<td>(Please refer to Training)</td>
											@else
												<td>
													{!!view('sunstone.applicant.view-applicant-modal',['details'=>$value,'positions' => $positions,'campaigns' => $campaign])!!}
												</td>
											@endif
								       	</tr>
							    @endforeach
						       
						    </tbody>
						</table>
						@endif
					</div>
					</div>
				</div>
			</div>

	@include('pagination.default', ['paginator' => $applicants])
		</div>
	</div>


@endsection 

@section('footer-scripts')

@endsection