<?php $asset = URL::asset('/'); ?>
@extends('gem.master2')

@section('title', 'index')

@section('content')

    <div class="card">
        <h4 class="card-header">
            Active Applicants

            <div class="search-applicants-block">
                <form class="form-horizontal form-label-right" method="post" enctype ='multipart/form-data' action="{{route('sunstone.applicant.search')}}" novalidate>
                    {{ csrf_field() }}
                    <span>SEARCH:</span>
                    <input type="text" name="search_key" class="form-control-sm" placeholder="Search key">
                    <select class="form-control-sm" name="search_field">
                        <option value="id">ID</option>
                        <option value="name">Name</option>
                        <option value="position">Position</option>
                    </select>
                    <button type="submit" class="btn btn-sm">Submit</button>
                </form>
            </div>

        </h4>
        <div class="card-block">
            @if($applicants->isEmpty())
                <div class="card-header no-bg b-a-0">No record found</div>
            @else
                <ul class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link active" href="#dept" data-toggle="tab">Department Interview</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="#joboffer" data-toggle="tab">Job Offer</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="dept" role="tabpanel">
                        <table class="table">
                            <thead class="thead-inverse">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($applicants as $applicant)
                                <tr>
                                    @if($applicant->status == 6)
                                        <td>{{ $applicant->id }}</td>
                                        <td>{{ $applicant->first_name }} {{ $applicant->middel_name }} {{ $applicant->last_name }}</td>
                                        <td>{{ $applicant->position->name }}</td>
                                        <td class="row">
                                            <div class="col-sm-4">
                                                {!!view('sunstone.applicant.view-applicant-modal',['details'=>$applicant,'positions' => $positions,'campaigns' => $campaigns])!!}
                                            </div>
                                            <div class="col-sm-1">
                                                <a href="{{ route('sunstone.applicant.print-information', $applicant->id) }}" class="btn btn-primary btn-sm pull-left">
                                                    <i class="material-icons " aria-hidden="true">local_printshop </i>
                                                </a>
                                            </div>
                                            <div class="col-sm-7">
                                                <a href="#" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#pool-modal{{ $applicant->id }}">
                                                    Send back<br>to Pool
                                                </a>
                                                <a href="#" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#jo-modal{{ $applicant->id }}">
                                                    For<br>Job Offer
                                                </a>
                                                {!! view('sunstone.applicant.job-offer-modal', ['applicant' => $applicant, 'campaigns' => $campaigns]) !!}
                                                {!! view('sunstone.applicant.pool-modal', ['applicant' => $applicant, 'campaigns' => $campaigns]) !!}
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="joboffer" role="tabpanel">
                        <table class="table">
                            <thead class="thead-inverse">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($applicants as $applicant)
                                <tr>
                                    @if($applicant->status == 7)
                                        <td>{{ $applicant->id }}</td>
                                        <td>{{ $applicant->first_name }} {{ $applicant->middel_name }} {{ $applicant->last_name }}</td>
                                        <td>{{ $applicant->position->name }}</td>
                                        <td class="row">
                                            <div class="col-sm-4">
                                                {!!view('sunstone.applicant.view-applicant-modal',['details'=>$applicant,'positions' => $positions,'campaigns' => $campaigns])!!}
                                            </div>
                                            <div class="col-sm-1">
                                                <a href="{{ route('sunstone.applicant.print-information', $applicant->id) }}" class="btn btn-primary btn-sm pull-left">
                                                    <i class="material-icons " aria-hidden="true">local_printshop </i>
                                                </a>
                                            </div>
                                            <div class="col-sm-7">
                                                <a href="#" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#pool-modal{{ $applicant->id }}">
                                                    Send back<br>to Pool
                                                </a>
                                                <a href="#" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#jo-signed-modal{{ $applicant->id }}">
                                                    JO<br>Signed
                                                </a>
                                                {!! view('sunstone.applicant.pool-modal', ['applicant' => $applicant, 'campaigns' => $campaigns]) !!}
                                                {!! view('sunstone.applicant.jo-signed-modal', ['applicant' => $applicant, 'campaigns' => $campaigns]) !!}
                    </div>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>


@endsection

@section('footer-scripts')

@endsection