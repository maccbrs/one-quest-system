<div class="modal fade" id="pool-modal{{ $applicant->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Send back to Pool</h4>
            </div>

            <form id="interview{{ $applicant->id }}" method="post" action="{{ route('sunstone.applicant.back-to-pool') }}" enctype="multipart/form-data" validate>
                {{ csrf_field() }}
                <div class="modal-body">
                    @if(count($applicant->interviews) > 0)
                        <div class="row">
                            <div class="col-lg-12">
                                <fieldset class="form-group">
                                    <label>Interview History</label>
                                    <?php $text="" ; foreach ($applicant->interviews as $interview){ $text = $text."[".$interview->type."@".$interview->created_at."] ". $interview->interviewer_name.": ".$interview->remarks."&#013;&#010;"; } ?>
                                    <textarea rows="8" cols="90" name="interview_history" disabled>{{ $text }}</textarea>
                                </fieldset>
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-10">
                            <fieldset class="form-group">
                                <label>New Interview Remarks</label>
                                <textarea rows="8" cols="90" name="interview_remarks" placeholder="Please leave your interview remarks here..." required></textarea>
                            </fieldset>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <fieldset class="form-group">
                                <label>Pool Category</label>
                                <select class="form-control" name="pool_category">
                                    <option value="2">Far-hire</option>
                                    <option value="3">Near-hire</option>
                                    <option value="4">Ready-hire</option>
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="applicant_id" value="{{ $applicant->id }}">
                        <input type="submit" value="Send back to Pool" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>