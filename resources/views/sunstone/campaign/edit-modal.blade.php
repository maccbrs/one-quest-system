<div class="modal fade" id="campaign{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="{{route('sunstone.campaign.edit')}}" novalidate>
                {{ csrf_field() }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Campaign Information</h4>
                </div>
                <div class="modal-body">

                    <div class = "row">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <label for="recipient-name" class="control-label">Campaign Name</label>
                            </div>

                            <div class="col-xs-10">
                                <input type="text" class="form-control disputelabelinput2 "  name = "title"  value="{{ $value->title }}">
                            </div>
                        </div>
                    </div>

                    <div class = "row">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <label for="recipient-name" class="control-label">Alias</label>
                            </div>

                            <div class="col-xs-10">
                                <input type="text" class="form-control disputelabelinput2 "   name = "alias"  value="{{ $value->alias }}">
                            </div>
                        </div>
                    </div>

                    <div class = "row">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <label for="recipient-name" class="control-label">Status</label>
                            </div>

                            <div class="col-xs-10">
                                <select class="form-control disputelabelinput2 " name="status">
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <input type="hidden" name="id" value="{{ $value->id }}">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>