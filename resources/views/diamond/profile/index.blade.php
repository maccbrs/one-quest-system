<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="card profile-bio">
			    <a class="background"></a>
			    <a href="javascript:;" class="avatar" id="profilepic">
			    	
			    </a>
			    <div class="user-details">
			        <div class="user-name">
			        	<a href="javascript:;">Betty Simmons</a>
			        </div>
			        <a href="javascript:;">@ <span>bsimmons</span></a>
			    </div>
			    <div class="user-stats">
			        <ul>
			            <li><a href="javascript:;"><span class="small text-uppercase block text-muted"><strong>Posts</strong></span><h5 class="m-b-0"><strong>64.80</strong></h5></a>
			            </li>
			            <li><a href="javascript:;"><span class="small text-uppercase block text-muted"><strong>Following</strong></span><h5 class="m-b-0"><strong>24.90</strong></h5></a>
			            </li>
			            <li><a href="javascript:;"><span class="small text-uppercase block text-muted"><strong>Followers</strong></span><h5 class="m-b-0"><strong>1.402</strong></h5></a>
			            </li>
			        </ul>
			    </div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">

			<div class="card">
			    <div class="card-header no-bg b-a-0">Labelauty</div>
			    <div class="card-block">
			        <div class="row">
			            <div class="col-xs-6">
			            	<span id="upload-profile">

			            	</span>
			            </div>
			            <div class="col-xs-6">
			                <span id="root">

			                </span>
			            </div>
			            <div class="col-xs-6">
			                <span id="root2">

			                </span>
			            </div>			            
			        </div>
			    </div>
			</div>

		</div>

	</div>




@endsection 

@section('footer-scripts')
<script type="text/javascript">
var socketDiamond = io.connect('http://{{config("app.diamond")}}',{ query: "user=1" });
var defaultData = {
	baseUrl: 'http://{{config("app.diamond")}}',
	imagesUrl: 'http://{{config("app.images")}}',
	userId: '{{Auth::user()->id}}'
};
</script>
 <script src="{{$asset}}diamond/bin/profile.js"></script> 

@endsection