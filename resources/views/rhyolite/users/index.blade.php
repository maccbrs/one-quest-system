<?php $asset = URL::asset('/'); ?> 
@extends('rhyolite.master')

@section('title', 'index')

@section('content')

        <section class="content">

            <div class="row">
                <div class="col-lg-12" style="margin-top:20px;">
                    <div class="panel ">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="ti-layout-grid3"></i> Users
                                <span class="btn btn-primary btn-sm pull-right" style="margin-right:5px;" data-toggle="modal" data-target=".adduser-modal">Add</span>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Allowed</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($items as $i)
                                    <tr>
                                        <td>{{$i->name}}</td>
                                        <td>{{$i->type}}</td>
                                        <td>{{$i->allowed}}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




        </section>

        <div class="modal fade adduser-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body" >
                       
                        <div class="card">
                            <div class="card-block">
                                <form  action="{{route('rhyolite.users.create')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                                <select  name="gem_id" class="form-control" size="1">
                                                    <option value="0">
                                                        Please select
                                                    </option>
                                                    @foreach($users as $u)
                                                    <option value="{{$u->id}}||{{$u->name}}">{{$u->name}}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                    </div>                                                                                                
                                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                                    <div class="clearfix"></div>
                                </form>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

@endsection 

@section('footer-scripts')

@endsection