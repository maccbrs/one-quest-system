<?php $asset = URL::asset('/'); ?> 
@extends('rhyolite.master')

@section('title', 'index')

@section('header-scripts')
    <link rel="stylesheet" type="text/css" href="{{$asset}}timepicker2/jquery.datetimepicker.css"/>
@endsection

@section('content')
        <h4>Generate Call Records (raw data)</h4>
        <section class="content-header form-horizontal">

            <form method="get" action="{{route('rhyolite.calls.index')}}" class="form-horizontal p-10">
                {{ csrf_field() }}            
                <div class="row">
                    <div class="col-md-3">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">From:
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datetimepicker"  placeholder="from" name="from">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-3">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">To:
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datetimepicker"  placeholder="to" name="to">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group p-10">
                            <label class="col-md-3 control-label" for="example-select">Select:</label>
                            <div class="col-md-9">
                                <select name="camplist" class="form-control" size="1">
                                    @foreach($lists as $list)
                                        <option value="{{$list->id}}">{{$list->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group p-10 form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Submit
                                </button>
                            </div>
                        </div>
                    </div>               
                </div>
            </form>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                    <div class="panel">
                        <div class="panel-body">
                                  @if($items)

                                      @if($cached)
                                          <div class="row">
                                              <div class="col-md-12">
                                                  @if($cached)
                                                      <a href="{{route('rhyolite.calls.excel',$cached)}}" class="btn btn-primary pull-right">export</a>
                                                  @endif
                                                  <span class="btn btn-primary pull-right" data-toggle="modal" data-target=".filter-modal">
                                                  <i class="material-icons" aria-hidden="true">Filter</i>
                                                  </span> 
                                              </div>
                                          </div>
                                      @endif  

                                  <table class="table table-bordered table-striped">
                                      <thead>
                                      <tr>
                                          <th>Agent Name</th>
                                          <th>Call Date</th>
                                          <th>Local Number</th>
                                          <th>Remote Number</th>
                                          <th>Campaign (Ingroup Name)</th>
                                          <th>Disposition</th>
                                          <th>Call Direction</th>
                                          <th>Duration (sec)</th>
                                          <th>Queue(ms)</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                          @foreach($items as $item)
                                          <tr>
                                              <td>{{$item->LocalUserId}}</td>
                                              <td>{{$item->ConnectedDate}}</td>
                                              <td>{{$item->DNIS}}</td>
                                              <td>{{$item->RemoteNumber}}</td>
                                              <td>{{$item->AssignedWorkGroup}}</td>
                                              <td>{{($item->wrapup?$item->wrapup->WrapupCode:'-')}}</td>
                                              <td>{{$item->CallDirection}}</td>
                                              <td>{{$item->CallDurationSeconds}}</td>
                                              <td>{{$item->tQueueWait}}</td>
                                          </tr>
                                          @endforeach
                                      </tbody>
                                  </table> 

                                    @if($dispo)
                                    {!!view('rhyolite.calls.filter-modal',['dispo' => $dispo,'id' => $cached])!!}
                                    @endif
                                  @endif
                        </div>
                    </div>
            </div>
        </section>



@endsection 

@section('footer-scripts')
    <script src="{{$asset}}timepicker2/build/jquery.datetimepicker.full.js"></script>
<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z'
});
</script>    
@endsection