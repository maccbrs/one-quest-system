<div class="modal fade filter-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" >
               
                <div class="card">
                    <div class="card-block">

                        <form  action="{{route('rhyolite.calls.filter')}}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="cached_id" value="{{$id}}">
                            <div class="form-group row">

                                @foreach($dispo as $d)
                                <div class="col-sm-6">
                                    <input type="checkbox" name="dispo[]" value="{{$d}}" >
                                    <label>{{$d}}</label> 
                                </div>
                                @endforeach
                            </div>                                                                                                
                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                            <div class="clearfix"></div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>