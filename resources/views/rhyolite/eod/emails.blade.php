<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".mails-modal{{$item->id}}">
<i class="material-icons" aria-hidden="true">emails</i>
</button>

<div class="modal fade mails-modal{{$item->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" >
               
                <div class="card">
                    <div class="card-block">
                        <p style="font-weight:bold; font-size:16px;text-align:center;">Emails</p>
                        <p style="text-align:center;">please use comma(,)</p>
                        <form  action="{{route('rhyolite.eod.update-email',$item->id)}}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label for="Name" class="col-sm-2 form-control-label text-right">From</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="email_from" value="{{$help->parseEmailJsonToString($item->email_from)}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Name" class="col-sm-2 form-control-label text-right">To</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="email_to" value="{{$help->parseEmailJsonToString($item->email_to)}}">
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="Name" class="col-sm-2 form-control-label text-right">CC</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="email_cc" value="{{$help->parseEmailJsonToString($item->email_cc)}}">
                                </div>
                            </div>                                                                                                 
                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                            <div class="clearfix"></div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>