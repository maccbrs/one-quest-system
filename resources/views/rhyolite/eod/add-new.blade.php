<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".add-modal">
<i class="material-icons" aria-hidden="true">add</i>
</button>
<div class="clearfix"></div>
    <div class="modal fade add-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                    <div class="card">
                        <div class="card-header no-bg b-a-0">Add New Set Up</div>
                        <div class="card-block">

                            <form id="form-submit" action="{{route('rhyolite.eod.create')}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="name" >
                                    </div>
                                </div>              

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Send Time</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="stime" class="form-control" name="send_time" >
                                    </div>
                                </div>
<!--                                 <div class="form-group row" >
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Converted Send Time</label>
                                    <div class="col-sm-8">
                                        <input type="datetime" id="ctime" class="form-control" name="converted_sendtime" >
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-4 form-control-label text-right">From</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="from" class="form-control" name="from" >
                                    </div>
                                </div>
<!--                                 <div class="form-group row" hidden>
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Converted From</label>
                                    <div class="col-sm-8">
                                        <input type="datetime" id="cfrom" class="form-control" name="cfrom">
                                    </div>
                                </div>    -->                              
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-4 form-control-label text-right">To</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="to" class="form-control" name="to" >
                                    </div>
                                </div>
<!--                                 <div class="form-group row" hidden>
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Converted To</label>
                                    <div class="col-sm-8">
                                        <input type="datetime" id="cto" class="form-control" name="cto">
                                    </div>
                                </div>  -->                                 
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Timezones</label>
                                    <div class="col-sm-8">
                                        <select name="timezone" class="form-control">
                                            <?php $timezones = mbTimeZones(); sort($timezones); ?>
                                            <option  value="Asia/Manila">Asia/Manila</option>
                                            @foreach($timezones as $tz)
                                                <option  value="{{$tz}}">{{$tz}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>  
                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@section('footer-scripts')

<!-- <script type="text/javascript">
$(document).ready(function(){
$('#datetimepicker1').datetimepicker();
});
</script> -->

@endsection