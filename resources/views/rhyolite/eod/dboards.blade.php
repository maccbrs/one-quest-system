<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".db-modal{{$item->id}}">
<i class="material-icons" aria-hidden="true">DBoards</i>
</button>
<div class="clearfix"></div>
    <div class="modal fade db-modal{{$item->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" >
                   
                    <div class="card">
                        <div class="card-block">

                            <form id="form-submit" action="{{route('rhyolite.eod.update-dboards',$item->id)}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <?php $arr =($item->dummyboard_id?json_decode($item->dummyboard_id,true):[]); ?>
                                    @foreach($boards as $db)
                                    <div class="col-sm-4">
                                        <input type="checkbox" name="dummyboard_id[]" {{(in_array($db->campaign_id,$arr)?'checked':'')}} value="{{$db->campaign_id}}" >
                                        <label>{{substr($db->lob,0,20)}}</label> 
                                    </div>
                                    @endforeach
                                </div>                                                                                                
                                <button type="submit" class="btn btn-primary pull-right">Save</button>
                                <div class="clearfix"></div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>