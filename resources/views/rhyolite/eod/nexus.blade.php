<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".view-modal{{$campaigns->id}}">
<i class="material-icons" aria-hidden="true">Nexus</i>
</button>

<div class="modal fade view-modal{{$item->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" >
               
                <div class="card">
                    <div class="card-block">

                        <form id="form-submit" action="{{route('rhyolite.eod.update-cic',$item->id)}}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <?php $arr =($item->cic_id?json_decode($item->cic_id,true):[]); ?>
                                @foreach($workgroups as $wg)
                                <div class="col-sm-4">
                                    <input type="checkbox" name="cic[]" {{(in_array($wg->WorkGroup,$arr)?'checked':'')}} value="{{$wg->WorkGroup}}" >
                                    <label>{{$wg->WorkGroup}}</label> 
                                </div>
                                @endforeach
                            </div>                                                                                                
                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                            <div class="clearfix"></div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>