<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target=".edit-modal{{$item->id}}">
<i class="material-icons" aria-hidden="true">edit</i>
</button>
<div class="clearfix"></div>
    <div class="modal fade edit-modal{{$item->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body" >
                   
                    <div class="card">
                        <div class="card-block">
                            
                            <form id="form-submit" action="{{route('rhyolite.eod.update',$item->id)}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Send Time</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="estime" class="form-control" name="send_time" >
                                    </div>
                                </div>
                                <!-- <div class="form-group row" >
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Converted Send Time</label>
                                    <div class="col-sm-8">
                                        <input type="datetime" id="ectime" class="form-control" name="converted_sendtime" >
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-4 form-control-label text-right">From</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="efrom" class="form-control" name="from" >
                                    </div>
                                </div>
                                <!-- <div class="form-group row" hidden>
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Converted From</label>
                                    <div class="col-sm-8">
                                        <input type="datetime" id="ecfrom" class="form-control" name="cfrom">
                                    </div>
                                </div>    -->                              
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-4 form-control-label text-right">To</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="eto" class="form-control" name="to" >
                                    </div>
                                </div>
                                <!-- <div class="form-group row" hidden>
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Converted To</label>
                                    <div class="col-sm-8">
                                        <input type="datetime" id="ecto" class="form-control" name="cto">
                                    </div>
                                </div>   -->
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-4 form-control-label text-right">Timezones</label>
                                    <div class="col-sm-8">
                                        <select name="timezone" class="form-control">
                                            @foreach($timezones as $tz)
                                                <option {{($item->timezone == $tz?'selected':'')}} value="{{$tz}}">{{$tz}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-primary pull-right">Save</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

