<?php $asset = URL::asset('/'); ?> 
@extends('rhyolite.master')

@section('title', 'index')

@section('content')

        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            {!!view('rhyolite.eod.add-new')!!}
                            <?php $timezones = mbTimeZones(); ?>
                            <div class="table-responsive">
                                   <table class="table m-b-0">
                                      <thead>
                                         <tr>
                                            <th>Name</th> 
                                            <th>Send Time</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Timezone</th>
                                            <th>Nexus</th>
                                            <th>DBoard</th>
                                            <th>#</th>
                                            <th>#</th>
                                         </tr>
                                      </thead> 
                                      <tbody>
                                         @if($items->count())
                                            @foreach($items as $i)
                                            <tr>
                                               <td>{{$i->name}}</td>
                                               <td>{{$i->send_time}}</td>
                                               <td>{{$i->from}}</td>
                                               <td>{{$i->to}}</td>
                                               <td>{{$i->timezone}}</td>
                                               <td>{!!view('rhyolite.eod.nexus',['item' => $i,'workgroups' => $workgroups])!!}</td>
                                               <td>{!!view('rhyolite.eod.dboards',['item' => $i,'boards' => $boards])!!}</td>
                                               <td>{!!view('rhyolite.eod.emails',['item' => $i,'help' =>$help])!!}</td>
                                               <td>{!!view('rhyolite.eod.edit',['item' => $i,'timezones' => $timezones])!!}</td>
                                            </tr>
                                            @endforeach
                                         @endif
                                      </tbody>
                                   </table> 
                                   {!!$items->links()!!}                               
                            </div>
                        </div>
                    </div>

            </div>




        </section>



@endsection 

@section('footer-scripts')

<!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <script type="text/javascript">
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (day<10 ? '0' : '') + day;

        $(document).ready(function() {
            $('#stime').focusout(function(e) {
                var txtVal = $(this).val();
                $('#ctime').val(output + ' ' + txtVal);
            });
            
            $('#to').focusout(function(e) {
                var txtVal = $(this).val();
                $('#cto').val(output + ' ' + txtVal);
            });

            $('#from').focusout(function(e) {
                var txtVal = $(this).val();
                $('#cfrom').val(output + ' ' + txtVal);
            });

            $('#estime').focusout(function(e) {
                var txtVal = $(this).val();
                $('#ectime').val(output + ' ' + txtVal);
            });
            
            $('#eto').focusout(function(e) {
                var txtVal = $(this).val();
                $('#ecto').val(output + ' ' + txtVal);
            });

            $('#efrom').focusout(function(e) {
                var txtVal = $(this).val();
                $('#ecfrom').val(output + ' ' + txtVal);
            });
        });

    </script> -->
@endsection