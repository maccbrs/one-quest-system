<?php 
$asset = URL::asset('/'); 
$usertype = Auth::user()->user_type; 
$request = Request::instance();

?> 
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="{{$asset}}gemstone/img/favicon.ico"/>
    <link type="text/css" href="{{$asset}}gemstone/css/app.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{$asset}}gemstone/css/custom.css">
    <link rel="stylesheet" href="{{$asset}}gemstone/css/pages/dashboard1.css">
    <link rel="stylesheet" type="text/css" href="{{$asset}}gemstone/vendors/sweetalert2/css/sweetalert2.min.css"/>
    <link rel="stylesheet" href="{{$asset}}sweetalert/dist/sweetalert.css">
          <link rel="stylesheet" href="{{$asset}}jquery-ui/jquery-ui.css">

    <script src="{{$asset}}sweetalert/dist/sweetalert.min.js"></script>
    <style type="text/css">
    .head-span{
            font-size: 24px;
            font-weight: bold;
    }
    </style>
    @yield('header-scripts')
</head>

<body>

<header class="header">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="logo">
            <span class="head-span">Reporter V2</span>
        </a>
        <div>
            <a href="javascript:void(0)" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i
                    class="fa fa-fw fa-bars"></i>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i
                            class="fa  fa-fw fa-bell-o black"></i>
                        <span class="label label-danger">3</span>
                    </a>
                    <ul class="dropdown-menu dropdown-notifications table-striped">
                        <li>
                            <a href="#" class="notification striped-col">
                                <img class="notif-image img-circle" src="{{$asset}}Images/layouts/default.png" alt="avatar-image">
                                <div class="notif-body"><strong>Anderson</strong> shared post from
                                    <strong>Ipsum</strong>.
                                    <br>
                                    <small>Just Now</small>
                                </div>
                                <span class="label label-success label-mini msg-lable">New</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="notification">
                                <img class="notif-image img-circle" src="{{$asset}}Images/layouts/default.png" alt="avatar-image">
                                <div class="notif-body"><strong>Williams</strong> liked <strong>Lorem Ipsum</strong>
                                    typesetting.
                                    <br>
                                    <small>5 minutes ago</small>
                                </div>
                                <span class="label label-success label-mini msg-lable">New</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="notification striped-col">
                                <img class="notif-image img-circle" src="{{$asset}}Images/layouts/default.png" alt="avatar-image">
                                <div class="notif-body">
                                    <strong>Robinson</strong> started follwing <strong>Trac Theme</strong>.
                                    <br>
                                    <small>14/10/2014 1:31 pm</small>
                                </div>
                                <span class="label label-success label-mini msg-lable">New</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="notification">
                                <img class="notif-image img-circle" src="{{$asset}}Images/layouts/default.png" alt="avatar-image">
                                <div class="notif-body">
                                    <strong>Franklin</strong> Liked <strong>Trending Designs</strong> Post.
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-footer"><a href="javascript:void(0)">View All messages</a></li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown-->
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0)" class="dropdown-toggle padding-user" data-toggle="dropdown">
                        <img src="{{$request->query('UserImageSm')}}" class="img-circle img-responsive pull-left" alt="User Image">
                        <div class="riot">
                            <div>
                                <i class="caret"></i>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User name-->
                        <li class="user-name text-center">
                            <span>{{Auth::user()->name}}</span>
                        </li>
                        <!-- Menu Body -->
                        <li class="p-t-3">
                            <a href="{{route('gem.dashboard.index')}}">
                                Milestone<i class="fa fa-fw fa-sign-out pull-right"></i>
                            </a> 
                        </li>
                        <li>
                          <a href="{{ url('/logout') }}">
                            Logout <i class="fa fa-fw fa-sign-out pull-right"></i>
                          </a>                           
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-aside">
        <!-- sidebar: style can be found in sidebar-->
        <section class="sidebar">
            <div id="menu" role="navigation">
                <div class="nav_profile">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="javascript:void(0)">
                            <img src="{{$request->query('UserImageSm')}}" class="img-circle" alt="User Image">
                        </a>
                        <div class="content-profile">
                            <h4 class="media-heading">
                                {{Auth::user()->name}} 
                            </h4>
                            <p>{{($usertype != 'operation') ? ($usertype) : ((Auth::user()->user_level == 1) ? 'Team Leader' : 'Manager')}}</p>
                        </div>
                    </div>
                </div>
                <ul class="navigation">
                
                    <li class="">
                        <a href="{{route('rhyolite.index')}}">
                            <i class="menu-icon fa fa-fw fa-home"></i>
                            <span class="mm-text ">Dashboard 1</span>
                        </a>
                    </li>

                    @if(isAdmin(Auth::user()))
                    <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-archive"></i>
                            <span>Users</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('rhyolite.users.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i>index
                                </a>
                            </li>                                                      
                        </ul>
                    </li>
                    @endif   

                    <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-archive"></i>
                            <span>Calls (raw data)</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('rhyolite.calls.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i>index
                                </a>
                            </li>                                                      
                        </ul>
                    </li>                           

                    <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-archive"></i>
                            <span>EOD</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('rhyolite.eod.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i>index
                                </a>
                            </li>                                                      
                        </ul>
                    </li>
                    <li class="menu-dropdown">
                        <a href="#"> <i class="menu-icon fa fa-fw fa-archive"></i>
                            <span>Productivity</span><span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('rhyolite.productivity.index')}}">
                                    <i class="fa fa-fw fa-list-ol"></i>generate
                                </a>
                            </li>
                            <li>
                                <a href="{{route('rhyolite.productivity.campaigns')}}">
                                    Campaigns
                                </a>
                            </li>                             
                            <li>
                                <a href="{{route('rhyolite.productivity.add-campaign')}}">
                                    <i class="fa fa-fw fa-plus"></i>Add Campaign
                                </a>
                            </li>                                                                                  
                        </ul>
                    </li>

                    


                </ul>
         
            </div>
            <!-- menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
    <aside class="right-aside">
        @include('sweet::alert')
        @yield('content')

        <!-- Content Header (Page header) -->
        
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->
<!-- global js -->
<script src="{{$asset}}gemstone/js/app.js" type="text/javascript"></script>

<script type="text/javascript" src="{{$asset}}gemstone/js/pages/alerts.js"></script>
      <script src="{{$asset}}jquery-ui/jquery-ui.js"></script>
@yield('footer-scripts')
<!-- end of page level js -->
</body>


<!-- Mirrored from demo.lorvent.com/clean/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 13 Apr 2017 09:54:33 GMT -->
</html>
