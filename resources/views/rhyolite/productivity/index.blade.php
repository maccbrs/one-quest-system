<?php $asset = URL::asset('/'); ?> 
@extends('rhyolite.master')

@section('title', 'index')

@section('header-scripts')
    <link rel="stylesheet" type="text/css" href="{{$asset}}timepicker2/jquery.datetimepicker.css"/>
@endsection

@section('content')
        <section class="content-header form-horizontal">
                
            <form method="get" action="{{route('rhyolite.productivity.generate')}}" class="form-horizontal p-10">
                {{ csrf_field() }}            
                <div class="row">
                    <div class="col-md-3">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">From:
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datetimepicker"  placeholder="from" name="from">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-3">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">To:
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datetimepicker"  placeholder="to" name="to">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group p-10">
                            <label class="col-md-3 control-label" for="example-select">Select:</label>
                            <div class="col-md-9">
                                <select name="camplist" class="form-control" size="1">
                                    @foreach($lists as $list)
                                        <option value="{{$list->id}}">{{$list->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group p-10 form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Submit
                                </button>
                            </div>
                        </div>
                    </div>               
                </div>
            </form>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">


                            </div>
                        </div>
                    </div>

            </div>




        </section>



@endsection 

@section('footer-scripts')
    <script src="{{$asset}}timepicker2/build/jquery.datetimepicker.full.js"></script>
<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z'
});
</script>    
@endsection