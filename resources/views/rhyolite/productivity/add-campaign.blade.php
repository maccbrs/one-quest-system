<?php $asset = URL::asset('/'); ?> 
@extends('rhyolite.master')

@section('title', 'index')

@section('content')
        <section class="content-header">
            <h1>Add New Campaign Group on Dropdown</h1>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div>
                                <form method="POST" action="{{route('rhyolite.productivity.create-campaign')}}" class="form-horizontal p-10">
                                    {{ csrf_field() }}
                                    <div class="col-md-12">
                                        <div class="form-group p-10">
                                            <label class="control-label col-md-2" >Name:
                                            </label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" placeholder="Name" name="title">
                                            </div>
                                        </div>

                                        <div class="form-group p-10">
                                            <label class="control-label col-md-2" for="input_Email">Campaigns:</label>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    @foreach($workgroups as $wg)
                                                    <div class="col-sm-6">
                                                        <input type="checkbox" name="camplist[]" value="{{$wg->WorkGroup}}" >
                                                        <label>{{$wg->WorkGroup}}</label> 
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group p-10 form-actions">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary">
                                                    Submit
                                                </button>
                                            </div>
                                        </div>

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

            </div>




        </section>



@endsection 

@section('footer-scripts')

@endsection