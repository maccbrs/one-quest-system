<?php $asset = URL::asset('/'); ?> 
@extends('rhyolite.master')

@section('title', 'index')

@section('header-scripts')

@endsection

@section('content')

<?php 

$array_list2 = array(
			  "header"=> "Update " . "Campaign" , 
			  "form-action"=> route('rhyolite.productivity.delete-campaign'), 
			  "form-list"=>  array("id" => "hidden|ID |id",
								   "action" => "hidden|action |action",
								   
								   
								  )
			  );
			  
?>
        <section class="content-header form-horizontal">

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">

                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">

                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Ingroups</th>
													<th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @if($campaigns->count())
                                                    @foreach($campaigns as $c)
                                                    <tr>
                                                        <td>{{$c->title}}</td>
                                                        <td>
                                                            @if($c->campaigns)
                                                                <?php $ingroups = json_decode($c->campaigns); ?>
                                                                @foreach($ingroups as $ig)
                                                                    {{$ig}} <br>
                                                                @endforeach
                                                            @endif
                                                        </td>
														<td>{!!view('rhyolite.productivity.update-modal',['item' => $c,'workgroups' => $campaigns])!!}| <button type="button" data-toggle="modal" 
											  onclick="console.log('test'); $('.class_id').val('{{$c->id}}');
											  $('.class_action').val('delete')" 
											  
											  
											  data-target=".modalDelete"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>delete</button></td>
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody>
                                            </table>   
                            </div>
                        </div>
                    </div>

            </div>




        </section>

{!!view('delete-modal',['tl_list' => $array_list2])!!}

@endsection 

@section('footer-scripts')

@endsection