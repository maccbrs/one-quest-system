<?php $asset = URL::asset('/'); ?> 
@extends('rhyolite.master')

@section('title', 'index')

@section('header-scripts')
    <link rel="stylesheet" type="text/css" href="{{$asset}}timepicker2/jquery.datetimepicker.css"/>
    <style type="text/css">
        
        tbody {
            display:block;
            height:500px;
            overflow:auto;
        }
        thead, tbody tr {
            display:table;
            width:100%;
            table-layout:fixed;
        }
        thead {
            width: calc( 100% );
        }

    </style>
@endsection

@section('content')
        <section class="content-header form-horizontal">
            <form method="get" action="{{route('rhyolite.productivity.generate')}}" class="form-horizontal p-10">
                {{ csrf_field() }}            
                <div class="row">
                    <div class="col-md-3">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">From:
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datetimepicker"  placeholder="from" name="from">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-3">
                            <div class="form-group p-10">
                                <label class="control-label col-md-2" for="text">To:
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datetimepicker"  placeholder="to" name="to">
                                </div>
                            </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group p-10">
                            <label class="col-md-3 control-label" for="example-select">Select:</label>
                            <div class="col-md-9">
                                <select name="camplist" class="form-control" size="1">
                                    @foreach($lists as $list)
                                        <option value="{{$list->id}}">{{$list->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group p-10 form-actions">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Submit
                                </button>
                            </div>
                        </div>
                    </div>               
                </div>
            </form>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                    @if(!$isEmpty)
                    <div class="panel">
                        
                        <div class="panel-body">
                            <div class="table-responsive">
                                    @if($cached)
                                        <div class="row">
                                            <div class="col-md-12">
                                                @if($export)
                                                    <a href="{{route('rhyolite.productivity.excel',$export)}}" class="btn btn-primary pull-right">export</a>
                                                @endif
                                                <span class="btn btn-primary pull-right" data-toggle="modal" data-target=".filter-modal">
                                                <i class="material-icons" aria-hidden="true">Filter</i>
                                                </span> 
                                            </div>
                                        </div>
                                    @endif   

                                    <ul class="nav nav-pills">
                                        <li class="active">
                                            <a href="#callDetails" data-toggle="tab">Call Details</a>
                                        </li>
                                        <li>
                                            <a href="#inbound" data-toggle="tab">Inbound</a>
                                        </li>
                                        <li>
                                            <a href="#outbound" data-toggle="tab">Outbound</a>
                                        </li>
                                        <li>
                                            <a href="#dailyCon" data-toggle="tab">Daily Connecttime</a>
                                        </li>
                                        <li>
                                            <a href="#dispoCount" data-toggle="tab">Daily CRC</a>
                                        </li> 
                                        <li>
                                            <a href="#dispoCount2" data-toggle="tab">Daily CRC(%)</a>
                                        </li>                                                                                 
                                    </ul>

                                    <div class="tab-content clearfix" >

                                        <div class="tab-pane active" id="callDetails">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Agent Name</th>
                                                    <th>Call Date</th>
                                                    <th>Phone Number</th>
                                                    <th>Campaign</th>
                                                    <th>Disposition</th>
                                                    <th>Duration</th>
                                                    <th>Call Direction</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($calldetails as $cd)
                                                    <tr>
                                                        <td>{{$cd['user']}}</td>
                                                        <td>{{$cd['date']}}</td>
                                                        <td>{{$cd['phone']}}</td>
                                                        <td>{{$cd['campaign']}}</td>
                                                        <td>{{$cd['dispo']}}</td>
                                                        <td>{{$cd['duration']}}</td>
                                                        <td>{{$cd['direction']}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>           
                                        </div>

                                        <div class="tab-pane" id="inbound">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Agent Name</th>
                                                    <th>Call Date</th>
                                                    <th>Phone Number</th>
                                                    <th>Campaign</th>
                                                    <th>Disposition</th>
                                                    <th>Duration</th>
                                                    <th>Call Direction</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($calldetails as $cd)
                                                    @if($cd['direction'] == 'Inbound')
                                                    <tr>
                                                        <td>{{$cd['user']}}</td>
                                                        <td>{{$cd['date']}}</td>
                                                        <td>{{$cd['phone']}}</td>
                                                        <td>{{$cd['campaign']}}</td>
                                                        <td>{{$cd['dispo']}}</td>
                                                        <td>{{$cd['duration']}}</td>
                                                        <td>{{$cd['direction']}}</td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>           
                                        </div>

                                        <div class="tab-pane" id="outbound">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Agent Name</th>
                                                    <th>Call Date</th>
                                                    <th>Phone Number</th>
                                                    <th>Campaign</th>
                                                    <th>Disposition</th>
                                                    <th>Duration</th>
                                                    <th>Call Direction</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($calldetails as $cd)
                                                    @if($cd['direction'] == 'Outbound')
                                                    <tr>
                                                        <td>{{$cd['user']}}</td>
                                                        <td>{{$cd['date']}}</td>
                                                        <td>{{$cd['phone']}}</td>
                                                        <td>{{$cd['campaign']}}</td>
                                                        <td>{{$cd['dispo']}}</td>
                                                        <td>{{$cd['duration']}}</td>
                                                        <td>{{$cd['direction']}}</td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>           
                                        </div>

                                        <div class="tab-pane" id="dailyCon">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Day</th>
                                                    <th>Date</th>
                                                    <th>Total Seconds</th>
                                                    <th>Total Minutes</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($dailyconntime as $dct)
                                                    <tr>
                                                        <td>{{$dct['day']}}</td>
                                                        <td>{{$dct['date']}}</td>
                                                        <td>{{$dct['seconds']}}</td>
                                                        <td>{{gmdate("H:i:s", $dct['seconds'])}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>  
                                        </div>

                                        <div class="tab-pane" id="dispoCount">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>CRC</th>
                                                    @foreach($dailycrc['dates'] as $k => $dcrc)
                                                    <th>{{$k}}</th>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    @foreach($dailycrc['dates'] as $k => $dcrc)
                                                    <th>{{$dcrc}}</th>
                                                    @endforeach
                                                </tr>                                                
                                                </thead>
                                                <tbody>
                                                    @foreach($dailycrc['data'] as $k1 => $v1)
                                                    <tr>
                                                            <td>{{$k1}}</td>
                                                        @foreach($dailycrc['dates'] as $k2 => $v2)
                                                            <td>{{$v1[$k2]['count']}}</td>
                                                        @endforeach                                                        
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>  
                                        </div>

                                         <div class="tab-pane" id="dispoCount2">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>CRC</th>
                                                    @foreach($dailycrc['dates'] as $k => $dcrc)
                                                    <th>{{$k}}</th>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    @foreach($dailycrc['dates'] as $k => $dcrc)
                                                    <th>{{$dcrc}}</th>
                                                    @endforeach
                                                </tr>                                                
                                                </thead>
                                                <tbody>
                                                    @foreach($dailycrc['data'] as $k1 => $v1)
                                                    <tr>
                                                            <td>{{$k1}}</td>
                                                        @foreach($dailycrc['dates'] as $k2 => $v2)
                                                            <td>{{round($v1[$k2]['percent'],2)}}%</td>
                                                        @endforeach                                                        
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>  
                                        </div>
                                                                               
                                    </div>


                            </div>
                        </div>
                    </div>
                    {!!view('rhyolite.productivity.filter-modal',['dispo' => $dispo,'id' => $cached])!!}
                    @endif

            </div>




        </section>



@endsection 

@section('footer-scripts')
    <script src="{{$asset}}timepicker2/build/jquery.datetimepicker.full.js"></script>
<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z'
});
</script>    
@endsection