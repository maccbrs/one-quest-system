 <button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".generate-report">
    Generate
 </button>
 <div class="modal fade generate-report" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <form method="post" action="{{$generateRoute}}" novalidate>
          <div class="modal-content">
            {{ csrf_field() }}                        
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">Date Range</h4>
              </div>
              <div class="modal-body">
                    <form method="get" action="#">
                          <div class="form-group">
                              <input type="text" name="daterange" class="form-control drp" value="" placeholder="Date range picker">
                          </div>  
                          <fieldset class="form-group">
                              <label>Bound</label>
                              <select class="form-control" name="bound">
                                  <option value="in">In</option>
                                  <option value="out">Out</option>
                                  <option value="trans">Transfer</option>
                              </select>
                          </fieldset>                                            
                    </form>    
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Go</button>
              </div>
          </div>
        </form>
    </div>
 </div>    