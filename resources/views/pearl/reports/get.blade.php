<?php $asset = URL::asset('/'); 

$generateRoute = ($campaign?route('pearl.reports.get',$campaign->id):route('pearl.reports.get',$cached->campaign_id));

if($cached):
  $filterRoute = route('pearl.reports.filter',$cached->id);
  $excelRoute = route('pearl.reports.excel',$cached->id);
else:
  $filterRoute = '#';
  $excelRoute = '#';
endif;

//$help->pre($filters);
?> 
@extends('gem.master2')

@section('title', 'reports')

@section('top-navigation')
   <div class="header-inner">
      <div class="navbar-item navbar-spacer-right brand hidden-lg-up">  <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen"><i class="material-icons">menu</i> </a>   <a class="brand-logo hidden-xs-down"><img src="{{$asset}}milestone/images/logo_white.png" alt="logo"> </a> </div>
      
      <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#"><span>Reports</span></a>
     
      <div class="navbar-item nav navbar-nav">

         <div class="nav-item nav-link">
            {!!view('pearl.reports.modal-filter',compact('filterRoute','result','filters'))!!}
            {!!view('pearl.reports.modal-generate',compact('generateRoute'))!!}
         </div>

         <div class="nav-item nav-link dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><span>English</span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="javascript:;">English</a> <a class="dropdown-item" href="javascript:;">Russian</a></div>
         </div>

         <div class="nav-item nav-link dropdown"> 
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">notifications</i> <span class="tag tag-danger">4</span></a>
            <div class="dropdown-menu dropdown-menu-right notifications">
               <div class="dropdown-item">
                  <div class="notifications-wrapper">
                     <ul class="notifications-list">
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-success text-white"><i class="material-icons">arrow_upward</i></div>
                              </div>
                              <div class="notification-message"><b>Sean</b> launched a new application <span class="time">2 seconds ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-danger text-white"><i class="material-icons">check</i></div>
                              </div>
                              <div class="notification-message"><b>Removed calendar</b> from app list <span class="time">4 hours ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-info text-white">J</span></span>
                              <div class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-primary text-white">C</span></span>
                              <div class="notification-message"><b>Conan Johns</b> created a new list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                     </ul>
                  </div>
                  <div class="notification-footer">Notifications</div>
               </div>
            </div>
         </div>
         <a href="javascript:;" class="nav-item nav-link nav-link-icon" data-toggle="modal" data-target=".chat-panel" data-backdrop="false"><i class="material-icons">chat_bubble</i></a>
      </div>                  
   </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if($data)
                <div class="card">
                    <div class="card-block">
                        <p>Reports generated using date range from <code>{{$fr}}</code> to <code>{{$to}}</code>
                            <a href="{{$excelRoute}}"  class="btn btn-success btn-icon btn-sm m-r-xs">
                                <i class="material-icons">file_download </i>
                                <span>excel</span> 
                            </a>
                        </p>

                        <div class="table-responsive scrollit">
                            <table class="table table-bordered customized-table "> 
                                <thead>
                                    <tr>
                                        @foreach($data[0] as $v)
                                        <th>{{$v}}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $k => $items)
                                        @if($k != 0)
                                        <tr>
                                            @foreach($items as $v)
                                            <td>{{$v}}</td>
                                            @endforeach
                                        </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>

@endsection 


@section('header-scripts')
 <link rel="stylesheet" href="{{$asset}}milestone/vendor/bootstrap-daterangepicker/daterangepicker.css">
@endsection

@section('footer-scripts')
<script src="{{$asset}}custom/js/ejs.js"></script>
<script type="text/javascript">
var campaignurl = "{{url('pearl/campaign/')}}";
var socket = io.connect('http://192.168.200.169:8890',{ query: "user=1" });
</script>
<script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>
<script src="{{$asset}}milestone/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="{{$asset}}milestone/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="{{$asset}}milestone/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript">
$('.drp').daterangepicker({
    timePicker: true,
    locale: {
        format: 'MM/DD/YYYY h:mm:ss A'
    }
});
</script>
<script src="{{$asset}}socketio/pearl/dashboard.js"></script> 
@endsection

