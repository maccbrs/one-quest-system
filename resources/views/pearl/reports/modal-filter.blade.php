@if($result)
   <button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".filter-report">
      filter
   </button>
   <div class="modal fade filter-report" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
          <form method="post" action="{{$filterRoute}}" novalidate>
            <div class="modal-content">
              {{ csrf_field() }}                        
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Filter Out</h4>
                </div>
                <div class="modal-body">
                      <form method="get" action="#">

                            @if($filters && $filters['dispo'])
                              <fieldset class="form-group">
                                  <label>Disposition</label>
                                  @foreach($filters['dispo'] as $k => $v)
                                  <label class="checkbox-inline">
                                      <input type="checkbox" name="dispo[]" value="{{$v}}">{{$v}}</label>                                
                                  @endforeach
                              </fieldset> 
                            @endif                                           
                      </form>    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Go</button>
                </div>
            </div>
          </form>
      </div>
   </div>    
 @endif