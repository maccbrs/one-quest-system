<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'dashboard')

@section('top-navigation')
   <div class="header-inner">
      <div class="navbar-item navbar-spacer-right brand hidden-lg-up">  <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen"><i class="material-icons">menu</i> </a>   <a class="brand-logo hidden-xs-down"><img src="{{$asset}}milestone/images/logo_white.png" alt="logo"> </a> </div>
      
      <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#"><span>Reports</span></a>
     
      <div class="navbar-item nav navbar-nav">

         <div class="nav-item nav-link dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><span>English</span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="javascript:;">English</a> <a class="dropdown-item" href="javascript:;">Russian</a></div>
         </div>

         <div class="nav-item nav-link dropdown"> 
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="material-icons">notifications</i> <span class="tag tag-danger">4</span></a>
            <div class="dropdown-menu dropdown-menu-right notifications">
               <div class="dropdown-item">
                  <div class="notifications-wrapper">
                     <ul class="notifications-list">
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-success text-white"><i class="material-icons">arrow_upward</i></div>
                              </div>
                              <div class="notification-message"><b>Sean</b> launched a new application <span class="time">2 seconds ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <div class="notification-icon">
                                 <div class="circle-icon bg-danger text-white"><i class="material-icons">check</i></div>
                              </div>
                              <div class="notification-message"><b>Removed calendar</b> from app list <span class="time">4 hours ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-info text-white">J</span></span>
                              <div class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="notification-icon"><span class="circle-icon bg-primary text-white">C</span></span>
                              <div class="notification-message"><b>Conan Johns</b> created a new list <span class="time">9 days ago</span></div>
                           </a>
                        </li>
                     </ul>
                  </div>
                  <div class="notification-footer">Notifications</div>
               </div>
            </div>
         </div>
         <a href="javascript:;" class="nav-item nav-link nav-link-icon" data-toggle="modal" data-target=".chat-panel" data-backdrop="false"><i class="material-icons">chat_bubble</i></a>
      </div>                  
   </div>
@endsection


@section('content')

   <div class="row">
      <div class="col-md-4">
          <div class="card card-primary card-inverse">
              <div class="card-header no-bg b-a-0">Campaign Info</div>
              <div class="card-block">
                <div class="card-text bold">Name: <span class="highlight content-campaign"></span></div> 
                <div class="card-text bold">Alias: <span class="highlight content-alias"></span></div>
                <div class="card-text bold">Email: <span class="highlight content-email"></span></div> 
                <div class="card-text bold">Contact Name: <span class="highlight content-contact_name">..</span></div>
                <div class="card-text bold">Contact No: <span class="highlight content-contact_no">..</span></div>  
                <div class="card-text bold">Date Created: <span class="highlight content-createdAt"></span></div>           
              </div>
          </div> 

          <div class="card card-success card-inverse">
              <div class="card-header no-bg b-a-0">Technical Info</div>
              <div class="card-block">
                <div class="card-text bold">Account type: <span class="highlight content-account_type"></span></div> 
                <div class="card-text bold">DIDs: <span class="highlight content-dids"></span></div>
                <div class="card-text bold">Transfer prefix: <span class="highlight content-prefix"></span></div>
                <div class="card-text bold">Billing Cycle: <span class="highlight content-billing_cycle"></span></div> 
                <div class="card-text bold">Avg calls daily:<span class="highlight content-daily_calls"></span></div>
                <div class="card-text bold">Avg mins daily<span class="highlight content-daily_mins"></span></div>  
         
              </div>
          </div>

          <div class="card">
              <div class="card-header no-bg b-a-0">Others</div>
              <button type="button" class="btn btn-primary btn-sm btn-block m-b-xs btn-inbound"><span>inbound</span></button>
               <button type="button" class="btn btn-primary btn-sm btn-block m-b-xs btn-outbound"><span>outbound</span></button>
                <button type="button" class="btn btn-primary btn-sm btn-block m-b-xs btn-transfer"><span>transfer</span></button>
              <button type="button" class="btn btn-primary btn-sm btn-block m-b-xs editInfo"><span>edit Campaign Info</span></button>
              <button type="button" class="btn btn-success btn-sm btn-block m-b-xs editTechInfo"><span>edit Tech Info</span></button>
              <a href="#" class="btn btn-primary btn-sm btn-block m-b-xs"><span>Billing Invoices</span></a>
              <a href="{{route('pearl.reports.get',$id)}}" class="btn btn-primary btn-sm btn-block m-b-xs"><span>Report generator</span></a>  
              <button type="button" class="btn btn-primary btn-sm btn-block m-b-xs"><span>Auto Mailers</span></button>  
              <button type="button" class="btn btn-primary btn-sm btn-block m-b-xs"><span>Dummyboards and scripts</span></button>          
          </div> 

      </div>

      <div class="col-md-8">
          <div class="card inbound-tpl">
              <div class="card-header no-bg b-a-0">
                  <h4 >Inbound Calls</h4>
                  <p class="date-coverage"></p>
              </div>
              <div class="card-block">
                  <div class="row">

                      <div class="col-md-6">
                          <table class="table table-bordered customized-table">
                              <tbody>
                                  <tr>
                                      <th>Total Mins</th>
                                      <td class="inbound-totalmin-text">..</td>
                                  </tr>
                                  <tr>
                                      <th>Total Calls</th>
                                      <td class="inbound-totalcalls-text">..</td>
                                  </tr>
                              </tbody>
                          </table>
                          <br>
                          <table class="table table-bordered customized-table">
                              <thead>
                                  <tr>
                                      <th>dispo</th>
                                      <th>count</th>
                                      <th>sec</th>
                                  </tr>
                              </thead>
                              <tbody class="inbound-dispo-append">

                              </tbody>
                          </table>
                      </div>

                      <div class="col-md-6">
                          <table class="table table-bordered customized-table">
                              <thead>
                                  <tr>
                                      <th>date</th>
                                      <th>count</th>
                                  </tr>
                              </thead>
                              <tbody class="inbound-dates-append">

                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>

          <div class="card outbound-tpl">
              <div class="card-header no-bg b-a-0">
                  <h4 >Outbound Calls</h4>
                  <p class="date-coverage"></p>
              </div>
              <div class="card-block">
                  <div class="row">

                      <div class="col-md-6">
                          <table class="table table-bordered customized-table">
                              <tbody>
                                  <tr>
                                      <th>Total Mins</th>
                                      <td class="outbound-totalmin-text">..</td>
                                  </tr>
                                  <tr>
                                      <th>Total Calls</th>
                                      <td class="outbound-totalcalls-text">..</td>
                                  </tr>
                              </tbody>
                          </table>
                          <br>
                          <table class="table table-bordered customized-table">
                              <thead>
                                  <tr>
                                      <th>dispo</th>
                                      <th>count</th>
                                      <th>sec</th>
                                  </tr>
                              </thead>
                              <tbody class="outbound-dispo-append">

                              </tbody>
                          </table>
                      </div>

                      <div class="col-md-6">
                          <table class="table table-bordered customized-table">
                              <thead>
                                  <tr>
                                      <th>date</th>
                                      <th>count</th>
                                  </tr>
                              </thead>
                              <tbody class="outbound-dates-append">

                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>

          <div class="card transfer-tpl">
              <div class="card-header no-bg b-a-0">
                  <h4 >Transfer Calls</h4>
                  <p class="date-coverage"></p>
              </div>
              <div class="card-block">
                  <div class="row">

                      <div class="col-md-6">
                          <table class="table table-bordered customized-table">
                              <tbody>
                                  <tr>
                                      <th>Total Mins</th>
                                      <td class="transfer-totalmin-text">..</td>
                                  </tr>
                                  <tr>
                                      <th>Total Calls</th>
                                      <td class="transfer-totalcalls-text">..</td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>

                      <div class="col-md-6">
                          <table class="table table-bordered customized-table">
                              <thead>
                                  <tr>
                                      <th>date</th>
                                      <th>count</th>
                                  </tr>
                              </thead>
                              <tbody class="transfer-dates-append">

                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>

      </div>         
  </div>


<div class="modal fade addCampaignModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" id="addCampaignModal-container">
               


            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary formdata-submit">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade editInfoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" id="editInfoModal-container">
               
               <div class="card-block">
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Campaign</label>
                       <div class="col-xs-10">
                           <input class="form-control formdata formdata-campaign" type="text" name="campaign" value="">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Alias</label>
                       <div class="col-xs-10">
                           <input class="form-control formdata formdata-alias" type="text" name="alias" value="">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Email</label>
                       <div class="col-xs-10">
                           <input class="form-control formdata formdata-email" type="text" name="email" value="">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Contact Name</label>
                       <div class="col-xs-10">
                           <input class="form-control formdata formdata-contact_name" type="text" name="contact_name" value="">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Contact No</label>
                       <div class="col-xs-10">
                           <input class="form-control formdata formdata-contact_no" type="text" name="contact_no" value="">
                       </div>
                   </div>                                                                                                                                        
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary formdata-submit">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade editTechInfoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" id="editTechInfoModal-container">
               
               <div class="card-block">
                    <form id="techDetail">
                     <div class="form-group row">
                         <label class="col-xs-2 col-form-label">Account Type</label>
                         <div class="col-xs-10">
                                <label class="checkbox-inline"><input type="checkbox" class="account_type" name="account_type" value="inbound">Inbound</label>
                                <label class="checkbox-inline"><input type="checkbox" class="account_type" name="account_type" value="outbound">Onbound</label>
                                <label class="checkbox-inline"><input type="checkbox" class="account_type" name="account_type" value="transfer">Transfer</label>
                         </div>
                     </div>
                     <div class="form-group row">
                         <label class="col-xs-2 col-form-label">DIDs</label>
                         <div class="col-xs-10">
                             <input class="form-control formdata-dids" type="text" name="dids" value="">
                         </div>
                     </div>
                     <div class="form-group row">
                         <label class="col-xs-2 col-form-label">Ingroups</label>
                         <div class="col-xs-10">
                             <input class="form-control formdata-ingroups" type="text" name="ingroups" value="">
                         </div>
                     </div>                     
                     <div class="form-group row">
                         <label class="col-xs-2 col-form-label">Transfer prefix</label>
                         <div class="col-xs-10">
                             <input class="form-control formdata-prefix" type="text" name="prefix" value="">
                         </div>
                     </div>
                     <div class="form-group row">
                         <label class="col-xs-2 col-form-label">Billing Cycle Day</label>
                         <div class="col-xs-10">
                             <input class="form-control formdata-billing_cycle" type="text" name="billing_cycle" value="">
                         </div>
                     </div>
                   </form>                                                                                                                                       
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary tech-formdata-submit">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection 

@section('footer-scripts')
<style type="text/css">
.highlight{
  color: #fff;
  font-size: 16px;
  font-weight: bold;
  margin-left: 10px;
}
.outbound-tpl,.inbound-tpl,.transfer-tpl,.btn-inbound,.btn-outbound,.btn-transfer{
  display: none;
}
</style>

<script src="{{$asset}}custom/js/ejs.js"></script>
<script type="text/javascript">
var socket = io.connect('http://192.168.200.169:8890',{ query: "user=1" });
var cid = "{{$id}}";
</script>
<script src="{{$asset}}socketio/pearl/campaign-get.js"></script> 
@endsection 