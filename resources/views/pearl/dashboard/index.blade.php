<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'dashboard')

@section('content')

   <div class="row">
      <div class="col-md-12">

            <div class="card">
                <div class="card-header no-bg b-a-0">Campaigns</div>
                <div class="card-block">
                    <p><button type="button" class="btn btn-primary btn-sm addCampaign">add</button></p>
                    <div class="table-responsive">
                        <table class="table table-bordered customized-table">
                            <thead>
                                <tr>
                                    <th>Campaign</th>
                                    <th>Alias</th>
                                    <th>Email</th>
                                    <th>Contact Name</th>
                                    <th>Contact No</th>
                                </tr>
                            </thead>
                            <tbody id="campaignTblBody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
   </div>
      </div>


<div class="modal fade addCampaignModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" id="addCampaignModal-container">
               
               <div class="card-block">

                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Campaign</label>
                       <div class="col-xs-10">
                           <input class="form-control formdata" type="text" name="campaign" value="">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Alias</label>
                       <div class="col-xs-10">
                           <input class="form-control formdata" type="text" name="alias" value="">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Email</label>
                       <div class="col-xs-10">
                           <input class="form-control formdata" type="text" name="email" value="">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Contact Name</label>
                       <div class="col-xs-10">
                           <input class="form-control formdata" type="text" name="contact_name" value="">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label class="col-xs-2 col-form-label">Contact No</label>
                       <div class="col-xs-10">
                           <input class="form-control formdata" type="text" name="contact_no" value="">
                       </div>
                   </div>                                       
               </div>

            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary formdata-submit">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script id="td-tpl" type="text/template">
   <%for(var i in row){%>
    <tr class="tbllink" data-link="<%=url%>/<%=row[i].id%>">
        <td><%=row[i].campaign%></td>
        <td><%=row[i].alias%></td>
        <td><%=row[i].email%></td>
        <td><%=row[i].contact_name%></td>
        <td><%=row[i].contact_no%></td>
    </tr>
    <%}%>
</script>



@endsection 

@section('footer-scripts')
<style type="text/css">
.tbllink{
   cursor: pointer;
}
</style>

<script src="{{$asset}}custom/js/ejs.js"></script>
<script type="text/javascript">
var campaignurl = "{{url('pearl/campaign/')}}";
var socket = io.connect('http://192.168.200.169:8890',{ query: "user=1" });
</script>
<script src="{{$asset}}socketio/pearl/dashboard.js"></script> 
@endsection