<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

                  <div class="card">
                     <div class="card-header no-bg b-a-0">Users</div>
                     <div class="card-block">
                        <div class="table-responsive">
                           <table class="table m-b-0">
                              <thead>
                                 <tr>
                                    <th>Name</th> 
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>User Level</th>
                                 </tr>
                              </thead>
                              <tbody>

                                 @foreach($users as $u)
                                 <tr>
                                    <td>{{$u->name}}</td>
                                    <td>{{$u->username}}</td>
                                    <td>{{$u->email}}</td>
                                    <td>{{$u->user_type}}</td>
                                 </tr>
                                 @endforeach

                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>


@endsection 

@section('footer-scripts')

@endsection