<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

                  <div class="card">
                     <div class="card-header no-bg b-a-0">Users
                        {!!view('lilac.campaign.add-modal')!!}
                     </div>
                     <div class="card-block">
                        <div class="table-responsive">
                           <table class="table m-b-0">
                              <thead>
                                 <tr>
                                    <th>Name</th> 
                                    <th>Live Agents</th> 
                                    <th>#</th>
                                 </tr>
                              </thead>
                              <tbody>

                                 @foreach($campaigns as $c)
                                 <tr>
                                    <td>{{$c->campaign_id}}</td>
                                    <td>{{$c->live_agents}}</td>
                                    <td>
                                       {!!view('lilac.campaign.edit-modal',['id' => $c->id,'campaign' => $c, 'help' => $help])!!}
                                    </td>
                                 </tr>
                                 @endforeach

                              </tbody>
                           </table>
                        </div>
                        {!!$campaigns->links()!!}
                     </div>
                  </div>


@endsection 

@section('footer-scripts')

@endsection