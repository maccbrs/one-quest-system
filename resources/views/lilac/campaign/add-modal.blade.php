<button type="button" class="btn btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".modalAddCampaign"></button>
  <div class="modal fade bd-example-modal-lg modalAddCampaign" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <form class="form-horizontal form-label-left" method="post" action="{{route('lilac.campaign.create')}}" novalidate>

           {{ csrf_field() }}
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Campaign</h4>
                </div>

                <div class="modal-body">

                    {!!view('lilac.forms.text',['label' => 'Campaign Name','name' => 'campaign_id','value' => ''])!!}
                   
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>

            </div>

        </form>
      </div>
  </div>
