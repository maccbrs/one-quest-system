<?php $asset = URL::asset('/'); ?> 
@extends('gem.master2')

@section('title', 'index')

@section('content')

                  <div class="card">
                     <div class="card-header no-bg b-a-0">Users
                        {!!view('lilac.user.add-modal',['list' => $campaigns])!!}
                     </div>
                     <div class="card-block">
                        <div class="table-responsive">
                           <table class="table m-b-0">
                              <thead>
                                 <tr>
                                    <th>Name</th> 
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>User Level</th>
                                    <th>Color</th>
                                    <th>#</th>
                                 </tr>
                              </thead>
                              <tbody>


                                 @foreach($users as $u)
                                 <tr>
                                    <td>{{$u->name}}</td>
                                    <td>{{$u->username}}</td>
                                    <td>{{$u->email}}</td>
                                    <td>{{$u->user_type}}</td>
                                    <td>
                                       <span style="background-color:{{$help->get_color($u->options)}};">{{$help->get_color($u->options)}}</span>
                                    </td>
                                    <td>
                                       {!!view('lilac.user.edit-modal',['id' => $u->id,'user' => $u,'help' => $help,'list' => $campaigns])!!}
                                    </td>
                                 </tr>
                                 @endforeach

                              </tbody>
                           </table>
                        </div>
                        {!!$users->links()!!}
                     </div>
                  </div>


@endsection 

@section('footer-scripts')

@endsection