<button type="button" class="btn btn-primary fa fa-group " data-toggle="modal" data-target=".modalEditUser{{$id}}"></button>
  <div class="modal fade bd-example-modal-lg modalEditUser{{$id}}" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <form class="form-horizontal form-label-left" method="post" action="{{route('lilac.user.access_edit',$id)}}" novalidate>

           {{ csrf_field() }}


            <div class="modal-content  ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Edit User Access</h4>
                </div>

                <div class="modal-body">
                    <div class = "access-group">
                        @foreach($access as $a => $value)
                       
                            {!!view('lilac.forms.checkbox',['label' => $value,'name' => $value ,'value' => $value])!!}

                        @endforeach
                    </div>

                    <input type="hidden" name ="id" value ="{{$id}}"></input>
      
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </form>
      </div>
  </div>
