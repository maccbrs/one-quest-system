<button type="button" class="btn btn-primary fa fa-user " data-toggle="modal" data-target=".modalEmpCode{{$details->id}}"></button>

  <div class="modal fade bd-example-modal-lg modalEmpCode{{$details->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
       <form class="form-horizontal form-label-left" method="post" action="{{route('lilac.user.empcode_edit',$details->id)}}" novalidate>
        <div class="modal-content  ">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Employee Code</h4>
            </div>
              
            <div class="modal-body">
                    
              {{ csrf_field() }}

              <input type="hidden" value ="{{$details->gem_id}}" name="id" >
              
              <fieldset class="form-group" style = "margin: 10px;">
                <input type="text" class="form-control"  value ="{{!empty($details->emp_code) ? $details->emp_code : "" }}" name="emp_code" >
              </fieldset>     

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </form>
    </div>
  </div>