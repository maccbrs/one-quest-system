
<a href="{{route('poppy.ticket.open')}}" data-toggle="modal" data-target=".modalmotto{{$id}}"><I>Add/Update your Motto</I> <i class="fa fa-plus"></i></a>
  <div class="modal fade bd-example-modal-lg modalmotto{{$id}}" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <form class="form-horizontal form-label-left" method="post" enctype ='multipart/form-data' action="{{route('garnet.settings.motto')}}" novalidate>

           {{ csrf_field() }}

            <input type="hidden"  name="user_id" value = '{{$id}}'  > 

            <div class="modal-content  ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add/Edit your Motto</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden"  name="user_id" value = '{{$id}}'  >

                    <fieldset class="form-group">
                     
                        <div class="col-lg-12">
                            <label for="exampleSelect1"></label>
                            <input type="text" class="form-control"  name="motto[]"  > 
                        </div>
                 
                    </fieldset>  

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Motto</button>
                </div>
            </div>

        </form>
      </div>
  </div>
