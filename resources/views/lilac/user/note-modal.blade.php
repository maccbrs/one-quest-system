<button type="button" class="btn btn-primary fa fa-edit " data-toggle="modal" data-target=".modalNote{{$id}}"></button>
  <div class="modal fade bd-example-modal-lg modalNote{{$id}}" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
       
            <div class="modal-content  ">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Notes/Credentials </h4>
                </div>
                  
                  @if(!empty($notedata[$id]['header']->subject))
          

                      <form id="validate" class="form-horizontal" role="form" method="POST" action="{{route('garnet.note.update_notes',$id)}}">
                          
                        <div class="modal-body">
                                
                            {{ csrf_field() }}

                            <input type="hidden" value ="2" name="status" > 
                            <input type="hidden" value ="{{$id}}" name="to" >
                            

                            @foreach($notedata[$id]['header']->subject as $ctr => $u)

                            <div class ="noc_notes ">
                            <div class ="noc_notes2"> 

                            <input type="hidden" value ="{{$userId}}" name="user" >

                            <fieldset class="form-group">
                              <label for="exampleInputEmail1">Type</label>
                              <select class="form-control" name="type[]" value = 3>
                                <option value = 1 {{($notedata[$id]["type"]->type[$ctr] == 1)? "selected" : ""}} >Default(Skyblue)</option>
                                <option value = 2 {{($notedata[$id]["type"]->type[$ctr] == 2)? "selected" : ""}}>Common(Blue)</option>
                                <option value = 3 {{($notedata[$id]["type"]->type[$ctr] == 3)? "selected" : ""}}>Informative(Green)</option>
                                <option value = 4 {{($notedata[$id]["type"]->type[$ctr] == 4)? "selected" : ""}}>Sensitive(Yellow)</option>
                                <option value = 5 {{($notedata[$id]["type"]->type[$ctr] == 5)? "selected" : ""}}>Very Important(Red)</option>
                              </select>
                            </fieldset>

                            <fieldset class="form-group">
                              <label >Subject</label>
                              <input type="text" class="form-control" placeholder="Enter Subject" name="subject[]" value= '{{$u}}' > 
                            </fieldset>

                            <fieldset class="form-group">
                              <label >Details</label>
                              <textarea class="form-control" rows="4" name="content[]"  >{{$notedata[$id]['content']->content[$ctr]}}</textarea>
                            </fieldset>

                            <fieldset class="form-group">
                              <label>Department</label>
                              <input type="text" class="form-control" readonly value ="{{$user_type}}" name="user_type[]" >
                            </fieldset>


                            <input type="hidden" value ="{{$notedata[$id]['id']}}" name="id" >
                            
                            <button type="button" class="btn btn-primary btn-sm add-content"><i class="material-icons ">add</i></button> 
                            <button type="button" class="btn btn-danger btn-sm remove-content"><i class="material-icons ">remove</i></button>

                            <br><br>

                            <hr>

                            </div>

                          </div>

                            @endforeach

                          <div class = "appendhere"></div>  
                            
                        </div>


                        <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                      </form>
                   
                      @else

                        <form id="validate" class="form-horizontal" role="form" method="POST" action="{{route('garnet.note.add_notes',$id)}}">
                          <div class="modal-body ">
                            <div class ="noc_notes ">
                              <div class ="noc_notes2 appendhere">   
                                {{ csrf_field() }}

                                <input type="hidden" value ="2" name="status" > 
                                <input type="hidden" value ="{{$id}}" name="to" >
                                <input type="hidden" value ="{{$userId}}" name="user" >
                                 

                               <fieldset class="form-group">
                                  <label for="exampleInputEmail1">Type</label>
                                  <select class="form-control" name="type[]" >
                                    <option value = 1>Default(Skyblue)</option>
                                    <option value = 2>Common(Blue)</option>
                                    <option value = 3>Informative(Green)</option>
                                    <option value = 4>Sensitive(Yellow)</option>
                                    <option value = 5>Very Important(Red)</option>
                                  </select>
                                </fieldset>

                                <fieldset class="form-group">
                                  <label >Subject</label>
                                  <input type="text" class="form-control" placeholder="Enter Subject"  name="subject[]"  > 
                                </fieldset>

                                <fieldset class="form-group">
                                  <label >Details</label>
                                  <textarea class="form-control" rows="4" name="content[]"   ></textarea>
                                </fieldset>

                                <fieldset class="form-group">
                                  <label >Department</label>
                                  <input type="text" class="form-control" readonly value ="{{$user_type}}" name="user_type[]" >
                                </fieldset>

                                <button type="button" class="btn btn-primary btn-sm add-content"><i class="material-icons ">add</i></button> 
                                <button type="button" class="btn btn-danger btn-sm remove-content"><i class="material-icons ">remove</i></button>

                                <br><br>

                                <hr>

                              </div>
                            </div>
                          </div>

                          <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Save changes</button>
                              </div>
                          </div>

                      </form>

                    @endif
                  
                      

                  

      </div>
  </div>
