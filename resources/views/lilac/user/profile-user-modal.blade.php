<button type="button" class="btn btn-success fa fa-group pull-right" data-toggle="modal" data-target=".modalEditUser"></button>
  <div class="modal fade bd-example-modal-lg modalEditUser" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <form class="form-horizontal form-label-left" method="post" enctype ='multipart/form-data' action="{{route('lilac.user.access_create')}}" novalidate>

           {{ csrf_field() }}


            <div class="modal-content  ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Edit User Access</h4>
                </div>

                <div class="modal-body">
                    
                    <fieldset class="form-group">
                        <label for="exampleSelect1">Name</label>
                        <input type="text" class="form-control" placeholder="Name" name="name"> 
                    </fieldset>  

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Email</label>
                        <input type="email" class="form-control" placeholder="Email" name="email"> 
                    </fieldset>  

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Password</label>
                        <input type="text" class="form-control" placeholder="Password" name="password"> 
                    </fieldset>

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Department</label>
                        <select class="form-control" name="user_type">
                            <option value="operation" >Operations</option>
                            <option value="NOC" >NOC</option>
                            <option value="Accounting">Accounting</option>
                            <option value="Human Resources">Human Resources</option>
                            <option value="Quality Assurance">Quality Assurance</option>
                            <option value="Report Analyst">Report Analyst</option>
                            <option value="Administrator">Administrator</option>
                            <option value="Others">Others</option>
                        </select>
                    </fieldset> 

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Access</label>

        

                    </fieldset>

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Select Image </label>
                        <span class="btn btn-success btn-icon fileinput-button m-b-1">
                            <i class="material-icons">add</i> <span>Select files...</span>  
                            <input id="fileupload attachment" type="file" name="avatar" files ='true'  multiple="multiple"> </span>
                    </fieldset> 

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </form>
      </div>
  </div>
