<button type="button" class="btn btn-primary fa fa-search " data-toggle="modal" data-target=".modalPassword{{$details->id}}"></button>
  <div class="modal fade bd-example-modal-lg modalPassword{{$details->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
       
        <div class="modal-content  ">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">View Password of {{$details->name}} </h4>
            </div>
              
            <div class="modal-body">
                    
                {{ csrf_field() }}

                <input type="hidden" value ="2" name="status" > 
                <input type="hidden" value ="{{$details->id}}" name="to" >
                
                <fieldset class="form-group">
                  <label>Password</label>
                  <input type="text" class="form-control" readonly value ="{{!empty($details->pass->password) ? $details->pass->password : "" }}" name="user_type[]" >
                </fieldset>           
            </div>
          </div>
        </div>
      </div>