<button type="button" class="btn btn-primary fa fa-edit pull-right" data-toggle="modal" data-target=".modalEditUser{{$id}}"></button>
  <div class="modal fade bd-example-modal-lg modalEditUser{{$id}}" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <form class="form-horizontal form-label-left" method="post" action="{{route('lilac.user.update',$id)}}" novalidate>

           {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Edit User</h4>
                </div>
                <div class="modal-body">
                    {!!view('lilac.forms.text',['label' => 'Name','name' => 'name','value' => $user->name])!!}

                    {!!view('lilac.forms.text',['label' => 'Email','name' => 'email','value' => $user->email])!!}
                    {!!view('lilac.forms.text',['label' => 'Color','name' => 'color','value' => $help->get_color($user->options)])!!}

                    <fieldset class="form-group">
                        <label for="exampleSelect1">User Level</label>
                        <select class="form-control" name="user_type">
                            <option value="admin" {{($user->user_type == 'admin'?'selected':'')}}>admin</option>
                            <option value="operation" {{($user->user_type == 'operation'?'selected':'')}}>operation</option>
                            <option value="user" {{($user->user_type == 'user'?'selected':'')}} >user</option>
                        </select>
                    </fieldset>   

                    <fieldset class="form-group">
                        <label for="exampleSelect1">Campaign name</label>
                        <select class="form-control" name="campaign_id">
                            @foreach($list as $key => $value)

                                <option value="{{$value->campaign_id}}" >{{$value->campaign_id}}</option>
                        
                            @endforeach
                       </select>
                    </fieldset>                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </form>
      </div>
  </div>
