<?php $asset = URL::asset('/'); ?> 
@extends('amber.master')

@section('title', 'dashboard')

@section('top-navigation')

	<nav class="header navbar">
	  <div class="header-inner">
	    <div class="navbar-item navbar-spacer-right brand hidden-lg-up">

	      <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen">
	        <i class="material-icons">menu</i>
	      </a>

	      <a class="brand-logo hidden-xs-down">
	        <img src="images/logo_white.png" alt="logo">
	      </a> 
	    </div>

	    <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#">
	      <span>
	      	<img src="{{$asset}}milestone/images/website_logo.png" alt="" class="img-rounded" style ="width: 2%;"> Magellan-Link</span>
	    </a>

	  </div>
	</nav>

@endsection

@section('content')

<style>
.announcement-format{
	margin-left: 50px;
	margin-right: 50px;
}
</style>

<div class="content-view">
	<div class="row">
		

		<div class="col-lg-12">
			<div class="card card-block">
				<form id="validate" class="form-horizontal" role="form" method="POST" action="{{route('amber.newsfeed.new_post')}}">

					{{ csrf_field() }}

					<input type="hidden" value ="" name="attachment" >
					<input type="hidden" value ="1" name="status" >
					<input type="hidden" value ="post" name="type" >
					<input type="hidden" value ="" name="tags" >
					<input type="hidden" value ="" name="reactions" >
					<input type="hidden" value ="{{$user_id}}"  name="created_for" >
					<input type="hidden" value ="{{$user_id}}" name="created_by" >


					<div class="panel-body">
	                	<textarea id="editor" class="form-control"  name="caption"></textarea>
	            	</div>
	            	
	            	<button type="submit" class="btn btn-primary pull-right">Post</button>
	            </form>
	        </div>
		</div>
	</div>
</div>



<script>
    window.onload = function() {
    
    $('#myModal').modal('show');
}
</script>
@endsection 

@section('footer-scripts')

@endsection