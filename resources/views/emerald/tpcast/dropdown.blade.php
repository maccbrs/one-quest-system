<?php $asset = URL::asset('/'); ?> 
<?php $recordlist = new AppHelper; $larr_dropdownlist = array();

$larr_dropdownlist2 = $recordlist->fetchTable('emerald' . chr(92) . 'MstDropDown', 'name' , 'SortBy'); ?>
@extends('emerald.master')

@section('title', 'dashboard')

@section('top-navigation')

	

@endsection

@section('content')

<?php
$array_list = array(
			  "modalName" => "CreateNew",
			  "header"=> "Add Drop Down" , 
			  "form-action"=> route('emerald.tpcast.dropdown.create'), 
			  "form-list"=>  array("name" => "text|Name|name",
								   
									"ModelName" =>"hidden|ModelName|ModelName|"
								   
								  ) 
			  );
			  
$array_list2 = array(
			  "modalName" => "EditModal",
			  "header"=> "Update Drop Down" , 
			 
			  "form-action"=> route('emerald.tpcast.dropdown.edit'), 
			  "form-list"=>   array("name" => "text|Name|name",
									 "action" => "hidden|Actions|action",
								   "id" => "hidden|id|id",
									"ModelName" =>"hidden|ModelName|ModelName|"
								   
								  ) 
			  );	


$array_list3 = array(
			  "modalName" => "DeleteModal",
			  "header"=> "Update Drop Down" , 
			 
			  "form-action"=> route('emerald.tpcast.dropdown.edit'), 
			  "form-list"=>   array("name" => "text|Name|name",
									 "action" => "hidden|Actions|action",
								   "id" => "hidden|id|id",
									"ModelName" =>"hidden|ModelName|ModelName|"
								   
								  ) 
			  );			  
			  ?>
{!!view('new-create-modal',['tl_list' => $array_list])!!}
{!!view('new-delete-modal',['tl_list' => $array_list3])!!}
{!!view('new-edit-modal',['tl_list' => $array_list2])!!}
	<div class="row" style="margin-top:10px">
		<div class="col-md-12">
		<h1>Drop Down <button type="button" class="btn btn-sm btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".CreateNew" onclick="$('.class_ModelName').val('MstDropDown');">Add</button></h1>
		<hr/>
		@if (session('return_msg'))
						<div class="alert alert-danger">
							{{ session('return_msg') }}
						</div>
		</div>
	
					@endif
		<div class="col-md-12">
		
						<table id="example" class="display table nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>id</th>
								<th>Name</th>
								<!--<th>Column</th> -->
								<th>Actions</th>
								
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>id</th>
								<th>Descriptions</th>
								<!--<th>Column</th> -->
								<th>Actions</th>
								
							</tr>
								
							
						</tfoot>
						<tbody>
							<?php foreach($larr_dropdownlist2 as $key => $val)
								{
									if ($val->status != "Disabled"){
									echo '<tr>' ;
									echo '<td>' . $val->id . '</td>';
									echo '<td>' . $val->name . '</td>';
									//echo '<td>' . $val->description . '</td>';
									//echo '<td><button value="Update">Update</button>';
									echo '';
									
							?>
											<td><button type="button" data-toggle="modal" 
											  onclick="
											   $('.class_id').val('{{$val->id}}');
											  $('.class_ModelName').val('MstDropDown');
											  $('.class_name').val('{{$val->name}}');
											  $('.class_action').val('edit')"
											 
											  
											  
											  data-target=".EditModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>edit</button>  |
												<button type="button" data-toggle="modal" 
											  onclick="console.log('test');$('.class_name').val('{{$val->id}}');
											  $('.class_id').val('{{$val->id}}');
											  $('.class_ModelName').val('MstDropDown');
											  $('.class_action').val('delete')" 
											  
											  
											  data-target=".DeleteModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>delete</button></td></tr>
							
							<?php
												}
								}?>
						</tbody>
					</table>
		
		
			
		</div>
	</div>



<script>
    window.onload = function() {
    
    $('#myModal').modal('show');
}
</script>
@endsection 

@section('footer-scripts')

@endsection