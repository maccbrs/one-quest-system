<?php $asset = URL::asset('/'); ?> 
<?php $recordlist = new AppHelper; $larr_dropdownlist = array();

$larr_dropdownlist2 = $recordlist->fetchTable('emerald' . chr(92) . 'TpCastForm', 'name' , 'SortBy'); 
$larr_columnfield = $recordlist->fetchTable('emerald' . chr(92) . 'MstDropDownList', 'name' , 'SortBy'); 
$service_type_list ="";
$campaign_list ="" ; 
$issue_type_list = "";
$agent_name_list = "";
$ticket_status_list ="";
$error_code_list = "";


foreach($larr_columnfield as $key => $val)
{
	
	if($val->column =="Campaign"){
	$campaign_list .= $val->name . ":" . $val->name . ",";
	}
	elseif($val->column =="Service Type"){
	$service_type_list .= $val->name . ":" . $val->name . ",";	
	}
	elseif($val->column =="Issue Type"){
	$issue_type_list .= $val->name . ":" . $val->name . ",";	
	}
	elseif($val->column =="Agent Name"){
	$agent_name_list .= $val->name . ":" . $val->name . ",";	
	}
	elseif($val->column =="Ticket Status"){
	$ticket_status_list .= $val->name . ":" . $val->name . ",";	
	}
	elseif($val->column =="Error Code"){
	$error_code_list .=  $val->name . ":" . $val->name . ",";	
	}
	
}

$service_type_list = rtrim($service_type_list,',');
$campaign_list = rtrim($campaign_list,',');
$issue_type_list = rtrim($issue_type_list,',');
$agent_name_list = rtrim($agent_name_list,',');
$ticket_status_list = rtrim($ticket_status_list,',');
$error_code_list = rtrim($error_code_list,',');


?>
@extends('emerald.master')

@section('title', 'dashboard')

@section('top-navigation')

	

@endsection

@section('content')

<?php
$array_list = array(
			  "modalName" => "CreateNew",
			  "header"=> "Add Drop Down" , 
			  "form-action"=> route('emerald.tpcast.dropdown.create'), 
			  "form-list"=>  array("name" => "text|Name|name",
								   
									"ModelName" =>"hidden|ModelName|ModelName|"
								   
								  ) 
			  );
			  
				/* 				"<th>Date</th>
								<th>Campaign</th>
								<th>Service Type</th> 
								<th>N/A</th>
								<th>Customer Name</th>
								<th>Reference ID</th>
								<th>Contact</th> 
								<th>Email Address</th>
								<th>SN</th>
								<th>Case Summary</th>
								<th>Issue Type</th> 
								<th>Error Code</th>
								<th>Status</th>
								<th>Remarks</th>
								<th>Action</th>			  " */
			  
$array_list2 = array(
			  "modalName" => "EditModal",
			  "header" => "Update Drop Down" , 
			 
			  "form-action" => route('emerald.tpcast.form.edit'), 
			  "form-list" =>   array(
									"id" => "hidden|id|id",
									 "action" => "hidden|Actions|action",
								     "date" => "text|Date|date",	
								   "campaign" => "select|Campaign|" . $campaign_list ,
								    "service_type" => "select|Service Type|" . $service_type_list ,
								   "agent_name" => "select|Agent Name|" . $agent_name_list , 
								   "customer_name" => "text|Customer Name|customer_name",
								    "reference_id" => "text|Reference Id|reference_id" ,	
								   "contact" => "text|Contact|contact" ,
						           "email_address" => "text|Email Address|email_address" ,
								   "sn" => "text|SN|sn",
								   "case_summary" => "text|Case Summary|case_summary",
								   "issue_type" => "select|IssueType|" . $issue_type_list ,
								   "error_code" => "select|Error Code|" . $error_code_list , 
								   /**/
								   "ticket_status" => "select|Ticket Status|" . $ticket_status_list ,
								   "remarks" => "text|Remarks|remarks",
									"ModelName" =>"hidden|ModelName|ModelName|"
								   
								  ) 
			  );	


$array_list3 = array(
			  "modalName" => "DeleteModal",
			  "header"=> "Update Drop Down" , 
			 
			  "form-action"=> route('emerald.tpcast.dropdown.edit'), 
			  "form-list"=>   array("name" => "text|Name|name",
									 "action" => "hidden|Actions|action",
								   "id" => "hidden|id|id",
									"ModelName" =>"hidden|ModelName|ModelName|"
								   
								  ) 
			  );			  
			  ?>

{!!view('new-delete-modal',['tl_list' => $array_list3])!!}
{!!view('emerald.tpcast.new-edit-modal',['tl_list' => $array_list2])!!}
	<div class="row" style="margin-top:10px">
		<div class="col-md-12">
		
		@if (session('return_msg'))
						<div class="alert alert-danger">
							{{ session('return_msg') }}
						</div>
		</div>
	
					@endif
		<div class="col-md-12">
		
						<table id="example" class="display table nowrap table-bordered"  cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Date</th>
								<th>Campaign</th>
								<th>Service Type</th> 
								<th>Agent Name</th>
								<th>Customer Name</th>
								<th>Reference ID</th>
								<th>Contact</th> 
								<th>Email Address</th>
								<th>SN</th>
								<th>Case Summary</th>
								<th>Issue Type</th> 
								<th>Error Code</th>
								<th>Status</th>
								<th>Remarks</th>
								<th>Action</th>
								
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Date</th>
								<th>Campaign</th>
								<th>Service Type</th> 
								<th>N/A</th>
								<th>Customer Name</th>
								<th>Reference ID</th>
								<th>Contact</th> 
								<th>Email Address</th>
								<th>SN</th>
								<th>Case Summary</th>
								<th>Issue Type</th> 
								<th>Error Code</th>
								<th>Status</th>
								<th>Remarks</th>
								<th>Action</th>
							</tr>
								
							
						</tfoot>
						<tbody>
								
						
								<?php
								foreach($larr_dropdownlist2 as $key => $val)
								{
									if ($val->status != "Disabled"){
									echo '<tr>' ;
									echo '<td>' . $val->date . '</td>';									
									echo '<td>' . $val->campaign . '</td>';
									echo '<td>' . $val->service_type . '</td>';
									echo '<td>' . $val->agent_name . '</td>';
									echo '<td>' . $val->customer_name . '</td>';
									echo '<td>' . $val->reference_id . '</td>';
									echo '<td>' . $val->contact . '</td>';
									echo '<td>' . $val->email_address . '</td>';
									echo '<td>' . $val->sn . '</td>';
									echo '<td>' . $val->case_summary . '</td>';
									echo '<td>' . $val->issue_type . '</td>';
									echo '<td>' . $val->error_code . '</td>';
									echo '<td>' . $val->ticket_status . '</td>';
									echo '<td>' . $val->remarks . '</td>';
									
									
									echo '';
									
							?>
											<td><button type="button" data-toggle="modal" 
											  onclick="
											  $('#edit-field-service_type').val('{{$val->service_type}}');
											  $('#edit-field-id').val('{{$val->id}}');
											  $('.class_ModelName').val('TpCastForm');
											  $('#edit-field-date').val('{{$val->date}}');
											  $('#edit-field-service_type').val('{{$val->service_type}}');
											  $('#edit-field-service_type').val('{{$val->service_type}}');
											  $('#edit-field-agent_name').val('{{$val->agent_name}}');
											  $('#edit-field-customer_name').val('{{$val->customer_name}}');
											  $('#edit-field-reference_id').val('{{$val->reference_id}}');
											  $('#edit-field-contact').val('{{$val->contact}}');
											  $('#edit-field-email_address').val('{{$val->email_address}}');
											  $('#edit-field-sn').val('{{$val->sn}}');
											  $('#edit-field-case_summary').val('{{$val->case_summary}}');
											  $('#edit-field-issue_type').val('{{$val->issue_type}}');
											  $('#edit-field-error_code').val('{{$val->error_code}}');
											  $('#edit-field-ticket_status').val('{{$val->ticket_status}}');
											  $('#edit-field-remarks').val('{{$val->remarks}}');
											  $('#edit-field-action').val('edit');"
											 
											  
											  
											  data-target=".EditModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>edit</button>  |
												<button type="button" data-toggle="modal" 
											  onclick="console.log('test');$('.class_name').val('{{$val->id}}');
											  $('.class_id').val('{{$val->id}}');
											  $('.class_ModelName').val('TpCastForm');
											  $('.class_action').val('delete')" 
											  
											  
											  data-target=".DeleteModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>delete</button></td></tr>
							
							<?php
												}
								}?>
						</tbody>
					</table>
		
		
			
		</div>
	</div>



<script>
    window.onload = function() {
    
    $('#myModal').modal('show');
}
</script>
@endsection 

@section('footer-scripts')

@endsection