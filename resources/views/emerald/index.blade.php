<?php $asset = URL::asset('/'); ?> 
@extends('emerald.master')

@section('title', 'dashboard')

@section('top-navigation')

	<nav class="header navbar">
	  <div class="header-inner">
	    <div class="navbar-item navbar-spacer-right brand hidden-lg-up">

	      <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen">
	        <i class="material-icons">menu</i>
	      </a>

	      <a class="brand-logo hidden-xs-down">
	        <img src="images/logo_white.png" alt="logo">
	      </a> 
	    </div>

	    <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#">
	      <span>
	      	<img src="{{$asset}}milestone/images/website_logo.png" alt="" class="img-rounded" style ="width: 2%;"> Magellan-Link</span>
	    </a>

	  </div>
	</nav>

@endsection

@section('content')


	<div class="row" style="margin-top:10px">
	
		<div class="col-md-12">
						
						
		
					<div class="row pricing"><div class="col-md-6 col-lg-3"><div class="pricing-plan"><h5>Dev</h5><p class="plan-title text-primary">For testing purposes</p><div class="plan-price text-primary"><span>FREE</span></div><ul class="plan-features"><li>Secure storage</li><li>Limited to 1 user</li><li>Data analytics</li><li class="plan-feature-inactive text-muted">Full search access</li><li class="plan-feature-inactive text-muted">Automatic backups</li></ul><button class="btn btn-primary btn-lg">Choose plan</button></div></div>
					
					
					<div class="col-md-6 col-lg-3"><div class="pricing-plan"><h5>Base</h5><p class="plan-title text-primary">For freelancers</p><div class="plan-price text-primary"><span class="plan-price-symbol">$</span> <span>10</span> <span class="plan-price-period">/ month</span></div><ul class="plan-features"><li>Secure storage</li><li>5 concurrent users</li><li>Data analytics</li><li class="plan-feature-inactive text-muted">Full search access</li><li class="plan-feature-inactive text-muted">Automatic backups</li></ul><button class="btn btn-primary btn-lg">Choose plan</button></div></div>
					
					
					<div class="col-md-6 col-lg-3"><div class="pricing-plan"><h5>Managed</h5><p class="plan-title text-primary">For small businesses</p><div class="plan-price text-primary"><span class="plan-price-symbol">$</span> <span>49</span> <span class="plan-price-period">/ month</span></div><ul class="plan-features"><li>Secure storage</li><li>Unlimited users</li><li>Data analytics</li><li>Full search access</li><li class="plan-feature-inactive text-muted">Automatic backups</li></ul><button class="btn btn-primary btn-lg">Choose plan</button></div></div>
					
					
					<div class="col-md-6 col-lg-3"><div class="pricing-plan"><h5>Enterprise</h5><p class="plan-title text-primary">For large companies</p><div class="plan-price text-primary"><span class="plan-price-symbol">$</span> <span>99</span> <span class="plan-price-period">/ month</span></div><ul class="plan-features"><li>Secure storage</li><li>Unlimited users</li><li>Data analytics</li><li>Full search access</li><li>Automatic backups</li></ul><button class="btn btn-primary btn-lg">Choose plan</button></div></div></div>
		
			
		</div>
	</div>



<script>
    window.onload = function() {
    
    $('#myModal').modal('show');
}
</script>
@endsection 

@section('footer-scripts')

@endsection