
<?php $assets = URL::asset('/');?>
<?php $pic = URL::asset('/uploads/');?>
<?php $Logger = new AppHelper; $larr_myarray = array();$larr_dropdownlist = array();
$larr_dropdownlist = $Logger->fetchTable('emerald' . chr(92) . 'MstDropDown', 'name' , 'SortBy'); 
$Auth = Auth::user()->id;?>
<?php $larr_myarray =  json_decode($Logger->DisplayLog(Auth::user()->id),true); $count = count($larr_myarray);?>
<?php $GemUser = new \App\Http\Models\gem\User;?>
<?php $avatar =  $GemUser->where('id','=',$Auth)->get();

$larr_columnfield = $Logger->fetchTable('emerald' . chr(92) . 'MstDropDownList', 'name' , 'SortBy'); 
$service_type_list ="";
$campaign_list ="" ; 
$issue_type_list = "";
$na_list = "";
$ticket_status_list ="";
$error_code_list = "";


foreach($larr_columnfield as $key => $val)
{
	
	if($val->column =="Campaign"){
	$campaign_list .= $val->name . ":" . $val->name . ",";
	}
	elseif($val->column =="Service Type"){
	$service_type_list .= $val->name . ":" . $val->name . ",";	
	}
	elseif($val->column =="Issue Type"){
	$issue_type_list .= strtolower(str_replace("-"," ",$val->name)) . ":" . $val->name . ",";	
	}
	elseif($val->column =="Agent Name"){
	$na_list .= strtolower(str_replace("-"," ",$val->name)) . ":" . $val->name . ",";	
	}
	elseif($val->column =="Ticket Status"){
	$ticket_status_list .= strtolower(str_replace("-"," ",$val->name)) . ":" . $val->name . ",";	
	}
	elseif($val->column =="Error Code"){
	$error_code_list .= strtolower(str_replace("-"," ",$val->name)) . ":" . $val->name . ",";	
	}
	
}

$service_type_list = rtrim($service_type_list,',');
$campaign_list = rtrim($campaign_list,',');
$issue_type_list = rtrim($issue_type_list,',');
$na_list = rtrim($na_list,',');
$ticket_status_list = rtrim($ticket_status_list,',');
$error_code_list = rtrim($error_code_list,',');


$array_form = array(
			  "modalName" => "CreateNew",
			  "header"=> "Add New Record" , 
			  "form-action"=> route('emerald.tpcast.form.create'), 
			  "form-list"=>  array(
								   "date" => "text|Date|date" ,	
								   "campaign" => "select|Campaign|" . $campaign_list ,
								   "service_type" => "select|Service Type|" . $service_type_list ,
								   "agent_name" => "select|Agent Name|" . $na_list ,
								   "customer_name" => "text|Customer Name|customer_name",
								   "reference_id" => "text|Reference Id|reference_id" ,	
								   "contact" => "text|Contact|contact" ,
						           "email_address" => "text|Email Address|email_address" ,
								   "sn" => "text|SN|sn",
								   "case_summary" => "text|Case Summary|case_summary",
								   "issue_type" => "select|IssueType|" . $issue_type_list ,
								   "error_code" => "select|Error Code|" . $error_code_list ,
								   
								   "ticket_status" => "select|Service Type|" . $ticket_status_list ,
								   "remarks" => "text|Remarks|remarks",
								   "ModelName" =>"hidden|ModelName|ModelName|"
								   
								  ) 
			  );
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @yield('header-css')
  <link rel="stylesheet" href="{{$assets}}inventory/css/bootstrap.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/css/buttons.dataTables.css">
   <link rel="stylesheet" href="{{$assets}}css/emerald.css">
   
   <script src="{{$assets}}inventory/js/jquery-3.2.1.js"></script>
      <script src="{{$assets}}inventory/datatables/jquery-ui.js"></script>
      <script src="{{$assets}}inventory/datatables/bootstrap.min.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
      <script src="{{$assets}}inventory/datatables/jquery.dataTables.yadcf.js"></script>

      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
      <script src="{{$assets}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>
	   <script src="{{$assets}}js/emerald.js"></script>
  

  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 667px;}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">VR </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
	  
        <li><a href="{{route('bloodstone.home')}}">Dashboard</a></li>
		<li><a href="{{route('emerald.tpcast.form')}}">Forms</a></li>
        
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Master Files
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
			<?php foreach($larr_dropdownlist as $key => $val)
			{	if($val->status != 'Disabled')
				{echo '<li><a href="' . $assets . 'emerald/tpcast/dropdownlist/' . $val->name. '">' . $val->name . '</a></li>';
				}
				
			}?>
			
		  
        </ul>
      </li>
		<li><a href="{{route('emerald.tpcast.dropdown')}}">Add DropDown</a></li>
	 
       
		
			     
      </ul>
      <ul class="nav navbar-nav navbar-right">
	  
			
			      <li ><a class="btn btn-info" data-toggle="modal" data-target=".FormCreateNew" onclick="$('.class_ModelName').val('TpCastForm');" style="color:#ffffff">Add</a> </li>
							
				@if(!empty($Logger->DisplayLog(Auth::user()->id)))

			  <li class="dropdown">
			  
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Logs <span class="label label-danger">{{$count}}</span><span class="caret"></span></a>
				
				
				<ul class="dropdown-menu" style="width:300px;font-size:10px;">
					
					@foreach($larr_myarray as $logkey => $logval)
					
									<li><div style='padding:10px;'>{{$logval['description'] . $logval['created_at']}}</div></li><hr style='margin-top:5px;margin-bottom:5px'/>
                    @endforeach
									<li><div style='padding:10px;'>View All</div></li><hr style='margin-top:5px;margin-bottom:5px'/>
                    @endif
                      
				</ul>
			  </li>
        <li><a href={{route('gem.dashboard.index')}}><span class="glyphicon glyphicon-home"></span> Milestone</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  
  
   
    	@yield('content')
    
 
</div>




{!!view('emerald.tpcast.new-create-modal',['tl_list' => $array_form])!!}

<!-- <footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer> -->

</body>
      

@yield('footer-scripts')

</html>