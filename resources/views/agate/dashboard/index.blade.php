<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                          {!! Breadcrumbs::render('home') !!} 
                    </h1>
                </div>
            </div>
        </div>
     </header>

@endsection 

@section('footer-scripts')

@endsection