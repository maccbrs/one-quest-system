<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
    
    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.metrics.keyfactors') !!}
                    </h1>
                </div>
            </div>
        </div>
     </header>

    <div class="widget widget-default">
        <header class="widget-header">
        Key Factors
        </header>
        <div class="widget-body">
        	<button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#add-keyfactors" data-placement="top"><span class="fa fa-fw fa-plus"></span></button>
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($keyfactors as $keyfactor)
                    <tr>
                        <td>{{$keyfactor->title}}</td>
                        <td>{{$keyfactor->description}}</td>
                        <td class="text-right">
                            <a href="{{route('agate.metrics.edit-keyfactor',$keyfactor->id)}}" class="btn btn-transparent btn-xs">
                                <span class="fa fa-pencil"></span> 
                                <span class="hidden-xs hidden-sm hidden-md">Edit</span>
                            </a> 
                            <span class="fa fa-trash btn btn-transparent btn-transparent-danger btn-xs" data-toggle="modal" data-target="#delete-keyfactor{{$keyfactor->id}}"></span>
                            {!!view('agate.metrics.modal-delete-keyfactor',compact('keyfactor'))!!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$keyfactors->links()}}
        </div>
    </div>

    <div class="modal fade" id="add-keyfactors" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{route('agate.metrics.keyfactor')}}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title custom_align">Add new Key Factor</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input class="form-control " type="text" placeholder="Title" name="title">
                        </div>
                        <div class="form-group">
                            <input class="form-control " type="text" placeholder="Description" name="description">
                        </div>                                
                    </div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok-sign"></span> Save
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection 

@section('footer-scripts')

@endsection