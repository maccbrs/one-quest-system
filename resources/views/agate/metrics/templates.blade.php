<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
    
    {!! Breadcrumbs::render('agate.metrics.templates') !!}

        <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.metrics.parameters') !!}
                    </h1>
                </div>
            </div>
        </div>
     </header>


        <div class="widget widget-default">
            <header class="widget-header">
               Templates
            </header>
            <div class="widget-body">
            	<button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#add-template" data-placement="top"><span class="fa fa-fw fa-plus"></span></button>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($templates as $template)
                        <tr>
                            <td>{{$template->title}}</td>
                            <td class="text-right">
                                <a href="{{route('agate.metrics.edit-template',$template->id)}}" class="btn btn-transparent btn-xs">
                                    <span class="fa fa-pencil"></span> 
                                    <span class="hidden-xs hidden-sm hidden-md">Edit</span>
                                </a> 
                                <span class="fa fa-trash btn btn-transparent btn-transparent-danger btn-xs" data-toggle="modal" data-target="#delete-template{{$template->id}}"></span>
                                {!!view('agate.metrics.modal-delete-template',compact('template'))!!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$templates->links()}}
            </div>
        </div>

        <div class="modal fade" id="add-template" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="{{route('agate.metrics.template')}}">
                        {{ csrf_field() }}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title custom_align">Add new Template</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input class="form-control " type="text" placeholder="Title" name="title">
                            </div>                              
                        </div>
                        <div class="modal-footer ">
                            <button type="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-ok-sign"></span> Save
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
@endsection 

@section('footer-scripts')

@endsection