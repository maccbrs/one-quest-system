<div class="modal fade" id="delete-parameter{{$parameter->id}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="{{route('agate.metrics.delete-parameter',['parameterid' => $parameter->id])}}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title custom_align">Are you sure you want to delete {{$parameter->title}}?</h4>
                </div>
                <input class="form-control" name="item" type="hidden" value="">
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-sign"></span> Delete
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>