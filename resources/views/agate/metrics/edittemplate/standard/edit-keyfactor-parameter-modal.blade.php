
    <div class="modal fade" id="edit-keyfactor-parameter{{$parameter->id}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{route('agate.metrics.template-edit-keyfactor-parameter',['templateid' => $template->id,'keyfactorid' => $kf->id,'parameterid' => $parameter->id])}}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title custom_align">Edit {{$parameter->title}}</h4>
                    </div>
                    <div class="modal-body"> 
                        
                        @if(isset($kf->scoringtype)&&$kf->scoringtype == 'standard')
                            <p>Points <span class="points">{{$parameter->points}}</span></p>
                            @foreach($parameter->items as $k => $item)
                                    <?php 
                                        $subpoint = false;
                                        if(isset($parameter->sub_points)):
                                            if(is_array($parameter->sub_points))
                                            $subpoint = $parameter->sub_points[$k]->point;
                                        endif;
                                     ?>
                                    <p class="mb-input"> -<input class="form-control" name="point_{{$k}}" type="text" value="{{($subpoint?$subpoint:'')}}"> {{$item}}:</p> 
                            @endforeach
                        @else
                            <p>Points <input type="text" class="form-control" name="points" value="{{$parameter->points}}"></p>
                        @endif                                                        
                                                                                                   
                    </div>                                                         
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok-sign"></span> Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>      