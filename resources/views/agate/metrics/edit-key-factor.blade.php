<?php $asset = URL::asset('/'); $except = []; ?> 
@extends('agate.master')

@section('title', 'index')

@section('header-scripts')
<style type="text/css">
    .edit-key-factor tr.pad-left td{
        padding-left: 30px;
    }
</style>
@endsection

@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.metrics.template-edit-keyfactor') !!}
                    </h1>
                </div>
            </div>
        </div>
     </header>
  
    <div class="widget widget-default edit-key-factor">
        <header class="widget-header">
            Edit {{$keyfactor->title}}
            <button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#add-parameter" data-placement="top"><span class="fa fa-fw fa-plus"></span></button>                    
        </header>
        <div class="widget-body">


            <div class="panel panel-default">
                <div class="panel-heading">{{$keyfactor->title}}
                    <button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#update-keyfactor" data-placement="top"><span class="fa fa-fw fa-pencil"></span></button> 
                

                </div>
                <div class="panel-body">
                    <p><mark>Description</mark>{{$keyfactor->description}}</p>
                </div>

                <table class="table">
                    <tbody>
                        @if($keyfactor->parameters)
                            @foreach(json_decode($keyfactor->parameters) as $parameter)
                                <tr>
                                    <td>
                                        {{$parameter->title}}
                                        <span class="fa fa-trash btn btn-transparent btn-transparent-danger btn-xs" data-toggle="modal" data-target="#delete-keyfactor-parameter{{$keyfactor->id}}{{$parameter->id}}"></span>
                                        {!!view('agate.metrics.modal-delete-keyfactor-parameter',['keyfactorid' => $keyfactor->id,'parameterid' => $parameter->id,'title' => $parameter->title])!!}
                                    </td>
                                </tr>
                                @if($parameter->items)
                                    @foreach($parameter->items as $item)
                                    <tr class="pad-left">
                                        <td>
                                           - {{$item}}
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    </tbody>
                </table>

            </div>

        </div>
    </div>

    <div class="modal fade" id="update-keyfactor" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{route('agate.metrics.update-keyfactor',$keyfactor->id)}}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title custom_align">Update Key Factor</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-group">
                                <input class="form-control " type="text" value="{{$keyfactor->title}}" name="title">
                            </div> 
                        </div> 
                        <div class="form-group">
                            <div class="form-group">
                                <input class="form-control " type="text" value="{{$keyfactor->description}}" name="description">
                            </div> 
                        </div>                                                        
                    </div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok-sign"></span> Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-parameter" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{route('metrics.keyfactor-add-parameter',['id' => $keyfactor->id])}}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title custom_align">Add New Parameter to {{$keyfactor->title}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select name="parameterid" class="form-control input-sm">
                                @foreach($parameters as $parameter)
                                    <option value="{{$parameter->id}}">{{$parameter->title}}</option>
                                @endforeach
                            </select>
                        </div>                              
                    </div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok-sign"></span> Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection 

@section('footer-scripts')

@endsection