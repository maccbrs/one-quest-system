<div class="modal fade" id="delete-template-keyfactor{{$templateid}}{{$keyfactorid}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="{{route('agate.metrics.delete-template-keyfactor',['keyfactorid' => $keyfactorid,'templateid' => $templateid])}}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title align-c">Are you sure you want to delete {{$title}}?</h4>
                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-sign"></span> Delete
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>