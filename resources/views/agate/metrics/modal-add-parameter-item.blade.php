<div class="modal fade" id="add-parameter-item{{$parameter->id}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="{{route('agate.metrics.parameter-item',['parameterid' => $parameter->id])}}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title custom_align">Add Item to {{$parameter->title}}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input class="form-control" name="item" type="text" >
                    </div>                              
                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok-sign"></span> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>