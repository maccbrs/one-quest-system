<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')
@section('header-scripts')
<style type="text/css">
    .parameters mark{
        margin-right: 10px;
    }
    .parameters .add-parameter-item,.parameters .delete-parameter,.parameters .edit-parameter-item{
        margin-left: 10px;
        cursor: pointer;
    }
    .parameters .pad-left td{
        padding-left: 30px;
    }
</style>
@endsection
@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">

                        {!! Breadcrumbs::render('agate.metrics.parameters') !!}

                    </h1>
                </div>
            </div>
        </div>
     </header>

    <div class="widget widget-default parameters">
        <header class="widget-header">
            Parameters 
        </header>
        <div class="widget-body">
        	<button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#add-parameter" data-placement="top"><span class="fa fa-fw fa-plus"></span></button>
            <table class="table table-bordered ">
                <tbody>
                    @foreach($parameters as $parameter)
                    <tr>
                        <td>
                            {{$parameter->title}} 
                            <span class="fa fa-edit add-parameter-item" data-toggle="modal" data-target="#add-parameter-item{{$parameter->id}}"></span>
                            <span class="fa fa-trash delete-parameter" data-toggle="modal" data-target="#delete-parameter{{$parameter->id}}"></span>
                        </td>
                    </tr>  
                        @if($parameter->items)
                            <?php $items = json_decode($parameter->items); ?>
                            @foreach($items as $k => $item)
                            <tr class="pad-left">
                                <td>- <span class="fa fa-edit edit-parameter-item" data-toggle="modal" data-target="#edit-parameter-item{{$parameter->id}}{{$k}}"></span>  
                                    <span class="fa fa-trash delete-parameter-item" data-toggle="modal" data-target="#delete-parameter-item{{$parameter->id}}{{$k}}"></span>  &nbsp; {{$item}} </td>

                            </tr>
                            <div class="modal fade" id="edit-parameter-item{{$parameter->id}}{{$k}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="POST" action="{{route('agate.metrics.parameter-item-edit',['parameterid' => $parameter->id,'item' => $item])}}">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title custom_align">Edit " {{$item}} "</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <input class="form-control" name="item" type="text" value="{{$item}}">
                                                </div>                              
                                            </div>
                                            <div class="modal-footer ">
                                                <button type="submit" class="btn btn-success">
                                                    <span class="glyphicon glyphicon-ok-sign"></span> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>    

                            <div class="modal fade" id="delete-parameter-item{{$parameter->id}}{{$k}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="POST" action="{{route('agate.metrics.parameter-item-delete',['parameterid' => $parameter->id,'item' => $item])}}">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title custom_align">Delete Item " {{$item}} "</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <input class="form-control" name="item" type="text" value="{{$item}}">
                                                </div>                              
                                            </div>
                                            <div class="modal-footer ">
                                                <button type="submit" class="btn btn-warning">
                                                    <span class="glyphicon glyphicon-ok-sign"></span> Delete
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>                       
                            @endforeach
                        @endif 
                        {!!view('agate.metrics.modal-add-parameter-item',compact('parameter'))!!}
                        {!!view('agate.metrics.modal-delete-parameter',compact('parameter'))!!}                           
                    @endforeach
                </tbody>
            </table>
            {{$parameters->links()}}
        </div>
    </div>

    <div class="modal fade" id="add-parameter" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{route('agate.metrics.parameter')}}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title custom_align">Add new Parameter</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input class="form-control " type="text" placeholder="Title" name="title">
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok-sign"></span> Save
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection 

@section('footer-scripts')

@endsection 