<?php $asset = URL::asset('/'); $except = []; ?> 
@extends('agate.master')

@section('title', 'index')

@section('header-scripts')
<style type="text/css">
    .template-edit mark{
        margin-right: 10px;
    }

    .template-edit .edit-keyfactor{
        margin-left: 10px;
        cursor: pointer;
    }
    .template-edit .pad-left td{
        padding-left: 30px;
    }
    .mb-input{
        padding-left: 40px;
    }
    .mb-input input{
        display: inline;
        width: 50px;
        margin-right: 10px; 
        margin-left: 5px;       
    }
    .points{
        font-size: 20px;
        font-weight: bold;
        padding: 10px;
    } 
</style>
@endsection
     
@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.metrics.edit-template',$template) !!}
                    </h1>
                </div>
            </div>
        </div>
     </header>
    
    <div class="widget widget-default template-edit">
        <header class="widget-header">
            {{$template->title}}
            <button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#add-keyfactor" data-placement="top"><span class="fa fa-fw fa-plus"></span></button>                    
			<button class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#add-keyfactor" data-placement="top">Header</button>                    
        </header>

        <div class="widget-body">
            @if($template->keyfactors)
                <?php $keyfactors_arr = json_decode($template->keyfactors); ?>
                @foreach($keyfactors_arr as $kf)

                    <?php if(!isset($kf->scoringtype)) $kf->scoringtype = 'standard'; ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{$kf->title}} 
                            <span class="fa fa-edit btn btn-transparent btn-transparent-danger btn-xs" data-toggle="modal" data-target="#edit-keyfactor{{$kf->id}}"></span>
                            <span class="fa fa-trash btn btn-transparent btn-transparent-danger btn-xs" data-toggle="modal" data-target="#delete-template-keyfactor{{$template->id}}{{$kf->id}}"></span>
                            {!!view('agate.metrics.modal-delete-template-keyfactor',['templateid' => $template->id,'keyfactorid' => $kf->id,'title' =>$kf->title])!!}                                    
                        </div>
                        <div class="panel-body">

                            <p><mark>Description</mark>{{$kf->description}}</p>
                            <p><mark>Points</mark>{{($kf->scoringtype != 'autozero'?$kf->points:'-')}}</p>
                            <p><mark>Scoring Type</mark>{{$kf->scoringtype}}</p>
                        </div>

                        <table class="table">  
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Points</th> 
                                    <th class="text-right">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($kf->parameters) 
                                @foreach($kf->parameters as $parameter) 
                                <tr>
                                    <td>{{$parameter->title}}</td>
                                    <td>{{($kf->scoringtype =='autozero'?'-':$parameter->points)}}</td>
                                    <td>
                                        @if($kf->scoringtype != 'autozero')
                                        <span class="fa fa-edit edit-keyfactor" data-toggle="modal" data-target="#edit-keyfactor-parameter{{$parameter->id}}"></span>
                                        @endif
                                    </td>
                                </tr>

                                    @foreach($parameter->items as $k => $item)
                                        <?php 
                                            $subpoint = false;
                                           // pre($item);
                                            if(isset($parameter->sub_points) && $kf->scoringtype != 'autozero'):
                                                
                                                if(is_array($parameter->sub_points) && $parameter->sub_points)
                                                $subpoint = $parameter->sub_points[$k]->point;

                                            endif;
                                         ?>                                    
                                        <tr class="pad-left">
                                            <td>- {{$item}}</td>
                                            <td>-{{($subpoint?$subpoint:0)}}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach 

                                    {!!view('agate.metrics.edittemplate.standard.edit-keyfactor-parameter-modal',compact('parameter','template','kf'))!!}                                         
                                @endforeach  

                                @endif                                        
                            </tbody> 
                        </table>                                 

                    </div> 

                    <div class="modal fade" id="edit-keyfactor{{$kf->id}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form method="POST" action="{{route('agate.metrics.template-edit-keyfactor',['templateid' => $template->id,'keyfactorid' => $kf->id])}}">
                                    {{ csrf_field() }}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title custom_align">Edit {{$kf->title}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <input class="form-control" name="description" type="text" value="{{$kf->description}}">
                                        </div> 
                                        <div class="form-group">
                                            <label>Scoring Type</label>
                                            <select class="form-control" name="scoringtype">
                                                <option value="standard" > Standard</option>
                                                <option value="category" {{($kf->scoringtype == 'category'?'selected':'')}}>Category</option>
                                                <option value="autozero" {{($kf->scoringtype == 'autozero'?'selected':'')}}> Auto Zero</option>
                                            </select>
                                        </div>                                                                                                                                 

                                    </div>
                                    <div class="modal-footer ">
                                        <button type="submit" class="btn btn-success">
                                            <span class="glyphicon glyphicon-ok-sign"></span> Save
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div> 



                @endforeach
            @endif
        </div>
    </div>

    <div class="modal fade" id="add-keyfactor" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{route('agate.metrics.template-add-key-factor',$template->id)}}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title custom_align">Add new Key Factor to {{$template->title}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select name="keyfactorid" class="form-control input-sm">
                                @foreach($keyfactors as $keyfactor)
                                    <option value="{{$keyfactor->id}}">{{$keyfactor->title}}</option>
                                @endforeach
                            </select>
                        </div>                              
                    </div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok-sign"></span> Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection 

@section('footer-scripts')

@endsection