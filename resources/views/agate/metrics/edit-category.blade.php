<?php $asset = URL::asset('/'); $except = []; ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
            <div class="widget widget-default">
                <header class="widget-header">
                    Categories
                    <button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#add-category" data-placement="top"><span class="fa fa-fw fa-plus"></span></button>                    
                </header>
                <div class="widget-body">


                    <div class="panel panel-default">
                        <div class="panel-heading">{{$category->title}}
                            <button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#update-category" data-placement="top"><span class="fa fa-fw fa-pencil"></span></button>
                        </div>
                        <div class="panel-body">
                            <p><mark>Description</mark>{{$category->description}}</p>
                            <p><mark>Scoring Type</mark>{{$category->type}}</p>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Points</th>
                                    <th class="text-right">#</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php  
                            if($category->items):
                                $catItems = json_decode($category->items);
                                $count = 0;
                            ?>
                            @foreach($catItems as $catItem)
                                <?php $except[] = $catItem->title; ?>                                
                                <tr>
                                    <td>{{$catItem->title}}</td>
                                    <td>{{(isset($catItem->points)?$catItem->points:'')}}</td>
                                    <td>
                                        <button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#edit-categoryItem{{$count}}" data-placement="top"><span class="fa fa-fw fa-pencil"></span></button>   
                                        <div class="modal fade" id="edit-categoryItem{{$count}}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form method="POST" action="{{route('agate.metrics.edit-categoryitem',['categoryId' => $category->id,'itemTitle' => $catItem->title ])}}">
                                                        {{ csrf_field() }}
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h4 class="modal-title custom_align">Edit "{{$catItem->title}}"</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" value="{{(isset($catItem->points)?$catItem->points:'')}}" name="points">
                                                            </div>                              
                                                        </div>
                                                        <div class="modal-footer ">
                                                            <button type="submit" class="btn btn-success">
                                                                <span class="glyphicon glyphicon-ok-sign"></span> Save
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>                                    
                                    </td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach
                        <?php endif; ?>                                
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>

            <div class="modal fade" id="add-category" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="POST" action="{{route('agate.metrics.category-item',$category->id)}}">
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title custom_align">Add new item to this category</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <select name="title" class="form-control input-sm">
                                        @foreach($items as $item)
                                            @if(!in_array($item->title,$except))
                                                <option value="{{$item->title}}" >{{$item->title}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>                              
                            </div>
                            <div class="modal-footer ">
                                <button type="submit" class="btn btn-success">
                                    <span class="glyphicon glyphicon-ok-sign"></span> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="update-category" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="POST" action="{{route('agate.metrics.update-category',$category->id)}}">
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title custom_align">Update Category</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input class="form-control " type="text" value="{{$category->title}}" name="title">
                                    </div> 
                                </div> 
                                <div class="form-group">
                                    <div class="form-group">
                                        <input class="form-control " type="text" value="{{$category->description}}" name="description">
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <input class="form-control " type="text" value="{{$category->type}}" name="type">
                                    </div> 
                                </div>                                                           
                            </div>
                            <div class="modal-footer ">
                                <button type="submit" class="btn btn-success">
                                    <span class="glyphicon glyphicon-ok-sign"></span> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection 

@section('footer-scripts')

@endsection