<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
            <div class="widget widget-default">
                <header class="widget-header">
                    Accounts
                </header>
                <div class="widget-body">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th>Agent</th>
                                <th>Call Date</th>
                                <th>Campaign Id</th>
                                <th>Call Length</th>
                                <th>Dispo</th>
                                <th>Listen</th>
                                <th>done</th>
                                <th>locked</th>
                                <th>score</th>
                                <th>Evaluator</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                          @if($results->count())
                            @foreach($results as $r)
                            <tr >
                                <td>{{$r->agent}}</td>
                                <td>{{$r->call_date}}</td>
                                <td>{{$r->campaign_id}}</td>
                                <td>{{$r->length_in_min}}</td>
                                <td>{{$r->status}}</td>
                                <td>
                                <audio controls="controls">  
                                    <source src="{{$r->location}}" />  
                                </audio>
                                </td> 
                                <td>{{($r->is_complete == '') ? 'no' : 'yes'}} </td>
                                <td>{{($r->locked == 0) ? 'no' : 'yes'}} </td>
                                <td>{{$r->score}} </td>
                                <td>{{$help->evaluator($r->evaluator)}}</td>
                                <td class="tbllink" data-link="{{route('ruby.audit.index',$r->id)}}?bound={{$bound}}&account=1" > <!-- //{base+"/audit/"+list.id+"?bound="+props.data.bound+"&account="+props.data.account_id} -->
                                  <a href='#' class="editable editable-click">Audit</a>
                                </td>
                            </tr> 
                            @endforeach
                          @endif                         
                        </tbody>
                </div>
            </div>
@endsection 

@section('footer-scripts')

@endsection