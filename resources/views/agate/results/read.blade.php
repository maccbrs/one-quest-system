<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
            <div class="widget widget-default">
                <header class="widget-header">
                    Accounts
                </header>
                <div class="widget-body">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th>Year</th>
                                <th>Week No</th>
                                <th>Audited Calls</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($results as $r)
                            <tr>
                                <td>{{$r->year}}</td>
                                <td>{{$r->week_no}}</td>
                                <td>{{(isset($counts[$r->week_no])?$counts[$r->week_no]:0)}}</td>
                                <td>
                                  <a href="{{route('agate.results.lists',['id' => $id,'weekno' => $r->week_no])}}" class="btn btn-primary btn-sm">view lists</a>
                                </td>
                            </tr>                              
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
@endsection 

@section('footer-scripts')

@endsection