<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
            <div class="widget widget-default">
                <header class="widget-header">
                    Accounts
                </header>
                <div class="widget-body">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th>Accounts</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($campaigns as $c)
                            <tr>
                                <td><a href="{{route('agate.results.read',$c->id)}}">{{$c->title}}</a> </td>
                            </tr>                              
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
@endsection 

@section('footer-scripts')

@endsection