<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
  
  <header class="page-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="page-header-heading">
                  {!! Breadcrumbs::render('agate.set.index', $id) !!}
                </h1>
            </div>
        </div>
    </div>
  </header>

  <div class="widget widget-default">

      <div class="widget-body">

         <table>
            <thead></thead>
            <tbody>
            <tr>
              <th> Title : </th>
              <td>{{$set->title}}</td>
              <th> Call duration (min):</th>
              <td>{{$set->duration_min}}</td>
              <th> Time generate: </th>
              <td>{{$set->time_generate}}</td>
            </tr>

            <tr>
              <th> Bound : </th>
              <td>{{$set->bound}}</td>
              <th> Call duration (max): </th>
              <td>{{$set->duration_max}}</td>
              <th>Next generate:  </th>
              <td>{{$set->next_generate}}</td>
            </tr>

            <tr>
              <th> Call count: </th>
              <td>{{$set->call_count}}</td>
              <td></td>
              <td></td>
              <th> Frequency:</th>
              <td>{{$set->frequency}}</td>
            </tr>
            </tbody>
          </table>

          <br/>

          <table>
            <thead></thead>
            <tbody>
            <tr>
              <th> Ingroups: </th>
              <td>{{$set->ingroups}}</td> 
            </tr>

            <tr>
              <th> Agents: </th>
              <td>{{$set->agents}}</td>
            </tr>

            <tr>
              <th> Dispositions: </th>
              <td>{{$set->disposition}}</td>
            </tr>
            </tbody>
          </table>

      </div>
  </div>

  <div class="widget widget-default">
      <div class="widget-body">
          <table class="table table-bordered customized-table">
              <thead>
                <tr>
                    <th>Audit Date</th>
                    <th>Count</th>
                    <th>Locked</th>
                </tr>
              </thead>
              <tbody>
                @foreach($audits as $a)
                <tr>
                    <td   > 
                      <a href="{{route('agate.audits.index',$a->id)}}">{{$a->audit_date}}</a>
                    </td>
                    <td > 
                      {{$a->closer->count()}}
                    </td>                          
                    <td > 
                      {{$a->closer->where('locked',1)->count()}}
                    </td>                            </tr> 
                @endforeach                              
              </tbody>
          </table>
         
            {{ $audits != null ? $audits->links() : ""  }}

         
      </div>
  </div>      
        
@endsection 

@section('footer-scripts')

@endsection


