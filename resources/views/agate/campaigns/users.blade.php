<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.campaigns.users',$bound,$campaign) !!}
                    </h1>
                </div>
            </div>
        </div>
    </header>

    <div class="widget widget-default">
        <header class="widget-header">
            {{$campaign}} ({{$bound}}) users
        </header>
        <div class="widget-body">
            <table class="table table-bordered">
                <tbody>
                    <tr> <?php $count = 0; ?>
                    	@foreach($users as $user)
                    		@if($user->agent)

                        		<td><a href="{{route('agate.campaigns.user',['bound' => $bound,'campaign' => $campaign,'id' => $user->agent])}}">{{$user->agent}}</a> </td>
                        		<?php $count++;
                        			if($count == 5):
                        				echo '</tr><tr>';
                        				$count = 0;
                        			endif;
                        		?>

                        	@endif
                        @endforeach                                                               
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
@endsection 

@section('footer-scripts')

@endsection