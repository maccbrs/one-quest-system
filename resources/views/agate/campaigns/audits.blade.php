<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                         
                    </h1>
                </div>
            </div>
        </div>
     </header>

    <div class="widget widget-default">

        <div class="widget-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Agent</td>
                                    <td>Call Date</td>
                                    <td>Dispo</td>
                                    <td>Campaign</td>
                                    <td>Score</td>
                                </tr>
                                @foreach($items as $item)
                                <tr>
                                    <td>{{$item->agentObj->name}}</td>
                                    <td>{{$item->call_date}}</td>
                                    <td>{{$item->dispo}}</td>
                                    <td>{{($item->campaign?$item->campaign->title:'')}}</td>
                                    <td>{{$item->score}}</td>
                                </tr>
                                @endforeach                       
                            </tbody>
                        </table>
        </div>
    </div>
@endsection 

@section('footer-scripts')

@endsection 