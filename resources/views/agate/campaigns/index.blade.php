<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                            {!! Breadcrumbs::render('agate.campaigns.index') !!}
                    </h1>
                </div>
            </div>
        </div>
     </header>

    <div class="widget widget-default">

        <div class="widget-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>Name</th>
                        <th>#</th>
                    </tr>
                    @foreach($campaigns as $camp)  
                    <tr>
                        <td>{{$camp->title}}</td>
                        <td class="text-right">
                        <a href="{{route('agate.campaigns.audits',$camp->id)}}" class="btn btn-transparent btn-xs">
                            <span class="fa fa-folder"></span> 
                        </a> 
                        </td>                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection 

@section('footer-scripts')

@endsection 