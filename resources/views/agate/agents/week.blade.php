<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
            <div class="widget widget-default">
                <header class="widget-header">
                    {{$bound}}</br> 
                    Agent: {{$agent}} </br>
                    Week No: {{$weekno}}
                </header>
                <div class="widget-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Call Date</th>
                                <th>Campaign Id</th>
                                <th>Length</th>
                                <th>Dispo</th>
                                <th>Recording</th>
                            </tr>
                        </thead>                        
                        <tbody>
                            @foreach($items as $item)
                            <tr> 
                                <td>{{$item->call_date}}</td>
                                <td>{{$item->campaign_id}}</td>
                                <td>{{$item->length_in_min}}</td>
                                <td>{{$item->status}}</td>
                                <td>
                                    <audio controls>
                                      <source src="{{$item->location}}" type="audio/mpeg">
                                    </audio>
                                </td>                       
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$items->links()}}
                </div>
            </div>
@endsection 

@section('footer-scripts')

@endsection