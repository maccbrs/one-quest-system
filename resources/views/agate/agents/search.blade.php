<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')
@section('header-scripts')
 <link rel="stylesheet" href="{{$asset}}varell/css/datetimepicker.css">
@endsection

@section('content')
            <div class="widget widget-default">
                <header class="widget-header">
                    <p>Filter Result: dispo = {{($dispo?$dispo:'empty')}}
                    </br>
                    Date Range: {{$daterange}}
                    </p>
                    {{$bound}}</br> Agent: {{$agent}} <button class="btn btn-success btn-xs pull-right" data-toggle="modal" data-target="#filter-search" data-placement="top"><span class="fa fa-fw fa-search"></span></button>
                    <div class="clearfix"></div> 
                </header>
                <div class="widget-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Call Date</th> 
                                <th>Campaign Id</th>
                                <th>Agent</th>
                                <th>Length</th>
                                <th>Dispo</th>
                                <th>Recording</th>
                                <th>#</th>
                            </tr>
                        </thead>                        
                        <tbody>
                            @foreach($items as $item)
                            <tr> 
                                <td>{{$item->call_date}}</td>
                                <td>{{$item->campaign_id}}</td>
                                <td>{{$item->agent}}</td>
                                <td>{{$item->length_in_min}}</td>
                                <td>{{$item->status}}</td>
                                <td>
                                    <audio controls>
                                      <source src="{{$item->location}}" type="audio/mpeg">
                                    </audio>
                                </td>
                                <td>
                                    @if(!$item->locked)
                                    <a href={{route('agate.audits.audit',['bound' => $bound,'id' =>$item->id])}} class="btn btn-success "><i class="fa fa-file-audio-o"></i></a>
                                    @else
                                    <a class="btn btn-faded btn-transparent" href={{route('agate.audits.audit',['bound' => $bound,'id' =>$item->id])}} class="btn btn-success "><i class="fa fa-file-audio-o"></i></a>
                                    @endif
                                </td>                       
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$items->appends(Request::only('dispo','daterange'))->links()}}
                </div>
            </div>

            <div class="modal fade" id="filter-search" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="GET" action="{{route('agate.agents.campaign-search',['bound' => $bound,'user' => $agent,'id' => $id])}}">
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title custom_align">Search</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Dispo</label>
                                        <input class="form-control " type="text" value="{{$dispo}}" name="dispo">
                                    </div> 
                                    <div class="form-group">
                                        <label>Date Range</label>
                                        <input type="text" name="daterange" class="form-control drp daterangeinput2" value="" placeholder="Date range picker">
                                    </div>
                                </div>                                                         
                            </div>
                            <div class="modal-footer ">
                                <a href="{{route('agate.agents.campaign',['bound' => $bound,'user' => $agent,'id' => $id])}}" class="btn btn-success">
                                    <span class="glyphicon glyphicon-ok-sign"></span> Clear Filter
                                </a>                                
                                <button type="submit" class="btn btn-success">
                                    <span class="glyphicon glyphicon-ok-sign"></span> Search
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

@endsection 

@section('footer-scripts')
    <script src="{{$asset}}milestone/vendor/moment/min/moment.min.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{{$asset}}milestone/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

    <script type="text/javascript">

      $('.drp').daterangepicker({
          timePicker: true,
          autoUpdateInput: false,
          locale: {
              cancelLabel: 'Clear'
          }
      }); 

    $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('YYYY-MM-DD HH:MM:SS') + ' | ' + picker.endDate.format('YYYY-MM-DD HH:MM:SS'));
    });

  $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
  
    </script>    
@endsection