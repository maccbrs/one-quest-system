<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
    
    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.agents.campaigns', $bound,$campaign,$user) !!}
                    </h1>
                </div>
            </div>
        </div>
    </header>

    <div class="widget widget-default">
        <header class="widget-header">
            {{$user}} ({{ucfirst($bound)}}) Campaigns
        </header>
        <div class="widget-body">
            <table class="table table-bordered">
                <tbody>
                    <tr> <?php $count = 0; ?>
                        @if($campaigns->count())
                    	@foreach($campaigns as $campaign)
                    		@if($campaign->campaign_id)

                        		<td><a href="{{route('agate.agents.campaign',['bound' => $bound,'user' => $user,'id' => $campaign->campaign_id])}}">{{$campaign->campaign_id}}</a> </td>
                        		<?php $count++;
                        			if($count == 5):
                        				echo '</tr><tr>';
                        				$count = 0;
                        			endif;
                        		?>

                        	@endif
                        @endforeach
                        @endif                                                               
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
@endsection 

@section('footer-scripts')

@endsection