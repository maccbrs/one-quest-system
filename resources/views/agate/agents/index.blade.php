<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.agents.index') !!}
                    </h1>
                </div>
            </div>
        </div>
     </header>

    <div class="widget widget-default">
        <div class="widget-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>Name</th>
                    </tr>
                    @foreach($agents as $agent)
                        <tr>
                            <td>{{$agent->name}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!!$agents->links()!!}
        </div>
    </div>
    
@endsection 

@section('footer-scripts')

@endsection