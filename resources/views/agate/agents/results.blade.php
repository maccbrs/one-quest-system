<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
            <div class="widget widget-default">
                <header class="widget-header">
                    {{$bound}} </br>
                    QA Results  

                </header>
                <div class="widget-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr> <?php $count = 0; ?>
                            	@foreach($items as $item)
                            		@if($item->agent)

                                		<td><a href="{{route('agate.agents.agent',['bound' => $bound,'agent' => $item->agent])}}">{{$item->agent}}</a> </td>
                                		<?php $count++;
                                			if($count == 8):
                                				echo '</tr><tr>';
                                				$count = 0;
                                			endif;
                                		?>

                                	@endif
                                @endforeach                                                               
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
@endsection 

@section('footer-scripts')

@endsection