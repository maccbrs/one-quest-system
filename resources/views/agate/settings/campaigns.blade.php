<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
   
    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                       {!! Breadcrumbs::render('agate.settings.campaigns') !!}
                    </h1>
                </div>
            </div>
        </div>
    </header>

    <div class="widget widget-default">
        <header class="widget-header">
            Campaigns 
            <button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#add-campaign" data-placement="top"><span class="fa fa-fw fa-plus"></span></button>
        </header>
        <div class="widget-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Campaign</th>
                        <th></th>
                    </tr>
                </thead>                        
                <tbody>
                    @foreach($campaigns as $campaign)
                    <tr >
                        <td>{{$campaign->title}}</td>
                        <td class="text-right">
                            <a href="{{route('agate.settings.edit-campaign',$campaign->id)}}" class="btn btn-transparent btn-xs">
                                <span class="fa fa-pencil"></span> 
                                <span class="hidden-xs hidden-sm hidden-md">Edit</span>
                            </a> &nbsp;

                            <button type="button" class="btn btn-transparent btn-xs" data-toggle="modal" data-target="#delete-categoryItem{{$campaign->id}}">
                                <span class="fa fa-pencil"></span> 
                                <span class="hidden-xs hidden-sm hidden-md">Delete</span>
                            </button>

                            <!-- Modal -->
                            <div id="delete-categoryItem{{$campaign->id}}" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <form method="POST" action="{{route('agate.settings.deletecampaign')}}">
                                                {{ csrf_field() }}
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title custom_align">Delete "{{$campaign->title}}"</h4>
                                                </div>
                                                
                                                <input class="form-control" type="text" value="{{$campaign->id}}" name="id">

                                                <div class="modal-footer ">
                                                    <button type="submit" class="btn btn-warning">
                                                        <span class="glyphicon glyphicon-warning-sign"></span> Delete
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                              </div>
                            </div>
                        </td>
    

                    </tr> 
                    @endforeach                             
                </tbody>
            </table> 
        </div>
    </div>

    <div class="modal fade" id="add-campaign" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{route('agate.settings.addcampaign')}}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title custom_align">Account Name</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input class="form-control" type="text" value="" name="title">
                        </div>                              
                    </div>                            
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok-sign"></span> Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>            
@endsection 

@section('footer-scripts')

@endsection