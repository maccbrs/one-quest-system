<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')
    
    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.settings.deactivate') !!}
                    </h1>
                </div>
            </div>
        </div>
    </header>

    <div class="widget widget-default agate-settings-deactivate">
        <form method="POST" action="{{route('agate.settings.deactivate',['bound' => $bound,'type' => $type])}}">
                {{ csrf_field() }}                  
        <header class="widget-header">
            {{$bound}} {{$type}}

            <button type="submit" class="btn btn-success pull-right">
                <span class="fa fa-refresh"></span> Update
            </button> 
            <div class="clearfix"></div>

        </header>
        <div class="widget-body">

            @if($type == 'campaigns')
            <table class="table table-bordered">
                <tbody>
                    <tr> <?php $count = 0; ?>
                    	@foreach($items as $item)
                    		@if($item->campaign_id)

                        		<td>
                                    <input type="checkbox" name="campaign_ids[]" value="{{$item->campaign_id}}">
                                    {{$item->campaign_id}}
                                </td>
                        		<?php $count++;
                        			if($count == 5):
                        				echo '</tr><tr>';
                        				$count = 0;
                        			endif;
                        		?>

                        	@endif
                        @endforeach                                                               
                    </tr>

                </tbody>
            </table>
            @endif

            @if($type == 'users')
            <table class="table table-bordered">
                <tbody>
                    <tr> <?php $count = 0; ?>
                        @foreach($items as $item)
                            @if($item->agent)

                                <td>
                                    <input type="checkbox" name="agents[]" value="{{$item->agent}}">
                                    {{$item->agent}}
                                </td>
                                <?php $count++;
                                    if($count == 8):
                                        echo '</tr><tr>';
                                        $count = 0;
                                    endif;
                                ?>

                            @endif
                        @endforeach                                                               
                    </tr>

                </tbody>
            </table>
            @endif                    
        </div>

    </form>
    </div>
@endsection 

@section('footer-scripts')

@endsection