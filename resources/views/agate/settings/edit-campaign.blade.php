<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')


@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.settings.edit-campaign', $id) !!}
                    </h1>
                </div>
            </div>
        </div>
    </header>

    <div class="widget widget-default agate-settings-edit_campaign">
        <form method="POST" action="{{route('agate.settings.update-campaign',$campaign->id)}}">
                {{ csrf_field() }}                 
                <header class="widget-header">
                    {{$campaign->title}} 
                    <button type="submit" class="btn btn-success pull-right">
                        <span class="fa fa-refresh"></span> Update
                    </button>   
                    <div class="clearfix"></div>                         
                </header>
                <div class="widget-body">                   
                    <table class="table table-bordered">                      
                        <tbody>
                            <tr >
                                <td class="text-right"><label>Customer Name</label></td>
                                <td>
                                <div class="form-group">
                                    <input class="form-control" type="text" value="{{$campaign->customer_name}}" name="customer_name">
                                </div>                                      
                                </td>
                            </tr> 
                             <tr >
                                <td class="text-right"><label>Caller Id</label></td>
                                <td>
                                <div class="form-group">
                                    <input class="form-control" type="text" value="{{$campaign->caller_id}}" name="caller_id">
                                </div>                                      
                                </td>
                            </tr>
                             <tr >
                                <td class="text-right"><label>Phone Number</label></td>
                                <td>
                                <div class="form-group">
                                    <input class="form-control" type="text" value="{{$campaign->phone_number}}" name="phone_number">
                                </div>                                      
                                </td>
                            </tr>  
                             <tr >
                                <td class="text-right"><label>File Name</label></td>
                                <td>
                                <div class="form-group">
                                    <input class="form-control" type="text" value="{{$campaign->file_name}}" name="file_name">
                                </div>                                      
                                </td>
                            </tr>
                             <tr >
                                <td class="text-right">
                                    <label>Logo</label>
                                    <span class="fa fa-edit mb-cursor btn btn-transparent btn-transparent-success btn-xs" data-toggle="modal" data-target="#update-logo"></span>
                                    </td>
                                <td>
                                <div class="form-group">
                                    <img src="{{asset('uploads/m-'.$campaign->logo)}}">
                                </div>                                      
                                </td>
                            </tr>                                         
                        </tbody>
                    </table> 
                </div>

        </form>
    </div>

    <div class="modal fade" id="update-logo" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{route('agate.settings.campaign-update-logo',$campaign->id)}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title custom_align">Update Logo</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="file" name="file" class="btn btn-sm btn-default"> </span>
                        </div>                              
                    </div>                            
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok-sign"></span> Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div> 

@endsection 

@section('footer-scripts')

@endsection