<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')

  <header class="page-header">
      <div class="container-fluid">
          <div class="row">
              <div class="col-xs-12">
                  <h1 class="page-header-heading">
                     {!! Breadcrumbs::render('agate.audits.index',$audit->id, $audit->set_id) !!}
                  </h1>
              </div>
          </div>
      </div>
   </header>

  <div class="widget widget-default">

      <div class="widget-body">

          <table class = 'audittable'>
            <thead></thead>
            <tbody>
            <tr>
              <th> Title : </th>
              <td>{{$audit->set->title}}</td>
              <th> Call duration (min):</th>
              <td>{{$audit->set->duration_min}}</td>
              <th> Time generate: </th>
              <td>{{$audit->set->time_generate}}</td>
            </tr>

            <tr>
              <th> Bound : </th>
              <td>{{$audit->set->bound}}</td>
              <th> Call duration (max): </th>
              <td>{{$audit->set->duration_max}}</td>
              <th>Next generate:  </th>
              <td>{{$audit->set->next_generate}}</td>
            </tr>

            <tr>
              <th> Call count: </th>
              <td>{{$audit->set->call_count}}</td>
              <td></td>
              <td></td>
              <th> Frequency:</th>
              <td>{{$audit->set->frequency}}</td>
            </tr>
            </tbody>
          </table>
          <br/>

          <table class = 'audittable'>
            <thead></thead>
            <tbody>
            <tr>
              <th> Ingroups: </th>
              <td>{{$audit->set->ingroups}}</td>
            </tr>

            <tr>
              <th> Agents: </th>
              <td>{{$audit->set->agents}}</td>
            </tr>

            <tr>
              <th> Dispositions: </th>
              <td>{{$audit->set->disposition}}</td>
            </tr>
            </tbody>
          </table>

      </div>
  </div>

  <div class="widget widget-default">
      <div class="widget-body">
          <table class="table table-bordered ">
              <thead>
                  <tr>
                      <th>Agent</th>
                      <th>Call Date</th>
                      <th>Campaign Id</th>
                      <th>Call Length</th>
                      <th>Dispo</th>
                      <th>Listen</th>
                      <th>done</th>
                      <th>locked</th>
                      <th>score</th>
                      <th>#</th>
                  </tr>
              </thead>
              <tbody>
                @if($audit->closer->count())
                  @foreach($audit->closer as $closer)
                  <tr >
                      <td>{{$closer->agent}}</td>
                      <td>{{$closer->call_date}}</td>
                      <td>{{$closer->campaign_id}}</td>
                      <td>{{$closer->length_in_min}}</td>
                      <td>{{$closer->status}}</td>
                      <td>
                      <audio controls="controls">  
                          <source src="{{$closer->location}}" />  
                      </audio>
                      </td> 
                      <td>{{($closer->is_complete == '') ? 'no' : 'yes'}} </td>
                      <td>{{($closer->locked == 0) ? 'no' : 'yes'}} </td>
                      <td>{{$closer->score}} </td>
                      <td class="tbllink" data-link="{{route('ruby.audit.index',$closer->id)}}?bound={{$audit->set->bound}}&account={{$audit->set->account_id}}" > <!-- //{base+"/audit/"+list.id+"?bound="+props.data.bound+"&account="+props.data.account_id} -->
                        <a href='#' class="editable editable-click">Audit</a>
                      </td>
                  </tr> 
                  @endforeach
                @endif
                @if($audit->recordings->count())
                <tr >
                    <td>{list.agent}</td>
                    <td>{timeFormat(list.call_date)}</td>
                    <td>{list.campaign_id}</td>
                    <td>{list.length_in_min}</td>
                    <td>{list.status}</td>
                    <td>
                      <audio id="player" controls crossorigin ><source src={list.location} type="audio/mp3" /></audio>
                    </td> 
                    <td class="tbllink" data-link={base+"/audit/"+list.id+"?bound="+props.data.bound+"&account="+props.data.account_id}>
                      <a href='#' class="editable editable-click">Audit</a>
                    </td>
                </tr> 
                @endif                          
              </tbody>
          </table>
      </div>
  </div>            
@endsection 

@section('footer-scripts')

@endsection