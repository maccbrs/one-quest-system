<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('header-scripts')
<style type="text/css">
  .audits-audit th{
        background-color: rgba(28, 162, 206, 0.25);
  }

  .audits-audit td .form-group,td .form-group{ 
    margin: 0;
  }
  .audits-audit .align-c{
    text-align: center;
  }


</style>
@endsection

@section('content')

  <div class="widget widget-default">

      <div class="widget-body">

          <table class ='table table-bordered table-striped audits-audit'>
            <tbody>
            <tr>
              <th> Agent:</th>
              <td>{{$item->agent}}</td>
              <th>Call Date:</th>
              <td>{{$item->call_date}}</td>
              <th>Recording</th>
              <td>
                  <audio controls>
                    <source src="{{$item->location}}" type="audio/mpeg">
                  </audio>
              </td>
            </tr>

            <tr>
              <th>Campaign</th>
              <td>{{$item->campaign_id}}</td>
              <th>Dispo:</th>
              <td>{{$item->status}}</td>
              <th>Term Reason</th>
              <td>{{$item->term_reason}}</td>
            </tr>

            <tr>
              <th>Week No</th>
              <td>{{$item->week_no}}</td>
              <th>Month</th>
              <td>{{$item->month}}</td>
              <th>Year</th>
              <td>{{$item->year}}</td>
            </tr>

            </tbody>
          </table>

          <div class="row">
            <div class="col-md-9">
              @if($item->account_id && $item->account)
              <table class ='table table-bordered table-striped audits-audit'>
                <tbody>

                <tr>
                  <th> Account Name:</th>
                  <td>{{$item->account->title}}</td>
                  <th> Evaluator:</th>
                  <td>{{($item->evaluator?$item->evaluatorObj->name:'')}}</td>
                  <th>Score</th>
                  <td>{{$item->score}}</td>                            
                </tr>

                </tbody>
              </table>
              @endif
            </div>
            <div class="col-md-3">

              @if($item->account_id && $item->account && $item->locked)
                <img src="{{asset('uploads/m-'.$item->account->logo)}}">
              @endif

            </div>
          </div>

          <div class="row" style="margin-bottom:10px;">
            <div class="col-md-4">
              <div class="form-group">
                  <label class="col-sm-5">Customer Name</label>
                  <div class="col-sm-7">
                      <p>{{$item->customer_name}}</p>
                  </div>
              </div>  
            </div>
            <div class="col-md-4">
              <div class="form-group">
                  <label class="col-sm-5">Customer Phone</label>
                  <div class="col-sm-7">
                      <p>{{$item->customer_phone}}</p>
                  </div>
              </div>  
            </div> 
            <div class="col-md-4">
              <div class="form-group">
                  <label class="col-sm-5">Monitoring Type</label>
                  <div class="col-sm-7">
                    <p>{{$item->monitoring_type}}</p>
                  </div>
              </div>  
            </div>                                                           
          </div>                    

          @if($item->locked)
            @if($item->qa_results)
              <?php 
                $template = json_decode($item->qa_results);
                $except = [];
               ?>
                  <div class="panel panel-default">
                      <div class="panel-body">
                          <table class ='table table-bordered audits-audit'>
                            <thead>
                                <tr>
                                    <th>Key Factors</th>
                                    <th>Parameters</th>
                                    <th>QA Elements</th>
                                    <th>Areas of Opportunity</th>
                                    <th>Feedback</th>
                                </tr>
                            </thead>                                      
                            <tbody>
                              @foreach($template->keyfactors as $keyfactor)
                                <tr>
                                  <th class="align-c">{{$keyfactor->title}}<br> {{$keyfactor->points}}(%)</th>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                  @foreach($keyfactor->parameters as $parameter)
                                    <tr>
                                      <td></td>
                                      <th class="align-c">
                                        {{$parameter->title}}
                                        <br>
                                        ({{$parameter->points}} points)
                                      </th>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                    </tr> 
                                        @foreach($parameter->items as $param)
                                        <tr>
                                          <td></td>
                                          <td></td>
                                          <td>{!!($param->status?'<i class="fa fa-check-square-o" aria-hidden="true"></i>':'<i class="fa fa-square-o" aria-hidden="true"></i>')!!} {{$param->title}}</td>
                                          <td>{{(isset($param->aoo)?$param->aoo:'')}}</td>
                                          <td>{{(isset($param->fb)?$param->fb:'')}}</td>
                                        </tr>                                              
                                        @endforeach                                         
                                  @endforeach
                              @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>                   
            @endif
          @endif


      </div>
  </div>



@endsection 

@section('footer-scripts')

@endsection