<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('header-scripts')
<style type="text/css">
  .audits-audit th{
        background-color: rgba(28, 162, 206, 0.25);
  }

  .audits-audit td .form-group,td .form-group{ 
    margin: 0;
  }
  .audits-audit .align-c{
    text-align: center;
  }


</style>
@endsection

@section('content')

  <header class="page-header">
      <div class="container-fluid">
          <div class="row">
              <div class="col-xs-12">
                  <h1 class="page-header-heading">
                      {!! Breadcrumbs::render('agate.audits.audit', $bound,$item) !!}
                  </h1>
              </div>
          </div>
      </div>
  </header>

  <div class="widget widget-default">
    <div class="widget-body">
      <table class ='table table-bordered table-striped audits-audit'>
        <tbody>
        <tr>
          <th> Agent:</th>
          <td>{{$item->agent}}</td>
          <th>Call Date:</th>
          <td>{{$item->call_date}}</td>
          <th>Recording</th>
          <td>
              <audio controls>
                <source src="{{$item->location}}" type="audio/mpeg">
              </audio>
          </td>
        </tr>

        <tr>
          <th>Campaign</th>
          <td>{{$item->campaign_id}}</td>
          <th>Dispo:</th>
          <td>{{$item->status}}</td>
          <th>Term Reason</th>
          <td>{{$item->term_reason}}</td>
        </tr>

        <tr>
          <th>Week No</th>
          <td>{{$item->week_no}}</td>
          <th>Month</th>
          <td>{{$item->month}}</td>
          <th>Year</th>
          <td>{{$item->year}}</td>
        </tr>

        </tbody>
      </table>

      <div class="row">
        <div class="col-md-9">
          @if($item->account_id && $item->account)
          <table class ='table table-bordered table-striped audits-audit'>
            <tbody>

            <tr>
              <th> Account Name:</th>
              <td>{{$item->account->title}}</td>
              <th> Evaluator:</th>
              <td>{{($item->evaluator?$item->evaluatorObj->name:'')}}</td>
              <th>Score</th>
              <td>{{$item->score}}</td>                            
            </tr>

            </tbody>
          </table>
          @endif
        </div>
        <div class="col-md-3">

            @if($item->account_id && $item->account && $item->locked)
              <span class="fa fa-edit mb-cursor btn btn-transparent btn-transparent-success btn-xs" data-toggle="modal" data-target="#update-account"></span>
              <img src="{{asset('uploads/m-'.$item->account->logo)}}">
            @endif

        </div>
      </div>

      <table class ='table table-bordered table-striped audits-audit'>
        <form method="POST" action="{{route('agate.audits.lock',['bound' => $bound,'id' => $item->id])}}">
          {{ csrf_field() }}                      
        <tbody>
        <tr>
          @if(!$item->locked)

            <th>Template</th>

            <td>
            <div class="form-group">
                <select class="form-control" name="template_id">
                    @foreach($templates as $template)
                    <option value="{{$template->id}}">{{$template->title}}</option>
                    @endforeach
                </select>
            </div>
            </td>

            <th>Account</th>
            <td>
              <div class="form-group"> 
                  <select class="form-control" name="account_id">
                      @foreach($campaigns as $campaign)
                      <option value="{{$campaign->id}}">{{$campaign->title}}</option>
                      @endforeach
                  </select>
              </div>
            </td> 

            <th>Archive</th>
            <td>
              <div class="form-group">
                  <select class="form-control" name="archive_id">
                      @foreach($archives as $archive)
                      <option value="{{$archive->id}}">{{$archive->name}}</option>
                      @endforeach
                  </select>
              </div>
            </td> 

            <td>
              <div class="form-group">
                <button type="submit" class="btn btn-success">submit</button>
              </div>
            </td>                      
          @endif

          
        </tr>
        </tbody>
        </form>

      </table> 
                  
    <!--   <th>Template</th>

      <td>
        <div class="form-group">
            <select class="form-control" name="template_id">
                @foreach($templates as $template)
                <option value="{{$template->id}}">{{$template->title}}</option>
                @endforeach
            </select>
        </div>
      </td>

      <th>Account</th>

      <td>
        <div class="form-group">
            <select class="form-control" name="account_id">
                @foreach($campaigns as $campaign)
                <option value="{{$campaign->id}}">{{$campaign->title}}</option>
                @endforeach
            </select>
        </div>
      </td> 

      <th>Archive</th>
      <td>
        <div class="form-group">
            <select class="form-control" name="archive_id">
                @foreach($archives as $archive)
                <option value="{{$archive->id}}">{{$archive->name}}</option>
                @endforeach
            </select>
        </div>
      </td> 

      <td>
        <div class="form-group">
          <button type="submit" class="btn btn-success">submit</button>
        </div>
      </td>     -->



            
          </tr>
          </tbody>
          </form>

        </table> 


    </div>
  </div>

  @if($item->locked) 

  <div class="widget widget-default">
      <div class="widget-body">

          @if($item->qa_results)

            <?php 
              $template = json_decode($item->qa_results);
              $except = [];
             ?>

             
            <form method="POST" action="{{route('agate.audits.score',['bound' => $bound,'id' => $id])}}">

                {{ csrf_field() }}

                <button type="button" style="margin-left:5px;" class="btn btn-success btn-xs btn-transparent btn-transparent-success pull-right" data-toggle="modal" data-target="#refresh-template" data-placement="top"><span class="fa fa-close"></span></button>
                <button type="submit" class="btn btn-success btn-xs pull-right">
                    <span class="fa fa-refresh"></span> Update
                </button>                            
                <div class="clearfix" style="margin-bottom:10px;"></div> 



                <div class="row" style="margin-bottom:10px;">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-5">Customer Name</label>
                        <div class="col-sm-7">
                            <input class="form-control" type="text" value="{{$item->customer_name}}" name="customer_name">
                        </div>
                    </div>  
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-5">Customer Phone</label>
                        <div class="col-sm-7">
                            <input class="form-control" type="text" value="{{$item->customer_phone}}" name="customer_phone">
                        </div>
                    </div>  
                  </div> 
                </div>
                <div class="row" style="margin-bottom:10px;">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-5">Monitoring Type</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="monitoring_type">
                              <option value="Call Recording" {{($item->monitoring_type == "Call Recording"?"selected":"")}} >Call Recording</option>
                              <option value="Remote" {{($item->monitoring_type == "Remote"?"selected":"")}} >Remote</option>
                              <option value="Side By Side" {{($item->monitoring_type == "Side By Side"?"selected":"")}} >Side By Side</option>
                            </select>
                        </div>
                    </div>  
                  </div>    
                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-5">Call Summary</label>
                        <div class="col-sm-7">

                          <textarea class="form-control" name="call_summary">{{$item->call_summary}}</textarea>

                        </div>
                    </div>  
                  </div>                                                        
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class ='table table-bordered audits-audit'>
                          <thead>
                              <tr>
                                  <th>Key Factors</th>
                                  <th>Parameters</th>
                                  <th>QA Elements</th>
                                  <th>Areas of Opportunity</th>
                                  <th>Feedback</th>
                              </tr>
                          </thead>                                      
                          <tbody>
                            @foreach($template->keyfactors as $keyfactor)
                              <tr>
                                <th class="align-c">{{$keyfactor->title}}<br> {{$keyfactor->points}}(%)</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                                @foreach($keyfactor->parameters as $parameter)
                                  <tr>
                                    <td></td>
                                    <th class="align-c">
                                      {{$parameter->title}}
                                      <br>
                                      ({{$parameter->points}} points)
                                    </th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                  </tr> 
                                      @foreach($parameter->items as $param)
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td><input type="checkbox" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$param->title}}|mark" {{($param->status?'checked':'')}}  data-icheck>{{$param->title}}</td>
                                        <td><input type="text" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$param->title}}|aoo" class="form-control" value="{{(isset($param->aoo)?$param->aoo:'')}}"></td>
                                        <td><input type="text" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$param->title}}|fb" class="form-control" value="{{(isset($param->fb)?$param->fb:'')}}" ></td>
                                      </tr>                                              
                                      @endforeach                                         
                                @endforeach
                            @endforeach
                          </tbody>
                        </table>
                    </div>
                </div>

            </form> 
                                     
          @endif

      </div>
  </div>

  <div class="modal fade" id="update-account" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <form method="POST" action="{{route('agate.audits.audit-account-update',['id' => $item->id,'bound' => $bound])}}" >
                  {{ csrf_field() }}
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title custom_align">Update Account</h4>
                  </div>
                  <div class="modal-body">
                      <div class="form-group">
                          <select class="form-control input-sm" name="account_id">
                            @foreach($campaigns as $campaign)
                              <option  value="{{$campaign->id}}">{{$campaign->title}}</option>
                            @endforeach
                          </select>
                      </div>                              
                  </div>                            
                  <div class="modal-footer ">
                      <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-ok-sign"></span> Save
                      </button>
                  </div>
              </form>
          </div>
      </div>
  </div> 

  <div class="modal fade" id="refresh-template" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <form method="POST" action="{{route('agate.audits.lock',['id' => $item->id,'bound' => $bound])}}" >
                  {{ csrf_field() }}
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <p class="modal-title custom_align">Refresh Template</p>
                  </div>
                  <div class="modal-body">
                      <div class="form-group">
                        <select class="form-control" name="template_id">
                            @foreach($templates as $template)
                            <option value="{{$template->id}}">{{$template->title}}</option>
                            @endforeach
                        </select>
                      </div>                              
                  </div>                            
                  <div class="modal-footer ">
                      <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-ok-sign"></span> Save
                      </button>
                  </div>
              </form>
          </div>
      </div>
  </div> 

  @else



  @endif

@endsection 

@section('footer-scripts')

@endsection