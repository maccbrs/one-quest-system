<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('header-scripts')
<style type="text/css">
.agate-archives-content .table>tbody>tr>td{
    padding: 2px;
}
.agate-archives-content input{
    cursor: pointer;
}
</style>
@endsection

@section('content')
            <form method="POST" action="{{route('agate.archives.isfinal')}}">
            {{ csrf_field() }}    
                <div class="widget widget-default agate-archives-content">
                    <header class="widget-header">
                        Audits      

                        <button type="submit" class="btn btn-success btn-xs pull-right">
                            <span class="fa fa-refresh"></span> Update
                        </button>
                        <div class="clearfix"></div> 

                    </header>
                    <div class="widget-body">
                    

                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Agent</td>
                                    <td>Call Date</td>
                                    <td>Dispo</td>
                                    <td>Campaign</td>
                                    <td>Score</td>
                                    <td>#</td>
                                </tr>
                                @foreach($items as $item)
                                <tr>
                                    <td>{{$item->agentObj->name}}</td>
                                    <td>{{$item->call_date}}</td>
                                    <td>{{$item->dispo}}</td>
                                    <td>{{($item->campaign?$item->campaign->title:'')}}</td>
                                    <td>{{$item->score}}</td>
                                    <td>
                                        @if($item->final)
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                        @else
                                        <input type="checkbox" name="inbound_{{$item->id}}">
                                        @endif
                                        <a href="{{route('agate.archives.audit',['bound' => 'inbound','id' => $item->id])}}" class="fa fa-folder-open mb-cursor" style="margin-left:10px;"></a>
                                    </td>
                                </tr>
                                @endforeach                       
                            </tbody>
                        </table>

                    </div>
                </div>

            </form>

     
@endsection 

@section('footer-scripts')

@endsection