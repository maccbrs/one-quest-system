<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.archives.index') !!}   
                    </h1>
                </div>
            </div>
        </div>
     </header>

    <div class="widget widget-default">
        <header class="widget-header">
            Archives
            <button class="btn btn-success btn-xs pull-right" data-toggle="modal" data-target="#add-archive" data-placement="top"><span class="fa fa-fw fa-plus"></span></button>
            <div class="clearfix"></div>                     
        </header>
        <div class="widget-body">

            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Campaign</th>
                        <th>Assigned Date</th>
                        <th>Date Created</th>
                        <th class="text-right">#</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($archives as $archive)
                    <tr>
                        <td>{{$archive->name}}</td>
                        <td>{{$archive->campaignObj->customer_name}}</td>
                        <td>{{$archive->month.'/'.$archive->date.'/'.$archive->year}}</td>
                        <td>{{$archive->created_at}}</td>
                        <td class="text-right">
                            <a href="{{route('agate.archives.contents',$archive->id)}}" class="btn btn-transparent btn-xs">
                                <span class="fa fa-folder"></span> 
                            </a> 
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
    </div>

    <div class="modal fade" id="add-archive" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{route('agate.archives.create')}}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title custom_align">Add New Archive</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">

                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control " type="text" value="" name="title">
                            </div> 

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <select class="form-control" name="date">

                                            @for($x = 1; $x <= 31; $x++)
                                            <option value="{{$x}}">{{$x}}</option>
                                            @endfor
                                        </select>
                                    </div> 
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Month</label>
                                        <select class="form-control" name="month">

                                            @for($x = 1; $x <= 12; $x++)
                                            <option value="{{$x}}">{{$x}}</option>
                                            @endfor
                                        </select>
                                    </div> 
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <select class="form-control" name="year">

                                            @for($x = 2017; $x <= 2022; $x++)
                                            <option value="{{$x}}">{{$x}}</option>
                                            @endfor
                                        </select>
                                    </div> 
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Campaigns</label>
                                        <select class="form-control" name="campaign">
                                            @foreach($campaigns as $campaign)
                                                <option value="{{$campaign->id}}">{{$campaign->title}}</option>
                                            @endforeach
                                        </select>
                                    </div> 
                                </div>                                                    
                            </div>                                    

                        </div>                                                         
                    </div>
                    <div class="modal-footer ">                             
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok-sign"></span> Search
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>   
                     
@endsection 

@section('footer-scripts')

@endsection