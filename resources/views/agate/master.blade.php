<?php 
$asset = URL::asset('/'); 
$usertype = Auth::user()->user_type; 
$request = Request::instance();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{$asset}}varell/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{$asset}}varell/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{$asset}}varell/css/typicons.min.css">
    <link rel="stylesheet" href="{{$asset}}varell/css/varello-theme.min.css">
    <link rel="stylesheet" href="{{$asset}}varell/css/varello-skins.min.css">
    <link rel="stylesheet" href="{{$asset}}varell/css/animate.min.css">
    <link rel="stylesheet" href="{{$asset}}varell/css/icheck-all-skins.css">
    <link rel="stylesheet" href="{{$asset}}varell/css/icheck-skins/flat/_all.css">
    <link href='https://fonts.googleapis.com/css?family=Hind+Vadodara:400,500,600,700,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{$asset}}varell/css/bootstrap-notify.css">
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="{{$asset}}varell/css/custom.css">
	 <link rel="stylesheet" href="http://localhost:8000/datatables/DataTables-1.10.15/media/css/jquery.dataTables.min.css">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .page-header .page-header-heading{
            font-size: 14px;
            margin-bottom: 7px;
        }        
        .wrapper .page-header{
            padding: 0px;
        }
		#data-table tbody tr td, #data-table tbody tr{
			/* background-color:#2f3638 !important; */
			color:#000000;
			background-color:#b3b3b3;
		
		}
    </style>
    @yield('header-scripts')
</head> 

<body>

    <div class="notifications top-right"></div>
    <div class="wrapper">
        <div class="page-wrapper">
            <aside class="left-sidebar">

                <section class="sidebar-user-panel">
                    <div id="user-panel-slide-toggleable">
                        <div class="user-panel-profile-picture">
                            <img src="{{$request->query('UserImageM')}}" alt="{{Auth::user()->name}}">
                        </div>
                        <span class="user-panel-logged-in-text"><span class="fa fa-circle-o text-success user-panel-status-icon"></span> Logged in as <strong> {{Auth::user()->name}}</strong>
                        </span>
                    </div>
                    <!-- <button class="user-panel-toggler" data-slide-toggle="user-panel-slide-toggleable"><span class="fa fa-chevron-up" data-slide-toggle-showing></span><span class="fa fa-chevron-down" data-slide-toggle-hidden></span></button> -->
                </section>

                <ul class="sidebar-nav">
                    <li class="sidebar-nav-link  active ">
                        <a href="{{route('agate.dashboard.index')}}">
                            <span class="typcn typcn-home sidebar-nav-link-logo"></span> Dashboard
                        </a>
                    </li>
                    <li class="sidebar-nav-heading">
                        Audit
                    </li> 
                    <li class="sidebar-nav-link">
                        <a href="#" data-toggle="modal" data-target=".custom-audit">
                           <span class="fa fa-pencil sidebar-nav-link-logo"></span> Custom
                        </a>
                    </li> 
					<li class="sidebar-nav-link">
                        <a href="{{route('agate.custom.index')}}" >
                           <span class="fa fa-list-alt sidebar-nav-link-logo"></span> Audit List
                        </a>
                    </li> 
                    <li class="sidebar-nav-link sidebar-nav-link-group ">
                        <a href="{{route('agate.agents.index')}}">
                           <span class="typcn typcn-user sidebar-nav-link-logo"></span>Agents
                        </a>                    
                    </li>

                    <li class="sidebar-nav-link">
                        <a href="{{route('agate.search.index')}}">
                           <span class="fa fa-search sidebar-nav-link-logo"></span> Search
                        </a>
                    </li>

                    <li class="sidebar-nav-link sidebar-nav-link-group ">
                        <a href="{{route('agate.campaigns.index')}}">
                           <span class="fa fa-flag sidebar-nav-link-logo"></span>Campaigns
                        </a>    
                    </li>                    
                    <li class="sidebar-nav-heading">
                        Results
                    </li> 
                    <li class="sidebar-nav-link">
                        <a href="{{route('agate.archives.index')}}">
                           <span class="fa fa-archive sidebar-nav-link-logo"></span> Archives
                        </a>
                    </li>

                    <li class="sidebar-nav-heading">
                        Metrics
                    </li>  
                    <li class="sidebar-nav-link">
                        <a href="{{route('agate.metrics.parameters')}}">
                           <span class="fa fa-list sidebar-nav-link-logo"></span> Parameters
                        </a>
                    </li>
                    <li class="sidebar-nav-link">
                        <a href="{{route('agate.metrics.keyfactors')}}">
                           <span class="fa fa-list-ul sidebar-nav-link-logo"></span> Key Factors
                        </a>
                    </li>  
                    <li class="sidebar-nav-link">
                        <a href="{{route('agate.metrics.templates')}}">
                           <span class="fa fa-list-ol sidebar-nav-link-logo"></span> Templates
                        </a>
                    </li>                                                            
                    <li class="sidebar-nav-heading">
                        Settings
                    </li>

                                                                                                                                                          
                </ul>

            </aside>


            <header class="top-header">
                <a href="index.html" class="top-header-logo">
                    <span class="text-primary">QA Tool</span>Admin
                </a>
                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">

                            <button type="button" onclick="goBack()" class="navbar-sidebar-toggle" >
                                <span class="typcn typcn-arrow-left visible-sidebar-sm-open"></span>
                                <span class="typcn typcn-arrow-right visible-sidebar-sm-closed"></span>
                                <span class="typcn typcn-arrow-left visible-sidebar-md-open"></span>
                                <span class="typcn typcn-arrow-right visible-sidebar-md-closed"></span>
                            </button>

                        </div>

                    </div>
                </nav>
            </header>


            <div class="content-wrapper">
                

                <div class="container-fluid">
                    @yield('content')

                    <div class="modal fade custom-audit" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-md">
                            <div class="modal-content">
                                <div class="modal-body" >
                                   
                                    <div class="card">

                                        <div class="card-block">
                                            <form id="form-submit" action="{{route('agate.custom.create')}}" method="post">
                                                {{ csrf_field() }}
                                                <div class="form-group row">
                                                    <label for="Name" class="col-sm-4 form-control-label text-right">Select Template</label>
                                                    <div class="col-sm-8">
                                                        <select name="template_id" class="form-control input-sm">
                                                            @foreach($templates as $tpl)
                                                                <option  value="{{$tpl->id}}">{{$tpl->title}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>  
                                                <div style="text-align: center">
                                                    <button type="submit" class="btn btn-primary">Proceed</button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="notifications top-right">

                </div>

            </div>
        </div>
    </div>



    <script src="{{$asset}}varell/js/Chart.min.js"></script>
    <script src="{{$asset}}varell/js/jquery-1.12.3.min.js"></script>
    <script src="{{$asset}}varell/js/bootstrap.min.js"></script>
    <script src="{{$asset}}varell/js/jquery.piety.min.js"></script>
    <script src="{{$asset}}varell/js/varello-theme.js"></script>
    <script src="{{$asset}}varell/js/icheck.min.js"></script>
    <script src="{{$asset}}varell/js/dropdown.js"></script>
    <script src="{{$asset}}varell/js/bootstrap-notify.min.js"></script>
	
      <script src="http://localhost:8000/datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

	
	<script>      
$(document).ready(function() {
    $('#data-table').DataTable( {
		
       
       
    } );
} ); // $(document).ready(function()  {
    

</script>

	
	
	
	
	
    <script type="text/javascript">

        @if (count($errors) > 0)
            var msgs = '';
            @foreach ($errors->all() as $error)
                msgs+="{{ $error }} </br>";
            @endforeach


            $('.top-right').notify({
                 message: { html: msgs },
                type: 'danger'
              }).show();  
            //$(".notifications").fadeOut(6000);
            $('.notifications').width(400); 
            $('.close').on("click",function(){
                $('.notifications').hide();
            });          
        @endif 


        @if(session()->has('success'))
            var msgs = "{{session()->get('success')}}";
            $('.top-right').notify({
                 message: { html: msgs },
                type: 'success'
              }).show();  
            $(".notifications").fadeOut(10000);
            $('.notifications').width(400); 
            $('.close').on("click",function(){
                $('.notifications').hide();
            });  
        @endif

        @if(session()->has('failed'))
            var msgs = "{{session()->get('failed')}}";
            $('.top-right').notify({
                 message: { html: msgs },
                type: 'danger'
              }).show();  
            $(".notifications").fadeOut(10000);
            $('.notifications').width(400); 
            $('.close').on("click",function(){
                $('.notifications').hide();
            });  
        @endif
        function goBack() {
            window.history.back();
        }        
    </script>    

    <style>
        .breadcrumb{
            margin-bottom: 0px;
        }

        .page-header{
            padding: 10px 0;
        }
    </style>

    @yield('footer-scripts')
</body>


</html>