<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                          {!! Breadcrumbs::render('home') !!} 
                    </h1>
                </div>
            </div>
        </div>
		
     </header>
  <div class="widget widget-default">
        <header class="widget-header">
            Archives
            <button class="btn btn-success btn-xs pull-right" data-toggle="modal" data-target="#add-archive" data-placement="top"><span class="fa fa-fw fa-plus"></span></button>
            <div class="clearfix"></div>                     
        </header>
        <div class="widget-body">

            <table class="table table-bordered  " id="data-table">
                <thead>
                    <tr><th><input type="checkbox"/></th>
                        <th>Agent Name</th>
                        <th>Evaluation Date</th>
                        <th>Customer Name</th>
                        <th>Phone Number</th>
                        <th class="text-right">#</th>
                    </tr>
                </thead>
                <tbody>
				<?php //var_dump($CustomAudits); 
				
				?>
				@foreach($CustomAudits as $CustomAudit)
				<tr><td><input type="checkbox"/></td>
					<td>{{$CustomAudit->agent}}</td>
					<td>{{$CustomAudit->created_at}}</td>
					<td>{{$CustomAudit->customer_name}}</td>
					<td>{{$CustomAudit->customer_phone}}</td>
					<td  class="text-right" style="font-size:9px;color:#000000">
					<a href="#" ><button type="button" data-toggle="modal"  class="btn btn-xs btn-danger"
											  onclick="" 
											  
											  
											  data-target=".modalDelete"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>delete</button>
											  </a>
					<a href="{{$asset}}agate/custom/audit/{{$CustomAudit->id}}" ><button class="btn btn-xs btn-info">Edit</button></a>
					<a href="{{$asset}}agate/custom/audit/{{$CustomAudit->id}}" ><button class="btn btn-xs btn-success">Post</button></a></td>
				</td>
				@endforeach
				<?php 
				//var_dump($CustomAudits);
				/*
					@foreach($CustomAudits as $tl => $tls)
											  <tr>
											  <td>{{$tls['agent']}}</td>
											  <td>{{$tls['created_at']}}</td>
											  <td>{{$tls['customer_name']}}</td>
											  <td>{{$tls['customer_phone']}}</td>
											  <td> <button type="button" data-toggle="modal" 
											  onclick="$('.class_parameter_key').val('{{$tls['parameter_key']}}');
											  $('.class_parameter_value').val('{{$tls['parameter_value']}}');
											  $('.class_id').val('{{$tls['id']}}');
											  $('.class_remarks').val('{{$tls['remarks']}}');
											  $('#desc').html('{{$tls['parameter_key']}}');
											  $('.class_action').val('delete')" 
											  
											  
											  data-target=".modalDelete"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>delete</button></a></td>
											  </tr>
                        
                    </tr> 
                    @endforeach
					*/
				
                 ?>

                </tbody>
            </table>

        </div>
    </div>
	<?php 
	$form_action = route('gemstone.system.edit');
		$array_list2 = array(
			  "header"=>"Parameter", 
			  "form-action"=> route('gemstone.system.edit'), 
			  "form-list"=>  array("hidden" => "hidden|Parameter Key|id",
								   "parameter-key" => "text|Parameter Key|parameter_key",
								   "parameter-value" => "text|Parameter Value|parameter_value",
								   "remarks" => "textarea|Remarks|remarks",
								   "action" => "hidden|Parameter Key|action",
								  )
			  );			  
			  

?>
	
	{!!view('delete-modal',['tl_list' => $array_list2])!!}
	
	<script>
	
	
	</script>
@endsection 

@section('footer-scripts')

@endsection