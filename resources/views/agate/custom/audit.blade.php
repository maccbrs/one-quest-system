<!doctype html><?php $asset = URL::asset('/'); ?> 
<html lang="en">
<!-- Mirrored from milestone.nyasha.me/latest/html/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Aug 2016 23:36:40 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Milestone">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Milestone">
    <meta name="theme-color" content="#4C7FF0">
    <title>Milestone - Bootstrap 4 Dashboard Template</title>

   <!-- <link rel="stylesheet" href="{{$asset}}CSS/bootstrap.css">
    <script src="{{$asset}}JS/bootstrap.min.js"></script>
    <script src="{{$asset}}JS/jquery-3.2.1.js"></script>
    
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <style type="text/css">
    .mb-body{
      width: 90%;
      margin: 20px auto;
    }
    .mb-table th,.mb-table td{
      padding: 2px 5px;
    }
    .mb-table select{
      background-color: #ebebeb; 
      color: #333;
      padding: 2px;      
    }
    .mb-table tr{
      line-height: 30px;
    }
    .mb-table tr input{
      line-height: 20px;
    }
    .keyfactors > p{
background-color: gray;
    color: #fff;
    padding: 6px;
    text-align: center;
    text-transform: uppercase;
    font-weight: bold;
    }
    .keyfactors{
      background-color: #ebebeb;
      padding: 10px;
      margin: 10px;      
    }
    .parameters{
      margin-left: 15px;
    }
    .parameters p{
      margin: 0;
    }
    .items{
      margin-left: 100px;
      padding: 10px;
    }
    .lighter{
          color: #999;
    }
    .items p{
      margin: 0;
    }
    .items input[type="text"]{
      display: inline-block;
      width: 100%;
      position: relative;

    }
    #mb-menu{
      position: absolute;
      z-index: 999;
      /* background-color: rgba(0,0,0,.3); */
	  background-color: #18a7cd;
      /* text-align: center; */
      padding: 5px;
      /* left: 0; */
      top: 0;
      position: fixed;
      border-radius: 2px;
      width: 100%;   
    }
	
    .mb-btn{
        color: #fff;
        background-color: #06BC5A;
        border-color: #05a34e;
        font-size: 12px;
        text-transform: uppercase;
        padding: 5px;
        border: 1px solid #05a34e;
    }
	.mb-btn:hover{
		color: #06BC5A;
        background-color: #fff;
		cursor:pointer;
	}
	
	
	
    .notset{
      background-color: red;
    }
        .custom-table  td, .custom-table th{
               padding: 0px;
            padding-left:10px;
                font-size: 13px;
        }
      ., .custom-table th{
               padding: 0px;
            padding-left:10px;
                font-size: 13px;
        }
        .text-align-center{
            text-align:center !important;
            
        }
        .padding-left-zero{
            padding-left:0px !important;
        }
		.padding-zero{
			padding:0px !important;
			
		}
        
        .custom-table tbody tr td select.form-control 
        {
            height:inherit !important;
            padding: 0px;
        }
        div.container{
            font-size:10px;
        }
		.hidden {display:none;}
		td {vertical-align: middle !important ;text-align:center !important;}
		td.align-left {text-align:left !important;}
		
		td input , td select.form-control {background-color:#f2f2f2;}
    </style>
	
	
	<script>
	$( document ).ready(function() {
		$( "#btn-test" ).click(function() {
			$("#qa_current_score").text("testing");
			
			$('input[type=checkbox]').each(function () {
			   if (this.checked) {
				   $(".parameter_" + $(this).data("param")).text("No"); 
			   }
			});		
		});
		$('input[type=checkbox]').each(function () {
			   if ($(this).prop("checked")==true){
					  $(".parameter_" + $(this).data("param")).text("No");
				}
				else {
					
					$("input[type=checkbox].param_" + $(this).data("param")).each(function () {
					   
					   if($("#qa_score_form .param_" + $(this).data("param") + ":checked").length > 0){
						    $(".parameter_" + $(this).data("param")).text("No");
					   }
					   else{
						   $(".parameter_" + $(this).data("param")).text("Yes");
					   }
					   
					   /*if (this.checked) {
						   $(".parameter_" + $(this).data("param")).text("No");
					   }
					   else {
						   $(".parameter_" + $(this).data("param")).text("Yes"); 
						   
					   }
					   */
					});	
				}
			});		
		

		
		$( ".checkbox" ).on( "change", function() {
				if ($(this).prop("checked")==true){
					  $(".parameter_" + $(this).data("param")).text("No");
					  $("#qa_current_score").text(parseInt($("#qa_current_score").text()) - parseInt($(this).data("qa_element_val")));
				}
				/*else if ($(this).prop("checked")==false){
					  $("#qa_current_score").text(parseInt($("#qa_current_score").text()) + parseInt($(this).data("qa_element_val"))); 	
					  $(".parameter_" + $(this).data("param")).text("Yes");
				} */
				else {
					  $("#qa_current_score").text(parseInt($("#qa_current_score").text()) + parseInt($(this).data("qa_element_val")));
					$("input[type=checkbox].param_" + $(this).data("param")).each(function () {
						
					   
					   if($("#qa_score_form .param_" + $(this).data("param") + ":checked").length > 0){
						    $(".parameter_" + $(this).data("param")).text("No");
					   }
					   else{
						   $(".parameter_" + $(this).data("param")).text("Yes");
					   }
					   
					   /*if (this.checked) {
						   $(".parameter_" + $(this).data("param")).text("No");
					   }
					   else {
						   $(".parameter_" + $(this).data("param")).text("Yes"); 
						   
					   }
					   */
					});	
				}
		  
		});
		  
			
			
		
			
	});</script>
</head>

<body>
<form method="POST" id="qa_score_form"" action="{{route('agate.custom.score',['id' => $id])}}">
     {{ csrf_field() }}
		

@if($item->qa_results)
 <?php 
              $template = json_decode($item->qa_results);
              $except = [];
             ?>
    <br/>      
 <div id="mb-menu">
		<div class="row">
			<div class="col-md-4">
			  <table class=" table-bordered col-md-12 ">
									  <!--<thead class="thead-inverse">
										<tr>
										  <th>#</th>
										  <th>First Name</th>


										</tr>
									  </thead> -->
									  <tbody>
										<tr>
										  <th scope="row">QA Score</th>
										  <td style="font-size: 25px;
				padding-bottom: 0px;
				padding-top: 0px;">
											  <span id="qa_current_score">{{$item->score}}</span>/{{$item->point}}
											</td>

										</tr>

									  </tbody>
									</table>
			</div>
			<div class="col-md-4">
			<img class="" src="http://rallyetoreno.mgstuff.net/RallyeToReno%20Logo%20200x400.png" style="max-height:35px"/>
			</div>
			 <!-- <input type="button" id="btn-test" class="mb-btn" value="test"/> JS Script Testing-->
			<div class="col-md-4"> 
			 <button type="submit" class="mb-btn">Update</button> 
			 <a href="{{route('agate.dashboard.index')}}" class="mb-btn">Dashboard</a>
			 </div>
		</div>
       </div>
					<div class="hidden">
							  <select name="campaign_id">
								  @foreach($campaigns as $campaign)
								  <option value="{{$campaign->id}}" {{($campaign->id == $item->campaign_id?'selected':'')}}>{{$campaign->title}}</option>
								  @endforeach
							  </select>
							  <select name="archive_id">
									  @foreach($archives as $archive)
									  <option value="{{$archive->id}}" {{($archive->id == $item->archive_id?'selected':'')}}>{{$archive->name}}</option>
									  @endforeach
							  </select>
				</div>
                <div class="container" style="    margin-top: 35px;">
                  <div class="row">
                    <div class="col-md-5 col">
                       <table class="table table-bordered custom-table">
                          <!--<thead class="thead-inverse">
                            <tr>
                              <th>#</th>
                              <th>First Name</th>


                            </tr>
                          </thead> -->
                          <tbody>
                            <tr>
                              <th scope="row">Agent</th>
                              <td>
							  
							  <!-- ## with groupings
                                  <select class="selectpicker form-control">
                                  <optgroup label="Picnic">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                  </optgroup>
                                  <optgroup label="Camping">
                                    <option>Tent</option>
                                    <option>Flashlight</option>
                                    <option>Toilet Paper</option>
                                  </optgroup>
                                </select>
								-->
                                  
                                  <select name="agent_id" class="form-control" style="font-size:14px;">
                                        @foreach($agents as $agent)
                                          <option value="{{$agent->id}}" {{($agent->id == $item->agent?'selected':'')}} >{{$agent->name}}</option> 
                                        @endforeach
                                      </select> 
                                </td>

                            </tr>
                            <tr>
                              <th scope="row">Supervisor:</th>
                             <!-- <td>Jacob</td> -->
							 <td> 
							  </td>

                            </tr>
                              <tr>
                              <th scope="row">Evaluator:</th>
                              <td> {{Auth::user()->name}}</td>

                            </tr>
                            <tr>
                              <th scope="row">Monitoring type</th>
                              <td><select name="monitoring_type" class="form-control" style="font-size:14px;">
                                    <option value="Recorded" {{($item->monitoring_type == "Recorded"?"selected":"")}} >Recorded</option>
                                    <option value="Remote" {{($item->monitoring_type == "Remote"?"selected":"")}} >Remote</option>
                                    <option value="Side By Side" {{($item->monitoring_type == "Side By Side"?"selected":"")}} >Side By Side</option>
                                  </select></td>              
                            </tr>
                               <tr>
                              <th scope="row">Call Date</th>
                              <td><input type="text" name="call_date" style="    width: 100%;" id="datetimepicker" value="{{$item->call_date}}"></td>
                            </tr>
                               <tr>
                              <th scope="row">Eval. Date</th>
                              <td> <input type="text" name="eval_date" style="    width: 100%;"  value="{{$item->call_date}}"></td>              
                            </tr>
                                <tr>
                              <th scope="row">Work Week</th>
                              <!-- <td>33</td> -->
							  <td>
							   </td>
                            </tr>
                                <tr>
                              <th scope="row">Month:</th>
                              <td>Aug</td>
                            </tr>
                          </tbody>
                        </table>

                      </div>
                    <div class="col-md-2 col">

                      <img class="col-md-12" src="http://rallyetoreno.mgstuff.net/RallyeToReno%20Logo%20200x400.png"/>
                        <button type="submit" class="col-md-12 mb-btn" >Save</button> 
                         <img class="col-md-12" src="{{$asset}}Images/Magellan-web-logo.png"/>
						 <h2>{{$template->title}}</h2>
                      </div>
                    <div class="col-md-5 col">
                       <table class="table table-bordered col-md-12 col">
                          <!--<thead class="thead-inverse">
                            <tr>
                              <th>#</th>
                              <th>First Name</th>


                            </tr>
                          </thead> -->
                          <tbody>
                            <tr>
                              <th scope="row">QA Score</th>
                              <td style="font-size: 25px;
    padding-bottom: 0px;
    padding-top: 0px;">
                                  <span id="qa_current_score">{{$item->score}}</span>/{{$item->point}}
                                </td>

                            </tr>

                          </tbody>
                        </table>
                        <table class="table table-bordered custom-table">
                          <!--<thead class="thead-inverse">
                            <tr>
                              <th>#</th>
                              <th>First Name</th>


                            </tr>
                          </thead> -->
                          <tbody>
                            <tr>
                              <th scope="row">Customer's Name:</th>
                              <td>
                                  Jacob
                                </td>

                            </tr>
                            <tr>
                              <th scope="row">Call ID:</th>
                              <td>Jacob</td>

                            </tr>

                               <tr>
                              <th scope="row">Phone Number:</th>
                              <td>Larry</td>
                            </tr>

                                <tr>
                              <th scope="row">User ID:</th>
                              <td>Larry</td>
                            </tr>
                                <tr>
                              <th scope="row">Vici Disposition</th>
                              <td>LarryLarryLarryLarryLarry</td>
                            </tr>
                          </tbody>
                        </table>

                      </div>
                  </div>  

                    <div class="row">
                        <table class="table table-bordered custom-table">
                         <thead class="thead-inverse">
                            <tr>

                              <th class="text-align-center">CALL SUMMARY</th>


                            </tr>
                          </thead>
                          <tbody>
                            <tr>

                              <td class="text-align-center  padding-left-zero">
                                <textarea class="form-control" name="call_summary" style="background-color:#f2f2f2;">{{$item->call_summary}}
                                  </textarea>
                                </td>

                            </tr>
                          </tbody>
                        </table>

                    </div> 
					
					<div class="row" >
                        <table class="table table-bordered" style="table-layout:fixed;
  width:100%;">
                         <thead class="thead-inverse">
                           <tr>

                                 <th>Key Factors</th>
                                 <th>Parameter</th>
                                 <th style="    width: 5%;">Rating</th>
                                 <th>QA Element</th>
                                 <th>Areas Of Opportunity</th>
                                 <th>Feedback</th>

                            </tr> 
                          </thead>
						  
                          <tbody>
					
					
					
                     @foreach($template->keyfactors as $keyfactor)
					  <?php if(!isset($keyfactor->scoringtype)) $keyfactor->scoringtype = 'standard'; ?>
					 
					
							<tr><td colspan="6"><?php //echo count($keyfactor->parameters); ?> </td></tr>
						   @foreach($keyfactor->parameters as $parameter)
						   
						    
                            <tr>
							<?php	if ($parameter === reset($keyfactor->parameters))
									echo ' <td class="padding-left-zero"rowspan="' .   count($keyfactor->parameters) .'" style="font-size:18px;vertical-align: middle;"><p>' . $keyfactor->title . '<span> (' . $keyfactor->points . 'points)</span></p></td>';

									if ($parameter === end($keyfactor->parameters))
									//echo ' <td class="padding-left-zero"rowspan="' .   count($template->keyfactors) .'"><p>' . $keyfactor->title . '<span>(' . $keyfactor->points . 'points)</span></p></td>';
								    echo "";
							?>	
                             
								
								
                                 <td class="padding-left-zero">
                               @if($keyfactor->scoringtype == 'standard')
								<p class="{{(isset($parameter->sub_points)?'':'notset')}}" style="    font-size: 12px;">{{$parameter->title}} ({{$parameter->points}} points) {{(isset($parameter->sub_points)?'':'score card not set*')}} </p>
							   @else 
								<p>{{$parameter->title}} ({{$parameter->points}} points)</p>
							   @endif
                                </td>
                                 <td class="padding-left-zero">
                                <span class="parameter_{{$parameter->id}}"></span>
                                </td>
                                 <td class=" padding-left-zero align-left" >
								 <?php $ch_area_of_opportunity = "";
									   $ch_feed_back = "";
									   
								 ?>
                                @foreach($parameter->items as $k => $param)

								  <?php 
								      $ch_area_of_opportunity = $ch_area_of_opportunity . (isset($param->aoo)?$param->aoo:'');
									  $ch_feed_back = $ch_feed_back  . (isset($param->fb)?$param->fb:'');
									  if($keyfactor->scoringtype == 'standard'):
										$subpoint = false;
										if(isset($parameter->sub_points)):
											$subpoint = $parameter->sub_points[$k]->point;
										endif;
									  endif;
								   ?>   

							   
								<p style="margin-bottom:3px;"><input type="checkbox" class="checkbox param_{{$parameter->id}}" data-qa_element_val="@if($keyfactor->scoringtype == 'standard'){{($subpoint?$subpoint:0)}}@else 0 @endif"  data-param="{{$parameter->id}}" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$k}}|status" {{($param->status?'checked':'')}}>{{$param->title}} @if($keyfactor->scoringtype == 'standard') (-{{($subpoint?$subpoint:0)}})@endif</p>
								<div style="display:none"><p><span class="lighter">Areas of opportunity:</span><input type="text" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$k}}|aoo"  value="{{(isset($param->aoo)?$param->aoo:'')}}"></p>
								<p><span class="lighter">Feedback:</span> <input type="text" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$k}}|fb"  value="{{(isset($param->fb)?$param->fb:'')}}" ></p>
								</div>
								@endforeach

                                </td>
								 
                                 <td class="padding-left-zero padding-zero" style="background-color:#f2f2f2;">
								 
								 
                                <textarea class="form-control" style="background-color:#f2f2f2;height: 100px" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$k}}|aoo">{{$ch_area_of_opportunity}}</textarea>
                                </td>
                                 <td class="padding-left-zero padding-zero" style="background-color:#f2f2f2;">
                                <textarea class="form-control" style="background-color:#f2f2f2; height: 100px" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$k}}|fb">{{$ch_feed_back}}</textarea>
                                </td>

                            </tr>
							@endforeach
                     
					@endforeach
                         </tbody>
						  
                        </table>

                    </div> 
                </div>
 @endif    

 
 </form>
   
</body>
  
  <!-- <script src="{{$asset}}milestone/scripts/app.min.js"></script> -->
  <script type="text/javascript" src="{{$asset}}bower_components/moment/min/moment.min.js"></script>
  <script type="text/javascript" src="{{$asset}}bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
  <link rel="stylesheet" href="{{$asset}}bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<script type="text/javascript">
$('#datetimepicker').datetimepicker({
    format: 'YYYY-MM-DD HH:MM:00'
});
</script>

</html>