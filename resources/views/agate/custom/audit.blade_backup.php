<!doctype html><?php $asset = URL::asset('/'); ?> 
<html lang="en">
<!-- Mirrored from milestone.nyasha.me/latest/html/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Aug 2016 23:36:40 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Milestone">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Milestone">
    <meta name="theme-color" content="#4C7FF0">
    <title>Milestone - Bootstrap 4 Dashboard Template</title>

   <!-- <link rel="stylesheet" href="{{$asset}}CSS/bootstrap.css">
    <script src="{{$asset}}JS/bootstrap.min.js"></script>
    <script src="{{$asset}}JS/jquery-3.2.1.js"></script>
    
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <style type="text/css">
    .mb-body{
      width: 90%;
      margin: 20px auto;
    }
    .mb-table th,.mb-table td{
      padding: 2px 5px;
    }
    .mb-table select{
      background-color: #ebebeb; 
      color: #333;
      padding: 2px;      
    }
    .mb-table tr{
      line-height: 30px;
    }
    .mb-table tr input{
      line-height: 20px;
    }
    .keyfactors > p{
background-color: gray;
    color: #fff;
    padding: 6px;
    text-align: center;
    text-transform: uppercase;
    font-weight: bold;
    }
    .keyfactors{
      background-color: #ebebeb;
      padding: 10px;
      margin: 10px;      
    }
    .parameters{
      margin-left: 15px;
    }
    .parameters p{
      margin: 0;
    }
    .items{
      margin-left: 100px;
      padding: 10px;
    }
    .lighter{
          color: #999;
    }
    .items p{
      margin: 0;
    }
    .items input[type="text"]{
      display: inline-block;
      width: 100%;
      position: relative;

    }
    #mb-menu{
      position: absolute;
      z-index: 999;
      background-color: rgba(0,0,0,.3);
      text-align: right;
      padding: 5px;
      left: 0;
      top: 0;
      position: fixed;
      border-radius: 2px;
      width: 100%;   
    }
    .mb-btn{
        color: #fff;
        background-color: #06BC5A;
        border-color: #05a34e;
        font-size: 12px;
        text-transform: uppercase;
        padding: 5px;
        border: 1px solid #05a34e;
    }
    .notset{
      background-color: red;
    }
    </style>
</head>

<body>
<form method="POST" action="{{route('agate.custom.score',['id' => $id])}}">
     {{ csrf_field() }}
    <div class="app">
      <div class="mb-body">
       <div id="mb-menu">
         <button type="submit" class="mb-btn">Update</button> 
         <a href="{{route('agate.dashboard.index')}}" class="mb-btn">Dashboard</a>
       </div>
          <div class="card" style="margin-top: 30px;">
              
              <div class="card-header no-bg b-a-0">Custom Audit</div>
              <div class="card-block">
              @if($item->qa_results)

                <?php 
                  $template = json_decode($item->qa_results);
                  $except = [];
                 ?>

                <table class="mb-table">
                  <tr>

                    <th>Agent</th>
                    <td>
                      <select name="agent_id">
                        @foreach($agents as $agent)
                          <option value="{{$agent->id}}" {{($agent->id == $item->agent?'selected':'')}} >{{$agent->name}}</option> 
                        @endforeach
                      </select>
                    </td>

                    <th>Campaigns:</th>
                    <td>
                      <select name="campaign_id">
                          @foreach($campaigns as $campaign)
                          <option value="{{$campaign->id}}" {{($campaign->id == $item->campaign_id?'selected':'')}}>{{$campaign->title}}</option>
                          @endforeach
                      </select>
                    </td>

                    <th>Monitoring Type</th>
                    <td>
                      <select name="monitoring_type">
                        <option value="Recorded" {{($item->monitoring_type == "Recorded"?"selected":"")}} >Recorded</option>
                        <option value="Remote" {{($item->monitoring_type == "Remote"?"selected":"")}} >Remote</option>
                        <option value="Side By Side" {{($item->monitoring_type == "Side By Side"?"selected":"")}} >Side By Side</option>
                      </select>                              
                    </td>

                    <th>Archives:</th>
                    <td>
                          <select name="archive_id">
                              @foreach($archives as $archive)
                              <option value="{{$archive->id}}" {{($archive->id == $item->archive_id?'selected':'')}}>{{$archive->name}}</option>
                              @endforeach
                          </select>
                    </td>    

                  </tr>

                  <tr>
                      <th>Evaluator:</th>
                      <td>
                        {{Auth::user()->name}}
                      </td>  
                      <th>Customer Name</th>
                      <td><input type="text" value="{{$item->customer_name}}" name="customer_name"></td>
                      <th>Customer Phone</th>
                      <td><input type="text" value="{{$item->customer_phone}}" name="customer_phone"></td>

                  </tr>
                  <tr>
                        <th>Dispo:</th>
                        <td>
                          <input type="text" name="dispo" value="{{$item->dispo}}" >
                        </td>
                        <th>Date:</th>
                        <td>
                          <input type="text" name="call_date" id="datetimepicker" value="{{$item->call_date}}">
                        </td> 
                      <th>Score/Points</th>
                      <td>{{$item->score}}/{{$item->point}}</td>                                                                    
                  </tr>
                </table>
                <div>
                  <label>Call Summary</label> <textarea class="form-control" name="call_summary">{{$item->call_summary}}</textarea>
                </div>

                @foreach($template->keyfactors as $keyfactor)
                  <?php if(!isset($keyfactor->scoringtype)) $keyfactor->scoringtype = 'standard'; ?>

                  <div class="keyfactors">
                    <p>{{$keyfactor->title}} <span>({{$keyfactor->points}} points)</span></p>
                    @foreach($keyfactor->parameters as $parameter)
                    <div class="parameters">
                    
                       @if($keyfactor->scoringtype == 'standard')
                        <p class="{{(isset($parameter->sub_points)?'':'notset')}}">{{$parameter->title}} ({{$parameter->points}} points) {{(isset($parameter->sub_points)?'':'score card not set*')}} </p>
                       @else
                        <p>{{$parameter->title}} ({{$parameter->points}} points)</p>
                       @endif

                       @foreach($parameter->items as $k => $param)

                          <?php 
                              if($keyfactor->scoringtype == 'standard'):
                                $subpoint = false;
                                if(isset($parameter->sub_points)):
                                    $subpoint = $parameter->sub_points[$k]->point;
                                endif;
                              endif;
                           ?>   

                       <div class="items">
                        <p><input type="checkbox" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$k}}|status" {{($param->status?'checked':'')}}>{{$param->title}} @if($keyfactor->scoringtype == 'standard') (-{{($subpoint?$subpoint:0)}})@endif</p>
                        <p><span class="lighter">Areas of opportunity:</span><input type="text" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$k}}|aoo"  value="{{(isset($param->aoo)?$param->aoo:'')}}"></p>
                        <p><span class="lighter">Feedback:</span> <input type="text" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$k}}|fb"  value="{{(isset($param->fb)?$param->fb:'')}}" ></p>
                       </div>
                       @endforeach
                    </div>
                    @endforeach
                  </div>
                @endforeach
              </div>

              @endif
          </div>
      </div>
    </div>

</form>
</body>
  
  <!-- <script src="{{$asset}}milestone/scripts/app.min.js"></script> -->
  <script type="text/javascript" src="{{$asset}}bower_components/moment/min/moment.min.js"></script>
  <script type="text/javascript" src="{{$asset}}bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
  <link rel="stylesheet" href="{{$asset}}bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<script type="text/javascript">
$('#datetimepicker').datetimepicker({
    format: 'YYYY-MM-DD HH:MM:00'
});
</script>

</html>