<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('header-scripts')
  <!-- ... -->

<style type="text/css">
  .audits-audit th{
        background-color: rgba(28, 162, 206, 0.25);
  }
</style> 
@endsection

@section('content')
<form method="POST" action="{{route('agate.custom.score',['id' => $id])}}">
	<header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.custom.audit',$id) !!}
                    </h1>
                </div>
            </div>
        </div>
     </header>


  <div class="widget widget-default">
      <div class="widget-body">

          @if($item->qa_results)

            <?php 
              $template = json_decode($item->qa_results);
              $except = [];
             ?>

             
            

                {{ csrf_field() }}

                <button type="button" style="margin-left:5px;" class="btn btn-success btn-xs btn-transparent btn-transparent-success pull-right" data-toggle="modal" data-target="#refresh-template" data-placement="top"><span class="fa fa-close"></span></button>
                <button type="submit" class="btn btn-success btn-xs pull-right">
                    <span class="fa fa-refresh"></span> Update
                </button>                            
                <div class="clearfix" style="margin-bottom:10px;"></div> 

                <table class ='table table-bordered table-striped audits-audit'>
                  <tbody> 
                    <tr class="form-group">
                        <th>Agent:</th>
                        <td>
                            <select class="form-control" name="agent_id"> 
                                @foreach($agents as $agent)
                                <option value="{{$agent->id}}" {{($agent->id == $item->agent?'selected':'')}}>{{$agent->name}}</option>
                                @endforeach
                            </select>                                
                        </td>
                        <th>Campaigns:</th>
                        <td>

                          <select class="form-control" name="campaign_id">
                              @foreach($campaigns as $campaign)
                              <option value="{{$campaign->id}}" {{($campaign->id == $item->campaign_id?'selected':'')}}>{{$campaign->title}}</option>
                              @endforeach
                          </select>


                        </td>                               
                        <th>Date:</th>
                        <td>
                          <input type="text" name="call_date" class="form-control" id="datetimepicker" value="{{$item->call_date}}">
                        </td>                             
                    </tr>

                    <tr>
                        <th>Evaluator:</th>
                        <td>
                          <p>{{Auth::user()->name}}</p>
                        </td> 
                        <th>Dispo:</th>
                        <td>
                          <input type="text" name="dispo" class="form-control" value="{{$item->dispo}}" >
                        </td>
                        <th>Archives:</th>
                        <td>
                              <select class="form-control" name="archive_id">
                                  @foreach($archives as $archive)
                                  <option value="{{$archive->id}}" {{($archive->id == $item->archive_id?'selected':'')}}>{{$archive->name}}</option>
                                  @endforeach
                              </select>
                        </td>                                                                         
                    </tr>

                    <tr>
                      <th>Customer Name</th>
                      <td><input class="form-control" type="text" value="{{$item->customer_name}}" name="customer_name"></td>
                      <th>Customer Phone</th>
                      <td><input class="form-control" type="text" value="{{$item->customer_phone}}" name="customer_phone"></td>
                      <th>Monitoring Type</th>
                      <td>
                        <select class="form-control" name="monitoring_type">
                          <option value="Call Recording" {{($item->monitoring_type == "Call Recording"?"selected":"")}} >Call Recording</option>
                          <option value="Remote" {{($item->monitoring_type == "Remote"?"selected":"")}} >Remote</option>
                          <option value="Side By Side" {{($item->monitoring_type == "Side By Side"?"selected":"")}} >Side By Side</option>
                        </select>                              
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table class ='table table-bordered table-striped audits-audit'>
                  <tbody>                    
                    <tr>
                      <th>Call Summary</th>
                      <td>
                        <textarea class="form-control" name="call_summary">{{$item->call_summary}}</textarea>
                      </td>
                    </tr>
                    <tr>
                      <th>Score</th>
                      <td>{{$item->score}}</td>
                    </tr>
                  </tbody>
                </table>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class ='table table-bordered audits-audit'>
                          <thead>
                              <tr>
                                  <th>Key Factors</th>
                                  <th>Parameters</th>
                                  <th>QA Elements</th>
                                  <th>Areas of Opportunity</th>
                                  <th>Feedback</th>
                              </tr>
                          </thead>                                      
                          <tbody>
                            @foreach($template->keyfactors as $keyfactor)
                              <tr>
                                <th class="align-c">{{$keyfactor->title}}<br> {{$keyfactor->points}}(%)</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                                @foreach($keyfactor->parameters as $parameter)
                                  <tr>
                                    <td></td>
                                    <th class="align-c">
                                      {{$parameter->title}}
                                      <br>
                                      ({{$parameter->points}} points)
                                    </th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                  </tr> 
                                      @foreach($parameter->items as $param)
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td><input type="checkbox" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$param->title}}|mark" {{($param->status?'checked':'')}}  data-icheck>{{$param->title}}</td>
                                        <td><input type="text" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$param->title}}|aoo" class="form-control" value="{{(isset($param->aoo)?$param->aoo:'')}}"></td>
                                        <td><input type="text" name="{{$keyfactor->id}}|{{$parameter->id}}|{{$param->title}}|fb" class="form-control" value="{{(isset($param->fb)?$param->fb:'')}}" ></td>
                                      </tr>                                              
                                      @endforeach                                         
                                @endforeach
                            @endforeach
                          </tbody>
                        </table>
                    </div>
                </div>

             
                                     


      </div>
  </div>



</form>

  @else



  @endif   
@endsection 

@section('footer-scripts') 
  <script type="text/javascript" src="{{$asset}}bower_components/moment/min/moment.min.js"></script>
  <script type="text/javascript" src="{{$asset}}bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
  <link rel="stylesheet" href="{{$asset}}bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<script type="text/javascript">
$('#datetimepicker').datetimepicker({
    format: 'YYYY-MM-DD HH:MM:00'
});
</script>

@endsection