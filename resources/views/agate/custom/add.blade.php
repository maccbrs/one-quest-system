<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('header-scripts')
@endsection

@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                        {!! Breadcrumbs::render('agate.custom.add') !!}
                    </h1>
                </div>
            </div>
        </div>
     </header>

    <div class="widget widget-default">

        <div class="widget-body">

            <form method="POST" action="{{route('agate.custom.create')}}">

                {{ csrf_field() }}


                <button type="submit" class="btn btn-success btn-xs pull-right">
                    <span class="fa fa-check"></span> Create
                </button> 

                <div class="clearfix" style="margin-bottom:10px;"></div> 



                <div class="row" style="margin-bottom:10px;">

                  <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-sm-5">Template</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="template_id">
                                @foreach($templates as $template)
                                <option value="{{$template->id}}">{{$template->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>  
                  </div> 

                  <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-sm-5">Bound</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="bound">
                              <option value="inbound"> Inbound</option>
                              <option value="outbound"> outbound</option>
                            </select>
                        </div>
                    </div>  
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-sm-5">Archive</label>
                        <div class="col-sm-7">
                        <select class="form-control" name="archive_id">
                            @foreach($archives as $archive)
                            <option value="{{$archive->id}}">{{$archive->name}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>  
                  </div>                                                                                           
                </div>



            </form>                  

        </div>
    </div>

@endsection 

@section('footer-scripts')

@endsection