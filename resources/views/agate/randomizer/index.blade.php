<?php $asset = URL::asset('/'); ?> 
@extends('agate.master')

@section('title', 'index')

@section('content')

    <header class="page-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="page-header-heading">
                       {!! Breadcrumbs::render('agate.randomizer.index') !!}
                    </h1>
                </div>
            </div>
        </div>
     </header>

    <div class="widget widget-default">
        <header class="widget-header">
            Randomizer
        </header>
        <div class="widget-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Project Name</th>
                    </tr>
                </thead>                        
                <tbody>
                  @if($sets->count())
                    @foreach($sets as $s)
                    <tr >
                        <td><a href="{{route('agate.set.index',$s->id)}}">{{$s->title}}</a></td>
                    </tr> 
                    @endforeach
                  @endif                              
                </tbody>
            </table>
        </div>
    </div>
@endsection 

@section('footer-scripts')

@endsection