@if ($paginator->lastPage() > 1)

<?php 

    $count = round(count($paginator->currentPage())/2);  
    $remaining = $paginator->currentPage() - 5;
    $limit  = $remaining + 9;

    if($remaining  < 0){

        $remaining = 0;
    }

?>

<div style ="text-align:center;">
    <ul class="pagination">

        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }} page-item">
            <a class="page-link" href="{{ $paginator->url($remaining-10) }}" aria-label="Previous">--</a>
        </li>

      
        <li class="{{ (($paginator->currentPage() -1) < 1 ) ? ' disabled' : '' }} page-item">
            <a class="page-link" href="{{ $paginator->url($paginator->currentPage() -1)  }}" >Previous</a>
        </li>


        @for ($i = $remaining; $i <= $limit; $i++)

            <?php  $remaining++ ?>

            @if( $remaining > 0)

                <li class="{{ ($paginator->currentPage() == $remaining) ? ' active' : '' }} {{ ($paginator->lastPage() < $i ) ? ' disabled' : '' }} page-item">
                    <a class="page-link" href="{{ $paginator->url($remaining) }}">{{$remaining }}</a>
                </li>

            @endif

        @endfor


        <li class="{{ (($paginator->currentPage() + 1) < 1 ) ? ' disabled' : '' }} page-item">
            <a class="page-link" href="{{ $paginator->url($paginator->currentPage() + 1)  }}" >Next</a>
        </li>


        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }} page-item">
            <a class="page-link" href="{{ $paginator->url($remaining+10) }}" >++</a>
        </li>
    </ul>
</div>


@endif

